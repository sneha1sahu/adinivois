<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<c:set var="now" value="<%=new java.util.Date()%>" />
<html>
<head>
	<title>Home</title>
</head>
<script type="text/javascript">
function getServcie() {
	
	var login=false;
	
	var userName=document.getElementById("userName").value;	
	var userPass=document.getElementById("userPass").value;
	if(userName==null || userName==''){
		alert('Please provide UserName ')
	}
	else if(userPass==null || userPass==''){
		alert('Please provide Password ')
	}
		
	else if((userName!=null && userPass!=null) && (userName=="admin" && userPass=="admin") )
		login=true;
	else
		alert(' wrong credentials')
	
	if(login){
		document.getElementById("loginTable").style.display= 'none';
		document.getElementById("servcieTable").style.display= 'block';
	}
}
</script>
<body style="background-color: #e1fcfd;">
<h1>
	Api-CloudUnion!  
</h1>

<P>  The time on the server is <fmt:formatDate type="both" 
            dateStyle="long" timeStyle="long" 
            value="${now}" />. </P>

<div style="display: block;" id="loginTable">
<table >
<tr>
<td>User Name :</td>
<td> <input type="text" name="userName" id="userName"> </td>
</tr>
<tr>
<td>Password :</td>
<td> <input type="password" name="userPass" id="userPass"> </td>
</tr>
<tr>

<td colspan="2"> <input type="button" name="submit" value="submit" onclick="javascript:getServcie()"> </td>
</tr>

</table></div>
<div style="display: none;" id="servcieTable">
<table border="2">
<thead style="background-color: #9dd29d;" align="center">
<tr>
<td>S.No.</td>
<td>Service Name</td>
<td>URL</td>
<td>Method</td>
<td>Request</td>
<td>Response</td>
</tr>
</thead>
<tbody  align="center">
<tr>
<td align="center">1</td>
<td align="center">Login</td>
<td align="left"><a ><c:out value="${path}"/>v1/login</a></td>
<td align="center">Post</td>
<td align="left">{"loginId": "srinivas.parvataneni@gmail.com","logintype": "2","password": "12345678"}</td>
<td align="left">{"data": {
"access_token": "IVpB6Q3jzAx6f9QzVE5cEKlXrGal3fbUAfQaFr0UHJS3r/eIaLycdjlnLzrcMh7k",</br>
"refresh_token": "QzN/WAShlRDe2jRSeaWLEXwMjSIP8ldV52ixeg5ZKyOFKHP4Dh2Tr2I6QNya67ET80vaqWdV2s6r</br> NTrMXWizR04hPKiXkmPWG4XOtM9thv4=",
"expires_in": 0,"loginId": "srinivas.parvataneni@gmail.com","uid": 452,
</br>"firstName": "Srinivas",
</br>"lastName": "Parvataneni","referralCode": "1490876505518487",
</br>"referralQr": "http://localhost:8080/api-cloudunion/v1/referral/1490876505522487.png",
</br>"profileUrl": "http://localhost:8080/api-cloudunion/v1/profile/487-1490942574177.jpg"
},
"_statusCode": "200",
"_statusMessage": "facebook user"
}</td>
</tr>
<tr>
<td align="center">2</td>
<td align="center">Is User Exist</td>
<td align="left"><a ><c:out value="${path}"/>v1/{emailId}/getmailid</a></td>
<td align="center">Get</td>
<td align="left">srinivas.parvataneni@gmail.com</td>
<td align="left">{
"data": true,
"_statusCode": "200",
"_statusMessage": "Email Id is already exist"
}</td>
</tr>
<tr>
<td align="center">3</td>
<td align="center">User Registration</td>
<td align="left"><a ><c:out value="${path}"/>v1/register</a></td>
<td align="center">Post</td>
<td align="left">{"loginId": "gouri986795@gmail.com", "loginType": 1,
   </br> "userType":1,"password": "muna",  "firstName": "neeraj",
   </br> "lastName": "sanodiya", "deviceToken":"kfjjeshfjr376", "osType":
} 
</td>
</tr>
<tr>
<td align="center">4</td>
<td align="center">Get Qr Code</td>
<td align="left"><a ><c:out value="${path}"/>v1/referral/1490704152231465.png</a></td>
<td align="center">GET</td>
<td align="left"><img alt="qrCode" src="http://localhost:8080/api-cloudunion/v1/referral/1490704152231465.png" width="100" height="100"> 
</td>
</tr>

<tr>
<td align="center">5</td>
<td align="center">Refresh Token</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/refreshToken</a></td>
<td align="center">Post</td>
<td align="left">{"refreshToken":"2FvcwTyNoTZAcgLPCybniOnXDIrdJ</br>ynwpkBJ78peHXJyy4TJg2Qx+3K6ZQeGkNvwyrLBEx+pkSVM\r\</br>ny10hj6sSME4hPKiXkmPWG4XOtM9thv4="}
</td>
</tr>

<tr>
<td align="center">6</td>
<td align="center">Logout</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/logout</a></td>
<td align="center">Post</td>
<td align="left"></td>
</tr>

<tr>
<td align="center">7</td>
<td align="center">getProfile Pic</td>
<td align="left"><a ><c:out value="${path}"/>v1/profile/1490874568339.jpg</a></td>
<td align="center">GET</td>
<td align="left"><img alt="userProfile" src="http://localhost:8080/api-cloudunion/v1/profile/1490874568339.jpg" width="100" height="100"> 
</td>
</tr>

<tr>
<td align="center">8</td>
<td align="center">Transaction Pic</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/268/uploads/transaction/image/1490515187374.jpg</a></td>
<td align="center">Get</td>
<td align="left"></td>
</tr>




<tr>
<td align="center">9</td>
<td align="center">User Points</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/user/getPoint</a></td>
<td align="center">Get</td>
<td align="left">{uid:260}</td>
</tr>
<tr>
<td align="center">10</td>
<td align="center">Update User Profile</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/user/updateProfile</a></td>
<td align="center">POST</td>
<td align="left">{"uId":"380","firstName":"postman","lastName":"postman",
</br>"gender":"male","mobileNo":"8125932366",
</br>"dateOfBirth":"14-Oct-1990","street1":"street1",</br>
"street2":"street2","city":"city","state":1,"zip":"500016"}</td>
</tr>

<tr>
<td align="center">11</td>
<td align="center">Update User Profile Image</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/user/updateProfileImage</a></td>
<td align="center">POST</td>
<td align="left"> </td>
</tr>

<tr>
<td align="center">12</td>
<td align="center">getUserProfile</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/user/getProfile</a></td>
<td align="center">Get</td>
<td align="left"></td>
</tr>
<tr>
<td align="center">13</td>
<td align="center">Get Suggestion</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/transaction/getSuggestion</a></td>
<td align="center">Get</td>
<td align="left"></td>
</tr>

<tr>
<td align="center">14</td>
<td align="center">Upload Transaction</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/transaction/uploadTransaction</a></td>
<td align="center">POST</td>
<td align="left">{"uId":"268", "transactionPic":"data:image/false;base64,encodedImage" ,
</br>"businessId":"abc", "transactionDate":"2017-03-03", "receiptNo":"1",
</br> "transactionAmount":"90", "comment":"Hiiiiiiii", "rating":"2"}</td>
</tr>

<tr>
<td align="center">15</td>
<td align="center">getPurchases </td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/business/getPurchases?offset=0&limit=20</a></td>
<td align="center">GET</td>
<td align="left">{"tranactionId":"132"}</td>
</tr>
<tr>
<td align="center">16</td>
<td align="center">getPurchases by Id</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/business/getPurchases?tranactionId=tranactionId</a></td>
<td align="center">GET</td>
<td align="left">{"tranactionId":"132"}</td>
</tr>

<tr>
<td align="center">17</td>
<td align="center">get Transaction Image</td>
<td align="left"><c:out value="${path}"/>v1/security/{uId}/transaction/getImage/1490359091923.jpg</td>
<td align="center">Get</td>
<td align="left">:access_token</td>
</tr>
<tr>
<td align="center">18</td>
<td align="center">Check Referral code</td>
<td align="left"><a ><c:out value="${path}"/>v1/referral/check/{referralCode}</a></td>
<td align="center">GET</td>
<td align="left">
</td>
</tr>
<tr>
<td align="center">19</td>
<td align="center">getSales Transaction</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/business/getSales?offset=0&limit=20</a></td>
<td align="center">GET</td>
<td align="left">{"uId":"458"}</td>
</tr>

<tr>
<td align="center">20</td>
<td align="center">getSales by Id</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/business/getSales?tranactionId=tranactionId</a></td>
<td align="center">GET</td>
<td align="left">{"tranactionId":"132"}</td>
</tr>
<tr>
<td align="center">21</td>
<td align="center">getBusiness by Id</td>
<td align="left"><a ><c:out value="${path}"/>v1/security/{uId}/business/getBusiness</a></td>
<td align="center">GET</td>
<td align="left">{"tranactionId":"132"}</td>
</tr>



</tbody>

</table></div>
</body>
</html>
