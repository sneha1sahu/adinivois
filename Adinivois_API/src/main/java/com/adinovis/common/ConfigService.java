/**
 * Program Name: ConfigService
 * 
 * Program Description / functionality: This class is for configuration service to read properties
 *            from properties files.
 * 
 * Modules Impacted:
 * 
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri        10/03/2017
 *  
 */

package com.adinovis.common;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service(value = "configService")
@Scope(value = "singleton")
@PropertySources({@PropertySource("classpath:errormessages.properties"),
    @PropertySource("classpath:mycompany.properties"),@PropertySource("classpath:azure.properties")})
public class ConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigService.class);
  
  @Autowired
  private Environment environment;

  private static Environment env;

  @PostConstruct
  public void init() {
    env = environment;
  }

  public static String getProperty(String key) {

    LOGGER.debug("Getting property  :" + key);

    return env.getProperty(key);

  }

}
