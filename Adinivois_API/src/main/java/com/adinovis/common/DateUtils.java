/**
 * Program Name: DateUtils
 * 
 * Program Description / functionality: This class is to provide date related utility operations such as date conversions for the
 * application
 * 
 * Modules Impacted: 
 * 
 *  * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri         10/03/2017 
 * 
 * * Associated Defects Raised : 
 *
 */

package com.adinovis.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;


public class DateUtils {
	public static final String CLOUDUNION_DATE_TIMESTAMP="MM-dd-yyyy HH:mm:ss aaa";
	public static final String CLOUDUNION_DATE="MM-dd-yyyy";
	public static final String CLOUDUNION_DB_DATE_TIMESTAMP="yyyy-MM-dd HH:mm:ss";
	public static final String CLOUDUNION_DB_DATE="yyyy-MM-dd";

  /**
   * 
   * Purpose: This method converts string value of date to yyyy-MM-dd format
   * 
   * 
   * @param  dateValue
   * @throws TrinetParseException
   * @return date
   */
  /*public static Date convertStringToDate(String dateValue) throws TrinetParseException{
    try{
      SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
      format.setLenient(true);
      Date date = format.parse(dateValue);
      return date;
    }catch(ParseException pe){
      throw new TrinetParseException("ERR-MYCOMP-60004") ;
    }
  }*/
  public static Date convertStringToDate(String dateValue) {
    
    Date date11=null;
    
       //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aaa");
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
       try
       {
           date11 = simpleDateFormat.parse(dateValue);

           System.out.println("date : "+simpleDateFormat.format(date11));
       }
       catch (Exception ex)
       {
           System.out.println("Exception "+ex);
       }
       return date11;
   }
  
 public static Date convertStringToDate(String format,String dateValue) {
    
    Date date=null;
    
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
       try
       {
         date = simpleDateFormat.parse(dateValue);
       }
       catch (Exception ex)
       {
           System.out.println("Exception "+ex);
       }
       return date;
   }
 public static Date convertDateToDate(String format,Date dateValue) {
	    
	    Date date=null;
	    
	       SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
	       try
	       {
	      String   sdate = simpleDateFormat.format(date);
	      date=simpleDateFormat.parse(sdate);
	       }
	       catch (Exception ex)
	       {
	           System.out.println("Exception "+ex);
	       }
	       return date;
	   }

  /**
   * 
   * Purpose: This method converts string value of date to yyyy-MM-dd format
   * 
   * 
   * @param dateValue
   * @return date
   */
  public static String convertDateToString(Date dateValue) {
    String date = null;
    //SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy ");
    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aaa");
    date = format.format(dateValue);
    return date;
  }
  public static String convertDateToString(Date dateValue,String dateformat) {
	    String date = null;
	    if (dateValue == null) {
	    	return "";
	    }
	    //SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy ");
	    SimpleDateFormat format = new SimpleDateFormat(dateformat);
	    date = format.format(dateValue);
	    return date;
	  }
  /**
   * 
   * Purpose: This method gets Effective Time Stamp
   * 
   * 
   * @return date
   */
  public static Date getEffectiveTimeStamp() {
	return new Date();
  }
  
  /**
   * 
   * Purpose: This method gets Effective date
   * 
   * 
   * @return date
   */
  public static Date getEffectiveDate() {
    return getZeroTime(new Date());
  }
  public static Date getStartDate() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DATE, +1);
	    return calendar.getTime();
	  }
  
  /**
   * 
   * Purpose: This method gets end date
   * 
   * 
   * @return date
   */
  public static Date getEndDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(2099, Calendar.DECEMBER, 31);
    return calendar.getTime();
  }
  
  /**
   * 
   * Purpose: This method gets previous date
   * 
   * 
   * @return date
   */
  public static Date getPreviousEndDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -1);
    return calendar.getTime();

  }
  
  /**
   * 
   * Purpose: This method is for comparing dates
   * 
   * 
   * @param date1
   * @param date2
   * @return date
   */
  public static int compareDates(Date date1, Date date2) {
    return getZeroTime(date1).compareTo(getZeroTime(date2));
  }
  
  /**
   * 
   * Purpose: This method is to get zero time
   * 
   * 
   * @return date
   */
  public static Date getZeroTime(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  /**
   * 
   * Purpose: This method is to get previous date
   * 
   * 
   * @return date
   */
  public static Date getPerviousDay(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int daysToDecrement = -1;
    cal.add(Calendar.DATE, daysToDecrement);
    date = cal.getTime();
    return date;
  }

  public static String getPeriod(Date date){
    Interval interval = new Interval(date.getTime(), DateUtils.getEffectiveDate().getTime());
    Period period = interval.toPeriod().normalizedStandard(PeriodType.yearMonthDay());
    PeriodFormatter formatter = new PeriodFormatterBuilder()
                .appendYears()
                .appendSuffix(" year", " years")
                .appendSeparator(", ")
                .appendMonths()
                .appendSuffix(" month", " months")
                .appendSeparator(", ")
                .appendDays()
                .appendSuffix(" day", " days")
                .toFormatter();
    return formatter.print(period);
  }

  
}
