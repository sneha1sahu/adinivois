package com.adinovis.common;

public class TransactionHandlerQuery {
	public static String GET_CU_POINTS="select cu_points.u_account_id,user_type_name,membership_cash_conv_percentage,membership_managementfee,membership_consumer_ref_percentage,membership_business_ref_percentage,membership_monthly_fee,p_sky_points,p_sky_purchase_points,p_sky_referral_points,p_sky_purchase_business_factor_points,p_sky_purchase_growth_factor_points,p_sky_referral_growth_factor_points,p_cash_points_commulative,p_ltime_sky_points,p_ltime_cash_points,p_ltime_redeemed_points,p_as_of_date from cu_points,ref_user_type,cu_users,ref_membership,cu_user_account where  cu_users.u_account_id=cu_points.u_account_id and cu_users.u_type=ref_user_type.user_type_id and cu_user_account.u_membership_level=ref_membership.membership_type";
	public static String GET_TRANSACTION="select cu_transaction_id,trans_posted_date,trans_actual_date,trans_business_id , trans_consumer_id,trans_amount_grand_total,trans_amount_before_tax_tips,trans_day_bus_referral trans_day_con_referral,trans_day_bus_referral_membership,trans_day_con_referral_membership trans_day_bus_discount,trans_status_id  from cu_transaction where cu_transaction_id =:cu_transaction_id ";
	
	public static String TransactionDependencies="select cu_transaction_id,trans_day_bus_discount, trans_day_con_referral,trans_day_bus_referral,trans_day_con_referral_membership,trans_day_bus_referral_membership,membership_consumer_ref_percentage,membership_business_ref_percentage from cu_transaction,ref_membership,cu_user_account where cu_user_account.u_account_id=cu_transaction.trans_consumer_id and cu_user_account.u_membership_level=ref_membership.membership_type and cu_transaction_id=";
	
	public static String sqlRefereeId="select 	referred_by_account_id from account_referral where account_id=";
	
	public static String GET_MEMBERSHIP_DETAILS="select membership_id,membership_type,membership_name,membership_for,membership_cash_conv_percentage,membership_managementfee,membership_consumer_ref_percentage,membership_business_ref_percentage,membership_monthly_fee,membership_details_effective_date,membership_details_end_date from ref_membership";
	public static String approveTransactionQuery = "update  cu_test.cu_transaction  set trans_status_id =:trans_status_id, trans_day_bus_discount =:trans_day_bus_discount,	trans_day_bus_referral =:trans_day_bus_referral, trans_day_bus_referral_membership =:trans_day_bus_referral_membership, trans_day_con_referral =:trans_day_con_referral ,trans_day_con_referral_membership =:trans_day_con_referral_membership , trans_day_bus_discount_amount =:trans_day_bus_discount_amount , purchase_points =:purchase_points	where cu_transaction_id =:cu_transaction_id ";
	public static String getReferralDependencyQuery (String consumerID, String businessID, String transDatePassed) {
		
		String asInString = "";
		String transDate = "'" + transDatePassed + "'"; // is it immutable
		
		// no need to execute the query - all null -- not a case - 
		if ((consumerID == null) && (businessID == null)) return asInString; 
			
		if ((consumerID != null) && (businessID != null)) {
			asInString = "(" + consumerID + "," + businessID + ")"; 
		} else if (businessID != null) {
			// consumer_id is null
			asInString = "(" + businessID + ")";
		} else {
			// business_id is null
			asInString = "(" + consumerID + ")";
		}
		
		String queryToReturn = 
				" ( select cua.u_account_id as \'account_id\', cua.referred_by_account_id as \'account_referee\', cam.am_membership_id as \'referee_membership\'"
						+ " from cu_user_account cua, cu_account_memberships cam" + 
						" where cua.u_account_id in " + asInString + 
				" and " +
				   " cua.referred_by_account_id = cam.account_id " +
                " and " +
				   " cam.am_effective_date <= " + transDate + ") " +
				   " union " +
                " ( select cua.u_account_id as \'account_id\', cua.referred_by_account_id as \'account_referee\', cama.previous_membership_id as \'referee_membership\' " +
				   " from cu_user_account cua, cu_account_memberships_audit cama " +
				   " where  cua.u_account_id in " + asInString +
				" and " +
				   " cua.referred_by_account_id = cama.account_id " +
                " and " +
				   " ( cama.previous_membership_start_date <=  " + transDate + " " + 
                "   and " + 
				   "   cama.previous_membership_end_date >= " + transDate + ")) ";
		
		
		// returns a QUERY string
		return queryToReturn;
		
	}
public static String getDiscountDependencyQuery (String businessID, String transDatePassed) {
	// query the business_discounts - two conditions - may be it is in current
	String sqlStr = 
			" select business_id, discount_rate " + 
	        " from business_discounts " +
			" where business_id = " + businessID + " " +
	        " and discount_start_date <= \'"+transDatePassed+"\'" +
	        " and ((discount_end_date is null) OR (discount_end_date >= \'" + transDatePassed + "\'))";
	return sqlStr;
}

public static String getTransactionQuery(String businessID,String transDatePassed){
	String sqlStr="Select cu_users.u_login_id,cu_users.u_id, cu_users_auth.ua_password_hash,"
			+ "cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token,"
			+ " u_os_type, u_type, up_refferal_code,up_refferal_url,up_picture_profile ,desc_intro,"
			+ "desc_detailed,business_name,cu_users.u_account_id ,u_login_type ,cu_users.u_date_of_join,"
			+ "ref_membership.membership_type from cu_test.cu_users,cu_test.cu_users_auth,"
			+ "cu_test.cu_users_common_profile,cu_test.cu_account_additional_profile,"
			+ "cu_test.ref_membership where cu_users.u_id = cu_users_auth.u_id "
			+ "and cu_users.u_id=cu_users_common_profile.u_id and cu_users.u_login_id=:loginId group BY "
			+ "cu_users.u_login_id,cu_users.u_id, cu_users_auth.ua_password_hash,"
			+ "cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token,"
			+ " u_os_type, u_type, up_refferal_code,up_refferal_url,up_picture_profile ,"
			+ "desc_intro,desc_detailed,business_name,cu_users.u_account_id ,u_login_type ,"
			+ "cu_users.u_date_of_join,ref_membership.membership_type";
	
    return sqlStr;
}
}
