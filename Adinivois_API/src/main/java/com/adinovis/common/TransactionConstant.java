package com.adinovis.common;

public class TransactionConstant {
	public static final Integer TRANSACTION_PENDING=1;
	public static final Integer TRANSACTION_APPROVED=2;
	public static final Integer TRANSACTION_DECLIEND=3;
	public static final Integer TRANSACTION_FUNDED=4;
	public static final Integer TRANSACTION_NOT_DISPUTE=0;
	public static final Integer TRANSACTION_DISPUTE=1;
	
	public static final String TRANSACTION_PENDING_STATUS="Pending";
	public static final String TRANSACTION_APPROVED_STATUS="Approved";
	public static final String TRANSACTION_DECLIEND_STATUS="Declined";
	public static final String TRANSACTION_FUNDED_STATUS="Funded";
	
	public static final Integer NO_REFUND_REQUEST=0;
	public static final Integer REQUESTED_FOR_REFUND=1;
	public static final Integer REQUEST_REFUNDED=2;
	public static final String FUNDING_RECEIPT="FUNDING_REQUEST";
	public static final String UPLOAD_RECEIPT="UPLOAD_TRANSACTION";
	
	public static final double percentageYield=0;
	public static final double businessFactorPercent=4;
	public static final double currentCUGrowthRate=4;
	
	public static final double GROWTH_FACTOR = 5;
	public static final double default_logitute = 40.7589;
	public static final double default_latitude = 73.9851;
	
	public static final int CONSUMER         = 1;
	public static final int BUSINESS         = 2;
	public static final int CONSUMER_REFEREE = 3;
	public static final int BUSINESS_REFEREE = 4;
	
	public static final int PURCHASE         = 100;
	public static final int STATUS_SUCCESS          = 1;
	public static final int STATUS_FAILURE          = 0;
	public static final String SUCCESS          = "SUCCESS";
	public static final String FAILURE          = "FAILURE";
}

