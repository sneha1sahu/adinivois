/**
 *                                                                 
 *  Program Name: CloudunionURIConstants 
 * Program Description / functionality: This is constant interface for Cloudunion controller 
 *                                      which contains all URI link for services.       
 *                        
 * Modules Impacted: My Company
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri         10/03/2017 
 * 
 * * Associated Defects Raised : 
 *
 */

package com.adinovis.common;

public interface CinchConstants {
	public static final String VERSION = "/v1";
	public static final String SECURITY = VERSION+"/security";
	public static final String VERSION_SECURITY = "security";
	public static final String LOGINID = "/{userid}/getloginId";
	
	
	public static final String LOGINIDWITHSOCIAL = "/login";
	public static final String LOGOUT = "/{uId}/logout";
	public static final String REGISTER = "/register";


	//image upload
	public static final String PROFILE_IMAGE = System.getProperty("catalina.home")+"/webapps/profile_images/";
    public static final String UPLOADIMAGE = "/uploadProfileImage/{uId}";
    public static final String GET_USER_IMAGE = "/{uId}/getUserimage";
    
    
	public static final String SIGNUP = "/signup";
	public static final String XERO = "/getxero";
	public static final String GETUSERPROFILE = "/getuserprofile";
	public static final String LOGIN = "/login";
	public static final String SAVECLIENTPROFILE = "/saveclientprofile";
	public static final String UPDATECLIENTPROFILE = "/updateclientprofile";
	public static final String UPDATEUSERPROFILE = "/updateuserprofile";
	public static final String GETCLIENTPROFILE ="/{loginid}/getclientprofile";
	public static final String FORGOTPASSWORD = "/forgotpassword";
	public static final String SIGNUPVALIDATION = "/signupvalidation";
	public static final String CLIENTVALIDATIONMAIL = "/clientvalidationmail";
	public static final String CHANGEPASSWORD = "/changepassword";
	public static final String RESETPASSWORD = "/resetpassword";
	public static final String CLIENTINVITEVALIDATION = "/clientinvitevalidation";
	public static final String GETJURISDICTION ="/getjurisdiction";
	public static final String GETTYPEOFENTITY = "/gettypeofentity";
	public static final String DELETECLIENTPROFILE = "/{clientfirmid}/{loginid}/deleteclientprofile";
	public static final String DELETEENGAGEMENT = "/{engagementsid}/{loginid}/deleteengagement";
	public static final String SAVEENGAGEMENT = "/saveengagement";
	public static final String GETAUDITORROLE = "/getauditorrole";
	public static final String GETALLAUDITOR = "/getallAuditor";
	public static final String GETENGAGEMENTDETAILS = "/{clientfirmid}/{loginid}/getengagementdetails";
	public static final String GET_ENGAGEMENT_YEAR = "/{inengagementsid}/getengagementyeardetails";
	public static final String LOGINVALIDATION = "/login";
	public static final String CLIENTVALIDATION = "/clientlogin/{statusid}";
	public static final String GETALLENGAGEMENT = "/{loginid}/getallengagement";
	public static final String GETBUSINESSDETAIL = "/{loginid}/getbusinessdetails";
	public static final String UPDATEENGAGEMENTDETAILS="/updateengagementdetails";
	public static final String GETENGAGEMENT = "/getengagement";
	public static final String GETENGAGEMENTSTATUS = "/getengagementstatus";
	
	public static final String GETCLIENTCOUNTDETAILS = "/{loginid}/getclientcountdetails";
	public static final String GETENGAGEMENTCOUNTDETAILS = "/{loginid}/getengagementcountdetail";
	
	public static final String GETCLIENTSTATUS = "/getclientstatus";

	public static final String GET_QUICKBOOK_CONNECT = "/connect";
	public static final String QUICKBOOK_REDIRECT = "/oauth2redirect";
	public static final String GET_XERO_CONNECT = "/connect";
	public static final String XERO_REDIRECT = "/XeroCallback";
	
	public static final String GETTRAILBALANCE = "/{engagementsid}/{year}/gettrailbalance";
	public static final String REFRESH_TRAIL_BALANCE = "/{engagementId}/trail-balance";
	 
	public static final String TRAIL_BALANCE_WITH_DUPLICATE ="/duplicate-trail-balance";
	
	public static final String MAP_SHEET_DETAILS="/getmapsheetdetails";
	
	public static final String SAVE_TRAILBALANCE_MAPPING="/save-trailbalance-mapping";
	
	public static final String SAVE_FS_DETAILS="/save-fs-details";
	
	public static final String GETADJUSTMENTTYPE = "/getadjustmenttype";
	
	public static final String GETCLIENTDETAILS = "/getclientdetail";
	
	public static final String DELETETRAILBALANCEROW = "/{inittrialbalid}/{loginid}/deletetrailbalancerow";
	
	public static final String GET_BALANCE_STATEMENT="/{engagementsid}/{year}/getbalancestatement";
	public static final String GET_BALANCE_SHEET="/{engagementsid}/{year}/getbalancesheet";
	public static final String GET_INCOME_STATEMENT="/{engagementsid}/{year}/getincomestatement";
	public static final String GET_INCOME_STATEMENT_TEST="/{engagementsid}/{year}/getincomestatementtest";
	public static final String GET_QUESTION_ANSWER="/{inquestiontypeid}/{inengagementsid}/getquestionanswer";
	public static final String SAVE_TRIALBALANCE_ADJUSTMENT = "/savetrialbalanceadjustment";
	public static final String GET_ALL_TRIAL_ADJUSTMENT = "/{inengagementsid}/getalltrailadjustment";
	public static final String GET_MAPPING_STATUS="/getmappingstatus";
	
	public static final String SAVE_FILE="/savefile";
	public static final String UPDATE_FILE="/updatefile";
	public static final String DELETE_FILE="/deletefile";
	public static final String DELETE_MULTIPLE_FILES="/deletefiles";
	public static final String DELETE_FOLDER="/deletefolder";
	public static final String COPY_FOLDER="/copyfolder";
	public static final String DELETE_FOLDER_WITH_FILES="/deletefolderwithfiles";
	public static final String MOVE_MULTIPLE_FILES="/movefiles";
	public static final String CREATE_FOLDER="/createfolder";
	public static final String GET_FILE="/getfile";
	public static final String GET_ALL_FILES="/getallfiles";
	public static final String SCHEDULAR = "/trail-balance";

}
