/**
 * Program Name: SendMailnotification
 * 
 * Program Description / functionality:This class is to handle notofication for required in the application
 * 
 * Modules Impacted: 
 * 
 * Developer    Created             /Modified Date       Purpose
 *******************************************************************************                              
 * Gouri        10/03/2017                  
 * * Associated Defects Raised : 
 *
 */
package com.adinovis.common;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
@Component
public class SendMailNotification {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMailNotification.class);

    
    @Autowired
	private Configuration freemarkerConfig;

	static String amazon = "AKIAYH5VJ5PN4WY7A5WY";
	static String amazonpassword = "BAybAFT0UINhbs1XuehGRawzkQytPft7JyxXrWimMYIr";
	static String amazonaccount = "admin@adinovis.com";

  
	public static Session setSessionamazon() {
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.starttls.required", "true");
		props.put("mail.smtp.host", "email-smtp.us-east-1.amazonaws.com");
		props.put("mail.port", "587");
		props.put("mail.smtp.port", "587");


		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(amazon, amazonpassword);
			}
		});
		return session;
	}


	public  void welcomeMail(String name, String emailId,String signupvalidation) throws IOException, TemplateException {
		String subject = "welcome to adinovis auditor login";
		LOGGER.debug("Sending mail for welcoming to Adinovis ");
		try {
			String hosturl="http://3.215.103.80:8081";
			MimeMessage message = new MimeMessage(setSessionamazon());
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			helper.setFrom(new InternetAddress(amazonaccount));
			helper.setTo(InternetAddress.parse(emailId));
			helper.setSubject(subject);
			Mail mail = new Mail();
			Template t = freemarkerConfig.getTemplate("verifyAccountEmail.html");
			Map<String, Object> model = new HashMap<>();
			model.put("HOST_URL", hosturl);
			model.put("firstName", emailId);
			model.put("verifyLink", signupvalidation);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			helper.setText(html, true);

			Transport.send(message);
			LOGGER.debug("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public  void welcomeMailclient(String name, String emailId,String signupvalidation) throws IOException, TemplateException {
		String subject = "welcome to adinovis client onboarding";
		LOGGER.debug("Sending mail for welcoming to Adinovis ");
		try {
			String hosturl="http://3.215.103.80:8081";
			MimeMessage message = new MimeMessage(setSessionamazon());
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			helper.setFrom(new InternetAddress(amazonaccount));
			helper.setTo(InternetAddress.parse(emailId));
			helper.setSubject(subject);
			Mail mail = new Mail();
			Template t = freemarkerConfig.getTemplate("inviteClientEmail.html");
			Map<String, Object> model = new HashMap<>();
			model.put("HOST_URL", hosturl);
			model.put("firstName", emailId);
			model.put("auditorFirstName", name);
			model.put("verifyLink", signupvalidation);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			helper.setText(html, true);

			Transport.send(message);
			LOGGER.debug("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	


	public void sendForgotPassword(String name,String toMail,String resetlink)
			throws AddressException, MessagingException, IOException, TemplateException {
		String subject = "Adinovis forgot password";
		LOGGER.debug("Sending mail for welcoming to Adinovis ");
		try {
			String hosturl="http://3.215.103.80:8081";
			MimeMessage message = new MimeMessage(setSessionamazon());
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			helper.setFrom(new InternetAddress(amazonaccount));
			helper.setTo(InternetAddress.parse(toMail));
			helper.setSubject(subject);
			Mail mail = new Mail();
			Template t = freemarkerConfig.getTemplate("forgetPasswordEmail.html");
			Map<String, Object> model = new HashMap<>();
			model.put("HOST_URL", hosturl);
			model.put("firstName", toMail);
			model.put("forgotPasswordLink", resetlink);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			helper.setText(html, true);

			Transport.send(message);
			LOGGER.debug("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
}
	
	public void clientvalidationemail(String name,String toMail,String clientvalidationemail)
			throws AddressException, MessagingException, IOException, TemplateException {
		String subject = "client invition to adinovis";
		LOGGER.debug("Sending mail for welcoming to Adinovis ");
		try {
			String hosturl="http://3.215.103.80:8081";
			MimeMessage message = new MimeMessage(setSessionamazon());
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			helper.setFrom(new InternetAddress(amazonaccount));
			helper.setTo(InternetAddress.parse(toMail));
			helper.setSubject(subject);
			Mail mail = new Mail();
			Template t = freemarkerConfig.getTemplate("inviteClientEmail.html");
			Map<String, Object> model = new HashMap<>();
			model.put("HOST_URL", hosturl);
			model.put("firstName", toMail);
			model.put("auditorFirstName", name);
			model.put("verifyLink", clientvalidationemail);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			helper.setText(html, true);

			Transport.send(message);
			LOGGER.debug("Done");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
}


public static void main(String[] args) throws IOException, TemplateException {
	SendMailNotification mail= new SendMailNotification();
	//mail.welcomeMail("Gouri", "sankar.nayak@ptgindia.com");
}
}
