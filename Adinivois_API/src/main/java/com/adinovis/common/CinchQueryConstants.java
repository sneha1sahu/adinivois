/**
 * Program Name: CloudunionQueryConstants
 * 
 * Program Description / functionality:This interface is to declare constant values required in the application
 * 
 * Modules Impacted:
 * 
 * Developer    Created             /Modified Date       Purpose
 ********************************************************************************
 * Gouri           10/03/2015 
 *
 */
package com.adinovis.common;

public interface CinchQueryConstants {
	Integer offset = 0;
	Integer limit = 20;

	String GET_IMPORTANT_NOTICES = "select t1.*,t2.DISPLAY_SEQ as displaySeq from cu_test.text_tokens t1, cu_test.splash_content_seq t2 where t1.text_tokenid=t2.text_tokenid order by t2.display_seq";

	String GET_ITEMOF_INTEREST = "select t1.*,t2.DISPLAY_SEQ as displaySeq from cu_test.text_tokens t1, cu_test.splash_content_seq t2 where t1.text_tokenid=t2.text_tokenid order by t2.display_seq";

	String GET_IMPORTANT_NOTICE_BYID = "select t1.*,t2.DISPLAY_SEQ as displaySeq from cu_test.text_tokens t1, cu_test.splash_content_seq t2 where t1.text_tokenid=t2.text_tokenid and t1.text_tokenid=:noticeid";

	String referInviteFriendQuery = "{call cu_test.spSaveFriendDetails(:friendEmailId,:referredById,:groupId,:schoolId,:userId,:eventName)}";

	String getloginId = "Select cu_users.u_login_id, cu_users_auth.ua_password_hash from cu_test.cu_users,cu_test.cu_users_auth where cu_users.u_id = cu_users_auth.u_id and cu_users.u_login_id=:userid";
	String getLoginBeanByUId = "Select cu_users.u_login_id,cu_users.u_id,cu_users_auth.ua_password_hash,cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token, u_os_type, u_type,up_refferal_code,up_refferal_url,up_picture_profile ,desc_intro,desc_detailed,business_name,cu_users.u_account_id,u_login_type,cu_users.u_date_of_join,ref_membership.membership_type from cu_test.cu_users,cu_test.cu_users_auth,cu_test.cu_users_common_profile,cu_test.cu_account_additional_profile,cu_test.ref_membership where cu_users.u_id = cu_users_auth.u_id and cu_users.u_id=cu_users_common_profile.u_id and cu_users.u_id=:loginId group BY cu_users.u_login_id,cu_users.u_id,cu_users_auth.ua_password_hash,cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token, u_os_type, u_type,up_refferal_code,up_refferal_url,up_picture_profile ,desc_intro,desc_detailed,business_name,cu_users.u_account_id,u_login_type,cu_users.u_date_of_join,ref_membership.membership_type";
	String getLoginBeanByLoginId = " Select cu_users.u_login_id,cu_users.u_id, cu_users_auth.ua_password_hash,cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token, u_os_type, u_type, up_refferal_code,up_refferal_url,up_picture_profile ,desc_intro,desc_detailed,business_name,cu_users.u_account_id ,u_login_type ,cu_users.u_date_of_join,ref_membership.membership_type from cu_test.cu_users,cu_test.cu_users_auth,cu_test.cu_users_common_profile,cu_test.cu_account_additional_profile,cu_test.ref_membership where cu_users.u_id = cu_users_auth.u_id and cu_users.u_id=cu_users_common_profile.u_id and cu_users.u_login_id=:loginId group BY cu_users.u_login_id,cu_users.u_id, cu_users_auth.ua_password_hash,cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,u_device_token, u_os_type, u_type, up_refferal_code,up_refferal_url,up_picture_profile ,desc_intro,desc_detailed,business_name,cu_users.u_account_id ,u_login_type ,cu_users.u_date_of_join,ref_membership.membership_type";

	String isLoginIdExist = "select u_login_id from cu_test.cu_users where u_login_id=:u_login_id";
	String GET_ACCOUNT = "select u_account_id from cu_test.cu_users where u_id=:u_id";
	String SAVE_SUGGETION = "insert into cu_test.cu_suggestion_data	(account_id,latitude,longitude)	values	(:account_id,:latitude,:longitude)";
	String UPDATE_SUGGETION = "update  cu_test.cu_suggestion_data	set latitude=:latitude,longitude=:longitude	where  account_id=:account_id ";
	String getloginthroughsocial = "Select cu_users.u_login_id, cu_users_auth.ua_password_hash from cu_test.cu_users,cu_test.cu_users_auth where cu_users.u_id = cu_users_auth.u_id  and cu_users.u_login_id=:loginId  and cu_users_auth.ua_password_hash=:password";
	String getloginsocial = "select u_id,u_login_id from cu_test.cu_users where u_login_id =:loginId";

	String getUserId = "SELECT @var\\:\\=u_id as UserID from cu_test.cu_users where u_login_id=:loginId";
	String getUId = "Select cu_users.u_id from cu_test.cu_users,cu_test.cu_users_auth where cu_users.u_id = cu_users_auth.u_id and cu_users.u_login_id=:mailid  and cu_users_auth.ua_password_hash=:password";
	String getUserPoint = " Select total_points,purchase_points,referral_points,pp_busi_factor_points,pp_grow_factor_points,cash_points_net,lt_total_points,lt_redeemed_points from cu_test.cu_points where customer_id=:accountId ";
	String GET_USER_POINTS = "Select p_sky_points, p_sky_purchase_points,trans_amount_before_tax_tips,up_pic, p_ltime_cash_points, p_as_of_date from cu_test.cu_points left join cu_test.cu_users on cu_points.u_account_id = cu_users.u_id left outer join cu_test.cu_transaction on cu_transaction.trans_consumer_id = cu_users.u_id LEFT outer JOIN cu_business_users_additional_profile on cu_business_users_additional_profile.up_id=cu_users.u_id where cu_users.u_id =:uId ";

	String updateUserImageQuery = "{call cu_test.sp_suggestion(:loginId,:profileImage)}";

	String GETIMAGE = "Select up_pic from cu_test.cu_business_users_additional_profile left join cu_users on cu_users.u_id=cu_business_users_additional_profile.up_id where cu_users.u_login_id=:loginId";

	String GET_PROFILE = "SELECT c.u_id,c.u_login_id,up_first_name, up_last_name,up_picture_profile,up_address_street1,up_address_street2,cu_state.id,up_address_city,up_address_zip,up_mobile_no,up_date_of_birth,up_user_gender ,u_date_of_join,up_refferal_code,up_refferal_url ,cu_state.name FROM cu_test.cu_users c,cu_test.cu_users_common_profile profile,cu_test.cu_state where c.u_id=profile.u_id AND cu_state.id= profile.up_address_state and c.u_id=:u_id";
	String SP_CREATE_FUNDING_REQUEST = "{call cu_test.sp_create_funding_request(:businessId,:frnumber,:frrequesteddollaramt)}";
	String SP_FUNDING_ADDEDD = "{call cu_test.sp_add_funding (:businessId,:frnumber,:frfundeddollaramt)}";
	String SP_TRANSATION_FUNDED = "{call cu_test.sp_on_funded(:businessId,:funding_account_Id,:transactionId,:amount)}";
	String sp_updateUserImageQuery = "{call cu_test.sp_suggestion(:loginId,:profileImage)}";
	String SP_TRANSACTION_ENTRY = "{call cu_test.sp_transactions (:con_account_id,:trans_business_id,:actual_date,:receipt_no,:trans_amount,:trans_receipt_url,:comment,:rating,:transaction_no,@out_value)}";
	String sp_user_registration = "{call cu_test.sp_userRegistration(:loginId,:loginType,:userType,:password,:encPassword,:firstName,:lastName,:devicetoken,:osType,:longitude,:latitude)}";
	String sp_business_registration = "{call cu_test.sp_business_userRegistration(:loginId,:loginType,:userType,:password,:firstName,:lastName,:osType,:businessName,:accountReferredId)}";
	String sp_UPDATE_LOGIN_TYPE = "{call cu_test.sp_login(:loginId,:loginType,:deviceToken,:osType)}";
	String sp_update_funding_request = "{call cu_test.sp_update_funding_request(:businessId,:frnumber,:frfundeddollaramt)}";
	String UPDATE_USER_PROFILE = "UPDATE cu_test.cu_users_common_profile SET up_first_name =:firstname,up_last_name =:lastname,up_address =:address WHERE u_id=:uId";
	String UPDATE_USER_PROFILE_IMAGE = "UPDATE cu_test.cu_users_common_profile SET up_first_name =:firstname,up_last_name =:lastname,up_address =:address, up_picture_profile =:profileImage WHERE u_id=:uId";
	String updateUserProfileImage = "UPDATE cu_test.cu_users_common_profile SET up_picture_profile =:profileImage WHERE u_id =:u_id";

	String GET_SUGGESTIONDETAILS = " SELECT account_id, business_hours, phone1, business_pic1_url, business_pic2_url, business_pic3_url,desc_intro, desc_detailed, business_name, business_address_street1, business_address_street2,business_address_city, business_address_state, business_address_zip,business_geo_longitude, business_geo_latitude FROM cu_test.cu_account_additional_profile left join cu_test.cu_users on cu_account_additional_profile.account_id = cu_users.u_id where cu_users.u_id = 245 ";

	String GET_SUGGESTION = "SELECT cu_account_additional_profile.account_id, business_name, rating_level as rating, business_geo_latitude as latitude, business_geo_longitude as longitude, business_address_street1, business_address_street2,business_address_city, name as state, business_address_zip, phone1 as mobile, business_hours as opening_time, desc_detailed as long_desc, desc_intro as short_desc, business_pic1_url as img1, business_pic2_url as img2, business_pic3_url as img3, cu_account_additional_profile.suggestion_id,cu_account_additional_profile.discount_info,cu_account_additional_profile.points_offered FROM cu_test.cu_account_additional_profile Left join cu_test.cu_state on cu_state.id = cu_account_additional_profile.business_address_state Left join cu_test.cu_account_rating_summary on business_account_id = cu_account_additional_profile.account_id";

	String GET_UPLOAD_TRANACTIONS = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id , cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,  cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_name,trans_amount_before_tax_tips,is_refund_requested,refund_request_reason,is_refund, is_dispute_requested,dispute_request_reason ,concat(cu_account_additional_profile.business_address_street1,' ',business_address_street2,' ',cu_state.name,' ',business_address_zip)as address FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction , cu_test.ref_transaction_status ,cu_test.cu_account_additional_profile ,cu_test.cu_state  where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and cu_transaction.is_refund_requested =0 and   cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and cu_state.id=cu_account_additional_profile.business_address_state and cu_transaction.trans_consumer_id=:consumer_id  ";
	String GET_UPLOAD_TRANACTION = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id , cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,  cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_name,trans_amount_before_tax_tips,is_refund_requested,refund_request_reason,is_refund, is_dispute_requested,dispute_request_reason ,concat(cu_account_additional_profile.business_address_street1,' ',business_address_street2,' ',cu_state.name,' ',business_address_zip)as address FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction ,cu_test.ref_transaction_status ,cu_test.cu_account_additional_profile ,cu_test.cu_state where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and cu_transaction.is_refund_requested =0 and   cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and cu_state.id=cu_account_additional_profile.business_address_state and cu_transaction.trans_consumer_id=:consumer_id and  cu_transaction.cu_transaction_id=:transaction_id ";
	String forgotPasswordQuery = "Select cu_users.u_login_id,cu_users_common_profile.up_first_name,cu_users_common_profile.up_last_name,cu_users_auth.ua_password_hash ,u_type ,cu_users.u_id from cu_test.cu_users,cu_test.cu_users_auth,cu_test.cu_users_common_profile where cu_users.u_id = cu_users_auth.u_id and cu_users.u_id=cu_users_common_profile.u_id and cu_users.u_login_id=:loginId";
	String updateUserSession = "UPDATE cu_test.cu_users_auth SET ua_user_sesion =:usersession WHERE u_id =:u_id";

	String userSession = "select ua_user_sesion from  cu_test.cu_users_auth WHERE u_id =:u_id";

	String INSERT_REFFERAL_CODE = "UPDATE cu_test.cu_users_common_profile SET up_refferal_code=:up_refferal_code ,up_refferal_url=:up_refferal_url  WHERE u_id =:u_id";

	String UPDATE_USER_PROFILE_ = "UPDATE cu_test.cu_users_common_profile SET up_first_name =:firstname,up_last_name =:lastname,up_address_street1 =:up_address_street1,up_address_street2 =:up_address_street2,up_address_city=:up_address_city,up_address_state=:up_address_state,up_address_zip=:up_address_zip,up_mobile_no=:up_mobile_no,up_date_of_birth=:up_date_of_birth,up_user_gender=:up_gender  WHERE u_id=:uId";
	String UPDATE_USER_PROFILE_IMAGE_ = "UPDATE cu_test.cu_users_common_profile SET up_first_name =:firstname,up_last_name =:lastname,up_address_street1 =:up_address_street1,up_address_street2 =:up_address_street2,up_address_city=:up_address_city,up_address_state=:up_address_state,up_address_zip=:up_address_zip,up_mobile_no=:up_mobile_no,up_date_of_birth="
			+ "':up_date_of_birth'" + ",up_user_gender=:up_gender, up_picture_profile =:profileImage WHERE u_id=:uId";

	String GET_SALE_TRANSACTION = "select cu_transaction.trans_consumer_id, CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName , cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment ,cu_transaction.is_dispute_requested,cu_transaction.dispute_request_reason from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_test.cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_test.cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and is_dispute_requested="
			+ TransactionConstant.TRANSACTION_NOT_DISPUTE
			+ " and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id  order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";
	String GET_SALE_TRANSACTION_BY_ID = "select cu_transaction.trans_consumer_id , CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName,cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment , cu_transaction.is_dispute_requested,cu_transaction.dispute_request_reason from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_test.cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_test.cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and is_dispute_requested="
			+ TransactionConstant.TRANSACTION_NOT_DISPUTE
			+ " and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id and cu_transaction.cu_transaction_id=:cu_transaction_id order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";

	String isRefferalCode = "select u_id from cu_test.cu_users_common_profile where up_refferal_code=:refferalCode";
	String getMyReferrals = " select  cu_users.u_login_id,concat(cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as referralName,cu_users_common_profile.up_picture_profile , cu_users.u_type ,referred_date from cu_test.cu_users ,cu_test.account_referral ,cu_test.cu_users_common_profile where  cu_users.u_account_id = account_referral.account_id and cu_users_common_profile.u_id=cu_users.u_id and account_referral.referred_by_account_id IN(select cu_users.u_account_id from cu_test.cu_users where  cu_users.u_id =:u_id) ";
	String GET_ACCOUNT_REFERRAL_CODE = "select u_account_id from cu_test.cu_users,cu_test.cu_users_common_profile where cu_users.u_id=cu_users_common_profile.u_id and up_refferal_code=:refferalCode";
	String SAVE_ACCOUNT_REFERRAL = "insert into cu_test.account_referral (referred_by_account_id, account_id) values (:referred_by_account_id,:referred_to_account_id)";
	String SP_FUNDINGREQUEST_ENTRY = "{call cu_test.sp_funding_request (:loginId,:frnumber,:frrequesteddollaramt,:frfundeddollaramt,:frstatus)}";
	String GET_FUNDINGREQUEST_DETAILS = " SELECT fr_number, fr_requested_dollar_amount, fr_funded_dollar_amount, fr_status_name, fr_created_date_time, fr_funded_date_time,fr_approved_by, fr_status_updated_by, fr_status_updated_at FROM  cu_test.business_funding_request Left join cu_test.ref_fr_status on ref_fr_status.fr_status_id = business_funding_request.fr_status where business_funding_request.fr_status=:status and u_id=:uId";
	String GET_FUNDING_DEPOSITE = " SELECT fr_number, fr_requested_dollar_amount, fr_funded_dollar_amount, fr_status_name, fr_created_date_time, fr_funded_date_time,fr_approved_by, fr_status_updated_by, fr_status_updated_at FROM  cu_test.business_funding_request Left join cu_test.ref_fr_status on ref_fr_status.fr_status_id = business_funding_request.fr_status where business_funding_request.fr_status=4 and u_id=:uId";
	String GET_ALL_COUNT_DETAILS = " SELECT fr_number, fr_requested_dollar_amount, fr_funded_dollar_amount, fr_status_name, fr_created_date_time, fr_funded_date_time,fr_approved_by, fr_status_updated_by, fr_status_updated_at FROM  cu_test.business_funding_request Left join cu_test.ref_fr_status on ref_fr_status.fr_status_id = business_funding_request.fr_status where u_id=:uId";
	String GET_REFERRAL_MSG = " SELECT message_id,message_key,long_message,short_message,cu_ad_pic_url from cu_test.ref_referral_message ";
	String GET_UI_CONFIG = " SELECT * from cu_test.cloudunion_config";
	String UPDATE_BUSINESS_PROFILE = "update cu_test.cu_account_additional_profile set ein=:ein,business_contact_people_first_name=:businessContactPeopleFirstName,business_contact_people_last_name=:businessContactPeopleLastName,store_contact_email=:storeContactEmail,business_hours=:businessHours,special_hours=:specialHours,phone1=:phone1,phone2=:phone2,business_pic1_url=:businessPic1Url,business_pic2_url=:businessPic2Url,business_pic3_url=:businessPic3Url,desc_intro=:shortDesc,desc_detailed=:longDesc,type_of_business=:typeOfBusiness,business_name=:businessName,business_address_street1=:businessAddressStreet1,business_address_street2=:businessAddressStreet2,business_address_city=:businessAddressCity,business_address_state=:businessAddressState,business_address_zip=:accobusinessAddressZip,business_special_address_instruction=:businessSpecialAddressInstruction,business_geo_longitude=:businessGeolongitude,business_geo_latitude=:businessGeolatitude,points_offered=:pointsOffered,discount_info=:discountInfo,specialties=:specialties,url_for_menu=:urlForMenu,url_for_website=:urlForWebsite, suggestion_id=:suggestionId ,is_geocoded=1 where account_id=:accountId";
	String GET_TRANACTIONFORADMIN = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id,cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_name,trans_amount_before_tax_tips,up_first_name,up_last_name FROM cu_transaction_ratings ,cu_transaction ,ref_transaction_status ,cu_account_additional_profile, cu_users_common_profile where  cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and ref_transaction_status.trans_status_id=1 group by cu_transaction.cu_transaction_id order by cu_transaction.cu_transaction_id";
	String GET_USER_TYPE = "select ref_user_type.user_type_name from cu_test.ref_user_type,cu_test.cu_users where ref_user_type.user_type_id=cu_users.u_type and cu_users.u_id=:u_id";
	String TOTAL_PURCHASE_TRANSACTION_COUNT = "SELECT count(*) FROM cu_test.cu_transaction where trans_consumer_id=:uId";
	String TOTAL_SALE_TRANSACTION_COUNT = "SELECT count(*) FROM cu_test.cu_transaction where trans_business_id=:uId ";
	String TOTAL_REFUNDREQUEST_COUNT = "select count(*) from cu_test.cu_transaction where is_refund_requested=1 and trans_business_id=:accountId ";
	String TOTAL_NOTIFICATION_COUNT = "SELECT count(*) FROM cu_test.cu_notification where notification_account_id=:accountId ";
	String TRANSACTION_PENDING_POINT_COUNT = "SELECT count(*) FROM cu_test.cu_transaction where trans_status_id = 1 and trans_consumer_id=:uId ";
	String TRANSACTION_APPROVE_POINT_COUNT = "SELECT count(*) FROM cu_test.cu_transaction where trans_status_id = 2 and trans_consumer_id=:uId ";
	String TRANSACTION_DENIED_POINT_COUNT = "SELECT count(*) FROM cu_test.cu_transaction where trans_status_id = 3 and trans_consumer_id=:uId ";
	String TOTAL_CLOUDUNION_POINT_COUNT = "SELECT count(*) FROM cu_test.cu_points where customer_id=:accountId";
	String FUNDING_REQUEST_COUNT = "SELECT count(*) FROM cu_test.business_funding_request where u_id=:uId";
	String ADMIIN_GET_TRANACTION = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id , cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,  cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_id,trans_amount_before_tax_tips FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction ,cu_test.ref_transaction_status ,cu_test.cu_account_additional_profile where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and   cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and  cu_transaction.cu_transaction_id=:transaction_id";
	String ADMIIN_UPDATE_TRANACTION = "update cu_test.cu_transaction , cu_transaction_ratings set cu_transaction.trans_status_id=:status_id,cu_transaction_ratings.comment=:comment where cu_transaction.cu_transaction_id=cu_transaction_ratings.transaction_id and cu_transaction.cu_transaction_id=:transactionId";
	String LOAD_SUGGESTION = "select suggestion_id,account_id,CONCAT(type_of_business,' ',business_name,' ',business_address_street1,' ',business_address_street2,'  ',business_address_city,' ',business_address_state,' ',business_address_zip,' ',business_special_address_instruction,' ',points_offered,' ',discount_info,' ',specialties,' ',description_in_star) as address,business_geo_longitude, business_geo_latitude  from cu_test.cu_account_additional_profile";
	String POST_REFUND_REQUEST = "update cu_test.cu_transaction set is_refund_requested=1,refund_request_reason=:refundComment where cu_transaction_id=:transaction_id";
	String GET_REFUND_REQUEST = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id , cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,  cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_name,trans_amount_before_tax_tips ,is_refund_requested,refund_request_reason,is_refund FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction ,cu_test.ref_transaction_status ,cu_test.cu_account_additional_profile  where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and cu_transaction.is_refund_requested =1 and   cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and cu_transaction.trans_consumer_id=:consumer_id  ";
	String GET_REFUND_REQUEST_BY_ID = "SELECT cu_transaction.cu_transaction_id, cu_transaction.trans_consumer_id , cu_account_additional_profile.business_name, trans_actual_date, trans_posted_date,receipt_no,trans_amount_grand_total,trans_receipt_url,  cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,ref_transaction_status.trans_status_name,trans_amount_before_tax_tips ,is_refund_requested,refund_request_reason,is_refund FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction ,cu_test.ref_transaction_status ,cu_test.cu_account_additional_profile where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id and cu_transaction.is_refund_requested =1 and   cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_account_additional_profile.account_id=cu_transaction.trans_business_id and cu_transaction.trans_consumer_id=:consumer_id and  cu_transaction.cu_transaction_id=:transaction_id ";
	String ACCEPT_REFUND_REQUEST = "update cu_test.cu_transaction set is_refund_requested=2 where cu_transaction_id=:transaction_id";
	String GET_REFUND_SALE_TRANSACTION = "select cu_transaction.trans_consumer_id, CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName , cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_transaction.is_refund_requested =1 and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id  order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";
	String GET_REFUNDING_SALE_TRANSACTION_BY_ID = "select cu_transaction.trans_consumer_id , CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName,cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and cu_transaction.is_refund_requested =1 and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id and cu_transaction.cu_transaction_id=:cu_transaction_id order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";
	String GET_BUSINESS_PROFILE = "select * from cu_test.cu_account_additional_profile,cu_test.cu_account_rating_summary where cu_account_additional_profile.account_id=cu_account_rating_summary.business_account_id and cu_account_additional_profile.account_id=:account_id";
	String GET_STATES = "select cu_state.id,cu_state.name,cu_state.abbreviation,cu_state.country from cu_test.cu_state";
	String POST_DISPUTE_TRANSACTION = "update cu_test.cu_transaction set is_dispute_requested=1,refund_request_reason=:refundComment where cu_transaction_id=:transaction_id";

	String GET_DISPUTE_TRANSACTION = "select cu_transaction.trans_consumer_id, CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName , cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment ,cu_transaction.is_dispute_requested,cu_transaction.dispute_request_reason from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and is_dispute_requested="
			+ TransactionConstant.TRANSACTION_DISPUTE
			+ " and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id  order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";
	String GET_DISPUTE_TRANSACTION_BY_ID = "select cu_transaction.trans_consumer_id , CONCAT( cu_users_common_profile.up_first_name,'  ',cu_users_common_profile.up_last_name) as consumerName,cu_transaction.cu_transaction_id,cu_transaction.trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,trans_status_name,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_receipt_url,cu_transaction.receipt_no ,cu_transaction_ratings.rating_level,cu_transaction_ratings.comment ,cu_transaction.is_dispute_requested,cu_transaction.dispute_request_reason from cu_test.cu_transaction  Left join cu_test.ref_transaction_status on ref_transaction_status.trans_status_id = cu_transaction.trans_status_id  Left join cu_users_common_profile on cu_users_common_profile.u_id = cu_transaction.trans_consumer_id Left join cu_transaction_ratings on cu_transaction_ratings.transaction_id =  cu_transaction.cu_transaction_id where cu_transaction.trans_status_id = ref_transaction_status.trans_status_id and is_dispute_requested="
			+ TransactionConstant.TRANSACTION_DISPUTE
			+ " and cu_transaction.cu_transaction_id = cu_transaction_ratings.transaction_id "
			+ "and trans_business_id =:trans_business_id and cu_transaction.cu_transaction_id=:cu_transaction_id order by cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,trans_consumer_id ";
	String GET_BUSINESS_TYPE = "select business_type_id,business_type_name from cu_test.ref_business_type ";
	String GET_BUSINESS_ACCOUNT_SUMMARY = "select business_account_id,rating_level,rating_level_count,1star_count,2star_count,3star_count,4star_count,5star_count	from cu_test.cu_account_rating_summary where business_account_id=:businessAcountId";
	String UPDATE_PASSWORD = " update cu_test.cu_users_auth,cu_users set ua_password_hash=:password where cu_users.u_id=cu_users_auth.u_id and cu_users.u_login_id=:mailid ";

	String REFUND_REQUEST_COUNT = " SELECT count(*) FROM cu_test.cu_transaction where trans_consumer_id=:uId ";
	String GET_SUGGETION_ID = "select suggestionId from  cu_test.cu_suggestion_data where account_id=:account_id";
	String GET_BUSINESS_ID = "select  account_id from  cu_test.cu_account_additional_profile where account_id=:account_id";
	String LOAD_B_SUGGSTION = "SELECT cu_account_additional_profile.account_id, business_name, rating_level as rating,business_geo_latitude as latitude, business_geo_longitude as longitude, business_address_street1, business_address_street2,business_address_city, cu_state.name as state, business_address_zip, phone1 as mobile, business_hours as opening_time, desc_detailed as long_desc,  desc_intro as short_desc, business_pic1_url as img1, business_pic2_url as img2, business_pic3_url as img3, cu_account_additional_profile.suggestion_id, cu_account_additional_profile.discount_info,cu_account_additional_profile.points_offered,special_hours,business_type_name,url_for_menu,url_for_website,specialties FROM cu_test.cu_account_additional_profile,cu_test.cu_account_rating_summary ,cu_test.cu_state,cu_test.ref_business_type where cu_account_rating_summary.business_account_id=cu_account_additional_profile.account_id and cu_state.id = cu_account_additional_profile.business_address_state and cu_account_additional_profile.type_of_business=ref_business_type.business_type_id ";
	String GET_NOTIFICATION_TYPE = " SELECT notification_type_id,notification_type_name,notification_type_message FROM cu_test.ref_notification_type where notification_type_name=:notificationType ";
	String SAVE_NOTIFICATION = "insert into cu_test.cu_notification(notification_type,notification_account_id,counter_party_account_id,related_item,notification_text,is_cleared,is_accessed)values (:notificationType,:notificationAccountId,:counterPpartyAccountId,:relatedItem,:notificationText,0,0)";
	String GET_MY_NOTIFICATION = "select notification_type_name,related_item,notification_text,is_cleared,created_date,is_accessed,notification_id from cu_test.cu_notification,cu_test.ref_notification_type where cu_notification.notification_type=ref_notification_type.notification_type_id and is_cleared =0 and cu_notification.notification_account_id=:userAccountId";
	String CLEAR_NOTIFICATION = "update cu_test.cu_notification set is_cleared=1 where cu_notification.notification_account_id=:userAccountId and notification_id in(:notificationId)";
	String GET_ALL_UPLOADED_TRANSACTION = "SELECT cu_transaction.cu_transaction_id,cu_transaction.trans_consumer_id,trans_business_id,cu_transaction.trans_actual_date,cu_transaction.trans_posted_date,cu_transaction.trans_type,cu_transaction.trans_status_id,cu_transaction.trans_funding_status_id,cu_transaction.trans_amount_before_tax_tips,cu_transaction.trans_amount_grand_total,cu_transaction.trans_day_bus_discount,cu_transaction.trans_day_bus_referral,cu_transaction.trans_day_bus_referral_membership,cu_transaction.trans_day_con_referral,cu_transaction.trans_day_con_referral_membership,cu_transaction.trans_receipt_url,cu_transaction.is_refund_requested,cu_transaction.refund_request_reason,cu_transaction.is_refund,cu_transaction.orig_trans_id,cu_transaction.cash_points,cu_transaction.redeem_points,cu_transaction.receipt_no,cu_transaction.is_dispute_requested,cu_transaction.dispute_request_reason,cu_transaction.is_rated,cu_transaction_ratings.rating_level ,cu_transaction.purchase_points,cu_transaction.reject_reason, concat(cu_account_additional_profile.business_address_street1,',',business_address_street2,',',cu_state.name,',',business_address_zip)as address,cu_transaction_ratings.comment FROM cu_test.cu_transaction_ratings ,cu_test.cu_transaction ,cu_test.cu_account_additional_profile,cu_test.cu_state where cu_transaction.cu_transaction_id =cu_transaction_ratings.transaction_id AND cu_account_additional_profile.account_id =cu_transaction.trans_business_id and cu_transaction.trans_consumer_id =cu_transaction_ratings.consumer_account_id and cu_state.id=cu_account_additional_profile.business_address_state ";
	String GET_MEMBERSHIP = " SELECT membership_id,membership_type,membership_name,membership_for,membership_cash_conv_percentage,membership_managementfee,membership_consumer_ref_percentage,membership_business_ref_percentage,membership_monthly_fee,membership_details_effective_date,membership_details_end_date FROM cu_test.ref_membership where membership_type=:membershipType ";
	String GET_FUND_ACCOUNT = "Select * from cu_test.business_funding_account where u_funding_account_id =:p_acount_id";

	String ADD_FUND = "Insert into business_funding_account(u_funding_account_id,ba_business_balance_in_points) values(:p_acount_id,:fundAmount)";
	String GET_BUSINESS_DISCOUNT = "select business_discounts.discount_id from cu_test.business_discounts  where  business_discounts.business_id=:business_id and  business_discounts.status=:status and business_discounts.discount_rate=:discount_rate and business_discounts.discount_start_date=:discount_start_date and business_discounts.discount_end_date=:discount_end_date";
	String UPDATE_BUSINESS_DISCOUNT_WITH_END_DATE = "insert into cu_test.business_discounts (business_id,status,discount_rate,discount_start_date,discount_end_date)values(:business_id,1,:discount_rate,:discount_start_date,:discount_end_date)";
	String UPDATE_BUSINESS_DISCOUNT_WITHOUT_END_DATE = "insert into cu_test.business_discounts (business_id,status,discount_rate,discount_start_date)values(:business_id,1,:discount_rate,:discount_start_date)";
	String INACTIVE_BUSINESS_DISCOUNT = "update cu_test.business_discounts set status=0 where business_id =:business_id";
	String UPDATE_FUND = "update cu_test.business_funding_account set ba_business_balance_in_points=:fundAmount where u_funding_account_id =:p_acount_id";

	String GET_MEMBERSHIPTYPE = "select * from cu_test.ref_membership where membership_type like :membershiptype ";
	String GET_CONSUMER_MAME = "select concat(up_first_name,' ',up_last_name) as consumerName from cu_test.cu_users_common_profile,cu_test.cu_users where cu_users.u_id=cu_users_common_profile.u_id and cu_users.u_account_id=:consumerId";
	String GET_BUSINESS_NAME = "select  business_name from cu_test.cu_account_additional_profile where account_id=:businessId";
	String GET_FAQ = "SELECT faq_questions,faq_answers ,faq_id  FROM cu_test.cu_faq";

	String GET_BUSINESS_DISCOUNTS = "SELECT discount_id,business_id,status,discount_rate,discount_start_date,discount_end_date FROM cu_test.business_discounts where business_id=:businessId";
	String GET_MYACCOUNT_DETAIL = "select bat_name as transactionType,bat_points as previousPoint,bat_balance_in_points_before as previousBalance,bat_balance_in_points_after as afterBalance,bat_as_of_date,bat_updated ,bat_transaction_num,bat_consumer_id from cu_test.ref_business_account_transaction_type,cu_test.business_funding_account_transaction where ref_business_account_transaction_type.bat_id=business_funding_account_transaction.bat_type and business_funding_account_transaction.u_funding_id=:accountId";
	String GET_BUSINESS_REVIEW = "select cu_transaction_ratings.comment,cu_transaction_ratings.rating_level,cu_transaction_ratings.date_of_rating ,cu_transaction_ratings.consumer_account_id from cu_test.cu_transaction_ratings where cu_transaction_ratings.business_account_id=:accountId";
	String CHECK_BUSINESS_DISCOUNT = "select count(*) from cu_test.business_discounts where business_discounts.status=1 and   business_discounts.business_id=:business_id and business_discounts.discount_start_date=getDate() ";
	String SP_ADD_BUSINESS_FUND = "";

	// Cinch SQL

	String SIGNUP = "{call adinovis.saveuserprofile(:Pfname,:Plname,:Pbsname,:Pemailadd,:Ppwd,:Paddr,:Pbphonenumber,:Pcphonenumber,:plicno)}";

	String token = "{call adinovis.savetokenauth(:Puseraccountid,:Ptokenvalue)}";

	String CLIENTTOKEN = "{call adinovis.saveclienttokenauth(:Puseraccountid,:Ptokenvalue)}";

	String UPDATEUSERPROFILE = "{call adinovis.updateuserprofile(:Puseraccountid,:Pfirstname,:Plastname,:Pcphonenumber,:Pbphonenumber,:Pmemberlicenseno,:Pbusinessname,:Pemailaddress,:Paddress,:Pprofilepict)}";

	String USERPROFILE = "{call adinovis.getuserprofile(:emailid,:password)}";

	String USERPROFILEUPDATE = "{call adinovis.getuser(:Puseraccountid)}";

	String SAVECLIENT = "{call adinovis.saveclientprofile(:Pbsname,:Pcontactperson,:Pbphonenumber,:Pcphonenumber,:Pemailadd,:Ppwd,:Pjurisdictionid,:Ptypeofentityid,:Pincorporationdate,:Paccountbook,:Paccesskey,:Psecretkey,:Ploginid)}";

	String UPDATECLIENT = "{call adinovis.updateclientprofile(:Pclientfirmid,:Pbsname,:Pcontactperson,:Pbphonenumber,:Pcphonenumber,:Pemailadd,:Pjurisdictionid,:Ptypeofentityid,:Pincorporationdate,:Paccountbook,:Paccesskey,:Psecretkey,:Ploginid)}";

	String RESETEMAILVALIDATION = "{call adinovis.resetemailvalidation(:emailid)}";

	String CLIENTVALIDATIONEMAIL = "{call adinovis.clientvalidation(:emailid)}";

	String FORGOTPASSWORD = "{call adinovis.forgotpassword(:emailid)}";

	String SIGNUPVALIDATION = "{call adinovis.signupvalidation(:emailid)}";

	String CLIENTVALIDATION = "{call adinovis.clientvalidation(:emailid)}";

	String CHANGEPASSWORD = "{call adinovis.changeuserpassword(:Puseraccountid,:oldpassword,:newpassword)}";

	String RESETPASSWORD = "{call adinovis.resetpassword(:tokenkey,:newpassword)}";

	String GETCLIENTPROFILE = "{call adinovis.getclientprofile(:Ploginid)}";

	String GETJURISDICTION = "{call adinovis.getjurisdiction()}";

	String GETTYPEOFENTITY = "{call adinovis.gettypeofentity()}";

	String GETALLAUDITOR = "{call adinovis.getallAuditor()}";

	String GETENGAGEMENT = "{call adinovis.getengagement()}";

	String GETENGAGEMENTSTATUS = "{call adinovis.getengagementstatus()}";
	
	String GETENGAGEMENTCOUNTDETAIL = "{call adinovis.getengagementcountdetail(:Ploginid)}";

	String UPDATEENGAGAEMENT = "{call adinovis.updateengagement(:Pengagementsid,:Pengagementname,:Pclientfirmid,:Pengagementtypeid,:Pcompliation,:Psubenties,:Pyearend,:Padditionalinfo,:Puseraccoutid_roleid_list,:Ploginid)}";
	
	String GETCLIENTCOUNTDETAILS = "{call adinovis.getclientcountdetails(:Ploginid)}";

	String GETCLIENTSTATUS = "{call adinovis.getclientstatus()}";

	String GETBUSINESSDETAILS = "{call adinovis.getbusinessdetails(:Ploginid)}";

	String GETCLIENTDETAILS = "{call adinovis.getclientdetail(:Ptokenvalue)}";

	String GETAUDITORROLE = "{call adinovis.getauditorrole()}";

	String GETEMAILID = "{call adinovis.emailvalidation(:emailid)}";
	
	String GET_USER_VERIFICATION="{call adinovis.userverification(:emailid)}";

	String AUTHVALIDATION = "{call adinovis.authvalidation(:emailid)}";

	String LOGINVALIDATION = "{call adinovis.loginvalidation(:tokenkey)}";

	String CLIENTVALIDATIONSTATUS = "{call adinovis.clientinvitevalidation(:tokenkey,:Pstatusid)}";

	String GETUSERID = "{call adinovis.getuserid(:emailid)}";

	String GETCLIENTID = "{call adinovis.getclientid(:emailid,:Ppwd)}";

	String DELETECLIENTPROFILE = "{call adinovis.deleteclientprofile(:clientfirmid,:Ploginid)}";

	String DELETEENGAGEMENT = "{call adinovis.deleteengagement(:Pengagementsid,:Ploginid)}";

	String SAVEENGAGEMENT = "{call adinovis.saveengaement(:Pclientfirmid,:Pengagementname,:Pengagementtypeid,:Pcompliation,:Psubenties,:Pyearend,:Padditionalinfo,:Puseraccoutid_roleid_list,:Ploginid)}";

	String GETENGAGEMENTDETAIL = "{call adinovis.getengagementdetails(:clientfirmid,:Ploginid)}";
	
	String GET_ENGAGEMENT_YEAR="{call adinovis.getengagementyear(:inengagementsid)}";
	
	String GET_ENGAGEMENT_ID="{call adinovis.getengagementsid(:Pclientfirmid)}";

	String GETALLENGAGEMENT = "{call getallengagement(:Ploginid)}";

	String GET_CLIENT_DATA = "{call adinovis.getclientdata()}";

	String UPDATE_CLIENT_TOKENS = "{call adinovis.updateclientdata(:clientFirmId, :accessToken, :refreshToken)}";

	String SAVE_CLIENT_DATA = "{call adinovis.saveclientdata(:engagementId, :receivedData, :responseStatus, :accountList)}";

	String GET_CLIENT_DATASOURCE = "{call adinovis.getclientdatasource(:clientId)}";

	String SAVE_CLIENT_DATASOURCE = "{call adinovis.saveclientdatasource(:tokenvalue, :sourcelink, :realmid, :refreshtoken, :accesstoken)}";

	String GETTRAILBALANCE = "{call adinovis.gettrailbalance(:Pengagementid,:inyear)}";

	String GETADJUSTMENTTYPE = "{call adinovis.getadjustmenttype()}";
	
	String DELETETRAILBALANCEROW = "{call adinovis.deletetrailbalancerow(:intrailbalid,:ploginid)}";
	
	String SAVE_TRAILBALANCE_ADJUSTMENT="{call adinovis.savetrailbalanceadjustmententry(:intrailbalanceid,:intrailadjustmentid,:inadjustmenttypeid,:inaccountcode,:inaccountname,:inacctcredit,:inacctdebit,:incomment,:incomment1,:inisdelete,:ploginid)}";
	
	String SAVE_DUPLICATE_TRAILBALANCE="{call adinovis.duplicateORaddTB(:intrailbalanceid,:inengagementsid,:inacctyear,:inaccountcode,:inaccountname,:inoriginalbalance,:inPY1,:inPY2,:inmapsheetid,:ploginid)}";
	
	String SAVE_TRAILBALANCE_MAPPING="{call adinovis.savetrailbalancemapping(:ptrailbalanceid,:pmapsheetid,:ploginid)}";
	
	String SAVE_FS_DETAILS="{call adinovis.savefsnamechange(:inengagementsid,:inacctyear,:infstype,:infsid,:incurrentname,:innewname,:inploginid)}";
			
	String GET_MAP_DETAILS="{call adinovis.getmapsheetDetails()}";
	
	String GET_ALL_TRAIL_ADJUSTMENT = "{call adinovis.getalltrailadjustment(:inengagementsid)}";
	
	String GET_MAPPING_STATUS="{call adinovis.mappingstatus()}";
	
	String GET_BALANCE_STATEMENT="{call adinovis.getbalancestatement(:inengagementid,:inyear)}";
	
	String GET_BALANCE_SHEET="{call adinovis.getbalancesheet(:inengagementid,:inyear)}";
	
	String GET_INCOME_STATEMENT="{call adinovis.getincomestatement(:inengagementid,:inyear)}";
	String GET_INCOME_STATEMENT_TEST="{call adinovis.getincomestatement_test(:inengagementid,:inyear)}";
	String LOAD_JSON_INTO_TRAILBALANCE = "{call adinovis.loadjsonintotrailbalance()}";
	
	String GET_QUESTION_ANSWER="{call adinovis.getquestionanswer(:inquestiontypeid,:inengagementsid)}";
	String GET_SOURCELINK = "{call adinovis.getsourcelink(:engagementId)};";
	String EXTRACT_ENG_TB_JSON_TABLE = "{call adinovis.extractEngTBjsontotable(:engagementId)};";
	String EXTRACT_ENG_AT_JSON_TABLE = "{call adinovis.extractEngATjsontotable(:engagementId)};";
	String UPDATE_ACCOUNT_LIST = "{call adinovis.updateaccountlist(:engagementId)};";
	String LOAD_TRAIL_BALANCE_MAPPING = "{call adinovis.loadtrailbalancemappingupdate(:engagementId)};";
	String LOAD_QUICKBOOK_DATA_PROCESS = "{call adinovis.loadquickbookdataprocess(:engagementId)};";
}