package com.adinovis.common;

public class ImageConstant {
	public static final String IMAGE_BASE_PATH_SECURITY = "v1/security/";
	public static final String IMAGE_BASE_PATH = "v1/";
	public static final String UPLOAD_FOLDER = "cloudunion-uploads/";
	public static final String TRANSACTION_IMAGE_LOCATON = UPLOAD_FOLDER + "uploads/transaction/";
	public static final String BUSINESS_PROFILE_IMAGE_LOCATON = UPLOAD_FOLDER + "uploads/business/";
	public static final String PROFILE_IMAGE_LOCATON = UPLOAD_FOLDER + "uploads/profile_images/";
	public static final String QR_CODE_IMAGE_LOCATON = UPLOAD_FOLDER + "uploads/qrCode/";
	public static final String SUGGESSTION_IMAGE_LOCATON = UPLOAD_FOLDER + "uploads/suggestions/";
	public static final String GET_IMAGE_URL = "image/";

	public static final String PROFILE = "profile";
	public static final String BUSINESS_PROFILE = "BusinessProfile";
	public static final String REFERRAL = "referral";
	public static final String SUGESTION = "suggestion";

	public static final String GET_USER_IMAGE = "/userimage/{imageName:.+}";
}
