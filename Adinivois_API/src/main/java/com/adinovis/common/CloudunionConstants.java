/**
 * Program Name: CloudunionConstants
 * 
 * Program Description / functionality:This interface is to declare constant values required in the application
 * 
 * Modules Impacted:
 * 
 * Developer    Created             /Modified Date       Purpose
 ********************************************************************************
 * Gouri           10/03/2015 
 *
 */

package com.adinovis.common;


public interface CloudunionConstants {

  int ZERO = 0;
  String UPDATE_FAIL = "Update failed";
  String NO_RECORDS = "No records found";
  String FILE_NO_FOUND = "File Not found";
  String HIBERNATE_EXCEPTION = "Database Exception";
  String VALID_INPUT = "Please provide valid input for";
  String SUCCESS = "Success";
  String FAILURE = "Failure";
  String BALANCESHHET = "No balancesheet map";
  String LOGINSUCCESS = "Login Success";
  String LOGINFAILURE = "password incorrect";
  String TRUE="True";
  String FALSE="False";
  String UPDATE_SUCCESS = "Updated Successfully";
  String UPDATE_FAILURE = "Updation Failed";
  String PROCESS_STEP_NOT_AVAILABLE="Process Steps Not available";
  String FACEBOOK="facebook user";
  String GOOGLE="Google user";
  String NORMAL="Normal user";
  
  String APPROVE="Its approved";
  String ACTIVE="active";
  String INACTIVE ="In-Active";

  String INSERT_SUCCESS = "Inserted Successfully";
  String INSERT_FAILURE = "Inserted Failed";
  String DELETE_SUCCESS = "Deleted Successfully";
  String LOGOUT_SUCCESS = "Logout Successfully";
  String DELETE_FAILURE = "Deletion Failed";
  
  String INVALID_PASSWORD="Invalid Password";
  String INVALID_LOGINID="User doesn't exist";
  String INVALID_LOGIN_TYPE="Invalid LoginType";
  String PASSWORD_NOT_MATCH="Password ";
  String INVALID_LOGINTYPE="Invalid LoginType";
  String REGISTER="Registration Successfully";
  String CLIENTSAVE="Client save Successfully";
  String CLIENTFAIL="Client save Failure";
  String ENGAGEMENTSAVE="Engagement save Successfully";
  String ENGAGEMENTSAVEFAIL="Engagement save Failure";
  
  String SAVEFILEINS3="File saved in S3";
  String SAVEFILEINS3FAIL="File saved in S3 Failure";
  
  String CLIENTUPDATE="Client update Successfully";
  String CLIENTUPDATEFAIL="Client update Failure";
  String USERPROFILEUPDATE="User update Successfully";
  String USERPROFILEUPDATEFAIL="User update Failure";
  String ENGAGEMENTUPDATE="Engagement update Successfully";
  String ENGAGEMENTUPDATEFAIL="Engagement update Failure";
 
  
  String FORGOTPASS="emailid exist";
  String FORGOTPASSNOT="emailid does not exist";
  String SIGNUPVALIDATION="emailid exist";
  String SIGNUPVALIDATIONFAIL="emailid does not exist";
  
  String LOGINVALIDATIONSUCCESS="login validation pass";
  String LOGINVALIDATIONFAIL="login validation fail";
  
  String CLIENTVALIDATIONSUCCESS="client validation pass";
  String CLIENTVALIDATIONFAIL="client validation fail";
  
  String CLIENTSUCCESS="Client accepted successfully";
  String CLIENTFAILURE="Client Link expire";
  
  
  String CHANGEPASSWORDSUCCESS="change password success";
  String CHANGEPASSWORDFAILURE="change password failure";
  String DELETECLIENTPROFILESUCCESS="Client profile deleted successfully";
  String DELETECLIENTPROFILEFAILURE="Failure in client profile deletion";
  
  String DELETE_FILE="Delete file in S3 Success";
  String DELETE_FOLDER="Delete folder in S3 Success";
  String DELETE_FOLDER_FAILURE="Delete folder in S3 Success";
  String DELETE_FILE_FAILURE="Delete file in S3 failure";
  String CREATE_FOLDER="Create folder in S3 success";
  String MOVE_FILES="Move files in S3 Success";
  String MOVE_FILES_FAILURE="Move files in S3 Failure";
  String CREATE_FOLDER_FAILURE="Create folder in S3 failure";
  String GET_FILE="Get file from S3 success";
  String GET_FILE_FAILURE="Get file from S3 failure";
  String UPDATEFILEINS3="Update File In S3 Successfully";
  String UPDATEFILEINS3FAILURE="Update File In S3 Failure";
  String FOLDERMOVED="Folder moved successfully";
  String FOLDERMOVEFAIL="Folder move failure";
  
  String DELETETRIALBALROW="delete trialbalrow success";
  String DELETETRIALBALROWFAILURE="delete trialbalrow failure";
  String SAVE_TRIALBALANACE_ADJUSTMENT="Save trialbalance adjustment success";
  String SAVE_TRIALBALANACE_ADJUSTMENT_FAIL="Save trialbalance adjustment failed";
  String DELETEENGAGEMENT="delete engagement success";
  String DELETEENGAGEMENTFAILURE="delete engagement failure";
  String UPDATEUSERPROFILESUCCESS="Update userprofile success";
  String UPDATEUSERPROFILEFAILURE="Update userprofile failure";
  String SIGNUPFAIL="User already exist";
  String SIGNUPFAILS="Signup Failure";
  String EMAILIDEXIST="Email Id is already exist";
  String EMAILIDNOTEXIST="Email Id is not exist";
  String EMAILVRIFY="Email Id is not verified";
  String INVALID_REFERRAL="Invalid referral code";
  String INVALID_REFRESH_TOKEN="invalid refresh token";
  String TRANSACTION_UPLOAD_SUCCESS="Transaction uploaded Successfully";
  String TRANSACTION_UPLOAD_FAIL="Transaction upload failed";
  String INVALID_TOKEN="Could not verify your session.";
  
  String AUTHVALIDATION="User authentication is not verified";
  
  String TRANSACTION_ALREADY_APPROVED="This transaction is already approved";
  String TRANSACTION_ALREADY_DECLIEND="This transaction is already decliend";
  String TRANSACTION_ALREADY_DISPUTE = "This transaction is already dispute";
  String TRANSACTION_ALREADY_FUNDED = "This transaction is already funded";

  String TRANSACTION_IS_PENDING_STAGE="This transaction is in pending stage";
  String TRANSACTION_IS_APPORVED_STAGE="This transaction is in approved stage";
  
  String TRANSACTION_APPROVED="This transaction is approved successfully";
  String TRANSACTION_DISPUTED="This transaction is disputed successfully";
  String TRANSACTION_DECLIEND="This transaction is decliend successfully";
  String TRANSACTION_FUNDED ="This transaction is funded successfully";
  String TRANSACTION_IS_NOT_FUNDED ="This transaction is not funded";
  String TRANSACTION_IS_NOT_APPROVED= "This transaction is not approved yet";
  String TRANSACTION_IS_NOT_PENDING= "This transaction is not pending stage.";
  String TRANSACTION_NOT_FOUND = "Transaction is not found";
  
  String FOUNDING_REQEUST_CREATED = "This funding request created successfully";
  String FOUND_ADDED = " fund addedd";
  int GEOLOCATED = 1;

  String LIMIT="limit";
  String OFFSET="offset";
  
  Integer FUNDING_REQUEST=3;
  
  Integer  TARGETED_CONSUMER=99999999;
  String INVALID_ADDRESS="Unable to find address . please provide correct address";
  String SCHEDULAR_JOB = "Schedular Job started in background";

  public static final String NOTIFICATION_FUNDING_REQUEST="Funding Request";
  public static final String NOTIFICATION_COMPLETE_PROFILE="Complete Profile";
  public static final double NOTIFICATION_FUNDING_REQUEST_AMOUNT=500;
  
  public static final String QUICKBOOK_CLIENT_NOT_EXIST = "Quickbook clientId not exists";
  public static final String XERO_CLIENT_NOT_EXIST = "Xero clientId not exists";
  public static String LINKED_SUCCESS = "Linked Successfully";
  public static final String LINKED_FAIL = "Failed to establish connection";
  
  public static final String SOURCELINK_QUICKBOOK = "QuickBook";
  public static final String SOURCELINK_XERO = "xero";

}
