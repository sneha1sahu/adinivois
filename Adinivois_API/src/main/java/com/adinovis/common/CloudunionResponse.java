package com.adinovis.common;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CloudunionResponse {
private String message;
private Integer transacationId;
private String href;


public CloudunionResponse(String message) {
  super();
  this.message = message;
}

public CloudunionResponse(Integer transacationId) {
	super();
	this.transacationId = transacationId;
}

public CloudunionResponse(String message, String href) {
	super();
	this.message = message;
	this.href = href;
}

public String getMessage() {
  return message;
}

public void setMessage(String message) {
  this.message = message;
}

public String getHref() {
	return href;
}

public void setHref(String href) {
	this.href = href;
}

public Integer getTransacationId() {
	return transacationId;
}

public void setTransacationId(Integer transacationId) {
	this.transacationId = transacationId;
}



}
