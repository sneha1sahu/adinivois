package com.adinovis.domain;

import java.math.BigDecimal;


public class UserPoint {
private BigDecimal pSkyPoints;
private Integer pSkyPurchasePoints;
private Double transAmountGrandTotal;
private Integer cashpointscommulative;
private BigDecimal lifeTimePoints;
private String updatedTime;
private BigDecimal skyReferralPoints;
private BigDecimal cashPoints;
private BigDecimal redeemPoints;

private BigDecimal totalPoints;
private BigDecimal purchasePoints;
private BigDecimal referralPoints;
private BigDecimal ppbusinessfactorPoints;
private BigDecimal ppgrowfactorPoints;
private BigDecimal itTotalPoints;
private BigDecimal itredeemPoints;
private BigDecimal cashPointsNet;

public BigDecimal getTotalPoints() {
	return totalPoints;
}
public void setTotalPoints(BigDecimal totalPoints) {
	this.totalPoints = totalPoints;
}
public BigDecimal getPurchasePoints() {
	return purchasePoints;
}
public void setPurchasePoints(BigDecimal purchasePoints) {
	this.purchasePoints = purchasePoints;
}
public BigDecimal getReferralPoints() {
	return referralPoints;
}
public void setReferralPoints(BigDecimal referralPoints) {
	this.referralPoints = referralPoints;
}
public BigDecimal getPpbusinessfactorPoints() {
	return ppbusinessfactorPoints;
}
public void setPpbusinessfactorPoints(BigDecimal ppbusinessfactorPoints) {
	this.ppbusinessfactorPoints = ppbusinessfactorPoints;
}
public BigDecimal getPpgrowfactorPoints() {
	return ppgrowfactorPoints;
}
public void setPpgrowfactorPoints(BigDecimal ppgrowfactorPoints) {
	this.ppgrowfactorPoints = ppgrowfactorPoints;
}
public BigDecimal getItTotalPoints() {
	return itTotalPoints;
}
public void setItTotalPoints(BigDecimal itTotalPoints) {
	this.itTotalPoints = itTotalPoints;
}
public BigDecimal getItredeemPoints() {
	return itredeemPoints;
}
public void setItredeemPoints(BigDecimal itredeemPoints) {
	this.itredeemPoints = itredeemPoints;
}
public BigDecimal getCashPointsNet() {
	return cashPointsNet;
}
public void setCashPointsNet(BigDecimal cashPointsNet) {
	this.cashPointsNet = cashPointsNet;
}
public BigDecimal getLifeTimePoints() {
	return lifeTimePoints;
}
public void setLifeTimePoints(BigDecimal lifeTimePoints) {
	this.lifeTimePoints = lifeTimePoints;
}
public String getUpdatedTime() {
	return updatedTime;
}
public void setUpdatedTime(String updatedTime) {
	this.updatedTime = updatedTime;
}
public BigDecimal getpSkyPoints() {
	return pSkyPoints;
}
public void setpSkyPoints(BigDecimal pSkyPoints) {
	this.pSkyPoints = pSkyPoints;
}
public Integer getpSkyPurchasePoints() {
	return pSkyPurchasePoints;
}
public void setpSkyPurchasePoints(Integer pSkyPurchasePoints) {
	this.pSkyPurchasePoints = pSkyPurchasePoints;
}
public Double getTransAmountGrandTotal() {
	return transAmountGrandTotal;
}
public void setTransAmountGrandTotal(Double transAmountGrandTotal) {
	this.transAmountGrandTotal = transAmountGrandTotal;
}


public Integer getCashpointscommulative() {
  return cashpointscommulative;
}
public void setCashpointscommulative(Integer cashpointscommulative) {
  this.cashpointscommulative = cashpointscommulative;
}

public BigDecimal getSkyReferralPoints() {
	return skyReferralPoints;
}
public void setSkyReferralPoints(BigDecimal skyReferralPoints) {
	this.skyReferralPoints = skyReferralPoints;
}
public BigDecimal getCashPoints() {
	return cashPoints;
}
public void setCashPoints(BigDecimal cashPoints) {
	this.cashPoints = cashPoints;
}
public BigDecimal getRedeemPoints() {
	return redeemPoints;
}
public void setRedeemPoints(BigDecimal redeemPoints) {
	this.redeemPoints = redeemPoints;
}
@Override
public int hashCode() {
  final int prime = 31;
  int result = 1;
  result = prime * result + ((cashpointscommulative == null) ? 0 : cashpointscommulative.hashCode());
  result = prime * result + ((pSkyPoints == null) ? 0 : pSkyPoints.hashCode());
  result = prime * result + ((pSkyPurchasePoints == null) ? 0 : pSkyPurchasePoints.hashCode());
  result = prime * result + ((transAmountGrandTotal == null) ? 0 : transAmountGrandTotal.hashCode());
  return result;
}
@Override
public boolean equals(Object obj) {
  if (this == obj)
    return true;
  if (obj == null)
    return false;
  if (getClass() != obj.getClass())
    return false;
  UserPoint other = (UserPoint) obj;
  if (cashpointscommulative == null) {
    if (other.cashpointscommulative != null)
      return false;
  } else if (!cashpointscommulative.equals(other.cashpointscommulative))
    return false;
  if (pSkyPoints == null) {
    if (other.pSkyPoints != null)
      return false;
  } else if (!pSkyPoints.equals(other.pSkyPoints))
    return false;
  if (pSkyPurchasePoints == null) {
    if (other.pSkyPurchasePoints != null)
      return false;
  } else if (!pSkyPurchasePoints.equals(other.pSkyPurchasePoints))
    return false;
  if (transAmountGrandTotal == null) {
    if (other.transAmountGrandTotal != null)
      return false;
  } else if (!transAmountGrandTotal.equals(other.transAmountGrandTotal))
    return false;
  return true;
}


}
