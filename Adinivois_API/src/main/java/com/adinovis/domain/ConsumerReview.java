package com.adinovis.domain;

public class ConsumerReview {
	
	private String consumerName;
	private String consumerRated;
	private String comment;
	private String dateOfRating;
	public String getConsumerName() {
		return consumerName;
	}
	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}
	public String getConsumerRated() {
		return consumerRated;
	}
	public void setConsumerRated(String consumerRated) {
		this.consumerRated = consumerRated;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDateOfRating() {
		return dateOfRating;
	}
	public void setDateOfRating(String dateOfRating) {
		this.dateOfRating = dateOfRating;
	}
	
	

}
