package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Register {
  private String firstName;
  private String lastName;
  private String loginId;
  private String password;
  @JsonIgnore
  private String encPassword;
  private String deviceToken;
  private Integer osType;
  private Integer loginType;
  private Integer userType;
  private String accountType;
  private String referralCode;
  private String businessName;
  private Double longitude;
  private Double latitude;
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public String getLoginId() {
    return loginId;
  }
  public String getPassword() {
    return password;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public Integer getLoginType() {
    return loginType;
  }
  public void setLoginType(Integer loginType) {
    this.loginType = loginType;
  }
  public String getEncPassword() {
    return encPassword;
  }
  public void setEncPassword(String encPassword) {
    this.encPassword = encPassword;
  }
public String getDeviceToken() {
	return deviceToken;
}
public void setDeviceToken(String deviceToken) {
	this.deviceToken = deviceToken;
}
public Integer getOsType() {
	return osType;
}
public void setOsType(Integer osType) {
	this.osType = osType;
}
public Integer getUserType() {
	return userType;
}
public void setUserType(Integer userType) {
	this.userType = userType;
}
public String getAccountType() {
	return accountType;
}
public void setAccountType(String accountType) {
	this.accountType = accountType;
}
public String getReferralCode() {
	return referralCode;
}
public void setReferralCode(String referralCode) {
	this.referralCode = referralCode;
}
public String getBusinessName() {
	return businessName;
}
public void setBusinessName(String businessName) {
	this.businessName = businessName;
}
public Double getLongitude() {
	return longitude;
}
public void setLongitude(Double longitude) {
	this.longitude = longitude;
}
public Double getLatitude() {
	return latitude;
}
public void setLatitude(Double latitude) {
	this.latitude = latitude;
}

  
  
  

}
