package com.adinovis.domain;

public class UIConfig {
	private int configId;
	private String configKey;
	private String configValue;
	
	public int getConfigId() {
	return configId;
	}
	public void setConfigId(int configId) {
	this.configId = configId;
	}
	public String getConfigKey() {
	return configKey;
	}
	public void setConfigKey(String configKey) {
	this.configKey = configKey;
	}
	public String getConfigValue() {
	return configValue;
	}
	public void setConfigValue(String configValue) {
	this.configValue = configValue;
	}
}
