package com.adinovis.domain;

public class Address {
  private String addressStreet1;
  private String addressStreet2;
  private String addressCity;
  private String addressState;
  private String addressZip;
  public String getAddressStreet1() {
    return addressStreet1;
  }
  public String getAddressStreet2() {
    return addressStreet2;
  }
  public String getAddressCity() {
    return addressCity;
  }
  public String getAddressState() {
    return addressState;
  }
  public String getAddressZip() {
    return addressZip;
  }
  public void setAddressStreet1(String addressStreet1) {
    this.addressStreet1 = addressStreet1;
  }
  public void setAddressStreet2(String addressStreet2) {
    this.addressStreet2 = addressStreet2;
  }
  public void setAddressCity(String addressCity) {
    this.addressCity = addressCity;
  }
  public void setAddressState(String addressState) {
    this.addressState = addressState;
  }
  public void setAddressZip(String addressZip) {
    this.addressZip = addressZip;
  }
  
  

}
