package com.adinovis.domain;

public class Social {
  private String loginId;
  private Integer uid;
  public String getLoginId() {
    return loginId;
  }
  public Integer getUid() {
    return uid;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
  public void setUid(Integer uid) {
    this.uid = uid;
  }
  
}
