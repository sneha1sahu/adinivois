package com.adinovis.domain;

import java.math.BigInteger;

import org.codehaus.jackson.annotate.JsonAutoDetect;
@JsonAutoDetect
public class CuserProfile {
   
	private BigInteger useracctid ;
	private String fullName; 
	private String firstname;
	private String lastname;
	private String businessName;
	private String emailAddress;
	private String loginpassword;
	private BigInteger status; 
	private Boolean userDelete;
	private BigInteger addressId;
	private String address;
	//private Boolean useractive;
	
	private String city;
	private String state;
	private String country;
	private Integer postCode;
	//private Boolean addressactive;
	private Boolean addressDelete;
	private BigInteger businessphoneId;
	private String businessphonetype;
	private String businessphoneNumber;
	//private String countryCode;
	//private Integer businessphoneactive;
	private Integer businessphoneDelete;
	private BigInteger cellphoneId;
	private String cellphonetype;
	private String cellphonenumber;
	//private Integer cellphoneactive;
	private Integer cellphonedelete;
	//private BigInteger useraccountroleId;
	private BigInteger userroleId;
	private String roleName;
	//private Boolean auditrole;
	//private Boolean userroleactive;
	private Boolean userroledelete;
	private String licenseno;
	private String tokenvalue;
	private String profilepicture;
	
	public BigInteger getUseracctid() {
		return useracctid;
	}
	public String getFullName() {
		return fullName;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getBusinessName() {
		return businessName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getLoginpassword() {
		return loginpassword;
	}
	public BigInteger getStatus() {
		return status;
	}
	public Boolean getUserDelete() {
		return userDelete;
	}
	public BigInteger getAddressId() {
		return addressId;
	}
	public String getAddress() {
		return address;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public String getCountry() {
		return country;
	}
	public Integer getPostCode() {
		return postCode;
	}
	public Boolean getAddressDelete() {
		return addressDelete;
	}
	public BigInteger getBusinessphoneId() {
		return businessphoneId;
	}
	public String getBusinessphonetype() {
		return businessphonetype;
	}
	public String getBusinessphoneNumber() {
		return businessphoneNumber;
	}
	public Integer getBusinessphoneDelete() {
		return businessphoneDelete;
	}
	public BigInteger getCellphoneId() {
		return cellphoneId;
	}
	public String getCellphonetype() {
		return cellphonetype;
	}
	public String getCellphonenumber() {
		return cellphonenumber;
	}
	public Integer getCellphonedelete() {
		return cellphonedelete;
	}
	public BigInteger getUserroleId() {
		return userroleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public Boolean getUserroledelete() {
		return userroledelete;
	}
	public String getLicenseno() {
		return licenseno;
	}
	public String getTokenvalue() {
		return tokenvalue;
	}
	public String getProfilepicture() {
		return profilepicture;
	}
	
	public void setUseracctid(BigInteger useracctid) {
		this.useracctid = useracctid;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}
	public void setStatus(BigInteger status) {
		this.status = status;
	}
	public void setUserDelete(Boolean userDelete) {
		this.userDelete = userDelete;
	}
	public void setAddressId(BigInteger addressId) {
		this.addressId = addressId;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setPostCode(Integer postCode) {
		this.postCode = postCode;
	}
	public void setAddressDelete(Boolean addressDelete) {
		this.addressDelete = addressDelete;
	}
	public void setBusinessphoneId(BigInteger businessphoneId) {
		this.businessphoneId = businessphoneId;
	}
	public void setBusinessphonetype(String businessphonetype) {
		this.businessphonetype = businessphonetype;
	}
	public void setBusinessphoneNumber(String businessphoneNumber) {
		this.businessphoneNumber = businessphoneNumber;
	}
	public void setBusinessphoneDelete(Integer businessphoneDelete) {
		this.businessphoneDelete = businessphoneDelete;
	}
	public void setCellphoneId(BigInteger cellphoneId) {
		this.cellphoneId = cellphoneId;
	}
	public void setCellphonetype(String cellphonetype) {
		this.cellphonetype = cellphonetype;
	}
	public void setCellphonenumber(String cellphonenumber) {
		this.cellphonenumber = cellphonenumber;
	}
	public void setCellphonedelete(Integer cellphonedelete) {
		this.cellphonedelete = cellphonedelete;
	}
	public void setUserroleId(BigInteger userroleId) {
		this.userroleId = userroleId;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public void setUserroledelete(Boolean userroledelete) {
		this.userroledelete = userroledelete;
	}
	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}
	public void setTokenvalue(String tokenvalue) {
		this.tokenvalue = tokenvalue;
	}
	public void setProfilepicture(String profilepicture) {
		this.profilepicture = profilepicture;
	}
	
	
	
}
