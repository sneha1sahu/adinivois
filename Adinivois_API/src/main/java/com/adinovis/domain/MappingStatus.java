package com.adinovis.domain;

public class MappingStatus {
	private Integer mappingStatusId;
	private String mappingStatusName;
	private String mappingStatusDescription;

	public Integer getMappingStatusId() {
		return mappingStatusId;
	}

	public void setMappingStatusId(Integer mappingStatusId) {
		this.mappingStatusId = mappingStatusId;
	}

	public String getMappingStatusName() {
		return mappingStatusName;
	}

	public void setMappingStatusName(String mappingStatusName) {
		this.mappingStatusName = mappingStatusName;
	}

	public String getMappingStatusDescription() {
		return mappingStatusDescription;
	}

	public void setMappingStatusDescription(String mappingStatusDescription) {
		this.mappingStatusDescription = mappingStatusDescription;
	}

}
