package com.adinovis.domain;

import java.math.BigInteger;

public class Typeofentity {
	
	private BigInteger typeofentityid;
	private String typeofentityname;
	private String typeofentitycode;
	public BigInteger getTypeofentityid() {
		return typeofentityid;
	}
	public String getTypeofentityname() {
		return typeofentityname;
	}
	public String getTypeofentitycode() {
		return typeofentitycode;
	}
	public void setTypeofentityid(BigInteger typeofentityid) {
		this.typeofentityid = typeofentityid;
	}
	public void setTypeofentityname(String typeofentityname) {
		this.typeofentityname = typeofentityname;
	}
	public void setTypeofentitycode(String typeofentitycode) {
		this.typeofentitycode = typeofentitycode;
	}
	

}
