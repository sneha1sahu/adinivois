package com.adinovis.domain;

public class RefundRequest {
	
	private Integer uId;
	private Integer transactionId;
	private String refundComment;
	private Integer transactionDispute;
	private String disputeComment;
	
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getRefundComment() {
		return refundComment;
	}
	public void setRefundComment(String refundComment) {
		this.refundComment = refundComment;
	}
	public Integer getuId() {
		return uId;
	}
	public void setuId(Integer uId) {
		this.uId = uId;
	}
	public Integer getTransactionDispute() {
		return transactionDispute;
	}
	public void setTransactionDispute(Integer transactionDispute) {
		this.transactionDispute = transactionDispute;
	}
	public String getDisputeComment() {
		return disputeComment;
	}
	public void setDisputeComment(String disputeComment) {
		this.disputeComment = disputeComment;
	}
	
	

}
