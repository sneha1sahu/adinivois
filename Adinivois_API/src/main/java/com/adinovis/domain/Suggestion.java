package com.adinovis.domain;

public class Suggestion {
  
  private String businessId;
  private String suggesstionId;
  private String businessName;
  private String pointsofferred;
  private String rating;
  private String discount_info;
  private Coordinateinfo coordinateinfo;
  private Address address;
  private String mobile;
  private String opening_time;
  private String special_time;
  private String long_desc;
  private String short_desc;
  private Businessimgs business_imgs;
  private String businessWebSite;
  private String menuUrl;
  private String typeOfBusiness;
  private String specialties;
  private double distanceInMiles;
  public String getBusinessId() {
    return businessId;
  }
  public String getSuggesstionId() {
    return suggesstionId;
  }
  public String getBusinessName() {
    return businessName;
  }
  public String getPointsofferred() {
    return pointsofferred;
  }
  public String getRating() {
    return rating;
  }
  public String getDiscount_info() {
    return discount_info;
  }
  public Coordinateinfo getCoordinateinfo() {
    return coordinateinfo;
  }

  public Address getAddress() {
    return address;
  }
  public String getMobile() {
    return mobile;
  }
  public String getOpening_time() {
    return opening_time;
  }
  public String getLong_desc() {
    return long_desc;
  }
  public String getShort_desc() {
    return short_desc;
  }
  public Businessimgs getBusiness_imgs() {
    return business_imgs;
  }
  public void setBusinessId(String businessId) {
    this.businessId = businessId;
  }
  public void setSuggesstionId(String suggesstionId) {
    this.suggesstionId = suggesstionId;
  }
  public void setBusinessName(String businessName) {
    this.businessName = businessName;
  }
  public void setPointsofferred(String pointsofferred) {
    this.pointsofferred = pointsofferred;
  }
  public void setRating(String rating) {
    this.rating = rating;
  }
  public void setDiscount_info(String discount_info) {
    this.discount_info = discount_info;
  }
  public void setCoordinateinfo(Coordinateinfo coordinateinfo) {
    this.coordinateinfo = coordinateinfo;
  }

  public void setAddress(Address address) {
    this.address = address;
  }
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  public void setOpening_time(String opening_time) {
    this.opening_time = opening_time;
  }
  public void setLong_desc(String long_desc) {
    this.long_desc = long_desc;
  }
  public void setShort_desc(String short_desc) {
    this.short_desc = short_desc;
  }
  public void setBusiness_imgs(Businessimgs business_imgs) {
    this.business_imgs = business_imgs;
  }
public double getDistanceInMiles() {
	return distanceInMiles;
}
public void setDistanceInMiles(double distanceInMiles) {
	this.distanceInMiles = distanceInMiles;
}
public String getBusinessWebSite() {
	return businessWebSite;
}
public void setBusinessWebSite(String businessWebSite) {
	this.businessWebSite = businessWebSite;
}
public String getMenuUrl() {
	return menuUrl;
}
public void setMenuUrl(String menuUrl) {
	this.menuUrl = menuUrl;
}
public String getTypeOfBusiness() {
	return typeOfBusiness;
}
public void setTypeOfBusiness(String typeOfBusiness) {
	this.typeOfBusiness = typeOfBusiness;
}
public String getSpecial_time() {
	return special_time;
}
public void setSpecial_time(String special_time) {
	this.special_time = special_time;
}
public String getSpecialties() {
	return specialties;
}
public void setSpecialties(String specialties) {
	this.specialties = specialties;
}
  
  

}
