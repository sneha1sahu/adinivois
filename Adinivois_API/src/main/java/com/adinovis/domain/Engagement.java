package com.adinovis.domain;

import java.math.BigInteger;

public class Engagement {
	
	private BigInteger pengagementsid;
	private Integer pclientfirmid;
	private String pengagementname;
    private Integer pengagementtypeid;
    private String pcompliation;
    private String psubenties;
    private String pyearend;
    private String puseraccoutidroleidlist;
    private String padditionalinfo;
    private Integer ploginid;
    
	public Integer getPclientfirmid() {
		return pclientfirmid;
	}
	public void setPclientfirmid(Integer pclientfirmid) {
		this.pclientfirmid = pclientfirmid;
	}
	public BigInteger getPengagementsid() {
		return pengagementsid;
	}
	public String getPengagementname() {
		return pengagementname;
	}
	public Integer getPengagementtypeid() {
		return pengagementtypeid;
	}
	public String getPcompliation() {
		return pcompliation;
	}
	public String getPsubenties() {
		return psubenties;
	}
	public String getPyearend() {
		return pyearend;
	}
	public String getPuseraccoutidroleidlist() {
		return puseraccoutidroleidlist;
	}
	public String getPadditionalinfo() {
		return padditionalinfo;
	}
	
	public Integer getPloginid() {
		return ploginid;
	}
	public void setPengagementname(String pengagementname) {
		this.pengagementname = pengagementname;
	}
	public void setPengagementtypeid(Integer pengagementtypeid) {
		this.pengagementtypeid = pengagementtypeid;
	}
	public void setPcompliation(String pcompliation) {
		this.pcompliation = pcompliation;
	}
	public void setPsubenties(String psubenties) {
		this.psubenties = psubenties;
	}
	public void setPyearend(String pyearend) {
		this.pyearend = pyearend;
	}
	public void setPuseraccoutidroleidlist(String puseraccoutidroleidlist) {
		this.puseraccoutidroleidlist = puseraccoutidroleidlist;
	}
	public void setPadditionalinfo(String padditionalinfo) {
		this.padditionalinfo = padditionalinfo;
	}
	public void setPengagementsid(BigInteger pengagementsid) {
		this.pengagementsid = pengagementsid;
	}
	public void setPloginid(Integer ploginid) {
		this.ploginid = ploginid;
	}
	
	
	
}
