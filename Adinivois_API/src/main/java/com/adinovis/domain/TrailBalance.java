package com.adinovis.domain;

public class TrailBalance {
	private Integer intrailbalanceid;
	private Integer engagementsid;
	private String inacctyear;
	private String inaccountcode;
	private String inaccountname;
	private String inoriginalbalance;
	private String inPY1;
	private String inPY2;
	private Integer inmapsheetid;
	private Integer ploginid;

	public Integer getIntrailbalanceid() {
		return intrailbalanceid;
	}

	public void setIntrailbalanceid(Integer intrailbalanceid) {
		this.intrailbalanceid = intrailbalanceid;
	}

	public Integer getEngagementsid() {
		return engagementsid;
	}

	public void setEngagementsid(Integer engagementsid) {
		this.engagementsid = engagementsid;
	}

	public String getInacctyear() {
		return inacctyear;
	}

	public void setInacctyear(String inacctyear) {
		this.inacctyear = inacctyear;
	}

	public String getInaccountcode() {
		return inaccountcode;
	}

	public void setInaccountcode(String inaccountcode) {
		this.inaccountcode = inaccountcode;
	}

	public String getInaccountname() {
		return inaccountname;
	}

	public void setInaccountname(String inaccountname) {
		this.inaccountname = inaccountname;
	}

	public String getInoriginalbalance() {
		return inoriginalbalance;
	}

	public void setInoriginalbalance(String inoriginalbalance) {
		this.inoriginalbalance = inoriginalbalance;
	}

	public String getInPY1() {
		return inPY1;
	}

	public void setInPY1(String inPY1) {
		this.inPY1 = inPY1;
	}

	public String getInPY2() {
		return inPY2;
	}

	public void setInPY2(String inPY2) {
		this.inPY2 = inPY2;
	}

	public Integer getInmapsheetid() {
		return inmapsheetid;
	}

	public void setInmapsheetid(Integer inmapsheetid) {
		this.inmapsheetid = inmapsheetid;
	}

	public Integer getPloginid() {
		return ploginid;
	}

	public void setPloginid(Integer ploginid) {
		this.ploginid = ploginid;
	}

}
