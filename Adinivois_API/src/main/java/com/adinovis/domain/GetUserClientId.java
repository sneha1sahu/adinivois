package com.adinovis.domain;

import java.math.BigInteger;

public class GetUserClientId {
	private BigInteger userAcctId;
	private BigInteger clientConfirmId;
	public BigInteger getUserAcctId() {
		return userAcctId;
	}
	public void setUserAcctId(BigInteger userAcctId) {
		this.userAcctId = userAcctId;
	}
	public BigInteger getClientConfirmId() {
		return clientConfirmId;
	}
	public void setClientConfirmId(BigInteger clientConfirmId) {
		this.clientConfirmId = clientConfirmId;
	}
	
}
