package com.adinovis.domain;

public class UploadImagedetails {
  private String loginId;
  //private String url;
  private String upDescIntro;
  private String upDescDetail;
  public String getLoginId() {
    return loginId;
  }
  public String getUpDescIntro() {
    return upDescIntro;
  }
  public String getUpDescDetail() {
    return upDescDetail;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
  public void setUpDescIntro(String upDescIntro) {
    this.upDescIntro = upDescIntro;
  }
  public void setUpDescDetail(String upDescDetail) {
    this.upDescDetail = upDescDetail;
  }
  
  

}
