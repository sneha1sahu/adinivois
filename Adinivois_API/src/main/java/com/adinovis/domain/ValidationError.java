/**
 * Program Name: ValidationError
 * 
 * Program Description / functionality: This is the domain class for Cloudunion service
 * 
 * Modules Impacted: 
 * 
 * Tables affected:
 * 
 * Developed By: Gouri Created Date: 10/03/2017 Code Reviewed By:  Code Review Date:
 * 
 * 
 * Associated Defects Raised :
 * 
 */

package com.adinovis.domain;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonAutoDetect
@JsonInclude(Include.NON_NULL)
public class ValidationError {
    private List<FieldError> fieldErrors = new ArrayList<FieldError>();

    private Integer status;

    public ValidationError() {

    }

    public void addFieldError(String path, String message) {
        FieldError error = new FieldError(path, message);
        fieldErrors.add(error);
    }

    public Integer getStatus() {
        return status;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setFieldErrors(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

}
