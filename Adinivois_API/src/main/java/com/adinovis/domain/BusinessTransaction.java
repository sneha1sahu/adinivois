package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessTransaction {
private String consumerName;
private String consumerId;
private String transactionId;
private String transactionDate;
private String postedDate;
private String comment;
private String rating;
private String businessId;
private String transactionType;
private String transactionstatus;
private String transactionAmountBeforeTaxTip;
private String transactionAmount;
private String transactionPic;
private String  receiptNo;
private String transactionDispute;
private String disputeComment;
public String getConsumerName() {
	return consumerName;
}
public void setConsumerName(String consumerName) {
	this.consumerName = consumerName;
}
public String getConsumerId() {
	return consumerId;
}
public void setConsumerId(String consumerId) {
	this.consumerId = consumerId;
}
public String getTransactionId() {
	return transactionId;
}
public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}
public String getTransactionDate() {
	return transactionDate;
}
public void setTransactionDate(String transactionDate) {
	this.transactionDate = transactionDate;
}
public String getPostedDate() {
	return postedDate;
}
public void setPostedDate(String postedDate) {
	this.postedDate = postedDate;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}
public String getRating() {
	return rating;
}
public void setRating(String rating) {
	this.rating = rating;
}
public String getBusinessId() {
	return businessId;
}
public void setBusinessId(String businessId) {
	this.businessId = businessId;
}
public String getTransactionType() {
	return transactionType;
}
public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
}
public String getTransactionstatus() {
	return transactionstatus;
}
public void setTransactionstatus(String transactionstatus) {
	this.transactionstatus = transactionstatus;
}
public String getTransactionAmountBeforeTaxTip() {
	return transactionAmountBeforeTaxTip;
}
public void setTransactionAmountBeforeTaxTip(String transactionAmountBeforeTaxTip) {
	this.transactionAmountBeforeTaxTip = transactionAmountBeforeTaxTip;
}
public String getTransactionAmount() {
	return transactionAmount;
}
public void setTransactionAmount(String transactionAmount) {
	this.transactionAmount = transactionAmount;
}
public String getTransactionPic() {
	return transactionPic;
}
public void setTransactionPic(String transactionPic) {
	this.transactionPic = transactionPic;
}
public String getReceiptNo() {
	return receiptNo;
}
public void setReceiptNo(String receiptNo) {
	this.receiptNo = receiptNo;
}
public String getTransactionDispute() {
	return transactionDispute;
}
public void setTransactionDispute(String transactionDispute) {
	this.transactionDispute = transactionDispute;
}
public String getDisputeComment() {
	return disputeComment;
}
public void setDisputeComment(String disputeComment) {
	this.disputeComment = disputeComment;
}


}
