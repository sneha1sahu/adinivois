package com.adinovis.domain;

import java.math.BigInteger;

public class Engagementcountdetail {

	
	private BigInteger totalclient;
	private BigInteger totalengagement;
	private BigInteger current;
	private BigInteger completed;
	public BigInteger getTotalclient() {
		return totalclient;
	}
	public BigInteger getTotalengagement() {
		return totalengagement;
	}
	public BigInteger getCurrent() {
		return current;
	}
	public BigInteger getCompleted() {
		return completed;
	}
	public void setTotalclient(BigInteger totalclient) {
		this.totalclient = totalclient;
	}
	public void setTotalengagement(BigInteger totalengagement) {
		this.totalengagement = totalengagement;
	}
	public void setCurrent(BigInteger current) {
		this.current = current;
	}
	public void setCompleted(BigInteger completed) {
		this.completed = completed;
	}
	
	
}
