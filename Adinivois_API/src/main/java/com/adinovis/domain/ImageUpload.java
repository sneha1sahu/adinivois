package com.adinovis.domain;

public class ImageUpload {
private int id;
private String imageData;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getImageData() {
	return imageData;
}
public void setImageData(String imageData) {
	this.imageData = imageData;
}


}
