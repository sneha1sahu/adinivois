package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadTransaction {
private String transactionId;
private String consumerId;
private String businessId;
private String businessName;
private String consumerName;
private String actualDate;
private String postedDate;
private String transType;
private String transStatus;
private String fundingStatus;
private String amountBeforeTaxTips;
private String amountGrandTotal;
private String dayBusDiscount;
private String transDayBusReferral;
private String transDayBusReferralMembership;
private String transDayConReferral;

private String transDayConReferralMembership;
private String receiptUrl;
private String isRefundRequested;
private String refundRequestReason;
private String isRefund;
private String origTransId;
private String cashPoints;
private String redeemPoints;
private String receiptNo;
private String isDispute;
private String disputeComment;
private String isRated;
private String ratingLevel;
private String purchasePoint;
private String rejectedReason;
private String address;
private String ratingComment;
private String businessDiscountedAmount;
private String transactionNo;
public String getTransactionId() {
	return transactionId;
}
public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}
public String getConsumerId() {
	return consumerId;
}
public void setConsumerId(String consumerId) {
	this.consumerId = consumerId;
}
public String getBusinessId() {
	return businessId;
}
public void setBusinessId(String businessId) {
	this.businessId = businessId;
}
public String getActualDate() {
	return actualDate;
}
public void setActualDate(String actualDate) {
	this.actualDate = actualDate;
}
public String getPostedDate() {
	return postedDate;
}
public void setPostedDate(String postedDate) {
	this.postedDate = postedDate;
}
public String getTransType() {
	return transType;
}
public void setTransType(String transType) {
	this.transType = transType;
}
public String getTransStatus() {
	return transStatus;
}
public void setTransStatus(String transStatus) {
	this.transStatus = transStatus;
}
public String getFundingStatus() {
	return fundingStatus;
}
public void setFundingStatus(String fundingStatus) {
	this.fundingStatus = fundingStatus;
}
public String getAmountBeforeTaxTips() {
	return amountBeforeTaxTips;
}
public void setAmountBeforeTaxTips(String amountBeforeTaxTips) {
	this.amountBeforeTaxTips = amountBeforeTaxTips;
}
public String getAmountGrandTotal() {
	return amountGrandTotal;
}
public void setAmountGrandTotal(String amountGrandTotal) {
	this.amountGrandTotal = amountGrandTotal;
}
public String getDayBusDiscount() {
	return dayBusDiscount;
}
public void setDayBusDiscount(String dayBusDiscount) {
	this.dayBusDiscount = dayBusDiscount;
}
public String getTransDayBusReferral() {
	return transDayBusReferral;
}
public void setTransDayBusReferral(String transDayBusReferral) {
	this.transDayBusReferral = transDayBusReferral;
}
public String getTransDayBusReferralMembership() {
	return transDayBusReferralMembership;
}
public void setTransDayBusReferralMembership(String transDayBusReferralMembership) {
	this.transDayBusReferralMembership = transDayBusReferralMembership;
}
public String getTransDayConReferral() {
	return transDayConReferral;
}
public void setTransDayConReferral(String transDayConReferral) {
	this.transDayConReferral = transDayConReferral;
}
public String getTransDayConReferralMembership() {
	return transDayConReferralMembership;
}
public void setTransDayConReferralMembership(String transDayConReferralMembership) {
	this.transDayConReferralMembership = transDayConReferralMembership;
}
public String getReceiptUrl() {
	return receiptUrl;
}
public void setReceiptUrl(String receiptUrl) {
	this.receiptUrl = receiptUrl;
}
public String getIsRefundRequested() {
	return isRefundRequested;
}
public void setIsRefundRequested(String isRefundRequested) {
	this.isRefundRequested = isRefundRequested;
}
public String getRefundRequestReason() {
	return refundRequestReason;
}
public void setRefundRequestReason(String refundRequestReason) {
	this.refundRequestReason = refundRequestReason;
}
public String getIsRefund() {
	return isRefund;
}
public void setIsRefund(String isRefund) {
	this.isRefund = isRefund;
}
public String getOrigTransId() {
	return origTransId;
}
public void setOrigTransId(String origTransId) {
	this.origTransId = origTransId;
}
public String getCashPoints() {
	return cashPoints;
}
public void setCashPoints(String cashPoints) {
	this.cashPoints = cashPoints;
}
public String getRedeemPoints() {
	return redeemPoints;
}
public void setRedeemPoints(String redeemPoints) {
	this.redeemPoints = redeemPoints;
}
public String getReceiptNo() {
	return receiptNo;
}
public void setReceiptNo(String receiptNo) {
	this.receiptNo = receiptNo;
}

public String getIsDispute() {
	return isDispute;
}
public void setIsDispute(String isDispute) {
	this.isDispute = isDispute;
}
public String getDisputeComment() {
	return disputeComment;
}
public void setDisputeComment(String disputeComment) {
	this.disputeComment = disputeComment;
}
public String getIsRated() {
	return isRated;
}
public void setIsRated(String isRated) {
	this.isRated = isRated;
}
public String getRatingLevel() {
	return ratingLevel;
}
public void setRatingLevel(String ratingLevel) {
	this.ratingLevel = ratingLevel;
}
public String getPurchasePoint() {
	return purchasePoint;
}
public void setPurchasePoint(String purchasePoint) {
	this.purchasePoint = purchasePoint;
}
public String getRejectedReason() {
	return rejectedReason;
}
public void setRejectedReason(String rejectedReason) {
	this.rejectedReason = rejectedReason;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getBusinessName() {
	return businessName;
}
public void setBusinessName(String businessName) {
	this.businessName = businessName;
}
public String getConsumerName() {
	return consumerName;
}
public void setConsumerName(String consumerName) {
	this.consumerName = consumerName;
}
public String getRatingComment() {
	return ratingComment;
}
public void setRatingComment(String ratingComment) {
	this.ratingComment = ratingComment;
}
public String getBusinessDiscountedAmount() {
	return businessDiscountedAmount;
}
public void setBusinessDiscountedAmount(String businessDiscountedAmount) {
	this.businessDiscountedAmount = businessDiscountedAmount;
}
public String getTransactionNo() {
	return transactionNo;
}
public void setTransactionNo(String transactionNo) {
	this.transactionNo = transactionNo;
}

}