package com.adinovis.domain;

public class LoginwithSocial {
  private String loginId;
  private String password;
  private Integer logintype;
  private Integer socialid;
  private String deviceToken;
  private Integer osType;
  public String getLoginId() {
    return loginId;
  }
  public String getPassword() {
    return password;
  }
  public Integer getLogintype() {
    return logintype;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setLogintype(Integer logintype) {
    this.logintype = logintype;
  }
  public Integer getSocialid() {
    return socialid;
  }
  public void setSocialid(Integer socialid) {
    this.socialid = socialid;
  }
  public String getDeviceToken() {
    return deviceToken;
  }
  public Integer getOsType() {
    return osType;
  }
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }
  public void setOsType(Integer osType) {
    this.osType = osType;
  }
  
  
  

}
