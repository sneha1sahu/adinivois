package com.adinovis.domain;

public class Devicetoken {
  private Integer userid;
  private String deviceToken;
  public Integer getUserid() {
    return userid;
  }
  public String getDeviceToken() {
    return deviceToken;
  }
  public void setUserid(Integer userid) {
    this.userid = userid;
  }
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }
  
  

}
