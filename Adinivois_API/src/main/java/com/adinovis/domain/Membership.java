package com.adinovis.domain;

import java.util.Date;

public class Membership {
	private Integer membershipId;
	private Integer membershipType;
	private String  membershipName;
	private Integer membershipFor;
	private Double membershipCashConvPercentage;
	private Double membershipManagementFee;
	private Double membershipCosumerRefPercentage;
	private Double membershipBusinessRefPercentage;
	private Double membershipMonthlyFee;
	private Date membershipDetailEffctDate;
	private Date membershipDetailEndDate;
	public Integer getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(Integer membershipId) {
		this.membershipId = membershipId;
	}
	public Integer getMembershipType() {
		return membershipType;
	}
	public void setMembershipType(Integer membershipType) {
		this.membershipType = membershipType;
	}
	public String getMembershipName() {
		return membershipName;
	}
	public void setMembershipName(String membershipName) {
		this.membershipName = membershipName;
	}
	public Integer getMembershipFor() {
		return membershipFor;
	}
	public void setMembershipFor(Integer membershipFor) {
		this.membershipFor = membershipFor;
	}
	public Double getMembershipCashConvPercentage() {
		return membershipCashConvPercentage;
	}
	public void setMembershipCashConvPercentage(Double membershipCashConvPercentage) {
		this.membershipCashConvPercentage = membershipCashConvPercentage;
	}
	public Double getMembershipManagementFee() {
		return membershipManagementFee;
	}
	public void setMembershipManagementFee(Double membershipManagementFee) {
		this.membershipManagementFee = membershipManagementFee;
	}
	public Double getMembershipCosumerRefPercentage() {
		return membershipCosumerRefPercentage;
	}
	public void setMembershipCosumerRefPercentage(
			Double membershipCosumerRefPercentage) {
		this.membershipCosumerRefPercentage = membershipCosumerRefPercentage;
	}
	public Double getMembershipBusinessRefPercentage() {
		return membershipBusinessRefPercentage;
	}
	public void setMembershipBusinessRefPercentage(
			Double membershipBusinessRefPercentage) {
		this.membershipBusinessRefPercentage = membershipBusinessRefPercentage;
	}
	public Double getMembershipMonthlyFee() {
		return membershipMonthlyFee;
	}
	public void setMembershipMonthlyFee(Double membershipMonthlyFee) {
		this.membershipMonthlyFee = membershipMonthlyFee;
	}
	public Date getMembershipDetailEffctDate() {
		return membershipDetailEffctDate;
	}
	public void setMembershipDetailEffctDate(Date membershipDetailEffctDate) {
		this.membershipDetailEffctDate = membershipDetailEffctDate;
	}
	public Date getMembershipDetailEndDate() {
		return membershipDetailEndDate;
	}
	public void setMembershipDetailEndDate(Date membershipDetailEndDate) {
		this.membershipDetailEndDate = membershipDetailEndDate;
	}
	
}
