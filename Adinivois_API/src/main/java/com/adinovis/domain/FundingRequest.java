package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundingRequest {
	private Integer businessId;
	private String fundingRequestNumber;
	private Float frRequestedDollarAmount;
	private Float frFundedDollarAmount;
	public Integer getBusinessId() {
		return businessId;
	}
	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}
	public String getFundingRequestNumber() {
		return fundingRequestNumber;
	}
	public void setFundingRequestNumber(String fundingRequestNumber) {
		this.fundingRequestNumber = fundingRequestNumber;
	}
	public Float getFrRequestedDollarAmount() {
		return frRequestedDollarAmount;
	}
	public void setFrRequestedDollarAmount(Float frRequestedDollarAmount) {
		this.frRequestedDollarAmount = frRequestedDollarAmount;
	}
	public Float getFrFundedDollarAmount() {
		return frFundedDollarAmount;
	}
	public void setFrFundedDollarAmount(Float frFundedDollarAmount) {
		this.frFundedDollarAmount = frFundedDollarAmount;
	}	
	
}
