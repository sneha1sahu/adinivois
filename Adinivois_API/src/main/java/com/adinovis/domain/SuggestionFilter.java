package com.adinovis.domain;

public class SuggestionFilter {
  private Integer logicalGroupId;
  private String logicalGroupName;
  public Integer getLogicalGroupId() {
    return logicalGroupId;
  }
  public String getLogicalGroupName() {
    return logicalGroupName;
  }
  public void setLogicalGroupId(Integer logicalGroupId) {
    this.logicalGroupId = logicalGroupId;
  }
  public void setLogicalGroupName(String logicalGroupName) {
    this.logicalGroupName = logicalGroupName;
  }
  

}
