package com.adinovis.domain;

import java.math.BigInteger;

public class Adjustmenttype {

	private BigInteger adjustmenttypeid;
	private String adjustmenttypename; 
	private String desciption;
	public BigInteger getAdjustmenttypeid() {
		return adjustmenttypeid;
	}
	public String getAdjustmenttypename() {
		return adjustmenttypename;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setAdjustmenttypeid(BigInteger adjustmenttypeid) {
		this.adjustmenttypeid = adjustmenttypeid;
	}
	public void setAdjustmenttypename(String adjustmenttypename) {
		this.adjustmenttypename = adjustmenttypename;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	
}
