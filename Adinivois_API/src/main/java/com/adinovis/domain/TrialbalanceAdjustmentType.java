package com.adinovis.domain;

import java.math.BigInteger;

public class TrialbalanceAdjustmentType {

	private BigInteger intrailbalanceid;
	private BigInteger intrailadjustmentid;
	private BigInteger inadjustmenttypeid;
	private String inadjustmenttypename;
	private String inaccountcode;
	private String inaccountname;
	private BigInteger inacctcredit;
	private BigInteger inacctdebit;
	private String incomment;
	private String incomment1;
	private Boolean inisdelete;
	private BigInteger ploginid;
	private String modifiedby;
	private String fullname;

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public BigInteger getIntrailbalanceid() {
		return intrailbalanceid;
	}

	public void setIntrailbalanceid(BigInteger intrailbalanceid) {
		this.intrailbalanceid = intrailbalanceid;
	}

	public BigInteger getIntrailadjustmentid() {
		return intrailadjustmentid;
	}

	public void setIntrailadjustmentid(BigInteger intrailadjustmentid) {
		this.intrailadjustmentid = intrailadjustmentid;
	}

	public BigInteger getInadjustmenttypeid() {
		return inadjustmenttypeid;
	}

	public void setInadjustmenttypeid(BigInteger inadjustmenttypeid) {
		this.inadjustmenttypeid = inadjustmenttypeid;
	}

	public String getInaccountcode() {
		return inaccountcode;
	}

	public void setInaccountcode(String inaccountcode) {
		this.inaccountcode = inaccountcode;
	}

	public String getInaccountname() {
		return inaccountname;
	}

	public void setInaccountname(String inaccountname) {
		this.inaccountname = inaccountname;
	}

	public BigInteger getInacctcredit() {
		return inacctcredit;
	}

	public void setInacctcredit(BigInteger inacctcredit) {
		this.inacctcredit = inacctcredit;
	}

	public BigInteger getInacctdebit() {
		return inacctdebit;
	}

	public void setInacctdebit(BigInteger inacctdebit) {
		this.inacctdebit = inacctdebit;
	}

	public String getIncomment() {
		return incomment;
	}

	public void setIncomment(String incomment) {
		this.incomment = incomment;
	}

	public String getIncomment1() {
		return incomment1;
	}

	public void setIncomment1(String incomment1) {
		this.incomment1 = incomment1;
	}

	public Boolean getInisdelete() {
		return inisdelete;
	}

	public void setInisdelete(Boolean inisdelete) {
		this.inisdelete = inisdelete;
	}

	public BigInteger getPloginid() {
		return ploginid;
	}

	public void setPloginid(BigInteger ploginid) {
		this.ploginid = ploginid;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getInadjustmenttypename() {
		return inadjustmenttypename;
	}

	public void setInadjustmenttypename(String inadjustmenttypename) {
		this.inadjustmenttypename = inadjustmenttypename;
	}

}
