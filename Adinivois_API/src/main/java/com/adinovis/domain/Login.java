package com.adinovis.domain;

public class Login {
  private String loginId;
  private int logintype;
  private String oldpassword;
  private String newpassword;
public String getLoginId() {
	return loginId;
}
public void setLoginId(String loginId) {
	this.loginId = loginId;
}
public String getOldpassword() {
	return oldpassword;
}
public void setOldpassword(String oldpassword) {
	this.oldpassword = oldpassword;
}
public String getNewpassword() {
	return newpassword;
}
public void setNewpassword(String newpassword) {
	this.newpassword = newpassword;
}
public int getLogintype() {
	return logintype;
}
public void setLogintype(int logintype) {
	this.logintype = logintype;
}
   

}
