package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {
private int uId;
private String transactionId;
private String transactionPic;
private String businessId;
private String transactionDate;
private String  receiptNo;
private String transactionAmount;
private String comment;
private String rating;
private String status;
private String transAmount;
private String is_refund_requested;
private String refund_request_reason;
private String is_refund;
private String isDispute;
private String disputeComment;
private String address;
private String isRated;
private String businessDiscountedAmount;
private String transactionNo;

public int getuId() {
  return uId;
}
public String getTransactionPic() {
  return transactionPic;
}


public String getReceiptNo() {
  return receiptNo;
}
public String getTransactionAmount() {
  return transactionAmount;
}
public void setuId(int uId) {
  this.uId = uId;
}
public void setTransactionPic(String transactionPic) {
  this.transactionPic = transactionPic;
}


public String getBusinessId() {
	return businessId;
}
public void setBusinessId(String businessId) {
	this.businessId = businessId;
}

public void setReceiptNo(String receiptNo) {
  this.receiptNo = receiptNo;
}
public void setTransactionAmount(String transactionAmount) {
  this.transactionAmount = transactionAmount;
}
public String getTransactionId() {
	return transactionId;
}
public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}
public String getRating() {
	return rating;
}
public void setRating(String rating) {
	this.rating = rating;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getTransAmount() {
	return transAmount;
}
public void setTransAmount(String transAmount) {
	this.transAmount = transAmount;
}
public String getTransactionDate() {
	return transactionDate;
}
public void setTransactionDate(String transactionDate) {
	this.transactionDate = transactionDate;
}
public String getIs_refund_requested() {
	return is_refund_requested;
}
public void setIs_refund_requested(String is_refund_requested) {
	this.is_refund_requested = is_refund_requested;
}
public String getRefund_request_reason() {
	return refund_request_reason;
}
public void setRefund_request_reason(String refund_request_reason) {
	this.refund_request_reason = refund_request_reason;
}
public String getIs_refund() {
	return is_refund;
}
public void setIs_refund(String is_refund) {
	this.is_refund = is_refund;
}

public String getIsDispute() {
	return isDispute;
}
public void setIsDispute(String isDispute) {
	this.isDispute = isDispute;
}
public String getDisputeComment() {
	return disputeComment;
}
public void setDisputeComment(String disputeComment) {
	this.disputeComment = disputeComment;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getIsRated() {
	return isRated;
}
public void setIsRated(String isRated) {
	this.isRated = isRated;
}
public String getBusinessDiscountedAmount() {
	return businessDiscountedAmount;
}
public void setBusinessDiscountedAmount(String businessDiscountedAmount) {
	this.businessDiscountedAmount = businessDiscountedAmount;
}
public String getTransactionNo() {
	return transactionNo;
}
public void setTransactionNo(String transactionNo) {
	this.transactionNo = transactionNo;
}

}
