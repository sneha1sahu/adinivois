package com.adinovis.domain;

public class RatingSummary {
	private Integer businessAccountId;
	private  Double ratingLevel;
	private Integer ratingLevelCount;
	private Integer starCount1;
	private Integer starCount2;
	private Integer starCount3;
	private Integer starCount4;
	private Integer starCount5;
	public Integer getBusinessAccountId() {
		return businessAccountId;
	}
	public void setBusinessAccountId(Integer businessAccountId) {
		this.businessAccountId = businessAccountId;
	}
	public Double getRatingLevel() {
		return ratingLevel;
	}
	public void setRatingLevel(Double ratingLevel) {
		this.ratingLevel = ratingLevel;
	}
	public Integer getRatingLevelCount() {
		return ratingLevelCount;
	}
	public void setRatingLevelCount(Integer ratingLevelCount) {
		this.ratingLevelCount = ratingLevelCount;
	}
	public Integer getStarCount1() {
		return starCount1;
	}
	public void setStarCount1(Integer starCount1) {
		this.starCount1 = starCount1;
	}
	public Integer getStarCount2() {
		return starCount2;
	}
	public void setStarCount2(Integer starCount2) {
		this.starCount2 = starCount2;
	}
	public Integer getStarCount3() {
		return starCount3;
	}
	public void setStarCount3(Integer starCount3) {
		this.starCount3 = starCount3;
	}
	public Integer getStarCount4() {
		return starCount4;
	}
	public void setStarCount4(Integer starCount4) {
		this.starCount4 = starCount4;
	}
	public Integer getStarCount5() {
		return starCount5;
	}
	public void setStarCount5(Integer starCount5) {
		this.starCount5 = starCount5;
	}
	
}
