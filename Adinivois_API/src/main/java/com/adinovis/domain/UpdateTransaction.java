package com.adinovis.domain;

public class UpdateTransaction {
private Integer uId;
private Integer transactionId;
private Integer currentStatus;
private Integer updateStatus;
private String comment;
public Integer getuId() {
	return uId;
}
public void setuId(Integer uId) {
	this.uId = uId;
}
public Integer getTransactionId() {
	return transactionId;
}
public void setTransactionId(Integer transactionId) {
	this.transactionId = transactionId;
}
public Integer getCurrentStatus() {
	return currentStatus;
}
public void setCurrentStatus(Integer currentStatus) {
	this.currentStatus = currentStatus;
}
public Integer getUpdateStatus() {
	return updateStatus;
}
public void setUpdateStatus(Integer updateStatus) {
	this.updateStatus = updateStatus;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}

}
