package com.adinovis.domain;

public class MyReferrals {
private String referralId;
private String referralName;
private String referralPic;
private String referralType;
private String referred_date;
public String getReferralId() {
	return referralId;
}
public void setReferralId(String referralId) {
	this.referralId = referralId;
}
public String getReferralName() {
	return referralName;
}
public void setReferralName(String referralName) {
	this.referralName = referralName;
}
public String getReferralPic() {
	return referralPic;
}
public void setReferralPic(String referralPic) {
	this.referralPic = referralPic;
}
public String getReferralType() {
	return referralType;
}
public void setReferralType(String referralType) {
	this.referralType = referralType;
}
public String getReferred_date() {
	return referred_date;
}
public void setReferred_date(String referred_date) {
	this.referred_date = referred_date;
}

}
