package com.adinovis.domain;

public class State {
private Integer stateId;
private String abbreviation;
private String country;
private String name;
public Integer getStateId() {
	return stateId;
}
public void setStateId(Integer stateId) {
	this.stateId = stateId;
}
public String getAbbreviation() {
	return abbreviation;
}
public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
