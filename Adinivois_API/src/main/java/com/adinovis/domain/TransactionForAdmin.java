package com.adinovis.domain;

public class TransactionForAdmin {
	private String transactionId;
	private String businessName;
	private String consumerId;
	private String businessId;
	private String consumerName;
	private String actualDate;
	private String postedDate;
	private String transType;
	private String transStatus;
	private String fundingStatus;
	private String amountBeforeTaxTips;
	private String dayBusDiscount;
	private String transDayBusReferral;
	private String transDayBusReferralMembership;
	private String transDayConReferral;
	private String transDayConReferralMembership;
	private String receiptUrl;
	private String isRefundRequested;
	private String refundRequestReason;
	private String isRefund;
	private String origTransId;
	private String cashPoints;
	private String redeemPoints;
	private String receiptNo;
	private String isDispute;
	private String disputeComment;
	private String isRated;
	private String ratingLevel;
	private String purchasePoint;
	private String rejectedReason;
	private String address;
	private String ratingComment;
	private String businessDiscountedAmount;
	private String transactionNo;
	
}
