package com.adinovis.domain;


public class ClientTokenStatus {

	
	private String tokenkey;
	private Integer pstatusid;
	public String getTokenkey() {
		return tokenkey;
	}
	public Integer getPstatusid() {
		return pstatusid;
	}
	public void setTokenkey(String tokenkey) {
		this.tokenkey = tokenkey;
	}
	public void setPstatusid(Integer pstatusid) {
		this.pstatusid = pstatusid;
	}
	
	
	
}
