package com.adinovis.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessProfile {
	private String accountId;
	private String ein;
	private String contactPeopleFirstName;
	private String contactPeopleLastName;
	private String storeContactEmail;
	private String businessHours;
	private String specialHours;
	private String phone1;
	private String phone2;
	private String businessPic1Url;
	private String businessPic2Url;
	private String businessPic3Url;
	private String shortDesc;
	private String longDesc;
	private String typeOfBusiness;
	private String businessName;
	private String businessAddressStreet1;
	private String businessAddressStreet2;
	private String businessAddressCity;
	private String businessAddressState;
	private String businessAddressZip;
	private String businessSpecialAddressInstruction;
	private String additionalFundingType;
	private String additionalFundingMethod;
	private String isGeocoded;
	private String businessGeolongitude;
	private String businessGeolatitude;
	private String SuggestionId;
	private String pointsOffered;
	private String discountInfo;
	private String specialties;
	private String descriptionInStar;
	private String urlForMenu;
	private String urlForWebsite;
	private String ratingLevel;
	private String ratingLevelCount;
	private String starCount1;
	private String starCount2;
	private String starCount3;
	private String starCount4;
	private String starCount5;
	private String updateTime;
	private String startDate;
	private String endDate;
	
	private List<BusinessDiscount> businessDiscounts;
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getEin() {
		return ein;
	}
	public void setEin(String ein) {
		this.ein = ein;
	}
	public String getContactPeopleFirstName() {
		return contactPeopleFirstName;
	}
	public void setContactPeopleFirstName(String contactPeopleFirstName) {
		this.contactPeopleFirstName = contactPeopleFirstName;
	}
	public String getContactPeopleLastName() {
		return contactPeopleLastName;
	}
	public void setContactPeopleLastName(String contactPeopleLastName) {
		this.contactPeopleLastName = contactPeopleLastName;
	}
	public String getStoreContactEmail() {
		return storeContactEmail;
	}
	public void setStoreContactEmail(String storeContactEmail) {
		this.storeContactEmail = storeContactEmail;
	}
	public String getBusinessHours() {
		return businessHours;
	}
	public void setBusinessHours(String businessHours) {
		this.businessHours = businessHours;
	}
	public String getSpecialHours() {
		return specialHours;
	}
	public void setSpecialHours(String specialHours) {
		this.specialHours = specialHours;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getBusinessPic1Url() {
		return businessPic1Url;
	}
	public void setBusinessPic1Url(String businessPic1Url) {
		this.businessPic1Url = businessPic1Url;
	}
	public String getBusinessPic2Url() {
		return businessPic2Url;
	}
	public void setBusinessPic2Url(String businessPic2Url) {
		this.businessPic2Url = businessPic2Url;
	}
	public String getBusinessPic3Url() {
		return businessPic3Url;
	}
	public void setBusinessPic3Url(String businessPic3Url) {
		this.businessPic3Url = businessPic3Url;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getLongDesc() {
		return longDesc;
	}
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	public String getTypeOfBusiness() {
		return typeOfBusiness;
	}
	public void setTypeOfBusiness(String typeOfBusiness) {
		this.typeOfBusiness = typeOfBusiness;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessAddressStreet1() {
		return businessAddressStreet1;
	}
	public void setBusinessAddressStreet1(String businessAddressStreet1) {
		this.businessAddressStreet1 = businessAddressStreet1;
	}
	public String getBusinessAddressStreet2() {
		return businessAddressStreet2;
	}
	public void setBusinessAddressStreet2(String businessAddressStreet2) {
		this.businessAddressStreet2 = businessAddressStreet2;
	}
	public String getBusinessAddressCity() {
		return businessAddressCity;
	}
	public void setBusinessAddressCity(String businessAddressCity) {
		this.businessAddressCity = businessAddressCity;
	}
	public String getBusinessAddressState() {
		return businessAddressState;
	}
	public void setBusinessAddressState(String businessAddressState) {
		this.businessAddressState = businessAddressState;
	}
	public String getBusinessAddressZip() {
		return businessAddressZip;
	}
	public void setBusinessAddressZip(String businessAddressZip) {
		this.businessAddressZip = businessAddressZip;
	}
	public String getBusinessSpecialAddressInstruction() {
		return businessSpecialAddressInstruction;
	}
	public void setBusinessSpecialAddressInstruction(String businessSpecialAddressInstruction) {
		this.businessSpecialAddressInstruction = businessSpecialAddressInstruction;
	}
	public String getAdditionalFundingType() {
		return additionalFundingType;
	}
	public void setAdditionalFundingType(String additionalFundingType) {
		this.additionalFundingType = additionalFundingType;
	}
	public String getAdditionalFundingMethod() {
		return additionalFundingMethod;
	}
	public void setAdditionalFundingMethod(String additionalFundingMethod) {
		this.additionalFundingMethod = additionalFundingMethod;
	}
	public String getIsGeocoded() {
		return isGeocoded;
	}
	public void setIsGeocoded(String isGeocoded) {
		this.isGeocoded = isGeocoded;
	}
	
	public String getBusinessGeolongitude() {
		return businessGeolongitude;
	}
	public void setBusinessGeolongitude(String businessGeolongitude) {
		this.businessGeolongitude = businessGeolongitude;
	}
	public String getBusinessGeolatitude() {
		return businessGeolatitude;
	}
	public void setBusinessGeolatitude(String businessGeolatitude) {
		this.businessGeolatitude = businessGeolatitude;
	}
	public String getSuggestionId() {
		return SuggestionId;
	}
	public void setSuggestionId(String suggestionId) {
		SuggestionId = suggestionId;
	}
	public String getPointsOffered() {
		return pointsOffered;
	}
	public void setPointsOffered(String pointsOffered) {
		this.pointsOffered = pointsOffered;
	}
	public String getDiscountInfo() {
		return discountInfo;
	}
	public void setDiscountInfo(String discountInfo) {
		this.discountInfo = discountInfo;
	}
	public String getSpecialties() {
		return specialties;
	}
	public void setSpecialties(String specialties) {
		this.specialties = specialties;
	}
	public String getDescriptionInStar() {
		return descriptionInStar;
	}
	public void setDescriptionInStar(String descriptionInStar) {
		this.descriptionInStar = descriptionInStar;
	}
	public String getUrlForMenu() {
		return urlForMenu;
	}
	public void setUrlForMenu(String urlForMenu) {
		this.urlForMenu = urlForMenu;
	}
	public String getUrlForWebsite() {
		return urlForWebsite;
	}
	public void setUrlForWebsite(String urlForWebsite) {
		this.urlForWebsite = urlForWebsite;
	}
	public String getRatingLevel() {
		return ratingLevel;
	}
	public void setRatingLevel(String ratingLevel) {
		this.ratingLevel = ratingLevel;
	}
	public String getRatingLevelCount() {
		return ratingLevelCount;
	}
	public void setRatingLevelCount(String ratingLevelCount) {
		this.ratingLevelCount = ratingLevelCount;
	}
	public String getStarCount1() {
		return starCount1;
	}
	public void setStarCount1(String starCount1) {
		this.starCount1 = starCount1;
	}
	public String getStarCount2() {
		return starCount2;
	}
	public void setStarCount2(String starCount2) {
		this.starCount2 = starCount2;
	}
	public String getStarCount3() {
		return starCount3;
	}
	public void setStarCount3(String starCount3) {
		this.starCount3 = starCount3;
	}
	public String getStarCount4() {
		return starCount4;
	}
	public void setStarCount4(String starCount4) {
		this.starCount4 = starCount4;
	}
	public String getStarCount5() {
		return starCount5;
	}
	public void setStarCount5(String starCount5) {
		this.starCount5 = starCount5;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public List<BusinessDiscount> getBusinessDiscounts() {
		return businessDiscounts;
	}
	public void setBusinessDiscounts(List<BusinessDiscount> businessDiscounts) {
		this.businessDiscounts = businessDiscounts;
	}


}
