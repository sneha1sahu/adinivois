package com.adinovis.domain;

public class RegisteredUser {
	 private String firstName;
	  private String lastName;
	  private String loginId;
	  private Integer uid;
	  private String referralCode;
	  private String referralQr;
	  
	public RegisteredUser() {

	}
	public RegisteredUser(String firstName, String lastName, String loginId, Integer uid) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.loginId = loginId;
		this.uid = uid;
	}
	
	public RegisteredUser(String firstName, String lastName, String loginId, Integer uid, String referralCode,
			String referralQr) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.loginId = loginId;
		this.uid = uid;
		this.referralCode = referralCode;
		this.referralQr = referralQr;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getReferralQr() {
		return referralQr;
	}
	public void setReferralQr(String referralQr) {
		this.referralQr = referralQr;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	  
}
