package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyBusinessAccount {
	private String transactionType;
	private String previousPoint;
	private String previousBalance;
	private String afterBalance;
	private String batAsOfDate;
	private String batUpdated;
	private String batTransactionId;
	private String customerName;
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPreviousPoint() {
		return previousPoint;
	}
	public void setPreviousPoint(String previousPoint) {
		this.previousPoint = previousPoint;
	}
	public String getPreviousBalance() {
		return previousBalance;
	}
	public void setPreviousBalance(String previousBalance) {
		this.previousBalance = previousBalance;
	}
	public String getAfterBalance() {
		return afterBalance;
	}
	public void setAfterBalance(String afterBalance) {
		this.afterBalance = afterBalance;
	}
	public String getBatAsOfDate() {
		return batAsOfDate;
	}
	public void setBatAsOfDate(String batAsOfDate) {
		this.batAsOfDate = batAsOfDate;
	}
	public String getBatUpdated() {
		return batUpdated;
	}
	public void setBatUpdated(String batUpdated) {
		this.batUpdated = batUpdated;
	}
	public String getBatTransactionId() {
		return batTransactionId;
	}
	public void setBatTransactionId(String batTransactionId) {
		this.batTransactionId = batTransactionId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
