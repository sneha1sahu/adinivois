package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CreateUserAccount {

	 private String role;
	    private String email;
	    private String password;
	    private String userName;
	    private String lastName;
	    private String firstName;
	    private String phoneNumber;
	    private String companyName;
	    private String licenseNumber;
	    private String businessPhone;
	    private String businessAddress;
	    private Boolean isAgreeWithTerms;
	    @JsonIgnore
	    private String encPassword;
		public String getRole() {
			return role;
		}
		public String getEmail() {
			return email;
		}
		public String getPassword() {
			return password;
		}
		public String getUserName() {
			return userName;
		}
		public String getLastName() {
			return lastName;
		}
		public String getFirstName() {
			return firstName;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public String getCompanyName() {
			return companyName;
		}
		public String getLicenseNumber() {
			return licenseNumber;
		}
		public String getBusinessPhone() {
			return businessPhone;
		}
		public String getBusinessAddress() {
			return businessAddress;
		}
		public Boolean getIsAgreeWithTerms() {
			return isAgreeWithTerms;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		public void setLicenseNumber(String licenseNumber) {
			this.licenseNumber = licenseNumber;
		}
		public void setBusinessPhone(String businessPhone) {
			this.businessPhone = businessPhone;
		}
		public void setBusinessAddress(String businessAddress) {
			this.businessAddress = businessAddress;
		}
		public void setIsAgreeWithTerms(Boolean isAgreeWithTerms) {
			this.isAgreeWithTerms = isAgreeWithTerms;
		}
		public String getEncPassword() {
			return encPassword;
		}
		public void setEncPassword(String encPassword) {
			this.encPassword = encPassword;
		}
		
	    
}
