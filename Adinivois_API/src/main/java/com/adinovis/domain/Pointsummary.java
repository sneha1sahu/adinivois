package com.adinovis.domain;

import java.math.BigDecimal;

public class Pointsummary {
	private BigDecimal cloudPoints;
	private Integer lifetimepoints;
	private Integer purchasepoints;
	private Integer referralPoints;
	private Integer cashPoints;
	private Integer lifetimeredeemedcashpoints;
	public BigDecimal getCloudPoints() {
		return cloudPoints;
	}
	public void setCloudPoints(BigDecimal cloudPoints) {
		this.cloudPoints = cloudPoints;
	}
	public Integer getLifetimepoints() {
		return lifetimepoints;
	}
	public void setLifetimepoints(Integer lifetimepoints) {
		this.lifetimepoints = lifetimepoints;
	}
	public Integer getPurchasepoints() {
		return purchasepoints;
	}
	public void setPurchasepoints(Integer purchasepoints) {
		this.purchasepoints = purchasepoints;
	}
	public Integer getReferralPoints() {
		return referralPoints;
	}
	public void setReferralPoints(Integer referralPoints) {
		this.referralPoints = referralPoints;
	}
	public Integer getCashPoints() {
		return cashPoints;
	}
	public void setCashPoints(Integer cashPoints) {
		this.cashPoints = cashPoints;
	}
	public Integer getLifetimeredeemedcashpoints() {
		return lifetimeredeemedcashpoints;
	}
	public void setLifetimeredeemedcashpoints(Integer lifetimeredeemedcashpoints) {
		this.lifetimeredeemedcashpoints = lifetimeredeemedcashpoints;
	}


}
