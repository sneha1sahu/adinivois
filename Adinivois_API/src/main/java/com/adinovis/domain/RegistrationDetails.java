package com.adinovis.domain;




public class RegistrationDetails {
  private Integer groupId;
  //private String groupName;
  private String groupQuestion;
  private Integer schoolId;
  private String schoolName;
  //private String subgroupName;
  private String logicalgroupName;
  private Integer logicalgroupId;
  private Integer userId;
  private String schoolmailId;
  private String password;
  private Integer referreduserId;
  private String roleName;
  private Boolean status;
  private String deviceToken;
  public Integer getGroupId() {
    return groupId;
  }
  public String getGroupQuestion() {
    return groupQuestion;
  }
  public Integer getSchoolId() {
    return schoolId;
  }
  public String getSchoolName() {
    return schoolName;
  }
  public String getLogicalgroupName() {
    return logicalgroupName;
  }
  public Integer getLogicalgroupId() {
    return logicalgroupId;
  }
  public Integer getUserId() {
    return userId;
  }
  public String getSchoolmailId() {
    return schoolmailId;
  }
  public String getPassword() {
    return password;
  }
  public Integer getReferreduserId() {
    return referreduserId;
  }
  public String getRoleName() {
    return roleName;
  }
  public Boolean getStatus() {
    return status;
  }
  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }
  public void setGroupQuestion(String groupQuestion) {
    this.groupQuestion = groupQuestion;
  }
  public void setSchoolId(Integer schoolId) {
    this.schoolId = schoolId;
  }
  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }
  public void setLogicalgroupName(String logicalgroupName) {
    this.logicalgroupName = logicalgroupName;
  }
  public void setLogicalgroupId(Integer logicalgroupId) {
    this.logicalgroupId = logicalgroupId;
  }
  public void setUserId(Integer userId) {
    this.userId = userId;
  }
  public void setSchoolmailId(String schoolmailId) {
    this.schoolmailId = schoolmailId;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setReferreduserId(Integer referreduserId) {
    this.referreduserId = referreduserId;
  }
  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }
  public void setStatus(Boolean status) {
    this.status = status;
  }
  public String getDeviceToken() {
    return deviceToken;
  }
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }
  
}
