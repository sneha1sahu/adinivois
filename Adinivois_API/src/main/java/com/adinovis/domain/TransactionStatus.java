package com.adinovis.domain;

import java.math.BigInteger;

public class TransactionStatus {
	private BigInteger pending;
	private BigInteger approved;
	private BigInteger denied;
	public BigInteger getPending() {
		return pending;
	}
	public void setPending(BigInteger pending) {
		this.pending = pending;
	}
	public BigInteger getApproved() {
		return approved;
	}
	public void setApproved(BigInteger approved) {
		this.approved = approved;
	}
	public BigInteger getDenied() {
		return denied;
	}
	public void setDenied(BigInteger denied) {
		this.denied = denied;
	}
	
	

}
