package com.adinovis.domain;

import java.math.BigInteger;

public class Clientdetails {

	
	private BigInteger clientfirmid;
	private String clientname;
	private String businessname;
	private String tokenvalue;
	
	public BigInteger getClientfirmid() {
		return clientfirmid;
	}
	public String getClientname() {
		return clientname;
	}

	public String getBusinessname() {
		return businessname;
	}
	public String getTokenvalue() {
		return tokenvalue;
	}
	public void setClientfirmid(BigInteger clientfirmid) {
		this.clientfirmid = clientfirmid;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}
	public void setTokenvalue(String tokenvalue) {
		this.tokenvalue = tokenvalue;
	}
	
	
}
