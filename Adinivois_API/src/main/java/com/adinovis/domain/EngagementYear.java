package com.adinovis.domain;

import java.math.BigInteger;

public class EngagementYear {
	private BigInteger engagementsid;
	private String acctyear;
	public BigInteger getEngagementsid() {
		return engagementsid;
	}
	public void setEngagementsid(BigInteger engagementsid) {
		this.engagementsid = engagementsid;
	}
	public String getAcctyear() {
		return acctyear;
	}
	public void setAcctyear(String acctyear) {
		this.acctyear = acctyear;
	}
	
}
