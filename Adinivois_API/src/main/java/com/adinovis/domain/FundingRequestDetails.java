package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundingRequestDetails {
	private String fundingRequestNumber;
	private String frRequestedDollarAmount;
	private String frFundedDollarAmount;
	private String frStatusName;
	private String frFundedDateTime;
	private String frcreatedDateTime;
	private String frapprovedBy;
	private String frstatusUpdatedBy;
	private String frstatusUpdatedDateTime;
	public String getFundingRequestNumber() {
		return fundingRequestNumber;
	}
	public void setFundingRequestNumber(String fundingRequestNumber) {
		this.fundingRequestNumber = fundingRequestNumber;
	}
	public String getFrRequestedDollarAmount() {
		return frRequestedDollarAmount;
	}
	public void setFrRequestedDollarAmount(String frRequestedDollarAmount) {
		this.frRequestedDollarAmount = frRequestedDollarAmount;
	}
	public String getFrFundedDollarAmount() {
		return frFundedDollarAmount;
	}
	public void setFrFundedDollarAmount(String frFundedDollarAmount) {
		this.frFundedDollarAmount = frFundedDollarAmount;
	}
	public String getFrStatusName() {
		return frStatusName;
	}
	public void setFrStatusName(String frStatusName) {
		this.frStatusName = frStatusName;
	}
	public String getFrFundedDateTime() {
		return frFundedDateTime;
	}
	public void setFrFundedDateTime(String frFundedDateTime) {
		this.frFundedDateTime = frFundedDateTime;
	}
	public String getFrcreatedDateTime() {
		return frcreatedDateTime;
	}
	public void setFrcreatedDateTime(String frcreatedDateTime) {
		this.frcreatedDateTime = frcreatedDateTime;
	}
	public String getFrapprovedBy() {
		return frapprovedBy;
	}
	public void setFrapprovedBy(String frapprovedBy) {
		this.frapprovedBy = frapprovedBy;
	}
	public String getFrstatusUpdatedBy() {
		return frstatusUpdatedBy;
	}
	public void setFrstatusUpdatedBy(String frstatusUpdatedBy) {
		this.frstatusUpdatedBy = frstatusUpdatedBy;
	}
	public String getFrstatusUpdatedDateTime() {
		return frstatusUpdatedDateTime;
	}
	public void setFrstatusUpdatedDateTime(String frstatusUpdatedDateTime) {
		this.frstatusUpdatedDateTime = frstatusUpdatedDateTime;
	}
	
	
}
