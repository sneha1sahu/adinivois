package com.adinovis.domain;

public class NotificationType {
	private Integer notificationTypeId;
	private String notificationTypeName;
	private String notificatonMessage;
	public Integer getNotificationTypeId() {
		return notificationTypeId;
	}
	public void setNotificationTypeId(Integer notificationTypeId) {
		this.notificationTypeId = notificationTypeId;
	}
	public String getNotificationTypeName() {
		return notificationTypeName;
	}
	public void setNotificationTypeName(String notificationTypeName) {
		this.notificationTypeName = notificationTypeName;
	}
	public String getNotificatonMessage() {
		return notificatonMessage;
	}
	public void setNotificatonMessage(String notificatonMessage) {
		this.notificatonMessage = notificatonMessage;
	}

}
