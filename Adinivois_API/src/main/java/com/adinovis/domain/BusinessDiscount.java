package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessDiscount {
	private Integer businessId;
	private Integer discountId; 
	private Integer discountStatus;
	private Float discountRate;
	private String startDate;
	private String endDate;
	public Integer getBusinessId() {
		return businessId;
	}
	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}
	public Integer getDiscountId() {
		return discountId;
	}
	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}
	public Integer getDiscountStatus() {
		return discountStatus;
	}
	public void setDiscountStatus(Integer discountStatus) {
		this.discountStatus = discountStatus;
	}
	public Float getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(Float discountRate) {
		this.discountRate = discountRate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}
