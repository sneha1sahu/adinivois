package com.adinovis.domain;

import java.math.BigInteger;

public class Clientcountdetails {

	private BigInteger totalclient;
	private BigInteger activeclient;
	private BigInteger invitependingwithclient;
	private BigInteger totalengagement;
	public BigInteger getTotalclient() {
		return totalclient;
	}
	public BigInteger getActiveclient() {
		return activeclient;
	}
	public BigInteger getInvitependingwithclient() {
		return invitependingwithclient;
	}
	public BigInteger getTotalengagement() {
		return totalengagement;
	}
	public void setTotalclient(BigInteger totalclient) {
		this.totalclient = totalclient;
	}
	public void setActiveclient(BigInteger activeclient) {
		this.activeclient = activeclient;
	}
	public void setInvitependingwithclient(BigInteger invitependingwithclient) {
		this.invitependingwithclient = invitependingwithclient;
	}
	public void setTotalengagement(BigInteger totalengagement) {
		this.totalengagement = totalengagement;
	}
	
	
}
