package com.adinovis.domain;

public class Resetpassword {

	
	private String tokenkey;
	private String newpassword;
	public String getTokenkey() {
		return tokenkey;
	}
	public String getNewpassword() {
		return newpassword;
	}
	public void setTokenkey(String tokenkey) {
		this.tokenkey = tokenkey;
	}
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	
}
