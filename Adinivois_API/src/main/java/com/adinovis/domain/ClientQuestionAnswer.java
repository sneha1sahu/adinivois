package com.adinovis.domain;

import java.math.BigInteger;

public class ClientQuestionAnswer {
	private BigInteger clientquestionid;
	private BigInteger clientquestiontypeid;
	private String questiondescription; 
	private String answertype;
	private Boolean quesactive;
	private Boolean quesdelete;
	private BigInteger clientquestionanswerid;
	private BigInteger engagementsid;
	private String clientanswer;
	private String clientnote;
	private Boolean ansactive;
	private Boolean ansdelete;
	public BigInteger getClientquestionid() {
		return clientquestionid;
	}
	public void setClientquestionid(BigInteger clientquestionid) {
		this.clientquestionid = clientquestionid;
	}
	public BigInteger getClientquestiontypeid() {
		return clientquestiontypeid;
	}
	public void setClientquestiontypeid(BigInteger clientquestiontypeid) {
		this.clientquestiontypeid = clientquestiontypeid;
	}
	public String getQuestiondescription() {
		return questiondescription;
	}
	public void setQuestiondescription(String questiondescription) {
		this.questiondescription = questiondescription;
	}
	public String getAnswertype() {
		return answertype;
	}
	public void setAnswertype(String answertype) {
		this.answertype = answertype;
	}
	public Boolean getQuesactive() {
		return quesactive;
	}
	public void setQuesactive(Boolean quesactive) {
		this.quesactive = quesactive;
	}
	public Boolean getQuesdelete() {
		return quesdelete;
	}
	public void setQuesdelete(Boolean quesdelete) {
		this.quesdelete = quesdelete;
	}
	public BigInteger getClientquestionanswerid() {
		return clientquestionanswerid;
	}
	public void setClientquestionanswerid(BigInteger clientquestionanswerid) {
		this.clientquestionanswerid = clientquestionanswerid;
	}
	public BigInteger getEngagementsid() {
		return engagementsid;
	}
	public void setEngagementsid(BigInteger engagementsid) {
		this.engagementsid = engagementsid;
	}
	public String getClientanswer() {
		return clientanswer;
	}
	public void setClientanswer(String clientanswer) {
		this.clientanswer = clientanswer;
	}
	public String getClientnote() {
		return clientnote;
	}
	public void setClientnote(String clientnote) {
		this.clientnote = clientnote;
	}
	public Boolean getAnsactive() {
		return ansactive;
	}
	public void setAnsactive(Boolean ansactive) {
		this.ansactive = ansactive;
	}
	public Boolean getAnsdelete() {
		return ansdelete;
	}
	public void setAnsdelete(Boolean ansdelete) {
		this.ansdelete = ansdelete;
	}
	
	
}
