package com.adinovis.domain;

import java.math.BigInteger;

public class Engagementtype {
  
	private BigInteger engagementtypeid;
	private String  engagementtype;
	
    
	public BigInteger getEngagementtypeid() {
		return engagementtypeid;
	}
	public String getEngagementtype() {
		return engagementtype;
	}

	public void setEngagementtypeid(BigInteger engagementtypeid) {
		this.engagementtypeid = engagementtypeid;
	}
	public void setEngagementtype(String engagementtype) {
		this.engagementtype = engagementtype;
	}
	
	
}
