package com.adinovis.domain;

import java.math.BigInteger;

public class MapSheet {

	private BigInteger mapsheetid;
	private String mapno;
	private String mapsheetname;
	private BigInteger fingroupid;
	private String fingroupname;
	private BigInteger finsubgrpid;
	private String finsubgroupname;
	private BigInteger finsubgroupchildId;
	private String finsubgroupchildname;
	private BigInteger leadsheetid;
	private String leadsheetname;
	private String leadsheetcode;
	private BigInteger balancetypeid;
	private String balancetypename;
	private BigInteger statementtypeid;
	private String statementtypename;
	private Integer gificode;
	private Boolean isdelete;
	private String Notes;

	public BigInteger getMapsheetid() {
		return mapsheetid;
	}

	public void setMapsheetid(BigInteger mapsheetid) {
		this.mapsheetid = mapsheetid;
	}

	public String getMapno() {
		return mapno;
	}

	public void setMapno(String mapno) {
		this.mapno = mapno;
	}

	public String getMapsheetname() {
		return mapsheetname;
	}

	public void setMapsheetname(String mapsheetname) {
		this.mapsheetname = mapsheetname;
	}

	public BigInteger getFingroupid() {
		return fingroupid;
	}

	public void setFingroupid(BigInteger fingroupid) {
		this.fingroupid = fingroupid;
	}

	public String getFingroupname() {
		return fingroupname;
	}

	public void setFingroupname(String fingroupname) {
		this.fingroupname = fingroupname;
	}

	public BigInteger getFinsubgrpid() {
		return finsubgrpid;
	}

	public void setFinsubgrpid(BigInteger finsubgrpid) {
		this.finsubgrpid = finsubgrpid;
	}

	public String getFinsubgroupname() {
		return finsubgroupname;
	}

	public void setFinsubgroupname(String finsubgroupname) {
		this.finsubgroupname = finsubgroupname;
	}

	public BigInteger getFinsubgroupchildId() {
		return finsubgroupchildId;
	}

	public void setFinsubgroupchildId(BigInteger finsubgroupchildId) {
		this.finsubgroupchildId = finsubgroupchildId;
	}

	public String getFinsubgroupchildname() {
		return finsubgroupchildname;
	}

	public void setFinsubgroupchildname(String finsubgroupchildname) {
		this.finsubgroupchildname = finsubgroupchildname;
	}

	public BigInteger getLeadsheetid() {
		return leadsheetid;
	}

	public void setLeadsheetid(BigInteger leadsheetid) {
		this.leadsheetid = leadsheetid;
	}

	public String getLeadsheetname() {
		return leadsheetname;
	}

	public void setLeadsheetname(String leadsheetname) {
		this.leadsheetname = leadsheetname;
	}

	public String getLeadsheetcode() {
		return leadsheetcode;
	}

	public void setLeadsheetcode(String leadsheetcode) {
		this.leadsheetcode = leadsheetcode;
	}

	public BigInteger getBalancetypeid() {
		return balancetypeid;
	}

	public void setBalancetypeid(BigInteger balancetypeid) {
		this.balancetypeid = balancetypeid;
	}

	public String getBalancetypename() {
		return balancetypename;
	}

	public void setBalancetypename(String balancetypename) {
		this.balancetypename = balancetypename;
	}

	public BigInteger getStatementtypeid() {
		return statementtypeid;
	}

	public void setStatementtypeid(BigInteger statementtypeid) {
		this.statementtypeid = statementtypeid;
	}

	public String getStatementtypename() {
		return statementtypename;
	}

	public void setStatementtypename(String statementtypename) {
		this.statementtypename = statementtypename;
	}

	public Integer getGificode() {
		return gificode;
	}

	public void setGificode(Integer gificode) {
		this.gificode = gificode;
	}

	public Boolean getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(Boolean isdelete) {
		this.isdelete = isdelete;
	}

	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

}
