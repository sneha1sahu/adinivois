package com.adinovis.domain;

public class Changepassword {

	private Integer puseraccountid;
	private String oldpassword;
	private String newpassword;
	public Integer getPuseraccountid() {
		return puseraccountid;
	}
	public String getNewpassword() {
		return newpassword;
	}
	
	public String getOldpassword() {
		return oldpassword;
	}
	public void setPuseraccountid(Integer puseraccountid) {
		this.puseraccountid = puseraccountid;
	}
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}
	
	
}
