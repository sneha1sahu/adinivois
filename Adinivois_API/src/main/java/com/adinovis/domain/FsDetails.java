package com.adinovis.domain;

import java.math.BigInteger;

public class FsDetails
{
	private BigInteger engagementsid;
	private String acctyear;
	private String fstype;
	private String fsid;
	private String currentname;
	private String newname;
	private Integer ploginid;
	public BigInteger getEngagementsid() {
		return engagementsid;
	}
	public void setEngagementsid(BigInteger engagementsid) {
		this.engagementsid = engagementsid;
	}
	public String getAcctyear() {
		return acctyear;
	}
	public void setAcctyear(String acctyear) {
		this.acctyear = acctyear;
	}
	public String getFstype() {
		return fstype;
	}
	public void setFstype(String fstype) {
		this.fstype = fstype;
	}
	public String getFsid() {
		return fsid;
	}
	public void setFsid(String fsid) {
		this.fsid = fsid;
	}
	public String getCurrentname() {
		return currentname;
	}
	public void setCurrentname(String currentname) {
		this.currentname = currentname;
	}
	public String getNewname() {
		return newname;
	}
	public void setNewname(String newname) {
		this.newname = newname;
	}
	public Integer getPloginid() {
		return ploginid;
	}
	public void setPloginid(Integer ploginid) {
		this.ploginid = ploginid;
	}
	
}
