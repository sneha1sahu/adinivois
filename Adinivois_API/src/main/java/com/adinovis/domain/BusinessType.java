package com.adinovis.domain;

public class BusinessType {
private Integer businessId;
private String businessType;
public Integer getBusinessId() {
	return businessId;
}
public void setBusinessId(Integer businessId) {
	this.businessId = businessId;
}
public String getBusinessType() {
	return businessType;
}
public void setBusinessType(String businessType) {
	this.businessType = businessType;
}

}
