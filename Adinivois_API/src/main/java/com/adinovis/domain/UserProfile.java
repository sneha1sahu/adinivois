package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserProfile {
private int uId;
  private String firstName;
  private String lastName;
  private Integer gender;
  private String mobileNo;
  private String dateOfBirth;
  private String street1;
  private String street2;
  private String city;
  private String state;
  private String zip;
  private String userImage;
public int getuId() {
	return uId;
}
public void setuId(int uId) {
	this.uId = uId;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public Integer getGender() {
	return gender;
}
public void setGender(Integer gender) {
	this.gender = gender;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
public String getDateOfBirth() {
	return dateOfBirth;
}
public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}
public String getStreet1() {
	return street1;
}
public void setStreet1(String street1) {
	this.street1 = street1;
}
public String getStreet2() {
	return street2;
}
public void setStreet2(String street2) {
	this.street2 = street2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getZip() {
	return zip;
}
public void setZip(String zip) {
	this.zip = zip;
}
public String getUserImage() {
	return userImage;
}
public void setUserImage(String userImage) {
	this.userImage = userImage;
}
  
  
  
 
}
