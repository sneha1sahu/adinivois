package com.adinovis.domain;

public class UserProfiles {
	private int uId;
	private String emailId;
  private String firstName;
  private String lastName;
  private String address;
  private String profile;
  private String gender;
  private String mobileNo;
  private String dateOfBirth;
  private String street1;
  private String street2;
  private String city;
  private String stateId;
  private String zip;
  private String dateOfJoining;
  private String referralCode;
  private String referralUrl;
  private String stateName;
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public String getAddress() {
    return address;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public void setAddress(String address) {
    this.address = address;
  }
public int getuId() {
	return uId;
}
public void setuId(int uId) {
	this.uId = uId;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getProfile() {
	return profile;
}
public void setProfile(String profile) {
	this.profile = profile;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
public String getDateOfBirth() {
	return dateOfBirth;
}
public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}
public String getStreet1() {
	return street1;
}
public void setStreet1(String street1) {
	this.street1 = street1;
}
public String getStreet2() {
	return street2;
}
public void setStreet2(String street2) {
	this.street2 = street2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}

public String getStateId() {
	return stateId;
}
public void setStateId(String stateId) {
	this.stateId = stateId;
}
public String getStateName() {
	return stateName;
}
public void setStateName(String stateName) {
	this.stateName = stateName;
}
public String getZip() {
	return zip;
}
public void setZip(String zip) {
	this.zip = zip;
}
public String getDateOfJoining() {
	return dateOfJoining;
}
public void setDateOfJoining(String dateOfJoining) {
	this.dateOfJoining = dateOfJoining;
}
public String getReferralCode() {
	return referralCode;
}
public void setReferralCode(String referralCode) {
	this.referralCode = referralCode;
}
public String getReferralUrl() {
	return referralUrl;
}
public void setReferralUrl(String referralUrl) {
	this.referralUrl = referralUrl;
}
  
  
 
}