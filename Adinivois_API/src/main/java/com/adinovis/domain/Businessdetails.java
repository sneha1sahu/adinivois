package com.adinovis.domain;

import java.math.BigInteger;
import java.util.Date;

public class Businessdetails {

	private String businessname;
	private BigInteger clientfirmid;
	private Date incorporationdate;
	private BigInteger typeofentityid;

	public BigInteger getTypeofentityid() {
		return typeofentityid;
	}

	public void setTypeofentityid(BigInteger typeofentityid) {
		this.typeofentityid = typeofentityid;
	}

	public String getBusinessname() {
		return businessname;
	}

	public BigInteger getClientfirmid() {
		return clientfirmid;
	}

	public Date getIncorporationdate() {
		return incorporationdate;
	}

	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}

	public void setClientfirmid(BigInteger clientfirmid) {
		this.clientfirmid = clientfirmid;
	}

	public void setIncorporationdate(Date incorporationdate) {
		this.incorporationdate = incorporationdate;
	}

}
