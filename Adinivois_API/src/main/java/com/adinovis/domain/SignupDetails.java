package com.adinovis.domain;

import java.math.BigInteger;

import org.codehaus.jackson.annotate.JsonAutoDetect;
@JsonAutoDetect
public class SignupDetails {

	
	
	 private String pfname;
	 private String plname;
	 private String pbsname;
	 private String pemailadd;
	 private String ppwd;
	 private String paddr;
	 private String pphonebtype;
	 private String pbphonenumber;
	 private String pphonectype;
	 private String pcphonenumber;
	 private BigInteger prole;
	 private String encPassword;
	 private String plicno;


	public String getPfname() {
		return pfname;
	}
	
	public String getPlname() {
		return plname;
	}

	public String getPbsname() {
		return pbsname;
	}

	public String getPemailadd() {
		return pemailadd;
	}

	public String getPpwd() {
		return ppwd;
	}

	public String getPaddr() {
		return paddr;
	}

	public String getPphonebtype() {
		return pphonebtype;
	}

	public String getPbphonenumber() {
		return pbphonenumber;
	}

	public String getPphonectype() {
		return pphonectype;
	}

	public String getPcphonenumber() {
		return pcphonenumber;
	}

	public BigInteger getProle() {
		return prole;
	}

	public String getEncPassword() {
		return encPassword;
	}

	public void setPfname(String pfname) {
		this.pfname = pfname;
	}

	public void setPlname(String plname) {
		this.plname = plname;
	}

	public void setPbsname(String pbsname) {
		this.pbsname = pbsname;
	}

	public void setPemailadd(String pemailadd) {
		this.pemailadd = pemailadd;
	}

	public void setPpwd(String ppwd) {
		this.ppwd = ppwd;
	}

	public void setPaddr(String paddr) {
		this.paddr = paddr;
	}

	public void setPphonebtype(String pphonebtype) {
		this.pphonebtype = pphonebtype;
	}

	public void setPbphonenumber(String pbphonenumber) {
		this.pbphonenumber = pbphonenumber;
	}

	public void setPphonectype(String pphonectype) {
		this.pphonectype = pphonectype;
	}

	public void setPcphonenumber(String pcphonenumber) {
		this.pcphonenumber = pcphonenumber;
	}

	public void setProle(BigInteger prole) {
		this.prole = prole;
	}

	public void setEncPassword(String encPassword) {
		this.encPassword = encPassword;
	}

	public String getPlicno() {
		return plicno;
	}

	public void setPlicno(String plicno) {
		this.plicno = plicno;
	}

	 
	 
	
	    
}
