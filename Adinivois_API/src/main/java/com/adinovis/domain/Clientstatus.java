package com.adinovis.domain;

import java.math.BigInteger;

public class Clientstatus {

	
	private BigInteger statusid;
	private String  statusname;
	public BigInteger getStatusid() {
		return statusid;
	}
	public String getStatusname() {
		return statusname;
	}
	public void setStatusid(BigInteger statusid) {
		this.statusid = statusid;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
	
	
}
