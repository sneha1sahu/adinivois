package com.adinovis.domain;



public class MailImages {

	private String type;
	private String name;
	private Object content;
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	public Object getContent() {
		return content;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	
	
}
