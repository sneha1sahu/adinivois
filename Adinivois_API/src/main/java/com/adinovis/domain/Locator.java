package com.adinovis.domain;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.adinovis.location.Location;

public class Locator {
	

	public static List<Suggestion> loadSuggetion(List<Suggestion> stores, CurrentLocation currentLocation)
			throws IOException {
		double defaultDistance = 0;
		if (currentLocation.getDistance() > 0)
			defaultDistance =currentLocation.getDistance();

		List<Suggestion> list = new ArrayList<Suggestion>();
		Location loc = new Location(currentLocation.getLatitude(), currentLocation.getLongitude());
		double distanceInMiles=0;
		for (Suggestion suggestion : stores) {
			distanceInMiles = calculateMiles(loc.distanceTo(suggestion));
			suggestion.setDistanceInMiles(distanceInMiles);
			
			if(defaultDistance >0){
				if(distanceInMiles<=defaultDistance)
				list.add(suggestion);
			}
			else 
				list.add(suggestion);
		}

		return list;
	}

	private static double calculateMiles(double meters) {
		double miles = (meters * 0.000621371);
		DecimalFormat df = new DecimalFormat("#.##");
		return Double.valueOf(df.format(miles));
	}
	
	
}
