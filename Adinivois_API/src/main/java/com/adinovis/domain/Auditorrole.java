package com.adinovis.domain;

import java.math.BigInteger;

public class Auditorrole {
	
	private BigInteger auditroleid;
	private Integer rowindex;
	private String rolename;
	private BigInteger userroleid;
	public BigInteger getAuditroleid() {
		return auditroleid;
	}
	public Integer getRowindex() {
		return rowindex;
	}
	public String getRolename() {
		return rolename;
	}
	public BigInteger getUserroleid() {
		return userroleid;
	}
	public void setAuditroleid(BigInteger auditroleid) {
		this.auditroleid = auditroleid;
	}
	public void setRowindex(Integer rowindex) {
		this.rowindex = rowindex;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public void setUserroleid(BigInteger userroleid) {
		this.userroleid = userroleid;
	}
	
	

}
