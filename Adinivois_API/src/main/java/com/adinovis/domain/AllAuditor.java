package com.adinovis.domain;

import java.math.BigInteger;

public class AllAuditor {
	
	private BigInteger useraccountid;
	private String fullname;
	private String emailaddress;
	private BigInteger useraccountroleid;
	private BigInteger userroleid;
	private String rolename;
	public BigInteger getUseraccountid() {
		return useraccountid;
	}
	public String getFullname() {
		return fullname;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public BigInteger getUseraccountroleid() {
		return useraccountroleid;
	}
	public BigInteger getUserroleid() {
		return userroleid;
	}
	public String getRolename() {
		return rolename;
	}
	public void setUseraccountid(BigInteger useraccountid) {
		this.useraccountid = useraccountid;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public void setUseraccountroleid(BigInteger useraccountroleid) {
		this.useraccountroleid = useraccountroleid;
	}
	public void setUserroleid(BigInteger userroleid) {
		this.userroleid = userroleid;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	

}
