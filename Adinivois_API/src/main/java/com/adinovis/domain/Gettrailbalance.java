package com.adinovis.domain;

import java.math.BigInteger;
import java.util.Date;

public class Gettrailbalance {

	private BigInteger trailbalanceid;
	private BigInteger engagementsid;
	private BigInteger clientdatasourceid;
	private BigInteger clientapidataid;
	private Date recieveddate;
	private String accountcode;
	private String accountname;
	private String accounttype;
	private String originalbalance;
	private BigInteger trailadjustmentid;
	private BigInteger adjustmenttypeid;
	private String adjustmenttypename;
	private String adjustmentamount;
	private String finalamount;
	private String PY1;
	private String changes;
	private String PY2;
	private BigInteger trailbalancemapingid;
	private BigInteger mapsheetid;
	private String mapno;
	private String mapsheetname;
	private BigInteger fingroupid;
	private String fingroupname;
	private BigInteger finsubgrpid;
	private String finsubgroupname;
	private BigInteger finsubgroupchild;
	private String finsubgroupchildname;
	private BigInteger leadsheetid;
	private String leadsheetname;
	private BigInteger balancetypeid;
	private String balancetypename;
	private BigInteger statementtypeid;
	private String statementtypename;
	private String Notes;
	private Integer gificode;
	private String fxtranslation;
	private String leadsheetcode;
	private Boolean isclientdatasource;
	private String mappingstatus;
	private BigInteger mappingstatusid;
	private String originalbalancetotal;
	private String adjustmentamounttotal;
	private String finalamounttotal;
	private String PY1total;
	private String PY2total;
	private String originalred;
	private String finalred;

	public BigInteger getMappingstatusid() {
		return mappingstatusid;
	}

	public void setMappingstatusid(BigInteger mappingstatusid) {
		this.mappingstatusid = mappingstatusid;
	}

	public BigInteger getTrailbalanceid() {
		return trailbalanceid;
	}

	public BigInteger getEngagementsid() {
		return engagementsid;
	}

	public BigInteger getClientdatasourceid() {
		return clientdatasourceid;
	}

	public BigInteger getClientapidataid() {
		return clientapidataid;
	}

	public Date getRecieveddate() {
		return recieveddate;
	}

	public String getAccountcode() {
		return accountcode;
	}

	public String getAccountname() {
		return accountname;
	}

	public String getAccounttype() {
		return accounttype;
	}

	public String getOriginalbalance() {
		return originalbalance;
	}

	public BigInteger getTrailadjustmentid() {
		return trailadjustmentid;
	}

	public BigInteger getAdjustmenttypeid() {
		return adjustmenttypeid;
	}

	public String getAdjustmenttypename() {
		return adjustmenttypename;
	}

	public String getAdjustmentamount() {
		return adjustmentamount;
	}

	public String getFinalamount() {
		return finalamount;
	}

	public String getPY1() {
		return PY1;
	}

	public String getChanges() {
		return changes;
	}

	public String getPY2() {
		return PY2;
	}

	public BigInteger getTrailbalancemapingid() {
		return trailbalancemapingid;
	}

	public BigInteger getMapsheetid() {
		return mapsheetid;
	}

	public String getMapno() {
		return mapno;
	}

	public String getMapsheetname() {
		return mapsheetname;
	}

	public BigInteger getFingroupid() {
		return fingroupid;
	}

	public String getFingroupname() {
		return fingroupname;
	}

	public BigInteger getFinsubgrpid() {
		return finsubgrpid;
	}

	public String getFinsubgroupname() {
		return finsubgroupname;
	}

	public BigInteger getFinsubgroupchild() {
		return finsubgroupchild;
	}

	public String getFinsubgroupchildname() {
		return finsubgroupchildname;
	}

	public BigInteger getLeadsheetid() {
		return leadsheetid;
	}

	public String getLeadsheetname() {
		return leadsheetname;
	}

	public BigInteger getBalancetypeid() {
		return balancetypeid;
	}

	public String getBalancetypename() {
		return balancetypename;
	}

	public BigInteger getStatementtypeid() {
		return statementtypeid;
	}

	public String getStatementtypename() {
		return statementtypename;
	}

	public String getNotes() {
		return Notes;
	}

	public Integer getGificode() {
		return gificode;
	}

	public String getLeadsheetcode() {
		return leadsheetcode;
	}

	public Boolean getIsclientdatasource() {
		return isclientdatasource;
	}

	public String getMappingstatus() {
		return mappingstatus;
	}

	public void setTrailbalanceid(BigInteger trailbalanceid) {
		this.trailbalanceid = trailbalanceid;
	}

	public void setEngagementsid(BigInteger engagementsid) {
		this.engagementsid = engagementsid;
	}

	public void setClientdatasourceid(BigInteger clientdatasourceid) {
		this.clientdatasourceid = clientdatasourceid;
	}

	public void setClientapidataid(BigInteger clientapidataid) {
		this.clientapidataid = clientapidataid;
	}

	public void setRecieveddate(Date recieveddate) {
		this.recieveddate = recieveddate;
	}

	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	public void setOriginalbalance(String originalbalance) {
		this.originalbalance = originalbalance;
	}

	public void setTrailadjustmentid(BigInteger trailadjustmentid) {
		this.trailadjustmentid = trailadjustmentid;
	}

	public void setAdjustmenttypeid(BigInteger adjustmenttypeid) {
		this.adjustmenttypeid = adjustmenttypeid;
	}

	public void setAdjustmenttypename(String adjustmenttypename) {
		this.adjustmenttypename = adjustmenttypename;
	}

	public void setAdjustmentamount(String adjustmentamount) {
		this.adjustmentamount = adjustmentamount;
	}

	public void setFinalamount(String finalamount) {
		this.finalamount = finalamount;
	}

	public void setPY1(String pY1) {
		PY1 = pY1;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}

	public void setPY2(String pY2) {
		PY2 = pY2;
	}

	public void setTrailbalancemapingid(BigInteger trailbalancemapingid) {
		this.trailbalancemapingid = trailbalancemapingid;
	}

	public void setMapsheetid(BigInteger mapsheetid) {
		this.mapsheetid = mapsheetid;
	}

	public void setMapno(String mapno) {
		this.mapno = mapno;
	}

	public void setMapsheetname(String mapsheetname) {
		this.mapsheetname = mapsheetname;
	}

	public void setFingroupid(BigInteger fingroupid) {
		this.fingroupid = fingroupid;
	}

	public void setFingroupname(String fingroupname) {
		this.fingroupname = fingroupname;
	}

	public void setFinsubgrpid(BigInteger finsubgrpid) {
		this.finsubgrpid = finsubgrpid;
	}

	public void setFinsubgroupname(String finsubgroupname) {
		this.finsubgroupname = finsubgroupname;
	}

	public void setFinsubgroupchild(BigInteger finsubgroupchild) {
		this.finsubgroupchild = finsubgroupchild;
	}

	public void setFinsubgroupchildname(String finsubgroupchildname) {
		this.finsubgroupchildname = finsubgroupchildname;
	}

	public void setLeadsheetid(BigInteger leadsheetid) {
		this.leadsheetid = leadsheetid;
	}

	public void setLeadsheetname(String leadsheetname) {
		this.leadsheetname = leadsheetname;
	}

	public void setBalancetypeid(BigInteger balancetypeid) {
		this.balancetypeid = balancetypeid;
	}

	public void setBalancetypename(String balancetypename) {
		this.balancetypename = balancetypename;
	}

	public void setStatementtypeid(BigInteger statementtypeid) {
		this.statementtypeid = statementtypeid;
	}

	public void setStatementtypename(String statementtypename) {
		this.statementtypename = statementtypename;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public void setGificode(Integer gificode) {
		this.gificode = gificode;
	}

	public void setLeadsheetcode(String leadsheetcode) {
		this.leadsheetcode = leadsheetcode;
	}

	public void setIsclientdatasource(Boolean isclientdatasource) {
		this.isclientdatasource = isclientdatasource;
	}

	public void setMappingstatus(String mappingstatus) {
		this.mappingstatus = mappingstatus;
	}

	public String getFxtranslation() {
		return fxtranslation;
	}

	public void setFxtranslation(String fxtranslation) {
		this.fxtranslation = fxtranslation;
	}

	public String getOriginalbalancetotal() {
		return originalbalancetotal;
	}

	public void setOriginalbalancetotal(String originalbalancetotal) {
		this.originalbalancetotal = originalbalancetotal;
	}

	public String getAdjustmentamounttotal() {
		return adjustmentamounttotal;
	}

	public void setAdjustmentamounttotal(String adjustmentamounttotal) {
		this.adjustmentamounttotal = adjustmentamounttotal;
	}

	public String getFinalamounttotal() {
		return finalamounttotal;
	}

	public void setFinalamounttotal(String finalamounttotal) {
		this.finalamounttotal = finalamounttotal;
	}

	public String getPY1total() {
		return PY1total;
	}

	public void setPY1total(String pY1total) {
		PY1total = pY1total;
	}

	public String getPY2total() {
		return PY2total;
	}

	public void setPY2total(String pY2total) {
		PY2total = pY2total;
	}

	public String getOriginalred() {
		return originalred;
	}

	public void setOriginalred(String originalred) {
		this.originalred = originalred;
	}

	public String getFinalred() {
		return finalred;
	}

	public void setFinalred(String finalred) {
		this.finalred = finalred;
	}
	
}
