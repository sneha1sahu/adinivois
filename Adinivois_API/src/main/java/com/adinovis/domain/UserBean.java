package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserBean {
  private String loginId;
  private String email;
  private String password;
  private Integer uid;
  private String firstName;
  private String lastName;
  private String deviceToken;
  private Integer osType;
  private Integer userType;
  private String referralCode;
  private String referralQr;
  private String profileUrl;
  private String businessName;
  private String shortDesc;
  private String longDesc;
  private Integer loginType;
  private String joiningDate;
  private String memberShipType;
  
  @JsonIgnore
  private int 	acocuntId;
  public String getLoginId() {
    return loginId;
  }
  public String getPassword() {
    return password;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
  public void setPassword(String password) {
    this.password = password;
  }
public Integer getUid() {
	return uid;
}
public void setUid(Integer uid) {
	this.uid = uid;
}
public String getFirstName() {
  return firstName;
}
public String getLastName() {
  return lastName;
}
public void setFirstName(String firstName) {
  this.firstName = firstName;
}
public void setLastName(String lastName) {
  this.lastName = lastName;
}
public String getDeviceToken() {
  return deviceToken;
}

public void setDeviceToken(String deviceToken) {
  this.deviceToken = deviceToken;
}
public Integer getOsType() {
  return osType;
}
public void setOsType(Integer osType) {
  this.osType = osType;
}
public Integer getUserType() {
	return userType;
}
public void setUserType(Integer userType) {
	this.userType = userType;
}
public String getReferralCode() {
	return referralCode;
}
public void setReferralCode(String referralCode) {
	this.referralCode = referralCode;
}
public String getReferralQr() {
	return referralQr;
}
public void setReferralQr(String referralQr) {
	this.referralQr = referralQr;
}
public String getProfileUrl() {
	return profileUrl;
}
public void setProfileUrl(String profileUrl) {
	this.profileUrl = profileUrl;
}
public String getShortDesc() {
	return shortDesc;
}
public void setShortDesc(String shortDesc) {
	this.shortDesc = shortDesc;
}
public String getLongDesc() {
	return longDesc;
}
public void setLongDesc(String longDesc) {
	this.longDesc = longDesc;
}
public int getAcocuntId() {
	return acocuntId;
}
public void setAcocuntId(int acocuntId) {
	this.acocuntId = acocuntId;
}
public String getBusinessName() {
	return businessName;
}
public void setBusinessName(String businessName) {
	this.businessName = businessName;
}
public Integer getLoginType() {
	return loginType;
}
public void setLoginType(Integer loginType) {
	this.loginType = loginType;
}
public String getJoiningDate() {
	return joiningDate;
}
public void setJoiningDate(String joiningDate) {
	this.joiningDate = joiningDate;
}
public String getMemberShipType() {
	return memberShipType;
}
public void setMemberShipType(String memberShipType) {
	this.memberShipType = memberShipType;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

 
  
  

}