package com.adinovis.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

public class Clientprofiles {

	private BigInteger useraccountid;
	private BigInteger clientfirmid;
	private String businessname;
	private BigInteger engagement;
	private BigInteger statusid;
	private String statusname;
	private String sourcelink;
	private String contactperson;
	private String typeofentry;
	private Date clientonboarding;
	private String tokenvalue;
	private String cellphonenumber;
	private String businessphonenumber;
	private String emailaddress;
	private BigInteger jurisdictionid;
	private String provincename;
	private String provincescode;
	private String statename;
	private BigInteger typeofentotyid;
	private String typeofentityname;
	private String typeofentitycode;
	private Date incorportiondate;
	private String clientid;
	private String secretkey;
	private Date modifieddate;
	private BigInteger isowned;
	
	public Date getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public BigInteger getIsowned() {
		return isowned;
	}

	public void setIsowned(BigInteger isowned) {
		this.isowned = isowned;
	}

	public String getBusinessname() {
		return businessname;
	}

	public BigInteger getClientfirmid() {
		return clientfirmid;
	}

	public BigInteger getEngagement() {
		return engagement;
	}

	public BigInteger getStatusid() {
		return statusid;
	}

	public String getStatusname() {
		return statusname;
	}
	public String getSourcelink() {
		return sourcelink;
	}
	public String getContactperson() {
		return contactperson;
	}
	public String getTypeofentry() {
		return typeofentry;
	}
	public Date getClientonboarding() {
		return clientonboarding;
	}
	
	public BigInteger getUseraccountid() {
		return useraccountid;
	}

	
	public String getTokenvalue() {
		return tokenvalue;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public BigInteger getJurisdictionid() {
		return jurisdictionid;
	}

	public String getProvincename() {
		return provincename;
	}

	public String getProvincescode() {
		return provincescode;
	}

	public String getStatename() {
		return statename;
	}

	public BigInteger getTypeofentotyid() {
		return typeofentotyid;
	}

	public String getTypeofentityname() {
		return typeofentityname;
	}

	public Date getIncorportiondate() {
		return incorportiondate;
	}

	public String getClientid() {
		return clientid;
	}

	public String getSecretkey() {
		return secretkey;
	}

	public String getCellphonenumber() {
		return cellphonenumber;
	}

	public String getTypeofentitycode() {
		return typeofentitycode;
	}

	public String getBusinessphonenumber() {
		return businessphonenumber;
	}

	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}

	public void setClientfirmid(BigInteger clientfirmid) {
		this.clientfirmid = clientfirmid;
	}

	public void setEngagement(BigInteger engagement) {
		this.engagement = engagement;
	}

	public void setStatusid(BigInteger statusid) {
		this.statusid = statusid;
	}

	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
	public void setSourcelink(String sourcelink) {
		this.sourcelink = sourcelink;
	}
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}
	public void setTypeofentry(String typeofentry) {
		this.typeofentry = typeofentry;
	}
	public void setClientonboarding(Date clientonboarding) {
		this.clientonboarding = clientonboarding;
	}

	public void setUseraccountid(BigInteger useraccountid) {
		this.useraccountid = useraccountid;
	}

	public void setTokenvalue(String tokenvalue) {
		this.tokenvalue = tokenvalue;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public void setJurisdictionid(BigInteger jurisdictionid) {
		this.jurisdictionid = jurisdictionid;
	}

	public void setProvincename(String provincename) {
		this.provincename = provincename;
	}

	public void setProvincescode(String provincescode) {
		this.provincescode = provincescode;
	}

	public void setStatename(String statename) {
		this.statename = statename;
	}

	public void setTypeofentotyid(BigInteger typeofentotyid) {
		this.typeofentotyid = typeofentotyid;
	}

	public void setTypeofentityname(String typeofentityname) {
		this.typeofentityname = typeofentityname;
	}

	public void setIncorportiondate(Date incorportiondate) {
		this.incorportiondate = incorportiondate;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public void setSecretkey(String secretkey) {
		this.secretkey = secretkey;
	}

	public void setCellphonenumber(String cellphonenumber) {
		this.cellphonenumber = cellphonenumber;
	}

	public void setTypeofentitycode(String typeofentitycode) {
		this.typeofentitycode = typeofentitycode;
	}

	public void setBusinessphonenumber(String businessphonenumber) {
		this.businessphonenumber = businessphonenumber;
	}
   
}
