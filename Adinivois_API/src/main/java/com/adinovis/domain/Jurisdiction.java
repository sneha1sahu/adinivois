package com.adinovis.domain;

import java.math.BigInteger;

public class Jurisdiction {
	
	private BigInteger jurisdictionid;
	private String provincesname;
	private String provincescode;
	private String statename;
	public BigInteger getJurisdictionid() {
		return jurisdictionid;
	}
	public String getProvincesname() {
		return provincesname;
	}
	public String getProvincescode() {
		return provincescode;
	}
	public String getStatename() {
		return statename;
	}
	public void setJurisdictionid(BigInteger jurisdictionid) {
		this.jurisdictionid = jurisdictionid;
	}
	public void setProvincesname(String provincesname) {
		this.provincesname = provincesname;
	}
	public void setProvincescode(String provincescode) {
		this.provincescode = provincescode;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	

}
