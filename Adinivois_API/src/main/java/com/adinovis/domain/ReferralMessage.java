package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReferralMessage {
	private int messageId;
private String messageKey;
private String longmsg;
private String shortmsg;
public int getMessageId() {
	return messageId;
}
public void setMessageId(int messageId) {
	this.messageId = messageId;
}
public String getMessageKey() {
	return messageKey;
}
public void setMessageKey(String messageKey) {
	this.messageKey = messageKey;
}
public String getLongmsg() {
	return longmsg;
}
public void setLongmsg(String longmsg) {
	this.longmsg = longmsg;
}
public String getShortmsg() {
	return shortmsg;
}
public void setShortmsg(String shortmsg) {
	this.shortmsg = shortmsg;
}

}
