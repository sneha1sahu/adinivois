package com.adinovis.domain;

import java.math.BigInteger;

public class ClientData {

	private BigInteger clientFirmId;
	private BigInteger userAccountId;
	private BigInteger clientDataSourceId;
	private String clientId;
	private String clientSecret;
	private String startDate;
	private String endDate;
	private BigInteger accountYear;
	private String sourceLink;
	private String realmId;
	private String refreshToken;
	private String accessToken;
	private BigInteger engagementId;

	public BigInteger getClientFirmId() {
		return clientFirmId;
	}

	public void setClientFirmId(BigInteger clientFirmId) {
		this.clientFirmId = clientFirmId;
	}

	public BigInteger getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(BigInteger userAccountId) {
		this.userAccountId = userAccountId;
	}

	public BigInteger getClientDataSourceId() {
		return clientDataSourceId;
	}

	public void setClientDataSourceId(BigInteger clientDataSourceId) {
		this.clientDataSourceId = clientDataSourceId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public BigInteger getAccountYear() {
		return accountYear;
	}

	public void setAccountYear(BigInteger accountYear) {
		this.accountYear = accountYear;
	}

	public String getSourceLink() {
		return sourceLink;
	}

	public void setSourceLink(String sourceLink) {
		this.sourceLink = sourceLink;
	}

	public String getRealmId() {
		return realmId;
	}

	public void setRealmId(String realmId) {
		this.realmId = realmId;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public BigInteger getEngagementId() {
		return engagementId;
	}

	public void setEngagementId(BigInteger engagementId) {
		this.engagementId = engagementId;
	}

}
