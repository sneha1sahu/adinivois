package com.adinovis.domain;

import java.math.BigInteger;

public class ClientProfile {
	
	private BigInteger pclientfirmid;
	private String pbsname;
	private String pcontactperson;
	private String pphonebtype;
	private String pbphonenumber;
	private String pphonectype;
	private String pcphonenumber;
	private String pemailadd;
	private String ppwd;
	private Integer pjurisdictionid;
	private Integer ptypeofentityid ;
	private String pincorporationdate;
	private String paccountbook;
	private String paccesskey;
	private String psecretkey;
	private BigInteger prole;
	private Integer ploginid; 
	
	public BigInteger getPclientfirmid() {
		return pclientfirmid;
	}
	public String getPcontactperson() {
		return pcontactperson;
	}
	public String getPphonebtype() {
		return pphonebtype;
	}
	public String getPbphonenumber() {
		return pbphonenumber;
	}
	public String getPphonectype() {
		return pphonectype;
	}
	public String getPcphonenumber() {
		return pcphonenumber;
	}
	public String getPemailadd() {
		return pemailadd;
	}
	public String getPpwd() {
		return ppwd;
	}

	public Integer getPjurisdictionid() {
		return pjurisdictionid;
	}
	public Integer getPtypeofentityid() {
		return ptypeofentityid;
	}
	public String getPincorporationdate() {
		return pincorporationdate;
	}
	public String getPaccountbook() {
		return paccountbook;
	}
	public String getPaccesskey() {
		return paccesskey;
	}
	public String getPsecretkey() {
		return psecretkey;
	}
	public BigInteger getProle() {
		return prole;
	}
	

	public String getPbsname() {
		return pbsname;
	}
	
	public Integer getPloginid() {
		return ploginid;
	}
	public void setPcontactperson(String pcontactperson) {
		this.pcontactperson = pcontactperson;
	}
	public void setPphonebtype(String pphonebtype) {
		this.pphonebtype = pphonebtype;
	}
	public void setPbphonenumber(String pbphonenumber) {
		this.pbphonenumber = pbphonenumber;
	}
	public void setPphonectype(String pphonectype) {
		this.pphonectype = pphonectype;
	}
	public void setPcphonenumber(String pcphonenumber) {
		this.pcphonenumber = pcphonenumber;
	}
	public void setPemailadd(String pemailadd) {
		this.pemailadd = pemailadd;
	}
	public void setPpwd(String ppwd) {
		this.ppwd = ppwd;
	}

	public void setPjurisdictionid(Integer pjurisdictionid) {
		this.pjurisdictionid = pjurisdictionid;
	}
	public void setPtypeofentityid(Integer ptypeofentityid) {
		this.ptypeofentityid = ptypeofentityid;
	}
	public void setPincorporationdate(String pincorporationdate) {
		this.pincorporationdate = pincorporationdate;
	}
	public void setPaccountbook(String paccountbook) {
		this.paccountbook = paccountbook;
	}
	public void setPaccesskey(String paccesskey) {
		this.paccesskey = paccesskey;
	}
	public void setPsecretkey(String psecretkey) {
		this.psecretkey = psecretkey;
	}
	public void setProle(BigInteger prole) {
		this.prole = prole;
	}
	public void setPbsname(String pbsname) {
		this.pbsname = pbsname;
	}
	public void setPclientfirmid(BigInteger pclientfirmid) {
		this.pclientfirmid = pclientfirmid;
	}
	public void setPloginid(Integer ploginid) {
		this.ploginid = ploginid;
	}


	
	

}



