package com.adinovis.domain;

public class CountDetails {
	private String transaction;
	
	private String totalPoints;
	private String notification;
	private String refundRequest;
	private String referrals;
	private String fundingRequest;
	private String balance;
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getTotalPoints() {
		return totalPoints;
	}
	public void setTotalPoints(String totalPoints) {
		this.totalPoints = totalPoints;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getRefundRequest() {
		return refundRequest;
	}
	public void setRefundRequest(String refundRequest) {
		this.refundRequest = refundRequest;
	}
	public String getReferrals() {
		return referrals;
	}
	public void setReferrals(String referrals) {
		this.referrals = referrals;
	}
	public String getFundingRequest() {
		return fundingRequest;
	}
	public void setFundingRequest(String fundingRequest) {
		this.fundingRequest = fundingRequest;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
}
