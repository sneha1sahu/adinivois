package com.adinovis.domain;

public class AddRating {
	private String ratingId;
	private String transactionId;
	private String businessAccountId;
	private String comment;
	private String consumerAccountId;
	private String ratingLevel;
	private String dateofRating;
	public String getRatingId() {
		return ratingId;
	}
	public void setRatingId(String ratingId) {
		this.ratingId = ratingId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getBusinessAccountId() {
		return businessAccountId;
	}
	public void setBusinessAccountId(String businessAccountId) {
		this.businessAccountId = businessAccountId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getConsumerAccountId() {
		return consumerAccountId;
	}
	public void setConsumerAccountId(String consumerAccountId) {
		this.consumerAccountId = consumerAccountId;
	}
	public String getRatingLevel() {
		return ratingLevel;
	}
	public void setRatingLevel(String ratingLevel) {
		this.ratingLevel = ratingLevel;
	}
	public String getDateofRating() {
		return dateofRating;
	}
	public void setDateofRating(String dateofRating) {
		this.dateofRating = dateofRating;
	}
	

}
