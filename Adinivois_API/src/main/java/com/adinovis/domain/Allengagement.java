package com.adinovis.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

public class Allengagement {

	private BigInteger engagementsid;
	private BigInteger clientfirmid;
	private BigInteger useraccountid;
	private String businessname;
	private String contactperson;
	private Date incorporationdate;
	private String engagementname;
	private String subenties;
	private BigInteger engagementtypeid;
	private String engagementtype;
	private String compilationtype;
	private Date finanicalyearenddate;
	private String additionalinfo;
	private BigInteger statusid;
	private BigInteger tbloadstatusid;
	private String statusname;
	private Date engcreateddate;
	private String auditor_list;
	private Date modifieddate;
	private BigInteger isowned;
	
	public Date getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public BigInteger getIsowned() {
		return isowned;
	}

	public void setIsowned(BigInteger isowned) {
		this.isowned = isowned;
	}

	public BigInteger getTbloadstatusid() {
		return tbloadstatusid;
	}

	public void setTbloadstatusid(BigInteger tbloadstatusid) {
		this.tbloadstatusid = tbloadstatusid;
	}

	public BigInteger getEngagementsid() {
		return engagementsid;
	}

	public BigInteger getClientfirmid() {
		return clientfirmid;
	}

	public BigInteger getUseraccountid() {
		return useraccountid;
	}

	public String getBusinessname() {
		return businessname;
	}

	public String getContactperson() {
		return contactperson;
	}

	public Date getIncorporationdate() {
		return incorporationdate;
	}

	public String getEngagementname() {
		return engagementname;
	}

	public String getSubenties() {
		return subenties;
	}

	public BigInteger getEngagementtypeid() {
		return engagementtypeid;
	}

	public String getEngagementtype() {
		return engagementtype;
	}

	public String getCompilationtype() {
		return compilationtype;
	}

	public Date getFinanicalyearenddate() {
		return finanicalyearenddate;
	}

	public String getAdditionalinfo() {
		return additionalinfo;
	}

	public BigInteger getStatusid() {
		return statusid;
	}

	public String getStatusname() {
		return statusname;
	}

	public Date getEngcreateddate() {
		return engcreateddate;
	}

	public String getAuditor_list() {
		return auditor_list;
	}

	public void setEngagementsid(BigInteger engagementsid) {
		this.engagementsid = engagementsid;
	}

	public void setClientfirmid(BigInteger clientfirmid) {
		this.clientfirmid = clientfirmid;
	}

	public void setUseraccountid(BigInteger useraccountid) {
		this.useraccountid = useraccountid;
	}

	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}

	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}

	public void setIncorporationdate(Date incorporationdate) {
		this.incorporationdate = incorporationdate;
	}

	public void setEngagementname(String engagementname) {
		this.engagementname = engagementname;
	}

	public void setSubenties(String subenties) {
		this.subenties = subenties;
	}

	public void setEngagementtypeid(BigInteger engagementtypeid) {
		this.engagementtypeid = engagementtypeid;
	}

	public void setEngagementtype(String engagementtype) {
		this.engagementtype = engagementtype;
	}

	public void setCompilationtype(String compilationtype) {
		this.compilationtype = compilationtype;
	}

	public void setFinanicalyearenddate(Date finanicalyearenddate) {
		this.finanicalyearenddate = finanicalyearenddate;
	}

	public void setAdditionalinfo(String additionalinfo) {
		this.additionalinfo = additionalinfo;
	}

	public void setStatusid(BigInteger statusid) {
		this.statusid = statusid;
	}

	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}

	public void setEngcreateddate(Date engcreateddate) {
		this.engcreateddate = engcreateddate;
	}

	public void setAuditor_list(String auditor_list) {
		this.auditor_list = auditor_list;
	}

}
