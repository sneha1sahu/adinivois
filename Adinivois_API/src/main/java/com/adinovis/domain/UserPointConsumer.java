package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserPointConsumer {
private String pSkyPoints;
private String pSkyPurchasePoints;
private String transAmountGrandTotal;
private String cashpointscommulative;
private String lifeTimePoints;
private String updatedTime;
private String skyReferralPoints;
private String cashPoints;
private String redeemPoints;
private String pendingTransaction;
public String getpSkyPoints() {
	return pSkyPoints;
}
public void setpSkyPoints(String pSkyPoints) {
	this.pSkyPoints = pSkyPoints;
}
public String getpSkyPurchasePoints() {
	return pSkyPurchasePoints;
}
public void setpSkyPurchasePoints(String pSkyPurchasePoints) {
	this.pSkyPurchasePoints = pSkyPurchasePoints;
}
public String getTransAmountGrandTotal() {
	return transAmountGrandTotal;
}
public void setTransAmountGrandTotal(String transAmountGrandTotal) {
	this.transAmountGrandTotal = transAmountGrandTotal;
}
public String getCashpointscommulative() {
	return cashpointscommulative;
}
public void setCashpointscommulative(String cashpointscommulative) {
	this.cashpointscommulative = cashpointscommulative;
}
public String getLifeTimePoints() {
	return lifeTimePoints;
}
public void setLifeTimePoints(String lifeTimePoints) {
	this.lifeTimePoints = lifeTimePoints;
}
public String getUpdatedTime() {
	return updatedTime;
}
public void setUpdatedTime(String updatedTime) {
	this.updatedTime = updatedTime;
}
public String getSkyReferralPoints() {
	return skyReferralPoints;
}
public void setSkyReferralPoints(String skyReferralPoints) {
	this.skyReferralPoints = skyReferralPoints;
}
public String getCashPoints() {
	return cashPoints;
}
public void setCashPoints(String cashPoints) {
	this.cashPoints = cashPoints;
}
public String getRedeemPoints() {
	return redeemPoints;
}
public void setRedeemPoints(String redeemPoints) {
	this.redeemPoints = redeemPoints;
}
public String getPendingTransaction() {
	return pendingTransaction;
}
public void setPendingTransaction(String pendingTransaction) {
	this.pendingTransaction = pendingTransaction;
}

}
