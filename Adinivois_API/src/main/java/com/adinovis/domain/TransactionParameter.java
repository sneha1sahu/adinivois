package com.adinovis.domain;

public class TransactionParameter {
	private int transactionId;
	private int consumerId;
	private int businessId;
	private int transType;
	private int transStatus;
	private int fundingStatus;
	private boolean isRefundRequested;
	private boolean isRefund;
	private boolean isRated;
	private int isDispute;
	private int limit;
	private int offset;
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public int getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(int consumerId) {
		this.consumerId = consumerId;
	}
	public int getBusinessId() {
		return businessId;
	}
	public void setBusinessId(int businessId) {
		this.businessId = businessId;
	}
	public int getTransType() {
		return transType;
	}
	public void setTransType(int transType) {
		this.transType = transType;
	}
	public int getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(int transStatus) {
		this.transStatus = transStatus;
	}
	public int getFundingStatus() {
		return fundingStatus;
	}
	public void setFundingStatus(int fundingStatus) {
		this.fundingStatus = fundingStatus;
	}
	public boolean isRefundRequested() {
		return isRefundRequested;
	}
	public void setRefundRequested(boolean isRefundRequested) {
		this.isRefundRequested = isRefundRequested;
	}
	public boolean isRefund() {
		return isRefund;
	}
	public void setRefund(boolean isRefund) {
		this.isRefund = isRefund;
	}
	public boolean isRated() {
		return isRated;
	}
	public void setRated(boolean isRated) {
		this.isRated = isRated;
	}
	public int getIsDispute() {
		return isDispute;
	}
	public void setIsDispute(int isDispute) {
		this.isDispute = isDispute;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	
}
