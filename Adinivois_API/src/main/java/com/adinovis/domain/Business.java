package com.adinovis.domain;

public class Business {
private Integer businessId;
private String businessName;
private String address;
public Integer getBusinessId() {
	return businessId;
}
public void setBusinessId(Integer businessId) {
	this.businessId = businessId;
}
public String getBusinessName() {
	return businessName;
}
public void setBusinessName(String businessName) {
	this.businessName = businessName;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
}
