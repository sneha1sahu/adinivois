package com.adinovis.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserNotification {
private String notificationId;
private Integer notificationType;
private Integer notificationAccountId;
private Integer counterPartyAccountId;
private String relatedItem;
private String notificationText;
private String isCleared;
private String isAaccessed;
private String createdDate;
public String getNotificationId() {
	return notificationId;
}
public void setNotificationId(String notificationId) {
	this.notificationId = notificationId;
}
public Integer getNotificationType() {
	return notificationType;
}
public void setNotificationType(Integer notificationType) {
	this.notificationType = notificationType;
}
public Integer getNotificationAccountId() {
	return notificationAccountId;
}
public void setNotificationAccountId(Integer notificationAccountId) {
	this.notificationAccountId = notificationAccountId;
}
public Integer getCounterPartyAccountId() {
	return counterPartyAccountId;
}
public void setCounterPartyAccountId(Integer counterPartyAccountId) {
	this.counterPartyAccountId = counterPartyAccountId;
}
public String getRelatedItem() {
	return relatedItem;
}
public void setRelatedItem(String relatedItem) {
	this.relatedItem = relatedItem;
}
public String getNotificationText() {
	return notificationText;
}
public void setNotificationText(String notificationText) {
	this.notificationText = notificationText;
}
public String getIsCleared() {
	return isCleared;
}
public void setIsCleared(String isCleared) {
	this.isCleared = isCleared;
}
public String getIsAaccessed() {
	return isAaccessed;
}
public void setIsAaccessed(String isAaccessed) {
	this.isAaccessed = isAaccessed;
}
public String getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
}



}
