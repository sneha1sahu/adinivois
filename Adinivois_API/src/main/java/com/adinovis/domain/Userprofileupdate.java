package com.adinovis.domain;

import java.math.BigInteger;

public class Userprofileupdate {

	private BigInteger 	useraccountid;
    private String firstname;
    private String lastname;
    private String pcphonenumber;
    private String pbphonenumber;
    private String memberlicenseno;
    private String businessname;
    private String emailaddress;
    private String address;
    private String profilepict;
	public BigInteger getUseraccountid() {
		return useraccountid;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getPcphonenumber() {
		return pcphonenumber;
	}
	public String getPbphonenumber() {
		return pbphonenumber;
	}
	public String getMemberlicenseno() {
		return memberlicenseno;
	}
	public String getBusinessname() {
		return businessname;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public String getAddress() {
		return address;
	}
	public String getProfilepict() {
		return profilepict;
	}
	public void setUseraccountid(BigInteger useraccountid) {
		this.useraccountid = useraccountid;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setPcphonenumber(String pcphonenumber) {
		this.pcphonenumber = pcphonenumber;
	}
	public void setPbphonenumber(String pbphonenumber) {
		this.pbphonenumber = pbphonenumber;
	}
	public void setMemberlicenseno(String memberlicenseno) {
		this.memberlicenseno = memberlicenseno;
	}
	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setProfilepict(String profilepict) {
		this.profilepict = profilepict;
	}
    
}
