package com.adinovis.amazon;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@Component
public class AmazonS3Gateway {
	private static final Logger LOG = LoggerFactory.getLogger(AmazonS3Gateway.class);

	@Autowired
	@Qualifier("amazonS3Client")
	private AmazonS3 amazonS3client;

	@Autowired
	@Qualifier("baseUrl")
	private String baseUrl;
	
	private static final String FILE_SEPERATOR= "/";
	
	@Autowired
	@Qualifier("clientDocsBucketName")
	private String clientBucketName;
	
	@Autowired
	@Qualifier("profileImageBucketName")
	private String profileImageBucketName;

	public AmazonS3Gateway() {

	}
	
	public String getBucketNameByStatus(Integer status)
	{/*
		 * if(status==0) return clientBucketName; else return profileImageBucketName;
		 */
		//String bucketname=
		return (status==0)?clientBucketName:profileImageBucketName;
	}
	public boolean saveS3Object(Integer status,String fileNameWithExtension, File fileToUpload) {
		try {
			String bucketName = getBucketNameByStatus(status);
			amazonS3client.putObject(bucketName, fileNameWithExtension, fileToUpload);

		} catch (SdkClientException ex) {
			LOG.error("Failed to upload on S3: " + ex.getStackTrace());
			return false;
		}

		return true;
	}

	public boolean createFolder(Integer status,String folderName) {
		try {
			ObjectListing objects = null;
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			PutObjectRequest putObjectRequest = null;
			String bucketName = getBucketNameByStatus(status);
			objects = amazonS3client.listObjects(
					new ListObjectsRequest().withBucketName(bucketName).withPrefix(folderName + FILE_SEPERATOR));
			for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
				if (objectSummary.getKey().equals(folderName)) {
					return false;
				}
			}
			putObjectRequest = new PutObjectRequest(bucketName, folderName + FILE_SEPERATOR, emptyContent, metadata);
			amazonS3client.putObject(putObjectRequest);
			return true;
		} catch (SdkClientException ex) {
			LOG.error("Failed to upload on S3: " + ex.getStackTrace());
			return false;
		}

	}

	public byte[] getS3Object(Integer status, String fileNameWithExtension) {
		S3Object object = null;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			String bucketName = getBucketNameByStatus(status);
			object = amazonS3client.getObject(new GetObjectRequest(bucketName, fileNameWithExtension));
		} catch (AmazonS3Exception e) {
			LOG.error("Document not available on s3");
			return new byte[0];
		}
		if (object == null) {
			LOG.error("Document not available on s3");
			return new byte[0];
		}
		try (InputStream inputStream = object.getObjectContent()) {
			int n = 0;
			byte[] buffer = new byte[1024];
			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}
		} catch (Exception e) {
			LOG.error("Unable to get object from s3");
			return new byte[0];
		}

		return output.toByteArray();
	}

	public boolean findS3Bucket(Integer status) {

		try {
			String bucketName = getBucketNameByStatus(status);
			return amazonS3client.doesBucketExist(bucketName);
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	public boolean deleteS3Object(Integer status,String signatureKey) {

		try {
			String bucketName = getBucketNameByStatus(status);
			amazonS3client.deleteObject(bucketName, signatureKey);
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	public boolean renameS3Folder(Integer status,String signatureKey, String destinationKey) {
		try {
			LOG.info("source :" + signatureKey);
			LOG.info("destination :" + destinationKey);
			String bucketName = getBucketNameByStatus(status);
			renameAwsFolder(bucketName, signatureKey, destinationKey);
			deleteMultipleObjects(bucketName, signatureKey);
			return true;
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}

	}
	
	public boolean renameS3Object(Integer status,String signatureKey, String destinationKey) {

		try {
			// signatureKey += "/"; destinationKey += "/";
			LOG.info("source :" + signatureKey);
			LOG.info("destination :" + destinationKey);
			String bucketName = getBucketNameByStatus(status);
			amazonS3client.copyObject(bucketName, signatureKey, clientBucketName, destinationKey);
			amazonS3client.deleteObject(bucketName, signatureKey);
			return true;
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}

	}

	public List<String> getObjectslistFromFolder(Integer status, String folderKey) {
		LOG.info("folder name:" + folderKey);
		List<String> keys = new ArrayList<>();
		String bucketName = getBucketNameByStatus(status);
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
				.withPrefix(folderKey + FILE_SEPERATOR);
		ObjectListing objects = amazonS3client.listObjects(listObjectsRequest);
		for (;;) {
			List<S3ObjectSummary> summaries = objects.getObjectSummaries();
			if (summaries.size() < 1) {
				break;
			}
			summaries.forEach(s -> keys.add(baseUrl + bucketName + FILE_SEPERATOR + s.getKey()));
			objects = amazonS3client.listNextBatchOfObjects(objects);
		}
		return keys;
	}

	public boolean deleteMultipleObjects(String bucketName, String folderName) {
		try {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
					.withPrefix(folderName);
			ObjectListing objectListing = amazonS3client.listObjects(listObjectsRequest);
			while (true) {
				for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
					amazonS3client.deleteObject(bucketName, objectSummary.getKey());
				}
				if (objectListing.isTruncated()) {
					objectListing = amazonS3client.listNextBatchOfObjects(objectListing);
				} else {
					break;
				}
			}
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	public boolean deleteFolder(Integer status,String folderName) {
		try {
			folderName += FILE_SEPERATOR;
			String bucketName = getBucketNameByStatus(status);
			amazonS3client.deleteObject(bucketName, folderName);
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	public boolean moveMultipleObjects(Integer status,String sourceFolder, String destinationFolder,String names[]) {
		try {
			String bucketName = getBucketNameByStatus(status);
			for (int i = 0; i < names.length; i++) {
				amazonS3client.copyObject(bucketName, sourceFolder + FILE_SEPERATOR + names[i], bucketName,
						destinationFolder + FILE_SEPERATOR + names[i]);
				amazonS3client.deleteObject(bucketName, sourceFolder + FILE_SEPERATOR + names[i]);
			}
		} catch (AmazonS3Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	public boolean copyFolder(Integer status,String sourcefolder, String destinationfolder) {
		String bucketName = getBucketNameByStatus(status);
		ObjectListing objectListing = amazonS3client.listObjects(
				new ListObjectsRequest().withBucketName(bucketName).withPrefix(sourcefolder + FILE_SEPERATOR));
		String sourcePrefix = "";
		StringBuffer sourcekey = new StringBuffer(sourcefolder + FILE_SEPERATOR);
		int position = sourcefolder.lastIndexOf("/", sourcefolder.length());
		if (position == -1)
			sourcePrefix = sourcefolder;
		else
			sourcePrefix = sourcefolder.substring(position + 1);
		LOG.info("source prefix:" + sourcePrefix);
		StringBuffer destinationKey = new StringBuffer(
				destinationfolder + FILE_SEPERATOR + sourcePrefix + FILE_SEPERATOR);
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			LOG.info("destinationString:" + destinationKey + objectSummary.getKey().substring(sourcekey.length()));
			LOG.info("object summary key" + objectSummary.getKey());
			amazonS3client.copyObject(bucketName, objectSummary.getKey(), bucketName,
					destinationKey + objectSummary.getKey().substring(sourcekey.length()));
		}
		objectListing = amazonS3client.listNextBatchOfObjects(objectListing);
		return true;
	}

	public boolean renameAwsFolder(String bucketName, String keyName, String newName) {
		try {
			List<S3ObjectSummary> fileList = amazonS3client.listObjects(bucketName, keyName).getObjectSummaries();
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			String finalLocation = keyName.substring(0, keyName.lastIndexOf('/') + 1) + newName;
			for (S3ObjectSummary file : fileList) {
				String key = file.getKey();
				// updating child folder location with the newlocation
				String destinationKeyName = key.replace(keyName, finalLocation);
				if (key.charAt(key.length() - 1) == '/') {
					// if name ends with suffix (/) means its a folders
					PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, destinationKeyName,
							emptyContent, metadata);
					amazonS3client.putObject(putObjectRequest);
				} else {
					// if name doesnot ends with suffix (/) means its a file
					CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, file.getKey(), bucketName,
							destinationKeyName);
					amazonS3client.copyObject(copyObjRequest);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean deleteFolderWithFiles(Integer status,String folderName) {
		String bucketName = getBucketNameByStatus(status);
		return deleteMultipleObjects(bucketName, folderName);	
	}

}
