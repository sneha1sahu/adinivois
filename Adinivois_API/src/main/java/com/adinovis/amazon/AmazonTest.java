package com.adinovis.amazon;

import java.io.ByteArrayInputStream;

/*import static com.cinch.common.errors.CommunicationErrorType.DOC_NOT_AVAILABLE_ON_STORAGE;
import static com.cinch.common.errors.CommunicationErrorType.UNABLE_TO_GET_OBJECT_FROM_S3;*/

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AmazonTest {
	 private static final Logger LOG = LoggerFactory.getLogger(AmazonTest.class);

	@Autowired
	org.springframework.core.env.Environment environment;
	
    private static final String FILE_EXTENSION_PNG = ".png";
    private static final String FILE_EXTENSION_PDF = ".pdf";
    private static final String CONTENT_TYPE = "application/octet-stream";
    private static final String SUFFIX = "/";
    @Autowired
    private Long expiryTime;
    @Autowired
    private String awsregion;
    @Autowired
    String accesskey="AKIAYH5VJ5PNSFYEAIVZ";
    String secretKey="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";
    String region="us-east-1";
    private AmazonS3 amazonS3Client;
    public void getBucket()
    {
    	AWSCredentials credentials = new BasicAWSCredentials(
    			accesskey, 
    			secretKey
    			);
    	amazonS3Client = AmazonS3ClientBuilder
    			  .standard()
    			  .withCredentials(new AWSStaticCredentialsProvider(credentials))
    			  .withRegion(Regions.US_EAST_1)
    			  .build();
    	//List<Bucket> buckets = amazonS3Client.listBuckets();
    	//int i=1;
    	//for(Bucket bucket : buckets) {
    		
    	 //   System.out.println(""+i+"."+bucket.getName());
    	  //  i++;
    //	}
    	String bucketName = "adinovisemailtemplateimage";
    	
    	// String signatureKey="profilepicture/sample.png";
    	 //boolean bucketstatus=getS3Object(signatureKey,bucketName);
    	 //System.out.println("BucketExist:"+bucketstatus);
    	 
		/*
		 * if(amazonS3Client.doesBucketExist(bucketName)) { LOG.info("Bucket is found");
		 * System.out.println("true"); return; }
		 * 
		 */
    	 String filenamewithextn="profilepicture/sample.png";
    	boolean filestatus=putS3Object(bucketName,filenamewithextn,new File("D:/Images/sample.png"));
    	 
    	System.out.println("FileUploaded:"+filestatus);
    	// boolean bucketstatus=deleteS3Object(signatureKey,bucketName);
    	//System.out.println("ObjectDeleted:"+bucketstatus);
    	//String foldername="AdinovisImages";
    	//boolean folderstatus=createFolder(bucketName, foldername, amazonS3Client);
    	//System.out.println("Folder created Successfully:"+folderstatus);
    	
    	 
    }
    public boolean putS3Object(String bucketName, String fileNameWithExtension, File fileToUpload)  {
        try {
        	amazonS3Client.putObject(bucketName, fileNameWithExtension, fileToUpload);
        
        }
        catch (SdkClientException ex) {
        	LOG.error("Failed to upload on S3: "+ex.getStackTrace());
            return false;
        }

        return true;
    }
    public boolean createFolder(String bucketName, String folderName, AmazonS3 client) {
    	
    	
    	try {
    		// create meta-data for your folder and set content-length to 0
    		ObjectMetadata metadata = new ObjectMetadata();
        	metadata.setContentLength(0);
        	
        	// create empty content
        	InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        	// create a PutObjectRequest passing the folder name suffixed by /
        	PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
        				folderName + SUFFIX, emptyContent, metadata);
        	// send request to S3 to create folder
        	client.putObject(putObjectRequest);
        }
        catch (SdkClientException ex) {
        	LOG.error("Failed to upload on S3: "+ex.getStackTrace());
            return false;
        }

        return true;
    }
    public byte[] getS3Object(String bucketName, String fileName, String fileExtension) {
        S3Object object = null;
        try {
         object = amazonS3Client.getObject(new GetObjectRequest(bucketName, fileName.concat(".").concat(fileExtension)));
        } catch (AmazonS3Exception e) {
            //throw new CinchGatewayException(DOC_NOT_AVAILABLE_ON_STORAGE);
        	  LOG.error("DOC_NOT_AVAILABLE_ON_STORAGE");
        }
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        if(object == null) {
           //throw new CinchGatewayException(DOC_NOT_AVAILABLE_ON_STORAGE);
            LOG.error("DOC_NOT_AVAILABLE_ON_STORAGE");
        }
        try (InputStream inputStream = object.getObjectContent()) {
            int n = 0;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
        } catch (Exception e) {
           // throw new CinchGatewayException(UNABLE_TO_GET_OBJECT_FROM_S3);
        	 LOG.error("UNABLE_TO_GET_OBJECT_FROM_S3");
        }

        return output.toByteArray();
    }
    public boolean getS3Object(String signatureKey, String bucketName) {

        try {
            return amazonS3Client.doesObjectExist(bucketName, signatureKey);
        } catch (AmazonS3Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
    }
    public boolean deleteS3Object(String signatureKey, String bucketName) {

        try {
             amazonS3Client.deleteObject(bucketName,signatureKey);
        } catch (AmazonS3Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }
    public static void main(String args[])
    {
    	AmazonTest test=new AmazonTest();
    	test.getBucket();
    }

}
