package com.adinovis.amazon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
@PropertySource("classpath:application.properties")
public class AmazonConfiguration {
	@Autowired
	org.springframework.core.env.Environment environment;

	@Bean(name = "amazonS3Client")
	public AmazonS3 amazonClient() {
		AWSCredentials credentials = new BasicAWSCredentials(environment.getProperty("aws_access_key_id"),
				environment.getProperty("aws_secret_access_key"));
		AmazonS3 amazonS3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(environment.getProperty("aws_region")).build();
		return amazonS3client;
	}
	@Bean(name = "baseUrl")
	public String baserUrl() {
		return environment.getProperty("aws_s3_endpoint");
	}
	@Bean(name = "clientDocsBucketName")
	public String clientBucketName() {
		return environment.getProperty("aws_clientdocs_bucket_name");
	}
	@Bean(name = "profileImageBucketName")
	public String profileBucketName() {
		return environment.getProperty("aws_profile_image_bucket_name");
	}
}
