package com.adinovis.quartz;
import java.util.concurrent.TimeUnit;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DailyWork implements Job
{
	public void execute(JobExecutionContext context)
            throws JobExecutionException {
		long startTime = System.currentTimeMillis();	
		TimeSchedulerTask.doWork();
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		String hms = String.format(
				"%02d:%02d:%02d",
				TimeUnit.MILLISECONDS.toHours(totalTime),
				TimeUnit.MILLISECONDS.toMinutes(totalTime)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(totalTime)),
				TimeUnit.MILLISECONDS.toSeconds(totalTime)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(totalTime)));
		System.out.println("Total Reports Generated In ---->" + hms);
    System.out.println("------------All Reports generated. ---------------------------------------");
}
}