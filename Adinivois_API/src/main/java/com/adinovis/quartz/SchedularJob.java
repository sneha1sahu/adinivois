package com.adinovis.quartz;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.configuration.quickbook.QuickBookGateway;
import com.adinovis.configuration.xero.XeroGateway;
import com.adinovis.dao.ClientDao;
import com.adinovis.domain.ClientData;
import com.google.gson.Gson;
import com.intuit.ipp.data.Report;
import com.intuit.ipp.exception.FMSException;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.OAuthException;
import com.xero.api.OAuthAccessToken;
import com.xero.models.accounting.ReportWithRows;

@Configuration
@EnableScheduling
public class SchedularJob {

	@Autowired
	private QuickBookGateway quickBooksGateway;

	@Autowired
	protected ClientDao clientDao;

	@Autowired
	private OAuthAccessToken accessToken;

	@Autowired
	private XeroGateway xeroGateway;

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedularJob.class);

	@Scheduled(cron = "${trailbalance.import.job}")
	public synchronized void getReports() {
		List<ClientData> clientDataList = clientDao.getClientData();

		for (ClientData clientData : clientDataList) {
			if (CloudunionConstants.SOURCELINK_QUICKBOOK.equalsIgnoreCase(clientData.getSourceLink())) {
				saveQuickBookReport(clientData);
			} else if (CloudunionConstants.SOURCELINK_XERO.equalsIgnoreCase(clientData.getSourceLink())) {
				 saveXeroReport(clientData);
			}
		}
		clientDao.loadJsonIntoTrailBalance();
	}

	private void saveQuickBookReport(ClientData clientData) {

		Report trialBalReport = null;
		Report accountListReport = null;
		try {
		 trialBalReport = quickBooksGateway.getTrialBalanceReport(clientData.getRealmId(), clientData.getAccessToken(),
				clientData.getStartDate(), clientData.getEndDate());
		 accountListReport = quickBooksGateway.getAccountList(clientData.getRealmId(), clientData.getAccessToken());
		} catch (OAuthException e) {
			BearerTokenResponse response = quickBooksGateway.getNewToken(clientData.getRefreshToken());
			LOGGER.info(" Oauth response " + new Gson().toJson(response));
			if (response == null) {
				LOGGER.warn("Failed to get Quickbook AccessToken");
				return;
			}
			clientData.setAccessToken(response.getAccessToken());
			clientData.setRefreshToken(response.getRefreshToken());
			clientDao.updateRefreshToken(clientData);
			saveQuickBookReport(clientData);
		} catch (FMSException e) {
			e.printStackTrace();
		}
		
		if (trialBalReport != null || accountListReport != null) {
			Gson gson = new Gson();
			String trailBalJson = gson.toJson(trialBalReport);
			String accountListJson = gson.toJson(accountListReport);
			LOGGER.info("Trail Balance data : " + trailBalJson);
			LOGGER.info("Account List data : " + accountListJson);
			clientDao.saveClientData(clientData.getEngagementId(), trailBalJson, HttpStatus.OK.value(), accountListJson);
		}

	}

	private void saveXeroReport(ClientData clientData) {

		LOGGER.info("access token success : " + accessToken.isSuccess());
		LOGGER.info("Token : " + accessToken.getToken() + " secret : " + accessToken.getTokenSecret());
		ReportWithRows rows = xeroGateway.getTrialBalanceReport(clientData.getAccessToken(), clientData.getRefreshToken(),
				clientData.getAccountYear() == null ? null : clientData.getAccountYear().intValue(), clientData.getEndDate());
		if (rows != null) {
			String json = new Gson().toJson(rows);
			LOGGER.info(json);
			clientDao.saveClientData(clientData.getEngagementId(), json, HttpStatus.OK.value(), null);
		}

	}

	public boolean loadTrailBalance(int engagementId) {

		List<ClientData> clientDataList = clientDao.getSourcelink(engagementId);
		LOGGER.info("client Data List  : " + clientDataList.size());
		if (clientDataList.isEmpty()) {
			return false;
		}
		for (ClientData clientData : clientDataList) {
			if (CloudunionConstants.SOURCELINK_QUICKBOOK.equalsIgnoreCase(clientData.getSourceLink())) {
				saveQuickBookReport(clientData);
			} else if (CloudunionConstants.SOURCELINK_XERO.equalsIgnoreCase(clientData.getSourceLink())) {
				saveXeroReport(clientData);
			}
		}

		LOGGER.info("load Quickbook Data Process : " + clientDao.loadQuickbookDataProcess(engagementId));
//		LOGGER.info("extractEngTBjsontotable : " + clientDao.extractEngTBjsontotable(engagementId));
//		LOGGER.info("extractEngATjsontotable : " + clientDao.extractEngATjsontotable(engagementId));
//		LOGGER.info("updateAccountList : " + clientDao.updateAccountList(engagementId));
//		LOGGER.info("loadtrailbalancemappingupdate : " + clientDao.loadtrailbalancemappingupdate(engagementId));
		return true;

	}
}
