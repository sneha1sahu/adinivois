/**
 * Program Name: DataBaseException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for Cloudunion service   
 *                            
 * Modules Impacted: 
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri       10/03/2017 
 * 
 * * Associated Defects Raised : 
 *
 */ 

package com.adinovis.exception;

public class DataBaseException extends CinchRuntimeBaseException {


  private static final long serialVersionUID = 1L;


  public DataBaseException(String message) {
    super(message);
  }

  public DataBaseException(String message, Throwable throwable) {
    super(message, throwable);
  }


}
