/**
 * Program Name: IdNotFoundException
 * 
 * Program Description / functionality: This is the Exception handling class for Cloudunion service
 * 
 * Modules Impacted: 
 * 
 *  * Developer    Created             /Modified Date       Purpose
 * *******************************************************************************
 * Gouri         10/03/2017               
 * 
 *   
 * Associated Defects Raised :
 * 
 */

package com.adinovis.exception;

public class IdNotFoundException extends CinchRuntimeBaseException {

  private static final long serialVersionUID = 1L;

  public IdNotFoundException(String errorCode) {
    this.errorCode = errorCode;
  }

  public IdNotFoundException(String message, String errorCode) {
    super(message, errorCode);

  }

  public IdNotFoundException(String message, Throwable throwable) {
    super(message, throwable);
  }



}
