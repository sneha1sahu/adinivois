/**
 * Program Name: TrinetParseException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for my company service   
 *                            
 * Modules Impacted: My Company
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Bhargava     17/08/2015 
 *
 * 
 * Associated Defects Raised : 
 *
 */ 

package com.adinovis.exception;

public class CinchParseException extends CinchBaseException {

  private static final long serialVersionUID = 1L;

  public CinchParseException(String errorCode) {
    this.errorCode = errorCode;
  }

  public CinchParseException(String message, String errorCode) {
    super(message, errorCode);

  }



}
