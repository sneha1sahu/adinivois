/**
 * Program Name: SecurityAuthDaoException
 * 
 * Program Description / functionality: This is the Exception handling class for Cloudunion service   
 * 
 * Modules Impacted:    
 * 
 * Developer    Created             /Modified Date       Purpose
 *******************************************************************************
 * Gouri       08/03/2017            
 *                                      
 * 
 * Associated Defects Raised :
 */
package com.adinovis.exception;

public class SecurityAuthDaoException extends RuntimeException {

    private static final long serialVersionUID = -9012898912300932547L;

    public SecurityAuthDaoException(String message) {
        super(message);
    }

    public SecurityAuthDaoException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
