/**
 * Program Name: NavigationServiceException
 * 
 * Program Description / functionality: This is the Exception handling class for my company service
 * 
 * Modules Impacted: My Company
 * 
 * Developer    Created             /Modified Date       Purpose
 * *******************************************************************************
 * Bhargava     17/08/2015               
 * 
 * Associated Defects Raised :
 * 
 */

package com.adinovis.exception;

public class NavigationServiceException extends CinchRuntimeBaseException {

  private static final long serialVersionUID = 1L;

  public NavigationServiceException(String message, String errorCode) {
    super(message, errorCode);

  }

  public NavigationServiceException(String errorCode) {
    super.errorCode = errorCode;

  }

  public NavigationServiceException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
