/**
 * Program Name: HomeDaoException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for Cloudunion service   
 *                            
 * Modules Impacted:  
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri       06/03/2017            
 * 
 * Associated Defects Raised : 
 *
 */
package com.adinovis.exception;

public class CinchDaoException extends RuntimeException {

  private static final long serialVersionUID = -9012898912300932547L;

  public CinchDaoException(String message) {
    super(message);
  }

  public CinchDaoException(String message, Throwable throwable) {
    super(message, throwable);
  }


}
