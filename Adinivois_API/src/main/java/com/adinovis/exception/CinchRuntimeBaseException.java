/**
 * Program Name: TrinetRuntimeBaseException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for my company service   
 *                            
 * Modules Impacted: My Company
 *                                                                    
 * Developer    Created             Modified Date       Purpose
 * *******************************************************************************
 * Bhargava     17/08/2015                                                                                                                      
 * 
 * Associated Defects Raised : 
 *                                                                                                         
 */

package com.adinovis.exception;

import com.adinovis.common.ExceptionConstants;

public class CinchRuntimeBaseException extends RuntimeException {

  /**
   * Generated UID
   */
  private static final long serialVersionUID = -3868232127218537194L;

  protected String errorCode;

  protected Object[] messageParams;

  protected ExceptionConstants exceptionConstants;

  public CinchRuntimeBaseException() {
    super();
  }

  public CinchRuntimeBaseException(String msg) {
    super(msg);
  }

  public CinchRuntimeBaseException(String msg, String errorCode) {
    super(msg);
    this.errorCode = errorCode;

  }

  public CinchRuntimeBaseException(Throwable e) {
    super(e);
  }

  public CinchRuntimeBaseException(String msg, Throwable e) {
    super(msg, e);
    this.errorCode=e.getMessage();
  }



  public static String getStack(Throwable e) {
    StackTraceElement[] st = Thread.currentThread().getStackTrace();
    return st[4].getClassName() + "." + st[4].getMethodName() + "():" + st[4].getLineNumber() + " ->" + e.getMessage();
  }

  public static String getStack() {
    StackTraceElement[] st = Thread.currentThread().getStackTrace();
    return st[4].getClassName() + "." + st[4].getMethodName() + "():" + st[4].getLineNumber() + " -> ";
  }


  public String getErrorCode() {
    return this.errorCode;
  }


  /**
   * @return Returns the messageParams.
   */
  public Object[] getMessageParams() {
    return messageParams;
  }

  public ExceptionConstants getExceptionConstants() {
    return exceptionConstants;
  }

}
