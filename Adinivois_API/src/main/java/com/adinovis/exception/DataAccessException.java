/**
 * Program Name: DataAccessException 
 *                                                                 
 * Program Description / functionality: This is the Exception handling class for Cloudunion service   
 *                            
 * Modules Impacted:   
 *                                                                    
 *                                                                                                         
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Gouri       10/03/2017 
 * 
 * Associated Defects Raised : 
 *
 */ 

package com.adinovis.exception;

public class DataAccessException extends CinchRuntimeBaseException {

  /**
   * Generated UID
   */
  private static final long serialVersionUID = 7369552997038420811L;

  public DataAccessException(String message) {
    super(message);
  }

  public DataAccessException(String message, Throwable throwable) {
    super(message, throwable);
  }


}
