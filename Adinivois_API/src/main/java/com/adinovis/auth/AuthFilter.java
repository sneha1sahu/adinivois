/**
 * Program Name: AuthFilter
 * 
 * Program Description / functionality: Auth filter service
 * 
 * Modules Impacted: Authorization auth2.0
 *
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Neeraj   06/03/2016
 * 
 * 
 * Associated Defects Raised :
 * 
 */
package com.adinovis.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.ReturnResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UrlPathHelper;

import com.google.gson.Gson;

public class AuthFilter extends OncePerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);
	private static final UrlPathHelper urlPathHelper = new UrlPathHelper();

	/**
	 *
	 * Purpose: This method is to implement cross origin resource sharing
	 * 
	 * @param Http
	 *            request
	 * @param Http
	 *            response
	 * @param filterChain
	 * 
	 * @see : Data
	 */

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		boolean authSerive = false;
		LOGGER.debug("Auth Filter begin");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type, access_token");
		response.addHeader("Access-Control-Max-Age", "1");
		String requestUri = urlPathHelper.getRequestUri(request);
		if(!requestUri.contains(CinchConstants.VERSION_SECURITY)){
			authSerive = true;
		}
		else if (FilterUtil.isHeaderEqual(request, FilterUtil.ACCESS_TOKEN)) {
			if (requestUri.contains(AuthURIConstants.REFRESH_TOKEN))
				authSerive = true;
			else if (FilterUtil.isHeaderValue(request, FilterUtil.ACCESS_TOKEN))
				authSerive = new AuthServiceImpl()
						.isTokenValid(FilterUtil.getHeaderValue(request, FilterUtil.ACCESS_TOKEN));
		}

		LOGGER.debug("Auth Filter end");
		if (authSerive)
			filterChain.doFilter(request, response);
		else {
			ReturnResponse returnResponse = new ReturnResponse();
			returnResponse.setErrorCode("401");
			returnResponse.setStatusCode("Access Denied");
			returnResponse.setData(null);
			returnResponse.setStatusMessage("Unauthorized : Full authentication is required to access this resource");
			response.setContentType("application/json");
			response.getWriter().print(new Gson().toJson(returnResponse));
		}

	}
}
