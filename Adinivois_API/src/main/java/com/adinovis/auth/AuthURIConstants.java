package com.adinovis.auth;

public interface AuthURIConstants {
  public static final String LOGIN_MATCH="getloginIdwithsocial";
  public static final String REFRESH_URI= "/refreshToken";
  public static final String REFRESH_TOKEN= "{uId}/refreshToken";
  public static final String LOGIN="/login";
}