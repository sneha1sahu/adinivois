package com.adinovis.auth;

/**
 * Program Name: AuthServiceImpl
 * 
 * Program Description / functionality: AuthServiceImpl
 * 
 * Modules Impacted: Authorization auth2.0
 *
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Neeraj   06/03/2016
 * 
 * 
 * Associated Defects Raised :
 * 
 */
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.adinovis.domain.ReturnResponse;

public class AuthServiceImpl implements AuthService{
	
    public static final String AUTH_SERVER_URI = "http://localhost:8082/api-authService";
    public static final String AUTH_SERVER_ACCESS_TOKEN ="/authUser/def";
    public static final String AUTH_TOKEN_URI = "/oauth/token";
    public static final String QPM_ACCESS_TOKEN = "?access_token=";
    public static final String LOGOUT = "?access_token=";
 
    /*
     * Prepare HTTP Headers.
     */
    private static HttpHeaders getHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
     
    private static HttpHeaders getHeadersWithClientCredentials(){
        String plainClientCredentials="ptg123:password";
        String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
        HttpHeaders headers = getHeaders();
        headers.add("Authorization", "Basic " + base64ClientCredentials);
       // headers.set("Authorization", "Basic cHRnOnNlY3JldA==");
        
        return headers;
    }
    
    private static HttpHeaders getHeadersWithBearerToken(String bearerToken){
    	HttpHeaders headers = getHeaders();
        headers.set("Authorization", "bearer "+bearerToken);
        return headers;
    }
    
    @SuppressWarnings({ "unchecked"})
    public  AuthTokenInfo createToken(AuthBean authBean){
    	String QPM_PASSWORD_GRANT = "?grant_type="+authBean.getGrant_type()+"&username="+authBean.getUsername()+"&password="+authBean.getPassword()+"";
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> request = new HttpEntity<String>(getHeadersWithClientCredentials());
        
        String requestUri=AUTH_SERVER_URI+AUTH_TOKEN_URI+QPM_PASSWORD_GRANT;
        ResponseEntity<Object> response = restTemplate.exchange(requestUri, HttpMethod.POST, request, Object.class);
        LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>)response.getBody();
        AuthTokenInfo tokenInfo = null;
         
        if(map!=null){
            tokenInfo = new AuthTokenInfo();
            tokenInfo.setAccess_token((String)map.get("access_token"));
            tokenInfo.setRefresh_token((String)map.get("refresh_token"));
            tokenInfo.setExpires_in((Integer)map.get("expires_in"));
            tokenInfo.setLoginId(authBean.getUsername());
        }
        return tokenInfo;
    }
 
	
	public boolean isTokenValid(String access_token) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> authRequest = new HttpEntity<String>( getHeadersWithBearerToken(access_token) );
        String requestUri=AUTH_SERVER_URI+AUTH_SERVER_ACCESS_TOKEN+QPM_ACCESS_TOKEN+access_token;
        try{
        	 ResponseEntity<ReturnResponse> authResponse=restTemplate.exchange(requestUri,HttpMethod.GET,authRequest, ReturnResponse.class);
        	 if(authResponse.getBody().getStatusMessage().equalsIgnoreCase("Success"))
        		 return true;
        	 else
        		 return false;
        	 
        }
        catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
    }
	
	
	public String validateToken(String access_token) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> authRequest = new HttpEntity<String>( getHeadersWithBearerToken(access_token) );
        String requestUri=AUTH_SERVER_URI+AUTH_SERVER_ACCESS_TOKEN+QPM_ACCESS_TOKEN+access_token;
        try{
        	 ResponseEntity<ReturnResponse> authResponse=restTemplate.exchange(requestUri,HttpMethod.GET,authRequest, ReturnResponse.class);
        	 System.out.println(authResponse.getBody());
        	 return "Y";
        }
        catch (Exception e) {
			System.out.println(e.getMessage());
			return "N";
		}
    }

	public String refreshTokens(AuthBean authBean) {
		String QPM_REFRESH_TOKEN = "?grant_type="+authBean.getGrant_type()+"&refresh_token="+authBean.getToken()+"";
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> authRequest = new HttpEntity<String>( getHeadersWithClientCredentials() );
        String requestUri=AUTH_SERVER_URI+AUTH_SERVER_ACCESS_TOKEN+QPM_REFRESH_TOKEN;
        try{
        	 ResponseEntity<Object> authResponse=restTemplate.exchange(requestUri,HttpMethod.GET,authRequest, Object.class);
        	 System.out.println(authResponse.getBody());
        	 return "Y";
        }
        catch (Exception e) {
			System.out.println(e.getMessage());
			return "N";
		}
    }
	@SuppressWarnings({ "unchecked"})
    public  AuthTokenInfo refreshToken(AuthBean authBean) {
		String QPM_REFRESH_TOKEN = "?grant_type="+authBean.getGrant_type()+"&refresh_token="+authBean.getToken()+"";
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> authRequest = new HttpEntity<String>(getHeadersWithClientCredentials());
        String requestUri=AUTH_SERVER_URI+AUTH_TOKEN_URI+QPM_REFRESH_TOKEN;
        AuthTokenInfo tokenInfo = null;
        try{
        	ResponseEntity<Object> authResponse=restTemplate.exchange(requestUri,HttpMethod.POST,authRequest, Object.class);
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>)authResponse.getBody();
            
             
            if(map!=null){
                tokenInfo = new AuthTokenInfo();
                tokenInfo.setAccess_token((String)map.get("access_token"));
                tokenInfo.setRefresh_token((String)map.get("refresh_token"));
                tokenInfo.setExpires_in((Integer)map.get("expires_in"));
                tokenInfo.setLoginId(authBean.getUsername());
            }
            return tokenInfo;
       }
       catch (Exception e) {
			System.out.println(e.getMessage());
			tokenInfo.setAccess_token("invalid_grant");
			tokenInfo.setRefresh_token("Invalid refresh token: "+authBean.getToken());
			return tokenInfo;
		}
    }
}