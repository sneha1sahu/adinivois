
/**
 * Program Name: AuthService
 * 
 * Program Description / functionality: Authservice
 * 
 * Modules Impacted: Authorization auth2.0
 *
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Neeraj   06/03/2016
 * 
 * 
 * Associated Defects Raised :
 * 
 */
package com.adinovis.auth;


public interface AuthService {
boolean isTokenValid(String access_token);
String validateToken(String access_token);
AuthTokenInfo createToken(AuthBean authBean);
AuthTokenInfo refreshToken(AuthBean authBean);
	
}