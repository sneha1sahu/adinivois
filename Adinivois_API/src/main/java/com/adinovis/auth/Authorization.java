package com.adinovis.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class Authorization {
	private static final Logger LOGGER = LoggerFactory.getLogger(Authorization.class);
	
	@Autowired
	private AuthService authService;
	
	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}
	
	public AuthTokenInfo getToken(AuthBean authBean) {
		LOGGER.info("doSignIn - Start");
		AuthTokenInfo authTokenInfo=null;
		authBean.setGrant_type("password");
		authTokenInfo=authService.createToken(authBean);
     	return authTokenInfo;
	}

	public AuthTokenInfo refreshUser(String refresh_token,String userName) {
		AuthTokenInfo authTokenInfo=null;
		if(refresh_token!=null && refresh_token.length() >0){
			
				AuthBean authBean=new AuthBean();
				if(refresh_token!=null && refresh_token.length() >0){
					authBean.setGrant_type("refresh_token");
					authBean.setToken(refresh_token);
					authBean.setUsername(userName);
					authTokenInfo=authService.refreshToken(authBean);	
				}
		}
		return authTokenInfo;	
}

	
}