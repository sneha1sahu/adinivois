package com.adinovis.auth;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class FilterUtil {
	public final static String USER_TOKEN="user_token";
	public final static String ACCESS_TOKEN="access_token";
	public final static String RERESH_TOKEN="refresh_token";
	

	public static boolean isHeaderValue(HttpServletRequest request,String headerName){
		boolean isHeaderValue=false;
		String headerValue=request.getHeader(headerName);
		if(headerValue!=null)
			isHeaderValue=true;
		return isHeaderValue;
	}
	public static boolean isParamValue(HttpServletRequest request,String paramName){
		boolean isParamValue=false;
		String paramValue=request.getParameter(paramName);
		if(paramValue!=null)
			isParamValue=true;
		return isParamValue;
	}
	public static String getHeaderValue(HttpServletRequest request,String headerName){
		return request.getHeader(headerName);
	}
	public static String getParamValue(HttpServletRequest request,String paramName){
		return request.getParameter(paramName);
	}
	
	
	public static boolean isHeaderEqual(HttpServletRequest request,String header){
		boolean isEqual=false;
		List<String> headers=new ArrayList<String>();
		Enumeration<String> headerNames = request.getHeaderNames();
	    while(headerNames.hasMoreElements())
	    {
	      headers.add((String)headerNames.nextElement());
	    }
	    
	    if(headers.contains(header)){
	    	isEqual=true;
	    }
		return isEqual;
	}
	
	}