package com.adinovis.auth;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.intuit.ipp.data.User;
import com.adinovis.domain.BusinessDetial;
import com.adinovis.domain.ReferralMessage;
import com.adinovis.domain.UIConfig;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginDetails implements Serializable {
	private static final long serialVersionUID = 3245167604686618001L;
	private String access_token;
	private String refresh_token;
	private String userType;
	private User userDetail;
	private BusinessDetial businessDetial;
	private ReferralMessage referralMessage;
	private List<UIConfig> uiConfigs;
	private String membership;
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public User getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(User userDetail) {
		this.userDetail = userDetail;
	}
	public BusinessDetial getBusinessDetial() {
		return businessDetial;
	}
	public void setBusinessDetial(BusinessDetial businessDetial) {
		this.businessDetial = businessDetial;
	}
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public List<UIConfig> getUiConfigs() {
		return uiConfigs;
	}
	public void setUiConfigs(List<UIConfig> uiConfigs) {
		this.uiConfigs = uiConfigs;
	}
	public ReferralMessage getReferralMessage() {
		return referralMessage;
	}
	public void setReferralMessage(ReferralMessage referralMessage) {
		this.referralMessage = referralMessage;
	}
	public String getMembership() {
		return membership;
	}
	public void setMembership(String membership) {
		this.membership = membership;
	}
	
	
	
	
}
