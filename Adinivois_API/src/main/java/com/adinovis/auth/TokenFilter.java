/**
 * Program Name: AuthFilter
 * 
 * Program Description / functionality: Auth filter service
 * 
 * Modules Impacted: Authorization auth2.0
 *
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Neeraj   06/03/2016
 * 
 * 
 * Associated Defects Raised :
 * 
 */
package com.adinovis.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UrlPathHelper;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.utils.security.TokenUtilities;
import com.google.gson.Gson;

public class TokenFilter extends OncePerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenFilter.class);
	private static final UrlPathHelper urlPathHelper = new UrlPathHelper();

	/**
	 *
	 * Purpose: This method is to implement cross origin resource sharing
	 * 
	 * @param Http
	 *            request
	 * @param Http
	 *            response
	 * @param filterChain
	 * 
	 * @see : Data
	 */

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		boolean authSerive = false;
		String requestUri = urlPathHelper.getRequestUri(request);
		LOGGER.debug("Token - Filter begin");
		ReturnResponse returnResponse = new ReturnResponse();
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Headers","Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, " + FilterUtil.ACCESS_TOKEN + ", " + FilterUtil.RERESH_TOKEN + "");
		response.addHeader("Access-Control-Max-Age", "1");
		
		if (!requestUri.contains(CinchConstants.VERSION_SECURITY)) {
			authSerive = true;
		} 
		else if (FilterUtil.isHeaderEqual(request, FilterUtil.ACCESS_TOKEN)
				&& FilterUtil.isHeaderValue(request, FilterUtil.ACCESS_TOKEN)) {
				String accessToken = FilterUtil.getHeaderValue(request, FilterUtil.ACCESS_TOKEN);
				//authSerive = TokenUtilities.isValidAccessToken(requestUri, accessToken);
				switch (TokenUtilities.isValidAccessToken(requestUri, accessToken)) {
				case TokenUtilities.InvalidToken:
					authSerive=false;
					returnResponse.setErrorCode(null);
					returnResponse.setData(null);
					returnResponse.setStatusCode(String.valueOf(HttpStatus.SC_UNAUTHORIZED));
					returnResponse.setStatusMessage("Access Denied : Unauthorized");
					response.setContentType("application/json");
					
					break;
				case TokenUtilities.ValidToken:
					authSerive=true;
					break;
					
				case TokenUtilities.InvalidUser:
					authSerive=false;
					returnResponse.setErrorCode(null);
					returnResponse.setData(null);
					returnResponse.setStatusCode(String.valueOf(HttpStatus.SC_UNAUTHORIZED));
					returnResponse.setStatusMessage("Could not vierify your jsession");
					response.setContentType("application/json");
					
					break;	
				}
				
				if(authSerive){
					switch (TokenUtilities.isUserRole(requestUri, accessToken)) {
					case TokenUtilities.InvalidRole:
						authSerive=false;
						returnResponse.setErrorCode(null);
						returnResponse.setData(null);
						returnResponse.setStatusCode(String.valueOf(HttpStatus.SC_UNAUTHORIZED));
						returnResponse.setStatusMessage("Access Denied : you are not allowed to access this url");
						response.setContentType("application/json");
						break;
					case TokenUtilities.ValidRole:
						authSerive=true;
						break;
					}
				}
				
		}		
		if (authSerive){
			LOGGER.debug("Token : valid");
            filterChain.doFilter(request, response);	
			
			
			}
		else {
			LOGGER.debug("Token-Filter end : Unauthorized");
			response.getWriter().print(new Gson().toJson(returnResponse));
			
		}

	}
}
