
package com.adinovis.auth;

import java.io.Serializable;

public class AuthBean implements Serializable {
	private static final long serialVersionUID = 1022968807807360443L;
private String username;
private String password;
private String grant_type;
private String token;

public AuthBean() {
}

public AuthBean(String username, String password) {
	super();
	this.username = username;
	this.password = password;
}

public AuthBean(String username, String grant_type, String token) {
	super();
	this.username = username;
	this.grant_type = grant_type;
	this.token = token;
}
public AuthBean(String username, String password, String grant_type, String token) {
	this.username = username;
	this.password = password;
	this.grant_type = grant_type;
	this.token = token;
}

public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getGrant_type() {
	return grant_type;
}
public void setGrant_type(String grant_type) {
	this.grant_type = grant_type;
}


public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}


	
	
}
