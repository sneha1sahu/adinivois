/**
 * Program Name: AuthTokenInfo
 * 
 * Program Description / functionality: AuthTokenInfo
 * 
 * Modules Impacted: Authorization auth2.0
 *
 * Developer    Created             /Modified Date       Purpose
  *******************************************************************************
 * Neeraj   06/03/2016
 * 
 * 
 * Associated Defects Raised :
 * 
 */

package com.adinovis.auth;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthTokenInfo {
	private String access_token;
	private String refresh_token;
	private int expires_in;
	private String loginId;
	private Integer uid;
	private String firstName;
	private String lastName;
	private String deviceToken;
	private Integer osType;
	private String referralCode;
	private String referralQr;
	private String profileUrl;
	private String shortDesc;
	private String longDesc;
	private String userType;
	public AuthTokenInfo() {
	}

	public String getAccess_token() {
		return access_token;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public String getLoginId() {
		return loginId;
	}

	public Integer getUid() {
		return uid;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public Integer getOsType() {
		return osType;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getReferralQr() {
		return referralQr;
	}

	public void setReferralQr(String referralQr) {
		this.referralQr = referralQr;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}