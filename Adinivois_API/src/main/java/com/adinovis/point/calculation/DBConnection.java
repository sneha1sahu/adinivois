package com.adinovis.point.calculation;



import java.sql.Connection;
import java.sql.DriverManager;
public class DBConnection {
	
	static Connection conn;
	
	public static Connection getConnection() throws Exception {
		if (conn != null) return conn;
		else {
			try {
				// conn = dataSource.getConnection();
				conn = DriverManager.getConnection("jdbc:mysql://3.215.103.80:3306/adinovis?serverTimezone=UTC&" + "user=admin&password=admin@123&useSSL=false");	
				if ( conn == null ) {
					   throw new Exception("Data source not found!");
				}
			} catch (Exception excep2) {
				throw excep2;
			}
			return conn;
		}
	}
	
	public void closeConnection() {
	  try {
		if (conn != null ) conn.close();
	  } catch (Exception excep) {
		  System.out.println("Exception in closing connections " + excep);
	  }
	}

}
