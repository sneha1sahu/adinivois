package com.adinovis.point.calculation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class PointService {
	static String cu_points="select cu_points.u_account_id,user_type_name,membership_cash_conv_percentage,membership_managementfee,membership_consumer_ref_percentage,membership_business_ref_percentage,membership_monthly_fee,p_sky_points,p_sky_purchase_points,p_sky_referral_points,p_sky_purchase_business_factor_points,p_sky_purchase_growth_factor_points,p_sky_referral_growth_factor_points,p_cash_points_commulative,p_ltime_sky_points,p_ltime_cash_points,p_ltime_redeemed_points,p_as_of_date from cu_test.cu_points,cu_test.ref_user_type,cu_test.cu_users,cu_test.ref_membership,cu_test.cu_user_account where  cu_users.u_account_id=cu_points.u_account_id and cu_users.u_type=ref_user_type.user_type_id and cu_user_account.u_membership_level=ref_membership.membership_type";
	static String GET_TRANSACTION="select cu_transaction_id,trans_posted_date,trans_actual_date,trans_business_id,"
			+ "trans_consumer_id,trans_amount_grand_total,trans_amount_before_tax_tips,trans_day_bus_referral,"
			+ " trans_day_con_referral,trans_day_bus_referral_membership,trans_day_con_referral_membership,"
			+ "trans_day_bus_discount,trans_status_id  from cu_test.cu_transaction where cu_transaction_id = ";
	static String TransactionDependencies="select cu_transaction_id,trans_day_bus_discount, trans_day_con_referral,trans_day_bus_referral,trans_day_con_referral_membership,trans_day_bus_referral_membership,membership_consumer_ref_percentage,membership_business_ref_percentage from cu_test.cu_transaction,cu_test.ref_membership,cu_test.cu_user_account where cu_user_account.u_account_id=cu_transaction.trans_consumer_id and cu_user_account.u_membership_level=ref_membership.membership_type and cu_transaction_id=";
	static String sqlRefereeId="select 	referred_by_account_id from cu_test.account_referral where account_id=";
	static Connection connection=null;
	public static Transaction getTransaction(int transactionId){
		Transaction transaction=null;
		try {
			connection=DBConnection.getConnection(); 
			Statement statement=connection.createStatement();
			String sqlQuery=GET_TRANSACTION+""+transactionId;
			ResultSet resultSet= statement.executeQuery(sqlQuery);
			if(resultSet.next()){
				transaction=new Transaction();
				transaction.setTransactionID(resultSet.getString("cu_transaction_id"));
				transaction.setPostedDate(resultSet.getDate("trans_posted_date"));
				transaction.setActualDate(resultSet.getDate("trans_actual_date"));
				transaction.setBusinessID(resultSet.getString("trans_business_id"));
				transaction.setConsumerID(resultSet.getString("trans_consumer_id"));
				transaction.setAmountOfTransaction(resultSet.getDouble("trans_amount_grand_total"));
				transaction.setAmountBeforeTaxAndTips(resultSet.getDouble("trans_amount_before_tax_tips"));
				transaction.setBusinessRefereeID(resultSet.getString("trans_day_bus_referral"));
				transaction.setConsumerRefereeID(resultSet.getString("trans_day_con_referral"));
				transaction.setBusinessRefereeMembership(resultSet.getInt("trans_day_bus_referral_membership"));
				transaction.setConsumerRefereeMembership(resultSet.getInt("trans_day_con_referral_membership"));
				transaction.setDiscountByBusiness(resultSet.getDouble("trans_day_bus_discount"));
				transaction.setTransactionStatus(resultSet.getInt("trans_status_id"));
				//transaction.setPurchasePoints(resultSet.getDouble(""));
				//transaction.setWf_status(resultSet.getInt(""));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return transaction;
	}
	
	public static TransactionDependencies getTransactionDependencies(int transactionId,String trantype){
		TransactionDependencies transactionDependencies=null;
		try {
			connection=DBConnection.getConnection(); 
			Statement statement=connection.createStatement();
			String sqlQuery=TransactionDependencies+""+transactionId;
			ResultSet resultSet= statement.executeQuery(sqlQuery);
			if(resultSet.next()){
				transactionDependencies=new TransactionDependencies();
				transactionDependencies.setTransactionID(resultSet.getString("cu_transaction_id"));
				transactionDependencies.setBusinessDiscount(resultSet.getDouble("trans_day_bus_discount"));
				transactionDependencies.setConsumerRefereeAccountID(resultSet.getString("trans_day_con_referral"));
				transactionDependencies.setBusinessRefereeAccountID(resultSet.getString("trans_day_bus_referral"));
				transactionDependencies.setConsumerRefereeMembership(resultSet.getInt("trans_day_con_referral_membership"));
				transactionDependencies.setBusinessRefereeMembership(resultSet.getInt("trans_day_bus_referral_membership"));
				transactionDependencies.setBusRefBeniRate(resultSet.getDouble("membership_business_ref_percentage"));
				transactionDependencies.setConRefBeniRate(resultSet.getDouble("membership_consumer_ref_percentage"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return transactionDependencies;
	}
	
	public static int getRefereeId(int trans_con_accout){
		int  getRefereeId=0;
	try {
		connection=DBConnection.getConnection(); 
		Statement statement=connection.createStatement();
		String sqlQuery=sqlRefereeId+""+trans_con_accout;
		ResultSet resultSet= statement.executeQuery(sqlQuery);
		if(resultSet.next()){
			getRefereeId=resultSet.getInt("referred_by_account_id");
		}
	} catch (Exception e) {

		e.printStackTrace();
	}
	return getRefereeId;
	}
	public static void main(String[] args) {
		System.out.println(" "+getRefereeId(62));
	}
	
}
