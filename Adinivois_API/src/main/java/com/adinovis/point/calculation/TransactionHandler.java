package com.adinovis.point.calculation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class TransactionHandler {
	
	public TransactionHandler() {
		
	}
	
	public void onPending(String transactionID) {
		// transaction is just submitted. It is in pending state. 
		
	}
	
	public Transaction getTransaction (int transactionID) {
		return PointService.getTransaction(transactionID);
		
	}
	
	public int saveTransaction (Transaction txToSave) {
		
		// Map the Transaction POJO to Query Objects
		// return the success or failure;
		return SUCCESS;
		
	}
	
	public TransactionDependencies getTransactionDependencies (int transactionID, Transaction trans) {
		// Transaction dependencies are 
		//  Transaction date - business discount , consumerReferee, consumerRefereeMembership, ConRefBeniRate
		//                                         businessReferee, businessRefereeMembership, busRefBeniRate
		TransactionDependencies tDeps = PointService.getTransactionDependencies(transactionID,null);
			
		// get the data from Database for the transaction date**** (not today), and populate into TransactionDependencies POJO
		
		// return TransactionDependencies POJO;
		return tDeps;
	}
	
	public void onApproved (int transactionID) {
		// Admin user logged in and approved the transaction. The event handler sent the event to TransactionHandler
		// calculate the referral and their memberships. Calculate the points that this transaction creates for every one
		
		
		Transaction txApproved = getTransaction (transactionID);
		TransactionDependencies tDepsApproved = getTransactionDependencies (transactionID, txApproved);
		txApproved.businessRefereeID = tDepsApproved.getBusinessRefereeAccountID();
		txApproved.consumerRefereeID = tDepsApproved.getConsumerRefereeAccountID();
		txApproved.businessRefereeMembership = tDepsApproved.getBusinessRefereeMembership();
		txApproved.consumerRefereeMembership = tDepsApproved.getConsumerRefereeMembership();
		txApproved.discountByBusiness        = tDepsApproved.getBusinessDiscount();
		txApproved.purchasePoints            = txApproved.amountBeforeTaxAndTips 
											   * txApproved.discountByBusiness 
											   * CloudUnionConfig.growthFactor;
		// transaction.fundingStatus = NotFunded;
		// before saving - see if the funding is available. // got to Business_Account andcheck
		// if yes - change the transaction status to funded. Make the changes in Business Account
		//        - there are some funding flags also. Update them accordingly
		//        - transaction.fundingStatus = Funded (in addition to the transaction status)
		// if no  - keep the transaction in approved state and save it.
		//        - create the funding request
		saveTransaction(txApproved);
		
		// update the transaction with their details
		// Save the record
		
		
	}
	
	public void onFundingAvailable ( String businessID) {
		// This is previously approved. Basic calculations are done. 
	    // This transaction is now subjected to the "funding available" event
		Transaction[] txApproved = getTransactions (businessID, "Approved");
		int fundedCount = 0;
		for (int i=0; i <= txApproved.length; i++) {
			// old for loop; check the transaction and the funding needed; one after one
			double fundingNeeded = txApproved[i].discountByBusiness * txApproved[i].amountBeforeTaxAndTips;
			boolean fundingAvailable = checkFundingAndDeduct (fundingNeeded, businessID);
			if (! fundingAvailable) {
				// ran out of funds
				// create funding request
				// break from the for loop
				createFundingRequest (businessID);
				break;
			} else {
				// create a status update request for the transaction;
				fundedCount++; 
				txApproved[i].transactionStatus = TransactionStatus.FUNDED;				
			}
		}
		for (int fCount=0; fCount < fundedCount; fCount++) {
			// update the transactions in batch format.
		    // execute the batch
		}
		for (int fCount=0; fCount < fundedCount; fCount++) {
			// create a trigger to onFunded();
		}
	}
	
	private boolean checkFundingAndDeduct(double fundingNeeded, String businessID) {
		// TODO Auto-generated method stub
		return false;
	}

	private Transaction[] getTransactions(String businessID, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private void createFundingRequest(String businessID) {
		// TODO Auto-generated method stub
		
	}

	final int CONSUMER         = 1;
	final int BUSINESS         = 2;
	final int CONSUMER_REFEREE = 3;
	final int BUSINESS_REFEREE = 4;
	
	final int PURCHASE         = 1;
	final int SUCCESS          = 99;
	final int FAILURE          = 0;
	
	public void onFunded (int transactionID) {
		
		// when the transacion is funded; create the points count in yeilds table
		Transaction txFunded = getTransaction (transactionID);
		TransactionYields txYeildsConsumer = new TransactionYields();
		TransactionYields txYieldsConsumerReferee = new TransactionYields();
		TransactionYields txYieldsBusinessReferee = new TransactionYields();
		
		Vector<String> batchQueries = new Vector<String>();
		
		// calculate the txYieldsConsumer; there is no percentage gain for consumer. Pass 0 (bring 10 and 5 from DB)
		String yieldQuery = calculateYields (txFunded, txFunded.consumerID, CONSUMER, 0,10,5);
		// create the batch SQL statement; Add the ConsumerYeilds statement
		if (yieldQuery != null) batchQueries.add(yieldQuery);
		
		if (txFunded.businessRefereeID != null) {
			// calculate the business Referee's referral points
			// fetch his membership details
			// create thh busiReferral Yields statment - add it to batch;
			String brYieldQuery = calculateYields (txFunded, txFunded.businessRefereeID, BUSINESS_REFEREE, 
									0.1 /* take it from DB */,
									10 /* take it from busi profile */,
									5 /* Take it from settings */);
			if (brYieldQuery != null) batchQueries.add(brYieldQuery);
		}
		if (txFunded.consumerRefereeID != null) {
			// calculate the consumer referee's referral points
			// fetch his membership details
			// create the consumerReferral yields SQL statement. add it to batch;
			String crYieldQuery = calculateYields (txFunded, txFunded.consumerRefereeID, 
					CONSUMER_REFEREE, 
					1, // percent - bzsed on membership
					10, // business given discount rate
					5);
			if (crYieldQuery != null) batchQueries.add(crYieldQuery);
		}
		
		// execute the batch. All the Funded transactions have their yelds calculated
		if (batchQueries.size() > 0) {
		    // ???? Execute the Batch;
		}
	}

	public String dateToday(String format) {
		Date dNow = Calendar.getInstance().getTime();
		SimpleDateFormat sdf;
		if (format.equals("MMddyyyy")) {
			sdf = new SimpleDateFormat("MM/dd/yyyy");
		} else if (format.equals("MM/dd/yyyy")) {
				sdf = new SimpleDateFormat("MM/dd/yyyy");
		} else if (format.equals("yyyy-MM-dd")) {
			sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		} else  {
			sdf = new SimpleDateFormat("MM/dd/yyyy");
		}
		return sdf.format(dNow);
	}
	
	public String calculateYields (Transaction transToUse, String yielderID, 
							int typeOfYielder, double percentageYield,
							double businessFactorPercent, double currentCUGrowthRate) {
		
		TransactionYields tyConsumer = new TransactionYields();
		tyConsumer.consumerID = yielderID;
		tyConsumer.transactionFundedDate = Calendar.getInstance().getTime();
		// get the discount rate; get the transaction amount; get the growth factor rate;
		tyConsumer.transactionID = transToUse.getTransactionID();
		tyConsumer.transactionType = this.PURCHASE;
		
		if (typeOfYielder == CONSUMER) {
			double totalPointsAccumulated = transToUse.amountBeforeTaxAndTips 
					* businessFactorPercent
					* currentCUGrowthRate;
		    double purchasePoints = totalPointsAccumulated;
		    double pp_bf_points = transToUse.amountBeforeTaxAndTips 
					* businessFactorPercent;
		    double pp_gf_points = transToUse.amountBeforeTaxAndTips
		    		* (currentCUGrowthRate - 1);
			tyConsumer.yieldPionts.setTotal_points(totalPointsAccumulated);
			tyConsumer.yieldPionts.setPurchase_points(totalPointsAccumulated);
			tyConsumer.yieldPionts.setPp_busi_factor_points(pp_bf_points);
			tyConsumer.yieldPionts.setPp_grow_factor_points(pp_gf_points);
			
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
			
		} else if (typeOfYielder == CONSUMER_REFEREE) {
			double totalConsRefPoints = transToUse.amountBeforeTaxAndTips
					* businessFactorPercent
					* currentCUGrowthRate 
					* (percentageYield / 100);
			tyConsumer.yieldPionts.setReferral_points(totalConsRefPoints);
			tyConsumer.yieldPionts.setRp_consumer_points(totalConsRefPoints);
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
		} else if (typeOfYielder == BUSINESS_REFEREE) {
			double totalBusiRefPoints = transToUse.amountBeforeTaxAndTips
					* businessFactorPercent
					* currentCUGrowthRate 
					* (percentageYield / 100);
			tyConsumer.yieldPionts.setReferral_points(totalBusiRefPoints);
			tyConsumer.yieldPionts.setRp_consumer_points(totalBusiRefPoints);
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
		} else {
			// not covered case;
			return null;
		}
		return null; // not reachable
	}

}
