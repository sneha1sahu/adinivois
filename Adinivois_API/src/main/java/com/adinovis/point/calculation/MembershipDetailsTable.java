package com.adinovis.point.calculation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MembershipDetailsTable {

	static List<MembershipDetails> ms;
	
	public static List<MembershipDetails> getInstance() {
		if (ms==null){
			ms=new ArrayList<MembershipDetails>();
			ms = load();
		}
		return ms;
	}
	

	

	private static List<MembershipDetails> load () {
		try {

	        Connection conn = DBConnection.getConnection();
	        String query = 	"select membership_id,membership_type,membership_name,membership_for,membership_cash_conv_percentage,membership_managementfee,membership_consumer_ref_percentage,membership_business_ref_percentage,membership_monthly_fee,membership_details_effective_date,membership_details_end_date from ref_membership,cu_user_account where cu_user_account.u_membership_level=ref_membership.membership_type and u_account_id";
	        Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(query);
			
			while (rs.next()) {
				MembershipDetails membershipDetails=new MembershipDetails();
				membershipDetails.setMembership_type(rs.getInt("membership_type"));
				membershipDetails.setMembership_name(rs.getString("membership_name"));
				membershipDetails.setMembership_for(rs.getInt("membership_for"));
				membershipDetails.setMembership_cash_conv_percentage(rs.getDouble("membership_cash_conv_percentage"));
				membershipDetails.setMembership_managementfee(rs.getDouble("membership_managementfee"));
				membershipDetails.setMembership_consumer_ref_percentage(rs.getDouble("membership_consumer_ref_percentage"));
				membershipDetails.setMembership_business_ref_percentage(rs.getDouble("membership_business_ref_percentage"));
				membershipDetails.setMembership_monthly_fee(rs.getDouble("membership_monthly_fee"));
				//membershipDetails.setMembership_details_effective_date(rs.getDate("membership_details_effective_date"));
				//membershipDetails.setMembership_details_end_date(rs.getDate("membership_details_end_date"));
				ms.add(membershipDetails);
			}
			return ms;
		  } catch (Exception excep) {
			  excep.printStackTrace();
			  System.out.println("Exception in DB");
		  }
		return ms;
	}
	
}
