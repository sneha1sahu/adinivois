package com.adinovis.point.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class CalculatePoints {
   
	public static void main (String[] args) throws Exception {
		processCashPointsForTodayDB();
	}
	
	// nightly job, that kicks in 
	public void processCashPoints () throws Exception {
		// read the current points as of this morning
		// - Read the database table (in this case - a file)
		processCashPointsForTodayDB();
		
		// once the cash points are done:
		// EOD - create the aggregate by type of trans in Daily Summary Table
		
		// read / get the Redeems for today
		// Calculate the net points resulted from redeem and keep it in Transaction table
		
	}
	
	
	
	public static void processCashPointsForTodayDB() throws Exception {
		// calculate cash points and update in Transactions Table with -ve points 
		// The -ve points decide on where to deduct the cash points from 
		// Calculate the Transaction fee. Adjust the record / transaction table entry
		// calculate the resulting points and  insert in DailySummaryTable as Cash Transaction Type 
		Connection connection=DBConnection.getConnection();
		
		PreparedStatement preparedStatement=connection.prepareStatement(PointService.cu_points);
		ResultSet resultSet= preparedStatement.executeQuery();
		HashMap <String, Points> todayPoints = new HashMap();
		while (resultSet.next()) {
			Points points=new Points();  
			points.setConsumer_name(resultSet.getString("cu_points.u_account_id"));
			points.setUser_type(resultSet.getString("user_type_name"));
			points.setTotal_points(resultSet.getDouble("p_sky_points"));
			points.setPurchase_points(resultSet.getDouble("p_sky_purchase_points"));
			points.setReferral_points(resultSet.getDouble("p_sky_referral_points"));
			points.setPp_busi_factor_points(resultSet.getDouble("p_sky_purchase_business_factor_points"));
			points.setPp_grow_factor_points(resultSet.getDouble("p_sky_purchase_growth_factor_points"));
			if(points.getUser_type().equalsIgnoreCase("Consumer")){
				points.setRp_consumer_points(resultSet.getDouble("p_sky_referral_growth_factor_points"));
			}
			if(points.getUser_type().equalsIgnoreCase("Business")){
				points.setRp_business_points(resultSet.getDouble("p_sky_referral_growth_factor_points"));
			}
			points.setMgmt_fee(resultSet.getDouble("membership_managementfee"));
			points.setCash_conv(resultSet.getDouble("membership_cash_conv_percentage"));
			points.setLt_cash_points(resultSet.getDouble("p_ltime_cash_points"));
			points.setLt_total_points(resultSet.getDouble("p_ltime_sky_points"));
			points.setLt_redeemed_points(resultSet.getDouble("p_ltime_redeemed_points"));
			todayPoints.put(points.getConsumer_name(), points);
		}
		
		// Have the hashMap of today's points
		
		// calculate the record for Transactions
		calculateTheCashPointRecords (todayPoints, null, null);
		// calculate the record for Daily Summary Table
	}
	
	public static double getMembership (String type, double value) {
		if (type.equalsIgnoreCase("MGMT_FEE")) {
			// ping DB and get mgmt fee for his membership
			return (value * 0.1); // 10%
		}
		else if (type.equalsIgnoreCase("CASH_CONV")) {
			// get the cash conversion rate;
			return (value * 0.0001); // 1/10000th
		}
		return (double) 1.0;
	}
	
	// get double value, rounded and two decimal
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static void calculateTheCashPointRecords (HashMap <String, Points> todayPoints,
										HashMap <String, Points> transPoints,
										HashMap <String, Points> summaryPoints) {
		// to calculate the cashpoints, we need the membership details and the mgmtfee details
		Set<String> hsKeys = todayPoints.keySet();
		Iterator<String> hsKeysIte = hsKeys.iterator();
		while (hsKeysIte.hasNext()) {
			// get the value of the key
			String userId = hsKeysIte.next(); // Now it is String. Change it as needed
			// get his points
			Points thisUserPoints = todayPoints.get(userId);
			if (thisUserPoints == null) return;
			// get the calculation params for that user
			double membershipMgmtFee  = getMembership("MGMT_FEE", thisUserPoints.getMgmt_fee());
			double membershipCashConv = getMembership("CASH_CONV", thisUserPoints.getCash_conv());
			
			//double membershipMgmtFee  =thisUserPoints.getMgmt_fee();
			//double membershipCashConv = thisUserPoints.getCash_conv();
			
			// now - perform the calculation. Rounding at 0.5 is needed.
			double cashPointsForThisUser = (double) thisUserPoints.getTotal_points() * membershipCashConv;
			cashPointsForThisUser = Math.rint(cashPointsForThisUser);
			
			// calculate mgmt fee and round it
			double mgmtFeeForThisUser = (double) (cashPointsForThisUser * membershipMgmtFee);
			mgmtFeeForThisUser = round(mgmtFeeForThisUser, 2);
			
			// assign the values to // transPoints
			Points transLocalPoints = getTransCashPointNegNumbers (thisUserPoints, cashPointsForThisUser, mgmtFeeForThisUser);
			System.out.println("Yesterday        = " + thisUserPoints.toString());
			System.out.println("New Trasnsaction = " + transLocalPoints.toString());
			
			Points cashSummaryPoints = getSummaryCashPointNumbers (thisUserPoints, 
																transLocalPoints);
			System.out.println("Summary          = " + cashSummaryPoints.toString());
		}
	}
	
	// provide yesterday's points, and calculated CashPoint Records
	public static Points getSummaryCashPointNumbers (Points todayPoints, Points transCashPoints) {
		// subtract (actually add) transcashPoints to todayPoints to get the Summary 
		// this shows remaining points etc as a result of Cash Point Calculation
		Points summaryTPoints = new Points();
		summaryTPoints.add(todayPoints);
		summaryTPoints.add(transCashPoints);
		return summaryTPoints;
		
	}
	
	public static Points getTransCashPointNegNumbers (Points todayPoints, double cashPointsGross, double mgmtFee) {
		Points newTransPoints = new Points(); // all zeros
		newTransPoints.setTotal_points(-1 * cashPointsGross);
		
		double stillToSubtract = cashPointsGross;
		double gf_deducted = 0;
		double bf_deducted = 0;
		// Subtract from PurchasePoints First
		double pp_Points = todayPoints.getPurchase_points();
		if (pp_Points > 0) {
			// there is some thing to deduct;
			if ( pp_Points >= stillToSubtract) {
				// all the points are available to subtract here
				newTransPoints.setPurchase_points(-1 * stillToSubtract);
				// now see if bf_points and gf_points
				if (todayPoints.getPp_busi_factor_points() >= stillToSubtract) {
					// all the points are available in here // no need to touch growth
					newTransPoints.setPp_busi_factor_points(-1 * stillToSubtract);
					bf_deducted += stillToSubtract;
					stillToSubtract = 0;
				}
				else {
					// subtract the available points, from busi_factor and rest from growth
					newTransPoints.setPp_busi_factor_points(-1 * todayPoints.getPp_busi_factor_points());
					stillToSubtract -= todayPoints.getPp_busi_factor_points();
					newTransPoints.setPp_grow_factor_points(-1 * stillToSubtract);
					bf_deducted += todayPoints.getPp_busi_factor_points();
					gf_deducted += stillToSubtract;
				}
				stillToSubtract = 0;
			} else {
				// some purchase points are available here
				// subtract all the purchase points and go to referral points
				// Basically, making it zero. Calculate the numbers with -1 and add
				stillToSubtract -= pp_Points;
				newTransPoints.setPurchase_points(-1 * todayPoints.getPurchase_points());
				newTransPoints.setPp_busi_factor_points(-1 * todayPoints.getPp_busi_factor_points());
				newTransPoints.setPp_grow_factor_points(-1 * todayPoints.getPp_grow_factor_points());
				bf_deducted += todayPoints.getPp_busi_factor_points();
				gf_deducted += todayPoints.getPp_grow_factor_points();
			}
		}
		// now irrespective of other conditions, if stillToSubtract has noZero value
		// deduct it from Referral Points. All referral points are gf_points
		if (stillToSubtract > 0) {
			// the points should be there. No need to check.
			newTransPoints.setReferral_points(-1 * stillToSubtract);
			// First subtract from the Consumer RP points
			if (todayPoints.getRp_consumer_points() >= stillToSubtract) {
				// subtract all and update gf_deducted count
				newTransPoints.setRp_consumer_points(-1 * stillToSubtract);
				gf_deducted += stillToSubtract;
				stillToSubtract = 0;
				
			} else {
				// the still to subtract > the consumer points.
				newTransPoints.setRp_consumer_points(-1 * todayPoints.getRp_consumer_points());
				stillToSubtract -= todayPoints.getRp_consumer_points();
				gf_deducted += todayPoints.getRp_consumer_points();
				
				// subtract remaining from the business referral points.
				newTransPoints.setRp_business_points(-1 * stillToSubtract);
				stillToSubtract = 0;
				gf_deducted += stillToSubtract;
			}
	
		}
		
		// now calculate net cash points fee // mgmtFee
		double netCashPoints = cashPointsGross - mgmtFee;
		newTransPoints.setCash_points(netCashPoints);
		
		// see where to adjust the mgmt fee
		// if today's GF of cashPoints + newTrans GF deucted
		if ((todayPoints.getCashp_grow_factor_points() + gf_deducted) > mgmtFee) {
			// enough GF points // deduct it and continue
			newTransPoints.setCashp_grow_factor_points(gf_deducted - mgmtFee);
		} else {
			// not enough GF points // deduct whatever they have and then deduct BF
			newTransPoints.setCashp_grow_factor_points((todayPoints.getCashp_grow_factor_points() 
									+ gf_deducted));
			newTransPoints.setCashp_busi_factor_points(bf_deducted - mgmtFee - (todayPoints.getCashp_grow_factor_points() 
									+ gf_deducted));
		}
		
		// set life time cashPoints
		newTransPoints.setLt_cash_points(netCashPoints);
		newTransPoints.setLt_cp_busi_factor_points(newTransPoints.getLt_cp_busi_factor_points());
		newTransPoints.setLt_cp_grow_factor_points(newTransPoints.getLt_cp_grow_factor_points());
		newTransPoints.setMgmt_fee(mgmtFee);
		
		return newTransPoints;
	}
	
}

