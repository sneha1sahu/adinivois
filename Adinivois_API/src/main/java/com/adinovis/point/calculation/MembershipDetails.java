package com.adinovis.point.calculation;

import java.util.Date;

public class MembershipDetails {
	
	private int membership_type;
	private String membership_name;
	private int membership_for;
	private double membership_cash_conv_percentage;
	private double membership_managementfee;
	private double membership_consumer_ref_percentage;
	private double membership_business_ref_percentage;
	private double membership_monthly_fee;
	private Date membership_details_effective_date;
	private Date membership_details_end_date;
	public int getMembership_type() {
		return membership_type;
	}
	public void setMembership_type(int membership_type) {
		this.membership_type = membership_type;
	}
	public String getMembership_name() {
		return membership_name;
	}
	public void setMembership_name(String membership_name) {
		this.membership_name = membership_name;
	}
	public int getMembership_for() {
		return membership_for;
	}
	public void setMembership_for(int membership_for) {
		this.membership_for = membership_for;
	}
	public double getMembership_cash_conv_percentage() {
		return membership_cash_conv_percentage;
	}
	public void setMembership_cash_conv_percentage(double membership_cash_conv_percentage) {
		this.membership_cash_conv_percentage = membership_cash_conv_percentage;
	}
	public double getMembership_managementfee() {
		return membership_managementfee;
	}
	public void setMembership_managementfee(double membership_managementfee) {
		this.membership_managementfee = membership_managementfee;
	}
	public double getMembership_consumer_ref_percentage() {
		return membership_consumer_ref_percentage;
	}
	public void setMembership_consumer_ref_percentage(double membership_consumer_ref_percentage) {
		this.membership_consumer_ref_percentage = membership_consumer_ref_percentage;
	}
	public double getMembership_business_ref_percentage() {
		return membership_business_ref_percentage;
	}
	public void setMembership_business_ref_percentage(double membership_business_ref_percentage) {
		this.membership_business_ref_percentage = membership_business_ref_percentage;
	}
	public double getMembership_monthly_fee() {
		return membership_monthly_fee;
	}
	public void setMembership_monthly_fee(double membership_monthly_fee) {
		this.membership_monthly_fee = membership_monthly_fee;
	}
	public Date getMembership_details_effective_date() {
		return membership_details_effective_date;
	}
	public void setMembership_details_effective_date(Date membership_details_effective_date) {
		this.membership_details_effective_date = membership_details_effective_date;
	}
	public Date getMembership_details_end_date() {
		return membership_details_end_date;
	}
	public void setMembership_details_end_date(Date membership_details_end_date) {
		this.membership_details_end_date = membership_details_end_date;
	}
	
	
}
