package com.adinovis.point.calculation;

import java.util.Date;

public class Transaction {

	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public Date getActualDate() {
		return actualDate;
	}
	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}
	public String getBusinessID() {
		return businessID;
	}
	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}
	public String getConsumerID() {
		return consumerID;
	}
	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}
	public double getAmountOfTransaction() {
		return amountOfTransaction;
	}
	public void setAmountOfTransaction(double amountOfTransaction) {
		this.amountOfTransaction = amountOfTransaction;
	}
	public double getAmountBeforeTaxAndTips() {
		return amountBeforeTaxAndTips;
	}
	public void setAmountBeforeTaxAndTips(double amountBeforeTaxAndTips) {
		this.amountBeforeTaxAndTips = amountBeforeTaxAndTips;
	}
	public String getBusinessRefereeID() {
		return businessRefereeID;
	}
	public void setBusinessRefereeID(String businessRefereeID) {
		this.businessRefereeID = businessRefereeID;
	}
	public String getConsumerRefereeID() {
		return consumerRefereeID;
	}
	public void setConsumerRefereeID(String consumerRefereeID) {
		this.consumerRefereeID = consumerRefereeID;
	}
	public int getBusinessRefereeMembership() {
		return businessRefereeMembership;
	}
	public void setBusinessRefereeMembership(int businessRefereeMembership) {
		this.businessRefereeMembership = businessRefereeMembership;
	}
	public int getConsumerRefereeMembership() {
		return consumerRefereeMembership;
	}
	public void setConsumerRefereeMembership(int consumerRefereeMembership) {
		this.consumerRefereeMembership = consumerRefereeMembership;
	}
	public double getDiscountByBusiness() {
		return discountByBusiness;
	}
	public void setDiscountByBusiness(double discountByBusiness) {
		this.discountByBusiness = discountByBusiness;
	}
	public int getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(int transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public int getWf_status() {
		return wf_status;
	}
	public void setWf_status(int wf_status) {
		this.wf_status = wf_status;
	}
	public double getPurchasePoints() {
		return purchasePoints;
	}
	public void setPurchasePoints(double purchasePoints) {
		this.purchasePoints = purchasePoints;
	}
	
	String transactionID;
	Date   postedDate;
	Date   actualDate;
	String businessID;
	String consumerID;
	double amountOfTransaction; // Grand Total
	double amountBeforeTaxAndTips; // Sub Total
	String businessRefereeID;
	String consumerRefereeID;
	int    businessRefereeMembership;
	int    consumerRefereeMembership;
	double discountByBusiness;
	int    transactionStatus;
	double purchasePoints;
	int    wf_status;
	
}
