package com.adinovis.point.calculation;

public class TransactionDependencies {
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public double getBusinessDiscount() {
		return businessDiscount;
	}
	public void setBusinessDiscount(double businessDiscount) {
		this.businessDiscount = businessDiscount;
	}
	public String getConsumerRefereeAccountID() {
		return consumerRefereeAccountID;
	}
	public void setConsumerRefereeAccountID(String consumerRefereeAccountID) {
		this.consumerRefereeAccountID = consumerRefereeAccountID;
	}
	public String getBusinessRefereeAccountID() {
		return businessRefereeAccountID;
	}
	public void setBusinessRefereeAccountID(String businessRefereeAccountID) {
		this.businessRefereeAccountID = businessRefereeAccountID;
	}
	public int getConsumerRefereeMembership() {
		return consumerRefereeMembership;
	}
	public void setConsumerRefereeMembership(int consumerRefereeMembership) {
		this.consumerRefereeMembership = consumerRefereeMembership;
	}
	public double getConRefBeniRate() {
		return conRefBeniRate;
	}
	public void setConRefBeniRate(double conRefBeniRate) {
		this.conRefBeniRate = conRefBeniRate;
	}
	public int getBusinessRefereeMembership() {
		return businessRefereeMembership;
	}
	public void setBusinessRefereeMembership(int businessRefereeMembership) {
		this.businessRefereeMembership = businessRefereeMembership;
	}
	public double getBusRefBeniRate() {
		return busRefBeniRate;
	}
	public void setBusRefBeniRate(double busRefBeniRate) {
		this.busRefBeniRate = busRefBeniRate;
	}
	
	String    transactionID;
	double    businessDiscount;
	String    consumerRefereeAccountID;
	String    businessRefereeAccountID;
	int    consumerRefereeMembership;
	int    businessRefereeMembership;
	double    busRefBeniRate;
	double    conRefBeniRate;
		
}
