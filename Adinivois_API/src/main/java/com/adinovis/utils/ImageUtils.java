package com.adinovis.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.adinovis.common.ConfigService;
import com.adinovis.common.ImageConstant;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

public class ImageUtils {

	@SuppressWarnings("deprecation")
	public static String getSecureUrl(HttpServletRequest request, String moduleName, String imageData, String uId) {
		String fullPath = null;
		if (StringUtils.isNotEmpty(imageData)) {
			String extention = (imageData!=null)?imageData.split(";")[0].split("/")[1]:"jpg";
			
			String imageName = Calendar.getInstance().getTimeInMillis() + "."+extention;
			String base64Image = imageData.split(",")[1];
			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
			String fileLocation = new File(request.getRealPath("/")).getParent() + "/" + moduleName;
			File dir = new File(fileLocation + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			try {

				if (imageBytes.length > 0) {
					File serverFile = new File(dir.getAbsolutePath() + File.separator + imageName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(imageBytes);
					stream.close();
					fullPath = connectAzureBlob(fileLocation, imageName);
					System.out.println("here is fullpath...."+fullPath);
				}
			} catch (Exception e) {
				e.printStackTrace();
				fullPath = null;
			}

		}

		return fullPath;
	}
	
	@SuppressWarnings("deprecation")
	public static String getUserProfileUrl(HttpServletRequest request,String moduleName,String imageData,String uId){
		String fullPath = null;
		if(StringUtils.isNotEmpty(imageData)){
			String extention = (imageData!=null)?imageData.split(";")[0].split("/")[1]:"jpg";
			
			String imageName = uId+"."+extention;
			String base64Image = imageData.split(",")[1];
			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
			String fileLocation=new File(request.getRealPath("/")).getParent()+"/"+moduleName;
			File dir = new File(fileLocation + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			try {

				if (imageBytes.length > 0) {
					File serverFile = new File(dir.getAbsolutePath() + File.separator + imageName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(imageBytes);
					stream.close();
					fullPath = connectAzureBlob(fileLocation, imageName);
					System.out.println("here is fullpath...."+fullPath);
				}
			} catch (Exception e) {
				fullPath = null;
			}	
		}
		

		return fullPath;
	}
	
	
	
	@SuppressWarnings("deprecation")
	public static String getBusinessProfileUrl(HttpServletRequest request,String moduleName,String imageData,String uId){
		String fullPath = null;
		if(StringUtils.isNotEmpty(imageData)){
			String extention = (imageData!=null)?imageData.split(";")[0].split("/")[1]:"jpg";
			
			String imageName = uId+"."+extention;
			String base64Image = imageData.split(",")[1];
			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
			String fileLocation=new File(request.getRealPath("/")).getParent()+"/"+moduleName;
			File dir = new File(fileLocation + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			try {

				if (imageBytes.length > 0) {
					File serverFile = new File(dir.getAbsolutePath() + File.separator + imageName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(imageBytes);
					stream.close();
					fullPath = connectAzureBlob(fileLocation, imageName);
					System.out.println("here is fullpath...."+fullPath);
				}
			} catch (Exception e) {
				fullPath = null;
			}	
		}
		

		return fullPath;
	}
	@SuppressWarnings("deprecation")
	public static String getSuggestionUrl(HttpServletRequest request,String moduleName,String imageData){
		String fullPath = null;
		if(StringUtils.isNotEmpty(imageData)){
			String extention = (imageData!=null)?imageData.split(";")[0].split("/")[1]:"jpg";
			String imageName = "suggestion-"+Calendar.getInstance().getTimeInMillis() + "."+extention;
			String base64Image = imageData.split(",")[1];
			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
			String fileLocation=new File(request.getRealPath("/")).getParent()+"/"+moduleName;
			File dir = new File(fileLocation + File.separator);
			if (!dir.exists())
				dir.mkdirs();
			try {

				if (imageBytes.length > 0) {
					File serverFile = new File(dir.getAbsolutePath() + File.separator + imageName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(imageBytes);
					stream.close();
					fullPath = connectAzureBlob(fileLocation, imageName);
					System.out.println("here is fullpath...."+fullPath);
				}
			} catch (Exception e) {
				fullPath = null;
			}	
		}
		

		return fullPath;
	}
	
	@SuppressWarnings("deprecation")
	public static String generateQrCode(HttpServletRequest request,String refferalCode,String uId) {
		String fileLocation=new File(request.getRealPath("/")).getParent()+"/"+ImageConstant.QR_CODE_IMAGE_LOCATON;
		String qrFile = refferalCode+uId+".png";
		String filePath = null;
		int size = 250;
		String fileType = "png";
		File dir = new File(fileLocation + File.separator);
		if (!dir.exists())
			dir.mkdirs();
		
		File serverFile = new File(dir.getAbsolutePath() + File.separator + qrFile);
		
		try {
			createQRImage(serverFile, refferalCode, size, fileType);
			filePath =ImageConstant.IMAGE_BASE_PATH +ImageConstant.REFERRAL+"/"+ qrFile;
		} catch (Exception  e) {
			filePath = null;
		}
		
	return filePath;

	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void createQRImage(File qrFile, String qrCodeText, int size,
			String fileType) throws WriterException, IOException {
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Hashtable hintMap = new Hashtable();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText,
				BarcodeFormat.QR_CODE, size, size, hintMap);
		// Make the BufferedImage that are to hold the QRCode
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
				BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		// Paint and save the image using the ByteMatrix
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		ImageIO.write(image, fileType, qrFile);
	}
	
	
	public static String connectAzureBlob(String fileLocation, String imageName){
		String ConnectionString = ConfigService.getProperty("storage.url");	
		CloudBlockBlob blob;
		try {
			CloudStorageAccount account = CloudStorageAccount.parse(ConnectionString);
			System.out.println("here its connected....");
			CloudBlobClient blobClient = account.createCloudBlobClient();
			CloudBlobContainer container = blobClient.getContainerReference("common");
			blob = container.getBlockBlobReference(imageName);
			File source = new File(fileLocation + "/" + imageName);
			blob.upload(new FileInputStream(source), source.length());
			System.out.println("Inserted");
			return ""+blob.getUri();
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		String str="data:image/png;biVBORw0KGgoAAAANSUhEUgAAA/QAAAIICAYAAADAAqW0AAAMFWlDQ1BJQ0MgUHJvZmlsZQAASImVVwdUk8kWnr+kEBJaIAJSQm+";
		String extention = (str!=null)?str.split(";")[0].split("/")[1]:"jpg";
		
		System.out.println(extention);
	}
	
}
