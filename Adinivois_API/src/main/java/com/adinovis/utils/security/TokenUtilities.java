package com.adinovis.utils.security;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.adinovis.context.CloudunionAutoServices;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class TokenUtilities {

	private static final String ALGORITHM = "AES";
	public static final int InvalidToken = 0;
	public static final int ValidToken = 1;
	public static final int InvalidUser = 2;
	public static final int InvalidRole = 3;
	public static final int  ValidRole= 4;
	// constant used to encrypt or decrypt the text to provide security
	private static final String KEY_ENCRYPT_DESCRYPT = "APIWS-CLOUDUNION";
	private static final byte[] keyValue = KEY_ENCRYPT_DESCRYPT.getBytes();

	public static String createToken(String valueToEnc) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encValue = c.doFinal(valueToEnc.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encValue);
		return encryptedValue;
	}

	public static String accessToken(String encryptedValue) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
		byte[] decValue = c.doFinal(decordedValue);
		String value = new String(decValue, 0, decValue.length, "UTF-8");
		String decryptedValue = value.split("_#_")[1];
		return decryptedValue;
	}

	public static String userSession(String encryptedValue) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
		byte[] decValue = c.doFinal(decordedValue);
		String value = new String(decValue, 0, decValue.length, "UTF-8");
		String decryptedValue = value.split("_#_")[0];
		return decryptedValue;
	}

	public static String refreshToken(String encryptedValue) throws Exception {
		encryptedValue = refresh(encryptedValue);
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
		byte[] decValue = c.doFinal(decordedValue);
		String value = new String(decValue, 0, decValue.length, "UTF-8");
		String decryptedValue = value.split("_#_")[1];
		return decryptedValue;
	}

	public static String refresh(String encryptedValue) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue, 0, decValue.length, "UTF-8");
		return decryptedValue;
	}

	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGORITHM);
		return key;
	}

	public static void checkToken(String token, String tokentype) throws Exception {

		switch (tokentype) {
		case "A":
			System.out.println("isAccessToken :" + accessToken(token));
			break;
		case "B":
			System.out.println("isRefresh_token :" + accessToken(refresh(token)));
			break;

		default:
			break;
		}

	}

	public static int isValidAccessToken(String requestUri, String access_token) {
		int isValidAccessToken = InvalidToken;
		try {
			String uId = accessToken(access_token);
			if (requestUri.contains(uId)) {
				String currentUserSession = CloudunionAutoServices.cloudunionCommonService().userSession(uId);
				if (currentUserSession.equals(userSession(access_token))) {
					isValidAccessToken = ValidToken;	
				}

			} else {
				isValidAccessToken = InvalidUser;
			}
		} catch (Exception e) {
			isValidAccessToken = InvalidToken;
		}
		return isValidAccessToken;
	}

	public static boolean isRefreshTokenValid(String requestUri, String refresh_token) {
		try {

			refresh_token = refresh(refresh_token);
			refresh_token = accessToken(refresh_token);
			if (requestUri.contains(refresh_token))
				return true;
			else
				return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static int isUserRole(String requestUri, String access_token) {
		int isUserRole = InvalidRole;
		try {

			String uId = accessToken(access_token);
			String userType = CloudunionAutoServices.cloudunionCommonService().getUserType(uId);
			switch (userType) {
			case "Consumer":
				for (String role : consmerRole()) {
					if (requestUri.contains(role)) {
						isUserRole = ValidRole;
					}
				}
				break;
			case "Business":
				for (String role : businessRole()) {
					if (requestUri.contains(role)) {
						isUserRole = ValidRole;
					}
				}
				break;
			case "CUAdmin":
				for (String role : adminRole()) {
					if (requestUri.contains(role)) {
						isUserRole = ValidRole;
					}
				}
				break;
			}

		} catch (Exception e) {
			return isUserRole;
		}
		return isUserRole;
	}

	public static List<String> consmerRole() {
		List<String> role = new ArrayList<String>();
		role.add("common");
		role.add("user");
		role.add("transaction");
		role.add("consumer");
		return role;
	}

	public static List<String> businessRole() {
		List<String> role = new ArrayList<String>();
		role.add("common");
		role.add("user");
		role.add("transaction");
		role.add("business");
		role.add("consumer");
		return role;
	}

	public static List<String> adminRole() {
		List<String> role = new ArrayList<String>();
		role.add("admin");
		return role;
	}

}