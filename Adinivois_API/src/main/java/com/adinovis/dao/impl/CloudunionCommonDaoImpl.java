package com.adinovis.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;

import com.adinovis.common.CinchQueryConstants;
import com.adinovis.common.DateUtils;
import com.adinovis.common.ExceptionConstants;
import com.adinovis.dao.CloudunionCommonDao;
import com.adinovis.domain.Login;
import com.adinovis.domain.LoginwithSocial;
import com.adinovis.domain.Social;
import com.adinovis.domain.UserBean;
import com.adinovis.exception.DataAccessException;
import com.adinovis.exception.DataBaseException;

public class CloudunionCommonDaoImpl extends HibernateDaoSupport implements CloudunionCommonDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudunionCommonDaoImpl.class);
	@SuppressWarnings("unchecked")
	@Override
	public List<Login> getloginId(String userid) {
		try {
			LOGGER.debug("getloginId begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.getloginId);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());

			subsQuery.setParameter("userid", userid);
			List<Object[]> eventsSearch = subsQuery.list();
			List<Login> login = new ArrayList<Login>();
			Login loginData;
			for (Object[] arr : eventsSearch) {
				loginData = new Login();
				loginData.setLoginId((String) arr[0]);
				loginData.setNewpassword((String) arr[1]);
				login.add(loginData);
			}
			LOGGER.debug("getloginId end");
			return login;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getloginId", ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Login> getloginthroughsocial(String loginId, String password) {
		try {
			LOGGER.debug("getloginId begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.getloginthroughsocial);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());

			subsQuery.setParameter("loginId", loginId);
			subsQuery.setParameter("password", password);
			List<Object[]> eventsSearch = subsQuery.list();
			List<Login> login = new ArrayList<Login>();
			Login loginData;
			for (Object[] arr : eventsSearch) {
				loginData = new Login();
				loginData.setLoginId((String) arr[0]);
				loginData.setNewpassword((String) arr[1]);
				// loginData.setLogintype((Integer) arr[2]);
				login.add(loginData);
			}
			LOGGER.debug("getloginId end");
			return login;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getloginId", ex);
		}
	}

	@Override
	public Integer updatelogin(LoginwithSocial loginid) {
		try {
			LOGGER.debug("In registration dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.sp_UPDATE_LOGIN_TYPE)
					.setParameter("loginId", loginid.getLoginId())
					.setParameter("loginType", loginid.getLogintype())
					//.setParameter("socialId", loginid.getSocialid())
					.setParameter("deviceToken", loginid.getDeviceToken())
					.setParameter("osType", loginid.getOsType());
			/*
			 * BigInteger update = (BigInteger) subsQuery.list().get(0);
			 * if(update){ LOGGER.debug("In registration dao end"); return true;
			 * }
			 */

			int update = subsQuery.executeUpdate();
			if (update > 0) {
				LOGGER.debug("In update comment dao begin");
				return update;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @registration", ex);
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Social> getloginsocial(String loginId) {
		try {
			LOGGER.debug("getloginId begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.getloginsocial);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());

			subsQuery.setParameter("loginId", loginId);
			List<Object[]> eventsSearch = subsQuery.list();
			List<Social> login = new ArrayList<Social>();
			Social loginData;
			for (Object[] arr : eventsSearch) {
				loginData = new Social();
				loginData.setUid((Integer) arr[0]);
				loginData.setLoginId((String) arr[1]);
				// loginData.setLogintype((Integer) arr[2]);
				login.add(loginData);
			}
			LOGGER.debug("getloginId end");
			return login;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getloginId", ex);
		}
	}

	@Override
	public List<UserBean> getLoginBean(String loginId) {
		try {
			LOGGER.debug("getLoginBean begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.getLoginBeanByUId);

			if (loginId.contains("@")) {
				superiorSb = new StringBuffer(CinchQueryConstants.getLoginBeanByLoginId);
			}
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());

			subsQuery.setParameter("loginId", loginId);
			@SuppressWarnings("unchecked")
			List<Object[]> eventsSearch = subsQuery.list();
			List<UserBean> login = new ArrayList<UserBean>();
			UserBean loginData;
			for (Object[] arr : eventsSearch) {
				loginData = new UserBean();
				loginData.setLoginId((String) arr[0]);
				loginData.setUid((Integer) arr[1]);
				loginData.setPassword((String) arr[2]);
				loginData.setFirstName((String) arr[3]);
				loginData.setLastName((String) arr[4]);
				loginData.setDeviceToken((String) arr[5]);
				loginData.setOsType((Integer) arr[6]);
				loginData.setUserType((Integer) arr[7]);
				loginData.setReferralCode((String) arr[8]);
				loginData.setReferralQr((String) arr[9]);
				loginData.setProfileUrl((String) arr[10]);
				loginData.setShortDesc((String) arr[11]);
				loginData.setLongDesc((String) arr[12]);
				loginData.setBusinessName((String) arr[13]);
				loginData.setAcocuntId(Integer.parseInt(String.valueOf(arr[14])));
				loginData.setLoginType((Integer) arr[15]);
				loginData.setJoiningDate(DateUtils.convertDateToString((Date)arr[16],DateUtils.CLOUDUNION_DATE));
				loginData.setMemberShipType(String.valueOf(arr[17]));
				login.add(loginData);
			}
			LOGGER.debug("getLoginBean end");
			return login;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getloginId", ex);
		}
	}
	
	
	@Override
	public int updateUserSession(String uId, String usersession) {
		try {
			LOGGER.debug("updateProfileImage dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.updateUserSession).setParameter("u_id", uId)
					.setParameter("usersession", usersession);
			LOGGER.debug("updateUserSession dao end");
			return subsQuery.executeUpdate();
		} catch (CannotCreateTransactionException e) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @updateProfileImage", ex);
		}
	}


}
