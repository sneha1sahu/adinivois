/**
 * Program Name: EngagementDaoImpl
 * 
 * Program Description / functionality: 
 * 
 * Modules Impacted: 
 * 
 * Tables affected: 
 * 
 * Developer Created /Modified Date Purpose
 *******************************************************************************
 * Srivalli                                                                                      * 
 * 
 * * Associated Defects Raised :
 *
 */
package com.adinovis.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;
import com.adinovis.common.CinchQueryConstants;
import com.adinovis.common.ExceptionConstants;
import com.adinovis.dao.EngagementDao;
import com.adinovis.domain.Allengagement;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.Engagement;
import com.adinovis.domain.EngagementYear;
import com.adinovis.domain.Engagementcountdetail;
import com.adinovis.domain.Engagementdetails;
import com.adinovis.domain.Engagementtype;
import com.adinovis.exception.DataAccessException;
import com.adinovis.exception.DataBaseException;

public class EngagementDaoImpl extends HibernateDaoSupport implements EngagementDao{
	private static final Logger LOGGER = LoggerFactory.getLogger(EngagementDaoImpl.class);
	  
	@Override
	public boolean updateengagementdetails(Engagement engagement) {
		try{
	        LOGGER.debug("In updateclientprofile dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.UPDATEENGAGAEMENT)
	        		.setParameter("Pengagementsid", engagement.getPengagementsid())
                    .setParameter("Pengagementname", engagement.getPengagementname())
	        		.setParameter("Pclientfirmid", engagement.getPclientfirmid())
	                .setParameter("Pengagementtypeid", engagement.getPengagementtypeid())
	                .setParameter("Pcompliation", engagement.getPcompliation())
	                .setParameter("Psubenties", engagement.getPsubenties())
	                .setParameter("Pyearend", engagement.getPyearend())
	                .setParameter("Padditionalinfo",engagement.getPadditionalinfo())
                    .setParameter("Puseraccoutid_roleid_list",engagement.getPuseraccoutidroleidlist())
	                .setParameter("Ploginid",engagement.getPloginid());
                
	        int update = subsQuery.executeUpdate();
	        if(update != 0){
	            LOGGER.debug("In updateengagement comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @updateengagement", ex);
	    }
		return false;
	}
	@SuppressWarnings("unchecked")
   	public List<Engagementtype> getengagement() {
   		try {
   			LOGGER.debug("getengagement begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETENGAGEMENT);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Engagementtype> engagementtypes = new ArrayList<Engagementtype>();
   			Engagementtype engagementtypeData;
   			for (Object[] arr : eventsSearch) {
   				engagementtypeData = new Engagementtype();
   				engagementtypeData.setEngagementtypeid((BigInteger) arr[0]);
   				engagementtypeData.setEngagementtype((String) arr[1]);
   				engagementtypes.add(engagementtypeData);
   			}
   			LOGGER.debug("getengagement end");
   			return engagementtypes;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagement", ex);
   		}
   	}
	
	@SuppressWarnings("unchecked")
   	public List<Clientstatus> getengagementstatus() {
   		try {
   			LOGGER.debug("getengagementstatus begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETENGAGEMENTSTATUS);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Clientstatus> clientstatustypes = new ArrayList<Clientstatus>();
   			Clientstatus clientstatusData;
   			for (Object[] arr : eventsSearch) {
   				clientstatusData = new Clientstatus();
   				clientstatusData.setStatusid((BigInteger) arr[0]);
   				clientstatusData.setStatusname((String) arr[1]);
   				clientstatustypes.add(clientstatusData);
   			}
   			LOGGER.debug("getengagementstatus end");
   			return clientstatustypes;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagementstatus", ex);
   		}
   	}
	
	@Override
	public boolean deleteengagement(String engagementsid,String loginid) {
		try{
	        LOGGER.debug("In deleteengagement dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.DELETEENGAGEMENT)
	                .setParameter("Pengagementsid", engagementsid)
	                .setParameter("Ploginid", loginid);
	        int delete = subsQuery.executeUpdate();
	        if(delete != 0){
	            LOGGER.debug("In deleteengagement comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @deleteengagement", ex);
	    }
	    return false;
	}
	@SuppressWarnings("unchecked")
   	public List<Engagementdetails> getengagementdetails(int clientfirmid,String loginid) {
   		try {
   			LOGGER.debug("getengagementdetails begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETENGAGEMENTDETAIL);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("clientfirmid", clientfirmid);
   			subsQuery.setParameter("Ploginid", loginid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Engagementdetails> engagementdetailss = new ArrayList<Engagementdetails>();
   			Engagementdetails engagementdetailsData;
   			for (Object[] arr : eventsSearch) {
   				engagementdetailsData = new Engagementdetails();
   				engagementdetailsData.setEngagementsid((BigInteger) arr[0]);
   				engagementdetailsData.setClientfirmid((BigInteger) arr[1]);
   				engagementdetailsData.setUseraccountid((BigInteger) arr[2]);
   				engagementdetailsData.setBusinessname((String) arr[3]);
   				engagementdetailsData.setContactperson((String) arr[4]);
   				engagementdetailsData.setIncorporationdate((Date) arr[5]);
   				engagementdetailsData.setEngagementname((String) arr[6]);
   				engagementdetailsData.setSubentitles((String) arr[7]);
   				engagementdetailsData.setEngagementtypeid((BigInteger) arr[8]);
   				engagementdetailsData.setEngagementtype((String) arr[9]);
   				engagementdetailsData.setCompilationtype((String) arr[10]);
   				engagementdetailsData.setFinanicalyearenddate((Date) arr[11]);
   				engagementdetailsData.setAdditionalinfo((String) arr[12]);
   				engagementdetailsData.setStatusid((BigInteger) arr[13]);
   				engagementdetailsData.setTbloadstatusid((BigInteger) arr[14]);
   				engagementdetailsData.setStatusname((String) arr[15]);
   				engagementdetailsData.setEngagementcreateddate((Date) arr[16]);
   				engagementdetailsData.setAuditorrole((String) arr[17]);
   				engagementdetailsData.setModifieddate((Date)arr[18]);
   				engagementdetailss.add(engagementdetailsData);
   			}
   			LOGGER.debug("getengagementdetails end");
   			return engagementdetailss;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagementdetails", ex);
   		}
   	}
	
	@SuppressWarnings("unchecked")
   	public List<Allengagement> getallengagement(String loginid) {
   		try {
   			LOGGER.debug("getallengagement begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETALLENGAGEMENT);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("Ploginid", loginid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Allengagement> allengagements = new ArrayList<Allengagement>();
   			Allengagement allengagementData;
   			for (Object[] arr : eventsSearch) {
   				allengagementData = new Allengagement();
   				allengagementData.setEngagementsid((BigInteger) arr[0]);
   				allengagementData.setClientfirmid((BigInteger) arr[1]);
   				allengagementData.setUseraccountid((BigInteger) arr[2]);
   				allengagementData.setBusinessname((String) arr[3]);
   				allengagementData.setContactperson((String) arr[4]);
   				allengagementData.setIncorporationdate((Date) arr[5]);
   				allengagementData.setEngagementname((String) arr[6]);
   				allengagementData.setSubenties((String) arr[7]);
   				allengagementData.setEngagementtypeid((BigInteger) arr[8]);
   				allengagementData.setEngagementtype((String) arr[9]);
   				allengagementData.setCompilationtype((String) arr[10]);
   				allengagementData.setFinanicalyearenddate((Date) arr[11]);
   				allengagementData.setAdditionalinfo((String) arr[12]);
   				allengagementData.setStatusid((BigInteger) arr[13]);
   				allengagementData.setTbloadstatusid((BigInteger) arr[14]);
   				allengagementData.setStatusname((String) arr[15]);
   				allengagementData.setEngcreateddate((Date) arr[16]);
   				allengagementData.setAuditor_list((String) arr[17]);
   				allengagementData.setModifieddate((Date) arr[18]);
   				allengagementData.setIsowned((BigInteger) arr[19]);
   				allengagements.add(allengagementData);
   			}
   			LOGGER.debug("getallengagement end");
   			return allengagements;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getallengagement", ex);
   		}
   	}
	public boolean saveengagement(Engagement engagement) {
		try{
	        LOGGER.debug("In saveengagement dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.SAVEENGAGEMENT)
	        		.setParameter("Pclientfirmid", engagement.getPclientfirmid())
	        		.setParameter("Pengagementname", engagement.getPengagementname())
	                .setParameter("Pengagementtypeid", engagement.getPengagementtypeid())
	                .setParameter("Pcompliation", engagement.getPcompliation())
	                .setParameter("Psubenties", engagement.getPsubenties())
	                .setParameter("Pyearend", engagement.getPyearend())
	                .setParameter("Padditionalinfo", engagement.getPadditionalinfo())
	                .setParameter("Puseraccoutid_roleid_list", engagement.getPuseraccoutidroleidlist())
	                .setParameter("Ploginid", engagement.getPloginid());
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In saveengagement comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @saveengagement", ex);
	    }
	    return false;
}
	@SuppressWarnings("unchecked")
   	public List<Engagementcountdetail> getengagementcountdetail(String loginid) {
   		try {
   			LOGGER.debug("getengagementcountdetail begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETENGAGEMENTCOUNTDETAIL);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("Ploginid", loginid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Engagementcountdetail> engagementcountdetails = new ArrayList<Engagementcountdetail>();
   			Engagementcountdetail engagementcountdetailData;
   			for (Object[] arr : eventsSearch) {
   				engagementcountdetailData = new Engagementcountdetail();
   				engagementcountdetailData.setTotalclient((BigInteger) arr[0]);
   				engagementcountdetailData.setTotalengagement((BigInteger) arr[1]);
   				engagementcountdetailData.setCurrent((BigInteger) arr[2]);
   				engagementcountdetailData.setCompleted((BigInteger) arr[3]);
   				engagementcountdetails.add(engagementcountdetailData);
   			}
   			LOGGER.debug("getengagementcountdetail end");
   			return engagementcountdetails;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagementcountdetail", ex);
   		}
   	}
	@SuppressWarnings("unchecked")
	public List<EngagementYear> getEngagementYearDetails(int inengagementsid) {
		try {
   			LOGGER.debug("get engagement year details begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_ENGAGEMENT_YEAR);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("inengagementsid", inengagementsid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<EngagementYear> engagementyaerdetails = new ArrayList<EngagementYear>();
   			EngagementYear engagementYearDetailsData;
   			for (Object[] arr : eventsSearch) {
   				engagementYearDetailsData = new EngagementYear();
   				engagementYearDetailsData.setEngagementsid((BigInteger) arr[0]);
   				engagementYearDetailsData.setAcctyear((String) arr[1]);
   				engagementyaerdetails.add(engagementYearDetailsData);
   			}
   			LOGGER.debug("get Engagement Year end");
   			return engagementyaerdetails;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagementyear", ex);
   		}
   				
	}
	@SuppressWarnings("unchecked")
	public List<Integer> getEngagementId(int clientfirmid) {
		try {
   			LOGGER.debug("get engagement id details begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_ENGAGEMENT_ID);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("Pclientfirmid", clientfirmid);
   			List<Integer> engagementId = subsQuery.list();
   			LOGGER.debug("get Engagement id end");
   			return engagementId;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getengagementid", ex);
   		}
	}
	
}


