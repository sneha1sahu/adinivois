/**
 * Program Name: CinchDaoImpl
 * 
 * Program Description / functionality: 
 * 
 * Modules Impacted: 
 * 
 * Tables affected: 
 * 
 * Developer Created /Modified Date Purpose
 *******************************************************************************
 * Gouri                                                                                      * 
 * 
 * * Associated Defects Raised :
 *
 */
package com.adinovis.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;

import com.adinovis.common.CinchQueryConstants;
import com.adinovis.common.ExceptionConstants;
import com.adinovis.dao.CinchDao;
import com.adinovis.domain.CuserProfile;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;
import com.adinovis.exception.DataAccessException;
import com.adinovis.exception.DataBaseException;

public class CinchDaoImpl extends HibernateDaoSupport implements CinchDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(CinchDaoImpl.class);
  
    @SuppressWarnings("unchecked")
	public List<CuserProfile> login(String emailid,String encpassword) {
		try {
			LOGGER.debug("getuserprofile begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.USERPROFILE);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());

			subsQuery.setParameter("emailid", emailid);
			subsQuery.setParameter("password", encpassword);
			List<Object[]> eventsSearch = subsQuery.list();
			List<CuserProfile> cuserProfile = new ArrayList<CuserProfile>();
			CuserProfile cuserProfileData;
			for (Object[] arr : eventsSearch) {
				cuserProfileData = new CuserProfile();
                cuserProfileData.setUseracctid((BigInteger) arr[0]);
				
				cuserProfileData.setFullName((String) arr[1]);
				cuserProfileData.setFirstname((String) arr[2]);
				
				cuserProfileData.setLastname((String) arr[3]);
				cuserProfileData.setBusinessName((String) arr[4]);
				cuserProfileData.setEmailAddress((String) arr[5]);
				cuserProfileData.setLoginpassword((String) arr[6]);
				cuserProfileData.setStatus((BigInteger) arr[7]);
				cuserProfileData.setUserDelete((Boolean) arr[8]);
				cuserProfileData.setAddressId((BigInteger) arr[9]);
				cuserProfileData.setAddress((String) arr[10]);
				cuserProfileData.setCity((String) arr[11]);
				cuserProfileData.setState((String) arr[12]);
				cuserProfileData.setCountry((String) arr[13]);
				cuserProfileData.setPostCode((Integer) arr[14]);
				cuserProfileData.setAddressDelete((Boolean) arr[15]);
				cuserProfileData.setBusinessphoneId((BigInteger) arr[16]);
				cuserProfileData.setBusinessphonetype((String) arr[17]);
				cuserProfileData.setBusinessphoneNumber((String) arr[18]);
				cuserProfileData.setBusinessphoneDelete((Integer) arr[19]);
				cuserProfileData.setCellphoneId((BigInteger) arr[20]);
				cuserProfileData.setCellphonetype((String) arr[21]);
				cuserProfileData.setCellphonenumber((String) arr[22]);
				cuserProfileData.setCellphonedelete((Integer) arr[23]);
				cuserProfileData.setUserroleId((BigInteger) arr[24]);
				cuserProfileData.setRoleName((String) arr[25]);
				cuserProfileData.setUserroledelete((Boolean) arr[26]);
				cuserProfileData.setLicenseno((String) arr[27]);
				cuserProfileData.setTokenvalue((String) arr[28]);
				cuserProfileData.setProfilepicture((String) arr[29]);
                cuserProfile.add(cuserProfileData);
			}
			LOGGER.debug("getuserprofile end");
			return cuserProfile;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getuserprofile", ex);
		}
	}
    
    @SuppressWarnings("unchecked")
   	public List<CuserProfile> getuserprofileupdate(BigInteger accountid) {
   		try {
   			LOGGER.debug("getuserprofileupdate begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.USERPROFILEUPDATE);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());

   			subsQuery.setParameter("Puseraccountid", accountid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<CuserProfile> cuserProfile = new ArrayList<CuserProfile>();
   			CuserProfile cuserProfileData;
   			for (Object[] arr : eventsSearch) {
   				cuserProfileData = new CuserProfile();
                   cuserProfileData.setUseracctid((BigInteger) arr[0]);
   				
   				cuserProfileData.setFullName((String) arr[1]);
   				cuserProfileData.setFirstname((String) arr[2]);
   				
   				cuserProfileData.setLastname((String) arr[3]);
   				cuserProfileData.setBusinessName((String) arr[4]);
   				cuserProfileData.setEmailAddress((String) arr[5]);
   				cuserProfileData.setLoginpassword((String) arr[6]);
   				cuserProfileData.setStatus((BigInteger) arr[7]);
   				cuserProfileData.setUserDelete((Boolean) arr[8]);
   				cuserProfileData.setAddressId((BigInteger) arr[9]);
   				cuserProfileData.setAddress((String) arr[10]);
   				cuserProfileData.setCity((String) arr[11]);
   				cuserProfileData.setState((String) arr[12]);
   				cuserProfileData.setCountry((String) arr[13]);
   				cuserProfileData.setPostCode((Integer) arr[14]);
   				cuserProfileData.setAddressDelete((Boolean) arr[15]);
   				cuserProfileData.setBusinessphoneId((BigInteger) arr[16]);
   				cuserProfileData.setBusinessphonetype((String) arr[17]);
   				cuserProfileData.setBusinessphoneNumber((String) arr[18]);
   				cuserProfileData.setBusinessphoneDelete((Integer) arr[19]);
   				cuserProfileData.setCellphoneId((BigInteger) arr[20]);
   				cuserProfileData.setCellphonetype((String) arr[21]);
   				cuserProfileData.setCellphonenumber((String) arr[22]);
   				cuserProfileData.setCellphonedelete((Integer) arr[23]);
   				cuserProfileData.setUserroleId((BigInteger) arr[24]);
   				cuserProfileData.setRoleName((String) arr[25]);
   				cuserProfileData.setUserroledelete((Boolean) arr[26]);
   				cuserProfileData.setLicenseno((String) arr[27]);
   				cuserProfileData.setTokenvalue((String) arr[28]);
   				cuserProfileData.setProfilepicture((String) arr[29]);
                   cuserProfile.add(cuserProfileData);
   			}
   			LOGGER.debug("getuserprofileupdate end");
   			return cuserProfile;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getuserprofileupdate", ex);
   		}
   	}
   
    
    public boolean updateuserprofile(Userprofileupdate userprofileupdate) {
		try{
	        LOGGER.debug("In updateuserprofile dao begin");
	        String user=userprofileupdate.getFirstname();
	        System.out.println("user:"+user);
	        System.out.println();
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.UPDATEUSERPROFILE)
	        		.setParameter("Puseraccountid",userprofileupdate.getUseraccountid())
	        		.setParameter("Pfirstname", userprofileupdate.getFirstname())
	        		.setParameter("Plastname", userprofileupdate.getLastname())
	                .setParameter("Pcphonenumber", userprofileupdate.getPcphonenumber())
	                .setParameter("Pbphonenumber", userprofileupdate.getPbphonenumber())
	                .setParameter("Pmemberlicenseno", userprofileupdate.getMemberlicenseno())
	                .setParameter("Pbusinessname", userprofileupdate.getBusinessname())
	                .setParameter("Pemailaddress", userprofileupdate.getEmailaddress())
                    .setParameter("Paddress",userprofileupdate.getAddress())
                    .setParameter("Pprofilepict", userprofileupdate.getProfilepict());
                  
	        int update = subsQuery.executeUpdate();
	        if(update != 0){
	            LOGGER.debug("In updateuserprofile comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @updateuserprofile", ex);
	    }
	    return false;
}

    @SuppressWarnings("unchecked")
   	public String getemailid(String emailid) {
   		try {
   			LOGGER.debug("getemailid begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETEMAILID);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("emailid", emailid);
   			return (String)subsQuery.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
   	}
    
    @SuppressWarnings("unchecked")
   	public BigInteger getuserid(String emailid) {
   		try {
   			LOGGER.debug("getuserid begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETUSERID);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("emailid", emailid);
   			BigInteger userid = (BigInteger)subsQuery.uniqueResult();
			if (userid != null)
				return userid;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
    
    @SuppressWarnings("unchecked")
   	public BigInteger getclientid(String emailid,String password) {
   		try {
   			LOGGER.debug("getclientid begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTID);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("emailid", emailid);
   			subsQuery.setParameter("Ppwd", password);
   			BigInteger clientid = (BigInteger)subsQuery.uniqueResult();
			if (clientid != null)
				return clientid;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
    
    public boolean tokensave(BigInteger accountid, String token) {
		try{
	        LOGGER.debug("In tokensave dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.token)
	                .setParameter("Puseraccountid", accountid)
                    .setParameter("Ptokenvalue", token);
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In tokensave comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @tokensave", ex);
	    }
	    return false;
}
    
	public boolean signup(SignupDetails signupDetails) {
		try{
	        LOGGER.debug("In signup dao begin");
	       // String s="@dbstaus";
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.SIGNUP)
	                .setParameter("Pfname", signupDetails.getPfname())
	                .setParameter("Plname", signupDetails.getPlname())
	                .setParameter("Pbsname", signupDetails.getPbsname())
	                .setParameter("Pemailadd", signupDetails.getPemailadd())
	                .setParameter("Ppwd", signupDetails.getPpwd())
	                .setParameter("Paddr", signupDetails.getPaddr())
	                .setParameter("Pbphonenumber", signupDetails.getPbphonenumber())
                    .setParameter("Pcphonenumber", signupDetails.getPcphonenumber())
                    .setParameter("plicno", signupDetails.getPlicno());
                    //.setParameter("@dbstaus", s);
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In signup comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @signup", ex);
	    }
	    return false;
}
	
	public String forgotPassword(String emailid) {
		try{
	        LOGGER.debug("In forgotPassword dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.RESETEMAILVALIDATION)
	                .setParameter("emailid", emailid);
	        return (String)subsQuery.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
   	}
	
	public boolean loginvalidation(String tokenkey) {
		try{
	        LOGGER.debug("In loginvalidation dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.LOGINVALIDATION)
	                .setParameter("tokenkey", tokenkey);
	        int update = subsQuery.executeUpdate();
	        if(update != 0){
	            LOGGER.debug("In tokensave comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @tokensave", ex);
	    }
	    return false;
}
	
	
	
	public String signupvalidation(String emailid) {
		try{
	        LOGGER.debug("In signupvalidation dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.SIGNUPVALIDATION)
	                .setParameter("emailid", emailid);
	        return (String)subsQuery.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
   	}
	
	public boolean changepassword(Integer useraccountid,String oldpassword,String newpassword) {
		try{
	        LOGGER.debug("In changepassword dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.CHANGEPASSWORD)
	                .setParameter("Puseraccountid", useraccountid)
	                .setParameter("oldpassword", oldpassword)
	                .setParameter("newpassword", newpassword);
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In changepassword comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @changepassword", ex);
	    }
	    return false;
}
	
	public boolean forgotPasswordstatuschange(String emailid) {
		try{
	        LOGGER.debug("In forgotPasswordstatuschange dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.FORGOTPASSWORD)
	                .setParameter("emailid", emailid);
	        int update = subsQuery.executeUpdate();
	        if(update != 0){
	            LOGGER.debug("In forgotPasswordstatuschange comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @forgotPasswordstatuschange", ex);
	    }
	    return false;
}
	
	public boolean resetpassword(String tokenkey,String newpassword) {
		try{
	        LOGGER.debug("In resetpassword dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.RESETPASSWORD)
	                .setParameter("tokenkey", tokenkey)
	                .setParameter("newpassword", newpassword);
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In resetpassword comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @resetpassword", ex);
	    }
	    return false;
}

	@Override
	public String getUserVerification(String emailid) {
		try {
   			LOGGER.debug("getUserVerification status begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_USER_VERIFICATION);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("emailid", emailid);
   			return (String)subsQuery.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	

}