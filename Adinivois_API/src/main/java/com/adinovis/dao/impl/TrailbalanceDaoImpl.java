package com.adinovis.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;
import com.adinovis.common.CinchQueryConstants;
import com.adinovis.common.ExceptionConstants;
import com.adinovis.dao.TrailbalanceDao;
import com.adinovis.domain.Adjustmenttype;
import com.adinovis.domain.BalanceSheet;
import com.adinovis.domain.ClientQuestionAnswer;
import com.adinovis.domain.FsDetails;
import com.adinovis.domain.TrailBalance;
import com.adinovis.domain.Gettrailbalance;
import com.adinovis.domain.IncomeSheet;
import com.adinovis.domain.MapSheet;
import com.adinovis.domain.MappingStatus;
import com.adinovis.domain.TrialbalanceAdjustmentType;
import com.adinovis.exception.DataAccessException;
import com.adinovis.exception.DataBaseException;

public class TrailbalanceDaoImpl extends HibernateDaoSupport implements TrailbalanceDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrailbalanceDaoImpl.class);

	@SuppressWarnings("unchecked")
	public List<Gettrailbalance> gettrailbalance(int engagementsid, int year) {
		try {
			LOGGER.debug("gettrailbalance begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETTRAILBALANCE);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("Pengagementid", engagementsid).setParameter("inyear", year);
			List<Object[]> eventsSearch = subsQuery.list();
			List<Gettrailbalance> gettrailbalancetypes = new ArrayList<Gettrailbalance>();
			Gettrailbalance gettrailbalanceData;
			for (Object[] arr : eventsSearch) {
				gettrailbalanceData = new Gettrailbalance();
				gettrailbalanceData.setTrailbalanceid((BigInteger) arr[0]);
				gettrailbalanceData.setEngagementsid((BigInteger) arr[1]);
				gettrailbalanceData.setClientdatasourceid((BigInteger) arr[2]);
				gettrailbalanceData.setClientapidataid((BigInteger) arr[3]);
				gettrailbalanceData.setRecieveddate((Date) arr[4]);
				gettrailbalanceData.setAccountcode((String) arr[5]);
				gettrailbalanceData.setAccountname((String) arr[6]);
				gettrailbalanceData.setAccounttype((String) arr[7]);
				gettrailbalanceData.setOriginalbalance((String) arr[8]);
				gettrailbalanceData.setTrailadjustmentid((BigInteger) arr[9]);
				gettrailbalanceData.setAdjustmenttypeid((BigInteger) arr[10]);
				gettrailbalanceData.setAdjustmenttypename((String) arr[11]);
				gettrailbalanceData.setAdjustmentamount((String) arr[12]);
				gettrailbalanceData.setFinalamount((String) arr[13]);
				gettrailbalanceData.setPY1((String) arr[14]);
				gettrailbalanceData.setChanges((String) arr[15]);
				gettrailbalanceData.setPY2((String) arr[16]);
				gettrailbalanceData.setTrailbalancemapingid((BigInteger) arr[17]);
				gettrailbalanceData.setMapsheetid((BigInteger) arr[18]);
				gettrailbalanceData.setMapno((String) arr[19]);
				gettrailbalanceData.setMapsheetname((String) arr[20]);
				gettrailbalanceData.setFingroupid((BigInteger) arr[21]);
				gettrailbalanceData.setFingroupname((String) arr[22]);
				gettrailbalanceData.setFinsubgrpid((BigInteger) arr[23]);
				gettrailbalanceData.setFinsubgroupname((String) arr[24]);
				gettrailbalanceData.setFinsubgroupchild((BigInteger) arr[25]);
				gettrailbalanceData.setFinsubgroupchildname((String) arr[26]);
				gettrailbalanceData.setLeadsheetid((BigInteger) arr[27]);
				gettrailbalanceData.setLeadsheetname((String) arr[28]);
				gettrailbalanceData.setLeadsheetcode((String) arr[29]);
				gettrailbalanceData.setBalancetypeid((BigInteger) arr[30]);
				gettrailbalanceData.setBalancetypename((String) arr[31]);
				gettrailbalanceData.setStatementtypeid((BigInteger) arr[32]);
				gettrailbalanceData.setStatementtypename((String) arr[33]);
				gettrailbalanceData.setNotes((String) arr[34]);
				gettrailbalanceData.setGificode((Integer) arr[35]);
				gettrailbalanceData.setFxtranslation((String) arr[36]);
				gettrailbalanceData.setIsclientdatasource((Boolean) arr[37]);
				gettrailbalanceData.setMappingstatus((String) arr[38]);
				gettrailbalanceData.setMappingstatusid((BigInteger) arr[39]);
				gettrailbalanceData.setOriginalbalancetotal((String) arr[40]);
				gettrailbalanceData.setAdjustmentamounttotal((String) arr[41]);
				gettrailbalanceData.setFinalamounttotal((String) arr[42]);
				gettrailbalanceData.setPY1total((String) arr[43]);
				gettrailbalanceData.setPY2total((String) arr[44]);
				gettrailbalanceData.setOriginalred((String) arr[45]);
				gettrailbalanceData.setFinalred((String) arr[46]);
				gettrailbalancetypes.add(gettrailbalanceData);
			}
			LOGGER.debug("gettrailbalance end");
			return gettrailbalancetypes;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @gettrailbalance", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Adjustmenttype> getadjustmenttype() {
		try {
			LOGGER.debug("getadjustmenttype begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETADJUSTMENTTYPE);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			List<Object[]> eventsSearch = subsQuery.list();
			List<Adjustmenttype> adjustmenttypes = new ArrayList<Adjustmenttype>();
			Adjustmenttype adjustmenttypeData;
			for (Object[] arr : eventsSearch) {
				adjustmenttypeData = new Adjustmenttype();
				adjustmenttypeData.setAdjustmenttypeid((BigInteger) arr[0]);
				adjustmenttypeData.setAdjustmenttypename((String) arr[1]);
				adjustmenttypes.add(adjustmenttypeData);
			}
			LOGGER.debug("getadjustmenttype end");
			return adjustmenttypes;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getadjustmenttype", ex);
		}
	}

	@Override
	public boolean deletetrailbalancerow(String intrailbalid, String loginid) {
		try {
			LOGGER.debug("In deletetrailbalancerow dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.DELETETRAILBALANCEROW)
					.setParameter("intrailbalid", intrailbalid).setParameter("ploginid", loginid);
			int delete = subsQuery.executeUpdate();
			if (delete != 0) {
				LOGGER.debug("In deletetrailbalancerow comment dao end");
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @deletetrailbalancerow", ex);
		}
		return false;
	}

	@Override
	public boolean saveTrialbalanceAdjustmentType(TrialbalanceAdjustmentType trialbalAdjustmentType) {
		try {
			LOGGER.debug("In save trialbalance adjustment dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.SAVE_TRAILBALANCE_ADJUSTMENT)
					.setParameter("intrailbalanceid", trialbalAdjustmentType.getIntrailbalanceid())
					.setParameter("intrailadjustmentid", trialbalAdjustmentType.getIntrailadjustmentid())
					.setParameter("inadjustmenttypeid", trialbalAdjustmentType.getInadjustmenttypeid())
					.setParameter("inaccountcode", trialbalAdjustmentType.getInaccountcode())
					.setParameter("inaccountname", trialbalAdjustmentType.getInaccountname())
					.setParameter("inacctcredit", trialbalAdjustmentType.getInacctcredit())
					.setParameter("inacctdebit", trialbalAdjustmentType.getInacctdebit())
					.setParameter("incomment", trialbalAdjustmentType.getIncomment())
					.setParameter("incomment1", trialbalAdjustmentType.getIncomment1())
					.setParameter("inisdelete", trialbalAdjustmentType.getInisdelete())
					.setParameter("ploginid", trialbalAdjustmentType.getPloginid());

			int insert = subsQuery.executeUpdate();
			if (insert != 0) {
				LOGGER.debug("In save trialbalance adjustment dao end");
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @save trialbalance adjustment", ex);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<TrialbalanceAdjustmentType> getalltrailadjustment(int inengagementsid) {
		try {
			LOGGER.debug("gettrailbalance begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_ALL_TRAIL_ADJUSTMENT);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inengagementsid", inengagementsid);
			List<Object[]> eventsSearch = subsQuery.list();
			List<TrialbalanceAdjustmentType> gettrailbalancetypes = new ArrayList<TrialbalanceAdjustmentType>();
			TrialbalanceAdjustmentType getAllTrailAdjustmentData;
			for (Object[] arr : eventsSearch) {
				getAllTrailAdjustmentData = new TrialbalanceAdjustmentType();
				getAllTrailAdjustmentData.setIntrailadjustmentid((BigInteger) arr[0]);
				getAllTrailAdjustmentData.setIntrailbalanceid((BigInteger) arr[1]);
				getAllTrailAdjustmentData.setInadjustmenttypeid((BigInteger) arr[2]);
				getAllTrailAdjustmentData.setInadjustmenttypename((String) arr[3]);
				getAllTrailAdjustmentData.setInaccountcode((String) arr[4]);
				getAllTrailAdjustmentData.setInaccountname((String) arr[5]);
				getAllTrailAdjustmentData.setInacctcredit((BigInteger) arr[6]);
				getAllTrailAdjustmentData.setInacctdebit((BigInteger) arr[7]);
				getAllTrailAdjustmentData.setIncomment((String) arr[8]);
				getAllTrailAdjustmentData.setIncomment1((String) arr[9]);
				getAllTrailAdjustmentData.setModifiedby((String) arr[10]);
				getAllTrailAdjustmentData.setFullname((String) arr[11]);
				gettrailbalancetypes.add(getAllTrailAdjustmentData);
			}
			LOGGER.debug("gettrailadjustment end");
			return gettrailbalancetypes;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getalltrailadjustment", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<MappingStatus> getMappingStatus() {
		try {
			LOGGER.debug("getmappingstatus dao begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_MAPPING_STATUS);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			List<Object[]> eventsSearch = subsQuery.list();
			List<MappingStatus> getMappingStatus = new ArrayList<MappingStatus>();
			MappingStatus getMappingStatusData;
			for (Object[] arr : eventsSearch) {
				getMappingStatusData = new MappingStatus();
				getMappingStatusData.setMappingStatusId((Integer) arr[0]);
				getMappingStatusData.setMappingStatusName((String) arr[1]);
				getMappingStatusData.setMappingStatusDescription((String) arr[2]);
				getMappingStatus.add(getMappingStatusData);
			}
			LOGGER.debug("getmappinsstatus dao end");
			return getMappingStatus;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getmappingstatus", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<BalanceSheet> getBalanceStatement(int engagementsid, int year) {
		try {
			LOGGER.debug("get balance statement begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_BALANCE_STATEMENT);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inengagementid", engagementsid).setParameter("inyear", year);
			List<BalanceSheet> getBalanceSheet = new ArrayList<BalanceSheet>();
			String result=(String)subsQuery.uniqueResult();
			//for (String arr[] : result) {
				BalanceSheet balanceSheetData = new BalanceSheet();
				
				balanceSheetData.setResult(result);
				getBalanceSheet.add(balanceSheetData);
			//}
			 return getBalanceSheet;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
   	}

	@SuppressWarnings("unchecked")
	public List<IncomeSheet> getIncomeStatement(int engagementsid, int year) {
		try {
			LOGGER.debug("get income statement begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_INCOME_STATEMENT);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inengagementid", engagementsid).setParameter("inyear", year);
			List<String> eventsSearch = subsQuery.list();
			List<IncomeSheet> getIncomeSheet = new ArrayList<IncomeSheet>();
			IncomeSheet incomeSheetData;
			for (String arr : eventsSearch) {
				incomeSheetData = new IncomeSheet();
				incomeSheetData.setResult(arr);
				getIncomeSheet.add(incomeSheetData);
			}
			LOGGER.debug("get incomestatement end");
			return getIncomeSheet;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @get incomestatement", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ClientQuestionAnswer> getQuestionAnswer(int inquestiontypeid, int inengagementsid) {
		try {
			LOGGER.debug("get question answer begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_QUESTION_ANSWER);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inquestiontypeid", inquestiontypeid).setParameter("inengagementsid", inengagementsid);
			List<Object[]> eventsSearch = subsQuery.list();
			List<ClientQuestionAnswer> getQuestionAnswerType = new ArrayList<ClientQuestionAnswer>();
			ClientQuestionAnswer getQuestionAnswer;
			for (Object[] arr : eventsSearch) {
				getQuestionAnswer = new ClientQuestionAnswer();
				getQuestionAnswer.setClientquestionid((BigInteger) arr[0]);
				getQuestionAnswer.setClientquestiontypeid((BigInteger) arr[1]);
				getQuestionAnswer.setQuestiondescription((String) arr[2]);
				getQuestionAnswer.setAnswertype((String) arr[3]);
				getQuestionAnswer.setQuesactive((Boolean) arr[4]);
				getQuestionAnswer.setQuesdelete((Boolean) arr[5]);
				getQuestionAnswer.setClientquestionanswerid((BigInteger) arr[6]);
				getQuestionAnswer.setEngagementsid((BigInteger) arr[7]);
				getQuestionAnswer.setClientanswer((String) arr[8]);
				getQuestionAnswer.setClientnote((String) arr[9]);
				getQuestionAnswer.setAnsactive((Boolean) arr[10]);
				getQuestionAnswer.setAnsdelete((Boolean) arr[11]);
				getQuestionAnswerType.add(getQuestionAnswer);
			}
			LOGGER.debug("get question answer end");
			return getQuestionAnswerType;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @get question answer", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<BalanceSheet> getBalanceSheet(int engagementsid, int year) {
		try {
			LOGGER.debug("get balance statement begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_BALANCE_SHEET);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inengagementid", engagementsid).setParameter("inyear", year);
			List<BalanceSheet> getBalanceSheet = new ArrayList<BalanceSheet>();
			String result = (String) subsQuery.uniqueResult();
			BalanceSheet balanceSheetData = new BalanceSheet();
			balanceSheetData.setResult(result);
			getBalanceSheet.add(balanceSheetData);
			return getBalanceSheet;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<IncomeSheet> getIncomeStatementTest(int engagementsid, int year) {
		try {
			LOGGER.debug("get income statement test dao begin");
			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GET_INCOME_STATEMENT_TEST);
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(superiorSb.toString());
			subsQuery.setParameter("inengagementid", engagementsid).setParameter("inyear", year);
			List<String> eventsSearch = subsQuery.list();
			List<IncomeSheet> getIncomeSheet = new ArrayList<IncomeSheet>();
			IncomeSheet incomeSheetData;
			for (String arr : eventsSearch) {
				incomeSheetData = new IncomeSheet();
				incomeSheetData.setResult(arr);
				getIncomeSheet.add(incomeSheetData);
			}
			LOGGER.debug("get incomestatement end");
			return getIncomeSheet;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @get incomestatement test", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Integer> trailbalanceWithDuplicates(TrailBalance duplicateTrailBalance) {
		try {
			LOGGER.debug("In save trialbalance with duplicates dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.SAVE_DUPLICATE_TRAILBALANCE)
					.setParameter("intrailbalanceid", duplicateTrailBalance.getIntrailbalanceid())
					.setParameter("inengagementsid", duplicateTrailBalance.getEngagementsid())
					.setParameter("inacctyear", duplicateTrailBalance.getInacctyear())
					.setParameter("inaccountcode", duplicateTrailBalance.getInaccountcode())
					.setParameter("inaccountname", duplicateTrailBalance.getInaccountname())
					.setParameter("inoriginalbalance", duplicateTrailBalance.getInoriginalbalance())
					.setParameter("inPY1", duplicateTrailBalance.getInPY1())
					.setParameter("inPY2", duplicateTrailBalance.getInPY2())
					.setParameter("inmapsheetid", duplicateTrailBalance.getInmapsheetid())
					.setParameter("ploginid", duplicateTrailBalance.getPloginid());
					

			List<Integer> eventsSearch = subsQuery.list();
			if (eventsSearch != null) {
				LOGGER.debug("In save duplicate trialbalance adjustment dao end");
				return eventsSearch;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @save duplicate trialbalance adjustment", ex);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<MapSheet> getMapSheetDetails() {
		try {
			LOGGER.debug("get map sheet details begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.GET_MAP_DETAILS);
			List<Object[]> eventsSearch = subsQuery.list();
			List<MapSheet> getMapSheetDetails = new ArrayList<MapSheet>();
			MapSheet getMapSheet;
			for (Object[] arr : eventsSearch) {
				getMapSheet = new MapSheet();
				getMapSheet.setMapsheetid((BigInteger) arr[0]);
				getMapSheet.setMapno((String) arr[1]);
				getMapSheet.setMapsheetname((String) arr[2]);
				getMapSheet.setFingroupid((BigInteger) arr[3]);
				getMapSheet.setFingroupname((String) arr[4]);
				getMapSheet.setFinsubgrpid((BigInteger) arr[5]);
				getMapSheet.setFinsubgroupname((String) arr[6]);
				getMapSheet.setFinsubgroupchildId((BigInteger) arr[7]);
				getMapSheet.setFinsubgroupchildname((String) arr[8]);
				getMapSheet.setLeadsheetid((BigInteger) arr[9]);
				getMapSheet.setLeadsheetname((String) arr[10]);
				getMapSheet.setLeadsheetcode((String) arr[11]);
				getMapSheet.setBalancetypeid((BigInteger) arr[12]);
				getMapSheet.setBalancetypename((String) arr[13]);
				getMapSheet.setStatementtypeid((BigInteger) arr[14]);
				getMapSheet.setStatementtypename((String) arr[15]);
				getMapSheet.setGificode((Integer) arr[16]);
				getMapSheet.setIsdelete((Boolean) arr[17]);
				getMapSheet.setNotes((String) arr[18]);
				getMapSheetDetails.add(getMapSheet);
			}
			LOGGER.debug("get map sheet details end");
			return getMapSheetDetails;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @get map sheet details", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Integer> saveTrailbalanceMapping(TrailBalance trailbalance) {
		try {
			LOGGER.debug("In save trialbalance mapping dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.SAVE_TRAILBALANCE_MAPPING)
					.setParameter("ptrailbalanceid", trailbalance.getIntrailbalanceid())
					.setParameter("pmapsheetid", trailbalance.getInmapsheetid())
					.setParameter("ploginid", trailbalance.getPloginid());
			List<Integer> eventsSearch = subsQuery.list();
			if (eventsSearch != null) {
				LOGGER.debug("In save trialbalance mapping dao end");
				return eventsSearch;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @save trialbalance mapping", ex);
		}
		return null;
	}

	@Override
	public boolean saveFsDetails(FsDetails fsDetails) {
		try {
			LOGGER.debug("In save fs details dao begin");
			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.SAVE_FS_DETAILS)
					.setParameter("inengagementsid", fsDetails.getEngagementsid())
					.setParameter("inacctyear", fsDetails.getAcctyear())
					.setParameter("infstype", fsDetails.getFstype())
					.setParameter("infsid", fsDetails.getFsid())
					.setParameter("incurrentname", fsDetails.getCurrentname())
					.setParameter("innewname", fsDetails.getNewname())
					.setParameter("inploginid", fsDetails.getPloginid());

			int insert = subsQuery.executeUpdate();
			if (insert != 0) {
				LOGGER.debug("In save fs details dao end");
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @save trialbalance adjustment", ex);
		}
		return false;
	}
}
