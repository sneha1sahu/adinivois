/**
 * Program Name: ClientDaoImpl
 * 
 * Program Description / functionality: 
 * 
 * Modules Impacted: 
 * 
 * Tables affected: 
 * 
 * Developer Created /Modified Date Purpose
 *******************************************************************************
 * Srivalli                                                                                      * 
 * 
 * * Associated Defects Raised :
 *
 */
package com.adinovis.dao.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.CannotCreateTransactionException;

import com.adinovis.common.CinchQueryConstants;
import com.adinovis.common.DateUtils;
import com.adinovis.common.ExceptionConstants;
import com.adinovis.dao.ClientDao;
import com.adinovis.domain.AllAuditor;
import com.adinovis.domain.Auditorrole;
import com.adinovis.domain.Businessdetails;
import com.adinovis.domain.ClientData;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.Clientcountdetails;
import com.adinovis.domain.Clientdetails;
import com.adinovis.domain.Clientprofiles;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.GetUserClientId;
import com.adinovis.domain.Jurisdiction;
import com.adinovis.domain.Typeofentity;
import com.adinovis.exception.DataAccessException;
import com.adinovis.exception.DataBaseException;

public class ClientDaoImpl extends HibernateDaoSupport implements ClientDao{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDaoImpl.class);
	 @SuppressWarnings("unchecked")
	   	public List<Clientprofiles> getclientprofile(String loginid) {
	   		try {
	   			LOGGER.debug("getclientprofile begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTPROFILE);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			        subsQuery.setParameter("Ploginid", loginid);
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Clientprofiles> Clientprofiles = new ArrayList<Clientprofiles>();
	   			Clientprofiles clientprofilesData;
	   			for (Object[] arr : eventsSearch) {
	   				clientprofilesData = new Clientprofiles();
	   				clientprofilesData.setUseraccountid((BigInteger) arr[0]);
	   				clientprofilesData.setClientfirmid((BigInteger) arr[1]);
	   				clientprofilesData.setBusinessname((String) arr[2]);
	   				clientprofilesData.setEngagement((BigInteger) arr[3]);
	   				clientprofilesData.setStatusid((BigInteger) arr[4]);
	   				clientprofilesData.setStatusname((String) arr[5]);
	   				clientprofilesData.setSourcelink((String) arr[6]);
	   				clientprofilesData.setContactperson((String) arr[7]);
	   				clientprofilesData.setClientonboarding((Date) arr[8]);
	   				clientprofilesData.setTokenvalue((String) arr[9]);
	   				clientprofilesData.setCellphonenumber((String) arr[10]);
	   				clientprofilesData.setBusinessphonenumber((String) arr[11]);
	   				clientprofilesData.setEmailaddress((String) arr[12]);
	   				clientprofilesData.setJurisdictionid((BigInteger) arr[13]);
	   				clientprofilesData.setProvincename((String) arr[14]);
	   				clientprofilesData.setProvincescode((String) arr[15]);
	   				clientprofilesData.setStatename((String) arr[16]);
	   				clientprofilesData.setTypeofentotyid((BigInteger) arr[17]);
	   				clientprofilesData.setTypeofentityname((String) arr[18]);
	   				clientprofilesData.setTypeofentitycode((String) arr[19]);
	   				clientprofilesData.setIncorportiondate((Date) arr[20]);
	   				clientprofilesData.setClientid((String) arr[21]);
	   				clientprofilesData.setSecretkey((String) arr[22]);
	   				clientprofilesData.setModifieddate((Date) arr[23]);
	   				clientprofilesData.setIsowned((BigInteger) arr[24]);
	   				Clientprofiles.add(clientprofilesData);
	   			}
	   			LOGGER.debug("getclientprofile end");
	   			return Clientprofiles;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getclientprofile", ex);
	   		}
	   	}
	 
	 public String clientvalidation(String emailid) {
			try{
		        LOGGER.debug("In clientvalidation dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.CLIENTVALIDATION)
		                .setParameter("emailid", emailid);
		        return (String)subsQuery.uniqueResult();

			} catch (Exception e) {
				e.printStackTrace();
			}

			return "";
	   	}
		
	 @SuppressWarnings("unchecked")
	   	public List<Clientstatus> getclientstatus() {
	   		try {
	   			LOGGER.debug("getclientstatus begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTSTATUS);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Clientstatus> clientstatuslist = new ArrayList<Clientstatus>();
	   			Clientstatus clientstatusData;
	   			for (Object[] arr : eventsSearch) {
	   				clientstatusData = new Clientstatus();
	   				clientstatusData.setStatusid((BigInteger) arr[0]);
	   				clientstatusData.setStatusname((String) arr[1]);
	   				clientstatuslist.add(clientstatusData);
	   			}
	   			LOGGER.debug("getclientstatus end");
	   			return clientstatuslist;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getclientstatus", ex);
	   		}
	   	}
		
	 public String clientvalidationemail(String emailid) {
			try{
		        LOGGER.debug("In clientvalidationemail dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.CLIENTVALIDATIONEMAIL)
		                .setParameter("emailid", emailid);
		        return (String)subsQuery.uniqueResult();

			} catch (Exception e) {
				e.printStackTrace();
			}

			return "";
	   	}
	   
	 public boolean saveclientprofile(ClientProfile clientProfile) {
			try{
		        LOGGER.debug("In saveclientprofile dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.SAVECLIENT)
		        		.setParameter("Pbsname", clientProfile.getPbsname())
		        		.setParameter("Pcontactperson", clientProfile.getPcontactperson())
		                .setParameter("Pbphonenumber", clientProfile.getPbphonenumber())
		                .setParameter("Pcphonenumber", clientProfile.getPcphonenumber())
		                .setParameter("Pemailadd", clientProfile.getPemailadd())
		                .setParameter("Ppwd", clientProfile.getPpwd())
		                .setParameter("Pjurisdictionid", clientProfile.getPjurisdictionid())
		                .setParameter("Ptypeofentityid", clientProfile.getPtypeofentityid())
	                    .setParameter("Pincorporationdate",clientProfile.getPincorporationdate())
	                    .setParameter("Paccountbook", clientProfile.getPaccountbook())
	                    .setParameter("Paccesskey", clientProfile.getPaccesskey())
	                    .setParameter("Psecretkey", clientProfile.getPsecretkey())
		                .setParameter("Ploginid", clientProfile.getPloginid());
		                
		        int insert = subsQuery.executeUpdate();
		        if(insert != 0){
		            LOGGER.debug("In saveclientprofile comment dao end");
		            return true;
		        }
		    }catch(CannotCreateTransactionException ce){
		        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		    }catch(Exception ex){
		        throw new DataAccessException("Error @saveclientprofile", ex);
		    }
		    return false;
	}
	 public boolean updateclientprofile(ClientProfile clientProfile) {
			try{
		        LOGGER.debug("In updateclientprofile dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.UPDATECLIENT)
		        		.setParameter("Pclientfirmid", clientProfile.getPclientfirmid())
		        		.setParameter("Pbsname", clientProfile.getPbsname())
		        		.setParameter("Pcontactperson", clientProfile.getPcontactperson())
		                .setParameter("Pbphonenumber", clientProfile.getPbphonenumber())
		                .setParameter("Pcphonenumber", clientProfile.getPcphonenumber())
		                .setParameter("Pemailadd", clientProfile.getPemailadd())
		                .setParameter("Pjurisdictionid", clientProfile.getPjurisdictionid())
		                .setParameter("Ptypeofentityid", clientProfile.getPtypeofentityid())
	                    .setParameter("Pincorporationdate",clientProfile.getPincorporationdate())
	                    .setParameter("Paccountbook", clientProfile.getPaccountbook())
	                    .setParameter("Paccesskey", clientProfile.getPaccesskey())
	                    .setParameter("Psecretkey", clientProfile.getPsecretkey())
		                .setParameter("Ploginid", clientProfile.getPloginid());
		        int update = subsQuery.executeUpdate();
		        if(update != 0){
		            LOGGER.debug("In updateclientprofile comment dao end");
		            return true;
		        }
		    }catch(CannotCreateTransactionException ce){
		        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		    }catch(Exception ex){
		        throw new DataAccessException("Error @updateclientprofile", ex);
		    }
		    return false;
	}
	 
	@SuppressWarnings("unchecked")
	public List<GetUserClientId> getclientid(String emailid,String password) {
	   		try {
	   			LOGGER.debug("getclientid begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTID);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			subsQuery.setParameter("emailid", emailid);
	   			subsQuery.setParameter("Ppwd", password);
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<GetUserClientId> clientUserIddetails = new ArrayList<GetUserClientId>();
	   			GetUserClientId clientUserId;
	   			for (Object[] arr : eventsSearch) {
	   				clientUserId = new GetUserClientId();
	   				clientUserId.setUserAcctId((BigInteger) arr[0]);
	   				clientUserId.setClientConfirmId((BigInteger) arr[1]);
	   				clientUserIddetails.add(clientUserId);
	   			}
	   			LOGGER.debug("getclientstatus end");
	   			return clientUserIddetails;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getclientstatus", ex);
	   		}

		
		}
	@SuppressWarnings("unchecked")
   	public List<Clientcountdetails> getclientcountdetails(String loginid) {
   		try {
   			LOGGER.debug("getclientcountdetails begin");
   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTCOUNTDETAILS);
   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
   					.createSQLQuery(superiorSb.toString());
   			subsQuery.setParameter("Ploginid", loginid);
   			List<Object[]> eventsSearch = subsQuery.list();
   			List<Clientcountdetails> clientcountdetailss = new ArrayList<Clientcountdetails>();
   			Clientcountdetails clientcountdetailsData;
   			for (Object[] arr : eventsSearch) {
   				clientcountdetailsData = new Clientcountdetails();
   				clientcountdetailsData.setTotalclient((BigInteger) arr[0]);
   				clientcountdetailsData.setActiveclient((BigInteger) arr[1]);
   				clientcountdetailsData.setInvitependingwithclient((BigInteger) arr[2]);
   				clientcountdetailsData.setTotalengagement((BigInteger) arr[3]);
   				clientcountdetailss.add(clientcountdetailsData);
   			}
   			LOGGER.debug("getclientcountdetails end");
   			return clientcountdetailss;
   		} catch (CannotCreateTransactionException ce) {
   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
   		} catch (Exception ex) {
   			throw new DataAccessException("Error @getclientcountdetails", ex);
   		}
   	}
	
	public boolean tokensave(BigInteger accountid, String token) {
		try{
	        LOGGER.debug("In tokensave dao begin");
	        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.CLIENTTOKEN)
	                .setParameter("Puseraccountid", accountid)
                    .setParameter("Ptokenvalue", token);
	        int insert = subsQuery.executeUpdate();
	        if(insert != 0){
	            LOGGER.debug("In tokensave comment dao end");
	            return true;
	        }
	    }catch(CannotCreateTransactionException ce){
	        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	    }catch(Exception ex){
	        throw new DataAccessException("Error @tokensave", ex);
	    }
	    return false;
}
	@SuppressWarnings("unchecked")
	public List<Jurisdiction> getjurisdiction() {
	   		try {
	   			LOGGER.debug("getjurisdiction begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETJURISDICTION);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Jurisdiction> Jurisdictions = new ArrayList<Jurisdiction>();
	   			Jurisdiction jurisdictionData;
	   			for (Object[] arr : eventsSearch) {
	   				jurisdictionData = new Jurisdiction();
	   				jurisdictionData.setJurisdictionid((BigInteger) arr[0]);
	   				jurisdictionData.setProvincesname((String) arr[1]);
	   				jurisdictionData.setProvincescode((String) arr[2]);
	   				jurisdictionData.setStatename((String) arr[3]);
	   				Jurisdictions.add(jurisdictionData);
	   			}
	   			LOGGER.debug("getjurisdiction end");
	   			return Jurisdictions;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getjurisdiction", ex);
	   		}
	   	}
		
		@SuppressWarnings("unchecked")
	   	public List<Typeofentity> gettypeofentity() {
	   		try {
	   			LOGGER.debug("gettypeofentity begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETTYPEOFENTITY);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Typeofentity> typeofentitys = new ArrayList<Typeofentity>();
	   			Typeofentity typeofentityData;
	   			for (Object[] arr : eventsSearch) {
	   				typeofentityData = new Typeofentity();
	   				typeofentityData.setTypeofentityid((BigInteger) arr[0]);
	   				typeofentityData.setTypeofentityname((String) arr[1]);
	   				typeofentityData.setTypeofentitycode((String) arr[2]);
	   				typeofentitys.add(typeofentityData);
	   			}
	   			LOGGER.debug("gettypeofentity end");
	   			return typeofentitys;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @gettypeofentity", ex);
	   		}
	   	}
		
		public boolean clientinvitevalidation(String tokenkey,int statusid) {
			try{
		        LOGGER.debug("In clientinvitevalidation dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.CLIENTVALIDATIONSTATUS)
		        		.setParameter("tokenkey", tokenkey)
		        		.setParameter("Pstatusid", statusid);
		        int update = subsQuery.executeUpdate();
		        if(update != 0){
		            LOGGER.debug("In clientinvitevalidation comment dao end");
		            return true;
		        }
		    }catch(CannotCreateTransactionException ce){
		        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		    }catch(Exception ex){
		        throw new DataAccessException("Error @clientinvitevalidation", ex);
		    }
		    return false;
	}
		
		@SuppressWarnings("unchecked")
	   	public List<AllAuditor> getallauditor() {
	   		try {
	   			LOGGER.debug("getallauditor begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETALLAUDITOR);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<AllAuditor> allAuditors = new ArrayList<AllAuditor>();
	   			AllAuditor allAuditorData;
	   			for (Object[] arr : eventsSearch) {
	   				allAuditorData = new AllAuditor();
	   				allAuditorData.setUseraccountid((BigInteger) arr[0]);
	   				allAuditorData.setFullname((String) arr[1]);
	   				allAuditorData.setEmailaddress((String) arr[2]);
	   				allAuditorData.setUseraccountroleid((BigInteger) arr[3]);
	   				allAuditorData.setUserroleid((BigInteger) arr[4]);
	   				allAuditorData.setRolename((String) arr[5]);
	   				allAuditors.add(allAuditorData);
	   			}
	   			LOGGER.debug("getallauditor end");
	   			return allAuditors;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getallauditor", ex);
	   		}
	   	}
		
		@SuppressWarnings("unchecked")
	   	public  List<Businessdetails> getbusinessdetails(String loginid) {
	   		try {
	   			LOGGER.debug("getbusinessdetails begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETBUSINESSDETAILS);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			subsQuery.setParameter("Ploginid", loginid);
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Businessdetails> businessdetailss = new ArrayList<Businessdetails>();
	   			Businessdetails businessdetailsData;
	   			for (Object[] arr : eventsSearch) {
	   				businessdetailsData = new Businessdetails();
	   				businessdetailsData.setBusinessname((String) arr[0]);
	   				businessdetailsData.setClientfirmid((BigInteger) arr[1]);
	   				businessdetailsData.setIncorporationdate((Date) arr[2]);
	   				businessdetailsData.setTypeofentityid((BigInteger) arr[3]);
	   				businessdetailss.add(businessdetailsData);
	   			}
	   			LOGGER.debug("getbusinessdetails end");
	   			return businessdetailss;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getbusinessdetails", ex);
	   		}
	   	}
		
		@SuppressWarnings("unchecked")
	   	public  List<Clientdetails> getclientdetail(String token) {
	   		try {
	   			LOGGER.debug("getclientdetail begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETCLIENTDETAILS);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			subsQuery.setParameter("Ptokenvalue", token);
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Clientdetails> clientdetailss = new ArrayList<Clientdetails>();
	   			Clientdetails clientdetailsData;
	   			for (Object[] arr : eventsSearch) {
	   				clientdetailsData = new Clientdetails();
	   				clientdetailsData.setClientfirmid((BigInteger) arr[0]);
	   				clientdetailsData.setClientname((String) arr[1]);
	   				clientdetailsData.setBusinessname((String) arr[2]);
	   				clientdetailsData.setTokenvalue((String) arr[3]);
	   				clientdetailss.add(clientdetailsData);
	   			}
	   			LOGGER.debug("getclientdetail end");
	   			return clientdetailss;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getclientdetail", ex);
	   		}
	   	}
		
		@SuppressWarnings("unchecked")
	   	public  List<Auditorrole> getauditorrole() {
	   		try {
	   			LOGGER.debug("getauditorrole begin");
	   			StringBuffer superiorSb = new StringBuffer(CinchQueryConstants.GETAUDITORROLE);
	   			Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession()
	   					.createSQLQuery(superiorSb.toString());
	   			List<Object[]> eventsSearch = subsQuery.list();
	   			List<Auditorrole> auditorroles = new ArrayList<Auditorrole>();
	   			Auditorrole auditorroleData;
	   			for (Object[] arr : eventsSearch) {
	   				auditorroleData = new Auditorrole();
	   				auditorroleData.setAuditroleid((BigInteger) arr[0]);
	   				auditorroleData.setRowindex((Integer) arr[1]);
	   				auditorroleData.setRolename((String) arr[2]);
	   				auditorroleData.setUserroleid((BigInteger) arr[3]);
	                auditorroles.add(auditorroleData);
	   			}
	   			LOGGER.debug("getauditorrole end");
	   			return auditorroles;
	   		} catch (CannotCreateTransactionException ce) {
	   			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
	   		} catch (Exception ex) {
	   			throw new DataAccessException("Error @getauditorrole", ex);
	   		}
	   	}
		
		@Override
		public boolean deleteclientprofile(String clientfirmid,String loginid) {
			try{
		        LOGGER.debug("In deleteclientprofile dao begin");
		        Query subsQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.DELETECLIENTPROFILE)
		                .setParameter("clientfirmid", clientfirmid)
		                .setParameter("Ploginid", loginid);
		        int delete = subsQuery.executeUpdate();
		        if(delete != 0){
		            LOGGER.debug("In deleteclientprofile comment dao end");
		            return true;
		        }
		    }catch(CannotCreateTransactionException ce){
		        throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		    }catch(Exception ex){
		        throw new DataAccessException("Error @deleteclientprofile", ex);
		    }
		    return false;
	}
		@SuppressWarnings("unchecked")
	public List<ClientData> getClientData() {

		try {
			LOGGER.debug("In getClientData dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.GET_CLIENT_DATA);
			List<Object[]> list = query.list();
			List<ClientData> clientDataList = new ArrayList<ClientData>(list.size());
			for (Object[] objArr : list) {
				clientDataList.add(getClientData(objArr));
			}
			return clientDataList;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getClientData", ex);
		}

	}

	public void updateRefreshToken(ClientData clientData) {
		try {
			LOGGER.debug("In updateRefreshToken dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.UPDATE_CLIENT_TOKENS)
					.setParameter("clientFirmId", clientData.getClientFirmId())
					.setParameter("accessToken", clientData.getAccessToken())
					.setParameter("refreshToken", clientData.getRefreshToken());
			int update = query.executeUpdate();
			if (update != 0) {
				 LOGGER.debug("In updateRefreshToken dao end");
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @updateRefreshToken", ex);
		}
	}

		public void saveClientData(BigInteger engagementId, String data, int status, String accountListJson) {
			try {
				LOGGER.debug("In saveClientData dao begin");
				LOGGER.debug("datasourceid: " + engagementId);
				LOGGER.debug("data: " + data);
				LOGGER.debug("status: " + status);
				Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createSQLQuery(CinchQueryConstants.SAVE_CLIENT_DATA)
						.setParameter("engagementId", engagementId).setParameter("receivedData", data)
						.setParameter("responseStatus", status)
						.setParameter("accountList", accountListJson);
				int update = query.executeUpdate();
				if (update != 0) {
					LOGGER.debug("In saveClientData dao end");
				}
			} catch (CannotCreateTransactionException ce) {
				throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
			} catch (Exception ex) {
				throw new DataAccessException("Error @saveClientData", ex);
			}
		}

		@SuppressWarnings("unchecked")
		public ClientData getClientDataSourceByTokenValue(String clientId) {
			try {
				LOGGER.debug("In getClientDataSourceByClientId dao begin");
				Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createSQLQuery(CinchQueryConstants.GET_CLIENT_DATASOURCE).setParameter("clientId", clientId);
				List<Object[]> objArrList = query.list();
				if (!objArrList.isEmpty()) {
					Object[] objArr = objArrList.get(0);
					ClientData clientData = new ClientData();
					clientData.setClientDataSourceId((BigInteger) objArr[0]);
					clientData.setClientFirmId((BigInteger) objArr[1]);
					clientData.setUserAccountId((BigInteger) objArr[2]);
					clientData.setClientId((String) objArr[3]);
					clientData.setClientSecret((String) objArr[4]);
					clientData.setStartDate(DateUtils.convertDateToString((Date) objArr[5], DateUtils.CLOUDUNION_DB_DATE));
					clientData.setEndDate(DateUtils.convertDateToString((Date) objArr[6], DateUtils.CLOUDUNION_DB_DATE));
					clientData.setAccountYear((BigInteger) objArr[7]);
					clientData.setSourceLink((String) objArr[8]);
					clientData.setRealmId((String) objArr[9]);
					clientData.setRefreshToken((String) objArr[10]);
					return clientData;

				}
			} catch (CannotCreateTransactionException ce) {
				throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
			} catch (Exception ex) {
				throw new DataAccessException("Error @getClientDataSourceByClientId", ex);
			}
			return null;
		}

		@Override
	public boolean saveClientDataSource(String tokenValue, ClientData clientData) {
		try {
			LOGGER.debug("In saveClientDataSource dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.SAVE_CLIENT_DATASOURCE).setParameter("tokenvalue", tokenValue)
					.setParameter("sourcelink", clientData.getSourceLink())
					.setParameter("realmid", clientData.getRealmId())
					.setParameter("refreshtoken", clientData.getRefreshToken())
					.setParameter("accesstoken", clientData.getAccessToken());
			int saveCount = query.executeUpdate();
			if (saveCount != 0) {
				LOGGER.debug("In saveClientDataSource dao end");
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @saveClientDataSource", ex);
		}
		return false;
	}

	@Override
	public void loadJsonIntoTrailBalance() {
		try {
			LOGGER.debug("In loadJsonIntoTrailBalance dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.LOAD_JSON_INTO_TRAILBALANCE);
			int insertCount = query.executeUpdate();
			if (insertCount != 0) {
				LOGGER.debug("In loadJsonIntoTrailBalance dao end");
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @loadJsonIntoTrailBalance", ex);
		}
	}

	@Override
	public List<ClientData> getSourcelink(int engagementId) {
		try {
			LOGGER.debug("In SourceLink dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.GET_SOURCELINK).setParameter("engagementId", engagementId);
			List<Object[]> list = query.list();
			List<ClientData> clientDataList = new ArrayList<>(list.size());
			for (Object[] objects : list) {
				clientDataList.add(getClientData(objects));
			}
			return clientDataList;
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @getSourcelink", ex);
		}
	}

	private ClientData getClientData(Object[] objArr) {
		ClientData clientData = new ClientData();
		clientData.setClientDataSourceId((BigInteger) objArr[0]);
		clientData.setClientFirmId((BigInteger) objArr[1]);
		clientData.setAccessToken((String) objArr[2]);
		clientData.setStartDate(DateUtils.convertDateToString((Date) objArr[3], DateUtils.CLOUDUNION_DB_DATE));
		clientData.setEndDate(DateUtils.convertDateToString((Date) objArr[4], DateUtils.CLOUDUNION_DB_DATE));
		clientData.setAccountYear((BigInteger) objArr[5]);
		clientData.setSourceLink((String) objArr[6]);
		clientData.setRealmId((String) objArr[7]);
		clientData.setRefreshToken((String) objArr[8]);
		clientData.setEngagementId((BigInteger) objArr[9]);
		return clientData;
	}

	@Override
	public boolean extractEngTBjsontotable(int engagementId) {
		try {
			LOGGER.debug("In extractEngTBjsontotable dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.EXTRACT_ENG_TB_JSON_TABLE).setParameter("engagementId", engagementId);
			int update = query.executeUpdate();
			if (update != 0) {
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @extractEngTBjsontotable", ex);
		}
		return false;
	}

	@Override
	public boolean extractEngATjsontotable(int engagementId) {
		try {
			LOGGER.debug("In extractEngATjsontotable dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.EXTRACT_ENG_AT_JSON_TABLE).setParameter("engagementId", engagementId);
			int update = query.executeUpdate();
			if (update != 0) {
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @extractEngATjsontotable", ex);
		}
		return false;
	}

	@Override
	public boolean updateAccountList(int engagementId) {
		try {
			LOGGER.debug("In updateAccountList dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.UPDATE_ACCOUNT_LIST).setParameter("engagementId", engagementId);
			int update = query.executeUpdate();
			if (update != 0) {
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @updateAccountList", ex);
		}
		return false;
	}

	@Override
	public boolean loadtrailbalancemappingupdate(int engagementId) {
		try {
			LOGGER.debug("In loadtrailbalancemappingupdate dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.createSQLQuery(CinchQueryConstants.LOAD_TRAIL_BALANCE_MAPPING).setParameter("engagementId", engagementId);
			int update = query.executeUpdate();
			if (update != 0) {
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @loadtrailbalancemappingupdate", ex);
		}
		return false;
	}

	public boolean loadQuickbookDataProcess(int engagementId) {
		try {
			LOGGER.debug("In loadQuickbookDataProcess dao begin");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(CinchQueryConstants.LOAD_QUICKBOOK_DATA_PROCESS)
					.setParameter("engagementId", engagementId);
			int update = query.executeUpdate();
			if (update != 0) {
				return true;
			}
		} catch (CannotCreateTransactionException ce) {
			throw new DataBaseException(ExceptionConstants.ERR_DATABASE_DOWN.getValue());
		} catch (Exception ex) {
			throw new DataAccessException("Error @loadQuickbookDataProcess", ex);
		}
		return false;
	}
}
