package com.adinovis.dao;

import java.util.List;

import com.adinovis.domain.FundingRequest;
import com.adinovis.domain.Login;
import com.adinovis.domain.LoginwithSocial;
import com.adinovis.domain.Membership;
import com.adinovis.domain.NotificationType;
import com.adinovis.domain.Social;
import com.adinovis.domain.UIConfig;
import com.adinovis.domain.UserBean;
import com.adinovis.domain.UserNotification;

public interface CloudunionCommonDao {
	List<Login> getloginId(String userid);

	List<UserBean> getLoginBean(String loginId);

	List<Login> getloginthroughsocial(String loginId, String password);

	Integer updatelogin(LoginwithSocial loginid);

	List<Social> getloginsocial(String loginId);

	int updateUserSession(String uId, String usersession);

}
