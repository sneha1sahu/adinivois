package com.adinovis.dao;

import java.math.BigInteger;
import java.util.List;

import com.adinovis.domain.AllAuditor;
import com.adinovis.domain.Auditorrole;
import com.adinovis.domain.Businessdetails;
import com.adinovis.domain.ClientData;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.Clientcountdetails;
import com.adinovis.domain.Clientdetails;
import com.adinovis.domain.Clientprofiles;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.GetUserClientId;
import com.adinovis.domain.Jurisdiction;
import com.adinovis.domain.Typeofentity;

public interface ClientDao
{
	boolean saveclientprofile(ClientProfile clientProfile);
	
	boolean tokensave(BigInteger accountid, String token);
	
    List<Clientprofiles> getclientprofile(String loginid);
    
    List<Clientstatus> getclientstatus();
    
    String clientvalidationemail(String emailid);
    
    List<Jurisdiction> getjurisdiction();
    
    List<Typeofentity> gettypeofentity();
    
    boolean clientinvitevalidation(String tokenkey,int statusid);
    
    List<Auditorrole> getauditorrole();
    
    List<AllAuditor> getallauditor();
    
    String clientvalidation(String emailid);
    
    List<Businessdetails> getbusinessdetails(String loginid);
    
    List<Clientdetails> getclientdetail(String token);
    
    boolean updateclientprofile(ClientProfile clientProfile);
    
    boolean deleteclientprofile(String clientfirmid,String loginid);
    
    List<GetUserClientId> getclientid(String emailid,String password);
    
    public List<ClientData> getClientData();

	void updateRefreshToken(ClientData clientData);

	void saveClientData(BigInteger engagementId, String json, int value, String accountListJson);

	ClientData getClientDataSourceByTokenValue(String tokenValue);
	
	boolean saveClientDataSource(String tokenValue, ClientData clientData);

    List<Clientcountdetails> getclientcountdetails(String loginid);

	void loadJsonIntoTrailBalance();

	List<ClientData> getSourcelink(int engagementId);

	boolean extractEngTBjsontotable(int engagementId);

	boolean extractEngATjsontotable(int engagementId);

	boolean updateAccountList(int engagementId);

	boolean loadtrailbalancemappingupdate(int engagementId);

	boolean loadQuickbookDataProcess(int engagementId);
}
