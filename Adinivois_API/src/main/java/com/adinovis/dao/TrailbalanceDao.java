package com.adinovis.dao;

import java.util.List;

import com.adinovis.domain.Adjustmenttype;
import com.adinovis.domain.BalanceSheet;
import com.adinovis.domain.ClientQuestionAnswer;
import com.adinovis.domain.FsDetails;
import com.adinovis.domain.TrailBalance;
import com.adinovis.domain.Gettrailbalance;
import com.adinovis.domain.IncomeSheet;
import com.adinovis.domain.MapSheet;
import com.adinovis.domain.MappingStatus;
import com.adinovis.domain.TrialbalanceAdjustmentType;

public interface TrailbalanceDao {

	List<Gettrailbalance> gettrailbalance(int engagementsid,int year);

	List<Adjustmenttype> getadjustmenttype();

	boolean deletetrailbalancerow(String intrailbalid, String loginid);

	boolean saveTrialbalanceAdjustmentType(TrialbalanceAdjustmentType trialbalAdjustmentType);

	List<TrialbalanceAdjustmentType> getalltrailadjustment(int inengagementsid);

	List<MappingStatus> getMappingStatus();
	
	List<BalanceSheet> getBalanceStatement(int engagementsid,int year);
	
	List<IncomeSheet> getIncomeStatement(int engagementsid,int year);
	
	List<IncomeSheet> getIncomeStatementTest(int engagementsid,int year);
	
	List<ClientQuestionAnswer> getQuestionAnswer(int inquestiontypeid,int inengagementsid);
	
	List<BalanceSheet> getBalanceSheet(int engagementsid,int year);

	List<Integer> trailbalanceWithDuplicates(TrailBalance duplicateTrailBalance);
	
	List<MapSheet> getMapSheetDetails();
	
	List<Integer> saveTrailbalanceMapping(TrailBalance trailbalance);
	
	boolean saveFsDetails(FsDetails fsDetails);
}
