/**
 * Program Name: CloudunionDao
 * 
 * Program Description / functionality: This is interface for CloudunionDao  layer 
 * 
 * Modules Impacted: 
 * 
 * Tables affected: 
 * 
 * Developer Created /Modified Date Purpose
 *******************************************************************************
 * Gouri     10/03/2017 
 * 
 * 
 * * Associated Defects Raised :
 *
 */
package com.adinovis.dao;

import java.math.BigInteger;
import java.util.List;

import com.adinovis.domain.CuserProfile;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;

public interface CinchDao {
	
	boolean signup(SignupDetails signupDetails);
	
	boolean tokensave(BigInteger accountid, String token);

	boolean updateuserprofile(Userprofileupdate userprofileupdate);
	
    List<CuserProfile> login(String emailid,String encpassword);
    
    List<CuserProfile> getuserprofileupdate(BigInteger accountid);
    
    String getemailid(String emailid);
    
    BigInteger getuserid(String emailid);
    
    BigInteger getclientid(String emailid,String password);
    
    String forgotPassword(String emailid);
    
    boolean loginvalidation(String tokenkey);
    
    String signupvalidation(String emailid);
    
    boolean changepassword(Integer useraccountid,String oldpassword,String newpassword);
    
    boolean forgotPasswordstatuschange(String emailid);
    
    boolean resetpassword(String tokenkey,String newpassword);
    
    String getUserVerification(String emailid);
    
}
