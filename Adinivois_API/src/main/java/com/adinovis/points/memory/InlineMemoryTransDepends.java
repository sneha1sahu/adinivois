package com.adinovis.points.memory;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.adinovis.points.domain.MembershipDetails;
import com.adinovis.points.domain.TransactionDependencies;

public class InlineMemoryTransDepends {

	private static LinkedHashMap<String, TransactionDependencies> transactionDependenciesMap;
	private static HashMap<Integer, MembershipDetails> membershipDetailsMap;
	
	public static LinkedHashMap<String, TransactionDependencies> getTransactionDependencies() {
		if (transactionDependenciesMap==null){
			transactionDependenciesMap=new LinkedHashMap<String, TransactionDependencies>();
		}
		return transactionDependenciesMap;
	}
	
	public static LinkedHashMap<String, TransactionDependencies> setTransactionDependencies(String consumerIdbusinessIdactualDate,TransactionDependencies dependencies) {
		getTransactionDependencies().put(consumerIdbusinessIdactualDate, dependencies);
		return transactionDependenciesMap;
	}
	
	public static TransactionDependencies getTransactionDependencies(String consumerIdbusinessIdactualDate) {
		TransactionDependencies transactionDependencies= getTransactionDependencies().get(consumerIdbusinessIdactualDate);
		return transactionDependencies;
	}
	
	public static MembershipDetails getMembershipDetails(int refereeMembership) {
		MembershipDetails membershipDetails= getMembershipDetailsMap().get(refereeMembership);
		return membershipDetails;
	}

	
	public static HashMap<Integer, MembershipDetails> getMembershipDetailsMap() {
		if (membershipDetailsMap==null){
			membershipDetailsMap=new HashMap<Integer, MembershipDetails>();
		}
		return membershipDetailsMap;
	}
	

	public static HashMap<Integer, MembershipDetails> setMembershipDetails(MembershipDetails membershipDetails) {
		getMembershipDetailsMap().put(membershipDetails.getMembership_type(), membershipDetails);
		return membershipDetailsMap;
	}

	
	
	public static MembershipDetails get (Integer keyPassed) {
          return getMembershipDetailsMap().get(keyPassed);         
	}
	
	
}