package com.adinovis.points;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.adinovis.common.TransactionConstant;
import com.adinovis.points.domain.PurchaseTransaction;
import com.adinovis.points.domain.TransactionYields;
import com.adinovis.points.memory.InlineMemoryTransDepends;




public class TransactionHandler {

	public void onFundingAvailable ( String businessID) {
		// This is previously approved. Basic calculations are done. 
	    // This transaction is now subjected to the "funding available" event
		PurchaseTransaction[] txApproved = getTransactions (businessID, "Approved");
		int fundedCount = 0;
		for (int i=0; i <= txApproved.length; i++) {
			// old for loop; check the transaction and the funding needed; one after one
			double fundingNeeded = txApproved[i].getDiscountByBusiness() * txApproved[i].getAmountBeforeTaxAndTips();
			boolean fundingAvailable = checkFundingAndDeduct (fundingNeeded, businessID);
			if (! fundingAvailable) {
				// ran out of funds
				// create funding request
				// break from the for loop
				createFundingRequest (businessID);
				break;
			} else {
				// create a status update request for the transaction;
				fundedCount++; 
				txApproved[i].setTransactionStatus(TransactionConstant.TRANSACTION_FUNDED);				
			}
		}
		for (int fCount=0; fCount < fundedCount; fCount++) {
			// update the transactions in batch format.
		    // execute the batch
		}
		for (int fCount=0; fCount < fundedCount; fCount++) {
			// create a trigger to onFunded();
		}
	}
	
	private boolean checkFundingAndDeduct(double fundingNeeded, String businessID) {
		// TODO Auto-generated method stub
		return false;
	}

	private PurchaseTransaction[] getTransactions(String businessID, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private void createFundingRequest(String businessID) {
		// TODO Auto-generated method stub
		
	}

	
	
	public void onFunded (PurchaseTransaction txFunded) {
		
		// when the transacion is funded; create the points count in yeilds table
		TransactionYields txYeildsConsumer = new TransactionYields();
		TransactionYields txYieldsConsumerReferee = new TransactionYields();
		TransactionYields txYieldsBusinessReferee = new TransactionYields();
		
		Vector<String> batchQueries = new Vector<String>();
		
		// calculate the txYieldsConsumer; there is no percentage gain for consumer. Pass 0 (bring 10 and 5 from DB)
		String yieldQuery = calculateYields (txFunded, txFunded.getConsumerID(), TransactionConstant.CONSUMER);
		// create the batch SQL statement; Add the ConsumerYeilds statement
		if (yieldQuery != null) batchQueries.add(yieldQuery);
		
		if (txFunded.getBusinessRefereeID() != null) {
			// calculate the business Referee's referral points
			// fetch his membership details
			// create thh busiReferral Yields statment - add it to batch;
			String brYieldQuery = calculateYields (txFunded, txFunded.getBusinessRefereeID(), TransactionConstant.BUSINESS_REFEREE);
									
			if (brYieldQuery != null) batchQueries.add(brYieldQuery);
		}
		if (txFunded.getConsumerRefereeID() != null) {
			// calculate the consumer referee's referral points
			// fetch his membership details
			// create the consumerReferral yields SQL statement. add it to batch;
			String crYieldQuery = calculateYields (txFunded, txFunded.getConsumerRefereeID(), 
					TransactionConstant.CONSUMER_REFEREE);
			if (crYieldQuery != null) batchQueries.add(crYieldQuery);
		}
		
		// execute the batch. All the Funded transactions have their yelds calculated
		if (batchQueries.size() > 0) {
			
		    // ???? Execute the Batch;
		}
	}

	public String dateToday(String format) {
		Date dNow = Calendar.getInstance().getTime();
		SimpleDateFormat sdf;
		if (format.equals("MMddyyyy")) {
			sdf = new SimpleDateFormat("MM/dd/yyyy");
		} else if (format.equals("MM/dd/yyyy")) {
				sdf = new SimpleDateFormat("MM/dd/yyyy");
		} else if (format.equals("yyyy-MM-dd")) {
			sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		} else  {
			sdf = new SimpleDateFormat("MM/dd/yyyy");
		}
		return sdf.format(dNow);
	}
	
	public String calculateYields (PurchaseTransaction transToUse, String yielderID, 
							int typeOfYielder) {
		   //	}, double percentageYield,
		   //				double businessFactorPercent, double currentCUGrowthRate) {
		
		TransactionYields tyConsumer = new TransactionYields();
		tyConsumer.setConsumerID(yielderID);
		tyConsumer.setTransactionFundedDate(Calendar.getInstance().getTime());
		// get the discount rate; get the transaction amount; get the growth factor rate;
		tyConsumer.setTransactionID(transToUse.getTransactionID());
		
		tyConsumer.setTransactionType(TransactionConstant.PURCHASE);
		
		// pionts is spelt wrong.. correct it when time permits.
		if (typeOfYielder == TransactionConstant.CONSUMER) {
			double totalPointsAccumulated = transToUse.getPurchasePoints();
			double purchasePoints = totalPointsAccumulated;
		    double pp_bf_points = transToUse.getAmountBeforeTaxAndTips() 
					* transToUse.getDiscountByBusiness();
		    double pp_gf_points = purchasePoints - pp_bf_points;
		    
		    
		    
			tyConsumer.getYieldPoints().setTotal_points(totalPointsAccumulated);
			tyConsumer.getYieldPoints().setPurchase_points(totalPointsAccumulated);
			tyConsumer.getYieldPoints().setPp_busi_factor_points(pp_bf_points);
			tyConsumer.getYieldPoints().setPp_grow_factor_points(pp_gf_points);
			
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
			
		} else if (typeOfYielder == TransactionConstant.CONSUMER_REFEREE) {
			double referRate = InlineMemoryTransDepends.getMembershipDetailsMap().get(transToUse.getConsumerRefereeMembership()).getMembership_consumer_ref_percentage();
			double totalConsRefPoints = transToUse.getAmountBeforeTaxAndTips()
					* referRate;
			tyConsumer.getYieldPoints().setTotal_points(totalConsRefPoints);	
			tyConsumer.getYieldPoints().setReferral_points(totalConsRefPoints);
			tyConsumer.getYieldPoints().setRp_consumer_points(totalConsRefPoints);
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
		} else if (typeOfYielder == TransactionConstant.BUSINESS_REFEREE) {
			double referRate =InlineMemoryTransDepends.getMembershipDetailsMap().get(transToUse.getBusinessRefereeMembership()).getMembership_business_ref_percentage();
			double totalBusiRefPoints = transToUse.getAmountBeforeTaxAndTips()
					* referRate;
			tyConsumer.getYieldPoints().setTotal_points(totalBusiRefPoints);	
			tyConsumer.getYieldPoints().setReferral_points(totalBusiRefPoints);
			tyConsumer.getYieldPoints().setRp_consumer_points(totalBusiRefPoints);
			// return the SQL statement for the insert
			// return ("insert into transactionYields set ( ) values ()");
		} else {
			// not covered case;
			return null;
		}
		// create a statement and return ;;; 
		String yieldInsertStatment = " insert into cu_transaction_yields " + 
		" (customer_id," +
		  " transaction_id," + 
		  " user_type," +
		  " as_of_date," + 
		  " p_as_of_timestamp," +
		  " trans_type," +
		  " total_points," + 
		  " purchase_points," + 
		  " pp_busi_factor_points," +
		  " pp_grow_factor_points," + 
		  " referral_points," + 
		  " rp_business_points," + 
		  " rp_consumer_points," +
		  " lt_total_points," + 
		  " lt_totalp_busi_factor_points," +
		  " lt_totalp_grow_factor_points," + 
		  " lt_purchase_points," +
		  " lt_pp_busi_factor_points," + 
		  " lt_pp_grow_factor_points) " +
		  " Values " +
		  " (" + tyConsumer.getConsumerID() + "," +
		  tyConsumer.getTransactionID() + "," +
		  tyConsumer.getTransactionFundedDate() + "," +
		  "now()," +
		  "1," +
		  tyConsumer.getYieldPoints().getTotal_points() + "," +
		  tyConsumer.getYieldPoints().getPurchase_points() + "," +
		  tyConsumer.getYieldPoints().getPp_busi_factor_points() + "," +
		  tyConsumer.getYieldPoints().getPp_grow_factor_points() + "," +
		  tyConsumer.getYieldPoints().getReferral_points() + "," +
		  tyConsumer.getYieldPoints().getRp_business_points() + "," +
		  tyConsumer.getYieldPoints().getRp_consumer_points() + "," +
		  tyConsumer.getYieldPoints().getTotal_points() + "," +
		  tyConsumer.getYieldPoints().getPp_busi_factor_points() + "," +
		  tyConsumer.getYieldPoints().getPp_grow_factor_points() + 
		  		tyConsumer.getYieldPoints().getReferral_points() + "," +
		  tyConsumer.getYieldPoints().getPurchase_points() + "," +
		  tyConsumer.getYieldPoints().getPp_busi_factor_points() + "," +
		  tyConsumer.getYieldPoints().getPp_grow_factor_points() + 
		  ")";
		  
		  
		return yieldInsertStatment;
		  
	}

}
