package com.adinovis.points.domain;

public class Points {

	public Points () {
		setConsumer_id("");
		setAs_of_date("");
	}
	
	
	
	public String getConsumer_id() {
		return consumer_id;
	}

	public void setConsumer_id(String consumer_id) {
		this.consumer_id = consumer_id;
	}

	public String getAs_of_date() {
		return as_of_date;
	}

	public void setAs_of_date(String as_of_date) {
		this.as_of_date = as_of_date;
	}

	public double getTotal_points() {
		return total_points;
	}

	public void setTotal_points(double total_points) {
		this.total_points = total_points;
	}

	public double getPurchase_points() {
		return purchase_points;
	}

	public void setPurchase_points(double purchase_points) {
		this.purchase_points = purchase_points;
	}

	public double getPp_busi_factor_points() {
		return pp_busi_factor_points;
	}

	public void setPp_busi_factor_points(double pp_busi_factor_points) {
		this.pp_busi_factor_points = pp_busi_factor_points;
	}

	public double getPp_grow_factor_points() {
		return pp_grow_factor_points;
	}

	public void setPp_grow_factor_points(double pp_grow_factor_points) {
		this.pp_grow_factor_points = pp_grow_factor_points;
	}

	public double getReferral_points() {
		return referral_points;
	}

	public void setReferral_points(double referral_points) {
		this.referral_points = referral_points;
	}

	public double getRp_business_points() {
		return rp_business_points;
	}

	public void setRp_business_points(double rp_business_points) {
		this.rp_business_points = rp_business_points;
	}

	public double getRp_consumer_points() {
		return rp_consumer_points;
	}

	public void setRp_consumer_points(double rp_consumer_points) {
		this.rp_consumer_points = rp_consumer_points;
	}

	public double getCash_points() {
		return cash_points;
	}

	public void setCash_points(double cash_points) {
		this.cash_points = cash_points;
	}

	public double getCashp_busi_factor_points() {
		return cashp_busi_factor_points;
	}

	public void setCashp_busi_factor_points(double cashp_busi_factor_points) {
		this.cashp_busi_factor_points = cashp_busi_factor_points;
	}

	public double getCashp_grow_factor_points() {
		return cashp_grow_factor_points;
	}

	public void setCashp_grow_factor_points(double cashp_grow_factor_points) {
		this.cashp_grow_factor_points = cashp_grow_factor_points;
	}

	public double getLt_total_points() {
		return lt_total_points;
	}

	public void setLt_total_points(double lt_total_points) {
		this.lt_total_points = lt_total_points;
	}

	public double getLt_totalp_busi_factor_points() {
		return lt_totalp_busi_factor_points;
	}

	public void setLt_totalp_busi_factor_points(double lt_totalp_busi_factor_points) {
		this.lt_totalp_busi_factor_points = lt_totalp_busi_factor_points;
	}

	public double getLt_totalp_grow_factor_points() {
		return lt_totalp_grow_factor_points;
	}

	public void setLt_totalp_grow_factor_points(double lt_totalp_grow_factor_points) {
		this.lt_totalp_grow_factor_points = lt_totalp_grow_factor_points;
	}

	public double getLt_purchase_points() {
		return lt_purchase_points;
	}

	public void setLt_purchase_points(double lt_purchase_points) {
		this.lt_purchase_points = lt_purchase_points;
	}

	public double getLt_pp_busi_factor_points() {
		return lt_pp_busi_factor_points;
	}

	public void setLt_pp_busi_factor_points(double lt_pp_busi_factor_points) {
		this.lt_pp_busi_factor_points = lt_pp_busi_factor_points;
	}

	public double getLt_pp_grow_factor_points() {
		return lt_pp_grow_factor_points;
	}

	public void setLt_pp_grow_factor_points(double lt_pp_grow_factor_points) {
		this.lt_pp_grow_factor_points = lt_pp_grow_factor_points;
	}

	public double getLt_cash_points() {
		return lt_cash_points;
	}

	public void setLt_cash_points(double lt_cash_points) {
		this.lt_cash_points = lt_cash_points;
	}

	public double getLt_cp_busi_factor_points() {
		return lt_cp_busi_factor_points;
	}

	public void setLt_cp_busi_factor_points(double lt_cp_busi_factor_points) {
		this.lt_cp_busi_factor_points = lt_cp_busi_factor_points;
	}

	public double getLt_cp_grow_factor_points() {
		return lt_cp_grow_factor_points;
	}

	public void setLt_cp_grow_factor_points(double lt_cp_grow_factor_points) {
		this.lt_cp_grow_factor_points = lt_cp_grow_factor_points;
	}

	public double getLt_redeemed_points() {
		return lt_redeemed_points;
	}

	public void setLt_redeemed_points(double lt_redeemed_points) {
		this.lt_redeemed_points = lt_redeemed_points;
	}

	public double getLt_rp_busi_factor_points() {
		return lt_rp_busi_factor_points;
	}

	public void setLt_rp_busi_factor_points(double lt_rp_busi_factor_points) {
		this.lt_rp_busi_factor_points = lt_rp_busi_factor_points;
	}

	public double getLt_rp_grow_factor_points() {
		return lt_rp_grow_factor_points;
	}

	public void setLt_rp_grow_factor_points(double lt_rp_grow_factor_points) {
		this.lt_rp_grow_factor_points = lt_rp_grow_factor_points;
	}


	
	
	public void addPoint (Points tTemp) {
		//3
		setTotal_points(getTotal_points() + tTemp.getTotal_points()); //3
		setPurchase_points (getPurchase_points() + tTemp.getPurchase_points()); //4
		setPp_busi_factor_points (getPp_busi_factor_points() + tTemp.getPp_busi_factor_points()); //5
		setPp_grow_factor_points (getPp_grow_factor_points() + tTemp.getPp_grow_factor_points()); //6
		setReferral_points (getReferral_points() + tTemp.getReferral_points()); //7
		setRp_business_points (getRp_business_points() + tTemp.getRp_business_points());
		setRp_consumer_points (getRp_consumer_points() + tTemp.getRp_consumer_points());
		setCash_points (getCash_points() + tTemp.getCash_points()); //10
		setCashp_busi_factor_points (getCashp_busi_factor_points() + tTemp.getCashp_busi_factor_points());
		setCashp_grow_factor_points (getCashp_grow_factor_points() + tTemp.getCashp_grow_factor_points());
		setLt_total_points (getLt_total_points() + tTemp.getLt_total_points()); //13
		setLt_totalp_busi_factor_points (getLt_totalp_busi_factor_points() + tTemp.getLt_totalp_busi_factor_points());
		setLt_totalp_grow_factor_points (getLt_totalp_grow_factor_points() + tTemp.getLt_totalp_grow_factor_points());
		setLt_purchase_points (getLt_purchase_points() + tTemp.getLt_purchase_points()); //16
		setLt_pp_busi_factor_points (getLt_pp_busi_factor_points() + tTemp.getLt_pp_busi_factor_points());
		setLt_pp_grow_factor_points (getLt_pp_grow_factor_points() + tTemp.getLt_pp_grow_factor_points());
		setLt_cash_points (getLt_cash_points() + tTemp.getLt_cash_points()); //19
		setLt_cp_busi_factor_points (getLt_cp_busi_factor_points() + tTemp.getLt_cp_busi_factor_points());
		setLt_cp_grow_factor_points (getLt_cp_grow_factor_points() + tTemp.getLt_cp_grow_factor_points()); //21
		setLt_redeemed_points (getLt_redeemed_points() + tTemp.getLt_redeemed_points());
		setLt_rp_busi_factor_points (getLt_rp_busi_factor_points() + tTemp.getLt_rp_busi_factor_points());
		setLt_rp_grow_factor_points (getLt_rp_grow_factor_points() + tTemp.getLt_rp_grow_factor_points()); //24
		setLt_mgmt_fee (getLt_mgmt_fee() + tTemp.getLt_mgmt_fee()); // 25
		setLt_mf_busi_factor_points (getLt_mf_busi_factor_points() + tTemp.getLt_mf_busi_factor_points()); // 28
		setLt_mf_grow_factor_points (getLt_mf_grow_factor_points() + tTemp.getLt_mf_grow_factor_points()); // 29
	}

	
	@Override
	public String toString() {
		return "Points [consumer_id=" + consumer_id + ", as_of_date=" + as_of_date + ", total_points="
				+ total_points + ", purchase_points=" + purchase_points + ", pp_busi_factor_points="
				+ pp_busi_factor_points + ", pp_grow_factor_points=" + pp_grow_factor_points + ", referral_points="
				+ referral_points + ", rp_business_points=" + rp_business_points + ", rp_consumer_points="
				+ rp_consumer_points + ", cash_points=" + cash_points + ", cashp_busi_factor_points="
				+ cashp_busi_factor_points + ", cashp_grow_factor_points=" + cashp_grow_factor_points
				+ ", lt_total_points=" + lt_total_points + ", lt_totalp_busi_factor_points="
				+ lt_totalp_busi_factor_points + ", lt_totalp_grow_factor_points=" + lt_totalp_grow_factor_points
				+ ", lt_purchase_points=" + lt_purchase_points + ", lt_pp_busi_factor_points="
				+ lt_pp_busi_factor_points + ", lt_pp_grow_factor_points=" + lt_pp_grow_factor_points
				+ ", lt_cash_points=" + lt_cash_points + ", lt_cp_busi_factor_points=" + lt_cp_busi_factor_points
				+ ", lt_cp_grow_factor_points=" + lt_cp_grow_factor_points + ", lt_redeemed_points="
				+ lt_redeemed_points + ", lt_rp_busi_factor_points=" + lt_rp_busi_factor_points
				+ ", lt_rp_grow_factor_points=" + lt_rp_grow_factor_points + ", mgmt_fee=" + lt_mgmt_fee + "]";
	}
	
	String consumer_id; //1 // consumer_id
	String as_of_date; //2
	double total_points; //3
	double purchase_points; //4
	double   pp_busi_factor_points; //5
	double   pp_grow_factor_points;  //6
	double referral_points; //7
	double	 rp_business_points; //8
	double   rp_consumer_points; // 9
	double cash_points;  // 10
	double   cashp_busi_factor_points; //11
	double   cashp_grow_factor_points; //12

	double lt_total_points;  //13
	double   lt_totalp_busi_factor_points; //14
	double   lt_totalp_grow_factor_points; //15
	double lt_purchase_points; //16
	double   lt_pp_busi_factor_points; //17
	double   lt_pp_grow_factor_points; //18
	double lt_cash_points; // 19
	double   lt_cp_busi_factor_points; // 20
	double   lt_cp_grow_factor_points; // 21
	double lt_redeemed_points; // 22
	double   lt_rp_busi_factor_points; // 23
	double   lt_rp_grow_factor_points; // 24


	double lt_mgmt_fee; // 25	
	double multiplier; //26
	int    user_type;//27


	double lt_mf_busi_factor_points; // 28
    double lt_mf_grow_factor_points; // 29
	

	public double getLt_mgmt_fee() {
		return lt_mgmt_fee;
	}

	public void setLt_mgmt_fee(double lt_mgmt_fee) {
		this.lt_mgmt_fee = lt_mgmt_fee;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	public double getLt_mf_busi_factor_points() {
		return lt_mf_busi_factor_points;
	}

	public void setLt_mf_busi_factor_points(double lt_mf_busi_factor_points) {
		this.lt_mf_busi_factor_points = lt_mf_busi_factor_points;
	}

	public double getLt_mf_grow_factor_points() {
		return lt_mf_grow_factor_points;
	}

	public void setLt_mf_grow_factor_points(double lt_mf_grow_factor_points) {
		this.lt_mf_grow_factor_points = lt_mf_grow_factor_points;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}
	
	public int getUser_type() {
		return user_type;
	}
}
