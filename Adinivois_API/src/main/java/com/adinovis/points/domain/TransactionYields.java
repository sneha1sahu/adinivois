package com.adinovis.points.domain;



import java.util.Date;

public class TransactionYields {
private	String transactionID;
private	Date   transactionFundedDate;
private	int    transactionType;
private	String consumerID;
private	Points yieldPoints;
public String getTransactionID() {
	return transactionID;
}
public void setTransactionID(String transactionID) {
	this.transactionID = transactionID;
}
public Date getTransactionFundedDate() {
	return transactionFundedDate;
}
public void setTransactionFundedDate(Date transactionFundedDate) {
	this.transactionFundedDate = transactionFundedDate;
}
public int getTransactionType() {
	return transactionType;
}
public void setTransactionType(int transactionType) {
	this.transactionType = transactionType;
}
public String getConsumerID() {
	return consumerID;
}
public void setConsumerID(String consumerID) {
	this.consumerID = consumerID;
}
public Points getYieldPoints() {
	return yieldPoints;
}
public void setYieldPoints(Points yieldPoints) {
	this.yieldPoints = yieldPoints;
}

}
