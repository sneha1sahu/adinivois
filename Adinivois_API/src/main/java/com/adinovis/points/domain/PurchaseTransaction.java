package com.adinovis.points.domain;



import java.util.Date;

public class PurchaseTransaction {

	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public Date getActualDate() {
		return actualDate;
	}
	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}
	public String getBusinessID() {
		return businessID;
	}
	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}
	public String getConsumerID() {
		return consumerID;
	}
	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}
	public double getAmountOfTransaction() {
		return amountOfTransaction;
	}
	public void setAmountOfTransaction(double amountOfTransaction) {
		this.amountOfTransaction = amountOfTransaction;
	}
	public double getAmountBeforeTaxAndTips() {
		return amountBeforeTaxAndTips;
	}
	public void setAmountBeforeTaxAndTips(double amountBeforeTaxAndTips) {
		this.amountBeforeTaxAndTips = amountBeforeTaxAndTips;
	}
	public String getBusinessRefereeID() {
		return businessRefereeID;
	}
	public void setBusinessRefereeID(String businessRefereeID) {
		this.businessRefereeID = businessRefereeID;
	}
	public String getConsumerRefereeID() {
		return consumerRefereeID;
	}
	public void setConsumerRefereeID(String consumerRefereeID) {
		this.consumerRefereeID = consumerRefereeID;
	}
	public int getBusinessRefereeMembership() {
		return businessRefereeMembership;
	}
	public void setBusinessRefereeMembership(int businessRefereeMembership) {
		this.businessRefereeMembership = businessRefereeMembership;
	}
	public int getConsumerRefereeMembership() {
		return consumerRefereeMembership;
	}
	public void setConsumerRefereeMembership(int consumerRefereeMembership) {
		this.consumerRefereeMembership = consumerRefereeMembership;
	}
	public double getDiscountByBusiness() {
		return discountByBusiness;
	}
	public void setDiscountByBusiness(double discountByBusiness) {
		this.discountByBusiness = discountByBusiness;
	}
	public int getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(int transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public int getWf_status() {
		return wf_status;
	}
	public void setWf_status(int wf_status) {
		this.wf_status = wf_status;
	}
	public double getPurchasePoints() {
		return purchasePoints;
	}
	public void setPurchasePoints(double purchasePoints) {
		this.purchasePoints = purchasePoints;
	}
	
	public double getCashPoints() {
		return cashPoints;
	}
	public void setCashPoints(double cashPoints) {
		this.cashPoints = cashPoints;
	}
	public double getRedeemPoints() {
		return redeemPoints;
	}
	public void setRedeemPoints(double redeemPoints) {
		this.redeemPoints = redeemPoints;
	}

	public double getDiscountedAmount() {
		discountedAmount=(getDiscountByBusiness() * getAmountBeforeTaxAndTips())/100;
		return discountedAmount;
	}

	public String getTransactionNum() {
		return transactionNum;
	}
	public void setTransactionNum(String transactionNum) {
		this.transactionNum = transactionNum;
	}

	private String transactionID;
	private String transactionNum;
	private Date   postedDate;
	private Date   actualDate;
	private String businessID;
	private String consumerID;
	private double amountOfTransaction; // Grand Total
	private double amountBeforeTaxAndTips; // Sub Total
	private String businessRefereeID;
	private String consumerRefereeID;
	private int    businessRefereeMembership;
	private int    consumerRefereeMembership;
	private double discountByBusiness;
	private int    transactionStatus;
	private double purchasePoints;
	private double cashPoints;
	private double redeemPoints;
	private double discountedAmount;
	private int    wf_status;
	
}
