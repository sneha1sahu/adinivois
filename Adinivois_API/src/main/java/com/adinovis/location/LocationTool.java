package com.adinovis.location;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.domain.Suggestion;

public class LocationTool {
	// Read from URL and return the content in a String
    public static String readURLContent(String urlString) 
        throws IOException
    {
        URL url = new URL(urlString);
        Scanner scan = new Scanner(url.openStream());
        String content = new String();
        while (scan.hasNext())
            content += scan.nextLine();
        scan.close();
        return content;
    }
     
    // Extract the middle part of a string from "open" to "close"
    public static String extractMiddle(String str, 
        String open, String close)
    {
        int begin = str.indexOf(open) + open.length();
        int end = str.indexOf(close, begin);
        return str.substring(begin, end);       
    }
     
    // Extract the middle part of a string from "open" to the end
    public static String extractMiddle(String str, String open)
    {
        int begin = str.indexOf(open) + open.length();
        return str.substring(begin);        
    }
    public static String extractFromAdress(String str, String open)
    {
        int begin = str.indexOf(open) + open.length();
        return str.substring(begin);        
    }
    // Make a Location object from a string address
    public static Location makeLocation(String addr) 
        throws IOException
    {
        String url ="http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
        url += URLEncoder.encode(addr, "UTF-8");
        String content = readURLContent(url);
        int isGeoLocatted=readMap(content);
        String formatted_address=null;
        double lat=0;
        double lng=0;
        if(isGeoLocatted==CloudunionConstants.GEOLOCATED){
        	 formatted_address = extractMiddle(content,"\"formatted_address\" : \"", "\",");
             String latlng = extractMiddle(content,"\"location\" : {", "},");
             lat = Double.parseDouble(extractMiddle(latlng,"\"lat\" :", ","));
             lng = Double.parseDouble(extractMiddle(latlng,"\"lng\" :"));
        }
        else{
        	
        }
        return new Location(formatted_address, lat, lng);
    }
    private static int readMap(String content) {
    //content {"results" : [],"status" : "ZERO_RESULTS"}
    	int geoLocated=0;
    	JSONParser parser = new JSONParser();

    try {

        Object obj = parser.parse(content);

        JSONObject jsonObject = (JSONObject) obj;

        org.json.simple.JSONArray results = (org.json.simple.JSONArray) jsonObject.get("results");
        if(results.size()>0){
        	geoLocated=CloudunionConstants.GEOLOCATED;
        }       

    } catch (Exception e) {
        e.printStackTrace();
    }
    return geoLocated;
}
	public static ArrayList<Location> loadData(ArrayList<Location> loadData) {
		return loadData;
	}

	public static List<Suggestion> loadData(List<Suggestion> suggestions) {
		return suggestions;
	}
}
