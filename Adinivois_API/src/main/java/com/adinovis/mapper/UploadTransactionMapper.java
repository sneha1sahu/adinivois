package com.adinovis.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.adinovis.common.DateUtils;
import com.adinovis.domain.BusinessTransaction;
import com.adinovis.domain.Transaction;
import com.adinovis.domain.UploadTransaction;
import com.adinovis.points.domain.PurchaseTransaction;

public class UploadTransactionMapper {

	public static List<Transaction> getPurchaseTransaction(List<UploadTransaction> uploadTransactions) {
		List<Transaction> purchaseTransaction = new ArrayList<Transaction>();

		for (UploadTransaction transaction : uploadTransactions) {
			Transaction purchase = new Transaction();
			purchase.setAddress(transaction.getAddress());
			purchase.setBusinessId(transaction.getBusinessName());
			purchase.setComment(transaction.getRatingComment());
			purchase.setDisputeComment(transaction.getDisputeComment());
			purchase.setIs_refund(transaction.getIsRefund());
			purchase.setIs_refund_requested(transaction.getIsRefundRequested());
			purchase.setRating(transaction.getRatingLevel());
			purchase.setReceiptNo(transaction.getReceiptNo());
			purchase.setRefund_request_reason(transaction.getRefundRequestReason());
			purchase.setStatus(getTransactionStatus(transaction.getTransStatus()));
			purchase.setTransactionAmount(transaction.getAmountBeforeTaxTips());
			purchase.setTransactionDate(transaction.getActualDate());
			purchase.setIsDispute(transaction.getIsDispute());
			purchase.setTransactionId(transaction.getTransactionId());
			purchase.setTransactionPic(transaction.getReceiptUrl());
			purchase.setTransAmount(transaction.getAmountGrandTotal());
			purchase.setBusinessDiscountedAmount(transaction.getBusinessDiscountedAmount());
			purchase.setTransactionNo(transaction.getTransactionNo());
			purchase.setIsRated(transaction.getIsRated());
			purchaseTransaction.add(purchase);
		}

		return purchaseTransaction;
	}
	
	

	public static List<Transaction> getConsumerTransaction(List<UploadTransaction> uploadTransactions, int uId) {
		List<Transaction> purchaseTransaction = new ArrayList<Transaction>();

		for (UploadTransaction transaction : uploadTransactions) {
			Transaction purchase = new Transaction();
			purchase.setuId(uId);
			purchase.setAddress(transaction.getAddress());
			purchase.setBusinessId(transaction.getBusinessName());
			purchase.setComment(transaction.getRatingComment());
			purchase.setDisputeComment(transaction.getDisputeComment());
			purchase.setIs_refund(transaction.getIsRefund());
			purchase.setIs_refund_requested(transaction.getIsRefundRequested());
			purchase.setRating(transaction.getRatingLevel());
			purchase.setReceiptNo(transaction.getReceiptNo());
			purchase.setRefund_request_reason(transaction.getRefundRequestReason());
			purchase.setStatus(getTransactionStatus(transaction.getTransStatus()));
			purchase.setTransactionAmount(transaction.getAmountBeforeTaxTips());
			purchase.setTransactionDate(transaction.getActualDate());
			purchase.setIsDispute(transaction.getIsDispute());
			purchase.setTransactionId(transaction.getTransactionId());
			purchase.setTransactionPic(transaction.getReceiptUrl());
			purchase.setTransAmount(transaction.getAmountGrandTotal());
			purchase.setBusinessDiscountedAmount(transaction.getBusinessDiscountedAmount());
			purchase.setTransactionNo(transaction.getTransactionNo());
			purchase.setIsRated(transaction.getIsRated());
			purchaseTransaction.add(purchase);
		}

		return purchaseTransaction;
	}

	public static PurchaseTransaction beforeApproveTransaction(UploadTransaction uploadTransaction) {
		PurchaseTransaction purchaseTransaction = new PurchaseTransaction();
		purchaseTransaction.setActualDate(DateUtils.convertStringToDate(DateUtils.CLOUDUNION_DATE, uploadTransaction.getActualDate()));
		purchaseTransaction.setAmountBeforeTaxAndTips(Double.parseDouble(uploadTransaction.getAmountBeforeTaxTips()));
		purchaseTransaction.setBusinessID(uploadTransaction.getBusinessId());
		purchaseTransaction.setConsumerID(uploadTransaction.getConsumerId());
		purchaseTransaction.setTransactionID(uploadTransaction.getTransactionId());
		purchaseTransaction.setTransactionStatus(Integer.parseInt(uploadTransaction.getTransStatus()));
		return purchaseTransaction;
	}

	public static PurchaseTransaction getPurchaseTransaction(UploadTransaction uploadTransaction) {
		PurchaseTransaction purchaseTransaction = new PurchaseTransaction();

		purchaseTransaction.setActualDate(
				DateUtils.convertStringToDate(DateUtils.CLOUDUNION_DATE, uploadTransaction.getActualDate()));
		purchaseTransaction.setAmountBeforeTaxAndTips(Double.parseDouble(uploadTransaction.getAmountBeforeTaxTips()));

		if (StringUtils.isNotBlank(uploadTransaction.getAmountGrandTotal()))
			purchaseTransaction.setAmountOfTransaction(Double.parseDouble(uploadTransaction.getAmountGrandTotal()));

		purchaseTransaction.setBusinessID(uploadTransaction.getBusinessId());
		if (StringUtils.isNotBlank(uploadTransaction.getTransDayBusReferral())
				&& uploadTransaction.getTransDayBusReferral().length() > 0)
			purchaseTransaction.setBusinessRefereeID(uploadTransaction.getTransDayBusReferral());

		if (StringUtils.isNotBlank(uploadTransaction.getTransDayBusReferralMembership())
				&& uploadTransaction.getTransDayBusReferralMembership().length() > 0)
			purchaseTransaction.setBusinessRefereeMembership(
					Integer.parseInt(uploadTransaction.getTransDayBusReferralMembership()));

		if (StringUtils.isNotBlank(uploadTransaction.getCashPoints()) && uploadTransaction.getCashPoints().length() > 0)
			purchaseTransaction.setCashPoints(Double.parseDouble(uploadTransaction.getCashPoints()));

		purchaseTransaction.setConsumerID(uploadTransaction.getConsumerId());

		if (StringUtils.isNotBlank(uploadTransaction.getTransDayConReferral())
				&& uploadTransaction.getTransDayConReferral().length() > 0)
			purchaseTransaction.setConsumerRefereeID(uploadTransaction.getTransDayConReferral());

		if (StringUtils.isNotBlank(uploadTransaction.getTransDayConReferralMembership())
				&& uploadTransaction.getTransDayConReferralMembership().length() > 0)
			purchaseTransaction.setConsumerRefereeMembership(
					Integer.parseInt(uploadTransaction.getTransDayConReferralMembership()));

		if (StringUtils.isNotBlank(uploadTransaction.getDayBusDiscount())
				&& uploadTransaction.getDayBusDiscount().length() > 0)
			purchaseTransaction.setDiscountByBusiness(Double.parseDouble(uploadTransaction.getDayBusDiscount()));

		if (StringUtils.isNotBlank(uploadTransaction.getPostedDate()) && uploadTransaction.getPostedDate().length() > 0)
			purchaseTransaction.setPostedDate(
					DateUtils.convertStringToDate(DateUtils.CLOUDUNION_DATE, uploadTransaction.getPostedDate()));

		if (StringUtils.isNotBlank(uploadTransaction.getPurchasePoint())
				&& uploadTransaction.getPurchasePoint().length() > 0)
			purchaseTransaction.setPurchasePoints(Double.parseDouble(uploadTransaction.getPurchasePoint()));

		if (StringUtils.isNotBlank(uploadTransaction.getRedeemPoints())
				&& uploadTransaction.getRedeemPoints().length() > 0)
			purchaseTransaction.setRedeemPoints(Double.parseDouble(uploadTransaction.getRedeemPoints()));
		
		if (StringUtils.isNotBlank(uploadTransaction.getTransactionNo())
				&& uploadTransaction.getTransactionNo().length() > 0)
			purchaseTransaction.setTransactionNum(uploadTransaction.getTransactionNo());

		purchaseTransaction.setTransactionID(uploadTransaction.getTransactionId());

		purchaseTransaction.setTransactionStatus(Integer.parseInt(uploadTransaction.getTransStatus()));

		return purchaseTransaction;
	}

	public static List<BusinessTransaction> getSaleTransaction(List<UploadTransaction> uploadTransactions) {
		List<BusinessTransaction> saleTransaction = new ArrayList<BusinessTransaction>();

		for (UploadTransaction transaction : uploadTransactions) {
			BusinessTransaction businessTransaction = new BusinessTransaction();
			businessTransaction.setBusinessId(transaction.getBusinessId());
			businessTransaction.setComment(transaction.getRatingComment());
			businessTransaction.setConsumerId(transaction.getConsumerId());
			businessTransaction.setConsumerName(transaction.getConsumerName());
			businessTransaction.setDisputeComment(transaction.getDisputeComment());
			businessTransaction.setPostedDate(transaction.getPostedDate());
			businessTransaction.setRating(transaction.getRatingLevel());
			businessTransaction.setReceiptNo(transaction.getReceiptNo());
			businessTransaction.setTransactionAmount(transaction.getAmountGrandTotal());
			businessTransaction.setTransactionAmountBeforeTaxTip(transaction.getAmountBeforeTaxTips());
			businessTransaction.setTransactionDate(transaction.getActualDate());
			businessTransaction.setTransactionDispute(transaction.getIsDispute());
			businessTransaction.setTransactionId(transaction.getTransactionId());
			businessTransaction.setTransactionPic(transaction.getReceiptUrl());
			businessTransaction.setTransactionstatus(getTransactionStatus(transaction.getTransStatus()));
			businessTransaction.setTransactionType(getTransactionType(transaction.getTransType()));
			saleTransaction.add(businessTransaction);
		}

		return saleTransaction;
	}

	private static String getTransactionStatus(String id) {
		String status = "";
		switch (Integer.parseInt(id)) {
		case 1:
			status = "Pending";
			break;
		case 2:
			status = "Approved";
			break;
		case 3:
			status = "Declined";
			break;
		case 4:
			status = "Funded";
			break;

		}
		return status;
	}

	private static String getTransactionType(String id) {
		String status = "";
		switch (Integer.parseInt(id)) {
		case 100:
			status = "Purchase";
			break;
		case 200:
			status = "Refund";
			break;
		case 300:
			status = "CashPoints";
			break;
		case 400:
			status = "Redeemed";
			break;
		case 500:
			status = "Transfer";
			break;

		}
		return status;
	}
}
