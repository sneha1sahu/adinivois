package com.adinovis.service;

import java.math.BigInteger;
import java.util.List;

import com.adinovis.domain.Allengagement;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.Engagement;
import com.adinovis.domain.EngagementYear;
import com.adinovis.domain.Engagementcountdetail;
import com.adinovis.domain.Engagementdetails;
import com.adinovis.domain.Engagementtype;

public interface EngagementService {
	
	boolean updateengagementdetails(Engagement engagement);
	
	boolean deleteengagement(String engagementsid,String loginid);
	
	List<Engagementdetails> getengagementdetails(int clientfirmid,String loginid);
	
	List<Allengagement> getallengagement(String loginid);
	
	List<Engagementtype> getengagement();
	
	List<Clientstatus> getengagementstatus();
	
	boolean saveengagement(Engagement engagement);
	
	List<Engagementcountdetail> getengagementcountdetail(String loginid);
	
	List<EngagementYear> getEngagementYearDetails(int inengagementsid);
	
	List<Integer> getEngagementId(int clientfirmid);
	
}
