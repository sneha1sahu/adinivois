package com.adinovis.service;

import java.math.BigInteger;
import java.util.List;

import com.adinovis.domain.AllAuditor;
import com.adinovis.domain.Auditorrole;
import com.adinovis.domain.Businessdetails;
import com.adinovis.domain.ClientData;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.Clientcountdetails;
import com.adinovis.domain.Clientdetails;
import com.adinovis.domain.Clientprofiles;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.GetUserClientId;
import com.adinovis.domain.Jurisdiction;
import com.adinovis.domain.Typeofentity;

public interface ClientService {
	
	boolean saveclientprofile(ClientProfile clientProfile);
	
    List<Clientprofiles> getclientprofile(String loginid);
    
	List<Clientstatus> getclientstatus();
    
	String clientvalidationemail(String emailid);
	
    List<Jurisdiction> getjurisdiction();
    
    boolean tokensave(BigInteger accountid,String token);
    
    String clientvalidation(String emailid);
    
    List<Typeofentity> gettypeofentity();
    
	boolean clientinvitevalidation(String tokenkey,int statusid);
    
    List<Auditorrole> getauditorrole();
    
    List<AllAuditor> getallauditor();
    
    List<Businessdetails> getbusinessdetails(String loginid);
    
    List<Clientdetails> getclientdetail(String token);
    
    boolean updateclientprofile(ClientProfile clientProfile);
    
    boolean deleteclientprofile(String clientfirmid,String loginid);
    
    List<GetUserClientId> getclientid (String emailid,String password);

	ClientData getClientDataSourceByTokenValue(String tokenValue);

    List<Clientcountdetails> getclientcountdetails(String loginid);
}
