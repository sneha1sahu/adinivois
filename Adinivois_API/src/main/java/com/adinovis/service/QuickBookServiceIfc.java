package com.adinovis.service;

import org.springframework.web.servlet.View;

public interface QuickBookServiceIfc {

	View connect(String tokenValue);

	boolean callBackFromOAuth(String code, String authCode, String realmId);
}
