package com.adinovis.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.adinovis.dao.TrailbalanceDao;
import com.adinovis.domain.Adjustmenttype;
import com.adinovis.domain.BalanceSheet;
import com.adinovis.domain.ClientQuestionAnswer;
import com.adinovis.domain.FsDetails;
import com.adinovis.domain.TrailBalance;
import com.adinovis.domain.Gettrailbalance;
import com.adinovis.domain.IncomeSheet;
import com.adinovis.domain.MapSheet;
import com.adinovis.domain.MappingStatus;
import com.adinovis.domain.TrialbalanceAdjustmentType;
import com.adinovis.service.TrailbalanceService;

public class TrailbalanceServiceImpl implements TrailbalanceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrailbalanceServiceImpl.class);

	@Autowired
	private TrailbalanceDao trailbalanceDao;

	public TrailbalanceDao getTrailbalanceDao() {
		return trailbalanceDao;
	}

	public void setTrailbalanceDao(TrailbalanceDao trailbalanceDao) {
		this.trailbalanceDao = trailbalanceDao;
	}

	public List<Gettrailbalance> gettrailbalance(int engagementsid,int year) {
		LOGGER.debug("gettrailbalance ");
		return trailbalanceDao.gettrailbalance(engagementsid,year);
	}

	public List<Adjustmenttype> getadjustmenttype() {
		LOGGER.debug("getadjustmenttype ");
		return trailbalanceDao.getadjustmenttype();
	}

	@Override
	public boolean deletetrailbalancerow(String intrailbalid, String loginid) {
		LOGGER.debug("deletetrailbalancerow ");
		return trailbalanceDao.deletetrailbalancerow(intrailbalid, loginid);
	}

	@Override
	public boolean saveTrialbalanceAdjustmentType(TrialbalanceAdjustmentType trialbalAdjustmentType) {
		LOGGER.debug("save trailbalance adjustmenttype ");
		return trailbalanceDao.saveTrialbalanceAdjustmentType(trialbalAdjustmentType);
	}

	@Override
	public List<TrialbalanceAdjustmentType> getalltrailadjustment(int inengagementsid) {
		LOGGER.debug("get alltrailadjustment ");
		return trailbalanceDao.getalltrailadjustment(inengagementsid);
	}

	@Override
	public List<MappingStatus> getMappingStaus() {
		LOGGER.debug("get mappingstatus ");
		return trailbalanceDao.getMappingStatus();
	}

	@Override
	public List<BalanceSheet> getBalanceStatement(int engagementsid, int year) {
		LOGGER.debug("get balance statement ");
		return trailbalanceDao.getBalanceStatement(engagementsid,year);
	}

	@Override
	public List<IncomeSheet> getIncomeStatement(int engagementsid, int year) {
		LOGGER.debug("get income statement ");
		return trailbalanceDao.getIncomeStatement(engagementsid,year);
	}

	@Override
	public List<ClientQuestionAnswer> getQuestionAnswer(int inquestiontypeid, int inengagementsid) {
		LOGGER.debug("get question answer ");
		return trailbalanceDao.getQuestionAnswer(inquestiontypeid,inengagementsid);
	}

	@Override
	public List<BalanceSheet> getBalanceSheet(int engagementsid, int year) {
		LOGGER.debug("get balance sheet ");
		return trailbalanceDao.getBalanceSheet(engagementsid,year);
	}

	@Override
	public List<IncomeSheet> getIncomeStatementTest(int engagementsid, int year) {
		LOGGER.debug("get income statement test");
		return trailbalanceDao.getIncomeStatementTest(engagementsid,year);
	}

	@Override
	public List<Integer> trailbalanceWithDuplicates(TrailBalance trailBalance) {
		LOGGER.debug("save trail balance with duplictes");
		return trailbalanceDao.trailbalanceWithDuplicates(trailBalance);
	}

	@Override
	public List<MapSheet> getMapSheetDetails() {
		LOGGER.debug("get map sheet details");
		return trailbalanceDao.getMapSheetDetails();
	}

	@Override
	public List<Integer> saveTrailbalanceMapping(TrailBalance trailbalance) {
		LOGGER.debug("save trail balance mapping");
		return trailbalanceDao.saveTrailbalanceMapping(trailbalance);
	}

	@Override
	public boolean saveFsDetails(FsDetails fsDetails) {
		LOGGER.debug("save fs detais");
		return trailbalanceDao.saveFsDetails(fsDetails);
	}
}
