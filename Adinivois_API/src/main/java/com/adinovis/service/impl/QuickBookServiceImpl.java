package com.adinovis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.configuration.quickbook.QuickBookGateway;
import com.adinovis.dao.ClientDao;
import com.adinovis.domain.ClientData;
import com.adinovis.service.QuickBookServiceIfc;
import com.intuit.oauth2.data.BearerTokenResponse;

@Service
public class QuickBookServiceImpl implements QuickBookServiceIfc {

	@Autowired
	private QuickBookGateway quickBooksGateway;
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickBookServiceImpl.class);
	private Boolean contextRelative = true;
	private Boolean http10Compatible = true;
	private Boolean exposeModelAttributes = false;

	@Autowired
	protected ClientDao clientDao;

	public View connect(String tokenValue) {
		LOGGER.debug("Quickbook client connect");
		return new RedirectView(quickBooksGateway.connect(tokenValue), contextRelative, http10Compatible,
				exposeModelAttributes);

	}

	public boolean callBackFromOAuth(String code, String tokenValue, String realmId) {

		tokenValue = tokenValue.replaceAll("\\s","+");
		try {
			LOGGER.debug("Quickbook client callback");
			BearerTokenResponse bearerTokenResponse = quickBooksGateway.getToken(code);
			if (bearerTokenResponse == null) {
				return false;
			}
			LOGGER.debug("tokenValue " + tokenValue);
			LOGGER.debug("RealmId " + realmId);
			LOGGER.debug("Access Token " + bearerTokenResponse.getAccessToken());
			LOGGER.debug("Refresh Token " + bearerTokenResponse.getRefreshToken());
			ClientData clientData = new ClientData();
			clientData.setSourceLink(CloudunionConstants.SOURCELINK_QUICKBOOK);
			clientData.setRealmId(realmId);
			clientData.setAccessToken(bearerTokenResponse.getAccessToken());
			clientData.setRefreshToken(bearerTokenResponse.getRefreshToken());
			if (clientDao.saveClientDataSource(tokenValue, clientData)) {
				clientDao.clientinvitevalidation(tokenValue, 5);
				return true;
			}
		} catch (Exception e) {
			LOGGER.debug("Quickbook client Connection failed", e);
		}
		clientDao.clientinvitevalidation(tokenValue, 6);
		return false;
		/*
		 * CompanyInfo companyInfo = quickBooksGateway.getCompanyInfo(realmId,
		 * bearerTokenResponse.getAccessToken()); System.out.println();
		 * System.out.println(); String company = new Gson().toJson(companyInfo);
		 * System.out.println("CompanyInfo:" + company); System.out.println(); Report
		 * report = quickBooksGateway.getTrialBalanceReport(realmId,
		 * bearerTokenResponse.getAccessToken(), "2018-05-1", "2018-05-12");
		 */

		/*
		 * List<Row> rows =
		 * report.getRows().getRow().stream().collect(Collectors.toList());
		 * 
		 * ReportHeader header = report.getHeader(); System.out.println();
		 * System.out.println("Headers:" + header); System.out.println();
		 * System.out.println("Columns" + cols.toString()); System.out.println();
		 * System.out.println("Rows:" + rows.toString());
		 * 
		 * List<Column> cols =
		 * report.getColumns().getColumn().stream().collect(Collectors.toList());
		 */

		/*
		 * String json = new Gson().toJson(report); System.out.println();
		 * System.out.println(); System.out.println("TrialBalanceReport" + json);
		 * System.out.println(); System.out.println();
		 */

	}

}
