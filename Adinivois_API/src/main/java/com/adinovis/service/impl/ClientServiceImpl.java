package com.adinovis.service.impl;

import java.math.BigInteger;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.adinovis.dao.ClientDao;
import com.adinovis.domain.AllAuditor;
import com.adinovis.domain.Auditorrole;
import com.adinovis.domain.Businessdetails;
import com.adinovis.domain.ClientData;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.Clientcountdetails;
import com.adinovis.domain.Clientdetails;
import com.adinovis.domain.Clientprofiles;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.GetUserClientId;
import com.adinovis.domain.Jurisdiction;
import com.adinovis.domain.Typeofentity;
import com.adinovis.service.ClientService;

public class ClientServiceImpl implements ClientService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);
	
	 @Autowired
	    private ClientDao clientDao;
	   
	 public ClientDao getClientDao() {
		return clientDao;
	}
	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}
	public boolean saveclientprofile(ClientProfile clientProfile) {
         LOGGER.debug("saveclientprofile service begin");
         return clientDao.saveclientprofile(clientProfile);
     }
	 public boolean updateclientprofile(ClientProfile clientProfile) {
         LOGGER.debug("updateclientprofile service begin");
         return clientDao.updateclientprofile(clientProfile);
     }
	 public List<Clientprofiles> getclientprofile(String loginid) {
			LOGGER.debug("getclientprofile ");
			return clientDao.getclientprofile(loginid);
		}
	 public List<Clientstatus> getclientstatus(){
			LOGGER.debug("getclientstatus ");
			return clientDao.getclientstatus();
		}
		
		public String clientvalidationemail(String emailid) {
	        LOGGER.debug("clientvalidationemail service begin");
	        return clientDao.clientvalidationemail(emailid);
	    }
	 
	 public List<Jurisdiction> getjurisdiction(){
			LOGGER.debug("getjurisdiction ");
			return clientDao.getjurisdiction();
		}
		
		public List<Typeofentity> gettypeofentity(){
			LOGGER.debug("gettypeofentity ");
			return clientDao.gettypeofentity();
		}
		public boolean clientinvitevalidation(String tokenkey,int statusid) {
	        LOGGER.debug("clientinvitevalidation service begin");
	        return clientDao.clientinvitevalidation(tokenkey,statusid);
	    }
		
		public List<Businessdetails> getbusinessdetails(String loginid){
			LOGGER.debug("getbusinessdetails ");
			return clientDao.getbusinessdetails(loginid);
		}
		
		public List<Clientdetails> getclientdetail(String token){
			LOGGER.debug("getbusinessdetails ");
			return clientDao.getclientdetail(token);
		}
		
		public List<GetUserClientId> getclientid(String emailid,String password) {
			LOGGER.debug("getclientid ");
			return clientDao.getclientid(emailid,password);
		}
		 public boolean tokensave(BigInteger accountid,String token) {
	         LOGGER.debug("tokensave service begin");
	         return clientDao.tokensave(accountid,token);
	     }
		
		@Override
		public boolean deleteclientprofile(String clientfirmid,String loginid) {
			LOGGER.debug("deleteclientprofile ");
			return clientDao.deleteclientprofile(clientfirmid,loginid);
		}
		public List<Clientcountdetails> getclientcountdetails(String loginid){
			LOGGER.debug("getclientcountdetails ");
			return clientDao.getclientcountdetails(loginid);
		}
		
		public List<Auditorrole> getauditorrole(){
			LOGGER.debug("getauditorrole ");
			return clientDao.getauditorrole();
		}
		
		public List<AllAuditor> getallauditor(){
			LOGGER.debug("getallauditor ");
			return clientDao.getallauditor();
		}
		
		public String clientvalidation(String emailid) {
	        LOGGER.debug("clientvalidation service begin");
	        return clientDao.clientvalidation(emailid);
	    }
		
		@Override
		public ClientData getClientDataSourceByTokenValue(String tokenValue) {
			LOGGER.debug("getClientDataSourceByTokenValue ");
			return clientDao.getClientDataSourceByTokenValue(tokenValue);
		}


}
