/** 
 * Program Name: CloudunionServiceImpl                                                            * 
 *                                                                                                *                                                                    
 * Program Description / functionality:  
 *                                                                                                *                               
 * Modules Impacted:                                                                              *                                                                  
 *                                                                                                *
 * Tables affected: *
 *                                                                                                *       
 * Developer  Created/Modified Date      Purpose                                                  *
 *******************************************************************************                  *             Implemented code changes as per review comments          *
 * Gouri                                                                                          *
 * Associated Defects Raised :                                                                    *
 *                                                                                                *
 */
package com.adinovis.service.impl;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.adinovis.dao.CinchDao;
import com.adinovis.domain.CuserProfile;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;
import com.adinovis.service.CinchService;

public class CloudunionServiceImpl implements CinchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CloudunionServiceImpl.class);
    @Autowired
    private CinchDao cinchDao;
    
 
     public CinchDao getCinchDao() {
		return cinchDao;
	}
	public void setCinchDao(CinchDao cinchDao) {
		this.cinchDao = cinchDao;
	}

     
     public boolean signup(SignupDetails signupDetails) {
         LOGGER.debug("signup service begin");
         return cinchDao.signup(signupDetails);
     }
     
     public boolean tokensave(BigInteger accountid,String token) {
         LOGGER.debug("tokensave service begin");
         return cinchDao.tokensave(accountid,token);
     }
     
     public boolean updateuserprofile(Userprofileupdate userprofileupdate) {
         LOGGER.debug("updateuserprofile service begin");
         return cinchDao.updateuserprofile(userprofileupdate);
     }
     
	public List<CuserProfile> login(String emailid,String encpassword) {
			LOGGER.debug("getuserprofile ");
			return cinchDao.login(emailid,encpassword);
		}
	
	public List<CuserProfile> getuserprofileupdate(BigInteger accountid) {
		LOGGER.debug("getuserprofileupdate ");
		return cinchDao.getuserprofileupdate(accountid);
	}
	
	public String getemailid(String emailid) {
		LOGGER.debug("getemailid ");
		return cinchDao.getemailid(emailid);
	}
	
	public BigInteger getuserid(String emailid) {
		LOGGER.debug("getuserid ");
		return cinchDao.getuserid(emailid);
	}
	
	public String forgotPassword(String emailid) {
        LOGGER.debug("forgotPassword service begin");
        return cinchDao.forgotPassword(emailid);
    }
	
	public String signupvalidation(String emailid) {
        LOGGER.debug("signupvalidation service begin");
        return cinchDao.signupvalidation(emailid);
    }

	public boolean changepassword(Integer useraccountid,String oldpassword,String newpassword) {
        LOGGER.debug("changepassword service begin");
        return cinchDao.changepassword(useraccountid,oldpassword,newpassword);
    }
	
	public boolean forgotPasswordstatuschange(String emailid) {
        LOGGER.debug("forgotPasswordstatuschange service begin");
        return cinchDao.forgotPasswordstatuschange(emailid);
    }
	
	public boolean resetpassword(String tokenkey,String newpassword) {
        LOGGER.debug("resetpassword service begin");
        return cinchDao.resetpassword(tokenkey,newpassword);
    }
	
	public boolean loginvalidation(String tokenkey) {
        LOGGER.debug("loginvalidation service begin");
        return cinchDao.loginvalidation(tokenkey);
    }
	@Override
	public String getUserVerification(String emailid) {
		LOGGER.debug("userverification service begin");
		return cinchDao.getUserVerification(emailid);
	}
	
		
}

