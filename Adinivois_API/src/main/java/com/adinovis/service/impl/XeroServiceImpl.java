package com.adinovis.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.configuration.xero.XeroConnectionDto;
import com.adinovis.configuration.xero.XeroGateway;
import com.adinovis.dao.ClientDao;
import com.adinovis.domain.ClientData;
import com.adinovis.service.XeroServiceIfc;
import com.google.gson.Gson;
import com.xero.api.Config;
import com.xero.api.OAuthAccessToken;
import com.xero.api.OAuthAuthorizeToken;
import com.xero.api.OAuthRequestToken;
import com.xero.models.accounting.Organisations;
import com.xero.models.accounting.ReportWithRows;

@Service
public class XeroServiceImpl implements XeroServiceIfc {

	private static final Logger LOGGER = LoggerFactory.getLogger(XeroServiceImpl.class);
	private Map<String, XeroConnectionDto> storage = new HashMap<String, XeroConnectionDto>();

	@Autowired
	private Config config;

	@Autowired
	private OAuthAccessToken accessToken;
	
	 @Autowired
    private OAuthRequestToken requestToken;

	@Autowired
	XeroGateway xeroGateway;
	
	@Autowired
	private ClientDao clientDao;

	public String connect(String tokenValue) {

		try {
			requestToken.execute();
			String tempToken = requestToken.getTempToken();
			storage.put(tempToken, new XeroConnectionDto(requestToken, tokenValue));
			OAuthAuthorizeToken authorizeToken = new OAuthAuthorizeToken(config, tempToken);

			return authorizeToken.getAuthUrl();
		} catch (IOException e) {
			LOGGER.error("unable to connect to xero", e);

		}
		return null;
	}

	public boolean callBackFromOAuth(String verifier, String tempToken) {
		XeroConnectionDto storageDto = storage.get(tempToken);
		String tokenValue = storageDto.getTokenValue();
		tokenValue = tokenValue.replaceAll("\\s","+");
		try {
			accessToken.build(verifier, storageDto.getAuthRequestToken().getTempToken(),
					storageDto.getAuthRequestToken().getTempTokenSecret()).execute();
			storage.remove(tempToken);
			LOGGER.info("Token Secret : " + accessToken.getTokenSecret());
			LOGGER.info("Token : " + accessToken.getToken());
			LOGGER.info("Session Handle : " + accessToken.getSessionHandle());

			if (!accessToken.isSuccess()) {
				LOGGER.info("Failed to get Access Tokens");
				clientDao.clientinvitevalidation(tokenValue, 6);
				return false;
			}

			ClientData clientData = new ClientData();
			clientData.setSourceLink(CloudunionConstants.SOURCELINK_XERO);
			clientData.setRealmId(accessToken.getSessionHandle());
			clientData.setAccessToken(accessToken.getToken());
			clientData.setRefreshToken(accessToken.getTokenSecret());
			if (clientDao.saveClientDataSource(tokenValue, clientData)) {
				clientDao.clientinvitevalidation(tokenValue, 5);
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		clientDao.clientinvitevalidation(tokenValue, 6);
		return false;

	}

	/*
	 * public List<ClientDetails> getXeroClientData() { // TODO Auto-generated
	 * method stub return null; }
	 */

}
