package com.adinovis.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.adinovis.dao.EngagementDao;
import com.adinovis.domain.Allengagement;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.Engagement;
import com.adinovis.domain.EngagementYear;
import com.adinovis.domain.Engagementcountdetail;
import com.adinovis.domain.Engagementdetails;
import com.adinovis.domain.Engagementtype;
import com.adinovis.service.EngagementService;

public class EngagementServiceImpl implements EngagementService{
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudunionServiceImpl.class);
    @Autowired
    private EngagementDao engagementDao;
	public EngagementDao getEngagementDao() {
		return engagementDao;
	}

	public void setEngagementDao(EngagementDao engagementDao) {
		this.engagementDao = engagementDao;
	}

	@Override
	public boolean updateengagementdetails(Engagement engagement) {
		LOGGER.debug("updateengagementdetails ");
		return engagementDao.updateengagementdetails(engagement);
	}
	@Override
	public boolean deleteengagement(String engagementsid,String loginid) {
		LOGGER.debug("deleteengagement ");
		return engagementDao.deleteengagement(engagementsid,loginid);
	}
	
	
	public List<Engagementdetails> getengagementdetails(int clientfirmid,String loginid){
		LOGGER.debug("getengagementdetails ");
		return engagementDao.getengagementdetails(clientfirmid,loginid);
	}
	
	public List<Allengagement> getallengagement(String loginid){
		LOGGER.debug("getallengagement ");
		return engagementDao.getallengagement(loginid);
	}
	public List<Clientstatus> getengagementstatus(){
		LOGGER.debug("getengagementstatus ");
		return engagementDao.getengagementstatus();
	}
	
	public List<Engagementtype> getengagement(){
		LOGGER.debug("getengagement ");
		return engagementDao.getengagement();
	}
	 public boolean saveengagement(Engagement engagement) {
         LOGGER.debug("saveengagement service begin");
         return engagementDao.saveengagement(engagement);
     }
	 
	 public List<Engagementcountdetail> getengagementcountdetail(String loginid){
			LOGGER.debug("getengagementcountdetail ");
			return engagementDao.getengagementcountdetail(loginid);
		}

	@Override
	public List<EngagementYear> getEngagementYearDetails(int inengagementsid) {
		LOGGER.debug("getengagementyeardetails ");
		return engagementDao.getEngagementYearDetails(inengagementsid);
	}

	@Override
	public List<Integer> getEngagementId(int clientfirmid) {
		LOGGER.debug("get engagement id ");
		return engagementDao.getEngagementId(clientfirmid);
	}
		
}
