package com.adinovis.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.adinovis.domain.AmazonS3Details;
import com.adinovis.amazon.AmazonS3Gateway;
import com.adinovis.service.AmazonS3Service;

@Service
public class AmazonS3ServiceImpl implements AmazonS3Service {
	private static final Logger LOG = LoggerFactory.getLogger(AmazonS3ServiceImpl.class);
	@Autowired
	private AmazonS3Gateway amazonS3Gateway;
	private static String fileSeparator = System.getProperty("file.separator");

	public boolean saveFileInS3(Integer status,String signatureKey, MultipartFile file, HttpSession session) {
		LOG.info("save file in S3 Service");
		String path = session.getServletContext().getRealPath("/");
		String filename = file.getOriginalFilename();
		System.out.println(path + "" + filename);
		LOG.info("Path:" + path + "" + filename);
		//LOG.info("BucketName:" + bucketname);
		File tempFolder = new File(path + "temp");
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		if (!file.getOriginalFilename().isEmpty()) {
			try {
				File newfile = new File(tempFolder + fileSeparator + filename);
				// if(newfile.exists())
				// {
				if (newfile.exists()) {
					LOG.info("File exists");
					newfile.delete();
				}

				byte barr[] = file.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(newfile));
				bout.write(barr);
				bout.flush();
				bout.close();
				/*boolean bucketStatus = amazonS3Gateway.findS3Bucket(bucketname);
				if (!bucketStatus == true) {
					LOG.info("Bucket not exist");
					newfile.delete();
					return false;
				} else {*/
					boolean fileuploaded = amazonS3Gateway.saveS3Object(status,signatureKey, newfile);
					newfile.delete();
					if (fileuploaded) {
						LOG.info("File uploaded in bucket");

						return true;
					}
					LOG.info("File not uploaded in bucket");
					return false;
				//}

				// }
			} catch (Exception e) {
				System.out.println(e);
			}
		}

		return false;
	}

	@Override
	public boolean updateFileInS3(AmazonS3Details amazonS3Details) {
		boolean fileNameModified = amazonS3Gateway.renameS3Object(amazonS3Details.getStatus(),
				amazonS3Details.getSignatureKey(), amazonS3Details.getDestinationKey());
		if (fileNameModified) {
			LOG.info("File modified in bucket");
			return true;
		}
		LOG.info("File not modified in bucket");
		return false;
	}

	@Override
	public boolean deleteFileInS3(AmazonS3Details amazonS3Details) {
		LOG.info("delete file in S3 Service");

		boolean fileDeleted = amazonS3Gateway.deleteS3Object(amazonS3Details.getStatus(),
				amazonS3Details.getSignatureKey());
		if (fileDeleted) {
			LOG.info("File deleted in bucket");
			return true;

		}
		LOG.info("File not deleted in bucket");
		return false;

	}

	@Override
	public boolean createFolderInS3(AmazonS3Details amazonS3Details) {
		LOG.info("create folder in S3 Service");

		boolean folderCreated = amazonS3Gateway.createFolder(amazonS3Details.getStatus(),
				amazonS3Details.getFoldername());
		if (folderCreated) {
			LOG.info("Folder created in bucket");
			return true;
		}
		LOG.info("Folder not created in bucket");
		return false;
	}

	@Override
	public boolean getFile(AmazonS3Details amazonS3Details) {
		LOG.info("get single file from folder S3 Service");

		byte[] fileExist = amazonS3Gateway.getS3Object(amazonS3Details.getStatus(), amazonS3Details.getFileName());
		if (fileExist.length > 0) {
			LOG.info("File exist in bucket");
			return true;
		}
		LOG.info("File not exist in bucket");
		return false;
	}

	@Override
	public List<String> getAllFiles(AmazonS3Details amazonS3Details) {
		LOG.info("get all files from given folder S3 Service");

		List<String> listOfFiles = null;
		// if (bucketStatus == true) {
		listOfFiles = amazonS3Gateway.getObjectslistFromFolder(amazonS3Details.getStatus(),
				amazonS3Details.getFoldername());
		return listOfFiles;
	}

	@Override
	public boolean deleteMultipleFiles(String bucketname, String folderName, MultipartFile[] files,
			HttpSession session) {
		/*
		 * LOG.info("save file in S3 Service"); String path =
		 * session.getServletContext().getRealPath("/"); LOG.info("BucketName:" +
		 * bucketname); boolean bucketStatus = amazonS3Gateway.findS3Bucket(bucketname);
		 * if (!bucketStatus == true) { LOG.info("Bucket not exist"); return false; }
		 * else { for (int i = 0; i < files.length; i++) { MultipartFile file =
		 * files[i];
		 * 
		 * try { String filename = file.getOriginalFilename();
		 * 
		 * File tempFolder = new File(path + "temp"); if (!tempFolder.exists()) {
		 * tempFolder.mkdir(); } if (!file.getOriginalFilename().isEmpty()) {
		 * 
		 * File newfile = new File(tempFolder + fileSeparator + filename);
		 * 
		 * if (newfile.exists()) { LOG.info("File exists"); newfile.delete(); }
		 * 
		 * byte barr[] = file.getBytes(); BufferedOutputStream bout = new
		 * BufferedOutputStream(new FileOutputStream(newfile)); bout.write(barr);
		 * bout.flush(); bout.close();
		 * 
		 * boolean filedeleted= amazonS3Gateway.deleteMultipleObjects(bucketname,
		 * folderName, filename); newfile.delete(); if (filedeleted) {
		 * LOG.info("File deleted in bucket");
		 * 
		 * return true; } LOG.info("File not deleted in bucket"); return false;
		 * 
		 * } // } } catch (Exception e) { System.out.println(e); } }
		 */
		return false;
	}
	//}

	@Override
	public boolean deleteFolder(AmazonS3Details amazonS3Details) {
		LOG.info("delete folder in S3 Service");
		boolean deletefolder = amazonS3Gateway.deleteFolder(amazonS3Details.getStatus(),
				amazonS3Details.getFoldername());
		if (deletefolder) {
			LOG.info("Folder deleted in bucket");
			return true;
		}
		LOG.info("Folder not deleted in bucket");
		return false;
	}

	@Override
	public boolean moveMultipleFiles(AmazonS3Details amazonS3Details) {
		boolean filesMoved = amazonS3Gateway.moveMultipleObjects(amazonS3Details.getStatus(),
				amazonS3Details.getSourceFolder(), amazonS3Details.getDestinationFolder(),
				amazonS3Details.getFilenames());
		if (filesMoved) {
			LOG.info("Files moved in bucket");
			return true;
		}
		LOG.info("Files not moved in bucket");
		return false;
	}

	@Override
	public boolean copyFolder(AmazonS3Details amazonS3Details) {
		boolean folderMoved = amazonS3Gateway.copyFolder(amazonS3Details.getStatus(), amazonS3Details.getSourceFolder(),
				amazonS3Details.getDestinationFolder());
		if (folderMoved) {
			LOG.info("Folder moved in bucket");
			return true;
		}
		LOG.info("Folder not moved in bucket");
		return false;
		
	}

	@Override
	public boolean deleteFolderWithFiles(AmazonS3Details amazonS3Details) {
		LOG.info("delete folder with files in S3 Service");
		boolean deletefolder = amazonS3Gateway.deleteFolderWithFiles(amazonS3Details.getStatus(),
				amazonS3Details.getFoldername());
		if (deletefolder) {
			LOG.info("Folder deleted in bucket");
			return true;
		}
		LOG.info("Folder not deleted in bucket");
		return false;
	}
}
