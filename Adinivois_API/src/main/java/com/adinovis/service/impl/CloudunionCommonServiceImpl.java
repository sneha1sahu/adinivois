package com.adinovis.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.adinovis.dao.CloudunionCommonDao;
import com.adinovis.domain.Login;
import com.adinovis.domain.LoginwithSocial;
import com.adinovis.domain.Social;
import com.adinovis.domain.UserBean;
import com.adinovis.service.CloudunionCommonService;


public class CloudunionCommonServiceImpl implements CloudunionCommonService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudunionCommonServiceImpl.class);
	@Autowired
	private CloudunionCommonDao cloudunionCommonDao;

	

	public CloudunionCommonDao getCloudunionCommonDao() {
		return cloudunionCommonDao;
	}

	public void setCloudunionCommonDao(CloudunionCommonDao cloudunionCommonDao) {
		this.cloudunionCommonDao = cloudunionCommonDao;
	}

	@Override
	public List<Login> getloginId(String userid) {
		LOGGER.debug("getloginId ");
		return cloudunionCommonDao.getloginId(userid);
	}

	@Override
	public List<Login> getloginthroughsocial(String loginId, String password) {
		LOGGER.debug("getloginthroughsocial");
		return cloudunionCommonDao.getloginthroughsocial(loginId, password);
	}

	@Override
	public Integer updatelogin(LoginwithSocial loginid) {
		LOGGER.debug("updatelogin");
		return cloudunionCommonDao.updatelogin(loginid);
	}

	@Override
	public List<Social> getloginsocial(String loginId) {
		LOGGER.debug("getloginsocial");
		return cloudunionCommonDao.getloginsocial(loginId);
	}
	
	@Override
	public int updateUserSession(String uId, String usersession) {
		LOGGER.debug("updateUserSession");
		return cloudunionCommonDao.updateUserSession(uId, usersession);
	}

	@Override
	public List<UserBean> getLoginBean(String loginId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getAcountId(String uId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String userSession(String u_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserType(String uId) {
		// TODO Auto-generated method stub
		return null;
	}


}
