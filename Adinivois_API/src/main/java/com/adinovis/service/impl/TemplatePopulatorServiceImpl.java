package com.adinovis.service.impl;
/*package com.cinch.service.impl;


import com.cinch.domain.MailImages;
import com.cinch.service.TemplatePopulatorService;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Service
public class TemplatePopulatorServiceImpl implements TemplatePopulatorService {

    @Qualifier("templateEngine")
    @Autowired
    private TemplateEngine templateEngine;

    private static final String IMAGE_TYPE = "image/png";

    @Override
    public List<MailImages> getMailImages(String path, List<String> images) {
        List<MailImages> mailImages = new ArrayList<>();
        if (images == null) {
            return mailImages;
        }

       // images.forEach(imageString -> mailImages.add(getMailImage(path, imageString)));

        return mailImages;
    }

    @Override
    public String getHtmlForEmail(String templateName, Map<String, String> keyValueMap) {
        Context ctx = new Context(Locale.ENGLISH);

        keyValueMap.keySet().forEach(key -> ctx.setVariable(key, keyValueMap.get(key)));
        return templateEngine.process(templateName, ctx);
    }

    private MailImages getMailImage(String path, String image) {
        try {
            return MailImages.builder()
                    .name(image)
                    .content(IOUtils.toByteArray(new ClassPathResource(path+image).getInputStream()))
                    .type(IMAGE_TYPE)
                    .build();
        } catch (IOException e) {
            //log.error("Error Details: ",e);
            return new MailImages();
        }
    }

}
*/