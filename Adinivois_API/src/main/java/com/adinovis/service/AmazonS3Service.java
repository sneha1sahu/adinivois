package com.adinovis.service;

import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.web.multipart.MultipartFile;

import com.adinovis.domain.AmazonS3Details;

public interface AmazonS3Service
{
	boolean saveFileInS3(Integer status,String signatureKey,MultipartFile file,HttpSession session);
	
	boolean updateFileInS3(AmazonS3Details amazonS3Details);
	
	boolean deleteFileInS3(AmazonS3Details amazonS3Details);
	
	boolean createFolderInS3(AmazonS3Details amazonS3Details);
	
	boolean getFile(AmazonS3Details amazonS3Details);
	
	List<String> getAllFiles(AmazonS3Details amazonS3Details);
	
	boolean deleteMultipleFiles(String bucketname,String folderName,MultipartFile[] files, HttpSession session);
	
	boolean deleteFolder(AmazonS3Details amazonS3Details);
	
	boolean moveMultipleFiles(AmazonS3Details amazonS3Details);
	
	boolean copyFolder(AmazonS3Details amazonS3Details);
	
	boolean deleteFolderWithFiles(AmazonS3Details amazonS3Details);
	
}
