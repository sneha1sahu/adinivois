/** 
 * Program Name: CloudunionService                                                                * 
 *                                                                                                *                                                                    
 * Program Description / functionality:  
 *                                                                                                *                               
 * Modules Impacted:                                                                              *                                                                  
 *                                                                                                *
 * Tables affected:                                                                               *
 *                                                                                                *       
 * Developer  Created/Modified Date      Purpose                                                  *
 *******************************************************************************                  *             Implemented code changes as per review comments          *
 * Gouri                                                                                              *
 * Associated Defects Raised :                                                                    *
 *                                                                                                *
 */
package com.adinovis.service;

import java.math.BigInteger;
import java.util.List;

import com.adinovis.domain.CuserProfile;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;

public interface CinchService {

	
	boolean signup(SignupDetails signupDetails);
	
	boolean tokensave(BigInteger accountid,String token);
    
	boolean updateuserprofile(Userprofileupdate userprofileupdate);
	
	List<CuserProfile> login(String emailid,String encpassword);
	
	List<CuserProfile> getuserprofileupdate(BigInteger accountid);
	
	String getemailid (String emailid);
	
	BigInteger getuserid(String emailid);
	
	String forgotPassword(String emailid);
	
	boolean forgotPasswordstatuschange(String emailid);
	
	boolean loginvalidation(String tokenkey);
	
	String signupvalidation(String emailid);
	
	boolean changepassword(Integer useraccountid,String oldpassword,String newpassword);
	
	boolean resetpassword(String tokenkey,String newpassword);
	
	String getUserVerification(String emailid);
	
}
