package com.adinovis.service;

public interface XeroServiceIfc {
	// List<ClientDetails> getXeroClientData();
	String connect(String tokenValue);

	boolean callBackFromOAuth(String verifier, String tempToken);
}
