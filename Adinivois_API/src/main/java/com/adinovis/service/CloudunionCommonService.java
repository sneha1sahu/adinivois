package com.adinovis.service;

import java.util.List;

import com.adinovis.domain.Login;
import com.adinovis.domain.LoginwithSocial;
import com.adinovis.domain.Social;
import com.adinovis.domain.UserBean;

public interface CloudunionCommonService {
	List<Login> getloginId(String userid);

	List<Login> getloginthroughsocial(String loginId, String password);

	Integer updatelogin(LoginwithSocial loginid);

	int updateUserSession(String uId, String usersession);
	
	List<Social> getloginsocial(String loginId);
	
	List<UserBean> getLoginBean(String loginId);
	
	int getAcountId(String uId);
	
	String userSession(String u_id);
	
	String getUserType(String uId);

}
