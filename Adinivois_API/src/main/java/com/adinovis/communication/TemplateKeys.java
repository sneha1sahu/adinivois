package com.adinovis.communication;

public enum TemplateKeys {

    FIRST_NAME_KEY("firstName"),
    COMPANY_NAME("companyName"),
    CUSTOMER_LEGAL_NAME("customerLegalName"),
    VERIFY_LINK_KEY("verifyLink"),
    FORGOT_LOGIN_CREDENTIAL_KEY("forgotPasswordLink"),
    AUDITOR_FIRST_NAME("auditorFirstName"),
    MESSAGE_KEY("message"),
    AR_CONFIRMATION_LINK_KEY("arConfirmationLink"),
    NOTIFICATION_KEY("notification"),
    BUTTON_KEY("button"),

    AUDITOR_NAME("AUDITOR_NAME"),
    AUDITOR_EMAIL("AUDITOR_EMAIL"),
    AUDITOR_ADDRESS("AUDITOR_ADDRESS"),
    DEBTOR_NAME("DEBTOR_NAME"),
    CONTACT_PERSON("CONTACT_PERSON"),
    CONTACT_PERSON_DATE("CONTACT_PERSON_DATE"),
    CONTACT_PERSON_ADDRESS("CONTACT_PERSON_ADDRESS"),
    CONFIRMATION_AMOUNT("CONFIRMATION_AMOUNT"),
    CONFIRMATION_DATE("CONFIRMATION_DATE"),
    CLIENT_COMPANY_NAME("CLIENT_COMPANY_NAME"),
    HEADER_IMG("HEADER_IMG"),
    FOOTER_IMG("FOOTER_IMG"),
    CLIENT_SIGNATURE_IMG("CLIENT_SIGNATURE_IMG"),
    CONFIRMATION_RECEIVED_DATE("CONFIRMATION_RECEIVED_DATE"),
    CUSTOMER_SIGNATURE_IMG("CUSTOMER_SIGNATURE_IMG");

    private String value;

    TemplateKeys(String value) {
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }
}
