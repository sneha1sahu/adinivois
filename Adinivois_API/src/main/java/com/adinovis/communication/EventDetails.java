package com.adinovis.communication;

public interface EventDetails {

    String getSubject();

    String getTemplateName();

}
