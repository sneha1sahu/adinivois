package com.adinovis.communication;

public enum EventType implements EventDetails {

    AUDITOR_VERIFY_MAIL("Verify your email on CinchDocs", "verifyAccountEmail"),
    FORGET_PASSWORD_MAIL("Reset password link on CinchDocs", "forgetPasswordEmail"),
    WELCOME_MAIL("Welcome to CinchDocs", "welcomeEmail"),
    INVITE_CLIENT_MAIL("Invitation to join CinchDocs", "inviteClientEmail"),
    AUDITOR_ENGAGEMENT_INITIATION_EMAIL("Auditor Engagement Initiated", "notificationEmailWithLink"),
    CLIENT_ADDED_ACCOUNTING_SOFTWARE_EMAIL("Accounting Software Registered", "notificationEmailWithLink"),
    CLIENT_CONFIRMATION_LETTER_REVIEW_EMAIL("Review Confirmations", "notificationEmailWithLink"),
    CLIENT_ADDED_SIGNATURE_EMAIL("eSignature Registered", "notificationEmail"),
    CLIENT_ADDED_LETTERHEAD_HEADER_EMAIL("Letterhead Header Registered", "notificationEmail"),
    CLIENT_ADDED_LETTERHEAD_FOOTER_EMAIL("Letterhead Footer Registered", "notificationEmail"),
    CONFIRMATION_LETTER_AGREE_RESPONSE_EMAIL("Confirmation Agreement Note", "notificationEmailWithLink"),
    CONFIRMATION_LETTER_DISAGREE_RESPONSE_EMAIL("Confirmation Disagreement Note", "notificationEmailWithLink"),
    ACCESS_AR_TRACKER_EMAIL("Confirmation Tracker Access", "notificationEmailWithLink"),
    AUDITOR_CONFIRMATION_LETTER_REVIEW_EMAIL("Review Confirmations", "notificationEmailWithLink"),
    COMPLETED_AUDIT_FOR_AR_EMAIL("Audit Engagement Completed", "notificationEmailWithLink"),
    EMAIL_TO_CUSTOMER_FOR_CONFIRMATION("Confirmation letter approval request", "customerEmailForApproval"),
    REVOKE_AR_TRACKER_EMAIL("Confirmation Tracker Access", "notificationEmail");

    private String subject;
    private String templateName;

    EventType(String subject, String templateName) {
        this.subject = subject;
        this.templateName = templateName;
    }

    @Override
    public String getSubject() {
        return this.subject;
    }

    @Override
    public String getTemplateName() {
        return this.templateName;
    }
}
