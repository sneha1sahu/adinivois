package com.adinovis.configuration.quickbook;

import java.math.BigDecimal;
import java.time.LocalDate;
import org.springframework.stereotype.Component;
@Component
public class TrialBalanceDto {
	 private LocalDate toDate;
	    private LocalDate fromDate;
	    private long id;
	    private Long companyID;
	    private Long accountID;
	    private Integer mapping;
	    private BigDecimal debit;
	    private BigDecimal credit;
	    private Integer accountingYear;
	    private String accountDescription;
	    public TrialBalanceDto()
	    {
	    	
	    }
		public TrialBalanceDto(LocalDate toDate, LocalDate fromDate, long id, Long companyID, Long accountID,
				Integer mapping, BigDecimal debit, BigDecimal credit, Integer accountingYear,
				String accountDescription) {
			super();
			this.toDate = toDate;
			this.fromDate = fromDate;
			this.id = id;
			this.companyID = companyID;
			this.accountID = accountID;
			this.mapping = mapping;
			this.debit = debit;
			this.credit = credit;
			this.accountingYear = accountingYear;
			this.accountDescription = accountDescription;
		}
		public LocalDate getToDate() {
			return toDate;
		}
		public void setToDate(LocalDate toDate) {
			this.toDate = toDate;
		}
		public LocalDate getFromDate() {
			return fromDate;
		}
		public void setFromDate(LocalDate fromDate) {
			this.fromDate = fromDate;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public Long getCompanyID() {
			return companyID;
		}
		public void setCompanyID(Long companyID) {
			this.companyID = companyID;
		}
		public Long getAccountID() {
			return accountID;
		}
		public void setAccountID(Long accountID) {
			this.accountID = accountID;
		}
		public Integer getMapping() {
			return mapping;
		}
		public void setMapping(Integer mapping) {
			this.mapping = mapping;
		}
		public BigDecimal getDebit() {
			return debit;
		}
		public void setDebit(BigDecimal debit) {
			this.debit = debit;
		}
		public BigDecimal getCredit() {
			return credit;
		}
		public void setCredit(BigDecimal credit) {
			this.credit = credit;
		}
		public Integer getAccountingYear() {
			return accountingYear;
		}
		public void setAccountingYear(Integer accountingYear) {
			this.accountingYear = accountingYear;
		}
		public String getAccountDescription() {
			return accountDescription;
		}
		public void setAccountDescription(String accountDescription) {
			this.accountDescription = accountDescription;
		}
		@Override
		public String toString() {
			return "TrialBalanceDto [toDate=" + toDate + ", fromDate=" + fromDate + ", id=" + id + ", companyID="
					+ companyID + ", accountID=" + accountID + ", mapping=" + mapping + ", debit=" + debit + ", credit="
					+ credit + ", accountingYear=" + accountingYear + ", accountDescription=" + accountDescription
					+ "]";
		}
		
	    
}
