package com.adinovis.configuration.quickbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.config.Environment;
import com.intuit.oauth2.config.OAuth2Config;
import com.intuit.oauth2.config.Scope;

@Configuration
@PropertySource("classpath:application.properties")
public class QuickBookConfiguration {

	@Autowired
	org.springframework.core.env.Environment environment;

	@Bean(name = "redirectUri")
	public String redirectUri() {
		return environment.getProperty("Quickbooks.OAuth2AppRedirectUri");
	}

	@Bean(name = "baseURL")
	public String baserUrl() {
		return environment.getProperty("IntuitAccountingAPIHost");
	}

	@Bean(name = "scopes")
	public Scope initScopes() {
		return Scope.fromValue(environment.getProperty("Quickbooks.Scope"));
	}

	@Bean
	public OAuth2Config initOAuth2Config() {
		return new OAuth2Config.OAuth2ConfigBuilder(environment.getProperty("Quickbooks.OAuth2AppClientId"), environment.getProperty("Quickbooks.OAuth2AppClientSecret"))
				.callDiscoveryAPI(Environment.fromValue(environment.getProperty("Quickbooks.Environment")))
				.buildConfig();
	}

	@Bean
	public OAuth2PlatformClient initOAuth2PlatformClient() {
		return new OAuth2PlatformClient(initOAuth2Config());
	}
}
