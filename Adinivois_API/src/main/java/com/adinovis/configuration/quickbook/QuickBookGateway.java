package com.adinovis.configuration.quickbook;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.data.CompanyInfo;
import com.intuit.ipp.data.Report;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.intuit.ipp.services.ReportService;
import com.intuit.ipp.util.Config;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.config.OAuth2Config;
import com.intuit.oauth2.config.Scope;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.InvalidRequestException;
import com.intuit.oauth2.exception.OAuthException;
import com.intuit.oauth2.http.HttpRequestClient;
import com.intuit.oauth2.http.MethodType;
import com.intuit.oauth2.http.Request;
import com.intuit.oauth2.http.Response;
import com.intuit.oauth2.utils.MapperImpl;
@Component
public class QuickBookGateway {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickBookGateway.class);
	
	protected static final String ACCOUNTING_METHOD = "Accrual";
    protected static final String TRIAL_BALANCE = "TrialBalance";
    protected static final String AGED_RECEIVABLE_DETAIL = "AgedReceivableDetail";
    protected static final String ACCOUNT_LIST = "AccountList";

    protected static final String GET_COMPANY_INFO = "/v3/company";
    protected static final String COMPANY_INFO_SQL = "select * from companyinfo";
    protected static final String GET_CUSTOMER_LIST_SQL = "SELECT * FROM Customer WHERE id in (%s)";


    @Autowired
    protected String baseURL;

    @Autowired
    protected String redirectUri;

	
	 @Autowired
	 @Qualifier("scopes")
	 protected Scope scope;
	 
  
    @Autowired
    protected OAuth2Config oauth2Config;

    @Autowired
    protected OAuth2PlatformClient client;
    
    private static final ObjectMapper mapper  = MapperImpl.getInstance();
    
    public QuickBookGateway()
    {
    }
    
    public String connect(String tokenValue) {
    	LOGGER.debug("Enter OAuth2config::prepareUrl");
		List<Scope> scopes = new ArrayList<>(1);
		scopes.add(scope);
		
		LOGGER.info("Redirect URI: " + redirectUri);

		if (scopes == null || scopes.isEmpty() || redirectUri.isEmpty()) {
			LOGGER.error("Invalid request for prepareUrl ");
			return null;
		}
		try {
			return oauth2Config.prepareUrl(scopes, redirectUri, tokenValue);
		} catch (InvalidRequestException e) {
			LOGGER.error("Exception while preparing url for redirect ", e);
		}
		return null;
		
    }
    
	public BearerTokenResponse getToken(String code) {
		BearerTokenResponse response = null;
		try {
			response = client.retrieveBearerTokens(code, redirectUri);
			LOGGER.debug("Tokens : " + response.getAccessToken() + " Refresh token : " + response.getRefreshToken());
		} catch (OAuthException e) {
			// throw new CinchGatewayException(ApplicationErrorType.SOMETHING_WENT_WRONG);
			LOGGER.error("SOMETHING_WENT_WRONG", e);
		}
		return response;
	}
	
	
	public BearerTokenResponse retrieveBearerTokens(String authCode, String clientId, String clientSecret) throws OAuthException {
		LOGGER.debug("Enter OAuth2PlatformClient::retrieveBearerTokens");

        try {
        	HttpRequestClient client = new HttpRequestClient();
        	Request request = new Request.RequestBuilder(MethodType.POST, oauth2Config.getIntuitBearerTokenEndpoint())
											.requiresAuthentication(true)
											.authString(getAuthHeader(clientId, clientSecret))
											.postParams(getUrlParameters(null, authCode, redirectUri))
											.build();

	        Response response = client.makeRequest(request);
	            
	        LOGGER.debug("Response Code : "+ response.getStatusCode());
	        if (response.getStatusCode() != 200) {
	        	LOGGER.debug("failed getting access token");
	        	throw new OAuthException("failed getting access token", response.getStatusCode() + "");
	        }
	
	        ObjectReader reader = mapper.readerFor(BearerTokenResponse.class);
	        BearerTokenResponse bearerTokenResponse = reader.readValue(response.getContent());
	        return bearerTokenResponse;
            
        } catch (Exception ex) {
        	LOGGER.error("Exception while retrieving bearer tokens", ex);
        	throw new OAuthException(ex.getMessage(), ex);
        }
	}
	
	private String getAuthHeader(String clientId, String clientSecret) {
		String base64ClientIdSec = DatatypeConverter.printBase64Binary((clientId + ":" + clientSecret).getBytes());
		return "Basic " + base64ClientIdSec;
	}

	/**
	 * Method to build post parameters
	 * 
	 * @param action
	 * @param token
	 * @param redirectUri
	 * @return
	 */
	private List<NameValuePair> getUrlParameters(String action, String token, String redirectUri) {
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        if (action == "revoke") {
        	urlParameters.add(new BasicNameValuePair("token", token));
        } else if (action == "refresh") {
        	urlParameters.add(new BasicNameValuePair("refresh_token", token));
            urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
        } else {
        	urlParameters.add(new BasicNameValuePair("code", token));
            urlParameters.add(new BasicNameValuePair("redirect_uri", redirectUri));
            urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
        }
		return urlParameters;
	}
	 

	
	  public CompanyInfo getCompanyInfo(String realmId, String accessToken) {
		  System.out.println();
		  System.out.println("BaseUrl"+baseURL);
		  System.out.println();
	  try { 
		  System.out.println("RealmID"+realmId);
		  System.out.println();
		  System.out.println("AccessToken"+accessToken);
		  System.out.println();
		  Config.setProperty(Config.BASE_URL_QBO,
	 baseURL.concat(GET_COMPANY_INFO)); 
	  DataService service =getDataService(realmId, accessToken); 
	 QueryResult queryResult =service.executeQuery(COMPANY_INFO_SQL);
	  
	 if(!CollectionUtils.isEmpty(queryResult.getEntities())) {
		CompanyInfo info=new CompanyInfo();
		 return (CompanyInfo) queryResult.getEntities().get(0);
			 
		
	 } 
	 } 
	  catch (FMSException e) {
	 //return getCompanyInfo(realmId, getNewAccessToken(accessToken));
		  LOGGER.error("SOMETHING_WENT_WRONG",e);
		  }
		  return null; 
		  }
	 
	public Report getTrialBalanceReport(String realmId, String accessToken, String startDate, String toDate) throws OAuthException {
		Report report = null;
		try {
			Config.setProperty(Config.BASE_URL_QBO, baseURL.concat(GET_COMPANY_INFO));
			report = getReport(realmId, accessToken, startDate, toDate, TRIAL_BALANCE);

		} catch (FMSException e) {

			LOGGER.error("QuickBook connection failed", e);
			throw new OAuthException(e.getLocalizedMessage());
		} catch (Exception e) {
			// throw new CinchGatewayException(QUICKBOOK_AUTHORIZATION_FAILED);
			LOGGER.error("QUICKBOOK_AUTHORIZATION_FAILED", e);
			throw new OAuthException(e.getLocalizedMessage());
		}
		return report;

	}
	  
	/* public Report getAgedReceivablesReport(Long realmId, String accessToken,
	 * String startDate, String toDate) throws InvalidTokenException {
	 * 
	 * try { Config.setProperty(Config.BASE_URL_QBO,
	 * baseURL.concat(GET_COMPANY_INFO)); return getReport(realmId.toString(),
	 * accessToken, startDate, toDate, AGED_RECEIVABLE_DETAIL);
	 * 
	 * } catch (FMSException e) { return getAgedReceivablesReport(realmId,
	 * getNewAccessToken(accessToken), startDate, toDate); } catch (Exception e) {
	 * throw new CinchGatewayException(QUICKBOOK_AUTHORIZATION_FAILED); } }
	 * 
	* public List<com.intuit.ipp.data.Customer> getCustomerDetails(String realmId,
	 * String accessToken, List<Long> customerIds) {
	 * Config.setProperty(Config.BASE_URL_QBO, baseURL.concat(GET_COMPANY_INFO));
	 * DataService service = null; List<com.intuit.ipp.data.Customer> customers =
	 * null; try { service = getDataService(realmId, accessToken); QueryResult
	 * queryResult = service.executeQuery(getCustomerInfoQuery(customerIds));
	 * customers = queryResult.getEntities() .stream() .map(queryResultEntity ->
	 * (com.intuit.ipp.data.Customer) queryResultEntity)
	 * .collect(Collectors.toList()); } catch (FMSException e) { throw new
	 * CinchGatewayException(QUICKBOOK_AUTHORIZATION_FAILED); }
	 * 
	 * return customers; }
	 */
    protected Report getReport(String realmId, String accessToken, String startDate, String toDate, String reportType) throws FMSException {
        ReportService service = getReportService(realmId, accessToken);
        service.setStart_date(startDate);
        service.setEnd_date(toDate);
        service.setReport_date(toDate);
        service.setAccounting_method(ACCOUNTING_METHOD);

        return service.executeReport(reportType);
    }

    protected DataService getDataService(String realmId, String accessToken) throws FMSException {

        //create oauth object
        OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
        //create context
        Context context = new Context(oauth, ServiceType.QBO, realmId);

        // create dataservice
        return new DataService(context);
    }

    protected ReportService getReportService(String realmId, String accessToken) throws FMSException {
        //create reportservice
        //create oauth object
        OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
        //create context
        Context context = new Context(oauth, ServiceType.QBO, realmId);

        // create dataservice
        return new ReportService(context);
    }
    
    public BearerTokenResponse getNewToken(String refreshToken) {
		BearerTokenResponse response = null;
		try {
			response = client.refreshToken(refreshToken);
			LOGGER.debug("Tokens : " + response.getAccessToken() + " Refresh token : " + response.getRefreshToken());
		} catch (OAuthException e) {
			// throw new CinchGatewayException(ApplicationErrorType.SOMETHING_WENT_WRONG);
			LOGGER.error("SOMETHING_WENT_WRONG", e);
		}
		return response;
	}

	public Report getAccountList(String realmId, String accessToken) throws FMSException {
		ReportService service = getReportService(realmId, accessToken);
		return service.executeReport(ACCOUNT_LIST);
	}
    
	/*
	 * public BearerTokenResponse refreshToken(String refreshToken, String clientId,
	 * String clientSecret) throws OAuthException {
	 * 
	 * LOGGER.debug("Enter OAuth2PlatformClient::refreshToken"); try {
	 * HttpRequestClient client = new HttpRequestClient(); Request request = new
	 * Request.RequestBuilder(MethodType.POST,
	 * oauth2Config.getIntuitBearerTokenEndpoint())
	 * .requiresAuthentication(true).authString(getAuthHeader(clientId,
	 * clientSecret)) .postParams(getUrlParameters("refresh", refreshToken,
	 * null)).build(); Response response = client.makeRequest(request);
	 * 
	 * LOGGER.debug("Response Code : " + response.getStatusCode()); if
	 * (response.getStatusCode() != 200) {
	 * LOGGER.debug("failed getting access token"); throw new
	 * OAuthException("failed getting access token", response.getStatusCode() + "");
	 * }
	 * 
	 * ObjectReader reader = mapper.readerFor(BearerTokenResponse.class);
	 * BearerTokenResponse bearerTokenResponse =
	 * reader.readValue(response.getContent()); return bearerTokenResponse; } catch
	 * (Exception ex) { LOGGER.error("Exception while calling refreshToken ", ex);
	 * throw new OAuthException(ex.getMessage(), ex); } }
	 */

}
