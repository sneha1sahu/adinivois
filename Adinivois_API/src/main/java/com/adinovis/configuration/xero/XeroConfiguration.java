package com.adinovis.configuration.xero;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xero.api.ApiClient;
import com.xero.api.Config;
import com.xero.api.JsonConfig;
import com.xero.api.OAuthAccessToken;
import com.xero.api.OAuthRequestToken;
import com.xero.api.XeroClient;
import com.xero.api.client.AccountingApi;


@Configuration
public class XeroConfiguration 
{
	 	@Bean
	    public Config initConfig() {
	        return JsonConfig.getInstance();
	    }

	    @Bean
	    public OAuthRequestToken initOAuthRequestToken() {
	        return new OAuthRequestToken(initConfig());
	    }

	    @Bean
	    public OAuthAccessToken initOAuthAccessToken() {
	        return new OAuthAccessToken(initConfig());
	    }

	    @Bean
	    public ApiClient initApiClient() {
	        return new ApiClient(initConfig().getApiUrl(), null, null, null);
	    }

	    @Bean
	    public AccountingApi initAccountingApi() {
	        return new AccountingApi(initApiClient());
	    }

	    @Bean
	    public XeroClient initXeroClient() {
	        return new XeroClient(initConfig());
	    }
}
