package com.adinovis.configuration.xero;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xero.api.OAuthRequestToken;

@Component
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class XeroConnectionDto {
	private OAuthRequestToken authRequestToken;
	private String tokenValue;

	public XeroConnectionDto() {

	}

	public XeroConnectionDto(OAuthRequestToken authRequestToken, String tokenValue) {
		super();
		this.authRequestToken = authRequestToken;
		this.tokenValue = tokenValue;
	}

	public OAuthRequestToken getAuthRequestToken() {
		return authRequestToken;
	}

	public void setAuthRequestToken(OAuthRequestToken authRequestToken) {
		this.authRequestToken = authRequestToken;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

}
