package com.adinovis.configuration.xero;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.xero.api.XeroClient;
import com.xero.api.client.AccountingApi;
import com.xero.model.Contact;
import com.xero.models.accounting.Organisations;
import com.xero.models.accounting.ReportWithRows;
@Component
public class XeroGateway {

	private static final Logger LOGGER = LoggerFactory.getLogger(XeroGateway.class);
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	@Autowired
	AccountingApi accountingApi;
	
	 @Autowired
	    protected XeroClient xeroClient;
	 
	 
    public Organisations getOrganisationsInfo(String accessToken, String accessSecret) {
        accountingApi.setOAuthToken(accessToken, accessSecret);
        try {
            return accountingApi.getOrganisations();
        } catch (IOException e) {
           // log.error("Error Detail :", e);
        	
        	LOGGER.error("Xero_Aothorization_Failed");
        }

        return null;
    }

    public ReportWithRows getTrialBalanceReport(String accessToken, String accessSecret, Integer accountingYear, String toDate) {
        try {
            accountingApi.setOAuthToken(accessToken, accessSecret);
            return accountingApi.getReportTrialBalance(toDate, false);
        } catch (IOException e) {
           // log.error("Error Detail :", e);
        } catch (Exception e) {
          //  log.error("Error Detail :", e);
        	LOGGER.error("Xero_Aothorization_Failed");
        }
        return null;
    }

    public ReportWithRows getTrialBalanceReportWithDate(String accessToken, String accessSecret, String accountingDate) {
        try {
            accountingApi.setOAuthToken(accessToken, accessSecret);
            return accountingApi.getReportTrialBalance(accountingDate, false);
        } catch (IOException e) {
           // log.error("Error Detail :", e);
        } catch (Exception e) {
           // log.error("Error Detail :", e);
            
        }
        return null;
    }

   public Map<Contact, ReportWithRows> getAgedReceivablesReport(String accessToken, String accessSecret, String date, String fromDate, String toDate) {
        Map<Contact, ReportWithRows> agedARMap = new HashMap<Contact, ReportWithRows>( );
        List<Contact> contacts = getContactList( accessToken, accessSecret );
        if (CollectionUtils.isEmpty( contacts )) {
            return agedARMap;
        }

        List<Contact> agedRecContacts = contacts.stream()
                .filter(contact -> contact.isIsCustomer())
                .collect(Collectors.toList());
       Integer counter = 0;
       for (Contact contact : agedRecContacts) {
           try {
               agedARMap.put (contact, accountingApi.getReportAgedReceivablesByContact(UUID.fromString(contact.getContactID()), date, fromDate, toDate));
               counter++;
               if(counter < agedRecContacts.size()) {
                   sleepForMilliSeconds(500);
               }
               //contact.getem
           } catch (IOException e) {
              // log.error("Error Detail :", e);
               return null;
           }
       }
        return agedARMap;
    }
   protected List<Contact> getContactList(String accessToken, String accessSecret) {
       accountingApi.setOAuthToken(accessToken, accessSecret);
       xeroClient.setOAuthToken(accessToken, accessSecret);
       try {
           return xeroClient.getContacts();
       } catch (IOException e) {
           //log.error("Error Detail :", e);
       } catch (Exception e) {
           //log.error("Error Detail :", e);
    	   LOGGER.error("Xero_Aothorization_Failed");
       }

       return Collections.emptyList();
   }

    private void sleepForMilliSeconds(long timeout) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private String getCurrentDate() {
        return LocalDate.now().format(DATE_TIME_FORMATTER);
    }
	
	
	
}
