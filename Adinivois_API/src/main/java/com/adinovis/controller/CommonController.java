package com.adinovis.controller;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.Changepassword;
import com.adinovis.domain.Resetpassword;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;

import freemarker.template.TemplateException;

@RequestMapping(value = CinchConstants.VERSION)
public interface CommonController {

	@RequestMapping(value = CinchConstants.SIGNUP, method = RequestMethod.POST)
	ReturnResponse signup(HttpServletRequest request, @RequestBody SignupDetails signupDetails)throws IOException, TemplateException;

	@RequestMapping(value = CinchConstants.CHANGEPASSWORD, method = RequestMethod.POST)
	ReturnResponse changepassword(HttpServletRequest request, @RequestBody Changepassword changepassword);

	@RequestMapping(value = CinchConstants.RESETPASSWORD, method = RequestMethod.POST)
	ReturnResponse resetpassword(HttpServletRequest request, @RequestBody Resetpassword resetpassword);

	@RequestMapping(value = CinchConstants.FORGOTPASSWORD, method = RequestMethod.POST)
	ReturnResponse forgotPassword(HttpServletRequest request, @RequestBody SignupDetails forgotpassword)throws AddressException, MessagingException, IOException, TemplateException;

	@RequestMapping(value = CinchConstants.UPDATEUSERPROFILE, method = RequestMethod.PUT)
	ReturnResponse updateuserprofile(HttpServletRequest request, @RequestBody Userprofileupdate userprofileupdate);

	@RequestMapping(value = CinchConstants.GETUSERPROFILE, method = RequestMethod.POST)
	ReturnResponse login(HttpServletRequest request, @RequestBody SignupDetails signupDetails);

	@RequestMapping(value = CinchConstants.SIGNUPVALIDATION, method = RequestMethod.POST)
	ReturnResponse signupvalidation(HttpServletRequest request, @RequestBody SignupDetails forgotpassword)throws AddressException, MessagingException, IOException, TemplateException;

	@RequestMapping(value = CinchConstants.LOGINVALIDATION, method = RequestMethod.GET)
	ReturnResponse loginvalidation(String tokenkey);

}
