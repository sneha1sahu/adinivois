package com.adinovis.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.adinovis.domain.AmazonS3Details;
import com.adinovis.common.CinchConstants;
import com.adinovis.domain.ReturnResponse;

@RequestMapping(value = CinchConstants.VERSION)
public interface AmazonS3Controller {
	@RequestMapping(value = CinchConstants.SAVE_FILE, method = RequestMethod.POST)
	ReturnResponse saveFileInS3(@RequestParam Integer status,@RequestParam String signatureKey, @RequestParam MultipartFile file, HttpSession session);

	@RequestMapping(value = CinchConstants.UPDATE_FILE, method = RequestMethod.PUT)
	ReturnResponse updateFileInS3(@RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.DELETE_FILE, method = RequestMethod.PUT)
	ReturnResponse deleteFileInS3(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.CREATE_FOLDER, method = RequestMethod.POST)
	ReturnResponse createFolderInS3(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.GET_FILE, method = RequestMethod.GET)
	ReturnResponse getFile(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.GET_ALL_FILES, method = RequestMethod.GET)
	ReturnResponse getALLFiles(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.DELETE_MULTIPLE_FILES, method = RequestMethod.PUT)
	ReturnResponse deleteMultipleFiles(HttpServletRequest request, @RequestParam String bucketname,
			@RequestParam String folderName, @RequestParam MultipartFile[] files, HttpSession session);

	@RequestMapping(value = CinchConstants.MOVE_MULTIPLE_FILES, method = RequestMethod.PUT)
	ReturnResponse moveMultipleFiles(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.DELETE_FOLDER, method = RequestMethod.PUT)
	ReturnResponse deleteFolder(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

	@RequestMapping(value = CinchConstants.COPY_FOLDER, method = RequestMethod.POST)
	ReturnResponse copyFolder(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);
	
	@RequestMapping(value = CinchConstants.DELETE_FOLDER_WITH_FILES, method = RequestMethod.PUT)
	ReturnResponse deleteFolderWithFiles(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details);

}
