package com.adinovis.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.adinovis.common.CinchConstants;

@RequestMapping(value = CinchConstants.VERSION + "/quickbook")
public interface QuickBookControllerIfc {

	@RequestMapping(value = CinchConstants.GET_QUICKBOOK_CONNECT)
	Object connect(String tokenValue, HttpServletRequest request);

	@RequestMapping(value = CinchConstants.QUICKBOOK_REDIRECT)
	RedirectView callBackFromOAuth(String code, String tokenValue, String realmId, HttpServletRequest request);
}
