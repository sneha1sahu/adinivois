package com.adinovis.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.adinovis.common.CinchConstants;

@RequestMapping(value = CinchConstants.VERSION + "/xero")
public interface XeroControllerIfc {

	void getXeroClientData();

	@RequestMapping(value = CinchConstants.GET_XERO_CONNECT)
	RedirectView connect(String tokenValue, HttpServletResponse response) throws IOException;

	@RequestMapping(value = CinchConstants.XERO_REDIRECT)
	RedirectView callBackFromOAuth(HttpServletRequest request, HttpServletResponse response);

}
