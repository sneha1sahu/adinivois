package com.adinovis.controller;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.ClientTokenStatus;
import com.adinovis.domain.ReturnResponse;

import freemarker.template.TemplateException;

@RequestMapping(value = CinchConstants.VERSION)
public interface ClientController {

	@RequestMapping(value = CinchConstants.SAVECLIENTPROFILE, method = RequestMethod.POST)
	ReturnResponse saveclientprofile(HttpServletRequest request, @RequestBody ClientProfile clientProfile)throws IOException, TemplateException;

	@RequestMapping(value = CinchConstants.UPDATECLIENTPROFILE, method = RequestMethod.PUT)
	ReturnResponse updateclientprofile(HttpServletRequest request, @RequestBody ClientProfile clientProfile);

	@RequestMapping(value = CinchConstants.GETCLIENTPROFILE, method = RequestMethod.GET)
	ReturnResponse getclientprofile(@PathVariable String loginid);

	@RequestMapping(value = CinchConstants.GETJURISDICTION, method = RequestMethod.GET)
	ReturnResponse getjurisdiction();

	@RequestMapping(value = CinchConstants.GETTYPEOFENTITY, method = RequestMethod.GET)
	ReturnResponse gettypeofentity();

	@RequestMapping(value = CinchConstants.GETAUDITORROLE, method = RequestMethod.GET)
	ReturnResponse getauditorrole();

	@RequestMapping(value = CinchConstants.GETALLAUDITOR, method = RequestMethod.GET)
	ReturnResponse getallauditor();

	@RequestMapping(value = CinchConstants.GETBUSINESSDETAIL, method = RequestMethod.GET)
	ReturnResponse getbusinessdetails(@PathVariable String loginid);

	@RequestMapping(value = CinchConstants.DELETECLIENTPROFILE, method = RequestMethod.DELETE)
	ReturnResponse deleteclientprofile(@PathVariable String clientfirmid, @PathVariable String loginid);

	@RequestMapping(value = CinchConstants.GETCLIENTCOUNTDETAILS, method = RequestMethod.GET)
	ReturnResponse getclientcountdetails(@PathVariable String loginid);

	@RequestMapping(value = CinchConstants.GETCLIENTSTATUS, method = RequestMethod.GET)
	ReturnResponse getclientstatus();

	@RequestMapping(value = CinchConstants.CLIENTVALIDATIONMAIL, method = RequestMethod.POST)
	ReturnResponse clientvalidationemail(HttpServletRequest request, @RequestBody ClientProfile clientProfile)
			throws AddressException, MessagingException, IOException, TemplateException;

	@RequestMapping(value = CinchConstants.CLIENTINVITEVALIDATION, method = RequestMethod.POST)
	ReturnResponse clientinvitevalidation(HttpServletRequest request, @RequestBody ClientTokenStatus clientTokenStatus);

	@RequestMapping(value = CinchConstants.GETCLIENTDETAILS, method = RequestMethod.GET)
	ReturnResponse getclientdetail(String tokenkey);
}
