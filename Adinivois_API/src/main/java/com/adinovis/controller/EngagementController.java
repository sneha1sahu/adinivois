package com.adinovis.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.Engagement;
import com.adinovis.domain.ReturnResponse;
@RequestMapping(value = CinchConstants.VERSION)
public interface EngagementController {
	
	@RequestMapping(value = CinchConstants.SAVEENGAGEMENT, method = RequestMethod.POST)
	ReturnResponse saveengagement(HttpServletRequest request, @RequestBody Engagement engagement);
	
	@RequestMapping(value = CinchConstants.GETENGAGEMENTDETAILS, method = RequestMethod.GET)
	ReturnResponse getengagementdetails(@PathVariable int clientfirmid,@PathVariable String loginid);
	
	@RequestMapping(value = CinchConstants.UPDATEENGAGEMENTDETAILS, method = RequestMethod.PUT)
	ReturnResponse updateengagementdetails(HttpServletRequest request, @RequestBody Engagement engagement);

	@RequestMapping(value = CinchConstants.GETALLENGAGEMENT, method = RequestMethod.GET)
	ReturnResponse getallengagement(@PathVariable String loginid);
	
	@RequestMapping(value = CinchConstants.DELETEENGAGEMENT, method = RequestMethod.DELETE)
	ReturnResponse deleteengagement(@PathVariable String engagementsid,@PathVariable String loginid);
	
	@RequestMapping(value = CinchConstants.GETENGAGEMENT, method = RequestMethod.GET)
	ReturnResponse getengagement();
	
	@RequestMapping(value = CinchConstants.GETENGAGEMENTCOUNTDETAILS, method = RequestMethod.GET)
	ReturnResponse getengagementcountdetail(@PathVariable String loginid);
	
	@RequestMapping(value = CinchConstants.GETENGAGEMENTSTATUS, method = RequestMethod.GET)
	ReturnResponse getengagementstatus();
	
	@RequestMapping(value = CinchConstants.GET_ENGAGEMENT_YEAR, method = RequestMethod.GET)
	ReturnResponse getengagementyeardetails(@PathVariable int inengagementsid);

}
