package com.adinovis.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.adinovis.common.CinchConstants;
import com.adinovis.domain.TrailBalance;
import com.adinovis.domain.FsDetails;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.domain.TrialbalanceAdjustmentType;

@RequestMapping(value = CinchConstants.VERSION)
public interface TrailbalanceController {
 
	@RequestMapping(value = CinchConstants.GETTRAILBALANCE, method = RequestMethod.GET)
	ReturnResponse gettrailbalance(@PathVariable int engagementsid,@PathVariable int year);
	
	@RequestMapping(value = CinchConstants.GETADJUSTMENTTYPE, method = RequestMethod.GET)
	ReturnResponse getadjustmenttype();
	
	@RequestMapping(value = CinchConstants.DELETETRAILBALANCEROW, method = RequestMethod.DELETE)
	ReturnResponse deletetrailbalancerow(@PathVariable String inittrialbalid,@PathVariable String loginid);
	
	@RequestMapping(value = CinchConstants.SAVE_TRIALBALANCE_ADJUSTMENT, method = RequestMethod.POST)
	ReturnResponse savetrailbalanceadjustment(HttpServletRequest request,@RequestBody TrialbalanceAdjustmentType trialbalAdjustmentType);
	
	@RequestMapping(value = CinchConstants.GET_ALL_TRIAL_ADJUSTMENT, method = RequestMethod.GET)
	ReturnResponse getAllTrailAdjustment(@PathVariable int inengagementsid);
	
	@RequestMapping(value = CinchConstants.GET_MAPPING_STATUS, method = RequestMethod.GET)
	ReturnResponse getMappingStatus();
	
	//@RequestMapping(value = CinchConstants.GET_BALANCE_STATEMENT, method = RequestMethod.GET)
	ReturnResponse getlBalanceStatement(@PathVariable int engagementsid,@PathVariable int year);
	
	@RequestMapping(value = CinchConstants.GET_BALANCE_SHEET, method = RequestMethod.GET)
	ReturnResponse getlBalanceSheet(@PathVariable int engagementsid,@PathVariable int year);
	
	@RequestMapping(value = CinchConstants.GET_INCOME_STATEMENT, method = RequestMethod.GET)
	ReturnResponse getlIncomeStatement(@PathVariable int engagementsid,@PathVariable int year);
	
	@RequestMapping(value = CinchConstants.GET_INCOME_STATEMENT_TEST, method = RequestMethod.GET)
	ReturnResponse getlIncomeStatementTest(@PathVariable int engagementsid,@PathVariable int year);
	
	@RequestMapping(value = CinchConstants.GET_QUESTION_ANSWER, method = RequestMethod.GET)
	ReturnResponse getQuestionAnswer(@PathVariable int inquestiontypeid,@PathVariable int inengagementsid);
	
	@RequestMapping(value = CinchConstants.REFRESH_TRAIL_BALANCE, method = RequestMethod.GET)
	ReturnResponse refreshTrailBalance(@PathVariable int engagementId);
	
	@RequestMapping(value = CinchConstants.TRAIL_BALANCE_WITH_DUPLICATE, method = RequestMethod.POST)
	ReturnResponse trailBalanceWithDuplicates(@RequestBody TrailBalance trailBalance);
	
	@RequestMapping(value = CinchConstants.MAP_SHEET_DETAILS, method = RequestMethod.GET)
	ReturnResponse getMapSheetDetails();
	
	@RequestMapping(value = CinchConstants.SAVE_TRAILBALANCE_MAPPING, method = RequestMethod.POST)
	ReturnResponse saveTrailbalanceMapping(@RequestBody TrailBalance trailBalance);
	
	@RequestMapping(value = CinchConstants.SAVE_FS_DETAILS, method = RequestMethod.POST)
	ReturnResponse saveFsDetails(@RequestBody FsDetails fsDetails);
	
}
