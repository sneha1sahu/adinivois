package com.adinovis.controller;

import org.springframework.web.bind.annotation.RequestMapping;

import com.adinovis.common.CinchConstants;
import com.adinovis.domain.ReturnResponse;

@RequestMapping(value = CinchConstants.VERSION + "/job")
public interface SchedularJobController {

	@RequestMapping(value = CinchConstants.SCHEDULAR)
	public ReturnResponse runSchedular();
}
