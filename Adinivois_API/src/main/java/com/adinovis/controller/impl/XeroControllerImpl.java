package com.adinovis.controller.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.adinovis.controller.SchedularJobController;
import com.adinovis.controller.XeroControllerIfc;
import com.adinovis.service.ClientService;
import com.adinovis.service.XeroServiceIfc;

@Controller
public class XeroControllerImpl implements XeroControllerIfc {
	@Autowired
	XeroServiceIfc xeroService;

	@Autowired
	ClientService cinchService;
	
	@Autowired
	private SchedularJobController schedularJobController;
	
	@Value("${client.redirect.endpoint}")
	private String clientRedirectEndpoint;

	public RedirectView connect(@RequestParam(value = "tokenValue") String tokenValue, HttpServletResponse response)
			throws IOException {

		/*
		 * ClientData clientData =
		 * cinchService.getClientDataSourceByTokenValue(tokenValue);
		 * 
		 * if (clientData == null ||
		 * !clientData.getSourceLink().toLowerCase().contains("xero")) { return
		 * CommonUtils.getHttpStatusResponse(CloudunionConstants.XERO_CLIENT_NOT_EXIST,
		 * HttpStatus.BAD_REQUEST, tokenValue, null); }
		 */

		String redirectUrl = xeroService.connect(tokenValue);
		response.sendRedirect(redirectUrl);
		return null;
	}

	public RedirectView callBackFromOAuth(HttpServletRequest request, HttpServletResponse response) {

		String verifier = request.getParameter("oauth_verifier");
		String tempToken = request.getParameter("oauth_token");
		int status = 0;
		if (xeroService.callBackFromOAuth(verifier, tempToken)) {
			status = 1;
		}
		return new RedirectView(clientRedirectEndpoint + "xero="  + status);
	}

	public void getXeroClientData() {
		// TODO Auto-generated method stub

	}

}
