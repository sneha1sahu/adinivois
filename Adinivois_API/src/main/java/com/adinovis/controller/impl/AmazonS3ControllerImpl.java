package com.adinovis.controller.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.adinovis.domain.AmazonS3Details;
import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.controller.AmazonS3Controller;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.service.AmazonS3Service;

@RestController
public class AmazonS3ControllerImpl implements AmazonS3Controller {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmazonS3ControllerImpl.class);

	@Autowired
	private AmazonS3Service amazonS3Service;

	@Override
	public ReturnResponse saveFileInS3(@RequestParam Integer status,@RequestParam String signatureKey,@RequestParam MultipartFile file,HttpSession session) {
		LOGGER.debug("save fileInS3 controller");
		boolean saveFile = amazonS3Service.saveFileInS3(status,signatureKey,file,session);
		if (saveFile == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.SAVEFILEINS3, HttpStatus.OK, saveFile, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SAVEFILEINS3FAIL, HttpStatus.BAD_REQUEST, saveFile,
				null);
	}

	@Override
	public ReturnResponse updateFileInS3(@RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("update FileInS3 controller");
		boolean updateFile = amazonS3Service.updateFileInS3(amazons3Details);
		if (updateFile == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.UPDATEFILEINS3, HttpStatus.OK, updateFile,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.UPDATEFILEINS3FAILURE, HttpStatus.BAD_REQUEST,
				updateFile, null);
	}

	@Override
	public ReturnResponse deleteFileInS3(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("delete FileInS3 controller");
		System.out.println("BucketName:" + amazons3Details.getBucketname());
		boolean deleteFile = amazonS3Service.deleteFileInS3(amazons3Details);
		LOGGER.debug("Returned from service");
		if (deleteFile == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FILE, HttpStatus.OK, deleteFile,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FILE_FAILURE, HttpStatus.BAD_REQUEST,
				deleteFile, null);
	}

	@Override
	public ReturnResponse createFolderInS3(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("Create Folder In S3 controller");
		boolean folderCreated = amazonS3Service.createFolderInS3(amazons3Details);
		if (folderCreated == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.CREATE_FOLDER, HttpStatus.OK, folderCreated,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.CREATE_FOLDER_FAILURE, HttpStatus.BAD_REQUEST,
				folderCreated, null);
	}

	@Override
	public ReturnResponse getFile(HttpServletRequest request, @RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("Get file from S3 controller");
		boolean fileExist= amazonS3Service.getFile(amazons3Details);
		if (fileExist == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.GET_FILE, HttpStatus.OK, fileExist,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.GET_FILE_FAILURE, HttpStatus.BAD_REQUEST,
				fileExist, null);
	}

	@Override
	public ReturnResponse getALLFiles(HttpServletRequest request,  @RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("Get all files from S3 controller");
		List<String> filesExist= amazonS3Service.getAllFiles(amazons3Details);
		if (filesExist==null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.GET_FILE_FAILURE, HttpStatus.BAD_REQUEST, filesExist,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.GET_FILE, HttpStatus.OK,
				filesExist, null);
	}

	@Override
	public ReturnResponse deleteMultipleFiles(HttpServletRequest request,@RequestParam String bucketname,@RequestParam String folderName,@RequestParam MultipartFile[] files, HttpSession session) {
		LOGGER.debug("delete multiple files controller");
		boolean deleteFile = amazonS3Service.deleteMultipleFiles(bucketname,folderName,files,session);
		LOGGER.debug("Returned from service");
		if (deleteFile == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FILE, HttpStatus.OK, deleteFile,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FILE_FAILURE, HttpStatus.BAD_REQUEST,
				deleteFile, null);
	}

	//@Override
	public ReturnResponse deleteFolder(HttpServletRequest request,@RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("delete folder controller");
		boolean deleteFolder = amazonS3Service.deleteFolder(amazons3Details);
		LOGGER.debug("Returned from service");
		if (deleteFolder == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FOLDER, HttpStatus.OK, deleteFolder,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FOLDER_FAILURE, HttpStatus.BAD_REQUEST,
				deleteFolder, null);
	}

	@Override
	public ReturnResponse moveMultipleFiles(HttpServletRequest request,@RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("move files In S3 controller");
		boolean filesmoved = amazonS3Service.moveMultipleFiles(amazons3Details);
		if (filesmoved == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.MOVE_FILES, HttpStatus.OK, filesmoved,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.MOVE_FILES_FAILURE, HttpStatus.BAD_REQUEST,
				filesmoved, null);
	}

	@Override
	public ReturnResponse copyFolder(HttpServletRequest request,@RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("copy one folder into another InS3 controller");
		boolean folderMoved = amazonS3Service.copyFolder(amazons3Details);
		if (folderMoved == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FOLDERMOVED, HttpStatus.OK, folderMoved,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FOLDERMOVEFAIL, HttpStatus.BAD_REQUEST,
				folderMoved, null);
	}

	@Override
	public ReturnResponse deleteFolderWithFiles(HttpServletRequest request,@RequestBody AmazonS3Details amazons3Details) {
		LOGGER.debug("delete folder with files controller");
		boolean deleteFolder = amazonS3Service.deleteFolderWithFiles(amazons3Details);
		LOGGER.debug("Returned from service");
		if (deleteFolder == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FOLDER, HttpStatus.OK, deleteFolder,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETE_FOLDER_FAILURE, HttpStatus.BAD_REQUEST,
				deleteFolder, null);
	}


}
