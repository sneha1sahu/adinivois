package com.adinovis.controller.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adinovis.auth.AuthTokenInfo;
import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.common.SendMailNotification;
import com.adinovis.controller.CommonController;
import com.adinovis.domain.Changepassword;
import com.adinovis.domain.CuserProfile;
import com.adinovis.domain.Resetpassword;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.domain.SignupDetails;
import com.adinovis.domain.Userprofileupdate;
import com.adinovis.service.CinchService;
import com.adinovis.service.CloudunionCommonService;

import freemarker.template.TemplateException;

@RestController
public class CommonControllerImpl extends AbstractController implements CommonController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonControllerImpl.class);
	
	@Autowired
    private CloudunionCommonService cloudunionCommonService;
	
	@Autowired
	private CinchService cinchService;
	
	@Autowired
	SendMailNotification sendMailNotification;
	
   //@Autowired
   // private Configuration freemarkerConfig;
	
	public CloudunionCommonService commonService(){
		return cloudunionCommonService;
		}
	
	@Autowired
	public PasswordEncoder passwordEncoder() {
		return new StandardPasswordEncoder();
	}

public ReturnResponse signup(HttpServletRequest request, @RequestBody SignupDetails signupDetails) throws IOException, TemplateException {
		LOGGER.debug("add signup controller");
		String emailID = cinchService.getemailid(signupDetails.getPemailadd());
		if (!StringUtils.isEmpty(emailID)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.EMAILIDEXIST, HttpStatus.BAD_REQUEST, false, null);
		} 
            boolean newRegister = cinchService.signup(signupDetails);
     		if (newRegister==true) {
				// List<CuserProfile> cuserProfile = cinchService.getuserprofile(signupDetails.getPemailadd(),signupDetails.getPpwd());
				 BigInteger accountid = cinchService.getuserid(signupDetails.getPemailadd());
		            
					AuthTokenInfo tokenInfo = createToken(request, accountid);
					//signupDetails.setAccess_token(tokenInfo.getAccess_token());
					boolean tokensave = cinchService.tokensave(accountid,tokenInfo.getAccess_token());
					String signupvalidation = cinchService.signupvalidation(signupDetails.getPemailadd());
					
					sendMailNotification.welcomeMail(signupDetails.getPfname(),signupDetails.getPemailadd(),signupvalidation);
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.REGISTER, HttpStatus.OK, null, null);
			}
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.SIGNUPFAIL, HttpStatus.BAD_REQUEST, newRegister, null);
		}


public ReturnResponse updateuserprofile(HttpServletRequest request, @RequestBody Userprofileupdate userprofileupdate) {
	LOGGER.debug("updateuserprofile controller");
    boolean userprofileupdates = cinchService.updateuserprofile(userprofileupdate);
 		if (userprofileupdates==true) {
 		 List<CuserProfile> cuserProfile = cinchService.getuserprofileupdate(userprofileupdate.getUseraccountid());
         return CommonUtils.getHttpStatusResponse(CloudunionConstants.UPDATEUSERPROFILESUCCESS, HttpStatus.OK, cuserProfile, null);
		}
		 return CommonUtils.getHttpStatusResponse(CloudunionConstants.UPDATEUSERPROFILEFAILURE, HttpStatus.BAD_REQUEST, userprofileupdates, null);
	}

	public ReturnResponse login(HttpServletRequest request, @RequestBody SignupDetails signupDetails) {
		LOGGER.debug("getuserprofile  controller");
		String emailID = cinchService.getemailid(signupDetails.getPemailadd());
		
		if (StringUtils.isEmpty(emailID)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.EMAILIDNOTEXIST, HttpStatus.OK, false, null);
		} 
		String emailvarify = cinchService.getUserVerification(signupDetails.getPemailadd());
		if (StringUtils.isEmpty(emailvarify)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.EMAILVRIFY, HttpStatus.OK, false, null);
		}
        List<CuserProfile> cuserProfile = cinchService.login(emailID,signupDetails.getPpwd());
		if(cuserProfile.isEmpty()){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.LOGINFAILURE, HttpStatus.BAD_REQUEST, cuserProfile, null);
	}
		
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.LOGINSUCCESS, HttpStatus.OK, cuserProfile, null);
	}
	
	public ReturnResponse forgotPassword(HttpServletRequest request, @RequestBody SignupDetails forgotpassword) throws AddressException, MessagingException, IOException, TemplateException {
		LOGGER.debug("forgotPassword  controller");
		boolean forgotPasswordstatuschangepass = cinchService.forgotPasswordstatuschange(forgotpassword.getPemailadd());
		
		String forgotpass = cinchService.forgotPassword(forgotpassword.getPemailadd());
		if (StringUtils.isEmpty(forgotpass)) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.FORGOTPASSNOT, HttpStatus.BAD_REQUEST, null, null);
			}
		   //new SendMailNotification().sendForgotPassword("Hi",forgotpassword.getPemailadd(),forgotpass);
		     sendMailNotification.sendForgotPassword("Hi",forgotpassword.getPemailadd(),forgotpass);
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.FORGOTPASS, HttpStatus.OK, forgotpass, null);
	}
	
	
	public ReturnResponse signupvalidation(HttpServletRequest request, @RequestBody SignupDetails forgotpassword) throws AddressException, MessagingException, IOException, TemplateException {
		LOGGER.debug("signupvalidation  controller");
		String signupvalidation = cinchService.forgotPassword(forgotpassword.getPemailadd());
		if (StringUtils.isEmpty(signupvalidation)) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.SIGNUPVALIDATIONFAIL, HttpStatus.BAD_REQUEST, null, null);
			}
		   new SendMailNotification().sendForgotPassword("Hi",forgotpassword.getPemailadd(),signupvalidation);
		     //sendMailNotification.welcomeMail(forgotpassword.getPfname(),forgotpassword.getPemailadd());
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.SIGNUPVALIDATION, HttpStatus.OK, signupvalidation, null);
	}
	
	public ReturnResponse changepassword(HttpServletRequest request, @RequestBody Changepassword changepassword) {
		LOGGER.debug("changepassword  controller");
		boolean forgotpass = cinchService.changepassword(changepassword.getPuseraccountid(),changepassword.getOldpassword(),changepassword.getNewpassword());
		if (forgotpass==true) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.CHANGEPASSWORDSUCCESS, HttpStatus.OK, forgotpass, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.CHANGEPASSWORDFAILURE, HttpStatus.BAD_REQUEST, forgotpass, null);
	}
	
	public ReturnResponse resetpassword(HttpServletRequest request, @RequestBody Resetpassword resetpassword) {
		LOGGER.debug("resetpassword  controller");
		boolean resetpasswordpass = cinchService.resetpassword(resetpassword.getTokenkey(),resetpassword.getNewpassword());
		if (resetpasswordpass==true) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.CHANGEPASSWORDSUCCESS, HttpStatus.OK, resetpasswordpass, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.CHANGEPASSWORDFAILURE, HttpStatus.BAD_REQUEST, resetpasswordpass, null);
	}
	
	public ReturnResponse loginvalidation(@RequestParam(required = true, value = "token") String tokenkey) {
		LOGGER.debug("loginvalidation  controller");
		String t= tokenkey.replaceAll("\\s","+");
		//String t= tokenkey.replaceAll("/","/");
		boolean loginvalidation = cinchService.loginvalidation(t);
		if (loginvalidation==false) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.LOGINVALIDATIONFAIL, HttpStatus.BAD_REQUEST, null, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.LOGINVALIDATIONSUCCESS, HttpStatus.OK, loginvalidation, null);
	}
	
	

}
