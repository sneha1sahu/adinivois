package com.adinovis.controller.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adinovis.amazon.AmazonS3Gateway;
import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.controller.EngagementController;
import com.adinovis.domain.Allengagement;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.Engagement;
import com.adinovis.domain.EngagementYear;
import com.adinovis.domain.Engagementcountdetail;
import com.adinovis.domain.Engagementdetails;
import com.adinovis.domain.Engagementtype;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.service.EngagementService;

@RestController
public class EngagementControllerImpl implements EngagementController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EngagementControllerImpl.class);

	@Autowired
	private EngagementService engagementService;

	@Autowired
	private AmazonS3Gateway amazonS3Gateway;
	
	private static final String CLIENTS_FOLDER = "Clients";
	public static final String LINUX_SEPARATOR = "/";
	public static final String ENGAGEMENTS_FOLDER = "engagements";
	public static final String ENGAGEMENTS = "engagements";
	public static final String CLIENT = "Client";
	public static final Integer CLIENT_BUCKET_STATUS=0;

	@Override
	public ReturnResponse updateengagementdetails(HttpServletRequest request, @RequestBody Engagement engagement) {
		LOGGER.debug("update engagementdetails controller");
		boolean engagementsave = engagementService.updateengagementdetails(engagement);
		if (engagementsave == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.ENGAGEMENTUPDATE, HttpStatus.OK,
					engagementsave, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.ENGAGEMENTUPDATEFAIL, HttpStatus.BAD_REQUEST,
				engagementsave, null);
	}

	public ReturnResponse saveengagement(HttpServletRequest request, @RequestBody Engagement engagement) {
		LOGGER.debug("add saveengagement controller");
		boolean engagementsave = engagementService.saveengagement(engagement);
		List<Integer> engagementId = engagementService.getEngagementId(engagement.getPclientfirmid());
		if (engagementsave) {
			boolean folderCopied = amazonS3Gateway.copyFolder(CLIENT_BUCKET_STATUS,ENGAGEMENTS_FOLDER,
					CLIENTS_FOLDER + LINUX_SEPARATOR + engagement.getPclientfirmid() + "_" + CLIENT);//+LINUX_SEPARATOR + ENGAGEMENTS
			LOGGER.debug("Folder copied into clients folder");
			if (folderCopied) {
				LOGGER.debug("Folder copied from engagements to  primakey_engagements");
				boolean renameFolder = amazonS3Gateway.renameS3Folder(CLIENT_BUCKET_STATUS,CLIENTS_FOLDER + LINUX_SEPARATOR
						+ engagement.getPclientfirmid() + "_" + CLIENT + LINUX_SEPARATOR + ENGAGEMENTS,
						engagementId.get(0) + "_" + ENGAGEMENTS);
				LOGGER.debug("Folder renamed  with engagement primary key");
			}
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.ENGAGEMENTSAVE, HttpStatus.OK, engagementsave,
					null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.ENGAGEMENTSAVEFAIL, HttpStatus.BAD_REQUEST,
				engagementsave, null);
	}

	public ReturnResponse getengagementdetails(@PathVariable int clientfirmid, @PathVariable String loginid) {
		LOGGER.debug("getallauditor  controller");

		List<Engagementdetails> engagementdetails = engagementService.getengagementdetails(clientfirmid, loginid);
		if (engagementdetails == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					engagementdetails, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, engagementdetails, null);
	}

	public ReturnResponse getallengagement(@PathVariable String loginid) {
		LOGGER.debug("getallauditor  controller");

		List<Allengagement> allengagements = engagementService.getallengagement(loginid);
		if (allengagements == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					allengagements, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, allengagements, null);
	}

	@Override
	public ReturnResponse deleteengagement(@PathVariable String engagementsid, @PathVariable String loginid) {

		LOGGER.debug("deleteclientprofile  controller");
		boolean deleteengagement = engagementService.deleteengagement(engagementsid, loginid);
		if (deleteengagement == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETEENGAGEMENT, HttpStatus.OK,
					deleteengagement, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETEENGAGEMENTFAILURE, HttpStatus.BAD_REQUEST,
				deleteengagement, null);
	}

	public ReturnResponse getengagement() {
		LOGGER.debug("getengagement  controller");

		List<Engagementtype> engagementtype = engagementService.getengagement();
		if (engagementtype == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					engagementtype, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, engagementtype, null);
	}

	public ReturnResponse getengagementcountdetail(@PathVariable String loginid) {
		LOGGER.debug("getengagementcountdetail  controller");

		List<Engagementcountdetail> engagementcountdetail = engagementService.getengagementcountdetail(loginid);
		if (engagementcountdetail == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					engagementcountdetail, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, engagementcountdetail,
				null);
	}

	public ReturnResponse getengagementstatus() {
		LOGGER.debug("getengagementstatus  controller");

		List<Clientstatus> engagementtype = engagementService.getengagementstatus();
		if (engagementtype == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					engagementtype, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, engagementtype, null);
	}

	public ReturnResponse getengagementyeardetails(@PathVariable int inengagementsid) {
		LOGGER.debug("get engagement year details  controller");

		List<EngagementYear> engagementyeardetails = engagementService.getEngagementYearDetails(inengagementsid);
		if (engagementyeardetails == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,
					engagementyeardetails, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, engagementyeardetails,
				null);
	}

}
