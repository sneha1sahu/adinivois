package com.adinovis.controller.impl;

import java.math.BigInteger;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import com.adinovis.auth.AuthTokenInfo;
import com.adinovis.common.TransactionConstant;
import com.adinovis.domain.TransactionParameter;
import com.adinovis.domain.UserBean;
import com.adinovis.service.CloudunionCommonService;
import com.adinovis.utils.security.TokenUtilities;

public class AbstractController {
	@Autowired
    private CloudunionCommonService cloudunionCommonService;
	

	
	public CloudunionCommonService commonService(){
		return cloudunionCommonService;
		}

	
	
	public  AuthTokenInfo createToken(HttpServletRequest request,BigInteger accountid){
		String sessionId=request.getSession().getId();
		String access_token=sessionId+"_#_"+accountid;
	    AuthTokenInfo tokenInfo = new AuthTokenInfo();
	    
	    try {
	    	access_token=TokenUtilities.createToken(access_token);
	    	String refresh_token=TokenUtilities.createToken(access_token);
	    	tokenInfo.setAccess_token(access_token);
			tokenInfo.setRefresh_token(refresh_token);
			updateUserSession(String.valueOf(accountid), sessionId);
		} catch (Exception e) {
		}
           return tokenInfo;
	}
	public UserBean getLoginBean(String loginId){
		List<UserBean> loginBeans = cloudunionCommonService.getLoginBean(loginId);
		UserBean loginBean=null;
		if(loginBeans!=null && loginBeans.size()>0){
			loginBean=loginBeans.get(0);
		}
		return loginBean;
		
	}
	public int getAccountId(String uId){
		int accountId= cloudunionCommonService.getAcountId(uId);
		if(accountId!=0 && accountId>0){
			return accountId;
		}
		return 0;
		
	}
	public void updateUserSession(String uId,String userSession){
		cloudunionCommonService.updateUserSession(uId, userSession);
	}
	
	
	

	
	private TransactionParameter setTransactionParameter(int consumerId,int businessId,int transactionId,int limit,int offset,boolean isRefundRequest,int isDisputed){
		TransactionParameter parameter=new TransactionParameter();
		parameter.setTransactionId(transactionId);
		parameter.setConsumerId(consumerId);
		parameter.setBusinessId(businessId);
		parameter.setLimit(limit);
		parameter.setOffset(offset);
		parameter.setRefundRequested(isRefundRequest);
		parameter.setTransType(TransactionConstant.PURCHASE);
		parameter.setIsDispute(isDisputed);
		return parameter;
	}
}
