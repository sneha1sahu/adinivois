package com.adinovis.controller.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.controller.TrailbalanceController;
import com.adinovis.domain.Adjustmenttype;
import com.adinovis.domain.BalanceSheet;
import com.adinovis.domain.ClientQuestionAnswer;
import com.adinovis.domain.FsDetails;
import com.adinovis.domain.TrailBalance;
import com.adinovis.domain.Gettrailbalance;
import com.adinovis.domain.IncomeSheet;
import com.adinovis.domain.MapSheet;
import com.adinovis.domain.MappingStatus;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.domain.TrialbalanceAdjustmentType;
import com.adinovis.quartz.SchedularJob;
import com.adinovis.service.TrailbalanceService;
@RestController
public class TrailbalanceControllerImpl implements TrailbalanceController{
	private static final Logger LOGGER = LoggerFactory.getLogger(TrailbalanceControllerImpl.class);
	
	@Autowired
	private TrailbalanceService trailbalanceService;
	
	@Autowired
	private SchedularJob schedularJob;
	
	public ReturnResponse gettrailbalance(@PathVariable int engagementsid,@PathVariable int year) {
		LOGGER.debug("gettrailbalance  controller");
		
		List<Gettrailbalance> gettrailbalancetype = trailbalanceService.gettrailbalance(engagementsid,year);
		if(gettrailbalancetype ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, gettrailbalancetype, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, gettrailbalancetype, null);
	}
	
	public ReturnResponse getadjustmenttype() {
		LOGGER.debug("getadjustmenttype  controller");
		
		List<Adjustmenttype> adjustmenttype = trailbalanceService.getadjustmenttype();
		if(adjustmenttype ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, adjustmenttype, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, adjustmenttype, null);
	}
	
	@Override
	public ReturnResponse deletetrailbalancerow(@PathVariable String inittrialbalid,@PathVariable String loginid) {
	
		LOGGER.debug("deletetrialbalrow  controller");
		boolean deleteengagement = trailbalanceService.deletetrailbalancerow(inittrialbalid,loginid);
		if (deleteengagement==true) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETETRIALBALROW, HttpStatus.OK, deleteengagement, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETETRIALBALROWFAILURE, HttpStatus.BAD_REQUEST, deleteengagement, null);
	}

	@Override
	public ReturnResponse savetrailbalanceadjustment(HttpServletRequest request,@RequestBody TrialbalanceAdjustmentType trialbalAdjustmentType) {
		LOGGER.debug("save trialbalanceadjustment  controller");
		boolean saveTrialbalanceAdjustment = trailbalanceService.saveTrialbalanceAdjustmentType(trialbalAdjustmentType);
		if (saveTrialbalanceAdjustment==true) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.SAVE_TRIALBALANACE_ADJUSTMENT, HttpStatus.OK, saveTrialbalanceAdjustment, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.SAVE_TRIALBALANACE_ADJUSTMENT_FAIL, HttpStatus.BAD_REQUEST, saveTrialbalanceAdjustment, null);
		
	}

	@Override
	public ReturnResponse getAllTrailAdjustment(@PathVariable int inengagementsid) {
LOGGER.debug("get all trailadjustment  controller");
		
		List<TrialbalanceAdjustmentType> getalltrailadjustment = trailbalanceService.getalltrailadjustment(inengagementsid);
		if(getalltrailadjustment ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getalltrailadjustment, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getalltrailadjustment, null);
	
	}

	@Override
	public ReturnResponse getMappingStatus() {
LOGGER.debug("getMappingStatus  controller");
		
		List<MappingStatus> getMappingStatus = trailbalanceService.getMappingStaus();
		if(getMappingStatus ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getMappingStatus, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getMappingStatus, null);
		
	}

	@Override
	public ReturnResponse getlBalanceStatement(@PathVariable int engagementsid,@PathVariable int year) {
		LOGGER.debug("getbalancestatement  controller");
		
		List<BalanceSheet> getbalancestatement = trailbalanceService.getBalanceStatement(engagementsid,year);
		if(getbalancestatement ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, CloudunionConstants.BALANCESHHET, null);
	    }
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getbalancestatement, null);
	}

	@Override
	public ReturnResponse getlIncomeStatement(@PathVariable int engagementsid,@PathVariable int year) {
		LOGGER.debug("getincomestatement  controller");
		
		List<IncomeSheet> getIncomestatement = trailbalanceService.getIncomeStatement(engagementsid,year);
		if(getIncomestatement ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getIncomestatement, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getIncomestatement, null);
	}

	@Override
	public ReturnResponse getQuestionAnswer(@PathVariable int inquestiontypeid,@PathVariable int inengagementsid) {
LOGGER.debug("get question answer controller");
		
		List<ClientQuestionAnswer> getQuestionanswer = trailbalanceService.getQuestionAnswer(inquestiontypeid,inengagementsid);
		if(getQuestionanswer ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getQuestionanswer, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getQuestionanswer, null);
	}

	@Override
	public ReturnResponse refreshTrailBalance(@PathVariable int engagementId) {
		LOGGER.debug("refresh trail balance controller for engagement Id : " + engagementId);
		if (schedularJob.loadTrailBalance(engagementId)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, null, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, null, null);
	}

	@Override
	public ReturnResponse getlBalanceSheet(@PathVariable int engagementsid,@PathVariable int year) {
LOGGER.debug("get balance sheet  controller");
		
		List<BalanceSheet> getBalanceSheet = trailbalanceService.getBalanceSheet(engagementsid,year);
		if(getBalanceSheet ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getBalanceSheet, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getBalanceSheet, null);
	}

	@Override
	public ReturnResponse getlIncomeStatementTest(@PathVariable int engagementsid,@PathVariable int year) {
	LOGGER.debug("getincomestatement test  controller");
		
		List<IncomeSheet> getIncomestatementTest = trailbalanceService.getIncomeStatementTest(engagementsid,year);
		if(getIncomestatementTest ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getIncomestatementTest, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getIncomestatementTest, null);
	}

	@Override
	public ReturnResponse trailBalanceWithDuplicates(@RequestBody TrailBalance duplicateTrailBalance) {
		LOGGER.debug("save trialbalanceadjustment  controller");
		List<Integer> saveTrailbalanceWithDuplicates = trailbalanceService.trailbalanceWithDuplicates(duplicateTrailBalance);
		if (saveTrailbalanceWithDuplicates==null) {
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, saveTrailbalanceWithDuplicates, null);
			}
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, saveTrailbalanceWithDuplicates, null);
		
	}

	@Override
	public ReturnResponse getMapSheetDetails() {
		List<MapSheet> getMapSheetDetails = trailbalanceService.getMapSheetDetails();
		if(getMapSheetDetails ==null){
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, getMapSheetDetails, null);
	}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, getMapSheetDetails, null);
	}

	@Override
	public ReturnResponse saveTrailbalanceMapping(@RequestBody TrailBalance trailBalance) {
		LOGGER.debug("save trialbalanceadjustment  controller");
		List<Integer> saveTrailbalanceMapping = trailbalanceService.saveTrailbalanceMapping(trailBalance);
		if (saveTrailbalanceMapping==null) {
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, saveTrailbalanceMapping, null);
			}
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, saveTrailbalanceMapping, null);
		
	}

	@Override
	public ReturnResponse saveFsDetails(@RequestBody FsDetails fsDetails) {
		LOGGER.debug("save fs details  controller");
		boolean fsDetailsStatus = trailbalanceService.saveFsDetails(fsDetails);
		if (fsDetailsStatus) {
            return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, fsDetailsStatus, null);
			}
			 return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, fsDetailsStatus, null);
		
	}
}
