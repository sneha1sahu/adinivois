package com.adinovis.controller.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.adinovis.controller.QuickBookControllerIfc;
import com.adinovis.service.CinchService;
import com.adinovis.service.QuickBookServiceIfc;

@Controller
public class QuickBooksControllerImpl implements QuickBookControllerIfc {

	@Autowired
	public QuickBookServiceIfc quickBookServiceIfc;

	@Autowired
	CinchService cinchService;
	
	@Value("${client.redirect.endpoint}")
	private String clientRedirectEndpoint;

	private static final Logger LOGGER = LoggerFactory.getLogger(QuickBooksControllerImpl.class);

	public Object connect(@RequestParam(value = "tokenValue") String tokenValue, HttpServletRequest request) {

		LOGGER.debug("Request for connecting quickbook client : " + tokenValue);
		
		return quickBookServiceIfc.connect(tokenValue);

	}

	public RedirectView callBackFromOAuth(@RequestParam(value = "code", required = false) String code,
			@RequestParam(value = "state", required = false) String tokenValue,
			@RequestParam(value = "realmId", required = false) String realmId, HttpServletRequest request) {

		int status = 0;
		if (quickBookServiceIfc.callBackFromOAuth(code, tokenValue, realmId)) {
			 status = 1;
		}
		return new RedirectView(clientRedirectEndpoint + "quickbooks=" + status);
	}

}
