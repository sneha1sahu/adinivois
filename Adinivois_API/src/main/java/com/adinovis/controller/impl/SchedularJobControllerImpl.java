package com.adinovis.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.controller.SchedularJobController;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.quartz.SchedularJob;

@RestController
public class SchedularJobControllerImpl implements SchedularJobController {

	@Autowired
	private SchedularJob schedularJob;
	
	@Override
	public ReturnResponse runSchedular() {

		Thread t = new Thread("TRAIL-BALANCE") {
			public void run() {
				schedularJob.getReports();
			}
		};
		t.start();
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SCHEDULAR_JOB, HttpStatus.OK, null, null);
	}

}
