package com.adinovis.controller.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adinovis.amazon.AmazonS3Gateway;
import com.adinovis.auth.AuthTokenInfo;
import com.adinovis.common.CloudunionConstants;
import com.adinovis.common.CommonUtils;
import com.adinovis.common.SendMailNotification;
import com.adinovis.controller.ClientController;
import com.adinovis.domain.AllAuditor;
import com.adinovis.domain.Auditorrole;
import com.adinovis.domain.Businessdetails;
import com.adinovis.domain.ClientProfile;
import com.adinovis.domain.ClientTokenStatus;
import com.adinovis.domain.Clientcountdetails;
import com.adinovis.domain.Clientdetails;
import com.adinovis.domain.Clientprofiles;
import com.adinovis.domain.Clientstatus;
import com.adinovis.domain.GetUserClientId;
import com.adinovis.domain.Jurisdiction;
import com.adinovis.domain.ReturnResponse;
import com.adinovis.domain.Typeofentity;
import com.adinovis.service.CinchService;
import com.adinovis.service.ClientService;

import freemarker.template.TemplateException;

@RestController
public class ClientControllerImpl extends AbstractController implements ClientController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientControllerImpl.class);

	@Autowired
	SendMailNotification sendMailNotification;

	@Autowired
	private ClientService clientService;

	@Autowired
	private AmazonS3Gateway amazonS3Gateway;

	@Autowired
	private CinchService cinchService;
	private static final String CLIENTS_FOLDER = "Clients";
	public static final String LINUX_SEPARATOR = "/";
	public static final String CLIENT = "Client";
	private static final String CLIENT_DOCS = "ClientDocs";
	public static final Integer CLIENT_BUCKET_STATUS=0;
	
	public ReturnResponse saveclientprofile(HttpServletRequest request, @RequestBody ClientProfile clientProfile)
			throws IOException, TemplateException {
		LOGGER.debug("add saveclientprofile controller");
		String emailID = cinchService.getemailid(clientProfile.getPemailadd());
		if (!StringUtils.isEmpty(emailID)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.EMAILIDEXIST, HttpStatus.BAD_REQUEST, false,
					null);
		}
		boolean clientsave = clientService.saveclientprofile(clientProfile);
		if (clientsave == true) {
			List<GetUserClientId> userclientId = clientService.getclientid(clientProfile.getPemailadd(),clientProfile.getPpwd());
			for (GetUserClientId getUserClient : userclientId) {
				boolean folderCreated = amazonS3Gateway.createFolder(CLIENT_BUCKET_STATUS,CLIENTS_FOLDER + LINUX_SEPARATOR
						+ getUserClient.getClientConfirmId() + "_" + CLIENT + LINUX_SEPARATOR + CLIENT_DOCS);
				AuthTokenInfo tokenInfo = createToken(request, getUserClient.getUserAcctId());
				// signupDetails.setAccess_token(tokenInfo.getAccess_token());
				boolean tokensave = clientService.tokensave(getUserClient.getUserAcctId(), tokenInfo.getAccess_token());
				String clientvalidation = clientService.clientvalidation(clientProfile.getPemailadd());
				sendMailNotification.welcomeMailclient(clientProfile.getPbsname(), clientProfile.getPemailadd(),
						clientvalidation);
			}
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTSAVE, HttpStatus.OK, clientsave, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTFAIL, HttpStatus.BAD_REQUEST, clientsave,null);
	}

	public ReturnResponse updateclientprofile(HttpServletRequest request, @RequestBody ClientProfile clientProfile) {
		LOGGER.debug("add saveclientprofile controller");
		String emailID = cinchService.getemailid(clientProfile.getPemailadd());
		if (!StringUtils.isEmpty(emailID)) {
			boolean clientsave = clientService.updateclientprofile(clientProfile);
			if (clientsave == true) {
				return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTUPDATE, HttpStatus.OK, clientsave,null);
			}
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTUPDATEFAIL, HttpStatus.BAD_REQUEST,clientsave, null);

		}
		/*
		 * boolean clientsave = clientService.updateclientprofile(clientProfile); if
		 * (clientsave==true) { return
		 * CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTUPDATE,
		 * HttpStatus.OK, clientsave, null); }
		 */
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.EMAILIDNOTEXIST, HttpStatus.BAD_REQUEST, false,null);
	}

	public ReturnResponse getclientprofile(@PathVariable String loginid) {
		LOGGER.debug("getclientprofile  controller");

		List<Clientprofiles> cuserProfile = clientService.getclientprofile(loginid);
		if (cuserProfile == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, cuserProfile,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, cuserProfile, null);
	}

	public ReturnResponse getclientstatus() {
		LOGGER.debug("getclientstatus  controller");

		List<Clientstatus> clientstatus = clientService.getclientstatus();
		if (clientstatus == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, clientstatus,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, clientstatus, null);
	}

	public ReturnResponse clientvalidationemail(HttpServletRequest request, @RequestBody ClientProfile clientProfile)
			throws AddressException, MessagingException, IOException, TemplateException {
		LOGGER.debug("clientvalidationemail  controller");
		String clientvalidationemail = clientService.clientvalidationemail(clientProfile.getPemailadd());
		if (StringUtils.isEmpty(clientvalidationemail)) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, null, null);
		}
		sendMailNotification.clientvalidationemail("Hi", clientProfile.getPemailadd(), clientvalidationemail);
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, clientvalidationemail,null);
	}

	public ReturnResponse getjurisdiction() {
		LOGGER.debug("getjurisdiction  controller");

		List<Jurisdiction> cuserProfile = clientService.getjurisdiction();
		if (cuserProfile == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, cuserProfile,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, cuserProfile, null);
	}

	public ReturnResponse gettypeofentity() {
		LOGGER.debug("gettypeofentity  controller");

		List<Typeofentity> cuserProfile = clientService.gettypeofentity();
		if (cuserProfile == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, cuserProfile,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, cuserProfile, null);
	}

	public ReturnResponse clientinvitevalidation(HttpServletRequest request,
			@RequestBody ClientTokenStatus clientTokenStatus) {
		LOGGER.debug("clientinvitevalidation  controller");
		boolean clientinvitevalidationpass = clientService.clientinvitevalidation(clientTokenStatus.getTokenkey(),
				clientTokenStatus.getPstatusid());
		if (clientinvitevalidationpass == true) {
			// List<Clientdetails> clientdetails =
			// clientService.getclientdetail(clientTokenStatus.getTokenkey());

			return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTSUCCESS, HttpStatus.OK,clientinvitevalidationpass, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.CLIENTFAILURE, HttpStatus.BAD_REQUEST,clientinvitevalidationpass, null);
	}

	public ReturnResponse getclientdetail(@RequestParam(required = true, value = "token") String tokenkey) {
		LOGGER.debug("getclientdetail  controller");
		String t = tokenkey.replaceAll("\\s", "+");
		List<Clientdetails> clientdetails = clientService.getclientdetail(t);
		if (clientdetails == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, clientdetails,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, clientdetails, null);
	}

	public ReturnResponse getauditorrole() {
		LOGGER.debug("getauditorrole  controller");

		List<Auditorrole> cuserProfile = clientService.getauditorrole();
		if (cuserProfile == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, cuserProfile,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, cuserProfile, null);
	}

	public ReturnResponse getallauditor() {
		LOGGER.debug("getallauditor  controller");

		List<AllAuditor> cuserProfile = clientService.getallauditor();
		if (cuserProfile == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST, cuserProfile,null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, cuserProfile, null);
	}

	public ReturnResponse getbusinessdetails(@PathVariable String loginid) {
		LOGGER.debug("getbusinessdetails  controller");

		List<Businessdetails> businessdetails = clientService.getbusinessdetails(loginid);
		if (businessdetails == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,businessdetails, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, businessdetails, null);
	}

	@Override
	public ReturnResponse deleteclientprofile(@PathVariable String clientfirmid, @PathVariable String loginid) {

		LOGGER.debug("deleteclientprofile  controller");
		boolean deleteclientprofile = clientService.deleteclientprofile(clientfirmid, loginid);
		if (deleteclientprofile == true) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETECLIENTPROFILESUCCESS, HttpStatus.OK,deleteclientprofile, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.DELETECLIENTPROFILEFAILURE, HttpStatus.BAD_REQUEST,deleteclientprofile, null);
	}

	public ReturnResponse getclientcountdetails(@PathVariable String loginid) {
		LOGGER.debug("getclientcountdetails  controller");

		List<Clientcountdetails> clientcountdetails = clientService.getclientcountdetails(loginid);
		if (clientcountdetails == null) {
			return CommonUtils.getHttpStatusResponse(CloudunionConstants.FAILURE, HttpStatus.BAD_REQUEST,clientcountdetails, null);
		}
		return CommonUtils.getHttpStatusResponse(CloudunionConstants.SUCCESS, HttpStatus.OK, clientcountdetails, null);
	}

}
