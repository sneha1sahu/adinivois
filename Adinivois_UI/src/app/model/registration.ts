export class RegisterModel {
  firstName: string;
  lastName: string;
  cellPhone: string;
  businessPhone: string;
  memberLicenseNo: string;
  businessName: string;
  businessEmail: string;
  businessAddress: string;
  password: string;
  ConfirmPassword: string;
  terms: boolean;
  


  constructor(values: Object = {}) {
    //Constructor initialization
    Object.assign(this, values);
  }

}
