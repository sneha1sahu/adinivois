import { Injectable } from '@angular/core';
import { Observable,throwError  } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private httpClient: HttpClient;
  constructor(private http: HttpClient, private router: Router, handler: HttpBackend) { 

    //Ignorable HttpClient Interceptors for multipart file upload
    this.httpClient = new HttpClient(handler);
  }

  public loginAuditor(data): Observable<any> {
    const path = environment.apiEndpoint + '/getuserprofile';
    return this.http.post(path, data);
  }

  public forgotPassword(data): Observable<any> {
    const path = environment.apiEndpoint + '/forgotpassword';
    return this.http.post(path, data);
  }

  public auditorSignUp(data): Observable<any> {
    const path = environment.apiEndpoint + '/signup';
    return this.http.post(path, data);
  }

  public getUserProfile(email){
    const path = environment.apiEndpoint + '/' + email + '/getuserprofile';
    return this.http.get(path);
  }

  public createClient(data): Observable<any>{
    const path = environment.apiEndpoint + '/saveclientprofile';
    return this.http.post(path, data);
  }

  public updateClient(data): Observable<any>{
    const path = environment.apiEndpoint + '/updateclientprofile';
    return this.http.put(path, data);
  }
  public deleteClient(clientid, auditorid): Observable<any>{
    const path = environment.apiEndpoint + '/' + clientid + '/' + auditorid + '/deleteclientprofile';
    return this.http.delete(path);
  }
  
  public getJurisdiction(): Observable<any>{
    const path = environment.apiEndpoint + '/getjurisdiction';
    return this.http.get(path);
  }

  public getTypeOfEntity(): Observable<any>{
    const path = environment.apiEndpoint + '/gettypeofentity';
    return this.http.get(path);
  }
  public getAllClients(auditorid){
    const path = environment.apiEndpoint + '/' + auditorid + '/getclientprofile';
    return this.http.get(path);
  }
  public resendLinkToClient(data): Observable<any>{
    const path = environment.apiEndpoint + '/clientvalidationmail';
    return this.http.post(path, data).pipe(catchError(this.handleError));
  }


  public getClientStatus(): Observable<any> {
    const path = environment.apiEndpoint + '/getclientstatus';
    return this.http.get(path);
  }
  public getEngagementStatus(): Observable<any> {
    const path = environment.apiEndpoint + '/getengagementstatus';
    return this.http.get(path);
  }

  public getclientcountdetails(auditorid): Observable<any> {
    const path = environment.apiEndpoint + '/' + auditorid + '/getclientcountdetails';
    return this.http.get(path);
  }

   public updateUserProfile(data): Observable<any> {
     const path = environment.apiEndpoint + '/updateuserprofile';
     return this.http.put(path, data);
  }
  public saveProfileImageToS3(data) {
    const path = environment.apiEndpoint + '/savefile';
    return this.httpClient.post<any>(path, data);
  }
  
  public getClientDetails(token): Observable<any> {
    const path = environment.apiEndpoint + '/getclientdetail?token='+ token;
    return this.http.get(path);
  }

  public quickbookConnect(token): Observable<any> {
    const path = environment.apiEndpoint + '/quickbook/connect?tokenValue=' + token;
    return this.http.get(path);
  }


  /**************** Create enagement * *******************/
  public createEngagement(data): Observable<any>{
    const path = environment.apiEndpoint + '/saveengagement';
    return this.http.post(path, data);
  }
  public getAllAuditor():Observable<any>{
    const path = environment.apiEndpoint + '/getallAuditor';
    return this.http.get(path);
  }
  public getAuditorRole(): Observable<any>{
    const path = environment.apiEndpoint + '/getauditorrole';
    return this.http.get(path);
  }
  public getBusinessDetails(auditorId): Observable<any>{
    const path = environment.apiEndpoint + '/' + auditorId + '/getbusinessdetails';
    return this.http.get(path);
  }
  public getAllengagement(auditorId): Observable<any>{
    const path = environment.apiEndpoint + '/' + auditorId + '/getallengagement';
    return this.http.get(path);
  }
  public deleteEngagement(id, auditorid): Observable<any>{
    const path = environment.apiEndpoint + '/' + id + '/' + auditorid + '/deleteengagement';
    return this.http.delete(path);
  } 
  public getEngagement(): Observable<any>{
    const path = environment.apiEndpoint +'/getengagement';
    return this.http.get(path);
  } 
  public getEngagementCountDetail(auditorId): Observable<any>{
    const path = environment.apiEndpoint + '/' + auditorId + '/getengagementcountdetail';
    return this.http.get(path);
  } 
  public updateEngagementDetails(data): Observable<any>{
    const path = environment.apiEndpoint +'/updateengagementdetails';
    return this.http.put(path, data);
  }  
  /****Login service */
  public verifyToken(data): Observable<any>{
    const path = environment.apiEndpoint +'/login?token='+data;
    return this.http.get(path);
  }
  /****Reset Password service */
  public resetPasswordVerifyToken(data): Observable<any>{
    const path = environment.apiEndpoint +'/resetpassword';
    return this.http.post(path, data);
  }

  /****Reset Password service */
  public inviteClientVerifyToken(data): Observable<any> {
    const path = environment.apiEndpoint + '/clientinvitevalidation';
    return this.http.post(path, data);
  }
  /****Chang Password service */
  public changePassword(data): Observable<any> {
    const path = environment.apiEndpoint + '/changepassword';
    return this.http.post(path, data);
  }
   /****Trial Balance */
   gettrailBalance(engagementId,year){
    const path = environment.apiEndpoint + '/' + engagementId +'/' + year + '/gettrailbalance';
    return this.http.get(path);
  }

  refreshTrialBalance(engagementId){
    const path = environment.apiEndpoint + '/' + engagementId +'/trail-balance';
    return this.http.get(path);
  }
  deleteAdjustedTrialBal(trailbalanceid,auditorId){
    const path = environment.apiEndpoint + '/' + trailbalanceid + '/'+ auditorId +'/deletetrailbalancerow';
    return this.http.delete(path);
  }
  getMappingStatus(){
    const path = environment.apiEndpoint +'/getmappingstatus';
    return this.http.get(path); 
  }
  getEngagementYearDetails(engementId){
    const path = environment.apiEndpoint + '/'+ engementId+'/getengagementyeardetails';
    return this.http.get(path); 
  }
  //Balance statement with ID's
  getBalanceSheet(engementId,year){
    const path = environment.apiEndpoint + '/' + engementId + '/' + year +'/getbalancesheet';
    return this.http.get(path); 
  }
  //Balance statement
  getBalanceStatement(engementId,year){
    const path = environment.apiEndpoint + '/'+ engementId+'/'+ year+'/getbalancestatement';
    return this.http.get(path); 
  }
  //Income statement
  getIncomeStatement(engementId,year){
    const path = environment.apiEndpoint + '/'+ engementId+'/'+ year+'/getincomestatementtest';
    return this.http.get(path); 
  } 
  //Adjusment entry
  getAdjustmentType(){
    const path = environment.apiEndpoint +'/getadjustmenttype';
    return this.http.get(path); 
  } 
  getAlltrailAdjustment(engementId){
    const path = environment.apiEndpoint + '/'+ engementId+'/getalltrailadjustment';
    return this.http.get(path); 
  }
  saveTrialBalanceAdjustment(data){
    const path = environment.apiEndpoint +'/savetrialbalanceadjustment';
    return this.http.post(path,data); 
  }

  getFinancialDocumentDetailsFromJSON(url){
    return this.http.get(url);
  }
  saveFinancialDocumentJSON(data){
    const path = environment.apiEndpoint + '/savefile';
    return this.httpClient.post<any>(path, data);
  }

  saveTrialBalanceRow(data){
    const path = environment.apiEndpoint + '/duplicate-trail-balance';
    return this.httpClient.post<any>(path, data);
  }
  deleteTrialBalanceRow(id, auditorid){
    const path = environment.apiEndpoint + '/' + id + '/' + auditorid + '/deletetrailbalancerow';
    return this.http.delete(path);
  }
  
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  // errorHandler(error: Response) {
  //   return Observable.throw(error);
  // }
}

