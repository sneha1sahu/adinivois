import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let user: any = JSON.parse((sessionStorage.getItem('userDetails')));
    if(user){
      const token:any = user.tokenvalue;
      if(token){
        request = request.clone({ headers: request.headers.set('access_token', token) });
      }
    }
    if (!request.headers.has('Content-Type')) {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
      }
  
      request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
      return next.handle(request);
  }
}
