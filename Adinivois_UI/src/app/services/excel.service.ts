import { Injectable } from '@angular/core';
import * as Excel from 'exceljs/dist/exceljs.min.js';
@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  sName: string;
  excelFileName: string = '';
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Acc.No','Description', 'Original', 'Adjusting', 'Final','PY1','Change','PY2','Map No','Leadsheet','Grouping','Sub Grouping','GIFI Code','FX Translation','Mapping Status'];
  reportTotalData: any = [];
  constructor() { }
  exportToExcel(data, engagementDetails, year) {
    this.excelFileName = 'Trail balance';
    const workbook = new Excel.Workbook();
    workbook.creator = 'Web';
    workbook.lastModifiedBy = 'Web';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 6, activeCell: 'A1', showGridLines: true }] });
    const sheet = workbook.getWorksheet(1);
    //const head1 = ['Exported Data'];
    //sheet.addRow(head1);
    
    
    sheet.mergeCells('E1', 'L2');
    sheet.getCell('E1').value = engagementDetails.engagementName ? 'Engagement Name: ' + engagementDetails.engagementName : '';
    sheet.getCell('E1').alignment = { horizontal:'center'} ;
    sheet.getCell('E1').font = { name: 'Calibri', family: 4, size: 12, bold: true }

    sheet.mergeCells('E3', 'L3');
    sheet.getCell('E3').value = year ? 'As of ' + year : '';
    sheet.getCell('E3').alignment = { horizontal:'center'} ;

    sheet.mergeCells('E4', 'L4');
    sheet.getCell('E4').value = 'Trial Balance';
    sheet.getCell('E4').alignment = { horizontal:'center'} ;
    sheet.getCell('E4').font = { name: 'Calibri', family: 4, size: 16, bold: true }

    sheet.addRow('');
    sheet.getRow(6).values = this.cols;
    sheet.columns = [
      { key: 'accountcode' },
      { key: 'accountname' },
      { key: 'originalbalance' },
      { key: 'adjustmentamount' },      
      { key: 'finalamount' },
      { key: 'py1' },
      { key: 'changes' },
      { key: 'py2' },
      { key: 'mapno' },
      { key: 'leadsheetcode' },
      { key: 'fingroupname' },
      { key: 'finsubgroupname' },
      { key: 'gificode' },
      { key: 'fxtranslation' },
      { key: 'mappingstatus' }
    ];
    sheet.addRows(data);
    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = this.excelFileName;
      a.click();
    });
  }
}
