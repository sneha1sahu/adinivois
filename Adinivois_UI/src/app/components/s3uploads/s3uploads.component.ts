import { Component, OnInit } from '@angular/core';
import { Observable,throwError  } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-s3uploads',
  templateUrl: './s3uploads.component.html',
  styleUrls: ['./s3uploads.component.scss']
})
export class S3uploadsComponent implements OnInit {
  private httpClient: HttpClient;
  profileImage: any;
  profilePic: any;
  folderName: any;
  constructor(private http: HttpClient, private router: Router, handler: HttpBackend) { 
    this.httpClient = new HttpClient(handler);
  }

  ngOnInit() {
  }

  uploadProfilePic(event) {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileImage  = file;
      const reader = new FileReader();
      reader.onload = e => this.profilePic = reader.result;
      reader.readAsDataURL(file);
    }
  }

  createFolder(){
    let url = 'http://3.215.103.80:8080/api-adinovis/v1/createfolder';
    let data = {
      "bucketname":"adinovisemailtemplateimage",
      "foldername": 'profilepicture' + this.folderName
    };
    this.httpClient.post(url, data).subscribe(response => {
      alert('folder created successfully');
    }, error =>{
      alert('error in creating folder');
    })
  }

  createTextFile(){
    //generate text file with content
    let financialDoc= {
      "coverPage": {"logo": "sample.jpg", "clientName": "SampleName", "date": "12/25/2018"},
      "page2": {"title1": "sample", "title2": "<strong>sample</strong>"},
      "page3": {"title1": "sample", "title2": "<strong>sample</strong>"}
    }
    let jsonString = JSON.stringify(financialDoc);
    var blob = new Blob([jsonString], {type: "text/json;charset=utf-8"});
    saveAs(blob, "sample.json");
    this.saveFileToS3(blob);
    //let jsonObj:any = JSON.parse(jsonString);
    // let url = "https://jsonplaceholder.typicode.com/todos/1";
    // this.http.get(url, {responseType: 'blob'})
    //   .subscribe((res) => {
    //     console.log(res);
    //     saveAs(res, "myfile.json")
    // })
  }

  saveFileToS3(blob){
    let url = 'http://3.215.103.80:8080/api-adinovis/v1/savefile';
    let formData:FormData = new FormData();
    let s3signatureKey = 'profilepicture/fd/' + 'sample1.json';
    formData.append('bucketname', 'adinovisemailtemplateimage');
    formData.append('signatureKey', s3signatureKey);
    formData.append('file', blob, 'sample1.json');
    this.httpClient.post(url, formData).subscribe(response => {
      alert('file saved successfully');
    }, error =>{
      alert('error in saving file');
    })

    this.getJSON().subscribe(data => {
      console.log(data);
    });
  }

  public getJSON(): Observable<any> {
      return this.http.get("http://s3.amazonaws.com/adinovisemailtemplateimage/profilepicture/sample1.json");
  }

}
