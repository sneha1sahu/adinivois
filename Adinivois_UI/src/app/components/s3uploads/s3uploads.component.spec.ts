import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S3uploadsComponent } from './s3uploads.component';

describe('S3uploadsComponent', () => {
  let component: S3uploadsComponent;
  let fixture: ComponentFixture<S3uploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S3uploadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S3uploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
