import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';

@Component({
  selector: 'dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardHeaderComponent implements OnInit {
  engagementDetailsObj: string;
  selectedEngagementName: any;
  engagementYearEnd: any;

  constructor(public router: Router,private sharedservice:SharedService) { }
  userData: any;
  nameAcronym: any;
  headerTitle:any;
  engagementName: any;
  yearEnd: any;
  profilePic: any = '';
  profileSubscription: any;
  ngOnInit() {
    this.sharedservice.selectedEngagementDetails.subscribe(reponse=>{
      if(reponse){
        this.selectedEngagementName=reponse['engagementName'];
        this.engagementYearEnd=reponse['yearEnd'];
      }
    
    })
this.engagementDetailsObj= JSON.parse(localStorage.getItem("selectedEngagementObj"))
if(this.engagementDetailsObj){
 // this.selectedEngagementName=this.engagementDetailsObj['engagementName'];
 // this.engagementYearEnd=this.engagementDetailsObj['yearEnd'];
}
    this.userData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.userData && this.userData.fullName){
      let matches = this.userData.fullName.match(/\b(\w)/g);
      let acronym = matches.join('');
      if(acronym && acronym.length > 2){
        acronym = acronym.slice(0, 2);
      }
      this.nameAcronym = acronym;
    }
    else{
      if(!this.userData){
        this.router.navigate(['/login']);
      }
    }
    this.profilePic = this.userData ? this.userData.profilepicture : '';
    this.headerTitle = this.sharedservice.headerDetails.subscribe(response => {
      if(response){
        this.engagementName = response.engagementName;
        this.yearEnd = response.yearEnd;
        if(this.yearEnd){
          localStorage.setItem("yearEnd", this.yearEnd); 
        }
        if(this.engagementName){
          localStorage.setItem("engementName", this.engagementName);
        }
        this.sharedservice.balStatementEngagementName=this.engagementName
        this.sharedservice.balStmtYearEnd=this.yearEnd;
      }
    })
    try{
      this.profileSubscription = this.sharedservice.profileDetails.subscribe(response => {
        if(response){
          if(this.userData){
            this.userData.fullName = response.fullName ? response.fullName : this.userData.fullName;
            this.profilePic = response.profilepicture ? response.profilepicture : this.profilePic;
          }
          
        }
      })
    }
    catch(e){

    }
  }


  logout(){
    sessionStorage.removeItem("userDetails");
    this.router.navigate(['/login']);
  }
  createNewEngagement(){
    this.sharedservice.selectedClient={};
    this.router.navigate(['/dashboard/createengagement']);
  }

  clientListSearch(filterValue: string) {
    this.sharedservice.clientSearchString.next(filterValue)
    //this.sharedservice.gridData.filter = filterValue.trim().toLowerCase();
  }
  engagementListSearch(filterValue: string) {
    this.sharedservice.gridDataEngage.filter = filterValue.trim().toLowerCase();
  }

}
