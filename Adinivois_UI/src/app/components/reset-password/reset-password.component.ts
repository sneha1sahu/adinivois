import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../helpers/must-match.validator';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { MessageAlertsComponent } from '../shared/message-alerts/message-alerts.component';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ResetPasswordComponent implements OnInit {

  user: any;

  resetform: FormGroup; // Declare the signupForm
    token: any;
    urlStr: any;
    isToken: boolean = false;
    data: any;

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private snackBar: MatSnackBar,
              private apiService: ApiService,
              private router: Router,
            private route: ActivatedRoute)
  {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      if(this.token){
        this.urlStr = this.token.replace(/ /g, "+");
      }
      
    });
  }

  ngOnInit() {

    this.resetform = this.fb.group({
      'newPassword': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'confirmPassword': new FormControl('', [Validators.required])

    }, { validator: MustMatch('newPassword', 'confirmPassword') });
  }

  get newPassword() {
    return this.resetform.get('newPassword');
  }
  get confirmPassword() {
    return this.resetform.get('confirmPassword');
  }

  //Submitting Reset form
  public onFormSubmit() {
    if (this.resetform.valid) {
      this.user = this.resetform.value;
      this.data = {
        "newpassword": this.user.newPassword,
        "tokenkey": this.urlStr
      }
      if (this.urlStr) {
        //  this.isToken=true;
        this.apiService.resetPasswordVerifyToken(this.data).subscribe(response => {
          //this.isToken = true;
          if (response._statusCode == 200) {
            this.isToken = true;
          }
          else {
            //alert('client created successfully');
            alert("link expired..");
          }

        }, error => {
          alert("error occured");
        })
      }

    }

  }

}
