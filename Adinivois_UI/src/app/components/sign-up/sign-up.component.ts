import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterModel } from '../../model/registration';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../helpers/must-match.validator';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { MessageAlertsComponent } from '../shared/message-alerts/message-alerts.component';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['../../../icons.scss', './sign-up.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SignUpComponent implements OnInit {

  submitted = false; 
  term = false; 
  signUpState = false; 

  user: RegisterModel = new RegisterModel();
  
  signupform: FormGroup; // Declare the signupForm

  durationInSeconds = 5;
  userEmailId: string;
  constructor(private fb: FormBuilder, private http: HttpClient, private snackBar: MatSnackBar,
     private apiService: ApiService, private toastr: ToastrService,
     private spinner: NgxSpinnerService) { }

  ngOnInit() {

    this.signupform = this.fb.group({
      'firstName': new FormControl("", [Validators.required]),
      'lastName': new FormControl("", [Validators.required]),
      'cellPhone': new FormControl("", [Validators.required]),
      'businessPhone': new FormControl("", [Validators.required]),
      'memberLicenseNo': new FormControl(),
      'businessName': new FormControl(),
      'businessEmail': new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'businessAddress': new FormControl(),
      'password': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'confirmPassword': new FormControl('', [Validators.required]),
      'terms': new FormControl(['', Validators.requiredTrue]),
      'recaptchaValue': new FormControl(null, Validators.required)

    },{validator: MustMatch('password', 'confirmPassword')});

    //this.apiService.getUserProfile().subscribe(response => {
    //  console.log(response);
    //},
    //error => {
    //  alert('error in get');
    //})
    
  } 

  get firstName(){
    return this.signupform.get('firstName');
  }
  get lastName() {
    return this.signupform.get('lastName');
  }
  get cellPhone() {
    return this.signupform.get('cellPhone');
  }
  get businessPhone() {
    return this.signupform.get('businessPhone');
  }
  get memberLicenseNo() {
    return this.signupform.get('memberLicenseNo');
  }
  get businessName() {
    return this.signupform.get('businessName');
  }
  get businessEmail() {
    return this.signupform.get('businessEmail');
  }
  get businessAddress() {
    return this.signupform.get('businessAddress');
  }
  get password() {
    return this.signupform.get('password');
  }
  get confirmPassword() {
    return this.signupform.get('confirmPassword');
  }
  get terms() {
    return this.signupform.get('terms');
  }

  /*resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
  } */

  // convenience getter for easy access to form fields
  get f() { return this.signupform.controls; }


  //Submitting form
  public onFormSubmit() {
    this.submitted = true;
    if (this.signupform.valid) {
      this.user = this.signupform.value;
      let data: any = {
        "pfname": this.user.firstName,
        "plname": this.user.lastName,
        "pbsname": this.user.businessName,
        "pemailadd": this.user.businessEmail,
        "ppwd": this.user.password,
        "paddr": this.user.businessAddress,
        "pphonebtype": "business phone",
        "pbphonenumber":this.user.businessPhone,
        "pphonectype":"cell phone",
        "pcphonenumber":this.user.cellPhone,
        "plicno": this.user.memberLicenseNo,
        "prole": 3
      }
      this.userEmailId=this.user.businessEmail
      //console.log(this.user);
      this.spinner.show();
      this.apiService.auditorSignUp(data).subscribe(response => {
        if(response._statusCode == 400){
          this.spinner.hide();
          this.toastr.error(response._statusMessage);
          // alert(response._statusMessage);
        }
        else{
          //alert('auditor created successfully');
          this.spinner.hide();
          this.toastr.success(response._statusMessage);
          this.signUpState = true
        }
        
      }, error => {
        this.spinner.hide();
        this.toastr.error('error in signup');
        // alert('error in signup')
      });
    
    }
    
  }

  public handlePhone (event, type) {
    const onlyNums = event.target.value.replace(/[^0-9]/g, '');
    if (onlyNums.length < 10) {
      if(type=='cell'){
        this.cellPhone.setValue(onlyNums); 
      }
      else if(type=='business'){
        this.businessPhone.setValue(onlyNums);
      }
      else{
        return;
      }
    } else if (onlyNums.length === 10) {
      const number = onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
      if(type=='cell'){
        this.cellPhone.setValue(number);
      }
      else if(type=='business'){
        this.businessPhone.setValue(number);
      }
      else{
        return;
      }
    }
  };

  public onlyChars(event, type){
    const noSpecialCHars = event.target.value.replace(/[^a-zA-Z ]/g, '');
    if(type=='fn'){
      this.firstName.setValue(noSpecialCHars); 
    }
    else if(type=='ln'){
      this.lastName.setValue(noSpecialCHars);
    }
    else if(type=="bn"){
      this.businessName.setValue(noSpecialCHars);
    }
    else{
      return;
    }
  }

  public aplphaNumericInput(event){
    const noSpecialCHars = event.target.value.replace(/[^a-zA-Z0-9]/g, '');
    this.memberLicenseNo.setValue(noSpecialCHars); 
  }




}
