import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  loginForms = false;
  isReadOnly: boolean = false;
  public selectedClient: any = {};
  constructor() { }
  public routeObj = new BehaviorSubject<any>({});
  public routeData = this.routeObj.asObservable();

  public headerObj = new BehaviorSubject<any>({});
  public headerDetails = this.headerObj.asObservable();

  public profileObj = new BehaviorSubject<any>({});
  public profileDetails = this.profileObj.asObservable();
  public selectedEnagagementObj=new BehaviorSubject<any>({});
  public selectedEngagementDetails=this.selectedEnagagementObj.asObservable();
  public userData: any;

  public gridData: any;

  public gridDataEngage: any;

  public selectedClientId: any;

  public filterEngagements: any
  public engagementId: any;

  public clientListCount = new Subject<string>();
  public engagemantListCount = new Subject<string>();
  public clientStausCount = new Subject<string>();
  public clientSearchString = new Subject<string>();

  public clientToken: any;

  public engagementDetails: any;
  public engagementName:any;
  public yearEnd:any;
  public trailBalYear:any;
  public balStatementEngagementName:any;
  public balStmtYearEnd:any;
  public tbslectedYear:any;
}
