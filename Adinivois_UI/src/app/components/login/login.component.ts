import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  token: any;
  isToken: boolean;
  urlStr: string;
    userData: any;
  constructor(
    public sharedService: SharedService, private fb: FormBuilder, 
    private apiService: ApiService, private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      if(this.token)
       this.urlStr =  this.token.replace(/ /g, "+");
      if(this.urlStr){
        this.verifyToken();
      }
  });
   }

  ngOnInit() {
    this.sharedService.loginForms = true;
    this.loginForm = this.fb.group({
      'emailId': new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'password': new FormControl("", [Validators.required]),
    });
    let user = JSON.parse((sessionStorage.getItem('userDetails')));
    if (user && user.useracctid) {
      this.router.navigate(['/dashboard']);
    }    
  }
  login(){
    if (this.loginForm.valid) {
      let user = this.loginForm.value;
      let data: any = {
        pemailadd: user.emailId,
        ppwd: user.password
      }

      this.apiService.loginAuditor(data).subscribe(response => {
        if(response){
          if (response._statusCode == 200 && response.data) {
            this.userData = response.data[0];
           

            this.sharedService.userData=this.userData.useracctid
          
            sessionStorage.setItem('userDetails', JSON.stringify(response.data[0]));
            this.router.navigate(['/dashboard']);
            //console.log(response.data[0]);
          }
          else{
            if (response._statusCode == 200 && !response.data) {
              this.toastr.error('Email Address does not exist');
              //alert('Email Address does not exist');
            }
            else {
              this.toastr.error('Password entered is wrong');
              //alert('Password entered is wrong');
            }
            
          }
        }
      }, error => {
        this.toastr.error('error in login');
        //alert('error in login')
      });
    }
  }
  ngOnDestroy() {
    this.sharedService.loginForms = false;
  }
  verifyToken(){
    if(this.urlStr){
    //  this.isToken=true;
    this.apiService.verifyToken(this.urlStr).subscribe(response =>{
      if (response._statusCode == 200) {
        this.toastr.success(response._statusMessage);
        this.router.navigate(['/active']);
       //this.isToken=true;     
      }
      else{
        //alert('client created successfully');
        this.toastr.error('link expired..');
        //alert( "link expired..");
      }
      
    }, error => {
      this.toastr.error('error occured');
    //alert("error occured");
    })
    }

  }

}
