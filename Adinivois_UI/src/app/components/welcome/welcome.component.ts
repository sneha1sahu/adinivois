import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {TooltipPosition} from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  isauditorSignup:boolean = false; 
  isLogin:boolean = false;
  isClientSignup:boolean = false;

  auditorLogin:boolean = false;
  clientLogin:boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    let user = JSON.parse((sessionStorage.getItem('userDetails')));
    if (user && user.useracctid) {
      this.router.navigate(['/dashboard']);
    }
  }
// tooltip position
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  auditorSignup(){
    this.isauditorSignup = true;
  }

  login(){

  }

}
