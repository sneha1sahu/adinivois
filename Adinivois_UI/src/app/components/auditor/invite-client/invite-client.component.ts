import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../shared/shared.service';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'invite-client',
  templateUrl: './invite-client.component.html',
  styleUrls: ['./invite-client.component.scss']
})
export class InviteClientComponent implements OnInit {

  token: any;
  urlStr: any;
  isToken: boolean = false;
  data: any;
  clientDetailsObj: any;
  clientResponse: any;
  accountBookSelected: any;
  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private router: Router,
    private toastr: ToastrService,
    private sharedService: SharedService,
    private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      if(this.token){
        this.urlStr = this.token.replace(/ /g, "+");
      }
    });
  }

  ngOnInit() {
    this.getClientDetails();
  }

  getClientDetails(){
    if(this.urlStr){
      this.apiService.getClientDetails(this.urlStr).subscribe(response => {
        if (response._statusCode == 200) {
          if(response && response.data){
            let clientDetails:any = response.data[0];
            this.clientDetailsObj = clientDetails;
          }
        }
      }, error => {

      })
    }
    
  }

  onResponseChange(){

  }

  connect(){
    if(this.accountBookSelected == 'quickbooks'){
      let connectionUrl = environment.apiEndpoint + '/quickbook/connect?tokenValue=' + this.urlStr;
      window.open(connectionUrl,"_self")
    }
    else if(this.accountBookSelected == 'xero'){
      let connectionUrl = environment.apiEndpoint + '/xero/connect?tokenValue=' + this.urlStr;
      window.open(connectionUrl,"_self")
    }
  }

  sendResponse(){
    if(this.clientResponse == 'no'){
      this.rejectOrNoAccount(6);
    }
    else{
      this.rejectOrNoAccount(10);
    }
  }
  //Inviting client
  public acceptInvite() {
    this.sharedService.clientToken = this.urlStr;
    this.router.navigate(['/accountbooks']);
    // this.data = {
    //   "tokenkey": this.urlStr,
    //   "pstatusid": 5
    // }
    //   if (this.urlStr) {
    //     this.apiService.inviteClientVerifyToken(this.data).subscribe(response => {
    //       if (response._statusCode == 200) {
    //         this.isToken = true;
    //         this.sharedService.clientToken = this.urlStr;
    //         this.router.navigate(['/accountbooks']);
    //         this.toastr.success(response._statusMessage);
    //         this.sharedService.clientListCount.next()
            
    //       }
    //       else {
    //         this.toastr.error("link expired..");
    //         this.router.navigate(['/']);
    //       }

    //     }, error => {
    //       this.toastr.error("error occured");
    //     })
    //   }

  }

  //Rejected client
  public rejectOrNoAccount(statusid) {
    this.data = {
      "tokenkey": this.urlStr,
      "pstatusid": statusid
    }
    if (this.urlStr) {
      //  this.isToken=true;
      this.apiService.inviteClientVerifyToken(this.data).subscribe(response => {
        //this.isToken = true;
        if (response._statusCode == 200) {
          this.isToken = true;
          this.toastr.success("Response sent to Auditor");
          this.router.navigate(['/'])
        }
        else {
          //alert('client created successfully');
          this.toastr.error("link expired..");
          //alert("link expired..");
          this.router.navigate(['/'])
        }

      }, error => {
        this.toastr.error("error occured");
        //alert("error occured");
      })
    }

  }

}
