import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild, ElementRef, Renderer2  } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import {MatPaginator, MatSort, MatTableDataSource, MatMenuTrigger, MatDialog, MatDialogRef, MAT_DIALOG_DATA, Sort, MatOption} from '@angular/material';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';
import { resolveComponentResources } from '@angular/core/src/metadata/resource_loading';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelService } from 'src/app/services/excel.service';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { filter } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

export interface trailBalanceData {
  trailbalanceid:number;
  accountcode: number;
  accountname: string;
  originalbalance: number;
  accountactions: any;
  adjustmentamount: number;
  finalamount: number;
  py1: number;
  py2: number;
  changes: number;
  mapno: any;
  leadsheetCode: any;
  fingroupname: any;
  finsubgroupname: any;
  gificode: any;
  fxtranslation: number;
  actions: string;
  mappingstatus: string;
  isclientdatasource:any;
}
export interface adjustmentData {
  accNum: number;
  decription: string;
  type: string;
  debit: number;
  credit: number;
  actions: string;
}
// const AdjustmentData: adjustmentData[] = [
//   {
//     accNum: 1, decription: 'Accounts Receivable', type: "Normal", debit: 20000, credit: 7000,  actions:""
//   }
// ];

@Component({
  selector: 'trailbalance-details',
  templateUrl: './trailbalance-details.component.html',
  styleUrls: ['./trailbalance-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class TrailbalanceDetailsComponent implements OnInit {

  isTrailBalanceGrid: boolean = false;
  istrailBalancePreview: boolean = false;

  //@ViewChild('adjustmentDialog') adjustmentDialog: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('deleteAdjustedTrialBalDialog') deleteAdjustedTrialBalDialog: TemplateRef<any>;
  @ViewChild('content') content: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('allSelected') private allSelected: MatOption;
  engmtID: any;
  trialBalDetails: any;

  tbYear = new FormControl();
  tbYearList: string[] = ['2016', '2017', '2018'];
  tbRows = new FormControl();
  tbRowsList: string[] = ['row1', 'row2', 'row3'];
  trialBalId: any;
  loggedInUserData: any;
  auditorId: any;
  adjustingFlyOut: boolean = false;
  postAdjustingEntryFlyOut: boolean = false;
  isSaved: boolean = false;
  mappingStatusDetails: any;
  EngagementYearDetails: any;
  filterTrailBal=[];
  trailBalYear: any;
  tbslectedYear: any;
  hidePagination: any = true;
  gridDataTrailBal: MatTableDataSource<unknown>;
  trailBalanceDetailsCopy: any;
  originalBalTotal: any;
  finalTotal: any;
  ajustmentTypeDetails: any;
  adjustmenttypeid: any;
  AdjustmentDataGridData: any[];
  debit:any;
  credit:any;
  engagementsid: any;
  trailbalanceid: any;
  allAdjustmentEntries: any;
  isEnableBlanceStatement: boolean;
  sortedData: any;
  trialBalDetailsCopy: any;
  filertTrailBalForm:FormGroup
  originalred: any;
  finalred: any;
  previousYear: any;
  engagementID: string;
  selectedObj: any;
  tbGridData: any;
  showSaveCancel: any = false;
  constructor(private apiService: ApiService, private sharedService: SharedService,
    public router: Router, public dialog: MatDialog, private toastr: ToastrService,
    private spinner: NgxSpinnerService, private excelService: ExcelService, private loc: Location, 
    private datePipe: DatePipe,
    private fb: FormBuilder, private renderer: Renderer2) {
  }
  selected = 'option1';

  ngOnInit() {
    this.selectedObj= JSON.parse(localStorage.getItem("selectedEngagementObj"))
    this.engagementID= localStorage.getItem('engagementID')  
   // let selectedEngagement = this.sharedService.engagementDetails;
    // if(selectedEngagement && selectedEngagement.statusid == 9){
    //     this.refreshTrialBalanceData();
    // }
    if(this.selectedObj && this.selectedObj.statusid == 9){
      this.refreshTrialBalanceData();
  }
    else{
      this.getEngagementYearDetails();
    }
    
    this.getMappingStatus();
    // if(!this.sharedService.engagementId){
      if(!this.engagementID){
     this.router.navigate(['/dashboard/engagement']);
    }
    else{
     // this.gettrailBalance();
    }
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.getAdjustmentType();
  //   var flags = {};
  //  // let tempData = this.trialBalDetails;
  // //  let vm = this;
  //   this.trialBalDetails.map(function (entry) {
  //     if (flags[entry.accountcode]) {
  //       this.trialBalDetails.map(function (elem) {
  //         if (entry.accountcode === elem.accountcode) {
  //           elem["duplicate"] = true;
  //         }
  //       })
  //     }
  //     flags[entry.accountcode] = true;
  //     return true;
  //   });
  this.filertTrailBalForm = this.fb.group({
    tbRows: new FormControl('')
  });
  }
  //displayedColumns: string[] = ['accNum', 'decription', 'original', 'adjusting', 'final', 'py1', 'change', 'mapNum', 'leedSheet', 'grouping', 'subGrouping', 'gifiCode', 'fxTransaction', 'actions'];
  displayedColumns: string[] = ['accountactions','accountcode', 'accountname', 'originalbalance', 'adjustmentamount', 'finalamount', 'py1', 'changes','py2', 'mapno', 'leadsheetcode', 'fingroupname', 'finsubgroupname', 'gificode', 'fxtranslation'];
 // trailBalanceGridData = ELEMENT_DATA; 
  AdjustmentColumns: string[] = ['accNum', 'decription', 'type', 'debit', 'credit', 'actions'];
  allAdjustmentEntiresColumns: string[] = ['inaccountcode', 'inaccountname', 'inacctdebit', 'inacctcredit', 'inadjustmenttypeid', 'actions'];
  //AdjustmentDataGridData = AdjustmentData;
  importTrailBalance() {

  this.isTrailBalanceGrid = true;
  }
  Adjustment(): void {
    this.adjustingFlyOut = true;
    var engmetid=10
    this.apiService.getAlltrailAdjustment(engmetid).subscribe(response=>{
if(response['_statusCode']==200){
this.allAdjustmentEntries=response['data'];
}
else{
  this.toastr.warning("fail to load adjustment entries");
}
    })
  }
  closeAdjustment() {
    if (this.adjustingFlyOut) {
      this.adjustingFlyOut = false;
      this.credit=0.00;
      this.debit=0.00;
    }
    if (this.postAdjustingEntryFlyOut) {
      this.postAdjustingEntryFlyOut = false;
      this.credit=0.00;
      this.debit=0.00;
    }
    
  }

  clickRow(elem) {

    let length = this.checkPendingChanges();
    if(length > 0 && (!elem.isDupliacate && !elem.isAdded)){
      this.toastr.warning('save the row');
    }
    else{
      this.trialBalDetails.forEach(element => {
        element.isSelected = false;
      });
      elem.isSelected = true;
    }

    
  }

  createDuplicate(item, index){
    let length = this.checkPendingChanges();
    if(length > 0){
      this.toastr.warning('save the row');
    }
    else{
      this.sort.direction = '';
      this.sort.active = 'name';
      this.trialBalDetails = JSON.parse(JSON.stringify(this.trailBalanceDetailsCopy));
      this.bindGrid(this.trialBalDetails);
      let obj: any = JSON.parse(JSON.stringify(item));
      let indx = 0;
      obj.isDupliacate = true;
      obj.isSelected = true;
      obj.isEditable = true;
      obj.isclientdatasource = false;
      obj.accountcode = (obj.accountcode || obj.accountcode == 0) ? parseFloat(obj.accountcode) : '';
      obj.originalbalance = obj.originalbalance ? obj.originalbalance.replace(/\,|\)/g, '').replace(/\(/g, '-') : '';
      obj.originalbalance = obj.originalbalance ? parseFloat(obj.originalbalance) : '';
      obj.py1 = obj.py1 ? obj.py1.replace(/\,|\)/g, '').replace(/\(/g, '-') : null;
      obj.py1 = obj.py1 ? parseFloat(obj.py1) : null;
      obj.py2 = obj.py2 ? obj.py2.replace(/\,|\)/g, '').replace(/\(/g, '-') : null;
      obj.py2 = obj.py2 ? parseFloat(obj.py2) : null;
      this.trialBalDetails.forEach((row, index )=> {
        if(row.trailbalanceid == item.trailbalanceid){
          indx = index;
          row.isSelected = false;
          row.isOriginal = true;
        }
      });
      this.trialBalDetails.splice(indx+1, 0, obj);
      item.isSelected = false;
      item.isOriginal = true;
      this.bindGrid(this.trialBalDetails);
      this.showSaveCancel = true;
      let id: any = indx+1;
      id = '#elem' + id;
      setTimeout(() => {
        this.renderer.selectRootElement(id, true).focus();
      }, 0);
      
    }
    
  }
  addRow(item, index){
    let length = this.checkPendingChanges();
    if(length > 0){
      this.toastr.warning('save the row');
    }
    else{
      this.sort.direction = '';
      this.sort.active = 'name';
      this.trialBalDetails = JSON.parse(JSON.stringify(this.trailBalanceDetailsCopy));
      this.bindGrid(this.trialBalDetails);
      let indx = 0;
      let obj:any = {};
      obj = {
        accountcode: "",
        accountname: "",
        accounttype: null,
        adjustmentamount: "",
        adjustmentamounttotal: "",
        adjustmenttypeid: null,
        adjustmenttypename: null,
        balancetypeid: 2,
        balancetypename: "",
        changes: "",
        clientapidataid: null,
        clientdatasourceid: null,
        finalamount: "",
        finalamounttotal: "",
        finalred: "",
        fingroupid: 1,
        fingroupname: "",
        finsubgroupchild: 1,
        finsubgroupchildname: "",
        finsubgroupname: "",
        finsubgrpid: 1,
        fxtranslation: "",
        gificode: 0,
        leadsheetcode: "",
        leadsheetid: 1,
        leadsheetname: "",
        mapno: "",
        mappingstatus: "",
        mappingstatusid: 0,
        mapsheetid: null,
        mapsheetname: "",
        notes: "",
        originalbalance: "",
        originalbalancetotal: "",
        originalred: "",
        py1: "",
        py1total: "",
        py2: "",
        py2total: "",
        recieveddate: 0,
        statementtypeid: 0,
        statementtypename: "0",
        trailadjustmentid: null,
        trailbalanceid: null,
        trailbalancemapingid: 0,
        isAdded: true,
        isSelected: true,
        isclientdatasource: false,
        isEditable: true
      }
      obj.engagementsid = item.engagementsid;
      this.trialBalDetails.forEach((row, index )=> {
        if(row.trailbalanceid == item.trailbalanceid){
          indx = index;
          row.isSelected = false;
          row.isOriginal = true;
        }
      });
      this.trialBalDetails.splice(indx+1, 0, obj);
      item.isSelected = false;
      item.isOriginal = true;
      this.bindGrid(this.trialBalDetails);
      this.showSaveCancel = true;
      let id: any = indx+1;
      id = '#elem' + id;
      setTimeout(() => {
        this.renderer.selectRootElement(id, true).focus();
      }, 0);
    } 
  }

  deleteRow(row, index){
    this.trialBalDetails.splice(index, 1);
    this.bindGrid(this.trialBalDetails);
    if(!row.isAdded && !row.isDupliacate){
      this.spinner.show();
      this.apiService.deleteTrialBalanceRow(row.trailbalanceid, this.auditorId).subscribe(response =>{
        this.spinner.hide();
        this.toastr.success('Deleted successfully');
        this.gettrailBalance(this.tbslectedYear);
      }, error =>{
        this.spinner.hide();
        this.toastr.error('Error in deleting');
      })
    }
    else{
      if(index > 0 ){
        this.trialBalDetails[index-1].isOriginal = false;
      }
      this.showSaveCancel = false;
    }
    
  }

  numbersOnly(event, item){
    //let value = event.replace(/[^0-9]/g, '');
    let value = event.target.value.replace(/[^0-9]/g, '');
    item.accountcode = value;
    this.bindGrid(this.trialBalDetails);
  }
  

  saveRow(){
    let duplicateOrAddedRows = this.trialBalDetails.filter((item) =>{
      return item.isDupliacate || item.isAdded;
    })
    if(duplicateOrAddedRows && duplicateOrAddedRows.length > 0){
      let row:any = duplicateOrAddedRows[0];
      if((row.accountcode === '' || row.accountcode === null) || (row.originalbalance === '' || row.originalbalance === null)  || (row.accountname === '' || row.accountname === null)){
        if(row.accountcode === '' || row.accountcode === null){
          this.toastr.error('Acc.No is required')
        }
        else if(row.accountname === '' || row.accountname === null){
          this.toastr.error('Description is required')
        }
        else if(row.originalbalance === '' || row.originalbalance === null){
          this.toastr.error('Original is required')
        }
        
      }
      else{
        let trialbalanceid = row.isAdded ? null : row.trailbalanceid;
        row.accountcode = row.accountcode + '';
        row.originalbalance = row.originalbalance + '';
        row.originalbalance = row.originalbalance ? row.originalbalance.replace(/\,|\)/g, '').replace(/\(/g, '-') : '';
        row.py1 = row.py1 ? row.py1 + '' : null;
        row.py1 = row.py1 ? row.py1.replace(/\,|\)/g, '').replace(/\(/g, '-') : null;
        row.py2 = row.py2 ? row.py2 + '' : null;
        row.py2 = row.py2 ? row.py2.replace(/\,|\)/g, '').replace(/\(/g, '-') : null;
        
        let data = {
          "intrailbalanceid": trialbalanceid,
          "engagementsid": row.engagementsid,
          "inacctyear": this.tbslectedYear,
          "inaccountcode": row.accountcode ? row.accountcode : null,
          "inaccountname": row.accountname ? row.accountname : null,
          "inoriginalbalance": row.originalbalance,
          "inPY1": row.py1,
          "inPY2": row.py2,
          "inmapsheetid": row.mapsheetid ? row.mapsheetid : null,
          "ploginid": this.auditorId
          }
        this.spinner.show();
        this.apiService.saveTrialBalanceRow(data).subscribe(response =>{
          this.spinner.hide();
          this.showSaveCancel = false;
          this.toastr.success('Saved successfully');
          this.trialBalDetails.forEach(element => {
            element.isSelected = false;
            element.isDupliacate = false;
            element.isAdded = false;
            element.isEditable = false;
            element.isOriginal = false;
          });
          this.gettrailBalance(this.tbslectedYear);
        }, error =>{
          this.spinner.hide();
          this.toastr.error('Error in saving');
        })
      }
      
    }
    
  }

  cancelSaveRow(){
    this.showSaveCancel = false;
    let savedRows = this.trialBalDetails.filter((item) =>{
      return !item.isDupliacate && !item.isAdded;
    })
    this.trialBalDetails = savedRows;
    this.trialBalDetails.forEach(element => {
      element.isSelected = false;
      element.isDupliacate = false;
      element.isAdded = false;
      element.isEditable = false;
      element.isOriginal = false;
    });
    this.bindGrid(this.trialBalDetails);
  }


  checkPendingChanges(){
    let length = 0;
    let changedRows = this.trialBalDetails.filter((item) =>{
      return item.isDupliacate || item.isAdded;
    })
    if(changedRows){
      length = changedRows.length;
    }
    return length;
  }

  trialBalanceYear(event){
    this.trailBalYear=event.value;
    this.tbslectedYear= this.trailBalYear
    this.sharedService.trailBalYear=this.trailBalYear
    this.gettrailBalance(this.trailBalYear);
    this.previousYearDetails(this.tbslectedYear);
  }
  previousYearDetails(selectedyear){
    let yearIndex;
    this.EngagementYearDetails.map((resp,i)=>{
      if(selectedyear==resp.acctyear){
        yearIndex = i
        console.log("index"+yearIndex);
      }
    })
    if(yearIndex && yearIndex != this.EngagementYearDetails.length - 2){
      this.previousYear = this.EngagementYearDetails[yearIndex - 1].acctyear;
      localStorage.setItem('PreviousYear',this.previousYear)
      console.log("previous yea"+this.previousYear)
    }
    else if(yearIndex==0){
      this.previousYear='';
      localStorage.setItem('PreviousYear',this.previousYear)
      console.log("previous year for zero"+this.previousYear)
    }
  }
  bindGrid(list){
    this.tbGridData = new MatTableDataSource(list);
   // this.sharedService.gridDataTrailBal = this.gridDataTrailBal;
    this.tbGridData.sort = this.sort;
    this.tbGridData.paginator = this.paginator;
    this.hidePagination = this.tbGridData.length > 10 ? false : true;
  }
  gettrailBalance(year){
   // var year=this.trailBalYear
    //var year=this.tbslectedYear
    if(year){
      localStorage.setItem('trailBalYear',year)
    }
   var engagementID= localStorage.getItem('engagementID')  //removed shared service id
  this.spinner.show();
    this.apiService.gettrailBalance(engagementID,year).subscribe(x=>{
    //  this.isTrailBalanceGrid=true;
    this.spinner.hide();
if(x['_statusCode']==200){
  this.trialBalDetails=x['data'];
  this.bindGrid(this.trialBalDetails)
  this.originalBalTotal=this.trialBalDetails[0].originalbalancetotal
  this.finalTotal=this.trialBalDetails[0].finalamounttotal
  this.originalred=this.trialBalDetails[0].originalred
  this.finalred=this.trialBalDetails[0].finalred

  this.trailBalanceDetailsCopy = JSON.parse(JSON.stringify(this.trialBalDetails));
this.tbRows.reset();
console.log(this.trialBalDetails);
var trailBalLength=(this.trialBalDetails).length
var mappingstatuscount=0;
var mappingStatusLength=this.trialBalDetails.map(response=>{
  
  if(response.mappingstatusid==1){
    mappingstatuscount=mappingstatuscount+1
  }
})
if(mappingstatuscount===trailBalLength){
this.isEnableBlanceStatement=true
}
else{
  this.isEnableBlanceStatement=false
}
console.log(trailBalLength)
console.log('mappin status lenght'+mappingstatuscount)
  //trailBalanceGridData=this.trialBalDetails
}
else{
  this.isTrailBalanceGrid=true;
  //this.toastr.warning("no trail bal for engament");
   }
    }, error =>{
      this.spinner.hide();
    })

  }

  deleteAdjustedTrialBal(row, index){    
    if(row.isAdded || row.isDupliacate){
      this.trialBalDetails.splice(index, 1);
      this.bindGrid(this.trialBalDetails);
      if(index > 0 ){
        this.trialBalDetails[index-1].isOriginal = false;
      }
      this.showSaveCancel = false;
    }
    else{
      let length = this.checkPendingChanges();
      if(length > 0){
        this.toastr.warning('save the row');
      } 
      else{
        this.trialBalId= row.trailbalanceid;
        if(this.trialBalId){
          const dialogRef = this.dialog.open(this.deleteAdjustedTrialBalDialog);
          dialogRef.afterClosed().subscribe(result => {
          });
        }
      }
    }

    

  }
  confirmDeleteAdjustedTrialBal(){
    const dialogRef = this.dialog.closeAll();
    this.spinner.show();
    this.apiService.deleteAdjustedTrialBal(this.trialBalId,this.auditorId).subscribe(response=>{
      if(response['_statusCode'] == 200){
        this.toastr.success("Deleted successfully");
        this.gettrailBalance(this.tbslectedYear);
      }
      else{
        this.toastr.warning("Error in deleting");
      }
    })
  }
  getMappingStatus(){
    this.apiService.getMappingStatus().subscribe(response=>{
      if(response['_statusCode'] == 200){
        this.mappingStatusDetails=response['data'];
        console.log( this.mappingStatusDetails)
      }
      // else{
      //   this.toastr.warning("trail bal delete failed");
      // }
      })
  }
  getEngagementYearDetails(){
 //  var id= this.sharedService.engagementId
   if(this.engagementID){
    localStorage.setItem("engagementID", this.engagementID); 
   }
    this.apiService.getEngagementYearDetails(this.engagementID).subscribe(response=>{
      if(response['_statusCode'] == 200){
        this.EngagementYearDetails=response['data'];
        console.log(this.EngagementYearDetails);
        if(this.EngagementYearDetails && this.EngagementYearDetails.length > 0){
         let index = this.EngagementYearDetails.length - 1;
          this.tbslectedYear= this.EngagementYearDetails[index].acctyear;
          this.gettrailBalance(this.tbslectedYear);
          this.previousYearDetails( this.tbslectedYear);
        }
      
     //  console.log("selected year: "+yearsel);
        console.log( this.EngagementYearDetails)
      }
      else if(response['_statusCode'] == 404){
        localStorage.setItem('trailBalYear','')
        this.toastr.warning("no data available");
        this.isTrailBalanceGrid=true;
      }
      })
  }
  //

  tosslePerOne(all){ 
    if (this.allSelected.selected) {  
     this.allSelected.deselect();
     return false;
  }
   if(this.tbRows.value.length==this.mappingStatusDetails.length)
     this.allSelected.select();
  }

  toggleAllSelection() {
    if (this.allSelected.selected) {
      this.tbRows
        .patchValue([...this.mappingStatusDetails.map(item => item.mappingStatusId), 0]);
        this.filterTrailBal = this.trailBalanceDetailsCopy;
        this.bindGrid(this.filterTrailBal);
    } else {
      this.tbRows.patchValue([]);
    }
  }


  trialBalanceFilter(e, selected){
   var dta= this.filertTrailBalForm.value
    var sel=selected[0]
    var arra=[];
    
    
    // if(sel==6){
    
      
    // }
    var tempStatus = JSON.parse(JSON.stringify(this.trailBalanceDetailsCopy));
    this.filterTrailBal = [];
    if(selected && selected.length > 0){
      selected.forEach(item => {
        const myfiltarray=tempStatus.filter((x,i)=>{
          if(x.mappingstatusid==item){
            this.filterTrailBal.push(x)
          }
        })
        })
    }
    else{
      this.filterTrailBal = this.trailBalanceDetailsCopy;
    }
    if(sel==6){
      this.filterTrailBal = this.trailBalanceDetailsCopy;
    }
    this.bindGrid(this.filterTrailBal);
  }
  clearAllstatus(){
    this.tbRows.patchValue([]);
    this.filterTrailBal = this.trailBalanceDetailsCopy;
    this.bindGrid(this.filterTrailBal);
  }
  //
  exportToExcel(){
  //  let engagementDetails: any = this.sharedService.engagementDetails;
    let engagementDetails: any = this.selectedObj
   // let yearEndDate = this.sharedService.balStmtYearEnd;
    let yearEndDate = this.selectedObj.yearEnd
   
    let reportDate = '';
    if(yearEndDate){
      let endDate = this.datePipe.transform(yearEndDate, "d MMMM, ");
      reportDate = endDate + this.tbslectedYear;
    }
    var finalTrailBalDetails=[...this.trialBalDetails, //Group total is added in excel sheet
                             {'accountname':'Total',
                              'originalbalance':this.originalBalTotal,
                              'finalamount':this.finalTotal
                             }]
    this.excelService.exportToExcel(finalTrailBalDetails, engagementDetails, reportDate);
  }
  exportToPdf(){
   // let engagementDetails: any = this.sharedService.engagementDetails;
    let engagementDetails: any = this.selectedObj;
    
    let engagementName = engagementDetails ? engagementDetails.engagementName : '';
    //let yearEndDate = this.sharedService.balStmtYearEnd;
    let yearEndDate = this.selectedObj.yearEnd;
    let reportDate = '';
    if(yearEndDate){
      let endDate = this.datePipe.transform(yearEndDate, "d MMMM, ");
      reportDate = endDate + this.tbslectedYear;
    }
    
    var doc = new jsPDF('landscape');

    doc.setFont('helvetica')
    doc.setFontType('bold')
    doc.setFontSize(14)
    doc.text('Engagement Name: ' + engagementName, 140, 30, {align:'center'})

    doc.setFont('helvetica')
    doc.setFontType('normal')
    doc.setFontSize(12)
    doc.text('As of ' + reportDate, 140, 40, {align:'center'})

    doc.setFont('helvetica')
    doc.setFontType('bold')
    doc.setFontSize(16)
    doc.text('Trial Balance', 140, 50, {align:'center'})

    var columns = [
      {title: "Acc.No", dataKey: "accountcode"},
      {title: "Description", dataKey: "accountname"},
      {title: "Original", dataKey: "originalbalance"},
      {title: "Adjusting", dataKey: "adjustmentamount"},
      {title: "Final", dataKey: "finalamount"},
      {title: "PY1", dataKey: "py1"},
      {title: "Change", dataKey: "changes"},
      {title: "PY2", dataKey: "py2"},
      {title: "Map No", dataKey: "mapno"} ,
      {title: "Leadsheet", dataKey: "leadsheetcode"},
      {title: "Grouping", dataKey: "fingroupname"},
      {title: "Sub Grouping", dataKey: "finsubgroupname"},
      {title: "GIFI Code", dataKey: "gificode"},
      {title: "FX Translation", dataKey: "fxtranslation"},
      {title: "Mapping Status", dataKey: "mappingstatus"} 
      ]
let newTrialBalDetails=[...this.trialBalDetails,
                        {
                        'accountname':'Total',
                        'originalbalance':this.originalBalTotal,
                        'finalamount':this.finalTotal
                       }
                      ] 
      doc.autoTable(columns, newTrialBalDetails,{
        margin: {horizontal:5},
        startY: 60,
        styles: {
          // overflow: 'linebreak',
          fontSize: 5,
          // rowHeight: 60,
          // columnWidth: 'wrap'
        },
     
    },
   );
    doc.save('trailbalance.pdf');
  }

  trailBalancePreview() {
    const angularRoute = this.loc.path();
    const url = window.location.href;
    const domainAndApp = url.replace(angularRoute, '');
    const newWindowUrl = domainAndApp + '/dashboard/prviewreport';
    window.open(newWindowUrl, "_blank", 'location=yes,height=900,width=1200,scrollbars=yes,status=yes');
    var endYear=localStorage.getItem('yearEnd');
    localStorage.setItem('endYear',endYear)
  }
 
//Adjustment Entry
getAdjustmentType(){
  //var id= this.sharedService.engagementId
 // var year=parseInt(localStorage.getItem('trailBalYear'))
  this.apiService.getAdjustmentType().subscribe(response=>{
    if(response['data']!=null){
this.ajustmentTypeDetails=response['data']
this.adjustmenttypeid=this.ajustmentTypeDetails[0]['adjustmenttypeid'];
console.log("adjustmenttypeid:-"+this.adjustmenttypeid)
    }
    else{
      this.toastr.warning("fail to load adjustment type");
    }
  })
}
adjustmentType(event){
var id =event.value
this.adjustmenttypeid=id
}
savePostAdjustingEntry(data) {
  var finaldata={
    'intrailbalanceid':this.trailbalanceid,
    'inaccountcode':parseInt(data.accNum),
    'inaccountname':data.decription,
    'inacctcredit':this.credit,
    'inacctdebit':this.debit,
    'inadjustmenttypeid':this.adjustmenttypeid,
    'intrailadjustmentid':0,
    'ploginid':this.engagementsid,
    "inisdelete":0
  }
  this.apiService.saveTrialBalanceAdjustment(finaldata).subscribe(response=>{
    if(response["_statusCode"]==200){
      this.toastr.success("adjusment entry save successfully");
  //    this.sharedService.trailBalYear=this.trailBalYear
      console.log(this.sharedService.trailBalYear);
      this.gettrailBalance(this.tbslectedYear);
    }
    else{
      this.toastr.warning("save adjusment entry failed");
    }
  })
  this.isSaved = true;
}
postAdjustmentEntry(element): void { 
  this.trailbalanceid=element.trailbalanceid
  this.engagementsid=element.engagementsid
  this.credit=element.adjustmentamount;
  this.debit=0.00;
  this.AdjustmentDataGridData=[{'accNum':element.accountcode,'decription':element.accountname}];
//  element.accountcode: " 4 "
//element.accountname: " Undeposited Funds 
  console.log(element);
  this.postAdjustingEntryFlyOut = true;
}
sortData(sort: Sort) {
  const data = (this.trialBalDetails).slice();
 // this.trialBalDetailsCopy=data
  if (!sort.active || sort.direction === '') {
    this.trialBalDetails = this.trailBalanceDetailsCopy; //copy of trailbalance from get call 
    return;
  }
  this.sortedData = data.sort((a, b) => {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'accountcode': return this.compare(a.accountcode, b.accountcode, isAsc);
      case 'accountname': return this.compare(a.accountname, b.accountname, isAsc);
      case 'originalbalance': return this.compare(a.originalbalance, b.originalbalance, isAsc);
      case 'adjustmentamount': return this.compare(a.adjustmentamount, b.adjustmentamount, isAsc);
      case 'finalamount': return this.compare(a.finalamount, b.finalamount, isAsc);
      case 'py1': return this.compare(a.py1, b.py1, isAsc);
      case 'changes': return this.compare(a.changes, b.changes, isAsc);
      case 'py2': return this.compare(a.py2, b.py2, isAsc);
      case 'mapno': return this.compare(a.mapno, b.mapno, isAsc);
      case 'leadsheetcode': return this.compare(a.leadsheetcode, b.leadsheetcode, isAsc);
      case 'fingroupname': return this.compare(a.fingroupname, b.fingroupname, isAsc);
      case 'finsubgroupname': return this.compare(a.finsubgroupname, b.finsubgroupname, isAsc);
      case 'gificode': return this.compare(a.gificode, b.gificode, isAsc);
      case 'fxtranslation': return this.compare(a.fxtranslation, b.fxtranslation, isAsc);
      default: return 0;
    }
  });
 this.trialBalDetails= this.sortedData 
}
 compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  refreshTrialBalanceData(){
    let engagementID = this.sharedService.engagementId;
    this.spinner.show();
    this.apiService.refreshTrialBalance(engagementID).subscribe(response =>{
      this.spinner.hide();
      this.getUpdatedEngagementDetails();
    },error =>{
      this.toastr.error('Error in refreshing Trial Balance');
    })
  }

  getUpdatedEngagementDetails(){
    var currentEngagementId= this.sharedService.engagementId
    this.apiService.getAllengagement(this.auditorId).subscribe(response=>{
      if (response._statusCode == 200) { 
        let engagementData=response['data'];
        let filteredEngagement = engagementData.filter(item =>{
          return item.engagementsid == currentEngagementId;
        })
        let updatedEngagementData = filteredEngagement[0];
        if(updatedEngagementData){
          if(updatedEngagementData.tbloadstatusid == 12){
            this.isTrailBalanceGrid = false;
            this.getEngagementYearDetails();
          }
          else if(updatedEngagementData.tbloadstatusid == 13){
            this.toastr.error('Error in getting trial balance details from your account book');
            this.isTrailBalanceGrid=true;
          }
          else if(updatedEngagementData.tbloadstatusid == 14){
            this.toastr.warning('No Data');
            this.isTrailBalanceGrid=true;
          }
        }
      }
    }, error =>{

    });
  }
}
