import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'compilation-procedures',
  templateUrl: './compilation-procedures.component.html',
  styleUrls: ['./compilation-procedures.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompilationProceduresComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
