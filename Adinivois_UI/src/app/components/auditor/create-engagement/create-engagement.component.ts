import { Component, ViewEncapsulation, OnInit, Inject, TemplateRef, ViewChild} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../../services/api.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { SharedService } from '../../shared/shared.service';
import { Observable} from 'rxjs';
import { map, startWith } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'create-engagement',
  templateUrl: './create-engagement.component.html',
  styleUrls: ['./create-engagement.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class CreateEngagementComponent implements OnInit {

  animal: string;
  name: string;

  clientDetails: boolean = true;
  assignAuditors: boolean = false;
  priviewEngagementDetails: boolean = false;
  isReadOnly:boolean=false;
  clientDetailsForm:FormGroup;
  assignAuditorForm:FormGroup;
  @ViewChild('addEngagementDialog') addEngagementDialog: TemplateRef<any>;
  @ViewChild('updateEngagementDialog') updateEngagementDialog: TemplateRef<any>;
  @ViewChild('inviteClientDialog') inviteClientDialog: TemplateRef<any>;
  @ViewChild('WatchoutforResponseDialog') WatchoutforResponseDialog: TemplateRef<any>;
  dialogRef: any;
  clientDetailsObj : any = {};
  assignAuditorObj:any={};
  auditorDetails: any;
  auditorRoleDetails: any;
  preparer: any;
  reviewer: any;
  Admin: any;
  accountIDRoleList:string='';
  selectedClientData: any;
  BusinessDetails: any = [];
  isSaveBtnText:boolean=true;
  roleName: string;
  filterAuditorList: any;
  List:any;
  additionalInformation:any;
  Puseraccountid1:any;
  Proleid1:any;
  seq1: number;
  seq2: number;
  seq3: number;
  engagementDetails: any;
  Pclientname: any;
  PincorporationDate: any;
  pengagementtypeid: any;
  Puseraccountid2: any;
  Proleid2: any;
  Puseraccountid3: any;
  Proleid3: any;
  engagementNameTitle: any;
  BusinessName: any;
  Preparer:any;
  firstReviewer:any;
  secondReviewer:any;
  rowData;
  index: number;
  newAuditorList=[];
  audtName: any[];
  AdminName: string;
  incorporationdate: any;
  msg: string;
  isDuplicate: boolean=false;
  tblecolumn: any;
  engagementTitle: any;
  selectedRoleName: any;
  isIncrptDate: boolean=false;
  engagementId: any;
  loggedInUserData: any;
  auditorId: any;
  isEnableNext: boolean=true;
  myControl = new FormControl();
  filteredOptions: Observable<any>;
  isClientError: any = false;
  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private apiService: ApiService,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private router:Router,
    private sharedService:SharedService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
   }

  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.clientDetailsForm = this.fb.group({
    'Pengagementname': new FormControl("", [Validators.required]),
    'Pclientname': new FormControl("", [Validators.required]),
    'Psubenties': new FormControl(),
    'pengagementtypeid': new FormControl("", [Validators.required]),
    'Pyearend': new FormControl("", [Validators.required]),
    'Pcompliation': new FormControl("", [Validators.required]),
    'PincorporationDate': new FormControl("", Validators.required)
    });
    this.assignAuditorForm = this.fb.group({
      'additionalInformation': new FormControl("")
    });
    this.pengagementtypeid=1 //Notice to reader hard coded on OnInit
    this.getAllAuditor();
    this.getAuditorRole();
    this.getBusinessDetails();
    this.getEngagement();
   if(this.router.url === '/dashboard/editengagement'){
      this.isSaveBtnText =false;
      this.selectedClientData = this.sharedService.selectedClient;
      if(this.selectedClientData && this.selectedClientData.clientfirmid){
       // this.createEngagementForClient();
       this.Pclientname= this.selectedClientData.clientfirmid;
       this.PincorporationDate=this.selectedClientData.incorportiondate
       this.clientDetailsForm.controls.Pclientname.disable();
       this.editEngagement();
      }
    }
    else{
      this.selectedClientData = this.sharedService.selectedClient;
      if(this.selectedClientData && this.selectedClientData.clientfirmid){
        this.isReadOnly= true;
        this.Pclientname= this.selectedClientData.clientfirmid;
        this.PincorporationDate=this.selectedClientData.incorportiondate
        this.clientDetailsForm.controls.Pclientname.disable();
      }
      else{
        this.isReadOnly=false;
        this.clientDetailsForm.controls.Pclientname.enable();
      }
    }
    if(this.router.url === '/dashboard/createengagement'){
      this.clientDetailsForm.reset();
      this.selectedClientData = this.sharedService.selectedClient;
      this.clientDetailsForm.patchValue({
        Pclientname: this.selectedClientData.clientfirmid, 
        PincorporationDate: this.selectedClientData.incorporationdate
      });
 
      
      this.assignAuditorForm.reset();
      this.rowData = [
        {
          //useraccountid: 0,
          auditroleid: 1,
       
        },
        {
          //useraccountid: 0,
          auditroleid: 6,
       
        },
        {
          //useraccountid: 0,
          auditroleid: 7,
       
        }
  
      ];
      
    }
    this.index=0
  
  }

  nextAssignAuditors() {
    this.clientDetails = false;
    this.assignAuditors = true;
    //firststep
  }
  nextPriviewEngagementDetails() {
    this.clientDetails = false;
    this.assignAuditors = false;
    this.priviewEngagementDetails = true;
// second step Next btn click
  }
  
  addClientDialogbox(): void {
    const dialogRef = this.dialog.open(this.addEngagementDialog, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  updateengagementDialogbox(): void {
    const dialogRef = this.dialog.open(this.updateEngagementDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  inviteClientDialogbox(): void {
    const dialogRef = this.dialog.open(this.inviteClientDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  WatchoutforResponseDialogbox(): void {
    const dialogRef = this.dialog.open(this.WatchoutforResponseDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  getAllAuditor(){
    this.apiService.getAllAuditor().subscribe(data=>{
      this.auditorDetails=data['data'];
      console.log(this.auditorDetails)
    })
  }
  
  getEngagement(){
    this.apiService.getEngagement().subscribe(data=>{
      this.engagementDetails=data['data'];
      console.log(this.engagementDetails)
      //this.pengagementtypeid=1; //hard coded 'Notice to Reader'
      console.log('Notice to reade ID'+this.pengagementtypeid)
    })
  }
  getAuditorRole(){
    this.apiService.getAuditorRole().subscribe(data=>{
      this.auditorRoleDetails=data['data'];
      console.log(this.auditorRoleDetails)
    })
  }
  getBusinessDetails(){
    this.apiService.getBusinessDetails(this.auditorId).subscribe(data=>{
      this.BusinessDetails=data['data'];
      console.log(this.BusinessDetails)
    })
  }

  gotoAccountBooks(){
    this.clientDetails = false;
    this.priviewEngagementDetails = false;
    this.assignAuditors = true;
  }
  editClientDetails(){
    this.clientDetails = true;
    this.assignAuditors = false;
    this.priviewEngagementDetails = false;
  }
  editAssignAuditor(){
    this.clientDetails = false;
    this.assignAuditors = true;
    this.priviewEngagementDetails = false;
  }

  validateClientDetails(){
    if (this.clientDetails && this.clientDetailsForm.valid) {
    this.clientDetailsObj=this.clientDetailsForm.getRawValue();
    this.clientDetails = false;
    this.assignAuditors = true;
    this.engagementTitle=this.clientDetailsObj.Pengagementname;
    }
    else{
      this.clientDetailsObj=this.clientDetailsForm.getRawValue();
      if(this.clientDetailsObj && !this.clientDetailsObj.Pclientname){
        this.isClientError = true;
      }
      if(this.clientDetailsObj.Pyearend){
        if(new Date(this.clientDetailsObj.Pyearend) < new Date(this.clientDetailsObj.PincorporationDate)){
          this.toastr.error('Year End Should not be less than Incorporation Date');
        }
      }
      
      
    }
  }
  validateAssignAuditor(){
    if (this.assignAuditors && this.assignAuditorForm.valid) {
    this.assignAuditorObj=this.assignAuditorForm.value;
    this.clientDetails = false;
    this.assignAuditors = false;
    this.priviewEngagementDetails = true;
    this.BusinessDetails.forEach(item => {
      if(item.clientfirmid == this.clientDetailsObj.Pclientname){
        this.BusinessName = item.businessname;
      }
    });

    this.engagementDetails.forEach(item => {
      if(item.engagementtypeid == this.clientDetailsObj.pengagementtypeid){
        this.engagementNameTitle = item.engagementtype;
      }
    });
  }
  //view assign details (first,second,reviewer,admin)
  this.audtName=this.rowData;
  console.log(this.rowData)
  var adminarr=[]
  this.audtName.map((x,index)=>{
    if(index==0){
      this.Preparer=x.fullname;
    }
    if(index==1){
      this.firstReviewer=x.fullname;
    }
    if(index==2){
      this.secondReviewer=x.fullname
    }
    if(x.auditroleid>=11){
     adminarr.push(x.fullname)
      const adminname=x.fullname
    
    }
  })
  this.AdminName=adminarr[0];
  }
 
addEngagement(){
  this.selectedClientData = this.sharedService.selectedClient;
  if(this.selectedClientData && this.selectedClientData.clientfirmid){
    this.isReadOnly=true;
    this.Pclientname= this.selectedClientData.clientfirmid;
    this.PincorporationDate=this.selectedClientData.incorportiondate
  }
  var arr=[];
  this.rowData.map(x=>{
    arr.push(x.useraccountid);
    arr.push(x.auditroleid)
  })
  console.log(arr);
  this.accountIDRoleList=arr.toString();
  console.log(this.accountIDRoleList)
  let data: any = {
    "pengagementname": this.clientDetailsObj.Pengagementname,
    "psubenties": this.clientDetailsObj.Psubenties,
    "pengagementtypeid":this.clientDetailsObj.pengagementtypeid,
    "pyearend":this.datePipe.transform(this.clientDetailsObj.Pyearend, 'yyyy-MM-dd'),
    "pcompliation": this.clientDetailsObj.Pcompliation,
  //  "pincorporationdate":this.datePipe.transform(this.clientDetailsObj.PincorporationDate, 'yyyy-MM-dd'),
    "padditionalinfo": this.assignAuditorObj.additionalInformation,
    "puseraccoutidroleidlist":this.accountIDRoleList,
    "pclientfirmid":this.clientDetailsObj.Pclientname,
    "ploginid": this.auditorId
   // "engagementsid":this.clientDetailsObj.engagementsid,
    
  }
  this.spinner.show();
  this.apiService.createEngagement(data).subscribe(response => {
    if (response._statusCode == 200) {
      this.spinner.hide();
      this.addClientDialogbox();
    }
    else{
      //alert('client created successfully');
     // alert(response._statusMessage);
     this.spinner.hide();
      this.toastr.error(response._statusMessage);
    }
    
  }, error => {
    this.toastr.error('error in creating engagement');
  });
}
editEngagement(){
  this.isEnableNext=false
  this.Pclientname=this.selectedClientData.clientfirmid;
  this.engagementId=this.selectedClientData.engagementsid
  this.PincorporationDate=this.selectedClientData.incorporationdate;
  this.pengagementtypeid=this.selectedClientData.pengagementtypeid; //[(ngmodel)] data binding
  this.clientDetailsForm.setValue({
    'Pengagementname':  this.selectedClientData.engagementName,
    'PincorporationDate': this.selectedClientData.incorporationdate,
    'Pclientname':this.selectedClientData.clientfirmid,
    'Psubenties': this.selectedClientData.subenties,
   // 'Pengagementtype':'Notice to Reader',
   'pengagementtypeid':this.selectedClientData.pengagementtypeid, //not working for two way data binding
    'Pyearend': this.selectedClientData.yearEnd,
    'Pcompliation':this.selectedClientData.compilationtype
  });
   console.log("data"+this.selectedClientData.auditor_list);
   var res = {
    "payload" : this.selectedClientData.auditor_list
    };
      
    var payloadArray = JSON.parse(res.payload);
    console.log( payloadArray )
    this.rowData=payloadArray;
    console.log(this.rowData)
  this.additionalInformation=this.selectedClientData.additionalinfo;
  this.assignAuditorForm.setValue({
    'additionalInformation':this.selectedClientData.additionalinfo
  //  'auditor_list':this.selectedClientData.auditor_list
  });
}
UpdateEngagement(){
  console.log(this.rowData);
  var arr=[];
  this.rowData.map(x=>{
    arr.push(x.useraccountid);
    arr.push(x.auditroleid)
  })
  console.log(arr);
  this.accountIDRoleList=arr.toString();
  console.log(this.accountIDRoleList)
  let data: any = {
    "pengagementsid":this.engagementId,
    "pengagementname": this.clientDetailsObj.Pengagementname,
    "pclientfirmid":this.clientDetailsObj.Pclientname,
    "pengagementtypeid":this.clientDetailsObj.pengagementtypeid,
    "pcompliation": this.clientDetailsObj.Pcompliation,
     "psubenties": this.clientDetailsObj.Psubenties,
     "pyearend":this.datePipe.transform(this.clientDetailsObj.Pyearend, 'yyyy-MM-dd'),
    // "pincorporationdate":this.datePipe.transform(this.clientDetailsObj.PincorporationDate, 'yyyy-MM-dd'),
     "padditionalinfo": this.assignAuditorObj.additionalInformation,
     "puseraccoutidroleidlist":this.accountIDRoleList,
     "ploginid": this.auditorId
    
     
  }
  this.spinner.show();
  this.apiService.updateEngagementDetails(data).subscribe(response => {
    if (response._statusCode == 200) {
     // this.addClientDialogbox();
    //  alert("engagement is updated successfully")
    this.spinner.hide();
      this.updateengagementDialogbox();
    }
    else{
      this.spinner.hide();
      this.toastr.error(response._statusMessage);
    //  alert(response._statusMessage);
    }
    
  }, error => {
    this.toastr.error('error in updating engagement')
  });
  console.log("welcome to update")
}
incoporationDate(incorptdate){
 // this.BusinessName=bName;
 if(incorptdate){
  this.PincorporationDate=incorptdate.incorporationdate;
 }
 else{
  this.PincorporationDate =  '';
 }

 // this.isReadOnly=true;
 this.isIncrptDate=true
}
addRole() {
  console.log(this.rowData);
  this.rowData.push({
    // useraccountid: "",
        auditroleid: "",
  });
  this.isEnableNext=true
  this.index = this.rowData.length;
}
deleteRole() {
 // this.showDelete = !this.showDelete;
}
//Delete the selected row 
removeRole(index) {
  this.rowData.splice(index, 1);
  let errorCount = 0;
  this.rowData.forEach(item => {
    if(!item.useraccountid || !item.auditroleid){
      errorCount++;
    }
  });
  if(errorCount == 0){
    this.isEnableNext=false;
  }
  
}
auditorName(id){
this.newAuditorList.push(id)
}
auditorRole(id){
  this.newAuditorList.push(id)
  console.log(this.newAuditorList);
}
auditorNameList(name){
this.rowData.fullName=name
console.log(this.rowData.fullName)
}
checkRole(data,event){
  //validate role
  var auditorList=this.rowData
  var auditorListLength=(this.rowData).length
  var auditNameCount=0
  auditorList.map(y=>{
    if(!y.useraccountid || !y.auditroleid){
      auditNameCount=auditNameCount+1
    }
  })
  if(auditNameCount==0){
    this.isEnableNext=false
  }
  else{
    this.isEnableNext=true
  }
  //console.log(this.rowData[data].fullname);
  //validate slected role
  let target = event.source.selected._element.nativeElement;
 var name= target.innerText.trim()
  let selectedData = {
    value: event.value,
    text: target.innerText.trim()
  };
  console.log(selectedData);
  this.tblecolumn = (this.rowData.filter(x => x.auditroleid == data)).length;
 // console.log("isavailable" + this.tblecolumn.length);
  if (this.tblecolumn == 2) {
    this.msg = name + " is already selected";
    this.isDuplicate = true;
    // setTimeout(() => {
    //   this.msg = "";
    // }, 4000);
  }else{ this.tblecolumn='';
      this.isDuplicate = false;
  }    
}
// selectedName(name){
// this.selectedRoleName=name
// }
validateAuditorName(id,event){
 //validate auditor name 
 var auditorList=this.rowData
 var auditorListLength=(this.rowData).length
 var auditNameCount=0
 auditorList.map(y=>{
   if(!y.useraccountid || !y.auditroleid){
     auditNameCount=auditNameCount+1
   }
 })
 if(auditNameCount==0){
   this.isEnableNext=false
 }
 else{
   this.isEnableNext=true
 }
}
}
