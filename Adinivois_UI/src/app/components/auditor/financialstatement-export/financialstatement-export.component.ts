import { Component, OnInit } from '@angular/core';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

import * as jsPDF  from 'jspdf';  
  
import html2canvas from 'html2canvas';  

@Component({
  selector: 'app-financialstatement-export',
  templateUrl: './financialstatement-export.component.html',
  styleUrls: ['./financialstatement-export.component.scss']
})
export class FinancialstatementExportComponent implements OnInit {
  exportAsConfig: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    //elementId: 'balance-sheet-preview', // the id of html/table element
    elementId: 'financialstatement-container', // the id of html/table element
    options: { // html-docx-js document options
      margins: {
        top: '20',
        bottom: '5'
      },
      pagebreak: { before: '.beforeClass', after: ['#after1', '#after2', '#after3'], avoid: 'p, li' },
      filename: 'financialstatement.pdf',
      html2canvas: { dpi: 192, letterRendering: false },
      image: { type: 'jpeg', quality: 1 },
    }
  }
  constructor(private exportAsService: ExportAsService) { }

  ngOnInit() {
  }

  exportToPdfFinancialStatement() {

    
    this.exportAsService.save(this.exportAsConfig, 'FinancialStatement').subscribe(() => {
      // save started
    });
    // var pdf = new jsPDF('p','pt','a4');
    // // pdf.addHTML(financialstatement-container,function() {
    // //     pdf.save('web.pdf');
    // // });
    // pdf.addHTML(document.getElementById('financialstatement-container'),function() {
    // pdf.save('web.pdf');
    // });
    // var data = document.getElementById('financialstatement-container');  
    // html2canvas(data).then(canvas => {  
    //   // Few necessary setting options  
    //   var imgWidth = 208;   
    //   var pageHeight = 295;    
    //   var imgHeight = canvas.height * imgWidth / canvas.width;  
    //   var heightLeft = imgHeight;  
  
    //   const contentDataURL = canvas.toDataURL('image/png')  
    //   let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
    //   var position = 0;  
    //   pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
    //   pdf.save('MYPdf.pdf'); // Generated PDF   
    // });  
  }
}
