import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource } from '@angular/material/tree';
import { ApiService } from 'src/app/services/api.service';
import { SharedService } from '../../shared/shared.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

interface IncomeStatementTree {
  name: string;
  children?: IncomeStatementTree[];
  grouptotal?: string;
  subgrouptotal?: string;
  total?: string;
  bal?: any;
}

/** Flat node with expandable and level information */
interface TreeFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'income-sheet-preview',
  templateUrl: './income-sheet-preview.component.html',
  styleUrls: ['./income-sheet-preview.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class IncomeSheetPreviewComponent implements OnInit {
  treeControl = new NestedTreeControl<IncomeStatementTree>(node => node.children);
  dataSource = new MatTreeNestedDataSource<IncomeStatementTree>();
  engagementId: number;
  year: number;
  incomeStatementDetails: any;
  incomeStmtYear: string;
  yearEnd: string;
  engagementName: string;
  previousYear: string;
  isIncomePrevYear: boolean = false;
  incomeStatementExportItemMenu: boolean = false;
  isEditable:boolean=false;
  selectedEngagementDetails: any;
  constructor(private apiservice: ApiService,
    private sharedservice: SharedService,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService,
    private datePipe:DatePipe,
    public router: Router) {
    //  this.dataSource.data = TREE_DATA;
  }

  hasChild = (_: number, node: IncomeStatementTree) => !!node.children && node.children.length > 0;
  @ViewChild('tree') tree;

  ngOnInit() {
    this.getIncomeStatement();
    this.previousYear = localStorage.getItem('PreviousYear')
  }
  getIncomeStatement() {
    if(this.router.url=="/dashboard/ntrdashboard/IncomeStatement")
    {
      this.selectedEngagementDetails = JSON.parse((localStorage.getItem('selectedEngagementObj')));
      this.engagementName =this.selectedEngagementDetails.engagementName;
      this.engagementId = this.selectedEngagementDetails.engagementsid
      this.yearEnd = this.selectedEngagementDetails.yearEnd;
      this.year = parseInt(this.datePipe.transform(this.yearEnd, 'y'))
    }
  else{
      this.engagementId = parseInt(localStorage.getItem('engagementID'))
      this.year = parseInt(localStorage.getItem('trailBalYear')) //coming from enagement year Drop down(trail-bal-grid comp)
      this.yearEnd = (localStorage.getItem('endYear'))
      this.engagementName = (localStorage.getItem('engementName'))
      if (!this.year) {
        this.year = parseInt(this.datePipe.transform(this.yearEnd, 'y'))
      }
    }
   
    if (this.engagementId && this.year) {
      this.spinner.show();
      this.apiservice.getIncomeStatement(this.engagementId, this.year).subscribe(response => {
        this.spinner.hide();
        if (response['_statusCode'] == 200) {
          // this.engementName=this.sharedservice.balStatementEngagementName;
          //  this.yearEnd=this.sharedservice.balStmtYearEnd;
          var balStmtDetails = response['data'][0]['result'];
          if (balStmtDetails == null) {
         //   this.toaster.warning("income statement is not available for year " + this.year)
          }
          this.incomeStatementDetails = JSON.parse(balStmtDetails);
          var prev = this.incomeStatementDetails[0]['Prevgrouptotal'];
          if (prev || prev == 0) {
            this.isIncomePrevYear = true
          }

          //this.dataSource=this.balanceStatementDetails;
          console.log(balStmtDetails);
          const TREE_DATA: IncomeStatementTree[] = this.incomeStatementDetails;
          this.dataSource.data = TREE_DATA;
          this.treeControl.dataNodes = TREE_DATA;
          this.tree.treeControl.expandAll();
        }
        else if (response['_statusCode'] == 404) {
          this.toaster.warning("No data available");
        }
      }, error => {
        this.spinner.hide();
      })
    }
  }
  refresh() {
    this.getIncomeStatement();
  }
  incomeStatementExport(e) {
    this.incomeStatementExportItemMenu = !this.incomeStatementExportItemMenu;

  }
  closeDaolog() {
    if (this.incomeStatementExportItemMenu) {
      this.incomeStatementExportItemMenu = false;
    }
  }
  UpdateincomeSheet(value){
console.log(value);
  }
}
