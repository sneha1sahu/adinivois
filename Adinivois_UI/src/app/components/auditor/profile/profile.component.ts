import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../../helpers/must-match.validator';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

  user: any;
  myprofiledetails: any;
  changePasswordForm: FormGroup; // Declare the changePassword
  myaccountform: FormGroup; // Declare the changePassword
  profilePic:any = "";
  userData: any;
  myAccountData: any;
  nameAcronym: any;
  profileImage: any;
 updateUserData = {
  "useraccountid": 0,
  "firstname": "",
  "lastname": "",
  "pcphonenumber": 0,
  "pbphonenumber": 0,
  "memberlicenseno": 0,
  "businessname": "",
  "emailaddress": "",
  "address": "",
  "profilepict": ""
}

  constructor(
    public router: Router,
    public sharedService: SharedService,
    private fb: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService,
    private apiService: ApiService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.userDetails();

    //
    this.myaccountform = this.fb.group({
      'firstName': new FormControl(this.userData.firstname, [Validators.required]),
      'lastName': new FormControl(this.userData.lastname, [Validators.required]),
      'cellPhone': new FormControl(this.userData.cellphonenumber, [Validators.required]),
      'businessPhone': new FormControl(this.userData.businessphoneNumber, [Validators.required]),
      'memberLicenseNo': new FormControl(this.userData.licenseno, [Validators.required]),
      'profileImgFile': ['']
    });

   
    //
    this.changePasswordForm = this.fb.group({

      'oldPassword': new FormControl('', [Validators.required]),
      'newPassword': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'confirmPassword': new FormControl('', [Validators.required])
    }, { validator: MustMatch('newPassword', 'confirmPassword') });
    
    
  }

  get oldPassword() {
    return this.changePasswordForm.get('oldPassword');
  }
  get newPassword() {
    return this.changePasswordForm.get('newPassword');
  }
  get confirmPassword() {
    return this.changePasswordForm.get('confirmPassword');
  }
  get f() { return this.changePasswordForm.controls; }

  //Submitting form
  public onFormSubmit() {

    if (this.changePasswordForm.valid) {
      this.user = this.changePasswordForm.value;
      let data: any = {
        "puseraccountid": this.sharedService.userData,
        "oldpassword": this.user.oldPassword,
        "newpassword": this.user.newPassword
      }
      this.apiService.changePassword(data).subscribe(response => {
        if (response._statusCode == 200) {
          //alert(response._statusMessage);
          this.toastr.success(response._statusMessage);
          sessionStorage.removeItem("userDetails");
          this.router.navigate(['/login']);
        }
        else {
          this.toastr.error("Your entered old password is invalid");
          //alert("Your entered old password is invalid");
        }
      }, error => {
        this.toastr.error('error in passwor');
        //alert('error in passwor')
      });
    }

  }


  get firstName() {
    return this.myaccountform.get('firstName');
  }
  get lastName() {
    return this.myaccountform.get('lastName');
  }
  get cellPhone() {
    return this.myaccountform.get('cellPhone');
  }
  get businessPhone() {
    return this.myaccountform.get('businessPhone');
  }
  get memberLicenseNo() {
    return this.myaccountform.get('memberLicenseNo');
  }

  //my account Submitting form
  public myAccountSubmit(image) {
    if (this.myaccountform.valid) {
      this.myprofiledetails = this.myaccountform.value;      
      this.updateUserData.useraccountid = this.userData.useracctid;
      this.updateUserData.firstname = this.myprofiledetails.firstName;
      this.updateUserData.lastname = this.myprofiledetails.lastName;
      this.updateUserData.pcphonenumber = this.myprofiledetails.cellPhone;
      this.updateUserData.pbphonenumber = this.myprofiledetails.businessPhone;
      this.updateUserData.memberlicenseno = this.myprofiledetails.memberLicenseNo;
      this.updateUserData.businessname = this.userData.businessName;
      this.updateUserData.emailaddress = this.userData.emailAddress;
      this.updateUserData.address = this.userData.address;
      this.updateUserData.profilepict = image ? image : '';
      this.spinner.show();
      this.apiService.updateUserProfile(this.updateUserData).subscribe(response => {
        this.spinner.hide();
        if (response._statusCode == 200) {
          this.toastr.success("Account details updated successfully");
          if(response){
            sessionStorage.setItem('userDetails', JSON.stringify(response.data[0]));
            let updatedDetails: any = response.data[0];
            this.sharedService.profileObj.next(updatedDetails);
          }
          
        }
        else {
          this.toastr.error("error in updating details");
        }
      }, error => {
        this.spinner.hide();
        this.toastr.error("error in updating details");
      });
    }


  }

  //File upload
  uploadProfilePic(event) {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileImage  = file;
      const reader = new FileReader();
      reader.onload = e => this.profilePic = reader.result;
      reader.readAsDataURL(file);
    }
  }

  saveProfileDetails(){
    if(this.profileImage){
      let formData:FormData = new FormData();
      let s3signatureKey = 'profilepicture/' + this.profileImage.name;
      formData.append('bucketname', 'adinovisemailtemplateimage');
      formData.append('signatureKey', s3signatureKey);
      formData.append('file', this.profileImage, this.profileImage.name);
      this.spinner.show();
      this.apiService.saveProfileImageToS3(formData).subscribe(response => {
        this.spinner.hide();
        if (response._statusCode == 200) {
          let imageUrl = 'https://s3.amazonaws.com/adinovisemailtemplateimage/profilepicture/' + this.profileImage.name;
          this.myAccountSubmit(imageUrl)
          //this.toastr.success("Account details updated successfully");
        }
        else {
          this.toastr.error("error in updating details");
        }
      }, error => {
        this.spinner.hide();
        this.toastr.error("error in updating details");
      });
    }
    else{
      this.myAccountSubmit(this.profilePic);
    }
      
  }

  userDetails() {
    this.userData = JSON.parse((sessionStorage.getItem('userDetails')));
    if (this.userData && this.userData.fullName) {
      let matches = this.userData.fullName.match(/\b(\w)/g);
      let acronym = matches.join('');
      if (acronym && acronym.length > 2) {
        acronym = acronym.slice(0, 2);
      }
      this.nameAcronym = acronym;
    }
    else {
      if (!this.userData) {
        this.router.navigate(['/login']);
      }
    }
    this.profilePic = this.userData ? this.userData.profilepicture : '';
  }

  //
  public handlePhone(event, type) {
    const onlyNums = event.target.value.replace(/[^0-9]/g, '');
    if (onlyNums.length < 10) {
      if (type == 'cell') {
        this.cellPhone.setValue(onlyNums);
      }
      else if (type == 'business') {
        this.businessPhone.setValue(onlyNums);
      }
      else {
        return;
      }
    } else if (onlyNums.length === 10) {
      const number = onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
      if (type == 'cell') {
        this.cellPhone.setValue(number);
      }
      else if (type == 'business') {
        this.businessPhone.setValue(number);
      }
      else {
        return;
      }
    }
  };

  public onlyChars(event, type) {
    const noSpecialCHars = event.target.value.replace(/[^a-zA-Z]/g, '');
    if (type == 'fn') {
      this.firstName.setValue(noSpecialCHars);
    }
    else if (type == 'ln') {
      this.lastName.setValue(noSpecialCHars);
    }
    else {
      return;
    }
  }

  public aplphaNumericInput(event) {
    const noSpecialCHars = event.target.value.replace(/[^a-zA-Z0-9]/g, '');
    this.memberLicenseNo.setValue(noSpecialCHars);
  }

}
