import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../shared/shared.service';
import { Subscription, timer, pipe } from 'rxjs';

@Component({
  selector: 'client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
    clientAcountDetails: any;
    loggedInUserData: any;
  auditorId: any;
  updatedClientsActivities: Subscription;
  updatedClientStatus: Subscription;
  countSubscription: any;
  clientsSubscription: any;

  constructor(private apiService: ApiService, private toastr: ToastrService, private sharedService: SharedService) { }

  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.getClientAcountDetails();
    this.updatedClientsActivities = this.sharedService.clientListCount.subscribe(response => {
      this.getClientAcountDetails();
    })
    this.updatedClientStatus = this.sharedService.clientStausCount.subscribe(response => {
      this.getClientAcountDetails();
    })
  }
  //
  getClientAcountDetails() {
    this.countSubscription = this.apiService.getclientcountdetails(this.auditorId).subscribe(data => {
      this.clientAcountDetails = data['data'];
      this.clientAcountDetails = this.clientAcountDetails[0];
      //console.log(this.clientAcountDetails);
      this.subscribeToClientsDetails();
    }, error => {
      this.toastr.error('error client status');
      //alert('error client status');
    });
  }

  subscribeToClientsDetails() {
    this.clientsSubscription = timer(5000).subscribe(() => this.getClientAcountDetails())
  }

  ngOnDestroy() {
    this.updatedClientsActivities.unsubscribe;
    this.updatedClientStatus.unsubscribe;
    if(this.clientsSubscription){
      this.clientsSubscription.unsubscribe();
    }
    if(this.countSubscription){
      this.countSubscription.unsubscribe();
    }
  }
  //
}
