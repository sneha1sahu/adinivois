import { Component, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource } from '@angular/material/tree';
import { ApiService } from 'src/app/services/api.service';
import { SharedService } from '../../shared/shared.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

interface BalanceSheetTree {
  name: string;
  children?: BalanceSheetTree[];
  grouptotal?: string;
  subgrouptotal?: string;
  total?: string;
  bal?: any;
}

/** Flat node with expandable and level information */
interface TreeFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'balance-sheet-preview',
  templateUrl: './balance-sheet-preview.component.html',
  styleUrls: ['./balance-sheet-preview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class BalanceSheetPreviewComponent implements OnInit {
  treeControl = new NestedTreeControl<BalanceSheetTree>(node => node.children);
  dataSource = new MatTreeNestedDataSource<BalanceSheetTree>();
  balanceStatementDetails: any;
  yearEnd: any;
  engementName: any;

  TREE_DATA: any;
  engagementId: any;
  year: number;
  balanceStatementYear: string;
  engagementName: string;
  mappedtotal: any;
  previousYear: any;
  isprevYear: boolean = false;
  balSheetExportItemMenu: boolean = false;
  balanceSheetName: any;
  isEditable:boolean=false;
  updatedBalanceSheetObj=[];
  exportAsConfig: ExportAsConfig = {
    type: 'xlsx', // the type you want to download
    //elementId: 'balance-sheet-preview', // the id of html/table element
    elementId: 'balance-sheet-table', // the id of html/table element
    options: { // html-docx-js document options
      margins: {
        top: '20',
        bottom: '20'
      }
    }
  }
  isBalanceSheet: boolean=true;
  selectedEngagementDetails: any;
  constructor(private apiservice: ApiService,
    private sharedservice: SharedService,
    private exportAsService: ExportAsService,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService,
    private datepipe: DatePipe, public router: Router,
    public dialog: MatDialog) {
    //this.dataSource.data = this.TREE_DATA;
  }
  hasChild = (_: number, node: BalanceSheetTree) => !!node.children && node.children.length > 0;
  //@ViewChild('tree') tree;

  ngOnInit() {
    this.getBalanceStatement();
    this.previousYear = localStorage.getItem('PreviousYear');
  }

  getBalanceStatement() {
    if(this.router.url=="/dashboard/ntrdashboard/BalanceSheet")
    {
      this.selectedEngagementDetails = JSON.parse((localStorage.getItem('selectedEngagementObj')));
      this.engagementName =this.selectedEngagementDetails.engagementName;
      this.engagementId = this.selectedEngagementDetails.engagementsid
      this.yearEnd = this.selectedEngagementDetails.yearEnd;
      this.year = parseInt(this.datepipe.transform(this.yearEnd, 'y'))
    }
    else{
      this.engagementName = (localStorage.getItem('engementName'))
      this.engagementId = parseInt(localStorage.getItem('engagementID'))
      this.yearEnd = (localStorage.getItem('endYear'))
     // var trialBalaceYear=parseInt(this.datepipe.transform(this.yearEnd,'y'))
      this.year = parseInt(localStorage.getItem('trailBalYear')) //coming from enagement year Drop down(trail-bal-grid comp)
      // if(!this.year){
      //   this.year=trialBalaceYear
      // }
    }
   
    if (this.engagementId && this.year) {
      this.spinner.show();
      this.apiservice.getBalanceSheet(this.engagementId, this.year).subscribe(response => {
        this.spinner.hide();
        if (response['_statusCode'] == 200) {
          var balStmtDetails = response['data'][0]['result'];
          var noDatamapped = response['data'];
          if (noDatamapped == 'No balancesheet map') { 
            this.isBalanceSheet=false;
             this.toaster.warning("no trial balance mapped")
          }
          // if (balStmtDetails == null) {
          //    this.toaster.warning("no data available for balance sheet ")
          // }
          else{
            console.log(this.balanceStatementDetails);
            this.balanceStatementDetails = JSON.parse(balStmtDetails);
            var prev = this.balanceStatementDetails[0]['Prevgrouptotal'];
            if (prev || prev == 0) {
              this.isprevYear = true
            }
            //console.log("pre is exist" + prev)
            var len = (this.balanceStatementDetails).length
            console.log("length:-" + len);
            var details = this.balanceStatementDetails[len - 1]['children']
            //this.mappedtotal = details['10']
            //console.log(this.mappedtotal);
            //this.dataSource=this.balanceStatementDetails;
            console.log(balStmtDetails);
            const TREE_DATA: BalanceSheetTree[] = this.balanceStatementDetails;
            this.dataSource.data = TREE_DATA;
            //this.treeControl.dataNodes = TREE_DATA;
            //this.tree.treeControl.expandAll();
          }
        }
        else if (response['_statusCode'] == 404) {
          this.toaster.warning("No data available");
        }
        else if (response['_statusCode'] == 400){
          this.toaster.warning("No data available");
        }

      }, error => {
        this.spinner.hide();
        
      })
    }
    this.balanceStatementYear = localStorage.getItem('trailBalYear')
    console.log('year:-' + this.balanceStatementYear)
  }
  refresh() {
    this.getBalanceStatement();
  }

  exportToPdf() {

  }

  exportToPdfBalanceSheet() {

    // download the file using old school javascript method
    this.exportAsService.save(this.exportAsConfig, 'My File Name').subscribe(() => {
      // save started
    });
    // get the data as base64 or json object for json type - this will be helpful in ionic or SSR
    /*this.exportAsService.get(this.config).subscribe(content => {
      console.log(content);
    });*/

  }

  balSheetExport() {
    this.balSheetExportItemMenu = !this.balSheetExportItemMenu;
  }
  closeDaolog() {
    if (this.balSheetExportItemMenu) {
      this.balSheetExportItemMenu = false;
    }
  }
  UpdateBalanceSheet(objchild){
   var obj={
            "name":objchild.name,
            "oldValue":objchild.oldvalue,
            "chldid":objchild.chldid}
this.updatedBalanceSheetObj.push(obj);

localStorage.setItem("updateBalanceSheetObj",(JSON.stringify(this.updatedBalanceSheetObj)))
    console.log('Id of child'+ obj)
  }
}
