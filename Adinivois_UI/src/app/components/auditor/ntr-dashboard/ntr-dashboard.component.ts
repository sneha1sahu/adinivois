import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlatformLocation } from '@angular/common'
import { SharedService } from '../../shared/shared.service';
@Component({
  selector: 'ntr-dashboard',
  templateUrl: './ntr-dashboard.component.html',
  styleUrls: ['./ntr-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NtrDashboardComponent implements OnInit {

  isSideMenuExpanded: boolean = true;

  selectedMenu: any = 'clientAcceptance';
  selectedEngagement: any = {};
  showAdvancedOptions: any = true;
  isClientOnboardingExpanded: boolean=false;
  isFinanacicalStatementExpanded: boolean=false;
  constructor(public router: Router,location: PlatformLocation,
              public shareService:SharedService) {  
    location.onPopState(() => {
      this.selectedMenu='';
    //  this.activeTab(this.router.url)
    })
  }

  ngOnInit() {
    this.selectedEngagement = JSON.parse((localStorage.getItem('selectedEngagementObj')));
    this.shareService.selectedEnagagementObj.next(this.selectedEngagement);
    if (this.selectedEngagement) {
      if (this.selectedEngagement.compilationtype == 'Advanced') {
        this.showAdvancedOptions = true;
      }
      else {
        this.showAdvancedOptions = false;
      }
     
    }
    this.activeTab(this.router.url) //checking for active tab
  }

  sideMenutoggleDrawer() {
    console.log(this.isSideMenuExpanded);
    this.isSideMenuExpanded = !this.isSideMenuExpanded;
  }

  viewNtrItems(item) {
    switch (item) {
      case ('trialbalance'):
        this.selectedMenu = 'trialbalance';
        break;
      case ('coverpage'):
        this.selectedMenu = 'coverpage';
        break;
      case ('tableofcontents'):
        this.selectedMenu = 'tableofcontents';
        break;
      case ('compilationreport'):
        this.selectedMenu = 'compilationreport';
        break;
      case ('balancesheet'):
        this.selectedMenu = 'balancesheet';
        break;
      case ('incomestatement'):
        this.selectedMenu = 'incomestatement';
        break;
      case ('clientAcceptance'):
        this.selectedMenu = 'clientAcceptance';
        break;
      case ('engagementLetter'):
        this.selectedMenu = 'engagementLetter';
        break;
      case ('planning'):
        this.selectedMenu = 'planning';
        break;
      case ('procedures'):
        this.selectedMenu = 'procedures';
        break;
      case ('statementCashFlows'):
        this.selectedMenu = 'statementCashFlows';
        break;
      case ('notesFS'):
        this.selectedMenu = 'notesFS';
        break;
      case ('tax'):
        this.selectedMenu = 'tax';
        break;
      case ('reports'):
        this.selectedMenu = 'reports';
        break;
      case ('completion-signOff'):
        this.selectedMenu = 'completion-signOff';
        break;

    }
  }
  activeTab(item) {
    switch (item) {
      case ('/dashboard/ntrdashboard/ClientAcceptance'):
        this.selectedMenu = 'clientAcceptance';
        this.isClientOnboardingExpanded=true;
        break;
      case ('/dashboard/ntrdashboard/EngagementLetter'):
        this.selectedMenu = 'engagementLetter';
        this.isClientOnboardingExpanded=true;
        break;
      case ('/dashboard/ntrdashboard/Planning'):
        this.selectedMenu = 'planning';
        this.isClientOnboardingExpanded=true;
        break;
      case ('/dashboard/ntrdashboard/TrialBalance'):
        this.selectedMenu = 'trialbalance';
        break;
      case ('/dashboard/ntrdashboard/Procedure'):
        this.selectedMenu = 'procedures';
        break;
      case ('/dashboard/ntrdashboard/CoverPage'):
        this.isFinanacicalStatementExpanded=true
        this.selectedMenu = 'coverpage';
        break;
      case ('/dashboard/ntrdashboard/TableOfContents'):
        this.selectedMenu = 'tableofcontents';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/CompilationReport'):
        this.selectedMenu = 'compilationreport';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/BalanceSheet'):
        this.selectedMenu = 'balancesheet';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/IncomeStatement'):
        this.selectedMenu = 'incomestatement';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/StatementCashFlows'):
        this.selectedMenu = 'statementCashFlows';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/NotesToFinancialStatement'):
        this.selectedMenu = 'notesFS';
        this.isFinanacicalStatementExpanded=true
        break;
      case ('/dashboard/ntrdashboard/CompletionSignOff'):
        this.selectedMenu = 'completion-signOff';
        break;
      case ('/dashboard/ntrdashboard/Tax'):
        this.selectedMenu = 'tax';
        break;
      case ('/dashboard/ntrdashboard/Report'):
        this.selectedMenu = 'reports';
        break;

    }
  }

}
