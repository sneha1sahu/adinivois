import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-of-contents',
  templateUrl: './table-of-contents.component.html',
  styleUrls: ['./table-of-contents.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableOfContentsComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
