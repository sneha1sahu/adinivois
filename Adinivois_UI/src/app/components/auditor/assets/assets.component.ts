import { Component, OnInit } from '@angular/core';

export interface CompilationProceduresAssetsCash {

  accNumCP: number;
  descriptionCP: string;
  prelimCP: number;
  adjusmentCP: number;
  finalCP: number;
  refDocCP: string;
  annotationsCP: string;
  py1CP: number;
  changeCP: number;
  py2CP: number;
}

const ELEMENT_DATA: CompilationProceduresAssetsCash[] = [
  { accNumCP: 234, descriptionCP: 'Accounts Receivable', prelimCP: 20000.00, adjusmentCP: 27986.35, finalCP: 0.00, refDocCP: 'receipt.doc', annotationsCP: 'Add', py1CP: 0.00, changeCP: 0.00, py2CP: 0.00 },
];

export interface asstesCashAdjData {
  accNum: number;
  decription: string;
  type: string;
  debit: number;
  credit: number;
  actions: string;
}
const ASSETS_CASH_ADJ_DATA: asstesCashAdjData[] = [
  {accNum: 1, decription: 'Accounts Receivable', type: "Normal", debit: 20000, credit: 7000, actions: ''}
];

export interface adjustmentData {
  accNum: number;
  decription: string;
  type: string;
  debit: number;
  credit: number;
  actions: string;
}
const AdjustmentData: adjustmentData[] = [
  {accNum: 1, decription: 'Accounts Receivable', type: "Normal", debit: 20000, credit: 7000, actions: ""}
];

@Component({
  selector: 'assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss']
})
export class AssetsComponent implements OnInit {

  assetsCassAdjFlyOut: boolean = false;
  isSaved: boolean = false;

  displayedColumns: string[] = ['accNumCP', 'descriptionCP', 'prelimCP', 'adjusmentCP', 'finalCP', 'refDocCP', 'annotationsCP', 'py1CP', 'changeCP', 'py2CP'];
  assetsCashData = ELEMENT_DATA;

  //

  AsstesCashAdjColumns: string[] = ['accNum', 'decription', 'type', 'debit', 'credit', 'actions'];
  AsstesCashGridData = ASSETS_CASH_ADJ_DATA;

  constructor() {
  }

  ngOnInit() {
    
  }

  selected = 'option2';

  assetsAdjuEntry() {
    this.assetsCassAdjFlyOut = true;
  }

  closeAdjustment() {
    if (this.assetsCassAdjFlyOut) {
      this.assetsCassAdjFlyOut = false;
    }

  }
  saveAsstesAdjEntry() {
    this.isSaved = true;
  }
  

}
