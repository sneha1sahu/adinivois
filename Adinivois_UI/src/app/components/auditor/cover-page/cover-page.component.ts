import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';
// import * as Quill from 'quill';

//import { MomentDateAdapter } from '@angular/material-moment-adapter';
//import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

//import { default as _rollupMoment } from 'moment';

//const moment = _rollupMoment;
//export const MY_FORMATS = {
//  parse: {
//    dateInput: 'LL',
//  },
//  display: {
//    dateInput: 'LL',
//    monthYearLabel: 'MMM YYYY',
//    dateA11yLabel: 'LL',
//    monthYearA11yLabel: 'MMMM YYYY',
//  },
//};

@Component({
  selector: 'app-cover-page',
  templateUrl: './cover-page.component.html',
  styleUrls: ['./cover-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  //providers: [

  //  { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

  //  { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  //],
})
export class CoverPageComponent implements OnInit {
  @ViewChild('textEditor') textEditor: TemplateRef<any>;
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  yearEnd: any;
  //date = new FormControl(moment());
  businessName: any = '<h4>Business Name</h4>';
  financialYear: any = '<h5>FY 18-19</h5>'
  editorContent: any = '';
  selectedElement: any = '';
  dialogRef: any;
  coverImage: any ="../../../../assets/images/adinovis_logo.png";
  imageFile: any;
  engagementID: any;
  documentDate: any = new Date();
  savedData: any;
  engagementClientID: any;
  constructor(public dialog: MatDialog, private apiService: ApiService, private sharedService: SharedService, public router: Router,  private location: Location) { }
  config: any = {
    height: 250,
    theme: 'modern',
    // powerpaste advcode toc tinymcespellchecker a11ychecker mediaembed linkchecker help
    plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image imagetools link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount contextmenu colorpicker textpattern',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  fontselect fontsizeselect | removeformat',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    menubar:false,
    statusbar: false,
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  };
  ngOnInit() {
    this.getJSON();
    // var quill = new Quill('#editor', {
    //   modules: {
    //     syntax: true, 
    //     toolbar: [
    //       [{ header: [1, 2, false] }],
    //       ['bold', 'italic', 'underline'],
    //       ['image', 'code-block']
    //     ]
    //   },
    //   placeholder: 'Compose an epic...',
    //   theme: 'snow'  // or 'bubble'
    // });
    //this.businessName="<h4><span style='color: #008080;'><em><strong>Business Name</strong></em></span></h4>"
  }

  public getJSON(){
    //this.engagementID = this.sharedService.engagementId;
    this.engagementID = parseInt(localStorage.getItem('engagementID'));
    this.engagementClientID = parseInt(localStorage.getItem('engagementClientID'));

    let url = 'https://s3.amazonaws.com/' + 'adinovisclientdocs/Clients/' + this.engagementClientID + '_Client/' + this.engagementID + '_engagements/' + 'FinancialStatement/' + this.engagementID + '_statement.json';
    this.apiService.getFinancialDocumentDetailsFromJSON(url).subscribe(response =>{
      this.savedData = response;
      if(this.savedData && this.savedData.coverPage){
        this.businessName = this.savedData.coverPage.businessName ? this.savedData.coverPage.businessName : this.businessName;
        this.financialYear = this.savedData.coverPage.financialYear ? this.savedData.coverPage.financialYear : this.financialYear;
        this.documentDate = this.savedData.coverPage.date;
        this.coverImage = this.savedData.coverPage.logo ? this.savedData.coverPage.logo : '../../../../assets/images/adinovis_logo.png';
      }
    }, error => {
      this.savedData = {
        "coverPage": {"logo": "", "businessName": "", "financialYear": "", "date": ""},
        "compilationReport": {"reportHeader": "", "topLeftContent": "", "letterBody": "", "bottomRightContent": "", "topImage": "", "bottomImage": ""},
        "page3": {"title1": "", "title2": ""}
      }
      console.log(error);
    })
  }
  openEditor(elementName){
    if(this.router.url !== '/dashboard/financialstatementexport'){
      if(elementName == 'businessName'){
        this.editorContent = this.businessName;
      }
      else if(elementName == 'financialYear'){
        this.editorContent = this.financialYear;
      }
      
      this.selectedElement = elementName;
      this.dialogRef = this.dialog.open(this.textEditor);
    }
    
    
  }
  doneEditing(){
    if(this.selectedElement == 'businessName'){
      this.businessName = this.editorContent;
    }
    else if(this.selectedElement == 'financialYear'){
      this.financialYear = this.editorContent;
    }
    this.dialogRef.close();
    this.saveJSON();
  }
  closeDialog(){
    this.dialogRef.close();
  }
  uploadCoverImage(event) {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imageFile  = file;
      const reader = new FileReader();
      reader.onload = e => this.coverImage = reader.result;
      reader.readAsDataURL(file);
      this.saveCoverImage();
    }
  }

  saveCoverImage(){
    if(this.imageFile){
      let formData:FormData = new FormData();
      let coverImageName = this.engagementID +'_' + this.imageFile.name;
      let s3signatureKey = 'profilepicture/fd/' + coverImageName;
     // formData.append('bucketname', 'adinovisemailtemplateimage');
      formData.append('signatureKey', s3signatureKey);
      formData.append('file', this.imageFile, coverImageName);
      this.apiService.saveProfileImageToS3(formData).subscribe(response => {
        if (response._statusCode == 200) {
          this.coverImage = 'https://s3.amazonaws.com/adinovisemailtemplateimage/profilepicture/fd/' + coverImageName;
          this.saveJSON();
        }
        else {

        }
      }, error => {

      });
    }
  }
  dateChange(){
    this.saveJSON();
  }
  saveJSON(){
    // let financialDoc= {
    //   "coverPage": {"logo": "sample.jpg", "businessName": this.businessName, "financialYear": this.financialYear, "date": this.documentDate},
    //   "compilationReport": {"reportHeader": "", "topLeftContent": "", "letterBody": "", "bottomRightContent": ""},
    //   "page3": {"title1": "", "title2": ""}
    // }
    this.savedData.coverPage = {"logo": this.coverImage, "businessName": this.businessName, "financialYear": this.financialYear, "date": this.documentDate}
    let jsonString = JSON.stringify(this.savedData);
    var blob = new Blob([jsonString], {type: "text/json;charset=utf-8"});

    let formData:FormData = new FormData();
    let jsonFileName = this.engagementID + '_statement.json';
    let s3signatureKey = 'Clients/' + this.engagementClientID + '_Client/' + this.engagementID + '_engagements/' + 'FinancialStatement/' + jsonFileName;
    //formData.append('bucketname', 'adinovisemailtemplateimage');
    let status: any = 0
    formData.append('status', status);
    formData.append('signatureKey', s3signatureKey);
    formData.append('file', blob, jsonFileName);
    this.apiService.saveFinancialDocumentJSON(formData).subscribe(response =>{

    }, error => {

    })
  }
  showStatementPreview(){
    const angularRoute = this.location.path();
    const url = window.location.href;
    const domainAndApp = url.replace(angularRoute, '');
    const newWindowUrl = domainAndApp + '/dashboard/financialstatementexport';
    window.open(newWindowUrl, "_blank");
  }
}
