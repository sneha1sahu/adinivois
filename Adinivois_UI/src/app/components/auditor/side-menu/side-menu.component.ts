import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  constructor(private router: Router, private sharedServices: SharedService) { }
  activePage: any = '';
  routeSubscription: any
  ngOnInit() {

    this.routeSubscription = this.sharedServices.routeData.subscribe(data => {
      if(data){
        let currentRoute = data.url;
        if (currentRoute === '/dashboard/client' || currentRoute === '/dashboard/addclient' || currentRoute === '/dashboard/editclient') {
          this.activePage = 'client';
        }
        else if (currentRoute === '/dashboard/engagement' || currentRoute === '/dashboard/createengagement' || currentRoute === '/dashboard/trailbalance') {
          this.activePage = 'engagement';
        }
        else if (currentRoute === '/dashboard/myaccount') {
          this.activePage = 'myaccount';
        }
        else {
          //console.log(data);
        }
      }
      
    });
  }

  viewPage(page){
    this.sharedServices.filterEngagements = false;
    this.sharedServices.selectedClientId = 0;
    this.router.navigate([page]);
  }

}
