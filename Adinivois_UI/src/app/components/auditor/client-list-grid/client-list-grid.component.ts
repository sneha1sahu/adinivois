import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatMenuTrigger, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { resolveComponentResources } from '@angular/core/src/metadata/resource_loading';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription, timer, pipe } from 'rxjs';


export interface PeriodicElement {
  businessname: string;
  engagement: number;
  statusname: string;
  sourcelink: string;
  contactperson: string;
  typeofentry: string;
  clientonboarding: string;
  actions: string;
}


@Component({
  selector: 'client-list-grid',
  templateUrl: './client-list-grid.component.html',
  styleUrls: ['./client-list-grid.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClientListGridComponent implements OnInit {

  ClienttListFilter = [
    { value: 'Clients', viewValue: 'All Clients' },
    { value: 'Assigned', viewValue: 'Assigned to me' },
    { value: 'Added', viewValue: 'Added by me' }
  ];

  displayedColumns = ['businessname', 'engagements', 'status', 'accountingSoftware', 'contactPerson', 'typeOfEntity', 'clientonboarding', 'actions'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  clientsData: any = [];
  clientsDataCopy: any = [];
  gridData: any;
  showQuickbooks: any = false;
  showXero: any = false;
  hidePagination: any = true;
  clientProfile: any;
  businessname: any;
  direction: number;
  isDesc: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('deleteClientDialog') deleteClientDialog: TemplateRef<any>;
  @ViewChild('inviteClientDialog') inviteClientDialog: TemplateRef<any>;
  @ViewChild('alertdeleteClientDialog') alertdeleteClientDialog: TemplateRef<any>; 
  column: any;
  clientFirmId: any;
  clientName: any;
  data: { "pemailadd": any; };
  resendEmail: any;
  resendBuisnessName: any;
  showAccepted: any;
  showInvited: any;
  showRejected: any;
  clientStatusDetails: any;
  active: any;
  invited: any;
  rejected: any;
  engagementCount: any;
  loggedInUserData: any;
  auditorId: any;
  clientsListSubscription: any;
  getListSubscription: any;
  searchString: any;
  searchSubscription: any;
  clientFilter: string;
  constructor(private apiService: ApiService, private sharedService: SharedService,
     public router: Router, public dialog: MatDialog, private toastr: ToastrService,
     private spinner: NgxSpinnerService) {
  }


  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.getClients();
    this.clientStatus();
    
    this.searchSubscription = this.sharedService.clientSearchString.subscribe(response => {
      if(response){
        this.searchString = response;
        this.gridData.filter = response.trim().toLowerCase();
      }
      else{
        this.searchString = '';
        this.gridData.filter = this.searchString;
      }
    })
    this.clientFilter='Clients'
  }

  getClients() {
  //  this.spinner.show();
    this.getListSubscription = this.apiService.getAllClients(this.auditorId).subscribe(response => {
      let responseObj: any = response
      if (responseObj && responseObj.data) {
       // this.spinner.hide();
        this.clientsData = responseObj.data;
        this.clientsData.forEach(item => {
          item.businessname = item.businessname.charAt(0).toUpperCase() + item.businessname.slice(1);
          if (item.clientonboarding) {
            item.clientonboarding = new Date(item.clientonboarding);
          }
        });
        this.clientsDataCopy = this.clientsData;
        this.bindGrid(this.clientsData);
        if(this.clientStatusDetails){
          let tempArray = [];
          tempArray = this.clientStatusDetails.filter(temp => {
            return temp.IsSelected;
          });
          if (tempArray && tempArray.length > 0) {
            this.clientStatusFilter('');
          }
        }
        
       // this.subscribeToClientsData();
      }
    }, error => {

    })
  }

  // subscribeToClientsData() {
  //   this.clientsListSubscription = timer(5000).subscribe(() => this.getClients())
  // }
  //client status filter
  clientStatusFilter(status) {
    if(status){
      status.IsSelected = !status.IsSelected
    }
    let clientStatusFilterArray = [];   
    
    let selectnone = [];
     selectnone=this.clientStatusDetails.filter(temp => {
      return temp.IsSelected
    });
    if (selectnone.length > 0) {
      this.clientStatusDetails.forEach(status => {
        this.clientsDataCopy.forEach(item => {
          if (item.statusname === status.statusname && status.IsSelected) {
            clientStatusFilterArray.push(item);
          }
        });
      });
    }
    else{
      clientStatusFilterArray= this.clientsDataCopy;

    }

    this.bindGrid(clientStatusFilterArray);
  }
  //client status filter end
  //
  filterAccSoftware() {
    let filtersArray = []; let filteredList = [];
    if (this.showQuickbooks) {
      filtersArray.push({ 'type': 'quickbooks' })
    }
    if (this.showXero) {
      filtersArray.push({ 'type': 'xero' })
    }
    if (filtersArray.length == 0) {
      this.clientsData = this.clientsDataCopy;
      this.bindGrid(this.clientsData);
    }
    else {
      filtersArray.forEach(item1 => {
        this.clientsDataCopy.forEach(item2 => {
          if (item1.type == item2.sourcelink) {
            filteredList.push(item2);
          }
        });
      });
      this.clientsData = filteredList;
      this.bindGrid(this.clientsData);
    }


  }

  bindGrid(list) {
    this.gridData = new MatTableDataSource(list);
    this.gridData.filterPredicate =
      (data: any, filter: string) => {
        return data['businessname'].toLowerCase().includes(filter)
      };
    this.sharedService.gridData = this.gridData;
    this.gridData.sort = this.sort;
    if(this.searchString)
    this.sharedService.clientSearchString.next(this.searchString)
    // this.gridData.paginator = this.paginator;
    // this.hidePagination = this.clientsData.length > 10 ? false : true;
  }
  inviteClientDialogbox(): void {
    const dialogRef = this.dialog.open(this.inviteClientDialog);

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      //  this.animal = result;
    });
  }

  editClient(client) {
    this.sharedService.selectedClient = client;
    this.router.navigate(['/dashboard/editclient']);
  }

  deleteClient(clientName, clientId,engagementcount) {
    this.engagementCount=engagementcount
    if (clientName && clientId) {
      this.clientFirmId = clientId;
      this.clientName = clientName
      if(engagementcount==0){
        const dialogRef = this.dialog.open(this.deleteClientDialog);
      dialogRef.afterClosed().subscribe(result => {
        //console.log('The dialog was closed');
      });
      }
      else{
        const dialogRef = this.dialog.open(this.alertdeleteClientDialog);
        dialogRef.afterClosed().subscribe(result => {
          //console.log('The dialog was closed');
        });
      }
      
    }

  }

  confirmDeleteClient() {
    this.apiService.deleteClient(this.clientFirmId, this.auditorId).subscribe(response => {
      if (response._statusCode == 200) {
        this.toastr.success(response._statusMessage);
        //alert("Client " + this.clientName + " is deleted successfully");
        this.getClients();
        this.sharedService.clientListCount.next()
      }
      else {
        //alert("Error occured please contact with Admin")
      }

    }, error => {
      this.toastr.error('error in delete');
      //alert('error in delete')
    })
  }
  engagementForClient(clientdata) {
    this.sharedService.isReadOnly = true;
    this.sharedService.selectedClient = clientdata;
    this.router.navigate(['/dashboard/createengagement']);
  }

  resendLinkToClient(email, name) {
    this.resendEmail = email;
    this.resendBuisnessName = name
    this.data = {
      "pemailadd": email
    }
    this.spinner.show();
    this.apiService.resendLinkToClient(this.data).subscribe(response => {
      if (response._statusCode == 200) {
        this.spinner.hide();
      //  alert("Client is invited successfully");
        this.inviteClientDialogbox();
      }
      else {
        this.spinner.hide();
        this.toastr.error("Client invitation failed");
        //alert("Client invitation is failed");
        //alert('client created successfully');
        //alert( "Error occured");
        //this.inviteClientDialogbox();
      }

    }
    );
  }

  clientStatus() {
    this.apiService.getClientStatus().subscribe(data => {
      this.clientStatusDetails = data['data'];

      this.active = this.clientStatusDetails[1].statusname;
      this.invited = this.clientStatusDetails[0].statusname;
      this.rejected = this.clientStatusDetails[2].statusname;
      this.clientStatusDetails.forEach(item => {
        item.IsSelected = false;
      });
      //console.log(this.clientStatusDetails);

    }, error => {
      this.toastr.error("error client status");
      //alert('error client status')
    });
  }

  showClientEngagements(client){
    this.sharedService.filterEngagements = true;
    this.sharedService.selectedClientId = client.clientfirmid;
    this.router.navigate(['/dashboard/engagement']);
  }

  ngOnDestroy() {
    if(this.getListSubscription){
      this.getListSubscription.unsubscribe();
    }
    if(this.clientsListSubscription){
      this.clientsListSubscription.unsubscribe();
    }
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }
  clinetListFilter(event){
    var id =event.value
    var filterId
    if(id=='Added'){
      filterId=1
      var clientFilteredData= this.clientsData.filter(x=>x.isowned == filterId)
    this.bindGrid(clientFilteredData);
    }
    else if(id=='Assigned'){
      filterId=0
      var clientFilteredData= this.clientsData.filter(x=>x.isowned == filterId)
      this.bindGrid(clientFilteredData);
    }
    else{
      this.bindGrid(this.clientsData)
    }
 

  }
}

