import { Component, ViewEncapsulation, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
  selector: 'add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})

export class AddClientComponent implements OnInit {

  animal: string;
  name: string;

  clientDetails: boolean = true;
  accountBook: boolean = false;
  priviewClientDetails: boolean = false;
  clientDetailsForm: FormGroup;
  accountBooksForm: FormGroup;
  clientDetailsObj : any = {};
  accountBooksObj: any = {};
  saveBtnText: any = 'Add Client';
  loggedInUserData: any;
  auditorId: any;
  @ViewChild('addClientDialog') addClientDialog: TemplateRef<any>;
  @ViewChild('inviteClientDialog') inviteClientDialog: TemplateRef<any>;
  @ViewChild('WatchoutforResponseDialog') WatchoutforResponseDialog: TemplateRef<any>;
  dialogRef: any;
  jurisdictionDetails: any;
  typeOfEntityDetails: any;
  selectedClientDetails: any;
  jurisdictionName: any;
  typeOfEntityName: any;
  customDateTitle: any = 'Incorporation Date';
  maxIncorpDate: any = new Date();
  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private apiService: ApiService,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    public router: Router,
    private sharedService: SharedService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.clientDetailsForm = this.fb.group({
      'businessName': new FormControl(),
      'contactPersonName': new FormControl("", [Validators.required]),
      'businessPhone': new FormControl("", [Validators.required]),
      'cellPhone': new FormControl("", [Validators.required]),
      'clientEmail': new FormControl("", [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'jurisdiction': new FormControl("", [Validators.required]),
      'typeOfEntity': new FormControl("", [Validators.required]),
      'incorporationDate': new FormControl("", Validators.required)
    });

    // this.accountBooksForm = this.fb.group({
    //   'accountbook': new FormControl("", [Validators.required]),
    //   'accesskey': new FormControl("", [Validators.required]),
    //   'secretkey': new FormControl("", [Validators.required]),
    // });
    this.getJurisdiction();
    this.getTypeOfEntity();
    if(this.router.url === '/dashboard/editclient'){
      this.saveBtnText = 'Update Client';
      this.selectedClientDetails = this.sharedService.selectedClient;
      if(this.selectedClientDetails && this.selectedClientDetails.clientfirmid){
        this.editClientForm();
      }
    }
  }

  clientDetailsFields() {
    this.clientDetails = false;
    this.accountBook = true;
  }

  accountBookFields() {
    this.clientDetails = false;
    this.accountBook = false;
    this.priviewClientDetails = true;
  }
  editAccountBooks(){
    this.clientDetails = false;
    this.accountBook = true;
    this.priviewClientDetails = false;
  }

  editClientDetails(){
    this.clientDetails = true;
    this.accountBook = false;
    this.priviewClientDetails = false;
  }
  
  onEntityChange(){
    let entity:any  = this.clientDetailsForm.get('typeOfEntity').value;
    if(entity == 2){
      this.customDateTitle = 'Formation Date';
    }
    else{
      this.customDateTitle = 'Incorporation Date';
    }
  }

  accounts: any[] = [
    { value: 'xero', viewValue: 'Xero' },
    { value: 'quickbooks', viewValue: 'QuickBooks' },
  ];

  validateClientDetails(){
    if (this.clientDetails && this.clientDetailsForm.valid) {
      this.clientDetailsObj = this.clientDetailsForm.value;
      this.saveClient();
      //
      // this.clientDetails = false;
      // this.accountBook = true;
      this.jurisdictionDetails.forEach(item => {
        if(item.jurisdictionid == this.clientDetailsObj.jurisdiction){
          this.jurisdictionName = item.provincesname;
        }
      });

      this.typeOfEntityDetails.forEach(item => {
        if(item.typeofentityid == this.clientDetailsObj.typeOfEntity){
          this.typeOfEntityName = item.typeofentityname;
        }
      });
    }
    else{
      if(new Date(this.clientDetailsForm.value.incorporationDate) > new Date()){
        let message = this.customDateTitle + " should not be greater than today's date";
        this.toastr.error(message);
      }
      
    }
  }

  validateAccountBooks(){
    if(this.accountBook && this.accountBooksForm.valid){
      this.accountBooksObj = this.accountBooksForm.value;
      this.clientDetails = false;
      this.accountBook = false;
      this.priviewClientDetails = true;
    }
  }

  gotoAccountBooks(){
    this.clientDetails = false;
    this.priviewClientDetails = false;
    this.accountBook = true;
  }
  saveClient(){
      if(this.clientDetailsObj.incorporationDate){
        this.clientDetailsObj.incorporationDate = this.datePipe.transform(this.clientDetailsObj.incorporationDate, 'yyyy-MM-dd');
      }

      if(this.router.url === '/dashboard/addclient'){
        let data: any = {
          "pbsname": this.clientDetailsObj.businessName,
          "pcontactperson": this.clientDetailsObj.contactPersonName,
          "pbphonenumber": this.clientDetailsObj.businessPhone,
          "pcphonenumber": this.clientDetailsObj.cellPhone,
          "pemailadd": this.clientDetailsObj.clientEmail,
          "ppwd":"welcome",
          "pjurisdictionid": this.clientDetailsObj.jurisdiction,
          "ptypeofentityid": this.clientDetailsObj.typeOfEntity,
          "pincorporationdate": this.clientDetailsObj.incorporationDate,
          "paccountbook": '-',
          "paccesskey": 'default',
          "psecretkey": 'default',
          "ploginid": this.auditorId
        }
        
        this.spinner.show();
        this.apiService.createClient(data).subscribe(response => {
          if (response._statusCode == 200) {
            this.spinner.hide();
            //this.toastr.success(response._statusMessage);
            this.addClientDialogbox();
          }
          else if(response._statusCode==400){
            this.spinner.hide();
            this.toastr.error(response._statusMessage);
          }

          else{
            this.spinner.hide();
            this.toastr.error(response._statusMessage);
          }
          
        }, error => {
          this.toastr.error('error in creating client');
        });
      }
      else{
        this.updateClient();
      }
      
      

    
  }

  public handlePhone (event, type) {
    const onlyNums = event.target.value.replace(/[^0-9]/g, '');
    if (onlyNums.length < 10) {
      if(type=='cell'){
        this.clientDetailsForm.controls['cellPhone'].setValue(onlyNums);
        //this.cellPhone.setValue(onlyNums); 
      }
      else if(type=='business'){
        //this.businessPhone.setValue(onlyNums);
        this.clientDetailsForm.controls['businessPhone'].setValue(onlyNums);
      }
      else{
        return;
      }
    } else if (onlyNums.length === 10) {
      const number = onlyNums.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
      if(type=='cell'){
        this.clientDetailsForm.controls['cellPhone'].setValue(number);
        //this.cellPhone.setValue(number);
      }
      else if(type=='business'){
        this.clientDetailsForm.controls['businessPhone'].setValue(number);
        //this.businessPhone.setValue(number);
      }
      else{
        return;
      }
    }
  };

  addClientDialogbox(): void {
    const dialogRef = this.dialog.open(this.addClientDialog, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.animal = result;
    });
  }
  inviteClientDialogbox(): void {
    const dialogRef = this.dialog.open(this.inviteClientDialog);

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.animal = result;
    });
  }
  WatchoutforResponseDialogbox(): void {
    const dialogRef = this.dialog.open(this.WatchoutforResponseDialog);

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.animal = result;
    });
  }
  getJurisdiction(){
    this.apiService.getJurisdiction().subscribe(data=>{
      this.jurisdictionDetails=data['data'];
      console.log(this.jurisdictionDetails)
    })
  }
  getTypeOfEntity(){
    this.apiService.getTypeOfEntity().subscribe(data=>{
      this.typeOfEntityDetails=data['data'];
      console.log(this.typeOfEntityDetails)
    })
  }

  editClientForm(){
    this.clientDetailsForm.setValue({
      'businessName': this.selectedClientDetails.businessname,
      'contactPersonName': this.selectedClientDetails.contactperson,
      'businessPhone': this.selectedClientDetails.businessphonenumber,
      'cellPhone': this.selectedClientDetails.cellphonenumber,
      'clientEmail': this.selectedClientDetails.emailaddress,
      'jurisdiction': this.selectedClientDetails.jurisdictionid,
      'typeOfEntity': this.selectedClientDetails.typeofentotyid,
      'incorporationDate': this.selectedClientDetails.incorportiondate
    });
    this.onEntityChange();
    
    // this.accountBooksForm.setValue({
    //   'accountbook': this.selectedClientDetails.sourcelink,
    //   'accesskey': this.selectedClientDetails.clientid,
    //   'secretkey': this.selectedClientDetails.secretkey,
    // });
  }

  updateClient(){
    let data: any = {
      "pclientfirmid": this.selectedClientDetails ? this.selectedClientDetails.clientfirmid : 0,
      "pbsname": this.clientDetailsObj.businessName,
      "pcontactperson": this.clientDetailsObj.contactPersonName,
      "pbphonenumber": this.clientDetailsObj.businessPhone,
      "pcphonenumber": this.clientDetailsObj.cellPhone,
      "pemailadd": this.clientDetailsObj.clientEmail,
      "pjurisdictionid": this.clientDetailsObj.jurisdiction,
      "ptypeofentityid": this.clientDetailsObj.typeOfEntity,
      "pincorporationdate": this.clientDetailsObj.incorporationDate,
      "paccountbook": 'quickbooks',
      "paccesskey": 'default',
      "psecretkey": 'default',
      "ploginid": this.auditorId
    }
    this.spinner.show();
    this.apiService.updateClient(data).subscribe(response => {
      if (response._statusCode == 200) {
        this.spinner.hide();
        //this.toastr.success(response._statusMessage);
        this.addClientDialogbox();        
      }
      else{
        this.spinner.hide();
        this.toastr.error(response._statusMessage);
      }
      
    }, error => {
      this.toastr.error('error in updating client');
    });
  }
}
