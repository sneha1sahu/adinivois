import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild  } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatMenuTrigger, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SharedService } from '../../shared/shared.service';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Observable} from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
export interface PeriodicElement {
  engagementName: string;
  clientName: string;
  type: string;
  yearEnd: string;
  assignedTo: string;
  status: string;
  dateCreated: string;
  actions: string;
}

@Component({
  selector: 'engagements-list-grid',
  templateUrl: './engagements-list-grid.component.html',
  styleUrls: ['./engagements-list-grid.component.scss'],
  providers:[DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class EngagementsListGridComponent implements OnInit {
  //tbYear = new FormControl();
  //tbYearList: string[] = ['All Engagements', 'Assigned to me ', 'Created by me'];
  engagementListFilter= [
    { value: 'Engagements', viewValue: 'All Engagements' },
    { value: 'Assigned', viewValue: 'Assigned to me' },
    { value: 'Created', viewValue: 'Created by me' }
  ];
  displayedColumns = ['engagementName', 'clientName', 'engagementtype', 'yearEnd', 'assignedTo', 'status', 'dateCreated', 'actions'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  @ViewChild(MatSort) sort: MatSort;
  engagementData:any[];
  
  gridDataEngage: any;
  filterEngagementData:any;
  clientNameForDelete:any
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('deleteEngagementDialog') deleteEngagementDialog: TemplateRef<any>;
  hidePagination: any = true;
  engagementId:any;
  preparerName: string;
  firstReviewerName: string;
  secondReviewerName: string;
  adminName: string;

  engagementsStatusLabels: any;
  completed: any;
  isProgress: any;
  newsts: any;
  loggedInUserData: any;
  auditorId: any;
  EngagementYearDetails: any;
  trialBalYear: any;
  enagementFilter: string;
  constructor(private sharedService:SharedService,
    private router:Router
    ,private apiservice:ApiService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.getAllengagement();
    this.engagementsStatusLbl();
    this.enagementFilter='Engagements'
  }
  editEngagement(value){
    this.sharedService.selectedClient =  value;
    this.router.navigate(['/dashboard/editengagement']);
  }
  deleteEngagement(id,name){
    this.engagementId=id
    this.clientNameForDelete=name
    if(id){

      const dialogRef = this.dialog.open(this.deleteEngagementDialog);
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }
  confirmDeleteEngagement() {
    const dialogRef = this.dialog.closeAll();
    this.apiservice.deleteEngagement(this.engagementId, this.auditorId).subscribe(response=>{
      if (response._statusCode == 200) {        
        //alert("engagement is deleted successfully");
        this.toastr.success(this.clientNameForDelete +" engagement is deleted successfully");
        this.getAllengagement();
        this.sharedService.clientListCount.next()
      }
      else{
        this.toastr.error(this.clientNameForDelete+response._statusMessage);
       // alert( this.clientNameForDelete+response._statusMessage);
      }
      
    }, error => {
      this.toastr.error("error in deleting engagement");
    });
     // alert( this.clientNameForDelete+"is deleted successfully");
  }
  getAllengagement(){
   // this.spinner.show();
  this.apiservice.getAllengagement(this.auditorId).subscribe(response=>{
    if (response._statusCode == 200) { 
      this.engagementData=response['data'];
      if(this.sharedService.filterEngagements){
        let clientID = this.sharedService.selectedClientId
        if(clientID){
          let filteredList = this.engagementData.filter(
            m => m.clientfirmid == clientID );
          this.engagementData = filteredList;
        }
      }
     // this.spinner.hide();
     }
     else{
    //  this.spinner.hide();
     }
    this.filterEngagementData = this.engagementData.map(x => {
      var res = {
        "auditorData" :x.auditor_list
        };
          
        var payloadArray = JSON.parse(res.auditorData);
      // if(payloadArray){
      //   this.preparerName=payloadArray[0].fullname,
      //   this.firstReviewerName=payloadArray[1].fullname,
      //   this.secondReviewerName=payloadArray[2].fullname,
      //   this.adminName=''
      // }
      var adminarr=[]
      if(payloadArray!=null){
        payloadArray.map((y,index)=>{
          if(index==0){
            this.preparerName=y.fullname;
          }
          if(index==1){
            this.firstReviewerName=y.fullname;
          }
          if(index==2){
            this.secondReviewerName=y.fullname
          }
          if(y.auditroleid>=11){
           adminarr.push(y.fullname)
           // const adminname=x.fullname
          
          }
        })
      }
 
  this.adminName=adminarr[0];
      return {
        'engagementName':x.engagementname.charAt(0).toUpperCase() + x.engagementname.slice(1),
        'clientName':x.businessname,
        'clientfirmid':x.clientfirmid,
        'engagementsid':x.engagementsid, //added
        'pengagementtypeid':x.engagementtypeid, //change from  engagementid to engagementtypeid
        'engagementtype':x.engagementtype,
        'yearEnd':x.finanicalyearenddate,
        'assignedTo':'View Assignees',
        'status':x.statusname,
        'dateCreated':x.engcreateddate,
        'Action':'',
        'additionalinfo':x.additionalinfo,
        'auditor_list':x.auditor_list,
        'compilationtype':x.compilationtype,
        'incorporationdate':x.incorporationdate,
        'subenties':x.subenties,
        'contactperson':x.contactperson,
        'useraccountid':x.useraccountid,
        'statusid': x.statusid,
        'tbloadstatusid': x.tbloadstatusid,
        'preparer': this.preparerName,
        'firstReviewerName': this.firstReviewerName,
        'secondReviewerName':this.secondReviewerName,
        'Admin':this.adminName,
        'isowned':x.isowned
        
      

      }
    })
    this.bindGrid(this.filterEngagementData);
  })
  }
  bindGrid(list){
    this.gridDataEngage = new MatTableDataSource(list);
    this.sharedService.gridDataEngage = this.gridDataEngage;
    this.gridDataEngage.sort = this.sort;
    this.gridDataEngage.paginator = this.paginator;
    this.hidePagination = this.engagementData.length > 10 ? false : true;
  }
  assignee(data){
    console.log(data);
  }

  engagementsStatusLbl() {
    this.apiservice.getEngagementStatus().subscribe(data => {
      this.engagementsStatusLabels = data['data'];

      this.completed = this.engagementsStatusLabels[1].statusname;
      this.isProgress = this.engagementsStatusLabels[0].statusname;
      this.newsts = this.engagementsStatusLabels[2].statusname;
      this.engagementsStatusLabels.forEach(item => {
        item.IsSelected = false;
      });

    }, error => {
      this.toastr.error("error client status");
    });

  }

  //Engagements status filter
  engagementsStatusFilter(e, selected) {

    let engagementsStatusFilterArray = [];
    let tempStatus = this.engagementsStatusLabels;
    tempStatus.forEach(item => {
      if (item.statusname === selected) {
        if (e) {
          item.IsSelected = true;
        }
        else {
          item.IsSelected = false;
        }
      }
    });
    let selectnone = [];
    selectnone = tempStatus.filter(temp => {
      return temp.IsSelected
    });
    if (selectnone.length > 0) {
      tempStatus.forEach(status => {
        this.filterEngagementData.forEach(item => {
          if (item.status === status.statusname && status.IsSelected) {
            engagementsStatusFilterArray.push(item);
          }
        });
      });
    }
    else {
      engagementsStatusFilterArray = this.filterEngagementData;

    }

    this.bindGrid(engagementsStatusFilterArray);
  }
  //engagements status filter end
  redirectToTrialBal(engagement){
     this.sharedService.engagementId = engagement.engagementsid;
     this.sharedService.engagementDetails = engagement;
     this.sharedService.headerObj.next(engagement);
   //  this.sharedService.selectedEnagagementObj.next(engagement);
     localStorage.setItem('incorporationDate',engagement.incorporationdate)
     localStorage.setItem('engagementID',engagement.engagementsid);
     localStorage.setItem('engagementClientID',engagement.clientfirmid);
     
     localStorage.setItem('selectedEngagementObj', JSON.stringify(engagement));
     
  
       this.apiservice.getEngagementYearDetails(engagement.engagementsid).subscribe(response=>{
         if(response['_statusCode'] == 200){
           this.EngagementYearDetails=response['data'];
           if(this.EngagementYearDetails && this.EngagementYearDetails.length > 0){
            let index = this.EngagementYearDetails.length - 1;
             this.trialBalYear= this.EngagementYearDetails[index].acctyear;
             localStorage.setItem('trailBalYear',this.trialBalYear)
           }
         }
        //  else if(response['_statusCode'] == 404){
        //    localStorage.setItem('trailBalYear','')
        //  }
         })
     localStorage.setItem('endYear',engagement.yearEnd); //both are same
     this.router.navigate(['/dashboard/ntrdashboard/ClientAcceptance']);
   }
   engagementFilter(event){
    var id =event.value
    var filterId
    if(id=='Created'){
      filterId=1
      var engagementFilterData= this.filterEngagementData.filter(x=>x.isowned == filterId)
     this.bindGrid(engagementFilterData);
     // this.gridDataEngage=engagementFilterData
    }
    else if(id=='Assigned'){
      filterId=0
      var engagementFilterData= this.filterEngagementData.filter(x=>x.isowned == filterId)
      this.bindGrid(engagementFilterData);
    }
    else{
      this.bindGrid(this.filterEngagementData)
    }
 
   }
}
