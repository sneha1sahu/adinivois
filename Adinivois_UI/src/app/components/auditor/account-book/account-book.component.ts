import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { ApiService } from '../../../services/api.service';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-book',
  templateUrl: './account-book.component.html',
  styleUrls: ['./account-book.component.scss']
})
export class AccountBookComponent implements OnInit {
  public token: any;
  public quickbookStatus: any;
  public xeroStatus: any;
  public statusMessage: any = 'Error in Connection';
  constructor(private sharedService: SharedService, private apiService: ApiService, private router: Router, private route: ActivatedRoute, private toastr: ToastrService) { 
    this.route.queryParams.subscribe(params => {
      this.quickbookStatus = params['quickbooks'];
      this.xeroStatus = params['xero'];
    });
  }

  ngOnInit() {
    sessionStorage.removeItem("userDetails");
    this.token = this.sharedService.clientToken;
    if(this.quickbookStatus == 1){
      this.statusMessage = 'Connected to Quickbooks Succesfully';
      //this.toastr.success('Connected to Quickbooks Succesfully');
    }
    else if(this.xeroStatus == 1){
      this.statusMessage = 'Connected to Xero Succesfully';
      //this.toastr.success('Connected to Xero Succesfully');
    }
    else if (this.quickbookStatus == 0){
      this.statusMessage = 'Error in connecting to Quickbooks';
      //this.toastr.error('Error in connecting to Quickbooks');
    }
    else if (this.xeroStatus == 0){
      this.statusMessage = 'Error in connecting to Xero'
      //this.toastr.error('Error in connecting to Xero');
    }
    setTimeout(() => { this.router.navigate(['/']); }, 2000);
  }

  quickbookConnect(){
    if(this.token){
      let connectionUrl = environment.apiEndpoint + '/quickbook/connect?tokenValue=' + this.token;
      window.open(connectionUrl,"_self")
    }
    
    // if(this.token){
    //   this.apiService.quickbookConnect(this.token).subscribe(response => {
    //     if (response._statusCode == 200) {
    //     }
    //   }, error => {

    //   })
    // }
    
  }
  xeroConnect(){
    if(this.token){
      let connectionUrl = environment.apiEndpoint + '/xero/connect?tokenValue=' + this.token;
      window.open(connectionUrl,"_self")
    }
  }
}
