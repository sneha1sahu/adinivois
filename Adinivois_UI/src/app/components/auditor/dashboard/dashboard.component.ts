import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  toggleMenu: boolean = true;

  constructor(private sharedServices: SharedService, private router: Router ) { }

  ngOnInit() {
  }

  toggleDrawer() {
    this.toggleMenu = !this.toggleMenu;
  }
  onRouteChange(){
    let routeData = {'url': this.router.url}
    this.sharedServices.routeObj.next(routeData);
  }

}
