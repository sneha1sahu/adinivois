import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-financial-statement',
  templateUrl: './financial-statement.component.html',
  styleUrls: ['./financial-statement.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FinancialStatementComponent implements OnInit {

  constructor() { }
  selectedTab: any = 1;
  ngOnInit() {
    this.selectedTab = 1
  }

  switchTabs(tab){
    this.selectedTab = tab.index + 1;
  }
}
