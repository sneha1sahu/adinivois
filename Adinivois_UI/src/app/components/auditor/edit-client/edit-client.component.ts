import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit {
  selectedClientDetails: any;
  constructor(private sharedService: SharedService, public router: Router) { }
  
  ngOnInit() {
    this.selectedClientDetails = this.sharedService.selectedClient;
    if(this.selectedClientDetails && !this.selectedClientDetails.clientfirmid){
      this.router.navigate(['/dashboard/client']);
    }
    //console.log(this.selectedClientDetails);
  }

}
