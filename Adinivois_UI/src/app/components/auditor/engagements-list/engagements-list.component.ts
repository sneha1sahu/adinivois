import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../shared/shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'engagements-list',
  templateUrl: './engagements-list.component.html',
  styleUrls: ['./engagements-list.component.scss']
})
export class EngagementsListComponent implements OnInit {
  engagementCountDetails: any;
  totalengagement: any;
  current: any;
  completed: any;
  loggedInUserData: any;
  auditorId: any;
  updatedEngagementActivities: Subscription;

  constructor(private apiservice: ApiService, private toastr: ToastrService, private sharedService: SharedService) { }

  ngOnInit() {
    this.loggedInUserData = JSON.parse((sessionStorage.getItem('userDetails')));
    if(this.loggedInUserData && this.loggedInUserData.useracctid){
      this.auditorId = this.loggedInUserData.useracctid
    }
    this.getEngagementCountDetail();

    this.updatedEngagementActivities = this.sharedService.clientListCount.subscribe(response => {
      this.getEngagementCountDetail();
    })

  }
  getEngagementCountDetail() {
    this.apiservice.getEngagementCountDetail(this.auditorId).subscribe(response => {
      if (response._statusCode == 200) {
        this.engagementCountDetails = response['data'];
        this.engagementCountDetails = this.engagementCountDetails[0]
        this.totalengagement = this.engagementCountDetails.totalengagement
        this.current = this.engagementCountDetails.current
        this.completed = this.engagementCountDetails.completed
      }
      else {
        this.toastr.error("error coccured");
        //alert("error coccured");
      }
    })
  }
  //

  ngOnDestroy() {
    this.updatedEngagementActivities.unsubscribe;
  }

}
