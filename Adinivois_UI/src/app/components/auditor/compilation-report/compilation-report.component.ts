import { Component, ViewEncapsulation, OnInit, TemplateRef, ViewChild  } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../services/api.service';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-compilation-report',
  templateUrl: './compilation-report.component.html',
  styleUrls: ['./compilation-report.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class CompilationReportComponent implements OnInit {
  config: any = {
    height: 250,
    theme: 'modern',
    // powerpaste advcode toc tinymcespellchecker a11ychecker mediaembed linkchecker help
    plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image imagetools link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount contextmenu colorpicker textpattern',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent fontselect fontsizeselect | removeformat',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    menubar:false,
    statusbar: false,
    // templates: [
    //   { title: 'Test template 1', content: 'Test 1' },
    //   { title: 'Test template 2', content: 'Test 2' }
    // ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  };
  @ViewChild('textEditor') textEditor: TemplateRef<any>;
  @ViewChild('SampleImages') SampleImages: TemplateRef<any>;
  constructor(public dialog: MatDialog, private apiService: ApiService, private sharedService: SharedService, public router: Router, private datePipe: DatePipe) { }
  editorContent: any = '';
  selectedElement: any = '';
  dialogRef: any;
  letterBody: any;
  reportHeader: any;
  topLeftContent: any;
  bottomRightContent: any;
  coverImage: any = '../../../../assets/images/adinovis_logo.png';
  imageFile: any;
  signatureImage: any = '../../../../assets/images/signatureimg.png';
  signatureFile: any;
  engagementID: any;
  savedData: any = {};
  images: any;
  engagementClientID: any;

  ngOnInit() {
  this.getJSON();
  let selectedEngagement = JSON.parse((localStorage.getItem('selectedEngagementObj')));
  let clientName = selectedEngagement ? selectedEngagement.clientName : 'ABC Company';
  
  this.letterBody = "<p>On the basis of information provided by management, we have compiled the balance sheet of ABC Company as at December 31, 20X1, the statement of income and retained earnings for the year then ended, and Note X, which describes the basis of accounting applied in the preparation of the financial information, [and other explanatory information] (“financial information”).</p>" +
      "<p>Management is responsible for the accompanying financial information, including the accuracy and completeness of the underlying information used to compile it.</p>"+
      "<p>We performed this engagement in accordance with Canadian Standard on Related Services (CSRS) 4200, Compilation Engagements, which requires us to comply with relevant ethical requirements. Our responsibility is to assist management in the preparation and presentation of the financial information of the entity.</p>"+
      "<p>We did not perform an audit engagement or a review engagement, nor were we required to perform procedures to verify the accuracy or completeness of the information provided by management. Accordingly, we do not express an audit opinion or a review conclusion, or provide any form of assurance on the financial information.</p>"+
      "<p>Readers are cautioned that the financial information may not be appropriate for their purposes.</p>";
  this.reportHeader = 'COMPILATION ENGAGEMENT REPORT To Management of ' +  clientName;
  this.topLeftContent = '<p>To Management </p>' + '<p>' + clientName + '</p><p>Henderson, Nevada</p>';
  
  this.images = [{name: 'https://image.freepik.com/free-vector/logo-sample-text_355-558.jpg'}, {name:'https://i.pinimg.com/originals/7c/51/98/7c5198d2a0751fa76c8433dba4a1a12a.jpg'}, {name:'https://www.nolimitlogic.com/img/samples/business-logos/045.jpg'}]
  let todayDate:any = new Date();
  todayDate = this.datePipe.transform(todayDate, 'MMM dd, yyyy');
  this.bottomRightContent = '<p>' + todayDate + '</p>' + '<p>Red Oak LLC,</p><p>Las Vegas,</p><p> Nevada</p>';
}

selectImage(img){
  this.images.forEach(item => {
    item.selected = false;
  });
  img.selected = true;
}
showImage(){
  this.images.forEach(item => {
    if(item.selected){
      this.coverImage = item.name;
    }
  });
  this.dialogRef.close();
  this.saveJSON();
  
}
public getJSON(){
  // this.engagementID = this.sharedService.engagementId;
  this.engagementID = parseInt(localStorage.getItem('engagementID'));
  this.engagementClientID = parseInt(localStorage.getItem('engagementClientID'));

  let url = 'https://s3.amazonaws.com/' + 'adinovisclientdocs/Clients/' + this.engagementClientID + '_Client/' + this.engagementID + '_engagements/' + 'FinancialStatement/' + this.engagementID + '_statement.json';
  this.apiService.getFinancialDocumentDetailsFromJSON(url).subscribe(response =>{
    this.savedData = response;
      if(this.savedData && this.savedData.compilationReport){
        this.reportHeader = this.savedData.compilationReport.reportHeader ? this.savedData.compilationReport.reportHeader : this.reportHeader;
        this.topLeftContent = this.savedData.compilationReport.topLeftContent ? this.savedData.compilationReport.topLeftContent : this.topLeftContent;
        this.letterBody = this.savedData.compilationReport.letterBody ? this.savedData.compilationReport.letterBody : this.letterBody;
        this.bottomRightContent = this.savedData.compilationReport.bottomRightContent ? this.savedData.compilationReport.bottomRightContent : this.bottomRightContent;
        this.coverImage = this.savedData.compilationReport.topImage ? this.savedData.compilationReport.topImage : '../../../../assets/images/adinovis_logo.png';
        this.signatureImage = this.savedData.compilationReport.bottomImage ? this.savedData.compilationReport.bottomImage : '../../../../assets/images/signatureimg.png';
      }
  }, error => {
    console.log(error);
    this.savedData = {
      "coverPage": {"logo": "", "businessName": "", "financialYear": "", "date": ""},
      "compilationReport": {"reportHeader": "", "topLeftContent": "", "letterBody": "", "bottomRightContent": "", "topImage": "", "bottomImage": ""},
      "page3": {"title1": "", "title2": ""}
    }
  })
}
openEditor(elementName){
  if(this.router.url !== '/dashboard/financialstatementexport'){
    if(elementName == 'reportHeader'){
      this.editorContent = this.reportHeader;
    }
    else if(elementName == 'reportTopLeftContent'){
      this.editorContent = this.topLeftContent;
    }
    else if(elementName == 'letterBody'){
      this.editorContent = this.letterBody;
    }
    else if(elementName == 'reportBottomRightContent'){
      this.editorContent = this.bottomRightContent;
    }
    this.selectedElement = elementName;
    this.dialogRef = this.dialog.open(this.textEditor);
  }
  
  
}
doneEditing(){
  if(this.selectedElement == 'reportHeader'){
    this.reportHeader = this.editorContent;
  }
  else if(this.selectedElement == 'reportTopLeftContent'){
    this.topLeftContent = this.editorContent;
  }
  else if(this.selectedElement == 'letterBody'){
    this.letterBody = this.editorContent;
  }
  else if(this.selectedElement == 'reportBottomRightContent'){
    this.bottomRightContent = this.editorContent;
  }
  this.dialogRef.close();
  this.saveJSON();
}
closeDialog(){
  this.dialogRef.close();
}
uploadletterImage(event) {
  if (event.target.files && event.target.files.length > 0) {
    const file = event.target.files[0];
    this.imageFile  = file;
    const reader = new FileReader();
    reader.onload = e => this.coverImage = reader.result;
    reader.readAsDataURL(file);
    this.saveImage(this.imageFile, 'top');
  }
}
uploadsignatureImage(event) {
  if (event.target.files && event.target.files.length > 0) {
    const file = event.target.files[0];
    this.signatureFile  = file;
    const reader = new FileReader();
    reader.onload = e => this.signatureImage = reader.result;
    reader.readAsDataURL(file);
    this.saveImage(this.signatureFile, 'btm');
  }
}
saveImage(imgFile, tag){
  if(imgFile){
    let formData:FormData = new FormData();
    let imageName = this.engagementID +'_' + tag + '_' + imgFile.name;
    let s3signatureKey = 'profilepicture/fd/' + imageName;
    formData.append('bucketname', 'adinovisemailtemplateimage');
    formData.append('signatureKey', s3signatureKey);
    formData.append('file', imgFile, imageName);
    this.apiService.saveProfileImageToS3(formData).subscribe(response => {
      if (response._statusCode == 200) {
        if(tag == 'top'){
          this.coverImage = 'https://s3.amazonaws.com/adinovisemailtemplateimage/profilepicture/fd/' + imageName;
        }
        else{
          this.signatureImage = 'https://s3.amazonaws.com/adinovisemailtemplateimage/profilepicture/fd/' + imageName;
        }
        
        this.saveJSON();
      }
      else {

      }
    }, error => {

    });
  }
}
saveJSON(){
  // let financialDoc= {
  //   "coverPage": {"logo": "sample.jpg", "businessName": this.businessName, "financialYear": this.financialYear, "date": this.documentDate},
  //   "compilationReport": {"reportHeader": "", "topLeftContent": "", "letterBody": "", "bottomRightContent": ""},
  //   "page3": {"title1": "", "title2": ""}
  // }
  this.savedData.compilationReport = {"reportHeader": this.reportHeader, "topLeftContent": this.topLeftContent, "letterBody": this.letterBody, "bottomRightContent": this.bottomRightContent, "topImage": this.coverImage, "bottomImage": this.signatureImage }
  let jsonString = JSON.stringify(this.savedData);
  var blob = new Blob([jsonString], {type: "text/json;charset=utf-8"});

  let formData:FormData = new FormData();
  let jsonFileName = this.engagementID + '_statement.json';
  let s3signatureKey = 'Clients/' + this.engagementClientID + '_Client/' + this.engagementID + '_engagements/' + 'FinancialStatement/' + jsonFileName;
  // formData.append('bucketname', 'adinovisemailtemplateimage');
  let status: any = 0
  formData.append('status', status);
  formData.append('signatureKey', s3signatureKey);
  formData.append('file', blob, jsonFileName);
  this.apiService.saveFinancialDocumentJSON(formData).subscribe(response =>{

  }, error => {

  })
}

showImages(){
  this.dialogRef = this.dialog.open(this.SampleImages);
}

}
