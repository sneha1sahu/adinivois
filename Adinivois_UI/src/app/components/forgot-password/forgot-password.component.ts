import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private apiService: ApiService, private toastr: ToastrService,private spinner: NgxSpinnerService) { }
  emailAddress: any = '';
  showForgotForm: any = true;
  ngOnInit() {
  }

  forgotPassword(){
    if(this.emailAddress){
      let data: any = {
        pemailadd: this.emailAddress,
      }
      this.spinner.show();
      this.apiService.forgotPassword(data).subscribe(response => {
        if(response._statusCode == 200){
          this.spinner.hide();
          this.showForgotForm = false;
          //this.toastr.success(response._statusMessage);
        }
        else {
          this.spinner.hide();
          this.toastr.error(response._statusMessage);
          //alert(response._statusMessage);
        }
        
      }, error => {
        this.spinner.hide();
        this.toastr.error('error');
      });
    }
  }
}
