import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { DashboardComponent } from './components/auditor/dashboard/dashboard.component';
import { AddClientComponent } from './components/auditor/add-client/add-client.component';
import { EditClientComponent } from './components/auditor/edit-client/edit-client.component';
import { ClientListComponent } from './components/auditor/client-list/client-list.component';
import { AuthGuard } from './services/auth.guard';
import { EngagementsListComponent } from './components/auditor/engagements-list/engagements-list.component';
import { CreateEngagementComponent } from './components/auditor/create-engagement/create-engagement.component';
import { EditEngagementComponent } from './components/auditor/edit-engagement/edit-engagement.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { InviteClientComponent } from './components/auditor/invite-client/invite-client.component';
import { ProfileComponent } from './components/auditor/profile/profile.component';
import { ActiveComponent } from './components/active/active.component';
import { TrailbalanceComponent } from './components/auditor/trailbalance/trailbalance.component';
import { AccountBookComponent } from './components/auditor/account-book/account-book.component';
import { PreviewReportComponent } from './components/auditor/preview-report/preview-report.component';
import { S3uploadsComponent } from './components/s3uploads/s3uploads.component';
import { NtrDashboardComponent } from './components/auditor/ntr-dashboard/ntr-dashboard.component';
import { TrailbalanceDetailsComponent } from './components/auditor/trailbalance-details/trailbalance-details.component';
import { FinancialstatementExportComponent } from './components/auditor/financialstatement-export/financialstatement-export.component'
import { BalanceSheetPreviewComponent } from './components/auditor/balance-sheet-preview/balance-sheet-preview.component'
import { IncomeSheetPreviewComponent } from './components/auditor/income-sheet-preview/income-sheet-preview.component';
import { ReportsComponent } from './components/auditor/reports/reports.component';
import { ProceduresComponent } from './components/auditor/procedures/procedures.component';
import { TaxComponent } from './components/auditor/tax/tax.component';
import { CompletionSignOffComponent } from './components/auditor/completion-sign-off/completion-sign-off.component';
import { ClientAcceptanceComponent } from './components/auditor/client-acceptance/client-acceptance.component';
import { EngagementLetterComponent } from './components/auditor/engagement-letter/engagement-letter.component';
import { ClientOnboardingPlanningComponent } from './components/auditor/client-onboarding-planning/client-onboarding-planning.component';
import { CoverPageComponent } from './components/auditor/cover-page/cover-page.component';
import { TableOfContentsComponent } from './components/auditor/table-of-contents/table-of-contents.component';
import { CompilationReportComponent } from './components/auditor/compilation-report/compilation-report.component';
import { NotesToFinancialStatementComponent } from './components/auditor/notes-to-financial-statement/notes-to-financial-statement.component';
import { StatementCashFlowsComponent } from './components/auditor/statement-cash-flows/statement-cash-flows.component';
import { NtrReportsComponent } from './components/auditor/ntr-reports/ntr-reports.component';
export const routes: Routes = [
  //{ path: '', redirectTo: 'WelcomeComponent', pathMatch: 'full' },
  { path: '', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'forgotpassword', component: ForgotPasswordComponent },
  { path: 'resetpassword', component: ResetPasswordComponent },
  { path: 'clientlogin', component: InviteClientComponent },
  { path: 'active', component: ActiveComponent },
  { path: 'accountbooks', component: AccountBookComponent },
  { path: 's3uploads', component: S3uploadsComponent },
  // { path: 'dashboard', component: DashboardComponent },
  {
    path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [

      { path: '', redirectTo: 'client', pathMatch: 'full' },
      {
        path: 'client',
        component: ClientListComponent
      },
      {
        path: 'addclient',
        component: AddClientComponent
      },
      {
        path: 'editclient',
        component: EditClientComponent
      },
      {
        path: 'engagement',
        component: EngagementsListComponent
      },
      {
        path: 'createengagement',
        component: CreateEngagementComponent
      },
      {
        path: 'editengagement',
        component: EditEngagementComponent
      },
      {
        path: 'myaccount',
        component: ProfileComponent
      },
      {
        path: 'prviewreport',
        component: PreviewReportComponent
      },
      {
        path: 'ntrdashboard',
        component: NtrDashboardComponent, children: [
          {
            path: 'ClientAcceptance',
            component: ClientAcceptanceComponent
          },
          {
            path: 'EngagementLetter',
            component: EngagementLetterComponent
          },
          {
            path: 'Planning',
            component: ClientOnboardingPlanningComponent
          },
          {
            path: 'TrialBalance',
            component: TrailbalanceDetailsComponent
          },
          {
            path: 'Procedure',
            component: ProceduresComponent
          },
          {
            path: 'CoverPage',
            component: CoverPageComponent
          },
          {
            path: 'TableOfContents',
            component: TableOfContentsComponent
          },
          {
            path: 'CompilationReport',
            component: CompilationReportComponent
          },
          {
            path: 'BalanceSheet',
            component: BalanceSheetPreviewComponent
          },
          {
            path: 'IncomeStatement',
            component: IncomeSheetPreviewComponent
          },
          {
            path: 'StatementCashFlows',
            component: StatementCashFlowsComponent
          },
          {
            path: 'NotesToFinancialStatement',
            component: NotesToFinancialStatementComponent
          },
          {
            path: 'CompletionSignOff',
            component: CompletionSignOffComponent
          },
          {
            path: 'Tax',
            component: TaxComponent
          },

          {
            path: 'Report',
            component: NtrReportsComponent
          }
        ]
      },
      {
        path: 'financialstatementexport',
        component: FinancialstatementExportComponent
      },

    ]
  },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }


