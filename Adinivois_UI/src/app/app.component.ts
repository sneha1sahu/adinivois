import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  loginTrue: any = false
  constructor(private router: Router,private activatedRoute: ActivatedRoute,private titleService: Title) {

    this.titleService.setTitle('');
    let currentRoute = this.router.url;
    let routeMatch = false;
    // this.router.config.forEach((item) => {
    //   if('/' + item.path == currentRoute){
    //     routeMatch = true;
    //   }
    // });
    try {
      this.router.events.forEach((event) => {
        if (event instanceof NavigationStart) {
          if (event['url'] == '/' || event['url'] == '/login' || event['url'] == '/signup' || event['url'] == '/forgotpassword') {
            this.loginTrue = false;
          } else { 
            this.loginTrue = true;
          }
        }
      });
    }
    catch (e) {
      this.loginTrue = false;
    }

  }

  ngOnInit() {
    /*Dynamic title change*/
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).pipe(map(() => this.activatedRoute)).pipe(map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      }))
      .pipe(filter((route) => route.outlet === 'primary'))
      .pipe(mergeMap((route) => route.data))
      .subscribe((event) => this.titleService.setTitle(event['title']));
  }

}

