import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ExportAsModule } from 'ngx-export-as';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {
          MatFormFieldModule,
          MatMenuModule, 
          MatButtonModule, 
          MatInputModule, 
          MatRippleModule, 
          MatIconModule, 
          MatTooltipModule, 
          MatTabsModule, 
          MatCheckboxModule,
          MatSnackBarModule,
          MatSidenavModule,
          MatSelectModule,
          MatTableModule,
          MatPaginatorModule,
          MatSortModule,
          MatNativeDateModule,
          MatTreeModule,
          MatExpansionModule,
          MatAutocompleteModule
          } from '@angular/material';

import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxTinymceModule } from 'ngx-tinymce';
import { NgxPrintModule } from 'ngx-print';

import { AppRoutingModule } from './app-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppComponent } from './app.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MessageAlertsComponent } from './components/shared/message-alerts/message-alerts.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { DashboardComponent } from './components/auditor/dashboard/dashboard.component';
import { SideMenuComponent } from './components/auditor/side-menu/side-menu.component';
import { DashboardHeaderComponent } from './components/dashboard-header/dashboard-header.component';
import { AddClientComponent } from './components/auditor/add-client/add-client.component';
import { EditClientComponent } from './components/auditor/edit-client/edit-client.component';
import { ClientListComponent } from './components/auditor/client-list/client-list.component';

import { SharedService } from '../app/components/shared/shared.service';
import { ClientListGridComponent } from './components/auditor/client-list-grid/client-list-grid.component';
import { EngagementsListComponent } from './components/auditor/engagements-list/engagements-list.component';
import { EngagementsListGridComponent } from './components/auditor/engagements-list-grid/engagements-list-grid.component';
import { CreateEngagementComponent } from './components/auditor/create-engagement/create-engagement.component';
import { AuthGuard } from './services/auth.guard';
import { EditEngagementComponent } from './components/auditor/edit-engagement/edit-engagement.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { InviteClientComponent } from './components/auditor/invite-client/invite-client.component';
import { ProfileComponent } from './components/auditor/profile/profile.component';
import { ActiveComponent } from './components/active/active.component';

import { Interceptor } from './services/interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TrailbalanceComponent } from './components/auditor/trailbalance/trailbalance.component';
import { TrailbalanceDetailsComponent } from './components/auditor/trailbalance-details/trailbalance-details.component';
import { AccountBookComponent } from './components/auditor/account-book/account-book.component';
import { ExcelService } from './services/excel.service';
import { BalanceSheetPreviewComponent } from './components/auditor/balance-sheet-preview/balance-sheet-preview.component';
import { PreviewReportComponent } from './components/auditor/preview-report/preview-report.component';
import { IncomeSheetPreviewComponent } from './components/auditor/income-sheet-preview/income-sheet-preview.component';
import { CompilationProceduresComponent } from './components/auditor/compilation-procedures/compilation-procedures.component';
import { AssetsComponent } from './components/auditor/assets/assets.component';
import { FinancialStatementComponent } from './components/auditor/financial-statement/financial-statement.component';
import { S3uploadsComponent } from './components/s3uploads/s3uploads.component';
import { CoverPageComponent } from './components/auditor/cover-page/cover-page.component';
import { ReportsComponent } from './components/auditor/reports/reports.component';
import { TableOfContentsComponent } from './components/auditor/table-of-contents/table-of-contents.component';
import { CompilationReportComponent } from './components/auditor/compilation-report/compilation-report.component';
import { SafeHtmlPipe } from './helpers/safehtml';
import { NtrDashboardComponent } from './components/auditor/ntr-dashboard/ntr-dashboard.component';
import { ClientAcceptanceComponent } from './components/auditor/client-acceptance/client-acceptance.component';
import { EngagementLetterComponent } from './components/auditor/engagement-letter/engagement-letter.component';
import { ClientOnboardingPlanningComponent } from './components/auditor/client-onboarding-planning/client-onboarding-planning.component';
import { ProceduresComponent } from './components/auditor/procedures/procedures.component';
import { StatementCashFlowsComponent } from './components/auditor/statement-cash-flows/statement-cash-flows.component';
import { NotesToFinancialStatementComponent } from './components/auditor/notes-to-financial-statement/notes-to-financial-statement.component';
import { TaxComponent } from './components/auditor/tax/tax.component';
import { NtrReportsComponent } from './components/auditor/ntr-reports/ntr-reports.component';
import { FinancialstatementExportComponent } from './components/auditor/financialstatement-export/financialstatement-export.component';
import { CompletionSignOffComponent } from './components/auditor/completion-sign-off/completion-sign-off.component';
@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    LoginComponent,
    WelcomeComponent,
    MessageAlertsComponent,
    ForgotPasswordComponent,
    DashboardComponent,
    SideMenuComponent,
    DashboardHeaderComponent,
    AddClientComponent,
    EditClientComponent,
    ClientListComponent,
    ClientListGridComponent,
    EngagementsListComponent,
    EngagementsListGridComponent,
    CreateEngagementComponent,
    EditEngagementComponent,
    ResetPasswordComponent,
    InviteClientComponent,
    ProfileComponent,
    ActiveComponent,
    TrailbalanceComponent,
    TrailbalanceDetailsComponent,
    AccountBookComponent,
    BalanceSheetPreviewComponent,
    PreviewReportComponent,
    IncomeSheetPreviewComponent,
    CompilationProceduresComponent,
    FinancialStatementComponent,
    AssetsComponent,
    S3uploadsComponent,
    CoverPageComponent,
    ReportsComponent,
    TableOfContentsComponent,
    CompilationReportComponent,
    SafeHtmlPipe,
    NtrDashboardComponent,
    ClientAcceptanceComponent,
    EngagementLetterComponent,
    ClientOnboardingPlanningComponent,
    ProceduresComponent,
    StatementCashFlowsComponent,
    NotesToFinancialStatementComponent,
    TaxComponent,
    NtrReportsComponent,
    FinancialstatementExportComponent,
    CompletionSignOffComponent
  ],
  entryComponents: [MessageAlertsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    MatSidenavModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatTooltipModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatTreeModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    NgxPrintModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    NgSelectModule,
      ExportAsModule,
      NgxTinymceModule.forRoot({
          baseURL: '//cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.0/'
      })
  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  providers: [    
    AuthGuard,
    SharedService, MatDatepickerModule, {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    ExcelService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
