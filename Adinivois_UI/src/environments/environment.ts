// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //dev
  apiEndpoint: 'http://3.215.103.80:8080/api-adinovis/v1',
  // apiEndpoint: 'http://3.215.103.80:8082/api-adinovisb/v1', //new spring API
  //qa
  //apiEndpoint: 'https://qaapi.adinovis.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
