export const environment = {
  production: true,
  //dev
  apiEndpoint: 'http://3.215.103.80:8080/api-adinovis/v1',
  // apiEndpoint: 'http://3.215.103.80:8082/api-adinovisb/v1', //new spring API
  //qa
  //apiEndpoint: 'https://qaapi.adinovis.com'
};
