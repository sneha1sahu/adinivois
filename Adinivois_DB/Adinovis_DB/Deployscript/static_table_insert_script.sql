﻿
INSERT INTO adinovis.userrole (`userroleid`,`rolename`,`auditrole`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'General',b'0',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(2,'Admin',b'1',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(3,'Auditor',b'0',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(4,'Client',b'0',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(5,'Preparer',b'1',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(6,'Reviewer',b'1',b'1',b'0','1','2019-05-06 08:08:42','1','2019-05-06 08:08:42'),
(7,'Super Admin',b'0',b'1',b'0','1','2019-05-07 10:27:02','1','2019-05-07 10:27:02');

-------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Account Verification','useraccount',b'1',b'0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09'),
(2,'Forget Password','useraccount',b'1',b'0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09'),
(3,'Access Token','useraccount',b'1',b'0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09'),
(4,'Invite Client','client',b'1',b'0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09'),
(5,'Accepted','client',b'1',b'0','1','2019-05-10 08:03:24','1','2019-05-10 08:03:24'),
(6,'Rejected','client',b'1',b'0','1','2019-05-10 08:04:14','1','2019-05-10 08:04:14'),
(7,'Is Progress','engagement',b'1',b'0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38'),
(8,'Completed','engagement',b'1',b'0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38'),
(9,'New','engagement',b'1',b'0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38');

------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.tokentype (`tokentypeid`,`tokenname`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (1,'Account Verification',b'1',b'0','1','2019-05-06 08:28:02','1','2019-05-06 08:28:02'),
(2,'Forget Password',b'1',b'0','1','2019-05-06 08:28:02','1','2019-05-06 08:28:02'),
(3,'Access Token',b'1',b'0','1','2019-05-06 08:28:02','1','2019-05-06 08:28:02'),
(4,'Invite Client',b'1',b'0','1','2019-05-06 08:28:02','1','2019-05-06 08:28:02');

-------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.typeofentity (`typeofentityid`,`typeofentityname`,`typeofentitycode`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Corporation','C',b'1',b'0','1','2019-05-13 09:54:41','1','2019-05-13 09:54:41'),
(2,'Partnership','P',b'1',b'0','1','2019-05-13 09:54:41','1','2019-05-13 09:54:41');

------------------------------------------------------------------------------------------------------------------

INSERT INTO adinovis.jurisdiction (`jurisdictionid`,`provincesname`,`provincescode`,`statename`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Alberta',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(2,'British Columnbia',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(3,'Manitoba',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(4,'New Brunwick',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(5,'Newfoundland and Labrador',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(6,'Nova Scotia',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(9,'Quebec',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(10,'Saskatchewan',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(11,'Northwest Territories',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(12,'Nunavut',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11'),
(13,'Yukon',NULL,'Cannada',b'1',b'0','1','2019-05-13 09:48:11','1','2019-05-13 09:48:11');

-----------------------------------------------------------------------------------------------------------------------------

INSERT INTO adinovis.engagement(`engagementid`,`engagementtype`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (1,'Notice To Reader',b'1',b'0','1','2019-05-21 05:35:32','1','2019-05-21 05:35:32');

-----------------------------------------------------------------------------------------------------------------------------

INSERT INTO adinovis.advpropertie (`advpropertieid`,`advpropertiename`,`advpropertievalue`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Environment','development',b'1',b'0','1','2019-05-14 09:24:20','1','2019-05-14 09:24:20'),
(2,'reset password link','http://3.215.103.80:8081/resetpassword?token=',b'1',b'0','1','2019-05-14 09:24:20','1','2019-05-14 09:24:20'),
(3,'emailvalidation','http://3.215.103.80:8081/login?token=',b'1',b'0','1','2019-05-22 10:01:57','1','2019-05-22 10:01:57'),
(4,'clientlogin','http://3.215.103.80:8081/clientlogin?token=',b'1',b'0','1','2019-05-24 06:33:32','1','2019-05-24 06:33:32');

------------------------------------------------------------------------------------------------------------------------------

INSERT INTO adinovis.fingroup (`fingroupid`,`fingroupname`,`fingroupcode`,`desciption`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Asset',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(2,'Liabilities',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(3,'Equity (corporations only)',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(4,'Equity',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(5,'Equity (Partners’ Capital (partnerships only) )',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(6,'Retained Earnings Information',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(7,'Income Statement Information',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(8,'Revenue',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(9,'Cost of sales',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(10,'Operating expenses',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(11,'Farming revenue',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(12,'Farming expenses',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(13,'Extraordinary items and income taxes',' ',' ',b'1',b'0','1','2019-05-08 11:46:23','1','2019-05-08 11:46:23'),
(14,'Expenses',' ',' ',b'1',b'0','1','2019-05-09 09:49:25','1','2019-05-09 09:49:25'),
(15,'OCI',' ',' ',b'1',b'0','1','2019-05-09 09:57:19','1','2019-05-09 09:57:19'),
(16,'NA','NA','NA',b'1',b'0','1','2019-05-10 12:16:49','1','2019-05-10 12:16:49');

-------------------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.finsubgroup (`finsubgroupid`,`fingroupid`,`finsubgroupname`,`finsubgroupcode`,`desciption`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,1,'Current Assets','CA','Current Assets',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(2,1,'Fixed Asset','FA','Fixed Asset',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(3,1,'Fixed Depreciation','FD','Fixed Depreciation',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(4,1,'Fixed No depreciation','FN','Fixed No depreciation',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(5,1,'Inventory','IN','Inventory',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(6,1,'Other Assets','OA','Other Assets',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(7,1,'Quick Assets','QA','Quick Assets',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(8,1,'Trade Receivable','TR','Trade Receivable',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(9,4,'Dividends','DI','Dividends',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(10,4,'Equity','EQ','Equity',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(11,4,'Retained earnings','RE','Retained earnings',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(12,14,'Cost of depreciation','OD','Cost of depreciation',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(13,14,'Cost of sales','CS','Cost of sales',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(14,14,'Income tax','IT','Income tax',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(15,14,'Interest cost','NA','	Interest cost',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(16,14,'Operating expenses','OE','Operating expenses',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(17,14,'expenses Other','EO','Expenses',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(18,2,'Current Assets','CA','Current Assets',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(19,2,'Current liabilities','CL','Current liabilities',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(20,2,'Long term liabilities','LL','Long term liabilities',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(21,2,'Other Assets','OA','Other Assets',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(22,2,'Trade payables','TP','Trade payables',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(23,8,'Cost of depreciation','NA','Cost of sales',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(24,8,'Cost of sales','CS','Cost of sales',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(25,8,'Interest cost','NA','Interest cost',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(26,8,'Sales operating','SO','Sales operating',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(27,8,'NA','CR','Revenue (Income)',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(28,8,'Revenue other','RO','Revenue other',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(29,15,'Equity','EQ','Equity',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(30,15,'Retained earnings','RE','Retained earnings',b'1',b'0','1','2019-05-10 08:01:31','1','2019-05-10 08:01:31'),
(31,8,'NA','OI','OI',b'1',b'0','1','2019-05-10 10:21:20','1','2019-05-10 10:21:20'),
(32,8,'NA','OR','OR',b'1',b'0','1','2019-05-10 10:21:20','1','2019-05-10 10:21:20'),
(34,1,'NA','OI','NA',b'1',b'0','1','2019-05-10 10:57:58','1','2019-05-10 10:57:58'),
(35,8,'NA','OR','NA',b'1',b'0','1','2019-05-10 10:58:13','1','2019-05-10 10:58:13'),
(37,16,'NA','NA','NA',b'1',b'0','1','2019-05-10 12:17:36','1','2019-05-10 12:17:36');

-------------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.leadsheet (`leadsheetid`,`fingroupid`,`leadsheetname`,`leadsheetcode`,`desciption`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`,`accttype`) VALUES 
(1,1,'Cash','A','Cash',b'1',b'0','1','2019-05-09 11:39:03','1','2019-05-09 11:39:03',NULL),
(2,1,'Accounts Receivable','C','Accounts Receivable',b'1',b'0','1','2019-05-09 11:42:59','1','2019-05-09 11:42:59',NULL),
(3,1,'Inventory','D','Inventory',b'1',b'0','1','2019-05-09 11:42:59','1','2019-05-09 11:42:59',NULL),
(4,1,'Prepaid Expenses','E','Prepaid Expenses',b'1',b'0','1','2019-05-09 11:42:59','1','2019-05-09 11:42:59',NULL),
(5,1,'Investments','G','Investments',b'1',b'0','1','2019-05-09 11:43:56','1','2019-05-09 11:43:56',NULL),
(6,1,'Property,Plant and Equipment','H','Property, Plant and Equipment',b'1',b'0','1','2019-05-09 11:43:56','1','2019-05-09 11:43:56',NULL),
(7,1,'Other Assets','I','Other Assets',b'1',b'0','1','2019-05-09 11:43:56','1','2019-05-09 11:43:56',NULL),
(8,1,'Intangible Assets','X','Intangible Assets',b'1',b'0','1','2019-05-09 11:44:06','1','2019-05-09 11:44:06',NULL),
(9,14,'Expenses','40','Expenses',b'1',b'0','1','2019-05-09 11:44:06','1','2019-05-09 11:44:06',NULL),
(10,14,'Farm Revenue','50','Farm Revenue',b'1',b'0','1','2019-05-09 11:44:06','1','2019-05-09 11:44:06',NULL),
(11,14,'Farm Production Expenses','51','Farm Production Expenses',b'1',b'0','1','2019-05-09 11:44:25','1','2019-05-09 11:44:25',NULL),
(12,14,'Other Farm Expenses','52','Other Farm Expenses',b'1',b'0','1','2019-05-09 11:44:25','1','2019-05-09 11:44:25',NULL),
(13,14,'Schedule of Administrative Expenses','60','Schedule of Administrative Expenses',b'1',b'0','1','2019-05-09 11:44:25','1','2019-05-09 11:44:25',NULL),
(14,14,'Schedule of Financial Expenses','61','Schedule of Financial Expenses',b'1',b'0','1','2019-05-09 11:44:35','1','2019-05-09 11:44:35',NULL),
(15,14,'Schedule of Occupancy Costs','62','Schedule of Occupancy Costs',b'1',b'0','1','2019-05-09 11:44:35','1','2019-05-09 11:44:35',NULL),
(16,14,'Schedule of Selling Expenses','63','Schedule of Selling Expenses',b'1',b'0','1','2019-05-09 11:44:35','1','2019-05-09 11:44:35',NULL),
(17,14,'Unallocated Departmental Expenses','69','Unallocated Departmental Expenses',b'1',b'0','1','2019-05-09 11:44:35','1','2019-05-09 11:44:35',NULL),
(18,14,'Other Items','70','Other Items',b'1',b'0','1','2019-05-09 11:44:35','1','2019-05-09 11:44:35',NULL),
(19,14,'Other Items','70','Other Items',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(20,14,'Amortization','71','Amortization',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(21,4,'Share Capital, Retained Earnings & Equity','TT','Share Capital, Retained Earnings & Equity',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(22,14,'Interest on Long-Term Debt	','72','Interest on Long-Term Debt',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(23,14,'Income Taxes','80','Income Taxes',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(24,14,'Discontinued Operations','83','Discontinued Operations',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(25,14,'Goodwill Impairment Loss','82','Goodwill Impairment Loss',b'1',b'0','1','2019-05-09 11:44:48','1','2019-05-09 11:44:48',NULL),
(26,14,'Extraordinary Items','81','Extraordinary Items',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(27,14,'Schedule of revenue and expenditures #1','90','Schedule of revenue and expenditures #1',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(28,14,'Schedule of revenue and expenditures #6','95','Schedule of revenue and expenditures #6',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(29,14,'Schedule of revenue and expenditures #5','94','Schedule of revenue and expenditures #5',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(30,14,'Schedule of revenue and expenditures #4','93','Schedule of revenue and expenditures #4',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(31,14,'Schedule of revenue and expenditures #3','92','Schedule of revenue and expenditures #3',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(32,14,'Schedule of revenue and expenditures #2','91','Schedule of revenue and expenditures #2',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(33,2,'Bank Indebtedness and Notes Payable','AA','Bank Indebtedness and Notes Payable',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(34,2,'Accounts Payable','BB','Accounts Payable',b'1',b'0','1','2019-05-09 11:45:01','1','2019-05-09 11:45:01',NULL),
(35,2,'Current & Future Taxes','CC','Current & Future Taxes',b'1',b'0','1','2019-05-09 11:45:26','1','2019-05-09 11:45:26',NULL),
(36,2,'Related Party Balances & Transactions','EE','Related Party Balances & Transactions',b'1',b'0','1','2019-05-09 11:45:42','1','2019-05-09 11:45:42',NULL),
(37,2,'Deferred Income & Other Liabilities','FF','Deferred Income & Other Liabilities',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(38,2,'Debt','NN','Debt',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(39,15,'Other Comprehensive Income','OCI','Other Comprehensive Income',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(40,8,'Revenue','20','Revenue',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(41,8,'Other Income Section #1','21','Other Income Section #1',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(42,8,'Other Income Section #2','22','Other Income Section #2',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(43,8,'Cost of Sales','30','Cost of Sales',b'1',b'0','1','2019-05-09 11:46:13','1','2019-05-09 11:46:13',NULL),
(44,1,'Marketable Securites','B','Marketable Securites',b'1',b'0','1','2019-05-09 11:46:31','1','2019-05-09 11:46:31',NULL),
(45,1,'NA','73','NA',b'1',b'0','1','2019-05-10 10:19:14','1','2019-05-10 10:19:14',NULL),
(46,14,'NA','73','NA',b'1',b'0','1','2019-05-10 10:56:21','1','2019-05-10 10:56:21',NULL),
(47,16,'NA','NA','NA',b'1',b'0','1','2019-05-10 12:18:59','1','2019-05-10 12:18:59',NULL);

-------------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.parentgroup (`parentgroupid`,`groupname`,`isactive`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Parent',b'1','1','2019-05-10 10:14:41','1','2019-05-10 10:14:41'),
(2,'Parent-SubGroup',b'1','1','2019-05-10 10:14:57','1','2019-05-10 10:14:57'),
(3,'Parent-Child',b'1','1','2019-05-10 10:15:10','1','2019-05-10 10:15:10'),
(4,'Parent-Child-subGroup',b'1','1','2019-05-10 10:16:05','1','2019-05-10 10:16:05');

-------------------------------------------------------------------------------------------------------------------------


INSERT INTO adinovis.typeofbalance (`typeofbalanceid`,`typeofbalance`,`isactive`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (1,'Credit',b'1','1','2019-05-10 10:09:37','1','2019-05-10 10:09:37'),
(2,'Debit',b'1','1','2019-05-10 10:09:45','1','2019-05-10 10:09:45'),
(3,'NA',b'1','1','2019-05-10 10:09:53','1','2019-05-10 10:09:53');

--------------------------------------------------------------------------------------------

INSERT INTO adinovis.statementtype (`statementtypeid`,`statementtypename`,`statementtypecode`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'Balance sheet','NA',b'1',b'0','1','2019-05-10 10:27:33','1','2019-05-10 10:27:33'),
(2,'Income statement','NA',b'1',b'0','1','2019-05-10 10:28:12','1','2019-05-10 10:28:12'),
(3,'NA','NA',b'1',b'0','1','2019-05-10 10:28:25','1','2019-05-10 10:28:25');


---------------------------------------------------------------------------------------

INSERT INTO adinovis.balancetype (`balancetypeid`,`balancetypename`,`balancetypecode`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,'credit','cr',b'1',b'0','1','2019-05-14 08:00:23','1','2019-05-14 08:00:23'),
(2,'debit','dr',b'1',b'0','1','2019-05-14 08:00:50','1','2019-05-14 08:00:50');

----------------------------------------------------------------------------------------

INSERT INTO adinovis.auditrole (`auditroleid`,`userroleid`,`rowindex`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES 
(1,5,1,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(2,5,2,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(3,5,3,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(4,5,4,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(5,5,5,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(6,6,1,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(7,6,2,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(8,6,3,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(9,6,4,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(10,6,5,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(11,2,1,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(12,2,2,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(13,2,3,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(14,2,4,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24'),
(15,2,5,b'1',b'0','1','2019-05-22 05:33:24','1','2019-05-22 05:33:24');

-----------------------------------------------------------------------------------------

INSERT INTO `adinovis`.`gificode`
(
`gificode`,
`gifidescription`,
`fingroup`,
`isactive`,
`isdelete`,
`createdby`,
`modifiedby`
)
VALUES
 ('1000','Cash and deposits','Asset',1, 0, 1, 1 )
,('1001','Cash bank drafts, bank notes, cheques, coins, currency, money orders, postal notes, and post-dated cheques','Asset',1, 0, 1, 1 )
,('1002','Deposits in Canadian banks and institutions – Canadian currency','Asset',1, 0, 1, 1 )
,('1003','Deposits in Canadian banks and institutions – Foreign currency','Asset',1, 0, 1, 1 )
,('1004','Deposits in foreign banks – Canadian currency','Asset',1, 0, 1, 1 )
,('1005','Deposits in foreign banks – Foreign currency','Asset',1, 0, 1, 1 )
,('1006','Credit union central deposits (credit unions only)','Asset',1, 0, 1, 1 )
,('1007','Other cash-like instruments (gold bullion and silver bullion)','Asset',1, 0, 1, 1 )
,('1060','Accounts receivable (claims, dividends, royalties, and subsidies receivable)','Asset',1, 0, 1, 1 )
,('1061','Allowance for doubtful accounts','Asset',1, 0, 1, 1 )
,('1062','Trade accounts receivable','Asset',1, 0, 1, 1 )
,('1063','Allowance for doubtful trade accounts receivable','Asset',1, 0, 1, 1 )
,('1064','Trade accounts receivable from related parties','Asset',1, 0, 1, 1 )
,('1065','Allowance for doubtful trade accounts receivable from related parties','Asset',1, 0, 1, 1 )
,('1066','Taxes receivable GST/HST, income tax refunds, and tax credits receivable','Asset',1, 0, 1, 1 )
,('1067','Interest receivable','Asset',1, 0, 1, 1 )
,('1068','Holdbacks receivable','Asset',1, 0, 1, 1 )
,('1069','Leases receivable','Asset',1, 0, 1, 1 )
,('1070','Allowance for doubtful amounts contained in leases receivable','Asset',1, 0, 1, 1 )
,('1071','Accounts receivable from employees','Asset',1, 0, 1, 1 )
,('1072','Allowance for doubtful accounts receivable from employees','Asset',1, 0, 1, 1 )
,('1073','Amounts receivable from members of NPOs','Asset',1, 0, 1, 1 )
,('1120','Inventories','Asset',1, 0, 1, 1 )
,('1121','Inventory of goods for sale finished goods','Asset',1, 0, 1, 1 )
,('1122','Inventory parts and supplies','Asset',1, 0, 1, 1 )
,('1123','Inventory properties (This item is intended for companies whose primary activities are real estate, subdivision, or construction, and who have real estate held for sale.)','Asset',1, 0, 1, 1 )
,('1124','Inventory of aggregates','Asset',1, 0, 1, 1 )
,('1125','Work in progress goods in process','Asset',1, 0, 1, 1 )
,('1126','Raw materials (includes amounts reported under inventories as raw materials usually found in the manufacturing sector)','Asset',1, 0, 1, 1 )
,('1127','Inventory of securities (This item is intended for companies such as brokers,stockbrokers, financial institutions, and investment companies that hold securities for sale.)','Asset',1, 0, 1, 1 )
,('1180','Short-term investments (short-term marketable securiti)','Asset',1, 0, 1, 1 )
,('1181','Canadian term deposits(short-term bearer deposit notes, collateral deposits, and guaranteed investment certificates)','Asset',1, 0, 1, 1 )
,('1182','Canadian shares','Asset',1, 0, 1, 1 )
,('1183','Canadian bonds (bond coupons, bond deposits, corporate bonds, government bonds, and debentures shown current)','Asset',1, 0, 1, 1 )
,('1184','Canadian treasury bills','Asset',1, 0, 1, 1 )
,('1185','Securities purchased under resale agreements (includes amounts reported as securities purchased under resale agreements found in returns filed by financial institutions, investments companies and brokers)','Asset',1, 0, 1, 1 )
,('1186','Other short-term Canadian investments','Asset',1, 0, 1, 1 )
,('1187','Short-term foreign investments (all types of foreign investment shown current)','Asset',1, 0, 1, 1 )
,('1240','Loans and notes receivable','Asset',1, 0, 1, 1 )
,('1241','Demand loans receivable (amounts such as call loans, day loans, and demand loans)','Asset',1, 0, 1, 1 )
,('1242','Other loans receivable','Asset',1, 0, 1, 1 )
,('1243','Notes receivable','Asset',1, 0, 1, 1 )
,('1244','Mortgages receivable','Asset',1, 0, 1, 1 )
,('1300','Due from shareholder(s)/director(s)','Asset',1, 0, 1, 1 )
,('1301','Due from individual shareholder(s)','Asset',1, 0, 1, 1 )
,('1302','Due from corporate shareholder(s) (due from parent company)','Asset',1, 0, 1, 1 )
,('1303','Due from director(s)','Asset',1, 0, 1, 1 )
,('1310','Due from member(s)/general partner(s)','Asset',1, 0, 1, 1 )
,('1311','Due from limited partners','Asset',1, 0, 1, 1 )
,('1312','Due from members that are partnerships','Asset',1, 0, 1, 1 )
,('1313','Due from general partners','Asset',1, 0, 1, 1 )
,('1314','Due from specified members who are not limited partners','Asset',1, 0, 1, 1 )
,('1360','Investment in joint venture(s)/partnership(s) (current investment or equity in joint venture(s), partnership(s), and syndicate(s))','Asset',1, 0, 1, 1 )
,('1380','Due from joint venture(s)/partnership(s) (current amounts due from joint venture(s)/partnership(s), or syndicate(s), such as advances, loans, and notes )','Asset',1, 0, 1, 1 )
,('1400','Due from/investment in related parties','Asset',1, 0, 1, 1 )
,('1401','Demand notes from related parties (amounts due from related parties such as call loans, day loans, and demand loans)','Asset',1, 0, 1, 1 )
,('1402','Interest receivable from related parties','Asset',1, 0, 1, 1 )
,('1403','Loans/advances due from related parties','Asset',1, 0, 1, 1 )
,('1460','Customers’ liability under acceptances (This item is for financial institutions – any amount reported under this item should be equal to the amount reported in Liabilities under item 2940 – Bankers acceptance s.)','Asset',1, 0, 1, 1 )
,('1480','Other current assets','Asset',1, 0, 1, 1 )
,('1481','Future (deferred) income taxes (corporations only) (income taxes applicable to future years, and reserve for income taxes, shown current )','Asset',1, 0, 1, 1 )
,('1482','Accrued investment income','Asset',1, 0, 1, 1 )
,('1483','Taxes recoverable/refundable','Asset',1, 0, 1, 1 )
,('1484','Prepaid expenses','Asset',1, 0, 1, 1 )
,('1485','Drilling advances (for mining, quarrying, and oil and gas industries)','Asset',1, 0, 1, 1 )
,('1486','Security/tender deposits','Asset',1, 0, 1, 1 )
,('1599','Total current assets','Asset',1, 0, 1, 1 )
,('1600','Land','Asset',1, 0, 1, 1 )
,('1601','Land improvements (landscaping)','Asset',1, 0, 1, 1 )
,('1602','Accumulated amortization of land improvements','Asset',1, 0, 1, 1 )
,('1620','Depletable assets (costs for mine-stripping, well drilling, and waste removal)','Asset',1, 0, 1, 1 )
,('1621','Accumulated amortization of depletable assets','Asset',1, 0, 1, 1 )
,('1622','Petroleum and natural gas properties','Asset',1, 0, 1, 1 )
,('1623','Accumulated amortization of petroleum and natural gas properties','Asset',1, 0, 1, 1 )
,('1624','Mining properties','Asset',1, 0, 1, 1 )
,('1625','Accumulated amortization of mining properties','Asset',1, 0, 1, 1 )
,('1626','Deferred exploration and development charges','Asset',1, 0, 1, 1 )
,('1627','Accumulated amortization of deferred exploration and development charges','Asset',1, 0, 1, 1 )
,('1628','Quarries','Asset',1, 0, 1, 1 )
,('1629','Accumulated amortization of quarries','Asset',1, 0, 1, 1 )
,('1630','Gravel pits','Asset',1, 0, 1, 1 )
,('1631','Accumulated amortization of gravel pits','Asset',1, 0, 1, 1 )
,('1632','Timber limits','Asset',1, 0, 1, 1 )
,('1633','Accumulated amortization of timber limits','Asset',1, 0, 1, 1 )
,('1680','Buildings','Asset',1, 0, 1, 1 )
,('1681','Accumulated amortization of buildings','Asset',1, 0, 1, 1 )
,('1682','Manufacturing and processing plant','Asset',1, 0, 1, 1 )
,('1683','Accumulated amortization of manufacturing and processing plant','Asset',1, 0, 1, 1 )
,('1684','Buildings under construction','Asset',1, 0, 1, 1 )
,('1740','Machinery, equipment, furniture, and fixtures','Asset',1, 0, 1, 1 )
,('1741','Accumulated amortization of machinery,equipment, furniture, and fixtures','Asset',1, 0, 1, 1 )
,('1742','Motor vehicles','Asset',1, 0, 1, 1 )
,('1743','Accumulated amortization of motor vehicles','Asset',1, 0, 1, 1 )
,('1744','Tools and dies','Asset',1, 0, 1, 1 )
,('1745','Accumulated amortization of tools and dies','Asset',1, 0, 1, 1 )
,('1746','Construction and excavating equipment','Asset',1, 0, 1, 1 )
,('1747','Accumulated amortization of construction and excavating equipment','Asset',1, 0, 1, 1 )
,('1748','Forestry and logging equipment','Asset',1, 0, 1, 1 )
,('1749','Accumulated amortization of forestry and logging equipment','Asset',1, 0, 1, 1 )
,('1750','Fishing gear and nets sonar equipment','Asset',1, 0, 1, 1 )
,('1751','Accumulated amortization of fishing gear and nets','Asset',1, 0, 1, 1 )
,('1752','Mining equipment','Asset',1, 0, 1, 1 )
,('1753','Accumulated amortization of mining equipment','Asset',1, 0, 1, 1 )
,('1754','Oil and gas systems pipelines and distribution systems','Asset',1, 0, 1, 1 )
,('1755','Accumulated amortization of oil and gas systems','Asset',1, 0, 1, 1 )
,('1756','Production equipment for resource industries','Asset',1, 0, 1, 1 )
,('1757','Accumulated amortization of production equipment for resource industries','Asset',1, 0, 1, 1 )
,('1758','Production equipment for other than resource industries','Asset',1, 0, 1, 1 )
,('1759','Accumulated amortization of production equipment for other than resource industries','Asset',1, 0, 1, 1 )
,('1760','Exploration equipment','Asset',1, 0, 1, 1 )
,('1761','Accumulated amortization of exploration equipment','Asset',1, 0, 1, 1 )
,('1762','Shipping equipment','Asset',1, 0, 1, 1 )
,('1763','Accumulated amortization of shipping equipment','Asset',1, 0, 1, 1 )
,('1764','Ships and boats','Asset',1, 0, 1, 1 )
,('1765','Accumulated amortization of ships and boats','Asset',1, 0, 1, 1 )
,('1766','Aircraft','Asset',1, 0, 1, 1 )
,('1767','Accumulated amortization of aircraft','Asset',1, 0, 1, 1 )
,('1768','Signs','Asset',1, 0, 1, 1 )
,('1769','Accumulated amortization of signs','Asset',1, 0, 1, 1 )
,('1770','Small tools','Asset',1, 0, 1, 1 )
,('1771','Accumulated amortization of small tools','Asset',1, 0, 1, 1 )
,('1772','Radio and communication equipment','Asset',1, 0, 1, 1 )
,('1773','Accumulated amortization of radio and communication equipment','Asset',1, 0, 1, 1 )
,('1774','Computer equipment/software','Asset',1, 0, 1, 1 )
,('1775','Accumulated amortization of computer equipment/software','Asset',1, 0, 1, 1 )
,('1776','Musical instruments','Asset',1, 0, 1, 1 )
,('1777','Accumulated amortization of musical instruments','Asset',1, 0, 1, 1 )
,('1778','Satellites','Asset',1, 0, 1, 1 )
,('1779','Accumulated amortization of satellites','Asset',1, 0, 1, 1 )
,('1780','Earth stations','Asset',1, 0, 1, 1 )
,('1781','Accumulated amortization of earth stations','Asset',1, 0, 1, 1 )
,('1782','Machinery and equipment under construction','Asset',1, 0, 1, 1 )
,('1783','Transportation equipment','Asset',1, 0, 1, 1 )
,('1784','Accumulated amortization of transportation equipment','Asset',1, 0, 1, 1 )
,('1785','Other machinery and equipment','Asset',1, 0, 1, 1 )
,('1786','Accumulated amortization of other machinery and equipment','Asset',1, 0, 1, 1 )
,('1787','Furniture and fixtures','Asset',1, 0, 1, 1 )
,('1788','Accumulated amortization of furniture and fixtures','Asset',1, 0, 1, 1 )
,('1900','Other tangible capital assets (art, books, chinaware, cutlery, utensils, uniforms, culverts, dams, golf courses, grain elevators, grandstands, swimming pools, towers, and trailer parks)','Asset',1, 0, 1, 1 )
,('1901','Accumulated amortization of other tangible capital assets','Asset',1, 0, 1, 1 )
,('1902','Logging roads','Asset',1, 0, 1, 1 )
,('1903','Accumulated amortization of logging roads','Asset',1, 0, 1, 1 )
,('1904','Asphalt and parking areas','Asset',1, 0, 1, 1 )
,('1905','Accumulated amortization of asphalt and parking areas','Asset',1, 0, 1, 1 )
,('1906','Wharves docks, float walks, and marinas','Asset',1, 0, 1, 1 )
,('1907','Accumulated amortization of wharves','Asset',1, 0, 1, 1 )
,('1908','Fences','Asset',1, 0, 1, 1 )
,('1909','Accumulated amortization of fences','Asset',1, 0, 1, 1 )
,('1910','Capital leases – Buildings','Asset',1, 0, 1, 1 )
,('1911','Accumulated amortization of capital leases – Buildings','Asset',1, 0, 1, 1 )
,('1912','Capital leases – Equipment','Asset',1, 0, 1, 1 )
,('1913','Accumulated amortization of capital leases – Equipment','Asset',1, 0, 1, 1 )
,('1914','Capital leases – Vehicles','Asset',1, 0, 1, 1 )
,('1915','Accumulated amortization of capital leases – Vehicles','Asset',1, 0, 1, 1 )
,('1916','Capital leases – Others rented signs','Asset',1, 0, 1, 1 )
,('1917','Accumulated amortization of capital leases – Others','Asset',1, 0, 1, 1 )
,('1918','Leasehold improvements','Asset',1, 0, 1, 1 )
,('1919','Accumulated amortization of leasehold improvements','Asset',1, 0, 1, 1 )
,('1920','Other capital assets under construction','Asset',1, 0, 1, 1 )
,('1921','Campsites','Asset',1, 0, 1, 1 )
,('1922','Accumulated amortization of campsites','Asset',1, 0, 1, 1 )
,('2008','Total tangible capital assets','Asset',1, 0, 1, 1 )
,('2009','Total accumulated amortization of tangible capital assets','Asset',1, 0, 1, 1 )
,('2010','Intangible assets concessions, formulas, franchises, and organization costs','Asset',1, 0, 1, 1 )
,('2011','Accumulated amortization of intangible assets','Asset',1, 0, 1, 1 )
,('2012','Goodwill','Asset',1, 0, 1, 1 )
,('2013','Accumulated amortization of goodwill applies to tax years before 2002','Asset',1, 0, 1, 1 )
,('2014','Quota','Asset',1, 0, 1, 1 )
,('2015','Accumulated amortization of quota','Asset',1, 0, 1, 1 )
,('2016','Licences','Asset',1, 0, 1, 1 )
,('2017','Accumulated amortization of licences','Asset',1, 0, 1, 1 )
,('2018','Incorporation costs (corporations only)','Asset',1, 0, 1, 1 )
,('2019','Accumulated amortization of incorporation costs (corporations only)','Asset',1, 0, 1, 1 )
,('2020','Trademarks/patents','Asset',1, 0, 1, 1 )
,('2021','Accumulated amortization of trademarks/patents','Asset',1, 0, 1, 1 )
,('2022','Customer lists','Asset',1, 0, 1, 1 )
,('2023','Accumulated amortization of customer lists','Asset',1, 0, 1, 1 )
,('2024','Rights','Asset',1, 0, 1, 1 )
,('2025','Accumulated amortization of rights','Asset',1, 0, 1, 1 )
,('2026','Research and development','Asset',1, 0, 1, 1 )
,('2027','Accumulated amortization of research and development','Asset',1, 0, 1, 1 )
,('2070','Resource rights','Asset',1, 0, 1, 1 )
,('2071','Accumulated amortization of resource rights','Asset',1, 0, 1, 1 )
,('2072','Timber rights','Asset',1, 0, 1, 1 )
,('2073','Accumulated amortization of timber rights','Asset',1, 0, 1, 1 )
,('2074','Mining rights','Asset',1, 0, 1, 1 )
,('2075','Accumulated amortization of mining rights','Asset',1, 0, 1, 1 )
,('2076','Oil and gas rights','Asset',1, 0, 1, 1 )
,('2077','Accumulated amortization of oil and gas rights','Asset',1, 0, 1, 1 )
,('2178','Total intangible capital assets','Asset',1, 0, 1, 1 )
,('2179','Total accumulated amortization of intangible capital assets','Asset',1, 0, 1, 1 )
,('2180','Due from shareholder(s)/director(s)','Asset',1, 0, 1, 1 )
,('2181','Due from individual shareholder(s)','Asset',1, 0, 1, 1 )
,('2182','Due from corporate shareholder(s) (due from parent company)','Asset',1, 0, 1, 1 )
,('2183','Due from director(s)','Asset',1, 0, 1, 1 )
,('2190','Due from members (advances, loans, and notes to members of co-operatives or credit unions. For cooperatives and credit unions.)','Asset',1, 0, 1, 1 )
,('2200','Investment in joint venture(s)/partnership(s) (long-term investment or equity in joint venture(s),partnership(s), and syndicate(s))','Asset',1, 0, 1, 1 )
,('2210','Due from member(s)/general partner(s)','Asset',1, 0, 1, 1 )
,('2211','Due from limited partners','Asset',1, 0, 1, 1 )
,('2212','Due from members that are partnerships','Asset',1, 0, 1, 1 )
,('2213','Due from general partners','Asset',1, 0, 1, 1 )
,('2214','Due from specified members who are not limited partners','Asset',1, 0, 1, 1 )
,('2220','Due from joint venture(s)/partnership(s)(long-term amounts due from joint venture(s)/partnership(s), or syndicate(s), such as advances, loans, and notes)','Asset',1, 0, 1, 1 )
,('2240','Due from/investment in related parties','Asset',1, 0, 1, 1 )
,('2241','Due from/investment in Canadian related parties','Asset',1, 0, 1, 1 )
,('2242','Shares in Canadian related corporations','Asset',1, 0, 1, 1 )
,('2243','Loans/advances to Canadian related corporations','Asset',1, 0, 1, 1 )
,('2244','Investment in Canadian related corporations at cost','Asset',1, 0, 1, 1 )
,('2245','Investment in Canadian related corporations at equity','Asset',1, 0, 1, 1 )
,('2246','Due from/investment in foreign related parties','Asset',1, 0, 1, 1 )
,('2247','Shares in foreign related corporations','Asset',1, 0, 1, 1 )
,('2248','Loans/advances to foreign related corporations','Asset',1, 0, 1, 1 )
,('2249','Investment in foreign related corporations at cost','Asset',1, 0, 1, 1 )
,('2250','Investment in foreign related corporations at equity','Asset',1, 0, 1, 1 )
,('2280','Investment in co-tenancy (investment in co-ownerships)','Asset',1, 0, 1, 1 )
,('2300','Long-term investments','Asset',1, 0, 1, 1 )
,('2301','Foreign shares','Asset',1, 0, 1, 1 )
,('2302','Other types of foreign investments (foreign investments in joint ventures, partnerships, bonds, and debentures)','Asset',1, 0, 1, 1 )
,('2303','Canadian shares','Asset',1, 0, 1, 1 )
,('2304','Government of Canada debt (Government of Canada long-term bonds and debentures)','Asset',1, 0, 1, 1 )
,('2305','Canadian, provincial, and municipal government debt','Asset',1, 0, 1, 1 )
,('2306','Canadian corporate bonds and debentures (long-term bond coupons and bond deposits )','Asset',1, 0, 1, 1 )
,('2307','Debt securities','Asset',1, 0, 1, 1 )
,('2308','Equity securities','Asset',1, 0, 1, 1 )
,('2309','Securities purchased under resale agreements','Asset',1, 0, 1, 1 )
,('2310','Central credit union shares','Asset',1, 0, 1, 1 )
,('2311','Other Canadian long-term investments','Asset',1, 0, 1, 1 )
,('2360','Long-term loans (advances and notes shown long-term )','Asset',1, 0, 1, 1 )
,('2361','Mortgages','Asset',1, 0, 1, 1 )
,('2362','Personal and credit card loans','Asset',1, 0, 1, 1 )
,('2363','Business and government loans','Asset',1, 0, 1, 1 )
,('2364','Line of credit','Asset',1, 0, 1, 1 )
,('2420','Other long-term assets (investment tax credits, stock exchange seats, and utilities deposits)','Asset',1, 0, 1, 1 )
,('2421','Future (deferred) income taxes (corporations only) (income taxes applicable to future years, and reserve for income taxes, shown long-term)','Asset',1, 0, 1, 1 )
,('2422','Deferred pension charges','Asset',1, 0, 1, 1 )
,('2423','Deferred unrealized exchange losses','Asset',1, 0, 1, 1 )
,('2424','Other deferred items/charges(debt discount and expense, deferred development costs, deferred finance charges, deferred organization expense, lease inducements, tenant inducements, and cost on incomplete contracts)','Asset',1, 0, 1, 1 )
,('2425','Accumulated amortization of deferred charges','Asset',1, 0, 1, 1 )
,('2426','Reserve fund','Asset',1, 0, 1, 1 )
,('2427','Cash surrender value of life insurance','Asset',1, 0, 1, 1 )
,('2589','Total long-term assets','Asset',1, 0, 1, 1 )
,('2590','Assets held in trust (trust fund, trust assets, or funds held in escrow – Corporations such as collection agencies, funeral homes,insurance agencies, real estate agencies, travel agencies,and travel wholesalers must use this item. An amount reported under this item must have a balancing amount reported under item 3470 – Amounts held in trust, in the liabilities section.)','Asset',1, 0, 1, 1 )
,('2599','Total assets(This item represents the total of all current, capital, long-term assets, and assets held in trus)','Asset',1, 0, 1, 1 )
,('2600','Bank overdraft (bank indebtedness)','Liabilities',1, 0, 1, 1 )
,('2620','Amounts payable and accrued liabilities (accrued liabilities, agreements payable, claims payable, rent payable, and utilities payable)','Liabilities',1, 0, 1, 1 )
,('2621','Trade payables','Liabilities',1, 0, 1, 1 )
,('2622','Trade payables to related parties','Liabilities',1, 0, 1, 1 )
,('2623','Holdbacks payable','Liabilities',1, 0, 1, 1 )
,('2624','Wages payable','Liabilities',1, 0, 1, 1 )
,('2625','Management fees payable','Liabilities',1, 0, 1, 1 )
,('2626','Bonuses payable','Liabilities',1, 0, 1, 1 )
,('2627','Employee deductions payable (payroll deductions for employee benefits such as employment insurance, Canada Pension Plan, Quebec Parental Insurance Plan, group insurance, and pension plans)','Liabilities',1, 0, 1, 1 )
,('2628','Withholding taxes payable','Liabilities',1, 0, 1, 1 )
,('2629','Interest payable (accrued interest payable)','Liabilities',1, 0, 1, 1 )
,('2630','Amounts payable to members of NPOs (This item is for corporations that are non-profit organizations to report amounts payable to members.)','Liabilities',1, 0, 1, 1 )
,('2680','Taxes payable (capital taxes, foreign taxes, GST/HST, current income taxes, logging taxes, sales taxes, and tax credits payable)','Liabilities',1, 0, 1, 1 )
,('2700','Short-term debt (corporate loans, demand loans, loans from foreign banks, and notes payable shown short-term)','Liabilities',1, 0, 1, 1 )
,('2701','Loans from Canadian banks','Liabilities',1, 0, 1, 1 )
,('2702','Liability for securities sold short','Liabilities',1, 0, 1, 1 )
,('2703','Liability for securities sold under repurchase agreements','Liabilities',1, 0, 1, 1 )
,('2704','Gold and silver certificates','Liabilities',1, 0, 1, 1 )
,('2705','Cheques and other items in transit','Liabilities',1, 0, 1, 1 )
,('2706','Lien notes','Liabilities',1, 0, 1, 1 )
,('2707','Credit card loans','Liabilities',1, 0, 1, 1 )
,('2770','Deferred income (deferred capital or book gain, unearned income, unearned interest, unearned service charges, and unrealized foreign exchange gain shown current)','Liabilities',1, 0, 1, 1 )
,('2780','Due to shareholder(s)/director(s)','Liabilities',1, 0, 1, 1 )
,('2781','Due to individual shareholder(s)','Liabilities',1, 0, 1, 1 )
,('2782','Due to corporate shareholder(s)(due to parent company)','Liabilities',1, 0, 1, 1 )
,('2783','Due to director(s)','Liabilities',1, 0, 1, 1 )
,('2790','Due to member(s)/general partner(s)','Liabilities',1, 0, 1, 1 )
,('2791','Due to limited partners','Liabilities',1, 0, 1, 1 )
,('2792','Due to members that are partnerships','Liabilities',1, 0, 1, 1 )
,('2793','Due to general partners','Liabilities',1, 0, 1, 1 )
,('2794','Due to specified members who are not limited partners','Liabilities',1, 0, 1, 1 )
,('2840','Due to joint venture(s)/partnership(s) (current amounts due to joint venture(s)/partnership(s),and syndicate(s) such as advances, loans, and notes)','Liabilities',1, 0, 1, 1 )
,('2860','Due to related parties','Liabilities',1, 0, 1, 1 )
,('2861','Demand notes due to related parties','Liabilities',1, 0, 1, 1 )
,('2862','Interest payable to related parties','Liabilities',1, 0, 1, 1 )
,('2863','Advances due to related parties','Liabilities',1, 0, 1, 1 )
,('2920','Current portion of long-term liability','Liabilities',1, 0, 1, 1 )
,('2940','Bankers’ acceptances','Liabilities',1, 0, 1, 1 )
,('2960','Other current liabilities (progress payments shown current )','Liabilities',1, 0, 1, 1 )
,('2961','Deposits received (bids, contract deposits, rental deposits, tenders, and security deposits)','Liabilities',1, 0, 1, 1 )
,('2962','Dividends payable','Liabilities',1, 0, 1, 1 )
,('2963','Future (deferred) income taxes (corporations only)(income taxes applicable to future years and reserve for income taxes shown current)','Liabilities',1, 0, 1, 1 )
,('2964','Reserves for guarantees, warranties, or indemnities','Liabilities',1, 0, 1, 1 )
,('2965','General provisions/reserves(contingent liabilities, provision for losses on loans, and pension reserves shown current)','Liabilities',1, 0, 1, 1 )
,('2966','Crew shares (amounts reported as crew shares in the fishing industry)','Liabilities',1, 0, 1, 1 )
,('3139','Total current liabilities','Liabilities',1, 0, 1, 1 )
,('3140','Long-term debt','Liabilities',1, 0, 1, 1 )
,('3141','Mortgages','Liabilities',1, 0, 1, 1 )
,('3142','Farm Credit Corporation loan','Liabilities',1, 0, 1, 1 )
,('3143','Chartered bank loan','Liabilities',1, 0, 1, 1 )
,('3144','Credit Union/Caisse Populaire loan','Liabilities',1, 0, 1, 1 )
,('3145','Provincial or territorial government loan','Liabilities',1, 0, 1, 1 )
,('3146','Supply company loan','Liabilities',1, 0, 1, 1 )
,('3147','Private loan','Liabilities',1, 0, 1, 1 )
,('3148','Central, league, and federation loans (for finance and insurance industries)','Liabilities',1, 0, 1, 1 )
,('3149','Line of credit','Liabilities',1, 0, 1, 1 )
,('3150','Liability for securities sold short','Liabilities',1, 0, 1, 1 )
,('3151','Liability for securities sold under repurchase agreements','Liabilities',1, 0, 1, 1 )
,('3152','Lien notes','Liabilities',1, 0, 1, 1 )
,('3200','Deposit liabilities of financial institutions (This item applies to financial institutions and represents deposits made by customers. For investment, finance,insurance, real estate and management of companies industries.)','Liabilities',1, 0, 1, 1 )
,('3210','Bonds and debentures','Liabilities',1, 0, 1, 1 )
,('3220','Deferred income (deferred capital or book gain, unearned income,unearned interest, unearned service charges, and unrealized foreign exchange gain shown long-term)','Liabilities',1, 0, 1, 1 )
,('3240','Future (deferred) income taxes (corporations only)(income taxes applicable to future years and reserve for income taxes shown long-term)','Liabilities',1, 0, 1, 1 )
,('3260','Due to shareholder(s)/director(s)','Liabilities',1, 0, 1, 1 )
,('3261','Due to individual shareholder(s)','Liabilities',1, 0, 1, 1 )
,('3262','Due to corporate shareholder(s)(due to parent company)','Liabilities',1, 0, 1, 1 )
,('3263','Due to director(s)','Liabilities',1, 0, 1, 1 )
,('3270','Due to members (advances, loans, and notes from members of co-operatives or credit unions)','Liabilities',1, 0, 1, 1 )
,('3280','Due to joint venture(s)/partnership(s)(long-term amounts due to joint venture(s)/partnership(s), and syndicate(s) such as advances, loans, and notes )','Liabilities',1, 0, 1, 1 )
,('3291','Due to member(s)/general partner(s)','Liabilities',1, 0, 1, 1 )
,('3292','Due to limited partners','Liabilities',1, 0, 1, 1 )
,('3293','Due to members that are partnerships','Liabilities',1, 0, 1, 1 )
,('3294','Due to general partners','Liabilities',1, 0, 1, 1 )
,('3295','Due to specified members who are not limited partners','Liabilities',1, 0, 1, 1 )
,('3300','Due to related parties','Liabilities',1, 0, 1, 1 )
,('3301','Amounts owing to related Canadian parties','Liabilities',1, 0, 1, 1 )
,('3302','Amounts owing to related foreign parties','Liabilities',1, 0, 1, 1 )
,('3320','Other long-term liabilities (minority shareholder interest and other deferred credits shown long-term)','Liabilities',1, 0, 1, 1 )
,('3321','Long-term obligations/commitments/capital leases','Liabilities',1, 0, 1, 1 )
,('3322','Reserves for guarantees, warranties, or indemnities','Liabilities',1, 0, 1, 1 )
,('3323','Provision for site restoration (dismantlement and abandonment costs, future removal, and site restoration costs. For resource industries.)','Liabilities',1, 0, 1, 1 )
,('3324','Contributions to qualifying environmental trust environmental trust, mine reclamation, and reclamation of waste disposal sites. For resource industries.)','Liabilities',1, 0, 1, 1 )
,('3325','General provisions/reserves (contingent liabilities, provision for losses on loans, and pension reserves shown long-term)','Liabilities',1, 0, 1, 1 )
,('3326','Preference shares restated (corporations only)(This item applies to preferred shares that have been restated as a liability and reported as a long-term liability.)','Liabilities',1, 0, 1, 1 )
,('3327','Member allocations (corporations only)(allocation to members of credit unions and co-operatives)','Liabilities',1, 0, 1, 1 )
,('3328','Deferred revenue from incomplete contracts (intended for contractors using the completion method of reporting revenue to report deferred revenue from incomplete contracts)','Liabilities',1, 0, 1, 1 )
,('3450','Total long-term liabilities','Liabilities',1, 0, 1, 1 )
,('3460','Subordinated debt','Liabilities',1, 0, 1, 1 )
,('3470','Amounts held in trust (trust fund, trust liabilities, or funds held in escrow –Corporations such as collection agencies, funeral homes,insurance agencies, real estate agencies, travel agencies,and travel wholesalers would use this item. An amount reported under this item should have a balancing amount reported in item 2590 – Assets held in trust, in the assets section.)','Liabilities',1, 0, 1, 1 )
,('3499','Total liabilities (This item represents the total of all current and long-term liabilities and must be reported)','Liabilities',1, 0, 1, 1 )
,('3500','Common shares','Equity (corporations only)',1, 0, 1, 1 )
,('3520','Preferred shares','Equity (corporations only)',1, 0, 1, 1 )
,('3540','Contributed and other surplus','Equity',1, 0, 1, 1 )
,('3541','Contributed surplus (capital donations, capital grants, and paid-in surplus)','Equity',1, 0, 1, 1 )
,('3542','Appraisal surplus(excess of appraisal value over cost, revaluation account,and revaluation surplus)','Equity',1, 0, 1, 1 )
,('3543','General reserve (general reserves, inventory reserves, mortgage reserves,and security reserves)','Equity',1, 0, 1, 1 )
,('3570','Head office account(home office account and head office investment)','Equity',1, 0, 1, 1 )
,('3580','Accumulated other comprehensive income','Equity',1, 0, 1, 1 )
,('3600','Retained earnings/deficit','Equity (corporations only)',1, 0, 1, 1 )
,('3620','Total shareholder equity','Equity (corporations only)',1, 0, 1, 1 )
,('3640','Total liabilities and shareholder equity','Equity (corporations only)',1, 0, 1, 1 )
,('3545','Net income/loss','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3546','Prior period adjustments','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3547','Currency adjustments','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3548','Unusual revenue items','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3550','Total net income/loss','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3551','General partners’ capital beginning balance','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3552','General partners’ net income (loss)','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3553','General partners’ drawings','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3554','General partners’ contributions during the fiscal period','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3560','General partners’ capital ending balance','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3561','Limited partners’ capital beginning balance','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3562','Limited partners’ net income (loss)','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3563','Limited partners’ drawings','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3564','Limited partners’ contribution during the fiscal period','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3571','Limited partners’ capital ending balance','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3575','Total partners’ capital(This item is the sum of all partner capital amounts and must be reported)','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3585','Total liabilities and partners’ capital','Equity (Partners’ Capital (partnerships only) )',1, 0, 1, 1 )
,('3660','Retained earnings/deficit – Start','Retained Earnings Information',1, 0, 1, 1 )
,('3680','Net income/loss','Retained Earnings Information',1, 0, 1, 1 )
,('3700','Dividends declared','Retained Earnings Information',1, 0, 1, 1 )
,('3701','Cash dividends','Retained Earnings Information',1, 0, 1, 1 )
,('3702','Patronage dividends','Retained Earnings Information',1, 0, 1, 1 )
,('3720','Prior period adjustments','Retained Earnings Information',1, 0, 1, 1 )
,('3740','Other items affecting retained earnings','Retained Earnings Information',1, 0, 1, 1 )
,('3741','Share redemptions','Retained Earnings Information',1, 0, 1, 1 )
,('3742','Special reserves (corporations and co-ops only)','Retained Earnings Information',1, 0, 1, 1 )
,('3743','Currency adjustments','Retained Earnings Information',1, 0, 1, 1 )
,('3744','Unusual revenue items','Retained Earnings Information',1, 0, 1, 1 )
,('3745','Interfund transfer (This item is intended for corporations that are non-profit organizations to report fund amounts transferred to/from retained earnings from/to the income statement.)','Retained Earnings Information',1, 0, 1, 1 )
,('3849','Retained earnings/deficit – End','Retained Earnings Information',1, 0, 1, 1 )
,('0001','Operating name (Complete this section if the operating name is different from the corporation’s or partnership’s name) 0002 Description of the operation (Complete the description of the operation when reporting more than one income statement, and the activity is different from the major business activity. See Appendix B of this guide for more information on reporting multiple lines of business.)','Income Statement Information',1, 0, 1, 1 )
,('0003','Sequence number (For more than one income statement, use sequence numbers for each statement: number 01 for the income statement relating to the main activity, with supplementary income statements numbered consecutively from 02.)','Income Statement Information',1, 0, 1, 1 )
,('7000','Revaluation surplus (change in carrying amount surplus as a result of revaluation of property, plant and equipment, and intangible assets)','Income Statement Information',1, 0, 1, 1 )
,('7002','Defined benefit gains/losses(actuarial gains and losses on defined benefit plans recognized in the period in which they occur)','Income Statement Information',1, 0, 1, 1 )
,('7004','Foreign operation translation gains/losses (gains and losses arising from translating the financial statements of a foreign operation)','Income Statement Information',1, 0, 1, 1 )
,('7006','Equity instruments gains/losses (subsequent changes in the fair market value of an investment in an equity instrument that is not held for trading)','Income Statement Information',1, 0, 1, 1 )
,('7008','Cash flow hedge effective portion gains/losses (effective portion of gains and losses on hedging instruments in a cash flow hedge)','Income Statement Information',1, 0, 1, 1 )
,('7010','Income tax relating to components of other(comprehensive income (corporations only) tax relating to each component of other comprehensive income if reported before tax)','Income Statement Information',1, 0, 1, 1 )
,('7020','Miscellaneous other comprehensive income (gains and losses relating to other comprehensive income not included in lines 7000 to 7008)','Income Statement Information',1, 0, 1, 1 )
,('8000','Trade sales of goods and services.','Revenue',1, 0, 1, 1 )
,('8020','Sales of goods and services to related parties','Revenue',1, 0, 1, 1 )
,('8030','Interdivisional sales','Revenue',1, 0, 1, 1 )
,('8040','Sales from resource properties','Revenue',1, 0, 1, 1 )
,('8041','Petroleum and natural gas sales','Revenue',1, 0, 1, 1 )
,('8042','Petroleum and natural gas sales to related parties','Revenue',1, 0, 1, 1 )
,('8043','Gas marketing','Revenue',1, 0, 1, 1 )
,('8044','Processing revenue','Revenue',1, 0, 1, 1 )
,('8045','Pipeline revenue','Revenue',1, 0, 1, 1 )
,('8046','Seismic sales','Revenue',1, 0, 1, 1 )
,('8047','Mining revenue','Revenue',1, 0, 1, 1 )
,('8048','Coal revenue','Revenue',1, 0, 1, 1 )
,('8049','Oil sands revenue','Revenue',1, 0, 1, 1 )
,('8050','Royalty income','Revenue',1, 0, 1, 1 )
,('8051','Oil and gas partnership/joint venture income/loss','Revenue',1, 0, 1, 1 )
,('8052','Mining partnership/joint venture income/loss','Revenue',1, 0, 1, 1 )
,('8053','Other production revenue well operating fees and sulphur revenue','Revenue',1, 0, 1, 1 )
,('8089','Total sales of goods and services','Revenue',1, 0, 1, 1 )
,('8090','Investment revenue','Revenue',1, 0, 1, 1 )
,('8091','Interest from foreign sources (This item may be reported as gross of withholding taxes; withholding taxes would then be reported under item 9283 – Withholding taxes.)','Revenue',1, 0, 1, 1 )
,('8092','Interest from Canadian bonds and debentures','Revenue',1, 0, 1, 1 )
,('8093','Interest from Canadian mortgage loans','Revenue',1, 0, 1, 1 )
,('8094','Interest from other Canadian sources (finance income, guaranteed investment certificates interest, interest on overpaid taxes, and loan interest)','Revenue',1, 0, 1, 1 )
,('8095','Dividend income','Revenue',1, 0, 1, 1 )
,('8096','Dividends from Canadian sources','Revenue',1, 0, 1, 1 )
,('8097','Dividends from foreign sources (may be reported gross of withholding taxes; withholding taxes would then be reported under item 9283 – Withholding taxes)','Revenue',1, 0, 1, 1 )
,('8100','Interest income (financial institutions)','Revenue',1, 0, 1, 1 )
,('8101','Loan interest','Revenue',1, 0, 1, 1 )
,('8102','Securities interest','Revenue',1, 0, 1, 1 )
,('8103','Deposits with banks’ interest','Revenue',1, 0, 1, 1 )
,('8120','Commission revenue','Revenue',1, 0, 1, 1 )
,('8121','Commission income on real estate transactions','Revenue',1, 0, 1, 1 )
,('8140','Rental revenue(revenue from the rental of boats, hotel or motel rooms,machinery or equipment, and storage lockers)','Revenue',1, 0, 1, 1 )
,('8141','Real estate rental revenue(amounts received as income from renting or leasing apartments, commercial buildings, land, office space, residential housing, and shopping centres. This item may also be used to report income from investments in co-tenancies and co-ownerships.)','Revenue',1, 0, 1, 1 )
,('8142','Film rental revenue','Revenue',1, 0, 1, 1 )
,('8150','Vehicle leasing – for the automotive equipment rental and leasing sector – long-term vehicle leasing and short-term vehicle leasing','Revenue',1, 0, 1, 1 )
,('8160','Fishing revenue','Revenue',1, 0, 1, 1 )
,('8161','Fish products','Revenue',1, 0, 1, 1 )
,('8162','Other marine products(amounts received from the sale of flippers, herring roe, herring scales, Irish moss, kelp, seal meat, and seaweed )','Revenue',1, 0, 1, 1 )
,('8163','Fishing grants, credits, and rebates','Revenue',1, 0, 1, 1 )
,('8164','Fishing subsidies','Revenue',1, 0, 1, 1 )
,('8165','Compensation for loss of fishing income or property amounts received from The Atlantic Groundfish Strategy (TAGS) or insurance proceeds','Revenue',1, 0, 1, 1 )
,('8166','Sharesperson income','Revenue',1, 0, 1, 1 )
,('8210','Realized gains/losses on disposal of assets(gain/loss or profit/loss on disposal/sale of capital assets 8211 Realized gains/losses on sale of investments profit/loss on disposal) of investments or marketable securities','Revenue',1, 0, 1, 1 )
,('8212','Realized gains/losses on sale of resource properties','Revenue',1, 0, 1, 1 )
,('8220','NPO amounts received','Revenue',1, 0, 1, 1 )
,('8221','Membership fees','Revenue',1, 0, 1, 1 )
,('8222','Assessments','Revenue',1, 0, 1, 1 )
,('8223','Gifts','Revenue',1, 0, 1, 1 )
,('8224','Gross sales and revenues from organizational activities','Revenue',1, 0, 1, 1 )
,('8230','Other revenue(gains on settlement of a debt and miscellaneous revenue)','Revenue',1, 0, 1, 1 )
,('8231','Foreign exchange gains/losses(amortization of deferred exchange gains and losses and realized gains and losses on foreign currency)','Revenue',1, 0, 1, 1 )
,('8232','Income/loss of subsidiaries/affiliates (for corporations or partnerships that report investment in subsidiary corporations on the equity basis)','Revenue',1, 0, 1, 1 )
,('8233','Income/loss of other divisions','Revenue',1, 0, 1, 1 )
,('8234','Income/loss of joint ventures(for corporations or partnerships that report investments in joint ventures on the equity basis)','Revenue',1, 0, 1, 1 )
,('8235','Income/loss of partnerships(for corporations or partnerships that report investments in partnerships on the equity basis)','Revenue',1, 0, 1, 1 )
,('8236','Realization of deferred revenues(realization of interest income, realization of instalment payments, and realization of service charges)','Revenue',1, 0, 1, 1 )
,('8237','Royalty income other than resource (royalty income or royalty fees from computer programs, copyrights, motion pictures, and patents)','Revenue',1, 0, 1, 1 )
,('8238','Alberta royalty tax credits','Revenue',1, 0, 1, 1 )
,('8239','Management and administration fees','Revenue',1, 0, 1, 1 )
,('8240','Telecommunications revenue','Revenue',1, 0, 1, 1 )
,('8241','Consulting fees','Revenue',1, 0, 1, 1 )
,('8242','Subsidies and grants','Revenue',1, 0, 1, 1 )
,('8243','Sale of by-products','Revenue',1, 0, 1, 1 )
,('8244','Deposit services','Revenue',1, 0, 1, 1 )
,('8245','Credit services','Revenue',1, 0, 1, 1 )
,('8246','Card services','Revenue',1, 0, 1, 1 )
,('8247','Patronage dividends','Revenue',1, 0, 1, 1 )
,('8248','Insurance recoveries(life insurance proceeds on the death of insured executives)','Revenue',1, 0, 1, 1 )
,('8249','Expense recoveries','Revenue',1, 0, 1, 1 )
,('8250','Bad debt recoveries','Revenue',1, 0, 1, 1 )
,('8299','Total revenue','Revenue',1, 0, 1, 1 )
,('8300','Opening inventory','Cost of sales',1, 0, 1, 1 )
,('8301','Opening inventory – Finished goods','Cost of sales',1, 0, 1, 1 )
,('8302','Opening inventory – Raw materials','Cost of sales',1, 0, 1, 1 )
,('8303','Opening inventory – Goods in process (opening inventory – work in progress)','Cost of sales',1, 0, 1, 1 )
,('8320','Purchases/cost of materials(cost of merchandise sold, fuel and purchased power,manufacturing supplies used, materials and merchandise purchased. The amounts related to this item may be reported net of discounts earned on purchases.)','Cost of sales',1, 0, 1, 1 )
,('8340','Direct wages (commissions, labour, production wages, and supervision when shown in cost of sales)','Cost of sales',1, 0, 1, 1 )
,('8350','Benefits on direct wages','Cost of sales',1, 0, 1, 1 )
,('8360','Trades and sub-contracts(contract labour, custom work, sub-contract labour, and outside labour)','Cost of sales',1, 0, 1, 1 )
,('8370','Production costs other than resource','Cost of sales',1, 0, 1, 1 )
,('8400','Resource production costs (gas processing, oil and gas operating expenses, oil and gas production, milling, smelting, and refining)','Cost of sales',1, 0, 1, 1 )
,('8401','Pipeline operations','Cost of sales',1, 0, 1, 1 )
,('8402','Drilling','Cost of sales',1, 0, 1, 1 )
,('8403','Site restoration costs future removal costs','Cost of sales',1, 0, 1, 1 )
,('8404','Gross overriding royalty','Cost of sales',1, 0, 1, 1 )
,('8405','Freehold royalties','Cost of sales',1, 0, 1, 1 )
,('8406','Other producing properties rental(freehold lease rentals and freehold delay rentals)','Cost of sales',1, 0, 1, 1 )
,('8407','Prospect/geological (digital processing, geochemical work, geophysical work,gravity meters, magnetic playbacks,seismographs,staking, and velocity surveys)','Cost of sales',1, 0, 1, 1 )
,('8408','Well operating, fuel and equipment','Cost of sales',1, 0, 1, 1 )
,('8409','Well abandonment and dry holes','Cost of sales',1, 0, 1, 1 )
,('8410','Other lease rentals','Cost of sales',1, 0, 1, 1 )
,('8411','Exploration expenses(aerial surveys)','Cost of sales',1, 0, 1, 1 )
,('8412','Development expenses(stripping costs)','Cost of sales',1, 0, 1, 1 )
,('8435','Crown charges','Cost of sales',1, 0, 1, 1 )
,('8436','Crown royalties','Cost of sales',1, 0, 1, 1 )
,('8437','Crown lease rentals','Cost of sales',1, 0, 1, 1 )
,('8438','Freehold mineral tax','Cost of sales',1, 0, 1, 1 )
,('8439','Mining taxes','Cost of sales',1, 0, 1, 1 )
,('8440','Oil sand leases','Cost of sales',1, 0, 1, 1 )
,('8441','Saskatchewan resource surcharge','Cost of sales',1, 0, 1, 1 )
,('8450','Other direct costs','Cost of sales',1, 0, 1, 1 )
,('8451','Equipment hire and operation','Cost of sales',1, 0, 1, 1 )
,('8452','Log yard(barker, bucking, clipper, log sorting, and sawing)','Cost of sales',1, 0, 1, 1 )
,('8453','Forestry costs(cutting, firefighting supplies, scaling, and silviculture)','Cost of sales',1, 0, 1, 1 )
,('8454','Logging road costs(road clearing, ploughing, and grating)','Cost of sales',1, 0, 1, 1 )
,('8455','Stumpage costs','Cost of sales',1, 0, 1, 1 )
,('8456','Royalty costs(royalties paid to holders of copyrights, movies, patents,performing rights, and trademarks, found in cost of sales)','Cost of sales',1, 0, 1, 1 )
,('8457','Freight-in and duty(customs and excise duty)','Cost of sales',1, 0, 1, 1 )
,('8458','Inventory write-down(revaluation of inventory and inventory adjustments)','Cost of sales',1, 0, 1, 1 )
,('8459','Direct cost amortization of tangible assets(amortization of leasehold improvements and amounts referred to as depreciation shown in cost of sales)','Cost of sales',1, 0, 1, 1 )
,('8460','Direct cost amortization of natural resource assets (amounts referred to as depletion shown in cost of sales)','Cost of sales',1, 0, 1, 1 )
,('8461','Overhead expenses allocated to cost of sales(amounts reported under cost of sales that are normally considered operating expenses)','Cost of sales',1, 0, 1, 1 )
,('8500','Closing inventory','Cost of sales',1, 0, 1, 1 )
,('8501','Closing inventory – Finished goods','Cost of sales',1, 0, 1, 1 )
,('8502','Closing inventory – Raw materials','Cost of sales',1, 0, 1, 1 )
,('8503','Closing inventory – Goods in process(closing inventory – work in progress)','Cost of sales',1, 0, 1, 1 )
,('8518','Cost of sales(This item represents the sum of all cost of sales amounts.)','Cost of sales',1, 0, 1, 1 )
,('8519','Gross profit/loss (This item represents the net amount of item 8089 –Total sales of goods and services, less item 8518 – Cost of sales. It may also be referred to as gross margin.)','Cost of sales',1, 0, 1, 1 )
,('8520','Advertising and promotion','Operating expenses',1, 0, 1, 1 )
,('8521','Advertising (catalogues, media expenses, and publications)','Operating expenses',1, 0, 1, 1 )
,('8522','Donations(charitable donations, donations to the Crown, and political donations)','Operating expenses',1, 0, 1, 1 )
,('8523','Meals and entertainment tickets (such as theatre, concert and athletic events.)','Operating expenses',1, 0, 1, 1 )
,('8524','Promotion (booths, demonstrations/presentations, displays,prospectus, samples, and seminars (given))','Operating expenses',1, 0, 1, 1 )
,('8570','Amortization of intangible assets(amortization of intangible assets such as deferred charges, goodwill (before 2002),patents, franchises,copyrights, trademarks, organization costs, and research and development costs','Operating expenses',1, 0, 1, 1 )
,('8571','Goodwill impairment loss(applies to 2002 and later tax years)','Operating expenses',1, 0, 1, 1 )
,('8590','Bad debt expense(allowance for bad debts, allowance/provision for doubtful accounts, bad debt, bad debt written-off,provision for bad debts, and reserve for bad debt)','Operating expenses',1, 0, 1, 1 )
,('8610','Loan losses(for loans, mortgages, and other loan-type amounts written off)','Operating expenses',1, 0, 1, 1 )
,('8611','Provision for loan losses(provision/allowance for loan, mortgage, or credit losses)','Operating expenses',1, 0, 1, 1 )
,('8620','Employee benefits(association dues, clothing allowance, lodging, payroll deductions/levies/taxes, and room and board)','Operating expenses',1, 0, 1, 1 )
,('8621','Group insurance benefits(medical, dental, and life insurance plans)','Operating expenses',1, 0, 1, 1 )
,('8622','Employer’s portion of employee benefits(Canada Pension Plan, company pension plan,employment insurance, Quebec Parental Insurance Plan, and Workers’ Compensation)','Operating expenses',1, 0, 1, 1 )
,('8623','Contributions to deferred income plans(contributions to a registered pension plan, a deferred profit sharing plan, an employee profit sharing plan,and a registered supplementary unemployment benefit plan)','Operating expenses',1, 0, 1, 1 )
,('8650','Amortization of natural resource assets(amounts referred to as depletion)','Operating expenses',1, 0, 1, 1 )
,('8670','Amortization of tangible assets(amortization of leasehold improvements and amounts referred to as depreciation)','Operating expenses',1, 0, 1, 1 )
,('8690','Insurance(bonding, fire insurance, liability insurance, premium expenses, property insurance, and vehicle insurance)','Operating expenses',1, 0, 1, 1 )
,('8691','Life insurance on executives(insurance policies where the beneficiary is the corporation rather than the estate of the executive)','Operating expenses',1, 0, 1, 1 )
,('8710','Interest and bank charges(finance charges, bank charges, and interest payments on capital leases)','Operating expenses',1, 0, 1, 1 )
,('8711','Interest on short-term debt','Operating expenses',1, 0, 1, 1 )
,('8712','Interest on bonds and debentures(amortization of bond discounts)','Operating expenses',1, 0, 1, 1 )
,('8713','Interest on mortgages(amortization of mortgage discount or expense)','Operating expenses',1, 0, 1, 1 )
,('8714','Interest on long-term debt','Operating expenses',1, 0, 1, 1 )
,('8715','Bank charges','Operating expenses',1, 0, 1, 1 )
,('8716','Credit card charges(interest on credit cards)','Operating expenses',1, 0, 1, 1 )
,('8717','Collection and credit costs','Operating expenses',1, 0, 1, 1 )
,('8740','Interest paid (financial institutions)','Operating expenses',1, 0, 1, 1 )
,('8741','Interest paid on deposits','Operating expenses',1, 0, 1, 1 )
,('8742','Interest paid on bonds and debentures','Operating expenses',1, 0, 1, 1 )
,('8760','Business taxes, licences, and memberships(municipal taxes and land transfer taxes, beverage licences, business charges, motor vehicle licences, motor vehicle registration permits, and trade licences)','Operating expenses',1, 0, 1, 1 )
,('8761','Memberships(dues and subscriptions)','Operating expenses',1, 0, 1, 1 )
,('8762','Business taxes(business tax, provincial capital tax (excluding Nova Scotia tax on large corporations), bridge tolls,gross receipt tax, health and education tax, hospital tax,permits, road tolls, and taxes on leases)','Operating expenses',1, 0, 1, 1 )
,('8763','Franchise fees','Operating expenses',1, 0, 1, 1 )
,('8764','Government fees','Operating expenses',1, 0, 1, 1 )
,('8790','Nova Scotia tax on large corporations','Operating expenses',1, 0, 1, 1 )
,('8810','Office expenses','Operating expenses',1, 0, 1, 1 )
,('8811','Office stationery and supplies','Operating expenses',1, 0, 1, 1 )
,('8812','Office utilities(utility expenses related to an office such as electricity, gas, heating, hydro, and telephone)','Operating expenses',1, 0, 1, 1 )
,('8813','Data processing(word processing)','Operating expenses',1, 0, 1, 1 )
,('8860','Professional fees(engineering fees, professional services, and surveyor fees)','Operating expenses',1, 0, 1, 1 )
,('8861','Legal fees(lawyer and notary fees)','Operating expenses',1, 0, 1, 1 )
,('8862','Accounting fees(bookkeeping)','Operating expenses',1, 0, 1, 1 )
,('8863','Consulting fees','Operating expenses',1, 0, 1, 1 )
,('8864','Architect fees(architectural design and illustration fees, and landscape architect fees)','Operating expenses',1, 0, 1, 1 )
,('8865','Appraisal fees(real estate and jewellery appraisal, and financial valuation services)','Operating expenses',1, 0, 1, 1 )
,('8866','Laboratory fees','Operating expenses',1, 0, 1, 1 )
,('8867','Medical fees','Operating expenses',1, 0, 1, 1 )
,('8868','Veterinary fees (breeding fees)','Operating expenses',1, 0, 1, 1 )
,('8869','Brokerage fees','Operating expenses',1, 0, 1, 1 )
,('8870','Transfer fees(land and property transfer fees)','Operating expenses',1, 0, 1, 1 )
,('8871','Management and administration fees','Operating expenses',1, 0, 1, 1 )
,('8872','Refining and assay','Operating expenses',1, 0, 1, 1 )
,('8873','Registrar and transfer agent fees','Operating expenses',1, 0, 1, 1 )
,('8874','Restructuring costs(reorganization costs)','Operating expenses',1, 0, 1, 1 )
,('8875','Security commission fees','Operating expenses',1, 0, 1, 1 )
,('8876','Training expense(animal training, management training, and staff development)','Operating expenses',1, 0, 1, 1 )
,('8877','Studio and recording','Operating expenses',1, 0, 1, 1 )
,('8910','Rental(rental expenses for arena, boat/vessel/ship, coal and lumberyards, railway sidings, safety deposit box/vaults,and parking charges)','Operating expenses',1, 0, 1, 1 )
,('8911','Real estate rental(apartment, building, land, and office rentals)','Operating expenses',1, 0, 1, 1 )
,('8912','Occupancy costs','Operating expenses',1, 0, 1, 1 )
,('8913','Condominium fees','Operating expenses',1, 0, 1, 1 )
,('8914','Equipment rental(rental expenses for computer equipment, film, office machines, and road and construction equipment)','Operating expenses',1, 0, 1, 1 )
,('8915','Motor vehicle rentals','Operating expenses',1, 0, 1, 1 )
,('8916','Moorage (boat)(dock and wharf space)','Operating expenses',1, 0, 1, 1 )
,('8917','Storage(rental expense for garages and warehouses)','Operating expenses',1, 0, 1, 1 )
,('8918','Quota rental(forestry and logging quota rental expenses)','Operating expenses',1, 0, 1, 1 )
,('8960','Repairs and maintenance(aircraft repairs and maintenance)','Operating expenses',1, 0, 1, 1 )
,('8961','Repairs and maintenance – Buildings(premises upkeep)','Operating expenses',1, 0, 1, 1 )
,('8962','Repairs and maintenance – Vehicles','Operating expenses',1, 0, 1, 1 )
,('8963','Repairs and maintenance – Boats','Operating expenses',1, 0, 1, 1 )
,('8964','Repairs and maintenance – Machinery and equipment gas and power line repairs and maintenance','Operating expenses',1, 0, 1, 1 )
,('9010','Other repairs and maintenance(janitorial services, landscaping, and yard maintenance)','Operating expenses',1, 0, 1, 1 )
,('9011','Machine shop expense','Operating expenses',1, 0, 1, 1 )
,('9012','Road costs (snow removal costs)','Operating expenses',1, 0, 1, 1 )
,('9013','Security(alarm system and surveillance equipment repairs and maintenance)','Operating expenses',1, 0, 1, 1 )
,('9014','Garbage removal','Operating expenses',1, 0, 1, 1 )
,('9060','Salaries and wages (amounts not found in cost of sales such as administrative salaries, casual labour, cost of living allowance, down time, fees to employees, minimum wage levies, payroll remuneration, severance pay,supervision, and vacation pay)','Operating expenses',1, 0, 1, 1 )
,('9061','Commissions','Operating expenses',1, 0, 1, 1 )
,('9062','Crew share(for the fishing industry)','Operating expenses',1, 0, 1, 1 )
,('9063','Bonuses(incentive compensation)','Operating expenses',1, 0, 1, 1 )
,('9064','Directors fees','Operating expenses',1, 0, 1, 1 )
,('9065','Management salaries(officers’ salaries)','Operating expenses',1, 0, 1, 1 )
,('9066','Employee salaries(office salaries)','Operating expenses',1, 0, 1, 1 )
,('9110','Sub-contracts(contract labour, contract work, custom work, and hired labour)','Operating expenses',1, 0, 1, 1 )
,('9130','Supplies(medical supplies, veterinary drugs and supplies, wrapping and packing supplies)','Operating expenses',1, 0, 1, 1 )
,('9131','Small tools','Operating expenses',1, 0, 1, 1 )
,('9132','Shop expense','Operating expenses',1, 0, 1, 1 )
,('9133','Uniforms','Operating expenses',1, 0, 1, 1 )
,('9134','Laundry(dry-cleaning)','Operating expenses',1, 0, 1, 1 )
,('9135','Food and catering','Operating expenses',1, 0, 1, 1 )
,('9136','Fishing gear','Operating expenses',1, 0, 1, 1 )
,('9137','Nets and traps','Operating expenses',1, 0, 1, 1 )
,('9138','Salt, bait, and ice','Operating expenses',1, 0, 1, 1 )
,('9139','Camp supplies','Operating expenses',1, 0, 1, 1 )
,('9150','Computer-related expenses','Operating expenses',1, 0, 1, 1 )
,('9151','Upgrade(updates to computer software)','Operating expenses',1, 0, 1, 1 )
,('9152','Internet','Operating expenses',1, 0, 1, 1 )
,('9180','Property taxes(municipal and realty taxes)','Operating expenses',1, 0, 1, 1 )
,('9200','Travel expenses(airfare, hotel rooms, travel allowance, travel, and accommodations)','Operating expenses',1, 0, 1, 1 )
,('9201','Meetings and conventions(seminars attended)','Operating expenses',1, 0, 1, 1 )
,('9220','Utilities','Operating expenses',1, 0, 1, 1 )
,('9221','Electricity(hydro)','Operating expenses',1, 0, 1, 1 )
,('9222','Water','Operating expenses',1, 0, 1, 1 )
,('9223','Heat','Operating expenses',1, 0, 1, 1 )
,('9224','Fuel costs(coal, diesel, fuel, natural gas, oil, and propane for heating and cooking)','Operating expenses',1, 0, 1, 1 )
,('9225','Telephone and telecommunications(cellular telephone, fax machine, and pager)','Operating expenses',1, 0, 1, 1 )
,('9270','Other expenses','Operating expenses',1, 0, 1, 1 )
,('9271','Cash over/short','Operating expenses',1, 0, 1, 1 )
,('9272','Reimbursement of parent company expense(corporations only)(portion of expenses owing to parent)','Operating expenses',1, 0, 1, 1 )
,('9273','Selling expenses','Operating expenses',1, 0, 1, 1 )
,('9274','Shipping and warehouse expense','Operating expenses',1, 0, 1, 1 )
,('9275','Delivery, freight and express(courier, customs, delivery and installation, distribution,ferry charges, freight and cartage, freight and duty,shipping and transportation)','Operating expenses',1, 0, 1, 1 )
,('9276','Warranty expenses(guarantee costs)','Operating expenses',1, 0, 1, 1 )
,('9277','Royalty expenses – Resident(amounts reported as non-resource royalties paid to Canadian residents such as copyrights, movies, patents,performing rights, and trademarks)','Operating expenses',1, 0, 1, 1 )
,('9278','Royalty expenses – Non-resident (amounts reported as non-resource royalties paid to non-residents such as copyrights, movies, patents,performing rights, and trademarks)','Operating expenses',1, 0, 1, 1 )
,('9279','Dumping charges','Operating expenses',1, 0, 1, 1 )
,('9280','Land fill fees','Operating expenses',1, 0, 1, 1 )
,('9281','Vehicle expenses(automobile expenses, gas, motor vehicle fuel, tires, and vehicle washing)','Operating expenses',1, 0, 1, 1 )
,('9282','Research and development','Operating expenses',1, 0, 1, 1 )
,('9283','Withholding taxes','Operating expenses',1, 0, 1, 1 )
,('9284','General and administrative expenses(marketing and administration, office and general expenses, selling and administrative expenses)','Operating expenses',1, 0, 1, 1 )
,('9285','Interdivisional expenses','Operating expenses',1, 0, 1, 1 )
,('9286','Interfund transfer(This item is for corporations or partnerships including non-profit organizations to report fund amounts transferred to or from the income statement, from or to retained earnings.)','Operating expenses',1, 0, 1, 1 )
,('9367','Total operating expenses (This item represents the sum of all operating expense )amounts.','Operating expenses',1, 0, 1, 1 )
,('9368','Total expenses(This item must be reported if there are no farming expenses (see “Validity check items” on page 7). Any amount reported under this item should be equal to the amount reported under item 8518 – Cost of sales, plus the amount reported under item 9367 – Total operating expenses.)','Operating expenses',1, 0, 1, 1 )
,('9369','Net non-farming income(The amount reported should be equal to the amount reported under item 8299 – Total revenue, minus the amount reported under item 9368 – Total expenses.)','Operating expenses',1, 0, 1, 1 )
,('9370','Grains and oilseeds(mustard seed, rye, and sunflower seeds )','Farming revenue',1, 0, 1, 1 )
,('9371','Wheat durum','Farming revenue',1, 0, 1, 1 )
,('9372','Oats','Farming revenue',1, 0, 1, 1 )
,('9373','Barley','Farming revenue',1, 0, 1, 1 )
,('9374','Mixed grains','Farming revenue',1, 0, 1, 1 )
,('9375','Corn','Farming revenue',1, 0, 1, 1 )
,('9376','Canola','Farming revenue',1, 0, 1, 1 )
,('9377','Flaxseed','Farming revenue',1, 0, 1, 1 )
,('9378','Soya beans','Farming revenue',1, 0, 1, 1 )
,('9379','Wheat Board payments','Farming revenue',1, 0, 1, 1 )
,('9420','Other crop revenues(herbs, hops, and sugar beets)','Farming revenue',1, 0, 1, 1 )
,('9421','Fruit','Farming revenue',1, 0, 1, 1 )
,('9422','Potatoes','Farming revenue',1, 0, 1, 1 )
,('9423','Vegetables','Farming revenue',1, 0, 1, 1 )
,('9424','Tobacco','Farming revenue',1, 0, 1, 1 )
,('9425','Greenhouse and nursery products(greenhouse or nursery products such as flowers,greenhouse vegetables, horticultural products,ornamental plants, rooted cuttings, seeds and bulbs,shrubs, sod and turf, and trees)','Farming revenue',1, 0, 1, 1 )
,('9426','Forage crops(alfalfa, alsike, clover, clover seeds, fescue, grass seed,hay, and timothy)','Farming revenue',1, 0, 1, 1 )
,('9470','Livestock and animal products revenue(revenue received from animal pelts, apiary operation,bison, chinchilla, deer, dog, elk, fox, goats, honey products, mink, market livestock income, rabbit, and wool)','Farming revenue',1, 0, 1, 1 )
,('9471','Cattle(revenue received from the sale of bulls, calves, and cows)','Farming revenue',1, 0, 1, 1 )
,('9472','Swine(revenue received from the sale of hogs and pigs)','Farming revenue',1, 0, 1, 1 )
,('9473','Poultry(revenue received from the sale of chicken, ducks, geese,and turkeys)','Farming revenue',1, 0, 1, 1 )
,('9474','Sheep and lambs','Farming revenue',1, 0, 1, 1 )
,('9475','Pregnant mare urine (PMU)','Farming revenue',1, 0, 1, 1 )
,('9476','Milk and cream (excluding dairy subsidies)','Farming revenue',1, 0, 1, 1 )
,('9477','Eggs for consumption','Farming revenue',1, 0, 1, 1 )
,('9478','Hatching eggs','Farming revenue',1, 0, 1, 1 )
,('9479','Aquaculture (hatching and raising)','Farming revenue',1, 0, 1, 1 )
,('9480','Horses (revenue received from the sale of ponies, and other equine animals)','Farming revenue',1, 0, 1, 1 )
,('9520','Other commodities(revenue received from ginseng, mushrooms, ostriches,and stud services)','Farming revenue',1, 0, 1, 1 )
,('9521','Maple products','Farming revenue',1, 0, 1, 1 )
,('9522','Artificial insemination','Farming revenue',1, 0, 1, 1 )
,('9523','Semen production','Farming revenue',1, 0, 1, 1 )
,('9524','Embryo production(revenue received from embryo transplants)','Farming revenue',1, 0, 1, 1 )
,('9540','Program payment revenues(Animal Contagious Diseases Act payments, farm subsidy, farm-support payments, grants, and stabilization subsidy)','Farming revenue',1, 0, 1, 1 )
,('9541','Dairy subsidies','Farming revenue',1, 0, 1, 1 )
,('9542','Crop insurance(insurance proceeds from federal or provincial programs for loss of crops)','Farming revenue',1, 0, 1, 1 )
,('9544','Disaster Assistance Program payments','Farming revenue',1, 0, 1, 1 )
,('9545','AgriStability and AgriInvest benefit','Farming revenue',1, 0, 1, 1 )
,('9546','Production insurance premium benefit','Farming revenue',1, 0, 1, 1 )
,('9570','Rebates','Farming revenue',1, 0, 1, 1 )
,('9571','Rebates – Fuel','Farming revenue',1, 0, 1, 1 )
,('9572','Rebates – Interest','Farming revenue',1, 0, 1, 1 )
,('9573','Rebates – Property taxes','Farming revenue',1, 0, 1, 1 )
,('9600','Other farm revenues/losses(sale of land, soil or stone, payment in kind, personal consumption benefit, and quality bonus)','Farming revenue',1, 0, 1, 1 )
,('9601','Custom or contract work(seed cleaning/drying/packing/treating, crop dusting or spraying, custom combining/harvesting, custom seeding, custom spraying, and custom trucking/hauling)','Farming revenue',1, 0, 1, 1 )
,('9602','Wood sales(amounts reported as income from a farmer’s woodlot such as Christmas trees, firewood, logs, lumber, and poles)','Farming revenue',1, 0, 1, 1 )
,('9603','Horse racing','Farming revenue',1, 0, 1, 1 )
,('9604','Insurance proceeds(insurance proceeds for the loss of a building to fire or the loss of livestock to disease)','Farming revenue',1, 0, 1, 1 )
,('9605','Patronage dividends(certificate of indebtedness)','Farming revenue',1, 0, 1, 1 )
,('9606','Rental income(building, land, machine, and pasture rental)','Farming revenue',1, 0, 1, 1 )
,('9607','Interest income','Farming revenue',1, 0, 1, 1 )
,('9608','Dividend income','Farming revenue',1, 0, 1, 1 )
,('9609','Gains/losses on disposal of assets(book gains/losses, gain/loss on disposal of fixed assets,profit/loss on disposal of fixed assets, profit/loss on sale of investments, and recaptured depreciation)','Farming revenue',1, 0, 1, 1 )
,('9660','Crop expenses','Farming expenses',1, 0, 1, 1 )
,('9661','Containers, twine, and baling wire','Farming expenses',1, 0, 1, 1 )
,('9662','Fertilizers and lime chemicals','Farming expenses',1, 0, 1, 1 )
,('9663','Pesticides(fungicides, herbicides, and insecticides)','Farming expenses',1, 0, 1, 1 )
,('9664','Seeds and plants','Farming expenses',1, 0, 1, 1 )
,('9710','Livestock expenses(Dairy Herd Improvement As)','Farming expenses',1, 0, 1, 1 )
,('9711','Feed, supplements, straw, and bedding(purchased dairy rations and forage)','Farming expenses',1, 0, 1, 1 )
,('9712','Livestock purchases','Farming expenses',1, 0, 1, 1 )
,('9713','Veterinary fees, medicine, and breeding fees(artificial insemination, disease testing, embryo transplants, neutering, semen, spaying, and stud service)','Farming expenses',1, 0, 1, 1 )
,('9714','Minerals and salts','Farming expenses',1, 0, 1, 1 )
,('9760','Machinery expenses','Farming expenses',1, 0, 1, 1 )
,('9761','Machinery insurance','Farming expenses',1, 0, 1, 1 )
,('9762','Machinery licences','Farming expenses',1, 0, 1, 1 )
,('9763','Machinery repairs','Farming expenses',1, 0, 1, 1 )
,('9764','Machinery fuel(lubricants)','Farming expenses',1, 0, 1, 1 )
,('9765','Machinery lease','Farming expenses',1, 0, 1, 1 )
,('9790','General farm expenses','Farming expenses',1, 0, 1, 1 )
,('9791','Amortization of tangible assets(amortization of leasehold improvements and amounts referred to as depreciation)','Farming expenses',1, 0, 1, 1 )
,('9792','Advertising, marketing costs, and promotion','Farming expenses',1, 0, 1, 1 )
,('9793','Bad debt(allowance for bad debts, allowance/provision for doubtful accounts, bad debt, bad debt written-off, provision for bad debts, and reserve for bad debt)','Farming expenses',1, 0, 1, 1 )
,('9794','Benefits related to employee salaries(contributions to deferred income plans (DPSP/EPSP/RPP), employer’s portion of employee benefits (CPP/EI/QPIP/WCB), group insurance benefits (dental/life/medical plans), payroll deductions, and lodging/room and board)','Farming expenses',1, 0, 1, 1 )
,('9795','Building repairs and maintenance','Farming expenses',1, 0, 1, 1 )
,('9796','Clearing, levelling, and draining land(expenses from building a road, digging/drilling a water well, installing land drainage, ploughing land, and bringing public utilities to the farm)','Farming expenses',1, 0, 1, 1 )
,('9797','Crop insurance, Revenue Protection Program, and stabilization premiums','Farming expenses',1, 0, 1, 1 )
,('9798','Custom or contract work(egg cleaning/grading/sorting/spraying, cheese aging,and contract harvesting/combining/crop dusting)','Farming expenses',1, 0, 1, 1 )
,('9799','Electricity','Farming expenses',1, 0, 1, 1 )
,('9800','Fence repairs and maintenance','Farming expenses',1, 0, 1, 1 )
,('9801','Freight and trucking(delivery and distribution costs and shipping)','Farming expenses',1, 0, 1, 1 )
,('9802','Heating fuel and curing fuel(coal, oil, natural gas, and fuel for curing tobacco/crop drying/greenhouses)','Farming expenses',1, 0, 1, 1 )
,('9803','Insurance program overpayment recapture','Farming expenses',1, 0, 1, 1 )
,('9804','Other insurance premiums(farm insurance, private crop insurance, livestock insurance, and business interruption insurance premiums)','Farming expenses',1, 0, 1, 1 )
,('9805','Interest and bank charges(finance charges, interest on a farm loan, interest on long-term debt, and interest on a mortgage)','Farming expenses',1, 0, 1, 1 )
,('9806','Marketing board fees','Farming expenses',1, 0, 1, 1 )
,('9807','Memberships/subscription fees(association fees)','Farming expenses',1, 0, 1, 1 )
,('9808','Office expenses(farm-related office expenses such as accounting/receipt books, invoices, and stationery)','Farming expenses',1, 0, 1, 1 )
,('9809','Professional fees(amounts reported as farm-related expenses such as accounting/bookkeeping fees, data processing costs, and legal fees)','Farming expenses',1, 0, 1, 1 )
,('9810','Property taxes(land, municipal, and realty taxes)','Farming expenses',1, 0, 1, 1 )
,('9811','Rent – Land and buildings','Farming expenses',1, 0, 1, 1 )
,('9812','Rent – Machinery','Farming expenses',1, 0, 1, 1 )
,('9813','Other rental expenses','Farming expenses',1, 0, 1, 1 )
,('9814','Salaries and wages','Farming expenses',1, 0, 1, 1 )
,('9815','Salaries and wages other than for spouse or dependants salaries for farmhand and self','Farming expenses',1, 0, 1, 1 )
,('9816','Salaries and wages paid to dependants','Farming expenses',1, 0, 1, 1 )
,('9817','Selling costs','Farming expenses',1, 0, 1, 1 )
,('9818','Supplies','Farming expenses',1, 0, 1, 1 )
,('9819','Motor vehicle expenses(automobile expenses, gas, motor vehicle fuel, propane,tires, vehicle repairs and maintenance, and vehicle washing)','Farming expenses',1, 0, 1, 1 )
,('9820','Small tools','Farming expenses',1, 0, 1, 1 )
,('9821','Soil testing','Farming expenses',1, 0, 1, 1 )
,('9822','Storage/drying','Farming expenses',1, 0, 1, 1 )
,('9823','Licences/permits','Farming expenses',1, 0, 1, 1 )
,('9824','Telephone','Farming expenses',1, 0, 1, 1 )
,('9825','Quota rental (tobacco, dairy)','Farming expenses',1, 0, 1, 1 )
,('9826','Gravel','Farming expenses',1, 0, 1, 1 )
,('9827','Purchases of commodities resold','Farming expenses',1, 0, 1, 1 )
,('9828','Salaries and wages paid to spouse','Farming expenses',1, 0, 1, 1 )
,('9829','Motor vehicle interest and leasing costs','Farming expenses',1, 0, 1, 1 )
,('9830','Prepared feed','Farming expenses',1, 0, 1, 1 )
,('9831','Custom feed','Farming expenses',1, 0, 1, 1 )
,('9832','Amortization of intangible assets','Farming expenses',1, 0, 1, 1 )
,('9833','Amortization of milk quota','Farming expenses',1, 0, 1, 1 )
,('9834','Travel expenses','Farming expenses',1, 0, 1, 1 )
,('9835','Capital/business taxes','Farming expenses',1, 0, 1, 1 )
,('9836','Commissions and levies','Farming expenses',1, 0, 1, 1 )
,('9850','Non-farming expenses(If you are not using items 8300 to 9368, use this item to report any non-farming expenses of a farming corporation.)','Farming expenses',1, 0, 1, 1 )
,('9870','Net inventory adjustment(This item may be used to report the farm’s opening inventory less its closing inventory.)If the closing inventory is greater than the opening inventory, this item should be reported as a negative.','Farming expenses',1, 0, 1, 1 )
,('9898','Total farm expenses(This item is intended for use by corporations and partnerships using either the pre-changeover accounting standards (Part V of the CICA Accounting Handbook) or the International Financial Reporting Standards (IFRS) (Part I of the CICA Accounting Handbook) when preparing their financial statements. It represents the sum of:Item 7000 – Revaluation surplus Item 7002 – Defined benefit gains/losses Item 7004 – Foreign operation translation gains/losses Item 7006 – Equity instruments gains/losses Item 7008 – Cash flow hedge effective portion gains/losses Item 7010 – Income tax relating to components of other comprehensive income Item 7020 – Miscellaneous other comprehensive income)','Farming expenses',1, 0, 1, 1 )
,('9899','Net farm income (The amount reported should be equal to the amount reported under item 9659 – Total farm revenue, minus the amount reported under item 9898 – Total farm expenses.)','Farming expenses',1, 0, 1, 1 )
,('9970','Net income/loss before taxes and extraordinary itemS(This item represents the sum of item 9369 – Net non-farming income, and item 9899 – Net farm income.For partnerships, this item is called Net income/loss before extraordinary items.)','Farming expenses',1, 0, 1, 1 )
,('9975','Extraordinary item(s)(This item includes gains/losses resulting from events)','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9976','Legal settlements(settlement of royalties)','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9980','Unrealized gains/losses(unrealized gains/losses resulting from the adjustment of book values on the revaluation of assets )','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9985','Unusual items(asset valuation adjustments such as write-downs and write-offs to net realizable values, items to be scrapped, and gains/losses from discontinued operations or wind-up of subsidiaries/affiliates. This item represents unusual and non-recurring items that do not meet the criteria set out for extraordinary gains/losses.)','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9990','Current income taxes (corporations only)(Canadian income taxes, federal income/large corporation tax, previous year adjustment to federal income/large corporation tax, and provincial and territorial income taxes)','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9995','Future (deferred) income tax provision (corporations only)(Canadian income taxes deferred, provision for deferred income taxes, and provision for future income taxes)','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9998','Total-other comprehensive income(This item is intended for use by corporations and partnerships using either the pre-changeover accounting standards (Part V of the CICA Accounting Handbook) or the International Financial Reporting )','Extraordinary items and income taxes',1, 0, 1, 1 )
,('9999','Net income/loss after taxes and extraordinary items','Extraordinary items and income taxes',1, 0, 1, 1 );

------------------------------------------------------------------------------------
