﻿﻿Alter table: clientquestionanswer


ALTER TABLE `adinovis`.`clientquestionanswer` 
DROP FOREIGN KEY `clientquestionanswer_ibfk_1`;
ALTER TABLE `adinovis`.`clientquestionanswer` 
DROP COLUMN `clientfirmid`,
DROP INDEX `fk_cquesans` ;

ALTER TABLE `adinovis`.`clientquestionanswer` 
ADD COLUMN `engagementsid` BIGINT(20) NOT NULL AFTER `clientquestionanswerid`;

ALTER TABLE `adinovis`.`clientquestionanswer` 
ADD INDEX `engagementsid_ibf_idx` (`engagementsid` ASC) VISIBLE;
;
ALTER TABLE `adinovis`.`clientquestionanswer` 
ADD CONSTRAINT `engagementsid_ibf`
  FOREIGN KEY (`engagementsid`)
  REFERENCES `adinovis`.`engagements` (`engagementsid`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



  Alter Table adinovis.accountlistdata
  ADD COLUMN isprocessed bit(1);
  
  insert into adinovis.status(statusid,statusname, processed, isactive, isdelete, createdby, createddate, modifiedby, modifieddate)
values (10,'Accepted; Data Source not in List' ,'client',b'1',b'0','1',current_timestamp,'1', current_timestamp);

ALTER TABLE `adinovis`.`engagements` 
ADD COLUMN `tbloadstatusid` BIGINT(20) NULL AFTER `statusid`;


ALTER TABLE `adinovis`.`engagements` 
ADD INDEX `fk_ef5_idx` (`tbloadstatusid` ASC) VISIBLE;
;
ALTER TABLE `adinovis`.`engagements` 
ADD CONSTRAINT `fk_ef5`
  FOREIGN KEY (`tbloadstatusid`)
  REFERENCES `adinovis`.`status` (`statusid`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



ALTER TABLE `adinovis`.`clientapidata` 
CHANGE COLUMN `recieveddata` `recieveddata` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `accountlistdata` `accountlistdata` LONGTEXT NULL DEFAULT NULL ;


ALTER TABLE `adinovis`.`trailbalance` 
CHANGE COLUMN `acctcredit` `acctcredit` DECIMAL(20,2) NULL DEFAULT NULL ,
CHANGE COLUMN `acctdebit` `acctdebit` DECIMAL(20,2) NULL DEFAULT NULL ;


insert into adinovis.engagementtype(engagementtype, createdby, modifiedby)
values ('Review', 1,1),
('Audit',1,1);

ALTER TABLE `adinovis`.`fingroup` 
ADD COLUMN `minactid` int(20) NULL AFTER `BSorIS`;

ALTER TABLE `adinovis`.`fingroup` 
ADD COLUMN `maxactid` int(20) NULL AFTER `minactid`;



-- asset
update adinovis.fingroup 
set minactid = 1000
, maxactid = 1999
where fingroupid = 1;

-- Liabilities
update adinovis.fingroup 
set minactid = 2000
, maxactid = 2999
where fingroupid = 2;


-- Equity
update adinovis.fingroup 
set minactid = 3000
, maxactid = 3999
where fingroupid = 3;


-- Expenses
update adinovis.fingroup 
set minactid = 5000
where fingroupid = 4;

-- Revenue
update adinovis.fingroup 
set minactid = 4000
, maxactid = 4999
where fingroupid = 5;



ALTER TABLE `adinovis`.`trailbalancedailyload` 
ADD COLUMN `TBshowonUI` BIT(1) NULL AFTER `modifieddate`;