﻿alter table `adinovis`.`trailbalance` 
ADD COLUMN `isclientdatasource` bit(1) NOT NULL DEFAULT b'1' after acctyear;

alter table `adinovis`.`trailbalancemaping` 
 ADD COLUMN `gificode` int(11) DEFAULT NULL;

 ALTER TABLE `adinovis`.`trailbalancemaping` 
DROP FOREIGN KEY `trailbalancemaping_ibfk_2`;
ALTER TABLE `adinovis`.`trailbalancemaping` 
DROP INDEX `fk_tbmap1` ;

;
ALTER TABLE `adinovis`.`trailbalancemaping` 
ADD CONSTRAINT `trailbalancemaping_ibfk_2`
  FOREIGN KEY (mapsheetid)
  REFERENCES `adinovis`.`mapsheet` (mapsheetid);

  ALTER TABLE `adinovis`.`trailadjustment` 
CHANGE COLUMN `comment` `comment` VARCHAR(255) NULL DEFAULT NULL ,
CHANGE COLUMN `comment1` `comment1` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `adinovis`.`fingroup` 
ADD COLUMN `sequenceorder` INT NULL AFTER `modifieddate`;


ALTER TABLE `adinovis`.`finsubgroup` 
ADD COLUMN `sequenceorder` INT NULL AFTER `modifieddate`;

ALTER TABLE `adinovis`.`fingroup` 
ADD COLUMN `statementtypeid` bigint(20) NULL AFTER `sequenceorder`;

ALTER TABLE `adinovis`.`clientapidata` 
ADD COLUMN `accountlistdata` text NULL AFTER `recieveddata`,

ALTER TABLE `adinovis`.`clientapidata` 
ADD COLUMN `engagementsid` BIGINT(20) NULL AFTER `clientdatasourceid`;

ALTER TABLE `adinovis`.`clientapidata` 
DROP FOREIGN KEY `clientapidata_ibfk_1`;
ALTER TABLE `adinovis`.`clientapidata` 
DROP INDEX `fk_cfapi` ;
;

ALTER TABLE `adinovis`.`clientapidata` 
ADD INDEX `clientapidata_engagementsid_idx` (`engagementsid` ASC) VISIBLE;
;
ALTER TABLE `adinovis`.`clientapidata` 
ADD CONSTRAINT `fk_clientapidata_engagementsid`
  FOREIGN KEY (`engagementsid`)
  REFERENCES `adinovis`.`engagements` (`engagementsid`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `adinovis`.`clientapidata` 
CHANGE COLUMN `clientdatasourceid` `clientdatasourceid` BIGINT(20) NULL ;



ALTER TABLE `adinovis`.`trailbalance` 
DROP FOREIGN KEY `trailbalance_ibfk_3`,
DROP FOREIGN KEY `trailbalance_ibfk_2`;
ALTER TABLE `adinovis`.`trailbalance` 
CHANGE COLUMN `clientdatasourceid` `clientdatasourceid` BIGINT(20) NULL ,
CHANGE COLUMN `clientapidataid` `clientapidataid` BIGINT(20) NULL ,
DROP INDEX `fk_tb2` ,
DROP INDEX `fk_tb1` ;
;


drop TABLE adinovis.`trailbalancedailyload`;

CREATE TABLE adinovis.`trailbalancedailyload` (
  `trailbalancedailyloadid` bigint(20) NOT NULL AUTO_INCREMENT,
  `engagementsid` bigint(20) DEFAULT NULL,
  `recieveddate` datetime DEFAULT NULL,
  `accountcode` varchar(100) DEFAULT NULL,
  `accountname` varchar(255) DEFAULT NULL,
  `accounttype` varchar(100) DEFAULT NULL,
  `acctcredit` decimal(10,2) DEFAULT NULL,
  `acctdebit` decimal(10,2) DEFAULT NULL,
  `acctyear` varchar(50) DEFAULT NULL,
  `isprocessed` bit(1) NOT NULL DEFAULT b'0',
  `trailbalanceid` bigint(20) DEFAULT NULL,
  `isdifferwithexisting` bit(1) NOT NULL DEFAULT b'0',
  `isnewrecord` bit(1) NOT NULL DEFAULT b'0',
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`trailbalancedailyloadid`),
  KEY `trailbalancedailyload_ibfk_1_idx` (`engagementsid`),
  CONSTRAINT `trailbalancedailyload_ibfk_1_idx` FOREIGN KEY (`engagementsid`) REFERENCES `engagements` (`engagementsid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci






update adinovis.finsubgroup
set fingroupid = 4
where finsubgroupid in (26,27);

update adinovis.finsubgroup
set fingroupid = 5
where finsubgroupid in (21);

update adinovis.finsubgroup
set sequenceorder = 5
where finsubgroupid in (21);

update adinovis.finsubgroup
set finsubgroupname = 'Capital assets'
where finsubgroupid = 2;


update adinovis.finsubgroup
set finsubgroupname = 'Intangible assets'
where finsubgroupid = 3;


update adinovis.finsubgroup
set finsubgroupname = 'Long-term assets (partnerships)'
where finsubgroupid = 6;


update adinovis.finsubgroup
set finsubgroupname = 'non-profit'
where finsubgroupid = 19;



update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 1;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 2;
update adinovis.finsubgroup set sequenceorder = 3 where finsubgroupid = 3;
update adinovis.finsubgroup set sequenceorder = 4 where finsubgroupid = 4;
update adinovis.finsubgroup set sequenceorder = 5 where finsubgroupid = 5;
update adinovis.finsubgroup set sequenceorder = 6 where finsubgroupid = 6;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 7;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 8;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 9;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 10;
update adinovis.finsubgroup set sequenceorder = 3 where finsubgroupid = 11;
update adinovis.finsubgroup set sequenceorder = 4 where finsubgroupid = 12;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 13;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 14;
update adinovis.finsubgroup set sequenceorder = 3 where finsubgroupid = 15;
update adinovis.finsubgroup set sequenceorder = 4 where finsubgroupid = 16;
update adinovis.finsubgroup set sequenceorder = 5 where finsubgroupid = 17;
update adinovis.finsubgroup set sequenceorder = 6 where finsubgroupid = 18;
update adinovis.finsubgroup set sequenceorder = 7 where finsubgroupid = 19;
update adinovis.finsubgroup set sequenceorder = 8 where finsubgroupid = 20;
update adinovis.finsubgroup set sequenceorder = 9 where finsubgroupid = 28;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 22;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 23;
update adinovis.finsubgroup set sequenceorder = 3 where finsubgroupid = 24;
update adinovis.finsubgroup set sequenceorder = 4 where finsubgroupid = 25;
update adinovis.finsubgroup set sequenceorder = 5 where finsubgroupid = 21;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 26;
update adinovis.finsubgroup set sequenceorder = 2 where finsubgroupid = 27;
update adinovis.finsubgroup set sequenceorder = 1 where finsubgroupid = 29;







drop table adinovis.`clientdatasource_history`;

CREATE TABLE adinovis.`clientdatasource_history` (
  `clientdatasourceHistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientdatasourceid` bigint(20) ,
  `engagementsid` bigint(20) DEFAULT NULL,
  `clientfirmid` bigint(20) DEFAULT NULL,
  `sourcelink` varchar(255) DEFAULT NULL,
  `clientid` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  `realmid` varchar(4000) DEFAULT NULL,
  `refreshtoken` varchar(4000) DEFAULT NULL,
  `accesstoken` varchar(4000) DEFAULT NULL,
  `isactive` bit(1)  DEFAULT NULL ,
  `isdelete` bit(1)  DEFAULT NULL ,
  `createdby` varchar(100) DEFAULT  NULL,
  `createddate` datetime  DEFAULT NULL,
  `modifiedby` varchar(100)  DEFAULT NULL,
      `modifieddate` datetime DEFAULT NULL,
  `historyloaddate` datetime  DEFAULT NULL,
  PRIMARY KEY (`clientdatasourceHistoryid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
