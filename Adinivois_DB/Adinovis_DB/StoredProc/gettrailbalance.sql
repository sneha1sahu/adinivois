﻿CREATE DEFINER=`admin`@`%` PROCEDURE `gettrailbalance`(Pengagementid int(20), inyear int(20))
BEGIN


with engtotal as
(
select 
	engagementsid
, 	sum(originalbalance) as originalbalancetotal
,	sum(adjustmentamount) as adjustmentamounttotal
,	sum(finalamount) as finalamounttotal
,	sum(PY1) as PY1total
,	sum(PY2) as PY2total
from adinovis.vtbmapping 
where engagementsid = Pengagementid and acctyear = inyear
)
SELECT distinct 
vt.trailbalanceid,
vt.engagementsid,
vt.clientdatasourceid,
vt.clientapidataid,
vt.recieveddate,
vt.accountcode,
vt.accountname,
vt.accounttype,
case 
when vt.originalbalance < 0 then concat('(', FORMAT(-(vt.originalbalance),2),')')
else
FORMAT(vt.originalbalance,2)
end as originalbalance,
vt.trailadjustmentid,
vt.adjustmenttypeid,
vt.adjustmenttypename,
case 
when vt.adjustmentamount < 0 then concat('(', FORMAT(-(vt.adjustmentamount),2),')')
else
FORMAT(vt.adjustmentamount,2)
end as adjustmentamount,
case 
when vt.finalamount < 0 then concat('(', FORMAT(-(vt.finalamount),2),')')
else
FORMAT(vt.finalamount,2)
end as finalamount,
case 
when vt.PY1 < 0 then concat('(', FORMAT(-(vt.PY1),2),')')
else
FORMAT(vt.PY1,2)
end as PY1,
case 
when vt.changes < 0 then concat('(', FORMAT(-(vt.changes),2),')')
else
FORMAT(vt.changes,2)
end as changes,
case 
when vt.PY2 < 0 then concat('(', FORMAT(-(vt.PY2),2),')')
else
FORMAT(vt.PY2,2)
end as PY2,
vt.trailbalancemapingid,
vt.mapsheetid,
vt.mapno,
vt.mapsheetname,
vt.fingroupid,
vt.fingroupname,
vt.finsubgrpid,
vt.finsubgroupname,
vt.finsubgroupchildId,
vt.finsubgroupchildname,
vt.leadsheetid,
vt.leadsheetname,
vt.leadsheetcode,
vt.balancetypeid,
vt.balancetypename,
vt.statementtypeid,
vt.statementtypename,
vt.Notes,
vt.gificode,
case 
when vt.fxtranslation < 0 then concat('(', FORMAT(-(vt.fxtranslation),2),')')
else
FORMAT(vt.fxtranslation,2)
end as fxtranslation,
vt.isclientdatasource,
vt.mappingstatus,
vt.mappingstatusid,
-- FORMAT(et.originalbalancetotal,2) as originalbalancetotal,
case 
when et.originalbalancetotal < 0 then concat('(', FORMAT(-(et.originalbalancetotal),2),')')
else
FORMAT(et.originalbalancetotal,2)
end as originalbalancetotal,
case 
when et.adjustmentamounttotal < 0 then concat('(', FORMAT(-(et.adjustmentamounttotal),2),')')
else
FORMAT(et.adjustmentamounttotal,2)
end as adjustmentamounttotal,
-- FORMAT(et.finalamounttotal,2) as finalamounttotal,
case 
when et.finalamounttotal < 0 then concat('(', FORMAT(-(et.finalamounttotal),2),')')
else
FORMAT(et.finalamounttotal,2)
end as finalamounttotal,
case 
when et.PY1total < 0 then concat('(', FORMAT(-(et.PY1total),2),')')
else
FORMAT(et.PY1total,2)
end as PY1total,
case 
when et.PY2total < 0 then concat('(', FORMAT(-(et.PY2total),2),')')
else
FORMAT(et.PY2total,2)
end as PY2total,
case when et.originalbalancetotal <> 0 then 'true' else 'false' end as originalred,
case when et.finalamounttotal <> 0 then 'true' else 'false' end as finalred

from adinovis.vtbmapping vt
join engtotal et on vt.engagementsid = et.engagementsid
where vt.engagementsid = Pengagementid
and vt.acctyear = inyear 
union 

SELECT distinct 
coalesce(tb.trailbalanceid,0),
coalesce(tb.engagementsid,0),
0 as clientdatasourceid,
0 as clientapidataid,
tb.recieveddate,
tb.accountcode,
tb.accountname,
tb.accounttype,
case when coalesce(`tb`.`acctcredit`,0) = 0 then
	case when coalesce(`tb`.`acctdebit`,0) <0 then concat('(', FORMAT(coalesce(`tb`.`acctdebit`,0),2),')') else coalesce(`tb`.`acctdebit`,0) end
else 
	case when coalesce(`tb`.`acctcredit`,0) <0 then concat('(', FORMAT(coalesce(`tb`.`acctcredit`,0),2),')') else coalesce(`tb`.`acctcredit`,0) end
end as originalbalance,
0 as trailadjustmentid,
0 as adjustmenttypeid,
null as adjustmenttypename,
null as adjustmentamount,
null as finalamount,
null as PY1,
null as changes,
null as PY2,
0 as trailbalancemapingid,
0 as mapsheetid,
null as mapno,
null as mapsheetname,
null as fingroupid,
null as fingroupname,
null as finsubgrpid,
null as finsubgroupname,
null as finsubgroupchildId,
null as finsubgroupchildname,
null as leadsheetid,
null as leadsheetname,
null as leadsheetcode,
null as balancetypeid,
null as balancetypename,
null as statementtypeid,
null as statementtypename,
null as Notes,
null as gificode,
null as fxtranslation,
tb.isprocessed  as isclientdatasource,
-- null as mappingstatus,
 case when coalesce(tb.trailbalanceid,0) > 0 then tt.mappingstatus else 
	case when coalesce(tb.trailbalanceid,0) = 0 then 'New row' else null end  end as mappingstatus,
-- case when coalesce(tb.trailbalanceid,0) > 0 then tt.mappingstatusid else null  end as mappingstatusid,
 case when coalesce(tb.isnewrecord,0) = 1 then 3 else tt.mappingstatusid end  as mappingstatusid,
null as originalbalancetotal,
null as adjustmentamounttotal,
null as finalamounttotal,
null as PY1total,
null as PY2total,
null originalred,
null finalred
from adinovis.trailbalancedailyload tb
join engtotal et on tb.engagementsid = et.engagementsid
left join adinovis.vtbmapping tt on tb.engagementsid = Pengagementid and tb.acctyear = inyear and tb.trailbalanceid = tt.trailbalanceid
where tb.engagementsid = Pengagementid
and tb.acctyear = inyear 
and isprocessed = 1
and (isdifferwithexisting = 1 or isnewrecord = 1) 
and tb.TbshowonUI = 1
order by accountcode, accountname,trailbalanceid;


END