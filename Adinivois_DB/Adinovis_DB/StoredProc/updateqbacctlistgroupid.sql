﻿CREATE DEFINER=`admin`@`%` PROCEDURE `updateqbacctlistgroupid`(IN inengagementsid int(20))
BEGIN

-- updating group id basis on detials type from acct list
update adinovis.accountlistdata
set fingroupid = case 	when detailtype like '%Assets%' then 1
						when detailtype like '%Liabilities%' then 2
                        when detailtype like '%Equity%' then 3
                        when detailtype like '%Expenses%' then 4
                        when detailtype like '%Expense%' then 4
                        when detailtype like '%Cost of Goods Sold%' then 4
                        when detailtype like '%Revenue%' then 5
                        when detailtype like '%Income%' then 5
					else null end
where engagementsid=inengagementsid ;



update adinovis.accountlistdata
set fingroupid = case 	when type like '%Assets%' then 1
						when type like '%Liabilities%' then 2
                        when type like '%Equity%' then 3
                        when type like '%Expenses%' then 4
                        when type like '%Expense%' then 4
                        when type like '%Cost of Goods Sold%' then 4
                        when type like '%Revenue%' then 5
                        when type like '%Income%' then 5
					else null end
where engagementsid=inengagementsid ;


-- updating group id basis on acct no
with updffgid as
(
select distinct
accountlistdataid as actid
, case when left(act.account,4) between minactid  and maxactid then fg.fingroupid 
		when left(act.account,4)  >= 5000 then 4 else 0 end fgid 
from adinovis.accountlistdata  act 
join adinovis.fingroup fg on 1= 1
where left(act.account,4) > 0
and engagementsid=inengagementsid 
and case when left(act.account,4) between minactid  and maxactid then fg.fingroupid 
		when left(act.account,4)  >= 5000 then 4 else 0 end > 0
)
update adinovis.accountlistdata
set fingroupid = (select fgid from updffgid where accountlistdataid = actid )
where left(account,4) > 0
and engagementsid=inengagementsid ;



-- left over acct group id udpate basis on previous and next record value
with misfgids as
(
select distinct
act.accountlistdataid as actids
, case when left(actmin.account,4) between minactid  and maxactid  
		then fg.fingroupid 
		when left(actmin.account,4)  >= 5000 then 4 
        else 0 end fgid 
from adinovis.accountlistdata act
join adinovis.accountlistdata actmin on act.accountlistdataid = actmin.accountlistdataid+1
join adinovis.accountlistdata actpl on act.accountlistdataid = actpl.accountlistdataid-1
join adinovis.fingroup fg on 1= 1
where act.engagementsid=inengagementsid 
and act.fingroupid is null
and case when left(actmin.account,4) between minactid  and maxactid  
		then fg.fingroupid when left(actmin.account,4)  >= 5000 then 4  else 0 end  >0
)
update adinovis.accountlistdata
set fingroupid = (select fgid from misfgids where accountlistdataid = actids )
where fingroupid is null
and engagementsid=inengagementsid ;

with misfgids as
(
select distinct
  act.accountlistdataid as actids
, act.fingroupid
, (select distinct accountlistdataid from adinovis.accountlistdata where engagementsid = act.engagementsid and act.accountlistdataid <= accountlistdataid and fingroupid is not null order by accountlistdataid limit 1  ) previd
, (select distinct accountlistdataid from adinovis.accountlistdata where engagementsid = act.engagementsid and act.accountlistdataid >= accountlistdataid and fingroupid is not null order by accountlistdataid desc limit 1 ) prvvid
from adinovis.accountlistdata act
where act.engagementsid=inengagementsid 
and act.fingroupid is null
)
 , fgidsext as
 (
select m.actids, a.fingroupid
from misfgids m
join adinovis.accountlistdata a on m.previd= a.accountlistdataid
)
update adinovis.accountlistdata
set fingroupid = (select fingroupid from fgidsext where accountlistdataid = actids )
where fingroupid is null
and engagementsid=inengagementsid ;



END