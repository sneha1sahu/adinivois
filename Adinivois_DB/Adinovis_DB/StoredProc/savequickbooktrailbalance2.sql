﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savequickbooktrailbalance2`()
BEGIN
DECLARE vtrailbalanceid INT(20);
DECLARE vmapsheetid INT(20);
DECLARE vparentmapsheetid INT(20);
Declare nodate date;

DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';
DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET  autocommit=0;
SET nodate = current_timestamp;

select tb.trailbalanceid,map_t.mapsheetid,parentmapsheetid into vtrailbalanceid, vmapsheetid, vparentmapsheetid 
from adinovis.trailbalance tb,adinovis.mapsheet_test map_t where tb.accountname = map_t.mapsheetname;

BEGIN 
INSERT INTO adinovis.trailbalancemaping_test(
trailbalanceid,
mapsheetid,
mapsheetchildid,
isactive,
isdelete,
createdby,
createddate,
modifiedby,
modifieddate
)		
		VALUES (vtrailbalanceid,
		vmapsheetid,
		vparentmapsheetid,
		b'0',
		b'1',
		'1',
		nodate,
        '1',
		nodate
        );
END;
  --  set dbstatus = 1;

	BEGIN
	IF errorCode != '00000' THEN
    ROLLBACK;
  --  set dbstatus = -1;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.savequickbooktrailbalance2',nodate); 
	end if;
    END;    
COMMIT; 

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END