﻿/********************************************************************************************************************************
** SP Name: saveclientdata
**
** Purpose: Save client Quick books and Xero data into DB.
**
**Parameters: 	clientdatasourceid
**				recieveddata
**				responsestatus
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 22-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `saveclientdata`(IN inengagementsid bigint(20),
											IN Precieveddata longtext,
											IN Presponsestatus varchar(100),
                                            IN inaccountlist longtext
											-- OUT dbstatus int(10)
                                            )
BEGIN
DECLARE vuseraccountid INT(20);
DECLARE vclientdatasourceid INT(20);
DECLARE vclientfirmid int(20);
DECLARE vrecieveddata longtext;
DECLARE Vresponsestatus varchar(100);
Declare nodate datetime;
Declare vacctyear varchar(50);

DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;


SET  autocommit=0;
-- SET dbstatus = null;
-- SET vrecieveddata = Precieveddata;
SET Vresponsestatus = Presponsestatus;
set nodate = current_timestamp;

select useraccountid into vuseraccountid from adinovis.clientfirm cf join adinovis.engagements eg on cf.clientfirmid = eg.clientfirmid
where eg.engagementsid = inengagementsid limit 1;

select year(cast(replace(json_extract(Precieveddata,'$.header.endPeriod'), '"',"")as date))  into vacctyear;


		BEGIN 
		INSERT INTO adinovis.clientapidata(
		engagementsid,
		recieveddata,
        accountlistdata,
		responsestatus,
        acctyear,
		isprocessed,
		isactive,
		isdelete,
		createdby,
		createddate,
		modifiedby,
		modifieddate)		
		VALUES (inengagementsid,
		Precieveddata,
        inaccountlist,
		Vresponsestatus,
        vacctyear,
		b'0',
		b'1',
		b'0',
		CAST(vuseraccountid AS CHAR),
		nodate,
		CAST(vuseraccountid AS CHAR),
		nodate);
        
       /* update adinovis.engagements set statusid = 7 
        where engagementsid = inengagementsid; */
		
		
        
        
		
		END;
  --  set dbstatus = 1;
	
	
	
	BEGIN
    
	IF errorCode != '00000' THEN
    ROLLBACK;
  --  set dbstatus = -1;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.saveclientdata',nodate); 
	end if;
    END;    


COMMIT; 

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

-- select  dbstatus;
END