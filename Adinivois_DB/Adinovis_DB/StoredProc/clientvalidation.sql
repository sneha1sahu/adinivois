﻿/********************************************************************************************************************************
** SP Name: clientvalidation
**
** Purpose: Validate that client is exist and send the URL+Token value to UI.
**
** Parameters: 
**             emailid			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 19-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `clientvalidation`(IN emailid varchar(100))
BEGIN
/* Variable Declaration*/
Declare checkemail int(2);
Declare vuseraccountid int(20);
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/*Error Handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variables value*/
set  autocommit=1;
set checkemail =0;
select 1, useraccountid into checkemail, vuseraccountid  from adinovis.useraccount where emailaddress = emailid and statusid = 4;

/* if user exist then send URL+Token value to UI*/
if checkemail = 1 then
select concat(a.advpropertievalue,u.tokenvalue) as clientlogin from adinovis.useraccountauth u, adinovis.advpropertie a 
where u.useraccountid = vuseraccountid and a.advpropertiename = 'clientlogin' and 1=1;
else
set errorCode = '99999';
         set errorMessage = 'token invalid';
	END IF;
COMMIT;
END