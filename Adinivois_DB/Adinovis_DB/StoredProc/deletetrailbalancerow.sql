﻿CREATE DEFINER=`admin`@`%` PROCEDURE `deletetrailbalancerow`(in intrailbalid bigint , ploginid int)
BEGIN
	Declare checkdsrc int;
	Declare nodate datetime;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';


    /* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;


 	BEGIN 
       /*START TRANSACTION */
		set nodate = current_timestamp;
		set checkdsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE isclientdatasource = 0 and trailbalanceid = intrailbalid ) ,1,0));
			IF (checkdsrc = 1 ) THEN
				update adinovis.trailbalance 
					set isdelete = 1
					, 	isactive = 0
					,	modifiedby = cast(ploginid AS CHAR)
					, 	modifieddate = nodate
				where isclientdatasource = 0 and trailbalanceid = intrailbalid;
			else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
			end if;		
            
	end;
	BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.deletetrailbalancerow',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
	
END