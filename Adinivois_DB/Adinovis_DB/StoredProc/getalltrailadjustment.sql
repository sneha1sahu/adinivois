﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getalltrailadjustment`( inengagementsid bigint )
BEGIN
	Declare checkengadjust int;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';


set checkengadjust = (SELECT IF ( EXISTS ( select 1 from trailadjustment taj join adinovis.trailbalance tb on taj.trailbalanceid = tb.trailbalanceid and tb.isactive = 1 and tb.isdelete = 0 and tb.engagementsid = inengagementsid ) ,1,0));

	if ( checkengadjust = 1 ) then		
		select  distinct
				taj.trailadjustmentid
			, 	taj.trailbalanceid
			, 	taj.adjustmenttypeid
			,	atj.adjustmenttypename
			, 	tb.accountcode
			, 	tb.accountname
			,	taj.acctcredit
			, 	taj.acctdebit
			, 	taj.comment
			, 	taj.comment1
			, 	taj.modifiedby
			,	CONCAT(COALESCE(`ua`.`firstname`, ' '),COALESCE(`ua`.`middlename`, ' '), COALESCE(`ua`.`lastname`, ' ')) AS fullname

		from adinovis.trailadjustment taj
		join adjustmenttype atj on taj.adjustmenttypeid = atj.adjustmenttypeid and atj.isactive = 1 and atj.isdelete = 0
		join trailbalance tb on taj.trailbalanceid = tb.trailbalanceid and tb.isactive = 1 and tb.isdelete = 0
		join useraccount ua on taj.modifiedby = ua.useraccountid and ua.isactive = 1 and ua.isdelete = 0
		where taj.isactive = 1 and taj.isdelete = 0
		and tb.engagementsid = inengagementsid;
	else
		set errorCode = '99999';
		set errorMessage = 'adjustment entry not found for the engagment';
	end if;


END