﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getbalancesheet`(inengagementid int(20), inyear int(20))
BEGIN
   Declare tbmapcnt int(20);
   DECLARE statementtype int ;
   DECLARE crtdeforg varchar(100) ;
   DECLARE prvdefprv varchar(100) ;
   DECLARE equchild varchar(4000) ;
    DECLARE equgrp varchar(4000) ;
	DECLARE libilgrp varchar(4000) ;
	DECLARE assetsgrp varchar(4000) ;
    DECLARE lieqtotal varchar(4000) ;
    DECLARE unmpjson varchar(4000) ;
    DECLARE equpy1sum int(20);
     DECLARE unmpcnt int(20);
    
drop TEMPORARY TABLE if exists tbloadeng;
drop TEMPORARY TABLE if exists equitytt;
drop TEMPORARY TABLE if exists tpequgrp;
drop TEMPORARY TABLE if exists tlinisubchild;
drop TEMPORARY TABLE if exists tasstsbgrp;
drop TEMPORARY TABLE if exists tastcchild;


drop TEMPORARY TABLE if exists tplibli;
drop TEMPORARY TABLE if exists tplibgrp;
drop TEMPORARY TABLE if exists tasstgrp;

drop TEMPORARY TABLE if exists unmdata;

 CREATE TEMPORARY TABLE tbloadeng
( tlid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, finsubgroupchildId int
, finsubgroupchildname varchar(255)
, statementtypeid int
, statementtypename varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, acctyear varchar(255)
,  engagementsid	int
, accountname varchar(255)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

-- equity
 CREATE TEMPORARY TABLE equitytt
( tpequid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`tpequid`)
)AUTO_INCREMENT=0;

-- libility 
 CREATE TEMPORARY TABLE tplibli
( tplibliid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`tplibliid`)
)AUTO_INCREMENT=0;


 CREATE TEMPORARY TABLE tlinisubchild
( id bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, finsubgrpid int
, finsubgroupchildname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`id`)
)AUTO_INCREMENT=0;


-- assets
CREATE TEMPORARY TABLE tasstsbgrp
( tplibliid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`tplibliid`)
)AUTO_INCREMENT=0;


 CREATE TEMPORARY TABLE tastcchild
( id bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, finsubgrpid int
, finsubgroupchildname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`id`)
)AUTO_INCREMENT=0;


-- group totals

-- equity
 CREATE TEMPORARY TABLE tpequgrp
( tpequid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`tpequid`)
)AUTO_INCREMENT=0;


-- libility
 CREATE TEMPORARY TABLE tplibgrp
( tplibgrpid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`tplibgrpid`)
)AUTO_INCREMENT=0;

-- assets
 CREATE TEMPORARY TABLE tasstgrp
( id bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`id`)
)AUTO_INCREMENT=0;

 CREATE TEMPORARY TABLE unmdata
( id bigint NOT NULL AUTO_INCREMENT
, tlid int
, accountname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`id`)
)AUTO_INCREMENT=0;


-- balance sheet
set statementtype = 1;


-- coding start

-- validating if we have trail balance mapping for balance sheet
select count(tbm.trailbalanceid) into tbmapcnt from adinovis.trailbalancemaping tbm
join adinovis.trailbalance tb on tbm.trailbalanceid = tb.trailbalanceid and tb.isactive  = 1 and tb.isdelete = 0
and tb.engagementsid=inengagementid and tb.acctyear = inyear
where tbm.isactive  = 1 and tbm.isdelete = 0;




if ( tbmapcnt > 0 ) then
	begin

		-- equity process start
			insert into tbloadeng
                (fingroupid, fingroupname, finsubgrpid, finsubgroupname, finsubgroupchildId, finsubgroupchildname,statementtypeid 
					,statementtypename,originalbalance, finalamount,PY1,acctyear,engagementsid,accountname)
				select 
					vt.fingroupid
                , 	vt.fingroupname
                ,	vt.finsubgrpid
                ,	vt.finsubgroupname
                , 	vt.finsubgroupchildId
                ,	vt.finsubgroupchildname
                , 	vt.statementtypeid
                ,	vt.statementtypename
                ,	vt.originalbalance 
                ,   coalesce(vt.finalamount,0) as finalamount
                , 	coalesce(vt.PY1,0) as PY1
                ,	vt.acctyear
                ,	vt.engagementsid  
                ,	vt.accountname
				from adinovis.vtbmapping vt
				where vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.finalamount != 0
                union all
				select distinct
						vt.fingroupid
					, 	vt.fingroupname
					,	vt.finsubgrpid
					,	vt.finsubgroupname
					, 	vt.finsubgroupchildId
					,	vt.finsubgroupchildname
					, 	vt.statementtypeid
					,	vt.statementtypename
					,	0 as originalbalance
					, 	0 as finalamount
                    ,	0 as PY1
					,	inyear as acctyear
					,	inengagementid as engagementsid     
                    ,	null as accountname
                from adinovis.vmapsheet vt
                where vt.fingroupid = 3
                and vt.finsubgrpid =11
                and vt.finsubgroupchildId = 76;
                
                -- unmapped data load
					insert into unmdata
					(tlid, accountname, finalamount, PY1 )
					select 5 as tlid
					, accountname 
					, coalesce(sum(finalamount),0) as finalamount
					, coalesce(sum(PY1),0) as PY1
					from tbloadeng where fingroupid is null
					group by accountname;

					select count(tlid) into unmpcnt from unmdata;
                
                
			
				 select coalesce(sum(finalamount),0) into crtdeforg from tbloadeng where fingroupid > 3 and fingroupid is not null;
				 select coalesce(sum(PY1),0) into prvdefprv from tbloadeng where fingroupid > 3 and fingroupid is not null;
			

						insert into equitytt
						(  fingroupid 
						, fingroupname 
						, finsubgrpid 
						, finsubgroupname 
						, originalbalance 
						, finalamount 
						, PY1 
                        , chldid
						, sequenceorder	
						)
						select 
						  t.fingroupid
						, t.fingroupname
						, t.finsubgrpid
						, t.finsubgroupname
						, format(sum(t.originalbalance),0) as originalbalance
						, case when t.finsubgroupchildId = 76 then crtdeforg else sum(t.finalamount) end as finalamount
						, case when t.finsubgroupchildId = 76 then prvdefprv else  coalesce(sum(t.PY1),0) end as PY1
						, concat(t.fingroupid,'.',t.finsubgrpid,'.',fsg.sequenceorder) as chldid
						, fsg.sequenceorder
						from tbloadeng t
						join finsubgroup fsg on t.fingroupid = fsg.fingroupid and t.finsubgrpid = fsg.finsubgroupid
						where t.fingroupid = 3
						group by t.fingroupid,t.finsubgrpid
						order by fsg.sequenceorder;
                        
                        
					 insert into tpequgrp
					(  fingroupid 
					, fingroupname 
					, finalamount 
					, PY1 
					)
					select 
                      fingroupid
                    , fingroupname
					, coalesce(sum(finalamount),0) + crtdeforg
					, coalesce(sum(PY1),0) + prvdefprv
                    from tbloadeng
                    where fingroupid = 3;

					-- validating if we have previous year details

					select coalesce(count(PY1),0) into equpy1sum from tbloadeng where fingroupid is not null and PY1 != 0;

					if ( equpy1sum > 0 ) then
						begin 
							set equgrp = (select JSON_ARRAYAGG(json_object
										('id',	t.fingroupid
                                            , 'name', t.fingroupname
                                            , 'Currgrouptotal', 
												case when coalesce(t.finalamount,0) < 0 then   concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
													else format(t.finalamount,0) end	
                                            
											, 'Prevgrouptotal' ,
                                            		case when coalesce(t.PY1,0) < 0 then   concat('(',format(right(t.PY1,(length(t.PY1)-1)),0),')')
													else format(t.PY1,0) end	
                                            
                                            , 'children'     
                                            ,(
                                            select JSON_ARRAYAGG(json_object
										('id',	ct.fingroupid
										, 'chldid', ct.chldid
										, 'name', ct.finsubgroupname
										, 'Currbal' ,
												case when coalesce(ct.finalamount,0) < 0 then   concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')')
													else format(ct.finalamount,0) end	
										, 'Prevbal' ,
												case when coalesce(ct.PY1,0) < 0 then   concat('(',format(right(ct.PY1,(length(ct.PY1)-1)),0),')')
													else format(ct.PY1,0) end	
										)) 
										from equitytt ct
                                        where ct.fingroupid = t.fingroupid
										order by ct.sequenceorder
                                            )
										))
										from tpequgrp t);  
						end;
					else
						begin
						set equgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', case when coalesce(t.finalamount,0) < 0 then   concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
													else format(t.finalamount,0) end	
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupname
											, 'Currbal' ,
												case when coalesce(ct.finalamount,0) < 0 then   concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')')
													else format(ct.finalamount,0) end	
											)) 
											from equitytt ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from tpequgrp t);     
						end;
					end if;

				-- equity process end


			--  Liabilities process start

					insert into tplibgrp
					(  fingroupid 
					, fingroupname 
					, finalamount 
					, PY1 
					)
					select 
                      fingroupid
                    , fingroupname
					, coalesce(sum(finalamount),0)
					, coalesce(sum(PY1) ,0)
                    from tbloadeng
                    where fingroupid = 2;

					

					insert into tplibli
						(  fingroupid 
						, fingroupname 
						, finsubgrpid 
						, finsubgroupname 
						, originalbalance 
						, finalamount 
						, PY1 
                        , chldid
						, sequenceorder	
						)
						select 
						  t.fingroupid
						, t.fingroupname
						, t.finsubgrpid
						, t.finsubgroupname
						, coalesce(sum(t.originalbalance),0) as originalbalance
						, coalesce(sum(t.finalamount),0)  as finalamount
						, coalesce(sum(t.PY1),0)  as PY1
						, concat(t.fingroupid,'.',t.finsubgrpid,'.',fsg.sequenceorder) as chldid
						, fsg.sequenceorder
						from tbloadeng t
						join finsubgroup fsg on t.fingroupid = fsg.fingroupid and t.finsubgrpid = fsg.finsubgroupid
						where t.fingroupid = 2
						group by t.fingroupid,t.finsubgrpid
						order by fsg.sequenceorder;
                        
				insert into tlinisubchild
                ( fingroupid
                , finsubgrpid
                , finsubgroupchildname
                , finalamount
                , PY1
                , chldid
                , sequenceorder
                )         
				select 
					t.fingroupid
				,   t.finsubgrpid
				, 	t.finsubgroupchildname
                , 	coalesce(sum(t.finalamount),0) as finalamount
                ,	coalesce(sum(t.PY1),0) as py1
                , 	concat(t.fingroupid,'.',t.finsubgrpid,'.',t.finsubgroupchildId,'.',fsg.sequenceorder) as chldid
                ,	fsg.sequenceorder
                from tbloadeng t
               join finsubgroup fsg on t.fingroupid = fsg.fingroupid  and t.finsubgrpid = fsg.finsubgroupid
                where t.fingroupid = 2  and t.finsubgrpid = 7
                group by t.fingroupid
				, 	t.finsubgroupchildname
                order by fsg.sequenceorder;
                        
                        

				if ( equpy1sum > 0 ) then
						begin 
						set libilgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', case when coalesce(t.finalamount,0) < 0 then   concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
                                             					else format(t.finalamount,0) end
                                             , 'Prevgrouptotal' ,
												case when coalesce(t.PY1,0) < 0 then   concat('(',format(right(t.PY1,(length(t.PY1)-1)),0),')')
													else format(t.PY1,0) end	
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupname
											, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')') 
													else format(ct.finalamount,0) end	
                                            , 'Prevbal' ,
												case when coalesce(ct.PY1,0) < 0 then   concat('(',format(right(ct.PY1,(length(ct.PY1)-1)),0),')')
													else format(ct.PY1,0) end	
											, 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	cth.fingroupid
											, 'chldid', cth.chldid
											, 'name', cth.finsubgroupchildname
											, 'Currbal' ,
                                            case when coalesce(cth.finalamount,0) < 0 then  concat('(',format(right(cth.finalamount,(length(cth.finalamount)-1)),0),')') 
													else format(cth.finalamount,0) end	                                            
                                              , 'Prevbal' ,
												case when coalesce(cth.PY1,0) < 0 then   concat('(',format(right(cth.PY1,(length(cth.PY1)-1)),0),')')
													else format(cth.PY1,0) end	
											)) 
											from tlinisubchild cth
                                            where cth.fingroupid = t.fingroupid
											and cth.finsubgrpid = ct.finsubgrpid
											order by ct.sequenceorder
                                            )
											)) 
											from tplibli ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from tplibgrp t);   
						end;
					else
						begin
						set libilgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', case when coalesce(t.finalamount,0) < 0 then  concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
                                             					else format(t.finalamount,0) end
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupname
											, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')') 
													else format(ct.finalamount,0) end	
											, 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	cth.fingroupid
											, 'chldid', cth.chldid
											, 'name', cth.finsubgroupchildname
											, 'Currbal' ,  case when coalesce(cth.finalamount,0) < 0 then  concat('(',format(right(cth.finalamount,(length(cth.finalamount)-1)),0),')') 
													else format(cth.finalamount,0) end	 
											)) 
											from tlinisubchild cth
                                            where cth.fingroupid = t.fingroupid
											and cth.finsubgrpid = ct.finsubgrpid
											order by ct.sequenceorder
                                            )
											)) 
											from tplibli ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from tplibgrp t);     
						end;
					end if;

	--  Liabilities process end


		--  assets  process start
					insert into tasstgrp
					(  fingroupid 
					, fingroupname 
					, finalamount 
					, PY1 
					)
					select 
                      fingroupid
                    , fingroupname
					, sum(finalamount)
					, sum(PY1) 
                    from tbloadeng
                    where fingroupid = 1;

					

					insert into tasstsbgrp
						(  fingroupid 
						, fingroupname 
						, finsubgrpid 
						, finsubgroupname 
						, originalbalance 
						, finalamount 
						, PY1 
                        , chldid
						, sequenceorder	
						)
						select 
						  t.fingroupid
						, t.fingroupname
						, t.finsubgrpid
						, t.finsubgroupname
						, coalesce(sum(t.originalbalance),0) as originalbalance
						, coalesce(sum(t.finalamount),0)  as finalamount
						, coalesce(sum(t.PY1),0)  as PY1
						, concat(t.fingroupid,'.',t.finsubgrpid,'.',fsg.sequenceorder) as chldid
						, fsg.sequenceorder
						from tbloadeng t
						join finsubgroup fsg on t.fingroupid = fsg.fingroupid and t.finsubgrpid = fsg.finsubgroupid
						where t.fingroupid = 1
						group by t.fingroupid,t.finsubgrpid
						order by fsg.sequenceorder;
                        
				insert into tastcchild
                ( fingroupid
                , finsubgrpid
                , finsubgroupchildname
                , finalamount
                , PY1
                , chldid
                , sequenceorder
                )         
				select 
					t.fingroupid
				,   t.finsubgrpid
				, 	t.finsubgroupchildname
                , 	coalesce(sum(t.finalamount),0) as finalamount
                ,	coalesce(sum(t.PY1),0) as py1
                , 	concat(t.fingroupid,'.',t.finsubgrpid,'.',t.finsubgroupchildId,'.',fsg.sequenceorder) as chldid
                ,	fsg.sequenceorder
                from tbloadeng t
               join finsubgroup fsg on t.fingroupid = fsg.fingroupid  and t.finsubgrpid = fsg.finsubgroupid
                where t.fingroupid = 1  and t.finsubgrpid = 1
                group by t.fingroupid
				, 	t.finsubgroupchildname
                order by fsg.sequenceorder;
                        
                        

				if ( equpy1sum > 0 ) then
						begin 
						set assetsgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', case when coalesce(t.finalamount,0) < 0 then  concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
                                             					else format(t.finalamount,0) end
                                             , 'Prevgrouptotal' ,case when coalesce(t.PY1,0) < 0 then   concat('(',format(right(t.PY1,(length(t.PY1)-1)),0),')')
													else format(t.PY1,0) end
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupname
											, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')') 
													else format(ct.finalamount,0) end
                                            , 'Prevbal' ,case when coalesce(ct.PY1,0) < 0 then   concat('(',format(right(ct.PY1,(length(ct.PY1)-1)),0),')')
													else format(ct.PY1,0) end
											, 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	cth.fingroupid
											, 'chldid', cth.chldid
											, 'name', cth.finsubgroupchildname
											, 'Currbal' ,case when coalesce(cth.finalamount,0) < 0 then  concat('(',format(right(cth.finalamount,(length(cth.finalamount)-1)),0),')') 
													else format(cth.finalamount,0) end
                                              , 'Prevbal' ,case when coalesce(cth.PY1,0) < 0 then   concat('(',format(right(cth.PY1,(length(cth.PY1)-1)),0),')')
													else format(cth.PY1,0) end	
											)) 
											from tastcchild cth
                                            where cth.fingroupid = t.fingroupid
											and cth.finsubgrpid = ct.finsubgrpid
											order by ct.sequenceorder
                                            )
											)) 
											from tasstsbgrp ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from tasstgrp t);   
						end;
					else
						begin
						set assetsgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', case when coalesce(t.finalamount,0) < 0 then  concat('(',format(right(t.finalamount,(length(t.finalamount)-1)),0),')')
                                             					else format(t.finalamount,0) end
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupname
											, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')') 
													else format(ct.finalamount,0) end
											, 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	cth.fingroupid
											, 'chldid', cth.chldid
											, 'name', cth.finsubgroupchildname
											, 'Currbal' ,case when coalesce(cth.finalamount,0) < 0 then  concat('(',format(right(cth.finalamount,(length(cth.finalamount)-1)),0),')') 
													else format(cth.finalamount,0) end
											)) 
											from tastcchild cth
                                            where cth.fingroupid = t.fingroupid
											and cth.finsubgrpid = ct.finsubgrpid
											order by ct.sequenceorder
                                            )
											)) 
											from tasstsbgrp ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from tasstgrp t);     
						end;
					end if;

	--  assets  process end

	--  Equity + Libilaity  + unammed process start
				if ( equpy1sum > 0 ) then
						begin 
							-- equity + lib sum
							with tls as
							 (
								select 
								 4 as id
							   , 'Total liabilities and Equity' as nme
							   , coalesce(sum(finalamount),0)+coalesce((crtdeforg),0)as final
							   , coalesce(sum(PY1),0)+coalesce((prvdefprv),0)  as py1
							   from tbloadeng
								where fingroupid in (2,3)
							 )
							select JSON_ARRAYAGG(json_object 
							('id',	ct.id
							, 'name', ct.nme
							, 'Currbal' ,case when coalesce(ct.final,0) < 0 then  concat('(',format(right(ct.final,(length(ct.final)-1)),0),')')
                                             					else format(ct.final,0) end
							, 'Prevbal' ,case when coalesce(ct.PY1,0) < 0 then  concat('(',format(right(ct.PY1,(length(ct.PY1)-1)),0),')')
                                             					else format(ct.PY1,0) end
                            
							 ))
							 into lieqtotal
							from tls ct;
                            
                            -- unamapped
                            with untls as
								(
								select 5 as id
								, 'Unmapped' as nme
								, sum(finalamount) as finalamount
								, sum(PY1) as PY1
								from tbloadeng where fingroupid is null
								)
							select JSON_ARRAYAGG(json_object 
							('id',	ct.id
							, 'name', ct.nme
							, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')')
                                             					else format(ct.finalamount,0) end
							, 'Prevbal' ,case when coalesce(ct.PY1,0) < 0 then  concat('(',format(right(ct.PY1,(length(ct.PY1)-1)),0),')')
                                             else format(ct.PY1,0) end
							, 'children' ,
							(select JSON_ARRAYAGG(json_object 
							('id',	tg.tlid
							, 'name', tg.accountname
							, 'Currbal' ,case when coalesce(tg.finalamount,0) < 0 then  concat('(',format(right(tg.finalamount,(length(tg.finalamount)-1)),0),')')
                                             else format(tg.finalamount,0) end
                            
							, 'Prevbal' ,case when coalesce(tg.PY1,0) < 0 then  concat('(',format(right(tg.PY1,(length(tg.PY1)-1)),0),')')
                                             else format(tg.PY1,0) end
								))
							from unmdata tg 
							where tg.tlid = ct.id)
								))
                                into unmpjson
							from untls ct;
						end;
					else
						begin 
							with tls as
							 (
								select 
								 4 as id
							   , 'Total liabilities and Equity' as nme
							   , ( coalesce(sum(finalamount),0)+coalesce(crtdeforg,0) ) as final
							   from tbloadeng
								where fingroupid in (2,3)
							 )
							select JSON_ARRAYAGG(json_object 
							('id',	ct.id
							, 'name', ct.nme
							, 'Currbal' , case when coalesce(ct.final,0) < 0 then  concat('(',format(right(ct.final,(length(ct.final)-1)),0),')')
                                             					else format(ct.final,0) end                            
							 ))
							 into lieqtotal
							from tls ct;

                              -- unamapped
                            with untls as
								(
								select 5 as id
								, 'Unmapped' as nme
								, sum(finalamount) as finalamount
								, sum(PY1) as PY1
								from tbloadeng where fingroupid is null
								)
							select JSON_ARRAYAGG(json_object 
							('id',	ct.id
							, 'name', ct.nme
							, 'Currbal' ,case when coalesce(ct.finalamount,0) < 0 then  concat('(',format(right(ct.finalamount,(length(ct.finalamount)-1)),0),')')
                                             					else format(ct.finalamount,0) end
							, 'children' ,
							(select JSON_ARRAYAGG(json_object 
							('id',	tg.tlid
							, 'name', coalesce(tg.accountname,0)
							, 'Currbal' ,case when coalesce(tg.finalamount,0) < 0 then  concat('(',format(right(tg.finalamount,(length(tg.finalamount)-1)),0),')')
                                             else format(tg.finalamount,0) end
								))
							from unmdata tg 
							where tg.tlid = ct.id)
								)) into unmpjson
                              from untls ct;
                            
						end;
					end if;

  	--  Equity + Libilaity  process end                          
                            
                            
                            
if ( unmpcnt > 0 ) then
	begin
     select JSON_MERGE_PRESERVE(assetsgrp,libilgrp,equgrp,lieqtotal,unmpjson) as result;    
    end;
else
	begin
         select JSON_MERGE_PRESERVE(assetsgrp,libilgrp,equgrp,lieqtotal) as result;       
    end;
end if;
 
	
		end;


	end if;
END