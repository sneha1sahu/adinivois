﻿/********************************************************************************************************************************
** SP Name: getengagement
**
** Purpose: provide all Engagement type like "Notice To Reader. 
**		
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getengagement`()
BEGIN
select  engagementtypeid as engagementid, engagementtype from adinovis.engagementtype;
END