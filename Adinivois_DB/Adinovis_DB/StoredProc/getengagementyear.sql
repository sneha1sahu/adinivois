﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getengagementyear`(inengagementsid bigint)
BEGIN
	Declare nodate datetime;
    Declare checkdsrc int;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';
    
    set nodate = current_timestamp;
    
    set checkdsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementsid ) ,1,0));
		if ( checkdsrc = 1 ) then 
			begin
				select distinct 
							engagementsid
                        , 	acctyear
				from adinovis.trailbalance
				where isactive = 1
				and isdelete = 0
                and engagementsid = inengagementsid;            
            end;
		else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
                    -- select errorCode,errorMessage;
		end if;
		
END