﻿CREATE DEFINER=`admin`@`%` PROCEDURE `extractEngTBjsontotable`(IN inengagementsid bigint(20))
BEGIN
Declare vuseraccountid int(20);
Declare vclientapidatatid int(20);
Declare vengagementsid bigint(20);
Declare vrecieveddate date;
Declare vaccountcode varchar(1000);
Declare vaccountname varchar(1000);
Declare vaccounttype varchar(100);
Declare vaccountdesc varchar(100);
Declare vacctcredit varchar(200);
Declare vacctdebit varchar(200);
Declare vacctyear varchar(20);
Declare vdetailacctype varchar(20);
Declare vaccountbal varchar(200);
Declare vrecieveddata text;
Declare json_row_count int(20);
Declare json_row_count1 int(20);
Declare nodate date;
Declare vstatusid int(20);
declare loopstart int(20);
declare loopend int(20);
Declare i int(10);
Declare checkengid int;


DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';

        
DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;



drop TEMPORARY TABLE if exists tableloop;
drop TEMPORARY TABLE if exists testdata;

CREATE TEMPORARY TABLE tableloop
( tlid bigint NOT NULL AUTO_INCREMENT
, engagementsid bigint 
, recieveddate date
, accountcode varchar(100)
, accountname varchar(500) 
, accounttype varchar(100)
, accountdetaildesc varchar(2000)
, acctcredit varchar(20)
, acctdebit varchar(20)
, acctyear varchar(20)
, createdby varchar(20)
, modifiedby varchar(20)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE testdata
( tlid bigint NOT NULL AUTO_INCREMENT
, clientapidataid int(20)
, recieveddata mediumtext
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


SET nodate = date(current_timestamp);
SET vengagementsid = inengagementsid;

select useraccountid into vuseraccountid 
from adinovis.clientfirm cf 
join adinovis.engagements eg on cf.clientfirmid = eg.clientfirmid
and eg.engagementsid = inengagementsid
limit 1;

select statusid into vstatusid from adinovis.engagements where engagementsid = inengagementsid;

if  vstatusid = 9 then 

insert into testdata(clientapidataid,recieveddata)
select clientapidataid,recieveddata from adinovis.clientapidata where engagementsid = inengagementsid order by clientapidataid desc limit 3;

set loopstart = (select min(tlid) from testdata);
set loopend = (select max(tlid) from testdata);

While loopend >= loopstart do

select recieveddata into vrecieveddata from testdata where tlid = loopstart;
select year(cast(replace(json_extract(vrecieveddata,'$.header.endPeriod'), '"',"")as date))  into vacctyear;

select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count;


select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count1;

if json_row_count1 is null then

set json_row_count1 = 0;
				
                update adinovis.engagements
                set tbloadstatusid = 14 
                where engagementsid = inengagementsid;
                
                else 
                
				update adinovis.engagements
                set tbloadstatusid = 12 
                where engagementsid = inengagementsid;
                
           

end if;

SET i = 0;

while json_row_count - 1 > 0
do 
select 
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].id')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[1].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[2].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[3].value'))  INTO
 vaccountname,vaccountcode,vacctdebit, vacctcredit,vaccountdesc;
 
      if vacctcredit = '""' then  set vacctcredit = '0.00'; end if;
      if vacctdebit = '""' then   set vacctdebit = '0.00';  end if;


insert into tableloop( 
engagementsid,
recieveddate,
accountcode,
accountname,
accounttype,
accountdetaildesc,
acctcredit,
acctdebit,
acctyear,
createdby,
modifiedby)
values 
(vengagementsid,
nodate,
trim(replace(vaccountcode,'"',' ')),
trim(replace(vaccountname,'"',' ')),
trim(vaccounttype), 
trim(vdetailacctype),
trim(replace(vacctcredit,'"',' ')), 
trim(replace(vacctdebit,'"',' ')) ,
substr((replace(vacctyear,'"',' ')),1,4),
/*year(trim(replace(vacctyear,'"',' '))),*/
cast(vuseraccountid as char), 
cast(vuseraccountid as char)
);

SET i = i + 1;
set json_row_count = json_row_count - 1;

end while;

set loopstart = loopstart + 1 ;

end while;

else

select clientapidataid,recieveddata into vclientapidatatid,vrecieveddata from adinovis.clientapidata where engagementsid = inengagementsid 
and acctyear = (select max(acctyear) from adinovis.clientapidata where engagementsid = inengagementsid)
order by clientapidataid desc limit 1;

/*
select engagementsid into vengagementsid from adinovis.clientapidata where clientapidataid = inclientapidataid;*/

select year(cast(replace(json_extract(vrecieveddata,'$.header.endPeriod'), '"',"")as date))  into vacctyear;

select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count;


select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count1;

if json_row_count1 is null then

set json_row_count1 = 0;
				
                update adinovis.engagements
                set tbloadstatusid = 14 
                where engagementsid = inengagementsid;
                
                else 
                
				update adinovis.engagements
                set tbloadstatusid = 12 
                where engagementsid = inengagementsid;
                
           

end if;

SET i = 0;

while json_row_count - 1 > 0
do 
select 
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].id')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[1].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[2].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[3].value'))  INTO
 vaccountname,vaccountcode,vacctdebit, vacctcredit,vaccountdesc;
 
      if vacctcredit = '""' then  set vacctcredit = '0.00'; end if;
      if vacctdebit = '""' then   set vacctdebit = '0.00';  end if;


insert into tableloop( 
engagementsid,
recieveddate,
accountcode,
accountname,
accounttype,
accountdetaildesc,
acctcredit,
acctdebit,
acctyear,
createdby,
modifiedby)
values 
(vengagementsid,
nodate,
trim(replace(vaccountcode,'"',' ')),
trim(replace(vaccountname,'"',' ')),
trim(vaccounttype), 
trim(vdetailacctype),
trim(replace(vacctcredit,'"',' ')), 
trim(replace(vacctdebit,'"',' ')) ,
substr((replace(vacctyear,'"',' ')),1,4),
/*year(trim(replace(vacctyear,'"',' '))),*/
cast(vuseraccountid as char), 
cast(vuseraccountid as char)
);

SET i = i + 1;
set json_row_count = json_row_count - 1;

end while;

end if;


UPDATE adinovis.tableloop 
SET accountcode = 0
where engagementsid = vengagementsid
and trim(left (accountname, locate(' ',accountname)-1))  REGEXP '[a-z]';


UPDATE adinovis.tableloop  
SET accountcode = trim(left (accountname, locate(' ',accountname)-1))
where engagementsid = vengagementsid
and trim(left (accountname, locate(' ',accountname)-1))  NOT REGEXP '[a-z]';

UPDATE adinovis.tableloop 
SET accountname = trim(RIGHT (accountname, LENGTH(accountname)-locate(' ',accountname)))
where engagementsid = vengagementsid
and trim(left (accountname, locate(' ',accountname)-1))  NOT REGEXP '[a-z]';


  
  
  set checkengid = (select if (exists ( select 1 from adinovis.trailbalance where engagementsid = vengagementsid ),1,0));
    if ( checkengid = 1 ) then
		begin
        
			update adinovis.trailbalancedailyload set TBshowonUI = b'0';
			insert into adinovis.trailbalancedailyload
				( engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , createdby
                , createddate
                , modifiedby
                , modifieddate
                , TBshowonUI
                )
			select  engagementsid
				,	recieveddate
				,	accountcode
				,	accountname
				,	accounttype
				,	acctcredit
				,	acctdebit
				,	acctyear
				,	createdby
                ,	nodate
				,	modifiedby
                ,	nodate
                ,   b'1'
			from tableloop;        
        end;
        
        call adinovis.updatetrailbalancedailyload(inengagementsid);
	else
		begin
			insert into adinovis.trailbalance
				( engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , createdby
                , createddate
                , modifiedby
                , modifieddate
                )
			select  engagementsid
				,	recieveddate
				,	accountcode
				,	accountname
				,	accounttype
				,	acctcredit
				,	acctdebit
				,	acctyear
				,	createdby
                ,	nodate
				,	modifiedby
                ,	nodate
			from tableloop;  
            
            insert into adinovis.engagementsourceload
				(engagementsid
                , clientapidataid
                , countrows
                , loaddate
                , createdby
                , createddate
                , modifiedby
                , modifieddate)
                values (inengagementsid,
						vclientapidatatid,
                        json_row_count1,
                        nodate,
                        cast(vuseraccountid as char),
                        nodate,
                        cast(vuseraccountid as char),
                        nodate);
				
            call adinovis.trailbalanceprevious(inengagementsid);   
                        
                
		end;
	end if;
    
		update adinovis.engagements set statusid = 7 
        where engagementsid = inengagementsid;
				
  
  /*select * from tableloop;*/
  
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
values(errorCode, errorMessage,'adinovis.extractEngTBjsontotable',nodate); 

				update adinovis.engagements
                set tbloadstatusid = 13 
                where engagementsid = inengagementsid;

end if;
END;



COMMIT;
set  autocommit=1;	


END