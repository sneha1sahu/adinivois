﻿/********************************************************************************************************************************
** SP Name: getclientdata
**
** Purpose: provide list of client Quick Books and Xero data. 
**		
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 16-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getclientdata`()
BEGIN
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%quick%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%xero%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5
;

END