﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savetrailbaldailyloadauditorstatus`(In intrailbalancedailyloadid int(20),IN inauditorstatusid INT(20),in ploginid int(5))
BEGIN
Declare vauditorstatus int(20);
Declare vtrailbalancedailyloadid int(20);
Declare vacctcredit decimal(20,2);
Declare vacctdebit decimal(20,2);
Declare vtrailbalanceid bigint(20);
Declare visdifferwithexisting bit(1);
Declare visnewrecord bit(1);
Declare nodate datetime;

set nodate = current_timestamp();

select 
 trailbalancedailyloadid
,acctcredit
,acctdebit
,trailbalanceid
,isdifferwithexisting
,isnewrecord
into   vtrailbalancedailyloadid
,vacctcredit
,vacctdebit
,vtrailbalanceid
,visdifferwithexisting
,visnewrecord 
from adinovis.trailbalancedailyload
where trailbalancedailyloadid = intrailbalancedailyloadid;


-- Accept the trail balance daily load status
if inauditorstatusid = 5  then
	begin
		-- modify existing trail balance credit/debit value from daily load
		if (visdifferwithexisting = 1 ) then
			begin
              -- moving the existing record to history table before modification
				insert into adinovis.trailbalance_history (
						trailbalanceid
						, engagementsid
						, clientdatasourceid
						, clientapidataid
						, recieveddate
						, accountcode
						, accountname
						, accounttype
						, acctcredit
						, acctdebit
						, acctyear
						, isclientdatasource
						, isactive
						, isdelete
						, createdby
						, createddate
						, modifiedby
						, modifieddate
						, PY1
						, PY2
						, parenttbid,
						 historyloaddate
						)
						select  
						 trailbalanceid
						, engagementsid
						, clientdatasourceid
						, clientapidataid
						, recieveddate
						, accountcode
						, accountname
						, accounttype
						, acctcredit
						, acctdebit
						, acctyear
						, isclientdatasource
						, isactive
						, isdelete
						, createdby
						, createddate
						, modifiedby
						, modifieddate
						, PY1
						, PY2
						, parenttbid
						, nodate
						 from  adinovis.trailbalance
						where trailbalanceid = vtrailbalanceid;
					
                    -- updating the trail balance
					update adinovis.trailbalance
						set acctcredit =  vacctcredit
                        , acctdebit = vacctdebit
						, modifiedby = cast(ploginid as char(50))
						, modifieddate = nodate
					where trailbalanceid = vtrailbalanceid;   
            end;            
		end if;


	if ( visnewrecord = 1 ) then
		begin
			insert into adinovis.trailbalance
				( engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , isclientdatasource
                , isactive
                , isdelete
                , createdby
                , createddate
                , modifiedby
                , modifieddate
				)
			select 
				engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , 1
                , isactive
                , isdelete
                , cast(ploginid as char(50))
                , nodate
                , cast(ploginid as char(50))
                , nodate
			from trailbalancedailyload
            where trailbalancedailyloadid = intrailbalancedailyloadid
            and isnewrecord = visnewrecord;
        end;
	end if;
end;
end if;

					-- updating trail balance daily load status
                        update adinovis.trailbalancedailyload
						set auditorstatus = inauditorstatusid
                        , modifiedby = cast(ploginid as char(50))
						, modifieddate = nodate
                        where trailbalancedailyloadid = intrailbalancedailyloadid;



END