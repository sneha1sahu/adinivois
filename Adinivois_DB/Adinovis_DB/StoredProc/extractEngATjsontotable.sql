﻿CREATE DEFINER=`admin`@`%` PROCEDURE `extractEngATjsontotable`(IN inengagementsid int(20))
BEGIN
Declare vuseraccountid int(20);
Declare vengagementsid bigint(20);
Declare vaccount varchar(255);
Declare vtype varchar(255);
Declare vdetailtype varchar(255);
Declare vdescription varchar(255);
Declare vbalance varchar(200);
Declare vrecieveddata longtext;
Declare json_row_count int(20);
Declare nodate date;
Declare i int(10);
Declare checkengid int;
Declare vunknown varchar(200);


DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';

        
DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;



drop TEMPORARY TABLE if exists tableloop;

CREATE TEMPORARY TABLE tableloop
( tlid bigint NOT NULL AUTO_INCREMENT
, engagementsid bigint 
, account varchar(255)
, type varchar(255)
, detailtype varchar(255)
, description varchar(255)
, balance varchar(200)
, createdby varchar(20)
, modifiedby varchar(20)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


SET nodate = date(current_timestamp);

select accountlistdata, engagementsid into vrecieveddata, vengagementsid from adinovis.clientapidata where engagementsid = inengagementsid order by clientapidataid desc limit 1;


select useraccountid into vuseraccountid 
from adinovis.clientfirm cf 
join adinovis.engagements eg on cf.clientfirmid = eg.clientfirmid
join adinovis.clientapidata cd on eg.engagementsid = cd.engagementsid
and cd.engagementsid = inengagementsid
limit 1;

-- select json_extract(vrecieveddata,'$.header.endPeriod')  into vacctyear;
    
select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count;

SET i = 0;

while json_row_count > 0
do 
select 
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[1].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[2].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[3].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[4].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[5].value'))  INTO
 vaccount,vtype,vdetailtype,vdescription,vbalance;
 
      if vbalance = '""' then  set vbalance = '0.00'; end if;


insert into tableloop( 
engagementsid,
account,
type,
detailtype,
description,
balance,
createdby,
modifiedby)
values 
(vengagementsid,
trim(replace(vaccount,'"',' ')),
trim(replace(vtype,'"',' ')),
trim(replace(vdetailtype,'"',' ')), 
trim(replace(vdescription,'"',' ')), 
trim(replace(vbalance,'"',' ')) ,
cast(vuseraccountid as char), 
cast(vuseraccountid as char)
);

SET i = i + 1;
set json_row_count = json_row_count - 1;

end while;
  select 1;
 
  
-- select * from tableloop;
insert into adinovis.accountlistdata(
						engagementsid
						,account
						,type
						,detailtype
						,description
						,balance
						,createdby
						,createddate
						,modifiedby
						,modifieddate)                       
					select tl.engagementsid
						,tl.account
						,tl.type
						,tl.detailtype
						,tl.description
						,tl.balance
						,tl.createdby
						,nodate
						,tl.modifiedby
						,nodate 
                        from tableloop tl
                        left join adinovis.accountlistdata acd on
	tl.engagementsid = acd.engagementsid and tl.account = acd.account
and tl.type = acd.type and tl.detailtype = acd.detailtype and tl.description = acd.description
where (accountlistdataid is null);
					

  
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
values(errorCode, errorMessage,'adinovis.extractEngATjsontotable',nodate); 
end if;
END;

COMMIT;
set  autocommit=1;	


END