﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savetrailbalance`(IN Pclientfirmid int(20))
BEGIN
Declare vengagementsid bigint(20);
Declare vuseraccountid int(20);
Declare vclientdatasourceid bigint(20);
Declare vclientapidataid bigint(20);
Declare vrecieveddate date;
Declare vaccountcode varchar(1000);
Declare vaccountname varchar(1000);
Declare vaccounttype varchar(100);
Declare vacctcredit varchar(200);
Declare vacctdebit varchar(200);
Declare vacctyear varchar(20);
Declare vcreatedby varchar(20);
Declare vmodifiedby varchar(20);
Declare vrecieveddata text;
Declare json_row_count int(20);
Declare nodate date;
Declare i int(10);
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';

        
DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;



drop TEMPORARY TABLE if exists tableloop;

CREATE TEMPORARY TABLE tableloop
( tlid bigint NOT NULL AUTO_INCREMENT
, engagementsid bigint 
, clientdatasourceid bigint 
, clientapidataid bigint 
, recieveddate date
, accountcode varchar(100)
, accountname varchar(500) 
, accounttype varchar(100)
, acctcredit varchar(20)
, acctdebit varchar(20)
, acctyear date
, createdby varchar(20)
, modifiedby varchar(20)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


SELECT clientdatasourceid, engagementsid into vclientdatasourceid, vengagementsid from adinovis.clientdatasource 
where clientfirmid = Pclientfirmid; -- (234)


select clientapidataid, recieveddata into vclientapidataid,vrecieveddata from
adinovis.clientapidata where clientdatasourceid = vclientdatasourceid
and modifieddate = (select max(modifieddate) from adinovis.clientapidata where clientdatasourceid = vclientdatasourceid);

select vrecieveddata;

SELECT useraccountid into vuseraccountid FROM adinovis.clientfirm where clientfirmid = Pclientfirmid ;

    select json_extract(vrecieveddata,'$.header.endPeriod')  into vacctyear;
-- select year(replace(vacctyear,'"',' '));
    
SET nodate = date(current_timestamp);
    select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count;
SET i = 0;
 select vrecieveddata;
while json_row_count - 1 > 0
do 
-- select i;
select json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].id')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[1].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[2].value'))
  INTO
vaccountname, vaccountcode, vacctdebit,vacctcredit;
 -- select vacctcredit;
      if vacctcredit = '""' then 
      set vacctcredit = '0.00';
      end if;
       if vacctdebit = '""' then 
      set vacctdebit = '0.00';
      end if;
insert into tableloop( -- adinovis.trailbalance_test1 (
engagementsid,
clientdatasourceid,
clientapidataid,
recieveddate,
accountcode,
accountname,
accounttype,
acctcredit,
acctdebit,
acctyear,
createdby,
modifiedby)
values (vengagementsid,
vclientdatasourceid,
vclientapidataid,
nodate,
replace(vaccountcode,'"',' '),
replace(vaccountname,'"',' '),
vaccounttype, 
replace(vacctcredit,'"',' '), 
replace(vacctdebit,'"',' ') ,
(replace(vacctyear,'"',' ')),
cast(vuseraccountid as char), 
cast(vuseraccountid as char));

/*select * from tableloop;*/
    
SET i = i + 1;
set json_row_count = json_row_count - 1;

select json_row_count;
end while;
    
  select * from tableloop;
  insert into adinovis.trailbalance_test1 (
engagementsid,
clientdatasourceid,
clientapidataid,
recieveddate,
accountcode,
accountname,
accounttype,
acctcredit,
acctdebit,
acctyear,
createdby,
modifiedby)
    select engagementsid,
clientdatasourceid,
clientapidataid,
recieveddate,
accountcode,
accountname,
    accounttype,
    cast(acctcredit as decimal(10,2)),
    cast(acctdebit as decimal(10,2)),
    year( cast(acctyear as date)),
    createdby,
    modifiedby from tableloop;
  
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
values(errorCode, errorMessage,'adinovis.savetb_test',nodate); 
end if;
END;

COMMIT;
set  autocommit=1;	
begin
IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END