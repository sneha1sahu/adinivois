﻿/********************************************************************************************************************************
** SP Name: getbusinessdetails
**
** Purpose: provide list of Business Details. 
**		
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 16-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getbusinessdetails`(Ploginid int(20))
BEGIN
select  distinct 
businessname,
clientfirmid ,
incorporationdate,
useraccountid,
typeofentityid
from adinovis.clientprofile cf
where userdelete = 0 and clientfirmdelete = 0 
and statusid in (2,5)
and usercreatedby = Ploginid
union
select  distinct 
cf.businessname,
cf.clientfirmid ,
cf.incorporationdate,
cf.useraccountid,
cf.typeofentityid
from adinovis.clientprofile cf
join adinovis.engagements eg on cf.clientfirmid = eg.clientfirmid and eg.isactive = true and eg.isdelete  = false
join adinovis.clientfirmauditorrole cfr on eg.engagementsid = cfr.engagementsid and cfr.isactive = true and cfr.isdelete  = false
where cf.userdelete = 0 and cf.clientfirmdelete = 0 
and cf.statusid in (2,5)
and cfr.useraccountid  = Ploginid and cf.usercreatedby != Ploginid
order by businessname;

END