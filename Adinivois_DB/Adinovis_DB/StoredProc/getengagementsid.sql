﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getengagementsid`(Pclientfirmid int(20))
BEGIN
select engagementsid from adinovis.engagements 
where clientfirmid = Pclientfirmid 
order by engagementsid desc 
limit 1;
END