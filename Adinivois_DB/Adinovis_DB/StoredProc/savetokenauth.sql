﻿/********************************************************************************************************************************
** SP Name: savetokenauth
**
** Purpose: Save user token key in DB.
**
**Parameters: 	Puseraccountid
**				Ptokenvalue
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 24-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `savetokenauth`(IN Puseraccountid INT(20), IN Ptokenvalue varchar(255) )
BEGIN	

/* Variable Declaration*/
Declare vuseraccountid INT(20);
Declare vtokenvalue varchar(255);
Declare nodate datetime;
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variables value*/
set nodate = current_timestamp;
set vuseraccountid = Puseraccountid;
set vtokenvalue = Ptokenvalue;
set  autocommit=0;

/*Save user token key in DB.*/				
					INSERT INTO adinovis.useraccountauth(
								useraccountid, 
								tokentypeid,
								tokenvalue,
								createdby,
								createddate,
								Modifiedby,
								Modifieddate)
						VALUES (vuseraccountid,
								1,
								vtokenvalue,
								CAST(vuseraccountid AS CHAR),
								nodate,
								CAST(vuseraccountid AS CHAR),
								nodate);


	BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.savetokenauth',current_timestamp);
	end if;
    END;    
COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
	
END