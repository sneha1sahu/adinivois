﻿CREATE DEFINER=`admin`@`%` PROCEDURE `loaqbtrailbalancemappingupdate`( inengagementsid bigint)
BEGIN
	Declare loopstart INT(20);
	Declare loopend INT(20);
	Declare searchindex varchar(4000);
    Declare acttypesearch varchar(4000);
    Declare gfcode varchar(40);
	Declare mpshid bigint;
    Declare gpid bigint;
    Declare tblid bigint;
	Declare bttype INT(20);
	Declare nodate datetime;
    Declare mpshetidnull int;
    

 drop TEMPORARY TABLE if exists trailmpp;
  drop TEMPORARY TABLE if exists updtrailmpp;

	CREATE TEMPORARY TABLE trailmpp
	( tlid bigint NOT NULL AUTO_INCREMENT
	,  trailbalid bigint null
	, accountname varchar(4000) null
	, accounttype varchar(4000) null
    , fggrpid int 	null
	, baltypeid int null
	, mapshetid bigint null
    , gificode varchar(50) null
	, PRIMARY KEY (`tlid`)
	)AUTO_INCREMENT=0 ;
    
    
	CREATE TEMPORARY TABLE updtrailmpp
	(  trailbalid bigint null
	, mapshetid bigint null
    , gificode varchar(50) null
	);



insert into trailmpp
(trailbalid, accountname, accounttype, baltypeid,fggrpid)
SELECT distinct
  vt.trailbalanceid
, case when rtrim(ltrim(vt.accountname)) like '%visa%'  or rtrim(ltrim(vt.accountname)) like '%master%'   
				or rtrim(ltrim(vt.accountname)) like '%Amex%' then 'Interest and bank charges'
			when rtrim(ltrim(vt.accountname)) like 'Checking%' or rtrim(ltrim(vt.accountname)) like '%Chequing%'  
				and  vt.originalbalance > 0 then 'bank' 
			when rtrim(ltrim(vt.accountname)) like 'Checking%' or rtrim(ltrim(vt.accountname)) like '%Chequing%'   
				and  vt.originalbalance < 0 then 'Bank overdraft'  
			when rtrim(ltrim(vt.accountname)) like '%Prepaid expenses%' then 'Prepaid expenses'     
			when rtrim(ltrim(vt.accountname)) like '%Hardware%' or rtrim(ltrim(vt.accountname)) like '%Software%' 
				or rtrim(ltrim(vt.accountname)) like '%SW Support%' then 'Computer-related expenses'     
			when rtrim(ltrim(vt.accountname)) like '%Shareholder Advances%' then 'Loans/advances due from related parties'  
			when rtrim(ltrim(vt.accountname)) like '%Accumulated Depreciation%' then 'amounts referred to as depreciation' 
		else rtrim(ltrim(vt.accountname)) end  as accountname
, COALESCE(vt.accounttype, ad.type) as accounttype
, case when vt.acctcredit > 0 then 1 else 2 end  as balancetype
, case when ad.fingroupid is null then case when originalbalance > 0 and  rtrim(ltrim(vt.accountname)) like 'Checking%' then 2 
	   when originalbalance < 0 and  rtrim(ltrim(vt.accountname)) like 'Checking%' then 1 
		else ad.fingroupid end 
  else ad.fingroupid end as fingroupid
FROM adinovis.vtrailbal vt
left  join accountlistdata ad on rtrim(ltrim(vt.accountname)) = rtrim(ltrim(ad.account)) and ad.engagementsid = vt.engagementsid
where  vt.engagementsid = inengagementsid;


 set nodate = current_timestamp;
 

  -- update  trail balance mapping
	select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
 
	while loopstart <= loopend
		do
				begin
					select  trailbalid into tblid from trailmpp where tlid = loopstart;
						select 
							m.mapsheetid into mpshid
						 from trailmpp t 
						 join fingroup fg on t.fggrpid = fg.fingroupid and fg.isactive = 1 and  fg.isdelete = 0
						 join adinovis.finsubgroup fsg on fg.fingroupid = fsg.fingroupid and fsg.isactive = 1 and  fsg.isdelete = 0
						 join adinovis.finsubgroupchild fgc on fsg.finsubgroupid = fgc.finsubgroupid and fgc.isactive = 1 and  fgc.isdelete = 0
						 join mapsheet m on fgc.finsubgroupchildId = m.finsubgroupchildId and m.isactive = 1 and  m.isdelete = 0
						 and t.accountname = m.mapsheetname	
                         where t.trailbalid = tblid
                         limit 1;
	
					update trailmpp
					set mapshetid = mpshid
                    where trailbalid = tblid;
                    
                    set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set tblid = null;
				end;

		set loopstart = loopstart + 1;
	end while; 
 

 

					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null; 
                    set gpid = null;
 

 -- mapping trail balance with map sheet
	select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
 
	while loopstart <= loopend
		do
				begin
					set mpshetidnull = (SELECT IF ( EXISTS ( SELECT 1 FROM trailmpp where tlid = loopstart and  mapshetid is null) ,1,0));
						if (mpshetidnull = 1) then
							begin           
								select accountname into searchindex from trailmpp where tlid = loopstart;
								select  baltypeid into bttype from trailmpp where tlid = loopstart;			
								select accounttype into acttypesearch from trailmpp where tlid = loopstart;
								select fggrpid into gpid from trailmpp where tlid = loopstart;
                                
								set searchindex = rtrim(ltrim(searchindex));   
                                
             
                             
								select mt.mapsheetid into mpshid
								from adinovis.mapsheet mt 
                                join adinovis.finsubgroupchild fsc on mt.finsubgroupchildId = fsc.finsubgroupchildId and fsc.isactive = 1 and fsc.isdelete = 0
                                join adinovis.finsubgroup fsg on fsc.finsubgroupid = fsg.finsubgroupid and fsg.isactive = 1 and fsg.isdelete = 0
								where mt.leadsheetid is not null
								and mt.finsubgroupchildId is not null
								and mt.balancetypeid is not null
								and mt.statementtypeid is not null
                                and mapsheetname not like 'Total%'
								and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
								and fsg.fingroupid = gpid 
								and mt.balancetypeid = bttype
								limit 1;

					
								update trailmpp
								set mapshetid = mpshid
								where tlid = loopstart;
                    
								end;
						end if;                    
                    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
				end;

		set loopstart = loopstart + 1;
	end while;
    

-- with out bal type    
				set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null; 
                    set gpid = null;
 

 -- mapping trail balance with map sheet
	select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
 
	while loopstart <= loopend
		do
				begin
					set mpshetidnull = (SELECT IF ( EXISTS ( SELECT 1 FROM trailmpp where tlid = loopstart and  mapshetid is null) ,1,0));
						if (mpshetidnull = 1) then
							begin           
								select accountname into searchindex from trailmpp where tlid = loopstart;
								select  baltypeid into bttype from trailmpp where tlid = loopstart;			
								select accounttype into acttypesearch from trailmpp where tlid = loopstart;
								select fggrpid into gpid from trailmpp where tlid = loopstart;
                                
								set searchindex = rtrim(ltrim(searchindex));   
                                
             
                             
								select mt.mapsheetid into mpshid
								from adinovis.mapsheet mt 
                                join adinovis.finsubgroupchild fsc on mt.finsubgroupchildId = fsc.finsubgroupchildId and fsc.isactive = 1 and fsc.isdelete = 0
                                join adinovis.finsubgroup fsg on fsc.finsubgroupid = fsg.finsubgroupid and fsg.isactive = 1 and fsg.isdelete = 0
								where mt.leadsheetid is not null
								and mt.finsubgroupchildId is not null
								and mt.balancetypeid is not null
								and mt.statementtypeid is not null
                                and mapsheetname not like 'Total%'
								and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
								and fsg.fingroupid = gpid 
								-- and mt.balancetypeid = bttype
								limit 1;

					
								update trailmpp
								set mapshetid = mpshid
								where tlid = loopstart;
                    
								end;
						end if;                    
                    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
				end;

		set loopstart = loopstart + 1;
	end while;
    



   -- mapping trail balance w.r.t gifi code which are not matched in above process
    select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
 
	while loopstart <= loopend
		do
			if (select 1 from trailmpp where tlid = loopstart and mapshetid is  null) then        
				begin
					select accountname into searchindex from trailmpp where tlid = loopstart;
					select  baltypeid into bttype from trailmpp where tlid = loopstart;			
					select accounttype into acttypesearch from trailmpp where tlid = loopstart;
				  
					set searchindex = rtrim(ltrim(searchindex));   
                    
                    if ( acttypesearch is null ) then
						select gificode into gfcode from adinovis.gificode
						where match(gifidescription) against(searchindex IN NATURAL LANGUAGE MODE)
						limit 1;                        
					else
						select gificode into gfcode from adinovis.gificode
						where match(gifidescription) against(searchindex IN NATURAL LANGUAGE MODE)
						and match(fingroup) against(acttypesearch IN NATURAL LANGUAGE MODE)
						limit 1;
					end if;
						
                   
					select mt.mapsheetid into mpshid
					from adinovis.mapsheet mt 
					where mt.leadsheetid is not null
					and mt.finsubgroupchildId is not null
					and mt.balancetypeid is not null
					and mt.statementtypeid is not null
                    and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
					and mt.gificode = gfcode
					and mt.balancetypeid = bttype
                    and mt.parentID > 0
                    order by mt.mapno
					limit 1;

										
                    if ( mpshid is null  or mpshid = '') then
						begin
							set mpshid = 9999999;
						end;
					end if;
                    
                    
			-- removing paretnt id > 0
					if ( mpshid = 9999999 ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
							and mt.gificode = gfcode
							and mt.balancetypeid = bttype
                            order by mt.mapno
							limit 1;
						end;
					end if;
                    
                    
          -- removing paretnt id > 0 and balance type (credit or debit)
					if ( mpshid = 9999999 ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
							and mt.gificode = gfcode
                            order by mt.mapno
							limit 1;
						end;
					end if;
                    
                    
       -- removing paretnt id > 0 and balance type (credit or debit) and gifcode
					if ( mpshid  = 9999999 ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
                            order by mt.mapno
							limit 1;
						end;
					end if;
                    
            -- matching with gifi code
					if ( mpshid = 9999999 ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and mt.gificode = gfcode
                            order by mt.mapno
							limit 1;
						end;
					end if;

                 if ( mpshid != 9999999 ) then
					begin
						update trailmpp
						set  gificode = gfcode
						, mapshetid =  mpshid  
						where tlid = loopstart;
					end;
				end if;
                    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
				end;
			end if;

		set loopstart = loopstart + 1;
	end while; 


 -- update  trail balance mapping
	select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
 
	while loopstart <= loopend
		do
				begin
					select  trailbalid into tblid from trailmpp where tlid = loopstart;
					select  mapshetid into mpshid from trailmpp where tlid = loopstart;			
	
					update adinovis.trailbalancemaping
					set mapsheetid = mpshid
                    where trailbalanceid = tblid;
                    
					set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
                    set tblid = null;
				end;

		set loopstart = loopstart + 1;
	end while; 
    
    
  -- inserting the mapping details to main table 
 insert into adinovis.trailbalancemaping
	( trailbalanceid
    , mapsheetid
    , gificode
    , createdby
    , createddate
    , modifiedby
    , modifieddate
    )
select 
		tm.trailbalid
	,	tm.mapshetid
    ,	tm.gificode
    ,	1
    ,	nodate
    ,	1
	,	nodate
from trailmpp tm
where tm.trailbalid not in ( select trailbalanceid from adinovis.trailbalancemaping where isactive = 1 and isdelete = 0)
and tm.mapshetid is not null; 

END