﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getsourcelink`(inengagementsid int(20))
BEGIN

Declare  vengagementstatus int(20);



select statusid into vengagementstatus from adinovis.engagements where engagementsid = inengagementsid;

if vengagementstatus = 9 then
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%quick%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 and egs.engagementsid = inengagementsid
where c.userdelete = 0 and c.statusid = 5
and year(c.incorporationdate) <= year(egs.finanicalyearenddate)
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) ,interval -365 day) as startdate,
date_add(egs.finanicalyearenddate, interval -365 day)  as enddate,
year(egs.finanicalyearenddate) - 1 as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%quick%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 and egs.engagementsid = inengagementsid
where c.userdelete = 0 and c.statusid = 5
and year(c.incorporationdate) <= year(egs.finanicalyearenddate) - 1
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) ,interval -730 day) as startdate,
date_add(egs.finanicalyearenddate, interval -730 day)  as enddate,
year(egs.finanicalyearenddate) - 2 as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%quick%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 and egs.engagementsid = inengagementsid
where c.userdelete = 0 and c.statusid = 5
and year(c.incorporationdate) <= year(egs.finanicalyearenddate) - 2
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
 date_add(finanicalyearenddate,interval -730 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%xero%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5 and egs.engagementsid = inengagementsid
and year(c.incorporationdate) <= year(egs.finanicalyearenddate)
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%xero%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5 and egs.engagementsid = inengagementsid
and year(c.incorporationdate) <= year(egs.finanicalyearenddate) - 1
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%xero%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5 and egs.engagementsid = inengagementsid
and year(c.incorporationdate) <= year(egs.finanicalyearenddate) - 2
;
else
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%quick%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 and egs.engagementsid = inengagementsid
where c.userdelete = 0 and c.statusid = 5
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
cd.accesstoken,
date_add(DATE_SUB(egs.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
egs.finanicalyearenddate  as enddate,
year(egs.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken,
egs.engagementsid
from adinovis.clientprofile c
join adinovis.clientdatasource cd on c.clientfirmid = cd.clientfirmid and( cd.sourcelink like '%xero%' ) and cd.isactive = 1 and cd.isdelete = 0
join adinovis.engagements egs on c.clientfirmid = egs.clientfirmid and egs.isactive = 1 and egs.isdelete = 0 
where c.userdelete = 0 and c.statusid = 5 and egs.engagementsid = inengagementsid
;
end if; 

END