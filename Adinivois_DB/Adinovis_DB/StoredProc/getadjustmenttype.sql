﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getadjustmenttype`()
BEGIN

select 
		adjustmenttypeid
	 , 	adjustmenttypename
from adinovis.adjustmenttype
where isactive = 1
and isdelete = 0;

END