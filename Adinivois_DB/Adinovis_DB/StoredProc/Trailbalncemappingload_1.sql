﻿CREATE DEFINER=`admin`@`%` PROCEDURE `Trailbalncemappingload`(inengagementsid bigint)
BEGIN
BEGIN
	Declare loopstart bigint(20);
	Declare loopend bigint(20);
	Declare vaccountname varchar(1000);
	Declare vmapsheetid bigint(20);
	Declare vfinsubgroupchildId INT(20);
	Declare vparentid varchar(100);
	Declare vmapsheetname varchar(1000);
	Declare vmapno varchar(100);
    Declare vtrailbalanceid bigint;
    Declare nodate date;
    Declare vbaltypeid  bigint;
    
SET nodate = current_timestamp;
 drop TEMPORARY TABLE if exists tableloop;

	CREATE TEMPORARY TABLE tableloop
	(  tlid bigint NOT NULL AUTO_INCREMENT
	,  trailbalid bigint null
	, accountname varchar(4000) null
	, accounttype varchar(4000) null
	, baltypeid int null
	, mapshetid bigint null
     , PRIMARY KEY (tlid)
	)AUTO_INCREMENT=0 ;


		insert into tableloop
		(trailbalid, accountname, accounttype, baltypeid)
		SELECT trailbalanceid
		, accountname
		, accounttype
		, case when acctcredit > 0 then 1 else 2 end  as balancetype
		FROM adinovis.trailbalance_test1
        where  engagementsid = inengagementsid
        order by accountname;
        

select min(tlid) into loopstart  from tableloop;
select max(tlid) into loopend  from tableloop;

	while loopstart <= loopend
		do
			select trailbalid,accountname,baltypeid into vtrailbalanceid,vaccountname,vbaltypeid from tableloop where tlid = loopstart;
		
			
			select A.mapsheetid,A.finsubgroupchildId, A.parentid, A.mapsheetname, A.mapno 
			into vmapsheetid,vfinsubgroupchildId,vparentid,vmapsheetname,vmapno
			FROM adinovis.mapsheet AS A
			where A.parentID != 0 
			and MATCH (a.mapsheetname)
            AGAINST (vaccountname IN Boolean MODE) 
                        limit 1;
        select vmapsheetid,vparentid,vmapsheetname,vmapno;

	       
       INSERT INTO adinovis.trailbalancemaping_test
(
trailbalanceid,
mapsheetid,
mapsheetchildid,
isactive,
isdelete,
createdby,
createddate,
modifiedby,
modifieddate)
VALUES
(
vtrailbalanceid,
vmapsheetid,
vfinsubgroupchildId,
b'1',
b'0',
1,
nodate,
1,
nodate
);
	set loopstart = loopstart + 1;
	end while;
    end;
BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.Trailbalncemappingload',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
     end;
END