﻿/********************************************************************************************************************************
** SP Name: signupvalidation
**
** Purpose: Validation when user signup.
**
**Parameters: 	emailid 
**				
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `signupvalidation`(IN emailid varchar(100))
BEGIN

/* Variable Declaration*/
Declare checkemail int(2);
Declare vuseraccountid int(20);
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variables value*/
set checkemail =0;
SET  autocommit=0; 
select 1, useraccountid into checkemail, vuseraccountid  from adinovis.useraccount where emailaddress = emailid and statusid = 1;

/* provide URL+token key when user exist*/
if checkemail = 1 then
select concat(a.advpropertievalue,u.tokenvalue) as resetvalidation from adinovis.useraccountauth u, adinovis.advpropertie a 
where u.useraccountid = vuseraccountid and a.advpropertiename = 'emailvalidation' and 1=1;
else
		set errorCode = '99999';
         set errorMessage = 'token invalid';
	END IF;
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.loginvalidation',nodate); 
	end if;
END;
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END