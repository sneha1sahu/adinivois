﻿/********************************************************************************************************************************
** SP Name: saveclientprofile
**
** Purpose: Save client information into DB.
**
**Parameters: 	Pbsname : business name
**				Pcontactperson : contact person
**				Pbphonenumber : business phone
**				Pcphonenumber : cell phone phone
**				Pemailadd : email address
**				Ppwd : password
**				Pjurisdictionid : jurisdictionid 
**				Ptypeofentityid : type of entity id
**				Pincorporationdate : incorporation date
**				Paccountbook : account book (Quick Book or Xero)
**				Paccesskey : access key
**				Psecretkey : secret key
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 18-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `saveclientprofile`(IN Pbsname varchar(100),
										IN Pcontactperson varchar(100),
										IN Pbphonenumber varchar(100),
										IN Pcphonenumber varchar(100),
										IN Pemailadd varchar(100),
                                        IN Ppwd varchar(100),
										IN Pjurisdictionid int(20),
										IN Ptypeofentityid int(20),
										IN Pincorporationdate date,
										IN Paccountbook varchar(100),
										IN Paccesskey varchar(255),
										IN Psecretkey varchar(255),
										IN Ploginid int(20))
BEGIN
Declare vbsname varchar(100);
Declare vcontactperson varchar(100);
Declare vphonebtype varchar(100);
Declare vbphonenumber varchar(100);
Declare vphonectype varchar(100);
Declare vcphonenumber varchar(100);
Declare vemailadd varchar(100);
Declare vpwd varchar(100);
Declare vjurisdictionid int(20);
Declare vtypeofentityid int(20);
Declare vincorporationdate date;
Declare vaccountbook varchar(100);
Declare vaccesskey varchar(255);
Declare vsecretkey varchar(255);
Declare vrole BIGINT(10);
Declare vclientfirmid INT(20);
Declare vuseraccountid INT(20);
Declare vauditorid INT(20);
Declare nodate datetime;

	DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
	
set nodate = current_timestamp;
set  autocommit=0;
set vbsname = Pbsname;
set vcontactperson = Pcontactperson;
set vphonebtype = 'business phone';
set vbphonenumber = Pbphonenumber;
set vphonectype = 'cell phone';
set vcphonenumber = Pcphonenumber;
set vemailadd = Pemailadd;
set vjurisdictionid = Pjurisdictionid;
set vtypeofentityid = Ptypeofentityid;
set vincorporationdate = Pincorporationdate;
set vaccountbook = Paccountbook;
set vaccesskey = Paccesskey;
set vsecretkey = Psecretkey;
set vrole = 4;
set vauditorid = Ploginid;
call adinovis.savepassword(Ppwd, @pwd);
            SET vpwd = @pwd;
-- set vpwd = Ppwd;
 
	BEGIN 
       /*START TRANSACTION */
	INSERT INTO adinovis.useraccount(
			firstname,	    		
			lastname,		
			businessname,	
			emailaddress,	
		    loginpassword,
			statusid,
			createdby,
			createddate,	
			modifiedby,
			modifieddate
			)
			VALUES (
			vcontactperson, 			
			vcontactperson, 			
			vbsname,
			vemailadd,
			vpwd,	
			4,
			CAST(vauditorid AS CHAR),
			nodate,
			CAST(vauditorid AS CHAR),
			nodate
			);


			-- select useraccountid into vuseraccountid from adinovis.useraccount where emailaddress = vemailadd;

set vuseraccountid = (SELECT  LAST_INSERT_ID());


/*
				UPDATE adinovis.useraccount 
			SET 
				createdby = CAST(vuseraccountid AS CHAR),
				modifiedby = CAST(vuseraccountid AS CHAR)
			WHERE
				emailaddress = vemailadd;*/
		
					INSERT INTO adinovis.useraccountrole(
					useraccountid,
					userroleid,
					createdby,
					createddate,	
					modifiedby,
					modifieddate)
					VALUES (
					vuseraccountid,
					vrole,
					CAST(vauditorid AS CHAR),
					nodate,
					CAST(vauditorid AS CHAR),
					nodate);
        
        
        
				 	INSERT INTO adinovis.phone(
					useraccountid,
					phonenumber,
					phonetype,	
					createdby,
					createddate,	
					modifiedby,
					modifieddate)
					VALUES (
					vuseraccountid,
					vbphonenumber,
					vphonebtype,
					CAST(vauditorid AS CHAR),
					nodate,
					CAST(vauditorid AS CHAR),
					nodate),                    
					(vuseraccountid ,
					vcphonenumber,
					vphonectype,
					CAST(vauditorid AS CHAR),
					nodate,
					CAST(vauditorid AS CHAR),
					nodate);
					
					
					INSERT INTO adinovis.clientfirm(
									useraccountid, 
									contactperson, 
									jurisdictionid,
									typeofentityid,
									incorporationdate,
                                    statusid,
									createdby,
									createddate,
									modifiedby,
									modifieddate)
							VALUES (vuseraccountid,
									vcontactperson,
									vjurisdictionid,
									vtypeofentityid,
									vincorporationdate,
                                    4,
									CAST(vauditorid AS CHAR),
									nodate,
									CAST(vauditorid AS CHAR),
									nodate);
									
	-- select  clientfirmid into vclientfirmid from adinovis.clientfirm where useraccountid = vuseraccountid and modifieddate = nodate ;
	set vclientfirmid = (SELECT  LAST_INSERT_ID());
        
						INSERT INTO adinovis.clientdatasource(
									clientfirmid,
									sourcelink,
									clientid,
									secretkey, 
									createdby,
									createddate,
									modifiedby,
									modifieddate)
							VALUES (vclientfirmid,
									vaccountbook,
									vaccesskey,
									vsecretkey,
									CAST(vauditorid AS CHAR),
									nodate,
									CAST(vauditorid AS CHAR),
									nodate);
									
									

	END;
 

	BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.saveclientprofile',current_timestamp);
	end if;
    END; 
	
COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END