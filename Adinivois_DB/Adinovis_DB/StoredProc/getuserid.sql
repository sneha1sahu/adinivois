﻿/********************************************************************************************************************************
** SP Name: getuserid
**
** Purpose: Provide the useraccountid for given user. 
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `getuserid`(in emailid varchar(100))
BEGIN
select useraccountid from adinovis.useraccount where emailaddress = emailid;
END