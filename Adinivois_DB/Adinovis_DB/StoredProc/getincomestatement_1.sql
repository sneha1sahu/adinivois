﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getincomestatement`(inengagementid int(20), inyear int(20))
BEGIN

	Declare nodate datetime;
    Declare checkengsrc int;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';
	DECLARE statementtype int ;
    Declare total_income varchar(500);
    

drop TEMPORARY TABLE if exists grptotal;
drop TEMPORARY TABLE if exists subgrptotal;
drop TEMPORARY TABLE if exists acttotal; 
    
 CREATE TEMPORARY TABLE grptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(100) 
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE subgrptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(100) 
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE acttotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(100) 
, acttotal varchar(100)
, grpid int
, sbgrpid int
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

set statementtype = 2;



                insert into acttotal
                (accountname ,  grpid, sbgrpid,acttotal)
				select 
					vt.accountname
				, 	vt.fingroupid
                ,	vt.finsubgrpid
                ,	sum(vt.originalbalance) acttotal
				from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                group by vt.accountname, vt.fingroupid ,vt.finsubgrpid;

   
    				insert into subgrptotal
                (accountname , acttotal  )
				select 
					vt.finsubgroupname
				,	sum(vt.originalbalance) subgrouptotal
				from adinovis.vtbmapping vt
				join fingroup fg on vt.fingroupid = fg.fingroupid
				join finsubgroup fsg on vt.fingroupid = fsg.fingroupid and vt.finsubgrpid = fsg.finsubgroupid
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                group by vt.finsubgroupname;
    
 				insert into grptotal
                (accountname , acttotal  )
				select 
					vt.fingroupname
				,	sum(vt.originalbalance) grouptotal
				from adinovis.vtbmapping vt
				join fingroup fg on vt.fingroupid = fg.fingroupid
				join finsubgroup fsg on vt.fingroupid = fsg.fingroupid and vt.finsubgrpid = fsg.finsubgroupid
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                group by vt.fingroupname;   
    
   select
	sum(vt.originalbalance) acttotal into total_income
				from adinovis.vtbmapping vt
				where vt.statementtypeid = 2
				and vt.engagementsid = 40
				and vt.acctyear = 2019;
                
     set nodate = current_timestamp;   
    
    set checkengsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementid ) ,1,0));
		if ( checkengsrc = 1 ) then 
			begin
				select JSON_ARRAYAGG(json_object('Total Income',  total_income
					,'children',
		(select JSON_ARRAYAGG(json_object ('name', fg.fingroupname,'grouptotal', bg.acttotal
					, 'children',(select JSON_ARRAYAGG(json_object ('name',fsg.finsubgroupname, 'subgrouptotal', sbg.acttotal
					, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'total', act.acttotal
					, 'children' , (select JSON_ARRAYAGG(json_object( 'name', vt.accountname,'bal',vt.originalbalance))
							from adinovis.vtbmapping vt  
							where vt.finsubgrpid = fsg.finsubgroupid
							and act.accountname = vt.accountname
							and vt.fingroupid = fg.fingroupid
							and vt.statementtypeid = statementtype
							and vt.engagementsid = inengagementid
							and vt.acctyear = inyear) 
							))
						from acttotal act  
						where act.sbgrpid = fsg.finsubgroupid
						)
				))
				from adinovis.finsubgroup fsg 
				join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
				where fg.fingroupid = fsg.fingroupid
				order by fsg.sequenceorder
				))) 
				from adinovis.fingroup fg
				join grptotal bg on fg.fingroupname = bg.accountname
				where bsoris = 'income Statement'
				order by fg.sequenceorder 
                 )
				)) as Result;
            end;
		else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
		end if;
END