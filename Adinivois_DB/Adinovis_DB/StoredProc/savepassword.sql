﻿/********************************************************************************************************************************
** SP Name: savepassword
**
** Purpose: Save encryptpassword in DB.
**
**Parameters: 	upassword
**				ecrytpass
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 16-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `savepassword`(
in upassword varchar(255),
out ecrytpass varchar(255)  
)
BEGIN

Declare encryptpassword varchar(255);


set encryptpassword = concat('0x0200', (sha1(upassword) ) );

set ecrytpass = encryptpassword;



END