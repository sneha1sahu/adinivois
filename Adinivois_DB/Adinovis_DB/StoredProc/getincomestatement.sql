﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getincomestatement`(inengagementid int(20), inyear int(20))
BEGIN

	Declare nodate datetime;
    Declare checkengsrc int;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';
	DECLARE statementtype int ;
    Declare total_income varchar(500);
    DEclare total_income_data varchar(500);
	Declare taxes varchar(500);
	Declare total_taxes varchar(500);
	Declare before_in_tax varchar(500);
	Declare exclude_tax varchar(500);
	Declare other_ex varchar(5000);
	Declare sub_rev_ex varchar(500);
	Declare other_ex_total varchar(500);
	Declare rev_ex varchar(500);
	Declare total_ex varchar(500);
	Declare total_rev varchar(500);
	Declare deficit_statring varchar(500);
	Declare staring_deficit_data varchar(500);
	Declare deficit_end varchar(500);
	Declare deficit_end_data varchar(500);
	Declare other_ex_data varchar(500);
	Declare vincorporationdate date; 
	declare vclientfirmid int(20);
		declare crbal int(20);
		declare drbal int(20);
    

drop TEMPORARY TABLE if exists grptotal;
drop TEMPORARY TABLE if exists subgrptotal;
drop TEMPORARY TABLE if exists acttotal;
drop TEMPORARY TABLE if exists otherexpence;  
    
 CREATE TEMPORARY TABLE grptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255) 
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE subgrptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255) 
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE acttotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255) 
, acttotal varchar(100)
, grpid int
, sbgrpid int
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


CREATE TEMPORARY TABLE otherexpence
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255)
, bal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

set statementtype = 2;

select clientfirmid into vclientfirmid from adinovis.engagements where engagementsid = inengagementid;

select incorporationdate into vincorporationdate from adinovis.clientfirm where
clientfirmid = vclientfirmid;  

			
				insert into otherexpence
                (accountname, bal)
                select 
					vt.accountname
                    , vt.originalbalance bal
                    from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.finsubgroupchildId = 154;


                insert into acttotal
                (accountname ,  grpid, sbgrpid,acttotal)
				select 
					vt.accountname
				, 	vt.fingroupid
                ,	vt.finsubgrpid
                ,	sum(vt.originalbalance) acttotal
				from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid in (4,5)
                group by vt.accountname, vt.fingroupid ,vt.finsubgrpid;

   
    				insert into subgrptotal
                (accountname , acttotal )
				select 
					vt.finsubgroupname
				,	sum(vt.originalbalance) subgrouptotal
				from adinovis.vtbmapping vt
				join fingroup fg on vt.fingroupid = fg.fingroupid
				join finsubgroup fsg on vt.fingroupid = fsg.fingroupid and vt.finsubgrpid = fsg.finsubgroupid
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid in (4,5)
                group by vt.finsubgroupname;
    
 				insert into grptotal
                (accountname , acttotal)
				select 
					vt.fingroupname
				,	sum(vt.originalbalance) grouptotal
				from adinovis.vtbmapping vt
				join fingroup fg on vt.fingroupid = fg.fingroupid
				join finsubgroup fsg on vt.fingroupid = fsg.fingroupid and vt.finsubgrpid = fsg.finsubgroupid
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid in (4,5)
                group by vt.fingroupname; 


				
    
                
		set total_rev = (select sum(vt.originalbalance) 
				from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid = 5);
                
		set total_ex = (select sum(vt.originalbalance) 
				from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid = 4);
                
		Set rev_ex = total_rev - total_ex ;


	-- if the incoperation date and inyear is same then no need to show previous year details		
	if year(vincorporationdate) = inyear then
		begin
			select sum(bal) into other_ex_total from otherexpence; 
		
				set other_ex_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Other Expenses','Currbal', case 
																			when other_ex_total < 0 then concat('(',format(-(other_ex_total),0),')')
																			else format(other_ex_total,0) end )));
        
				set sub_rev_ex = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before undernoted','Currbal',case 
																			when rev_ex < 0 then concat('(',format(-(rev_ex),0),')')
																			else format(rev_ex,0) end )));
        
            
                                                                    
				set other_ex = -- (select JSON_ARRAYAGG(json_object(
						(select JSON_MERGE_PRESERVE((select JSON_ARRAYAGG(json_object ('name',ox.accountname,'Currbal',case 
																when ox.bal < 0 then concat('(',format(-(ox.bal),0),')')
																else format(ox.bal,0)
																end))
						from otherexpence ox),other_ex_data));
      
				set exclude_tax = rev_ex - other_ex_total;
	
				 set before_in_tax = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before Income Tax','Currbal',case 
																				when exclude_tax < 0 then concat('(',format(-(exclude_tax),0),')')
																				else format(exclude_tax,0) end )));

				 set total_taxes = 0.00;
        
				set taxes = (select JSON_ARRAYAGG(json_object ('subgroupname','Provision for (recovery of) income taxes','CurrmainTotal',case 
																			when total_taxes < 0 then concat('(',format(-(total_taxes),0),')')
																			else format(total_taxes,0) end))); 
        
				 set total_income = exclude_tax - 0.00;
                
				  set total_income_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Net Earning (Loss)','CurrmainTotal',case 
																				when total_income < 0 then concat('(',format(-(total_income),0),')')
																				else format(total_income,0) end))); 
			
					set deficit_statring = 0.00;
					set staring_deficit_data =(select JSON_ARRAYAGG(json_object ('subgroupname','Retained earnings (deficit), beginning of year','CurrTotal',case 
																			when deficit_statring < 0 then concat('(',format(-(deficit_statring),0),')')
																			else format(deficit_statring,0) end))); 
			
					set	deficit_end = total_income - deficit_statring;
					set deficit_end_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Deficit, end of the year','CurrTotal',case 
																			when deficit_end < 0 then concat('(',format(-(deficit_end),0),')')
																			else format(deficit_end,0) end)));                                                                           
                                                                    
                
					set nodate = current_timestamp;
     
    
				set checkengsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementid ) ,1,0));

				if ( checkengsrc = 1 ) then 
					begin
						select JSON_MERGE_PRESERVE(              
							(select JSON_ARRAYAGG(json_object ('name', fg.fingroupname,'Currgrouptotal', case 
												when bg.acttotal < 0 then concat('(',format(-(bg.acttotal),0),')')
												else format(bg.acttotal,0) end
								, 'children' , (select JSON_ARRAYAGG(json_object( 'name', vt.accountname,'Currbal',case 
																					when vt.originalbalance < 0 then concat('(',format(-(vt.originalbalance),0),')')
																					else format(vt.originalbalance,0) end))
										from adinovis.vtbmapping vt  
										where
										vt.fingroupid = fg.fingroupid
										and vt.statementtypeid = statementtype
										and vt.engagementsid = inengagementid
										and vt.acctyear = inyear) 
							
				
						)) 
							from adinovis.fingroup fg
							join grptotal bg on fg.fingroupname = bg.accountname
							where bsoris = 'income Statement'
							order by fg.sequenceorder 
							 ),sub_rev_ex,other_ex_data, before_in_tax,taxes, total_income_data,staring_deficit_data, deficit_end_data)
							 as Result;
					end;            
				else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
				end if;
		end;
	end if; 

				select coalesce(sum(acctcredit),0), coalesce(sum(acctdebit),0)  into crbal , drbal from adinovis.trailbalance where engagementsid=inengagementid and acctyear = (inyear-1);
		

				if ( crbal > 0  or  drbal > 0 ) then
					begin
						select sum(bal) into other_ex_total from otherexpence; 
		
						set other_ex_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Other Expenses','Currbal', case 
																					when other_ex_total < 0 then concat('(',format(-(other_ex_total),0),')')
																					else format(other_ex_total,0) end ,'Prevbal', 0.00)));
        
						set sub_rev_ex = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before undernoted','Currbal',case 
																					when rev_ex < 0 then concat('(',format(-(rev_ex),0),')')
																					else format(rev_ex,0) end ,'Prevbal', 0.00)));	
                                                                    
						set other_ex = -- (select JSON_ARRAYAGG(json_object(
							(select JSON_MERGE_PRESERVE((select JSON_ARRAYAGG(json_object ('name',ox.accountname,'Currbal',case 
																	when ox.bal < 0 then concat('(',format(-(ox.bal),0),')')
																	else format(ox.bal,0)
																	end, 'Prevbal',0.00))
							from otherexpence ox),other_ex_data));
      
      
						set exclude_tax = rev_ex - other_ex_total;
	
						set before_in_tax = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before Income Tax','Currbal',case 
																				when exclude_tax < 0 then concat('(',format(-(exclude_tax),0),')')
																				else format(exclude_tax,0) end ,'Prevbal', 0.00)));
        
						set total_taxes = 0.00;
        
						set taxes = (select JSON_ARRAYAGG(json_object ('subgroupname','Provision for (recovery of) income taxes','CurrmainTotal',case 
																			when total_taxes < 0 then concat('(',format(-(total_taxes),0),')')
																			else format(total_taxes,0) end,'PrevmainTotal',0.00))); 
        
						 set total_income = exclude_tax - 0.00;
                
						set total_income_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Net Earning (Loss)','CurrmainTotal',case 
																				when total_income < 0 then concat('(',format(-(total_income),0),')')
																				else format(total_income,0) end,'PrevmainTotal',0.00))); 
			
						set deficit_statring = 0.00;
						set staring_deficit_data =(select JSON_ARRAYAGG(json_object ('subgroupname','Retained earnings (deficit), beginning of year','CurrTotal',case 
																				when deficit_statring < 0 then concat('(',format(-(deficit_statring),0),')')
																				else format(deficit_statring,0) end,'PrevTotal',0.00))); 
			
						set	deficit_end = total_income - deficit_statring;
						set deficit_end_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Deficit, end of the year','CurrTotal',case 
																				when deficit_end < 0 then concat('(',format(-(deficit_end),0),')')
																				else format(deficit_end,0) end,'PrevTotal',0.00)));        
					end;
				else
					begin
						select sum(bal) into other_ex_total from otherexpence; 
		
						set other_ex_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Other Expenses','Currbal', case 
																					when other_ex_total < 0 then concat('(',format(-(other_ex_total),0),')')
																					else format(other_ex_total,0) end )));
        
						set sub_rev_ex = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before undernoted','Currbal',case 
																					when rev_ex < 0 then concat('(',format(-(rev_ex),0),')')
																					else format(rev_ex,0) end )));	
                                                                    
						set other_ex = -- (select JSON_ARRAYAGG(json_object(
							(select JSON_MERGE_PRESERVE((select JSON_ARRAYAGG(json_object ('name',ox.accountname,'Currbal',case 
																	when ox.bal < 0 then concat('(',format(-(ox.bal),0),')')
																	else format(ox.bal,0)
																	end))
								from otherexpence ox),other_ex_data));
      
      
						set exclude_tax = rev_ex - other_ex_total;
	
						set before_in_tax = (select JSON_ARRAYAGG(json_object ('subgroupname','Earnings (Loss) before Income Tax','Currbal',case 
																				when exclude_tax < 0 then concat('(',format(-(exclude_tax),0),')')
																				else format(exclude_tax,0) end )));
        
						set total_taxes = 0.00;
        
						set taxes = (select JSON_ARRAYAGG(json_object ('subgroupname','Provision for (recovery of) income taxes','CurrmainTotal',case 
																			when total_taxes < 0 then concat('(',format(-(total_taxes),0),')')
																			else format(total_taxes,0) end))); 
        
						 set total_income = exclude_tax - 0.00;
                
						set total_income_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Net Earning (Loss)','CurrmainTotal',case 
																				when total_income < 0 then concat('(',format(-(total_income),0),')')
																				else format(total_income,0) end))); 
			
						set deficit_statring = 0.00;
						set staring_deficit_data =(select JSON_ARRAYAGG(json_object ('subgroupname','Retained earnings (deficit), beginning of year','CurrTotal',case 
																				when deficit_statring < 0 then concat('(',format(-(deficit_statring),0),')')
																				else format(deficit_statring,0) end))); 
			
						set	deficit_end = total_income - deficit_statring;
						set deficit_end_data = (select JSON_ARRAYAGG(json_object ('subgroupname','Deficit, end of the year','CurrTotal',case 
																				when deficit_end < 0 then concat('(',format(-(deficit_end),0),')')
																				else format(deficit_end,0) end)));        
					end;
				end if;
					                                                             
                
				set nodate = current_timestamp;
     
    
			-- including prev bal
				set checkengsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementid ) ,1,0));
					if ( checkengsrc = 1 ) then 
						begin
							if ( crbal > 0  or  drbal > 0 ) then
								begin
									select JSON_MERGE_PRESERVE(              
											(select JSON_ARRAYAGG(json_object ('name', fg.fingroupname,'Currgrouptotal', case 
																	when bg.acttotal < 0 then concat('(',format(-(bg.acttotal),0),')')
																	else format(bg.acttotal,0) end, 'Prevgrouptotal',0.00
													, 'children' , (select JSON_ARRAYAGG(json_object( 'name', vt.accountname,'Currbal',case 
																										when vt.originalbalance < 0 then concat('(',format(-(vt.originalbalance),0),')')
																										else format(vt.originalbalance,0) end, 'Prevbal', 0.00))
															from adinovis.vtbmapping vt  
															where -- vt.finsubgrpid = fsg.finsubgroupid and 
															-- act.accountname = vt.accountname
															-- and 
															vt.fingroupid = fg.fingroupid
															and vt.statementtypeid = statementtype
															and vt.engagementsid = inengagementid
															and vt.acctyear = inyear) 
							
				
											)) 
												from adinovis.fingroup fg
												join grptotal bg on fg.fingroupname = bg.accountname
												where bsoris = 'income Statement'
												order by fg.sequenceorder 
												 ),sub_rev_ex,other_ex_data, before_in_tax,taxes, total_income_data,staring_deficit_data, deficit_end_data)
												 as Result;
								end;
							else
								begin
									select JSON_MERGE_PRESERVE(              
											(select JSON_ARRAYAGG(json_object ('name', fg.fingroupname,'Currgrouptotal', case 
																	when bg.acttotal < 0 then concat('(',format(-(bg.acttotal),0),')')
																	else format(bg.acttotal,0) end
													, 'children' , (select JSON_ARRAYAGG(json_object( 'name', vt.accountname,'Currbal',case 
																										when vt.originalbalance < 0 then concat('(',format(-(vt.originalbalance),0),')')
																										else format(vt.originalbalance,0) end))
															from adinovis.vtbmapping vt  
															where -- vt.finsubgrpid = fsg.finsubgroupid and 
															-- act.accountname = vt.accountname
															-- and 
															vt.fingroupid = fg.fingroupid
															and vt.statementtypeid = statementtype
															and vt.engagementsid = inengagementid
															and vt.acctyear = inyear) 
							
				
											)) 
												from adinovis.fingroup fg
												join grptotal bg on fg.fingroupname = bg.accountname
												where bsoris = 'income Statement'
												order by fg.sequenceorder 
												 ),sub_rev_ex,other_ex_data, before_in_tax,taxes, total_income_data,staring_deficit_data, deficit_end_data)
												 as Result;
								end;
							end if;
						end;  	
					else
								set errorCode = '99999';
								set errorMessage = 'delete cannot peform on master trail balance record';
					end if;		


END