﻿CREATE DEFINER=`admin`@`%` PROCEDURE `updatetrailbalancedailyload`(inengagementsid int(20))
BEGIN

Declare loopstart int(10);
Declare loopend int(10);
Declare loopstart1 int(10);
Declare loopend1 int(10);
Declare dailyloadstartid int(10);
Declare dailyloadend int(10);
Declare tbid1 int(10);
Declare code1 varchar(100);
Declare accname1 varchar(100);
Declare crdt1 decimal(10,2);
Declare debt1 decimal(10,2);
Declare tbid2 int(10);
Declare code2 varchar(100);
Declare accname2 varchar(100);
Declare crdt2 decimal(10,2);
Declare debt2 decimal(10,2);
Declare visprocessed bit(1);
Declare visnewrecord bit(1);
Declare visdifferwithexisting bit(1);
Declare vtrailbalanceid int(20);
Declare vtrailbalancedailyloadid int(20);



drop TEMPORARY TABLE if exists tbtest;
drop TEMPORARY TABLE if exists dailyload;

CREATE TEMPORARY TABLE tbtest
( tlid bigint NOT NULL AUTO_INCREMENT,
trailbalanceid bigint,
engagementsid bigint,
accountcode1 varchar(500),
accountname1 varchar(500),
acctcredit1 decimal(10,2),
acctdebit1 decimal(10,2),
checked bit(1),
 PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


CREATE TEMPORARY TABLE dailyload
( tlid bigint NOT NULL AUTO_INCREMENT,
trailbalancedailyloadid bigint,
engagementsid bigint,
accountcode2 varchar(500),
accountname2 varchar(500),
acctcredit2 decimal(10,2),
acctdebit2 decimal(10,2),
isprocessed bit(1), 
trailbalanceid bigint, 
isdifferwithexisting bit(1), 
isnewrecord bit(1)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


insert into tbtest(trailbalanceid, engagementsid, accountcode1, accountname1, acctcredit1, acctdebit1, checked)
select trailbalanceid, engagementsid, accountcode, accountname, acctcredit, acctdebit, b'0' 
FROM adinovis.trailbalance where engagementsid = inengagementsid and 
acctyear = (select max(acctyear) from adinovis.trailbalance where engagementsid = inengagementsid) ; 

insert into dailyload(trailbalancedailyloadid, engagementsid, accountcode2, accountname2, acctcredit2, acctdebit2)
select trailbalancedailyloadid, engagementsid, accountcode, accountname, acctcredit, acctdebit 
FROM adinovis.trailbalancedailyload where engagementsid = inengagementsid and isprocessed = 0;

	select min(tlid) into dailyloadstartid  from dailyload;
	select max(tlid) into dailyloadend  from dailyload; 
    
    -- select dailyloadstartid,dailyloadend;
   
--     select * from dailyload;
	
	 dailtloadloop : while dailyloadstartid <= dailyloadend
	 	do

 
	select trailbalancedailyloadid,accountcode2, accountname2, acctcredit2, acctdebit2 into tbid2,code2, accname2,crdt2, debt2 
	from dailyload where tlid = dailyloadstartid; -- and isprocessed = 0;

	
	
	select min(tlid) into loopstart  from tbtest;-- where checked = 0;
	select max(tlid) into loopend  from tbtest;-- where checked = 0;
	
		tbloop : while loopstart <= loopend
		 do
				begin
				
				select trailbalanceid, accountcode1, accountname1, acctcredit1, acctdebit1 
                into  tbid1, code1, accname1,crdt1, debt1 from tbtest where tlid = loopstart;
				-- select tbid1;
                set loopstart = loopstart + 1;
				IF code1 = code2 and accname1 = accname2 then
               -- select code1, code2, accname1, accname2, tbid1;
					
					IF crdt1 = crdt2 and debt1 = debt2 then 
					-- select crdt1, crdt2, debt1, debt2 , tbid2;
					update   dailyload 
                     -- adinovis.trailbalancedailyload 
					set isprocessed = 1, trailbalanceid = tbid1, isdifferwithexisting = 0, isnewrecord = 0
					where trailbalancedailyloadid = tbid2;
                    LEAVE tbloop;
					select 1;
					ELSE
					
					update  dailyload 
                    -- adinovis.trailbalancedailyload 
					set isprocessed = 1, trailbalanceid = tbid1, isdifferwithexisting = 1, isnewrecord = 0
					where trailbalancedailyloadid = tbid2;
                    
                    LEAVE tbloop;
					
					END IF;
					
				-- ELSE
				
	
				
                
				END IF;
								
				end;

		
		 end while;
         
         
				update    dailyload 
                -- adinovis.trailbalancedailyload 
				set isprocessed = 1, isnewrecord = 1, trailbalanceid = null, isdifferwithexisting = 0
				where accountname2 not in (select accountname from adinovis.trailbalance where engagementsid =  inengagementsid)
                ;
		
         
		-- update tbtest set checked = 1 where trailbalanceid = tbid1;
		set dailyloadstartid = dailyloadstartid + 1;
		 end while;
         
         select min(tlid) into loopstart1  from dailyload;
		 select max(tlid) into loopend1  from dailyload;
         
         while loopstart1 <= loopend1
		 do
				begin
                
              select isprocessed,isnewrecord,isdifferwithexisting,trailbalanceid,trailbalancedailyloadid into
              visprocessed, visnewrecord, visdifferwithexisting, vtrailbalanceid, vtrailbalancedailyloadid from dailyload
              where tlid = loopstart1;
                
			update adinovis.trailbalancedailyload tbd
			set tbd.isprocessed = visprocessed
            , tbd.isnewrecord = visnewrecord
            , tbd.isdifferwithexisting = visdifferwithexisting
            , tbd.trailbalanceid = vtrailbalanceid
           where tbd.trailbalancedailyloadid = vtrailbalancedailyloadid;
           
           set loopstart1 = loopstart1 +1;
           end;
           end while;
		
         -- select * from tbtest;
      /*  update  adinovis.trailbalancedailyload set isprocessed = (select d.isprocessed from dailyload d 
        where trailbalancedailyloadid = d.trailbalancedailyloadid),
        set trailbalanceid = (select d.trailbalanceid from from dailyload d 
        where trailbalancedailyloadid = d.trailbalancedailyloadid),
        set isdifferwithexisting = , isnewrecord)
		  select isprocessed,trailbalanceid, isdifferwithexisting, isnewrecord from dailyload 
          where trailbalancedailyloadid = trailbalancedailyloadid ;*/
        /*  select * from dailyload;
		select * 
         from adinovis.trailbalancedailyload tbd
         join dailyload dl on tbd.trailbalancedailyloadid = dl.trailbalancedailyloadid;*/

End