﻿/********************************************************************************************************************************
** SP Name: resetemailvalidation
**
** Purpose: SP provide the URL+token value for exist user.
**
**Parameters: 	emailid
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 20-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `resetemailvalidation`(IN emailid varchar(100))
BEGIN
/* Variable Declaration*/
Declare checkemail int(2);
Declare vuseraccountid int(20);
Declare errorCode varchar(4000);
Declare errorMessage varchar(4000);

/* set variables value*/
set checkemail =0;
select 1, useraccountid into checkemail, vuseraccountid  from adinovis.useraccount where emailaddress = emailid;

/*Provide the URL+token value for exist user.*/
if checkemail = 1 then
select concat(a.advpropertievalue,u.tokenvalue) as resetvalidation from adinovis.useraccountauth u, adinovis.advpropertie a 
where u.useraccountid = vuseraccountid and a.advpropertiename = 'reset password link' and 1=1;
else
		set errorCode = '99999';
        set errorMessage = 'token invalid';
end if;
END