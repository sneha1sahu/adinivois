﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savefsnamechange`(
In inengagementsid bigint(20),inacctyear varchar(50),infstype varchar(255),infsid varchar(255), incurrentname varchar(255), innewname varchar(255),inploginid int(5))
BEGIN
Declare nodate date;
Declare chkrecordexists int(2);
 
set nodate = current_timestamp;

set chkrecordexists = if( exists (SELECT 1 FROM adinovis.fsnamechange where engagementsid = inengagementsid  and acctyear = inacctyear  and fsid = infsid ),1,0);

 if chkrecordexists = 1 then 
	begin 
			 if upper(trim(incurrentname)) != upper(trim(innewname)) then
				 insert into fsnamechange_history(fsnamechangeid, 
				 engagementsid, 
				 acctyear, 
				 fstype, 
				 fsid, 
				 currentname,
				 newname, 
				 isactive, 
				 isdelete, 
				 createdby, 
				 createddate, 
				 modifiedby, 
				 modifieddate, 
				 historydate)
				 SELECT fsnamechangeid,
				 engagementsid, 
				 acctyear, 
				 fstype, 
				 fsid, 
				 currentname,
				 newname, 
				 isactive, 
				 isdelete, 
				 createdby, 
				 createddate, 
				 modifiedby, 
				 modifieddate,nodate from adinovis.fsnamechange where engagementsid = inengagementsid
				 and acctyear = inacctyear
				 and fsid = infsid ;
				 
				 update adinovis.fsnamechange 
				 set  
				 currentname = incurrentname,
				 newname = innewname,
				 modifiedby = inploginid,
				 modifieddate = nodate
				 where engagementsid = inengagementsid 
				 and acctyear = inacctyear 
				 and fsid = infsid;
			end if;
		end;
  else
    insert into adinovis.fsnamechange(engagementsid
    ,acctyear
    ,fstype
    ,fsid
    ,currentname
    ,newname
    ,isactive
    ,isdelete
    ,createdby
    ,createddate
    ,modifiedby
    ,modifieddate)
    values
    (inengagementsid
    ,inacctyear
    ,infstype
    ,infsid
    ,incurrentname
    ,innewname
    ,b'1'
    ,b'1'
    ,inploginid
    ,nodate
    ,inploginid
    ,nodate);
  end if;
  

END