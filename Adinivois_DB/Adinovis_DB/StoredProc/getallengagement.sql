﻿/********************************************************************************************************************************
** SP Name: getallengagement
**
** Purpose: provide list of engagement of all client . 
**
** Parameters: 
**             			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 17-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `getallengagement`(Ploginid int(20))
BEGIN

select distinct
ed.engagementsid,
ed.clientfirmid,
cf.useraccountid,
(select c.businessname from adinovis.clientprofile c where c.clientfirmid = cf.clientfirmid) as businessname,
cf.contactperson,
cf.incorporationdate,
ed.engagementname,
ed.subentitles,
ed.engagementtypeid,
ed.engagementtype,
ed.compilationtype,
ed.finanicalyearenddate,
ed.additionalinfo,
ed.engagementstatusid as statusid,
ed.tbloadstatusid,
(select s.statusname from adinovis.status s where s.statusid = ed.engagementstatusid) as statusname,
date(ed.createddate) as engagementcreateddate,
(select JSON_ARRAYAGG(json_object('clientfirmauditroleid',cfr.clientfirmauditorroleid , 'engagementsid', cfr.engagementsid, 'useraccountid',cfr.useraccountid,'auditroleid',cfr.auditroleid,'rolename',ur.rolename,'fullname',ui.fullname,'rowindex',ar.rowindex))
		from adinovis.clientfirmauditorrole cfr 
		join adinovis.auditrole ar on cfr.auditroleid = ar.auditroleid and ar.isactive = 1 and ar.isdelete = 0
		join adinovis.userrole ur on ar.userroleid = ur.userroleid and ur.isactive = 1 and ur.isdelete = 0
		join adinovis.userinformation ui on cfr.useraccountid = ui.useraccountid 
		where cfr.engagementsid = ed.engagementsid ) as auditorrole
,cast(ed.modifieddate as date) as modifieddate
,true as isowned
from adinovis.clientprofile cf
join adinovis.engagementdetails ed on ed.clientfirmid = cf.clientfirmid and ed.isactive=1 and ed.isdelete =0 
join adinovis.engagementtype eg on ed.engagementtypeid = eg.engagementtypeid  and eg.isactive = 1 and eg.isdelete = 0
where  cf.clientcreatedby = Ploginid and 
cf. userdelete= 0
and cf.clientfirmdelete = 0
union
select distinct
ed.engagementsid,
ed.clientfirmid,
cf.useraccountid,
(select c.businessname from adinovis.clientprofile c where c.clientfirmid = cf.clientfirmid) as businessname,
cf.contactperson,
cf.incorporationdate,
ed.engagementname,
ed.subentitles,
ed.engagementtypeid,
ed.engagementtype,
ed.compilationtype,
ed.finanicalyearenddate,
ed.additionalinfo,
ed.engagementstatusid as statusid,
ed.tbloadstatusid,
(select s.statusname from adinovis.status s where s.statusid = ed.engagementstatusid) as statusname,
date(ed.createddate) as engagementcreateddate,
(select JSON_ARRAYAGG(json_object('clientfirmauditroleid',cfr.clientfirmauditorroleid , 'engagementsid', cfr.engagementsid, 'useraccountid',cfr.useraccountid,'auditroleid',cfr.auditroleid,'rolename',ur.rolename,'fullname',ui.fullname,'rowindex',ar.rowindex))
		from adinovis.clientfirmauditorrole cfr 
		join adinovis.auditrole ar on cfr.auditroleid = ar.auditroleid and ar.isactive = 1 and ar.isdelete = 0
		join adinovis.userrole ur on ar.userroleid = ur.userroleid and ur.isactive = 1 and ur.isdelete = 0
		join adinovis.userinformation ui on cfr.useraccountid = ui.useraccountid 
		where cfr.engagementsid = ed.engagementsid ) as auditorrole
,cast(ed.modifieddate as date) as modifieddate
,false as isowned
from adinovis.clientprofile cf
join adinovis.engagementdetails ed on ed.clientfirmid = cf.clientfirmid and ed.isactive=1 and ed.isdelete =0 
join adinovis.engagementtype eg on ed.engagementtypeid = eg.engagementtypeid  and eg.isactive = 1 and eg.isdelete = 0
where cf. userdelete= 0 and cf.clientfirmdelete = 0
and ed.engagementsid in (SELECT eg.engagementsid FROM adinovis.clientfirmauditorrole cfr
							join adinovis.engagements eg on cfr.engagementsid = eg.engagementsid and eg.isactive = true and eg.isdelete  = false
							join adinovis.clientfirm cf on eg.clientfirmid = cf.clientfirmid and cf.isactive = true and cf.isdelete  = false
							WHERE  cfr.isactive = true and cfr.isdelete  = false
							and cfr.useraccountid  = Ploginid and cf.createdby != Ploginid)
order by modifieddate desc;



END