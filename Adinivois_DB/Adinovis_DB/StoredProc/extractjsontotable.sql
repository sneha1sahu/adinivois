﻿﻿﻿CREATE DEFINER=`admin`@`%` PROCEDURE `extractjsontotable`(IN inclientapidataid bigint(20))
BEGIN
Declare vuseraccountid int(20);
Declare vengagementsid bigint(20);
Declare vrecieveddate date;
Declare vaccountcode varchar(1000);
Declare vaccountname varchar(1000);
Declare vaccounttype varchar(100);
Declare vaccountdesc varchar(100);
Declare vacctcredit varchar(200);
Declare vacctdebit varchar(200);
Declare vacctyear varchar(20);
Declare vdetailacctype varchar(20);
Declare vaccountbal varchar(200);
Declare vrecieveddata text;
Declare json_row_count int(20);
Declare nodate date;
Declare i int(10);
Declare checkengid int;


DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';

        
DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;



drop TEMPORARY TABLE if exists tableloop;

CREATE TEMPORARY TABLE tableloop
( tlid bigint NOT NULL AUTO_INCREMENT
, engagementsid bigint 
, recieveddate date
, accountcode varchar(100)
, accountname varchar(500) 
, accounttype varchar(100)
, accountdetaildesc varchar(2000)
, acctcredit varchar(20)
, acctdebit varchar(20)
, acctyear varchar(20)
, createdby varchar(20)
, modifiedby varchar(20)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


SET nodate = date(current_timestamp);

select recieveddata into vrecieveddata from adinovis.clientapidata where clientapidataid = inclientapidataid;
select engagementsid into vengagementsid from adinovis.clientapidata where clientapidataid = inclientapidataid;


select useraccountid into vuseraccountid 
from adinovis.clientfirm cf 
join adinovis.engagements eg on cf.clientfirmid = eg.clientfirmid
join adinovis.clientapidata cd on eg.engagementsid = cd.engagementsid
and cd.clientapidataid = inclientapidataid
limit 1;

select json_extract(vrecieveddata,'$.header.endPeriod')  into vacctyear;

    
select  JSON_LENGTH(vrecieveddata,'$.rows.row') into json_row_count;

SET i = 0;

while json_row_count - 1 > 0
do 
select 
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[0].id')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[1].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[2].value')),
json_extract(vrecieveddata,concat('$.rows.row[',i,'].colData[3].value'))  INTO
 vaccountname,vaccountcode,vacctdebit, vacctcredit,vaccountdesc;
 
      if vacctcredit = '""' then  set vacctcredit = '0.00'; end if;
      if vacctdebit = '""' then   set vacctdebit = '0.00';  end if;


insert into tableloop( 
engagementsid,
recieveddate,
accountcode,
accountname,
accounttype,
accountdetaildesc,
acctcredit,
acctdebit,
acctyear,
createdby,
modifiedby)
values 
(vengagementsid,
nodate,
trim(replace(vaccountcode,'"',' ')),
trim(replace(vaccountname,'"',' ')),
trim(vaccounttype), 
trim(vdetailacctype),
trim(replace(vacctcredit,'"',' ')), 
trim(replace(vacctdebit,'"',' ')) ,
(replace(vacctyear,'"',' ')),
/*year(trim(replace(vacctyear,'"',' '))),*/
cast(vuseraccountid as char), 
cast(vuseraccountid as char)
);

SET i = i + 1;
set json_row_count = json_row_count - 1;

end while;
  
  
  set checkengid = (select if (exists ( select 1 from adinovis.trailbalance where engagementsid = vengagementsid ),1,0));
	if ( checkengid = 1 ) then
		begin
			insert into adinovis.trailbalancedailyload
				( engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , createdby
                , createddate
                , modifiedby
                , modifieddate
                )
			select  engagementsid
				,	recieveddate
				,	accountcode
				,	accountname
				,	accounttype
				,	acctcredit
				,	acctdebit
				,	acctyear
				,	createdby
                ,	nodate
				,	modifiedby
                ,	nodate
			from tableloop;        
        end;
	else
		begin
			insert into adinovis.trailbalance
				( engagementsid
                , recieveddate
                , accountcode
                , accountname
                , accounttype
                , acctcredit
                , acctdebit
                , acctyear
                , createdby
                , createddate
                , modifiedby
                , modifieddate
                )
			select  engagementsid
				,	recieveddate
				,	accountcode
				,	accountname
				,	accounttype
				,	acctcredit
				,	acctdebit
				,	acctyear
				,	createdby
                ,	nodate
				,	modifiedby
                ,	nodate
			from tableloop;   
		end;
	end if;
  
-- select * from tableloop;
  
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
values(errorCode, errorMessage,'adinovis.sb_test',nodate); 
end if;
END;

COMMIT;
set  autocommit=1;	


END