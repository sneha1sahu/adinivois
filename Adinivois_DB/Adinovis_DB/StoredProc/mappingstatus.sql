﻿CREATE DEFINER=`admin`@`%` PROCEDURE `mappingstatus`()
BEGIN
	select  
			mappingstatusid
		, 	mappingstatusname
		, 	mappingstatusdescription
	from adinovis.mappingstatus
	where isactive = 1
	and isdelete = 0 
    and mappingstatusid not in (6);
    -- order by mappingstatusid desc;
END