﻿/********************************************************************************************************************************
** SP Name: getjurisdiction
**
** Purpose: Provide list of jurisdiction . 
**		
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `getjurisdiction`()
BEGIN
select jurisdictionid, provincesname, provincescode,statename from jurisdiction;
END