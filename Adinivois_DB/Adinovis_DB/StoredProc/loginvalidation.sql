﻿/********************************************************************************************************************************
** SP Name: loginvalidation
**
** Purpose: Validate user login and update status. 
**
**Parameters: 	tokenkey
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `loginvalidation`(IN tokenkey varchar(500))
BEGIN

/* Variable Declaration*/
DECLARE vuseraccountid INT(20);
Declare vtokenkey varchar(100);
DECLARE nodate datetime;
DECLARE checkmailid INT(2);
DECLARE tokencheck INT(2);
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
 
/* set variables value*/
SET checkmailid = 0;
SET tokencheck = 0;
set vtokenkey = tokenkey;
set nodate = current_timestamp;
SET  autocommit=0; 

    


select 1,useraccountid into tokencheck,vuseraccountid from adinovis.useraccountauth  where tokenvalue = vtokenkey  and  isactive = 1 and  isdelete= 0 and tokentypeid = 1;

 select 1 into checkmailid from adinovis.useraccount where useraccountid = vuseraccountid and  isactive = 1 and  isdelete= 0 and statusid = 1;


 /*Validate user login and update status.*/
 IF checkmailid = 1 AND tokencheck = 1  THEN
		UPDATE adinovis.useraccount SET statusid = 3,
										modifiedby = cast(vuseraccountid AS CHAR),
										modifieddate = nodate
		WHERE useraccountid = vuseraccountid;

		UPDATE adinovis.useraccountauth SET tokentypeid = 3,
										modifiedby = cast(vuseraccountid AS CHAR),
										modifieddate = nodate
		WHERE useraccountid = vuseraccountid;
	else
		set errorCode = '99999';
         set errorMessage = 'token invalid';
	END IF;

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.loginvalidation',nodate); 
	end if;
END;

COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END