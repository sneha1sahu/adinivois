﻿/********************************************************************************************************************************
** SP Name: updateengagement
**
** Purpose: Edit client engagement information.
**
**Parameters: 	Pclientfirmid : Client firm ID
**				Pengagementname : engagement name
**				Pclientname:  client name
**				Pengagementid : engagement id
**				Pcompliation : compliation
**				Psubenties : subenties
**				Pyearend : finanical year end date
**				Pincorporationdate :incorporation date
**				Padditionalinfo : additional information
**				Puseraccoutid_roleid_list : auditor and role list
**				
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 20-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `updateengagement`(Pengagementsid int(20),
																Pengagementname varchar(100),
																Pclientfirmid int(20),
																Pengagementtypeid int(20),
																Pcompliation varchar(100),
																Psubenties varchar(100),
																Pyearend date,
																Padditionalinfo varchar(500),
																Puseraccoutid_roleid_list varchar(4000)
                                                                ,ploginid int(20))
BEGIN

Declare vengagementsid int(20);
Declare vuseraccountid int(20);
Declare vclientfirmid int(20);
Declare vengagementname varchar(100);
Declare vengagementtypeid int(20);
Declare vengagementtype varchar(100);
Declare vcompliation varchar(100);
Declare vsubenties varchar(100);
Declare vyearend date;
Declare vadditionalinfo varchar(500);
Declare value1 varchar(4000);
Declare vclientfirmauditorroleid int(20);
Declare nodate date;
Declare checkval int(2);
Declare V_DESIGNATION_acc int(20);
Declare V_DESIGNATION_role int(20);
Declare vuserroleid int(20);
Declare val1 int(20);
Declare newcount int(20);
Declare oldcount int(20);
Declare n int(20);

DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';


DECLARE v_finished INTEGER DEFAULT 0;
DECLARE cur1 CURSOR FOR 
select
clientfirmauditorroleid
from clientfirmauditorrole  where engagementsid = val1 and isactive = 1;

 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
        
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET val1 = Pengagementsid;

SET vengagementsid = Pengagementsid;
SET vengagementname = Pengagementname;
SET vclientfirmid = Pclientfirmid;
SET vengagementtypeid = Pengagementtypeid;
SET vcompliation = Pcompliation;
SET vsubenties=Psubenties;
SET vyearend = Pyearend;
SET vadditionalinfo = Padditionalinfo;
SET value1 = Puseraccoutid_roleid_list;
SET autocommit = 0;
SET nodate = current_timestamp;
SET checkval = 0;

set newcount =  (length(value1) - length(replace (value1,',',''))+1)/2;
select count(clientfirmauditorroleid) into oldcount from adinovis.clientfirmauditorrole where engagementsid = vengagementsid;


select useraccountid into vuseraccountid
		from adinovis.clientfirm where clientfirmid = vclientfirmid;

				


	INSERT INTO adinovis.engagements_history(engagementsid,
											clientfirmid,
											engagementname,
											subentitles,
											engagementtypeid,
											compilationtype,
											finanicalyearenddate,
											additionalinfo,
											statusid,
											modifiedby,
											modifieddate)
										SELECT engagementsid,
											clientfirmid,
											engagementname,
											subentitles,
											engagementtypeid,
											compilationtype,
											finanicalyearenddate,
											additionalinfo,
											statusid,
											modifiedby,
											modifieddate
								FROM adinovis.engagements
								WHERE engagementsid =  vengagementsid;
								
	UPDATE adinovis.engagements SET engagementname = vengagementname,
                                    subentitles = vsubenties,
                                    additionalinfo = vadditionalinfo,
									engagementtypeid = vengagementtypeid,
									compilationtype = vcompliation, 
									finanicalyearenddate = vyearend,
									modifiedby = cast(ploginid as char),
									modifieddate = nodate
									Where engagementsid =  vengagementsid
									and isactive = 1;

	INSERT INTO adinovis.clientfirmauditorrole_history(clientfirmauditorroleid,
														engagementsid,
														useraccountid,
														userroleid,
														rolesequence,
														perhourcal,
														modifiedby,
														modifieddate)
												SELECT clientfirmauditorroleid,
														engagementsid,
														useraccountid,
														userroleid,
														rolesequence,
														perhourcal,
														modifiedby,
														modifieddate
												FROM adinovis.clientfirmauditorrole
												WHERE  engagementsid =  vengagementsid;
	
	

  if newcount < oldcount then
 -- select newcount,oldcount;
  set n = oldcount - newcount;
 -- select n;
   delete from adinovis.clientfirmauditorrole where engagementsid =  vengagementsid  ORDER BY clientfirmauditorroleid DESC limit n;
  end if;
   
SET v_finished = 0;
OPEN cur1; 
get_val: LOOP

FETCH cur1 INTO  
vclientfirmauditorroleid;

IF v_finished = 1 THEN 
 LEAVE get_val;
 END IF;



   
    -- select value1;
      SET V_DESIGNATION_acc = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
      SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1); 
   --   select V_DESIGNATION_acc; -- 406,
    
      if LOCATE(',', value1) = 0 then
      set V_DESIGNATION_role = value1;
    --  select V_DESIGNATION_role_seq;
      else
	  SET V_DESIGNATION_role = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
      SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1);
    --  select V_DESIGNATION_role_seq; -- 5
	  end if;
	
	select userroleid into vuserroleid from adinovis.auditrole where auditroleid = V_DESIGNATION_role;
	
	UPDATE adinovis.clientfirmauditorrole SET	useraccountid = V_DESIGNATION_acc,
												userroleid = vuserroleid,
                                                auditroleid = V_DESIGNATION_role,
												modifiedby = cast(ploginid as char),
												modifieddate = nodate
												WHERE clientfirmauditorroleid = vclientfirmauditorroleid;
                                                

	
END LOOP get_val;
 
 CLOSE cur1;	
 if newcount > oldcount then 
 BEGIN 
    WHILE (LOCATE(',', value1) > 0) DO -- 406,1,408,2,409,6
    -- select value1;
      SET V_DESIGNATION_acc = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
      SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1); 
    --  select V_DESIGNATION_acc; -- 406,
    
      if LOCATE(',', value1) = 0 then
      set V_DESIGNATION_role = value1;
      else
	  SET V_DESIGNATION_role = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
      SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1);
     -- select V_DESIGNATION_role_seq; -- 1
	  end if;
	  
	  select userroleid into vuserroleid from adinovis.auditrole where auditroleid = V_DESIGNATION_role;
 
	
	INSERT INTO adinovis.clientfirmauditorrole(engagementsid,
												useraccountid,
												userroleid,
                                                auditroleid,
												isactive,
												isdelete,
												createdby,
												createddate,
												modifiedby,
												modifieddate)
										Values(vengagementsid,
												V_DESIGNATION_acc,
												vuserroleid,
												V_DESIGNATION_role,
												b'1',
												b'0',
												cast(ploginid as char),
												nodate,
												cast(ploginid as char),
												nodate);
												
   END WHILE;
  END;
  end if;
  
 
BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.updateengagement',nodate); 
	end if;
END;

COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END