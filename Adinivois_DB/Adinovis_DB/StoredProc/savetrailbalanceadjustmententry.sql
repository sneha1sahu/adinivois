﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savetrailbalanceadjustmententry`(
	  intrailbalanceid		bigint(20)
	, intrailadjustmentid		bigint(20)
    , inadjustmenttypeid	bigint(20)
    , inaccountcode			varchar(100)		
    , inaccountname			varchar(100)
    , inacctcredit			bigint(20)
    , inacctdebit			bigint(20)
    , incomment				varchar(100)
    , incomment1			varchar(100)
    , inisdelete			bit
	, ploginid				bigint(20)

)
BEGIN

/* Variable Declaration*/	

	Declare checktrailbalid 	int;
    Declare checktrailadjustmentid	int;
    Declare nodate datetime;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';
    
    /* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
    
    /* set variables value*/
		set  autocommit=0;
		set nodate = current_timestamp;
		set checktrailbalid 	= (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE isclientdatasource = 1 and trailbalanceid = intrailbalanceid ) ,1,0));
		set checktrailadjustmentid 	= (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailadjustment WHERE trailbalanceid = intrailbalanceid  and trailadjustmentid = intrailadjustmentid) ,1,0));
 
 	BEGIN 
       /*START TRANSACTION */
 
				IF ( checktrailbalid = 1 ) THEN
					IF ( checktrailadjustmentid = 1 ) then
						update adinovis.trailadjustment set 
							  accountcode	= inaccountcode
							, accountname	= inaccountname
							, acctcredit	= inacctcredit
							, acctdebit		= inacctdebit
							, comment		= incomment
							, comment1		= incomment1
							, isdelete		= inisdelete
							, modifiedby	= cast(ploginid as char )
							, modifieddate	= nodate
						where trailbalanceid = intrailbalanceid
							and trailadjustmentid = intrailadjustmentid ;                   
                    else
						insert into adinovis.trailadjustment
							( trailbalanceid
							, adjustmenttypeid
							, accountcode
							, accountname
							, acctcredit
							, acctdebit
							, comment
							, comment1
							, isdelete
							, createdby
							, createddate
							, modifiedby
							, modifieddate
							)
					values
						(  	intrailbalanceid
                        ,	inadjustmenttypeid
						, 	inaccountcode		
						, 	inaccountname		
						, 	inacctcredit		
						, 	inacctdebit		
						, 	incomment			
						, 	incomment1	
						, 	inisdelete		
						, 	cast(ploginid as char )
                        , 	nodate
                        , 	cast(ploginid as char )
                        , 	nodate
                        );                                       
                    end if;     
				else
					set errorCode = '99999';
					set errorMessage = 'trail balance record not found';
                end if;

	end;
	BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.savetrailbalanceadjustmententry',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END