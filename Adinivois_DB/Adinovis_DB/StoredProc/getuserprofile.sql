﻿/********************************************************************************************************************************
** SP Name: getuserprofile
**
** Purpose: Provide the user details for given user email id and password. 
**
**Parameters: 	emailid
**				password
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/


CREATE DEFINER=`root`@`localhost` PROCEDURE `getuserprofile`(IN emailid varchar(100), IN password varchar(100))
BEGIN
Declare vpassword varchar(100);
Declare vvpassword varchar(100);
Declare vpawd varchar(100);
Declare checkpwd int(2);
set checkpwd =0;
set vvpassword = null;

select loginpassword into vpassword  from adinovis.useraccount where emailaddress = emailid;



	if(	concat('0x0200', (sha1(password))) = vpassword) THEN 
			Select useraccountid,
			fullname, 
            firstname,
            lastname,
			businessname,
			emailaddress,
			loginpassword,
			status,
			userdelete,
			addressid,
			address, 
			city, 
			state, country, 
			postcode, 
			addressdelete,
			businessphoneid,
			businessphonetype, 
			businessphonenumber,
			businessphonedelete,
			cellphoneid,
			cellphonetype,
			cellphonenumber,
			cellphonedelete,
			userroleid,
			rolename,
			userroledelete ,
			licenseno,
			tokenvalue,
			profilepicture
			from adinovis.userinformation
			Where emailaddress = emailid
			 AND loginpassword = vpassword
			AND useractive = 1
			AND userdelete = 0
            AND status = 3;
		 else
				Select useraccountid,
			fullname, 
			businessname,
			emailaddress,
			loginpassword,
			status,
			userdelete,
			addressid,
			address, 
			city, 
			state, country, 
			postcode, 
			addressdelete,
			businessphoneid,
			businessphonetype, 
			businessphonenumber,
			businessphonedelete,
			cellphoneid,
			cellphonetype,
			cellphonenumber,
			cellphonedelete,
			userroleid,
			rolename,
			userroledelete ,
			licenseno,
			tokenvalue,
			profilepicture
			from adinovis.userinformation
			Where emailaddress = emailid
			 AND loginpassword = vvpassword
			AND useractive = 1
			AND userdelete = 0
            AND status = 3;
            END IF;

-- END IF;
END