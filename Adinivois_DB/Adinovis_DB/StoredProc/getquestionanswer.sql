﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getquestionanswer`(inquestiontypeid int(20),
									inengagementsid int(20))
BEGIN

select cq.clientquestionid,
cq.clientquestiontypeid,
cq.questiondescription,
cq.answertype,
cq.isactive as quesactive,
cq.isdelete as quesdelete,
ca.clientquestionanswerid,
ca.engagementsid,
ca.clientquestionid,
ca.clientanswer,
ca.clientnote,
ca.isactive as ansactive,
ca.isdelete as ansdelete
from adinovis.clientquestion cq
LEFT JOIN adinovis.clientquestionanswer ca ON cq.clientquestiontypeid = ca.clientquestionid
where cq.clientquestiontypeid = inquestiontypeid
and ca.engagementsid = inengagementsid;

END