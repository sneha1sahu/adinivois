﻿/********************************************************************************************************************************
** SP Name: getclientid
**
** Purpose: provide clientfirm id based on emailid and password. 
**		
** Parameters: 	emailid
**				password
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `getclientid`(in emailid varchar(100), IN Ppwd varchar(100))
BEGIN

/* Variable Declaration*/
Declare vpassword varchar(100);
Declare vvpassword varchar(100);
Declare vpawd varchar(100);

/* set variables value*/
Declare checkpwd int(2);
set checkpwd =0;
set vvpassword = null;



select loginpassword into vpassword  from adinovis.useraccount where emailaddress = emailid;

/* Validate password */
if(	concat('0x0200', (sha1(Ppwd))) = vpassword) THEN 
			Select ua.useraccountid, cf.clientfirmid
            from adinovis.useraccount ua
            join adinovis.clientfirm cf on ua.useraccountid = cf.useraccountid and cf.isactive = 1 and cf.isdelete = 0
			Where ua.emailaddress = emailid
			 AND ua.loginpassword = vpassword
             and ua.isactive = 1
             and ua.isdelete = 0;
             else
			Select ua.useraccountid, cf.clientfirmid
            from adinovis.useraccount ua
            join adinovis.clientfirm cf on ua.useraccountid = cf.useraccountid and cf.isactive = 1 and cf.isdelete = 0
			Where ua.emailaddress = emailid
			 AND ua.loginpassword = vpassword
             and ua.isactive = 1
             and ua.isdelete = 0;
            END IF;
END