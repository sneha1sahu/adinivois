﻿CREATE DEFINER=`root`@`localhost` PROCEDURE `saveclientdatasource`(IN Ptokenvalue varchar(500),
											IN Psourcelink varchar(500),
											IN Prealmid VARCHAR(4000),
											IN Prefreshtoken varchar(4000),
											IN Paccesstoken varchar(4000)
                                            )
BEGIN
DECLARE vuseraccountid INT(20);
DECLARE vclientdatasourceid INT(20);
DECLARE vclientfirmid int(20);
DECLARE vrecieveddata TEXT;
DECLARE Vresponsestatus varchar(100);
Declare nodate datetime;
Declare vtokenvalue varchar(400);
Declare vsourcelink varchar(500);
Declare vrealmid VARCHAR(4000);
Declare vrefreshtoken varchar(4000);
Declare vaccesstoken varchar(4000);
Declare checkdsrc int;




DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;


SET  autocommit=0;
-- SET dbstatus = null;
set vtokenvalue = Ptokenvalue;
set vsourcelink = Psourcelink;
set vrealmid = Prealmid;
set vrefreshtoken = Prefreshtoken;
set vaccesstoken = Paccesstoken;
set nodate = current_timestamp;

 

select useraccountid into vuseraccountid from adinovis.useraccountauth where  tokenvalue = vtokenvalue;

SELECT clientfirmid into vclientfirmid FROM adinovis.clientfirm WHERE useraccountid = vuseraccountid;


		BEGIN 
			set checkdsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.clientdatasource WHERE  clientfirmid = vclientfirmid ) ,1,0));
					IF (checkdsrc = 1 ) THEN
						insert into adinovis.clientdatasource_history
							( clientdatasourceid
                            , engagementsid
                            , clientfirmid
                            , sourcelink
                            , clientid
                            , secretkey
                            , realmid
                            , refreshtoken
                            , accesstoken
                            , isactive
                            , isdelete
                            , createdby
                            , createddate
                            , modifiedby
                            , modifieddate
                            , historyloaddate
                            )
						select clientdatasourceid
							 , engagementsid
                             , clientfirmid
                             , sourcelink
                             , clientid
                             , secretkey
                             , realmid
                             , refreshtoken
                             , accesstoken
                             , isactive
                             , isdelete
                             , createdby
                             , createddate
                             , modifiedby
                             , modifieddate
                             , nodate
						from adinovis.clientdatasource
                        where clientfirmid = vclientfirmid;
                                        
						update adinovis.clientdatasource
							set sourcelink 		= vsourcelink
                              , realmid			= vrealmid
                              , refreshtoken	= vrefreshtoken
                              ,	accesstoken		= vaccesstoken
                              , modifiedby		= CAST(vuseraccountid AS CHAR)
                              , modifieddate	= nodate
						where clientfirmid = vclientfirmid;
						                   
                    ELSE                               
						INSERT INTO adinovis.clientdatasource(
										clientfirmid,
										sourcelink,
										realmid,
										refreshtoken,
										accesstoken,
										createdby,
										createddate,
										modifiedby,
										modifieddate)
								VALUES (vclientfirmid,
										vsourcelink,
										vrealmid,
										vrefreshtoken,
										vaccesstoken,
										CAST(vuseraccountid AS CHAR),
										nodate,
										CAST(vuseraccountid AS CHAR),
										nodate);
						END IF;
			
		
		
		END;
	
	
	
	BEGIN
    
	IF errorCode != '00000' THEN
    ROLLBACK;
  --  set dbstatus = -1;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.saveclientdatasource',nodate); 
	end if;
    END;    


COMMIT; 

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

-- select  dbstatus;
END