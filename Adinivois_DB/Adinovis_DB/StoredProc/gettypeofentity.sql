﻿/********************************************************************************************************************************
** SP Name: gettypeofentity
**
** Purpose: Provide list of typeofentity. 
**		
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `gettypeofentity`()
BEGIN
SELECT typeofentityid, typeofentityname, typeofentitycode from typeofentity;
END