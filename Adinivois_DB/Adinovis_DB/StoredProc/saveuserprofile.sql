﻿/********************************************************************************************************************************
** SP Name: saveuserprofile
**
** Purpose: Save user information in DB.
**
**Parameters: 	Pfname : first name
**				Plname : last name
**				Pbsname : business name
**				Pemailadd : email address
**				Ppwd : password
**				Paddr : address
**				Pbphonenumber : business phone number
**				Pcphonenumber : cell phone number
**				plicno : license number
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `saveuserprofile`(IN  Pfname varchar(100),
									IN Plname varchar(100),
									IN Pbsname varchar(100),
									IN Pemailadd varchar(100),
									IN Ppwd varchar(100),
									IN Paddr varchar(100),
									IN Pbphonenumber varchar(100),
									IN Pcphonenumber varchar(100),
                                    IN plicno varchar(25)                                    
                                    )
BEGIN

/* Variable Declaration*/	
Declare vemailaddress Varchar(100);
Declare vuseraccountid BIGINT(20);
Declare vaddress varchar(255);
Declare vfname varchar(100);
Declare vlname varchar(100);
Declare vbsname varchar(100);
Declare vpwd varchar(100);
Declare vbphonenumber varchar(100);	
Declare vphonebtype varchar(100);
Declare vcphonenumber varchar(100);	
Declare vphonectype varchar(100);
Declare vrole BIGINT(10);
Declare vcreateddate date;
Declare vmodifieddate date;
Declare vlicno varchar(25);
Declare nodate datetime;
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variables value*/
set nodate = current_timestamp;
set  autocommit=0;
			SET vemailaddress 	= Pemailadd;
			SET vaddress 		= Paddr;
			SET vfname 			= Pfname;
			SET vlname 			= Plname;
			SET vbsname 		= Pbsname;
			-- SET vpwd 			= Ppwd;
			SET vbphonenumber 	= Pbphonenumber;	
			SET vphonebtype 	= 'business phone';
			SET vcphonenumber 	= Pcphonenumber;	
			SET vphonectype 	= 'cell phone';
			SET vrole 			= 3;
			SET vlicno 			= plicno;
			call adinovis.savepassword(Ppwd, @pwd);
            SET vpwd = @pwd;

	BEGIN 
       /*START TRANSACTION */
			INSERT INTO adinovis.useraccount(
			firstname,	    		
			lastname,		
			businessname,	
			emailaddress,	
			loginpassword,
			statusid,
			 createdby,
			createddate,	
			 modifiedby,
			modifieddate,
			licenseno
			)
			VALUES (
			vfname, 			
			vlname, 			
			vbsname,
			vemailaddress,
			vpwd,	
			1,
			'0',
			nodate,
			'0',
			nodate,
			vlicno
			);

			set vuseraccountid = (SELECT  LAST_INSERT_ID());

				UPDATE adinovis.useraccount 
			SET 
				createdby = CAST(vuseraccountid AS CHAR),
				modifiedby = CAST(vuseraccountid AS CHAR)
			WHERE
				emailaddress = vemailaddress;
		
					INSERT INTO adinovis.useraccountrole(
					useraccountid,
					userroleid,
					createdby,
					createddate,	
					modifiedby,
					modifieddate)
					VALUES (
					vuseraccountid,
					vrole,
					CAST(vuseraccountid AS CHAR),
					nodate,
					CAST(vuseraccountid AS CHAR),
					nodate);
        
        
        
				 INSERT INTO adinovis.address(
				 useraccountid,	
				 address,									
				createdby,
				createddate,	
				modifiedby,
				modifieddate)
				VALUES (
				vuseraccountid ,
				vaddress,
				CAST(vuseraccountid AS CHAR),
				nodate,
				CAST(vuseraccountid AS CHAR),
				nodate);
	
 
					INSERT INTO adinovis.phone(
					useraccountid,
					phonenumber,
					phonetype,	
					createdby,
					createddate,	
					modifiedby,
					modifieddate)
					VALUES (
					vuseraccountid,
					vbphonenumber,
					vphonebtype,
					CAST(vuseraccountid AS CHAR),
					nodate,
					CAST(vuseraccountid AS CHAR),
					nodate),                    
					(vuseraccountid ,
					vcphonenumber,
					vphonectype,
					CAST(vuseraccountid AS CHAR),
					nodate,
					CAST(vuseraccountid AS CHAR),
					nodate);

	END;
 

	BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.saveuserprofile',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END