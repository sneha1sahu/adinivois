﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getbalancestatement`(inengagementid int(20), inyear int(20))
BEGIN

	Declare nodate datetime;
    Declare checkengsrc int;
	DECLARE errorCode CHAR(5) DEFAULT '00000';
	DECLARE errorMessage TEXT DEFAULT '';
    	DECLARE statementtype int ;
        declare v_total_lib_eq varchar(500);
        Declare total_deficit varchar(500);
        Declare share_equity varchar(100);
        Declare unmapdata varchar(8000);
        Declare total_l_e varchar(1000);
        Declare unmaptotal varchar(500);
		Declare unmapped_data varchar(500);
        Declare vincorporationdate date; 
		declare vclientfirmid int(20);
		declare vunmapcount int(20);
		declare crbal int(20);
		declare drbal int(20);
		
    

drop TEMPORARY TABLE if exists grptotal;
drop TEMPORARY TABLE if exists subgrptotal;
drop TEMPORARY TABLE if exists acttotal; 
drop TEMPORARY TABLE if exists tbaccount;
drop TEMPORARY TABLE if exists tbunmaped;
    
 CREATE TEMPORARY TABLE grptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255)
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE subgrptotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255) 
, acttotal varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE acttotal
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255) 
, fingrpname varchar(100)
, finsubgrpname varchar(100)
, acttotal varchar(100)
, grpid int
, sbgrpid int
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;


CREATE TEMPORARY TABLE tbaccount
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255)
, grpid int
, sbgrpid int
, originalbalance varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE tbunmaped
( tlid bigint NOT NULL AUTO_INCREMENT
, accountname varchar(255)
, originalbalance varchar(100)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

set unmapdata = null;
set statementtype = 1;

select clientfirmid into vclientfirmid from adinovis.engagements where engagementsid = inengagementid;

select incorporationdate into vincorporationdate from adinovis.clientfirm where
clientfirmid = vclientfirmid;  

   /* --------------total deficit-------------------------------------*/            
		select
	sum(vt.originalbalance) acttotal into total_deficit
				from adinovis.vtbmapping vt
				where vt.statementtypeid = 2
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear;  

        /*-------------Total of balance of equity and liabilty----------*/
				 select 
				sum(vt.originalbalance) into v_total_lib_eq
				from adinovis.vtbmapping vt
				where vt.statementtypeid in (1,2)
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                and vt.fingroupid in (2,3,4,5,6);  
                
                insert into tbunmaped
                (accountname, originalbalance )
                SELECT accountname,originalbalance 
                FROM adinovis.vtbmapping
                where engagementsid = inengagementid
                and acctyear = inyear 
                and mappingstatus ='Unmapped';

					select count(accountname) into vunmapcount from tbunmaped;
                
                insert into tbaccount
                (accountname,  grpid , sbgrpid, originalbalance )
                select 
					vt.accountname
                 ,   vt.fingroupid 
                 ,	 vt.finsubgrpid
                 ,   vt.originalbalance
                 from adinovis.vtbmapping vt
                 where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                union 
                select 
					vms.finsubgroupchildname
				,     vms.fingroupid
                ,	  vms.finsubgrpid
				,	total_deficit as originalbalance
                from adinovis.vmapsheet vms
                where vms.fingroupid = 3
                and vms.finsubgrpid =11
                and vms.finsubgroupchildId = 76;
                    

                insert into acttotal
                (accountname ,finsubgrpname, fingrpname, grpid, sbgrpid,acttotal)
				select 
					vt.finsubgroupchildname
				, 	vt.finsubgroupname
                ,	vt.fingroupname
				, 	vt.fingroupid
                ,	vt.finsubgrpid
                ,	sum(vt.originalbalance) acttotal
				from adinovis.vtbmapping vt
				where vt.statementtypeid = statementtype
				and vt.engagementsid = inengagementid
				and vt.acctyear = inyear
                
                -- and vt.finsubgrpid in (1,7)
                group by vt.finsubgroupchildname, vt.finsubgroupname, vt.fingroupname
                union 
                select vms.finsubgroupchildname
                ,	  vms.finsubgroupname
                ,	  vms.fingroupname
                ,     vms.fingroupid
                ,	  vms.finsubgrpid
                ,	  total_deficit as acttotal
                from adinovis.vmapsheet vms
                where vms.fingroupid = 3
                and vms.finsubgrpid =11
                and vms.finsubgroupchildId = 76
				order by fingroupid, finsubgrpid;
                
                insert into subgrptotal
                (accountname , acttotal  )
				select finsubgrpname,
                sum(acttotal) as subgrouptotal
                from acttotal
                group by finsubgrpname;
                
                insert into grptotal
                (accountname , acttotal  )
				select fingrpname,
                sum(acttotal) as subgrouptotal
                from acttotal
                group by fingrpname
                ;
       
	   -- if the incoperation date and trail balance year is same, then previous balance is not included into json	            
		if year(vincorporationdate) = inyear then             
				 set nodate = current_timestamp;   
    
				set checkengsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementid ) ,1,0));

				if ( checkengsrc = 1 ) then 
					begin		
						select sum(originalbalance) into unmaptotal from tbunmaped;
	  
						  set unmapped_data = (select JSON_ARRAYAGG(json_object( 'Currbal',case 
																		when unmaptotal < 0 then format(-(unmaptotal),0)
																		else format(unmaptotal,0)
																		end)));
							set unmapdata = (select JSON_ARRAYAGG(json_object('name','Unmapped','children',(select JSON_ARRAYAGG(json_object ('name',tbm.accountname,'Currbal',case 
																		when tbm.originalbalance < 0 then format(-(tbm.originalbalance),0)
																		else format(tbm.originalbalance,0)
																		end))
												from tbunmaped tbm),'Currbal',case 
																		when unmaptotal < 0 then format(-(unmaptotal),0)
																		else format(unmaptotal,0)
																		end)));
                
							set total_l_e = (select JSON_ARRAYAGG(json_object ('name','Total liabilities and Equity','CurrmainTotal',case 
																			when v_total_lib_eq < 0 then format(-(v_total_lib_eq),0)
																			else format(v_total_lib_eq,0)
																			end)));

					-- including the unmapped data if we have records else unmapped data will not be included into json
								if (vunmapcount > 0 ) then
									begin
										select JSON_MERGE_PRESERVE(
													(select  JSON_ARRAYAGG(json_object ('name', bg.accountname,'Currgrouptotal', case 
																		when bg.acttotal < 0 then format(-(bg.acttotal),0)
																		else format(bg.acttotal,0) end
														,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname, 'Currsubgrouptotal', case 
																										when sbg.acttotal < 0 then format(-(sbg.acttotal),0)
																										else format(sbg.acttotal,0) end
																, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																													when act.acttotal < 0 then format(-(act.acttotal),0)
																													else format(act.acttotal,0) end
																						))
																			from acttotal act  
																			where act.sbgrpid = fsg.finsubgroupid
																			and fsg.finsubgroupid in (1,7)
																		)
																		))
																from adinovis.finsubgroup fsg 
																join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
																where fg.fingroupid = fsg.fingroupid
																order by fsg.finsubgroupid , fsg.sequenceorder)
				
													)) 
                
													from adinovis.fingroup fg
													join grptotal bg on fg.fingroupname = bg.accountname
													where bsoris = 'Balance Statement'
													order by fg.fingroupid ,fg.sequenceorder 
										),total_l_e,unmapdata)
										as Result;
									end;
								else
											select JSON_MERGE_PRESERVE(
													(select  JSON_ARRAYAGG(json_object ('name', bg.accountname,'Currgrouptotal', case 
																		when bg.acttotal < 0 then format(-(bg.acttotal),0)
																		else format(bg.acttotal,0) end
														,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname, 'Currsubgrouptotal', case 
																										when sbg.acttotal < 0 then format(-(sbg.acttotal),0)
																										else format(sbg.acttotal,0) end
																, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																													when act.acttotal < 0 then format(-(act.acttotal),0)
																													else format(act.acttotal,0) end
																						))
																			from acttotal act  
																			where act.sbgrpid = fsg.finsubgroupid
																			and fsg.finsubgroupid in (1,7)
																		)
																		))
																from adinovis.finsubgroup fsg 
																join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
																where fg.fingroupid = fsg.fingroupid
																order by fsg.finsubgroupid , fsg.sequenceorder)
				
													)) 
                
													from adinovis.fingroup fg
													join grptotal bg on fg.fingroupname = bg.accountname
													where bsoris = 'Balance Statement'
													order by fg.fingroupid ,fg.sequenceorder 
										),total_l_e)
										as Result;
								end if;
            end;
		else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
		end if;
		
else    
	   -- if the incoperation date and trail balance year is differnt, then previous balance is  included into json	 
    set nodate = current_timestamp;   
    
    set checkengsrc = (SELECT IF ( EXISTS ( SELECT 1 FROM adinovis.trailbalance WHERE engagementsid = inengagementid ) ,1,0));
		if ( checkengsrc = 1 ) then 
			begin

			select coalesce(sum(acctcredit),0), coalesce(sum(acctdebit),0)  into crbal , drbal from adinovis.trailbalance where engagementsid=inengagementid and acctyear = (inyear-1);
			
			 select sum(originalbalance) into unmaptotal from tbunmaped;

			 if (crbal > 0 or drbal > 0) then
				begin	  
					 set unmapped_data = (select JSON_ARRAYAGG(json_object( 'Currbal',case 
																when unmaptotal < 0 then format(-(unmaptotal),0)
																else format(unmaptotal,0)
																end, 'Prevbal',0.00)));
				
					  set unmapdata = (select JSON_ARRAYAGG(json_object('name','Unmapped','children',(select JSON_ARRAYAGG(json_object ('name',tbm.accountname,'Currbal',case 
																when tbm.originalbalance < 0 then format(-(tbm.originalbalance),0)
																else format(tbm.originalbalance,0)
																end, 'Prevbal',0.00))
														from tbunmaped tbm),'Currbal',case 
																when unmaptotal < 0 then format(-(unmaptotal),0)
																else format(unmaptotal,0)
																end, 'Prevbal',0.00)));
																	
                
						set total_l_e = (select JSON_ARRAYAGG(json_object ('name','Total liabilities and Equity','CurrmainTotal',case 
																			when v_total_lib_eq < 0 then format(-(v_total_lib_eq),0)
																			else format(v_total_lib_eq,0)
																			end, 'PrevmainTotal',0.00)));
				end;
			else
				begin	  
					 set unmapped_data = (select JSON_ARRAYAGG(json_object( 'Currbal',case 
																when unmaptotal < 0 then format(-(unmaptotal),0)
																else format(unmaptotal,0)
																end)));
				
					  set unmapdata = (select JSON_ARRAYAGG(json_object('name','Unmapped','children',(select JSON_ARRAYAGG(json_object ('name',tbm.accountname,'Currbal',case 
																when tbm.originalbalance < 0 then format(-(tbm.originalbalance),0)
																else format(tbm.originalbalance,0)
																end))
														from tbunmaped tbm),'Currbal',case 
																when unmaptotal < 0 then format(-(unmaptotal),0)
																else format(unmaptotal,0)
																end)));
																	
                
						set total_l_e = (select JSON_ARRAYAGG(json_object ('name','Total liabilities and Equity','CurrmainTotal',case 
																			when v_total_lib_eq < 0 then format(-(v_total_lib_eq),0)
																			else format(v_total_lib_eq,0)
																			end)));
				end;
			end if;


			if (crbal > 0 or drbal > 0) then
				begin	
					if ( vunmapcount > 0 ) then
						begin
							select JSON_MERGE_PRESERVE(
												(select  JSON_ARRAYAGG(json_object ('name', bg.accountname,'Currgrouptotal', case 
																	when bg.acttotal < 0 then format(-(bg.acttotal),0)
																	else format(bg.acttotal,0) end, 'Prevgrouptotal',0.00
													,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname, 'Currsubgrouptotal', case 
																									when sbg.acttotal < 0 then format(-(sbg.acttotal),0)
																									else format(sbg.acttotal,0) end, 'Prevsubgrouptotal',0.00
															, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																												when act.acttotal < 0 then format(-(act.acttotal),0)
																												else format(act.acttotal,0) end, 'Prevtotal', 0.00
																					))
																		from acttotal act  
																	 where act.sbgrpid = fsg.finsubgroupid
																	 and fsg.finsubgroupid in (1,7)
																	 and act.acttotal != 0
																	)
																	))
															from adinovis.finsubgroup fsg 
															join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
															where fg.fingroupid = fsg.fingroupid
															order by fsg.finsubgroupid , fsg.sequenceorder)
				
												)) 
                
												from adinovis.fingroup fg
												join grptotal bg on fg.fingroupname = bg.accountname
												where bsoris = 'Balance Statement'
												order by fg.fingroupid ,fg.sequenceorder 
										),total_l_e,unmapdata)
								as Result;
							end;
						else
							begin
								select JSON_MERGE_PRESERVE(
												(select  JSON_ARRAYAGG(json_object ('name', bg.accountname
														,'Currgrouptotal', case when bg.acttotal < 0 then format(-(bg.acttotal),0) else format(bg.acttotal,0) end
														-- , 'Prevgrouptotal',0.00
													,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname, 'Currsubgrouptotal', case 
																									when sbg.acttotal < 0 then format(-(sbg.acttotal),0)
																									else format(sbg.acttotal,0) end
																									-- , 'Prevsubgrouptotal',0.00
															, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																												when act.acttotal < 0 then format(-(act.acttotal),0)
																												else format(act.acttotal,0) end
																												-- , 'Prevtotal', 0.00
																					))
																		from acttotal act  
																	 where act.sbgrpid = fsg.finsubgroupid
																	 and fsg.finsubgroupid in (1,7)
																	 and act.acttotal != 0
																	)
																	))
															from adinovis.finsubgroup fsg 
															join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
															where fg.fingroupid = fsg.fingroupid
															order by fsg.finsubgroupid , fsg.sequenceorder)
				
												)) 
                
												from adinovis.fingroup fg
												join grptotal bg on fg.fingroupname = bg.accountname
												where bsoris = 'Balance Statement'
												order by fg.fingroupid ,fg.sequenceorder 
										),total_l_e)
								as Result;
							end;
						end if;
				end;
			else
				begin
					if ( vunmapcount > 0 ) then
						begin
							select JSON_MERGE_PRESERVE(
												(select  JSON_ARRAYAGG(json_object ('name', bg.accountname
														,'Currgrouptotal', case when bg.acttotal < 0 then format(-(bg.acttotal),0) else format(bg.acttotal,0) end
														-- , 'Prevgrouptotal',0.00
													,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname
													, 'Currsubgrouptotal', case when bg.acttotal = sbg.acttotal then 0 else																		
																		case when sbg.acttotal < 0 then format(-(sbg.acttotal),0) else format(sbg.acttotal,0) end end
																									-- , 'Prevsubgrouptotal',0.00
															, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																												when act.acttotal < 0 then format(-(act.acttotal),0)
																												else format(act.acttotal,0) end
																												-- , 'Prevtotal', 0.00
																					))
																		from acttotal act  
																	 where act.sbgrpid = fsg.finsubgroupid
																	 and fsg.finsubgroupid in (1,7)
																	 and act.acttotal != 0
																	)
																	))
															from adinovis.finsubgroup fsg 
															join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
															where fg.fingroupid = fsg.fingroupid
															order by fsg.finsubgroupid , fsg.sequenceorder)
				
												)) 
                
												from adinovis.fingroup fg
												join grptotal bg on fg.fingroupname = bg.accountname
												where bsoris = 'Balance Statement'
												order by fg.fingroupid ,fg.sequenceorder 
										),total_l_e,unmapdata)
								as Result;
						end;
					else
						begin
							select JSON_MERGE_PRESERVE(
													(select  JSON_ARRAYAGG(json_object ('name', bg.accountname
															,'Currgrouptotal', case when bg.acttotal < 0 then format(-(bg.acttotal),0) else format(bg.acttotal,0) end
															-- , 'Prevgrouptotal',0.00
														,'children',(select JSON_ARRAYAGG(json_object ('name',sbg.accountname 
																		, 'Currsubgrouptotal', case when bg.acttotal = sbg.acttotal then 0 else																		
																		case when sbg.acttotal < 0 then format(-(sbg.acttotal),0) else format(sbg.acttotal,0) end end
																										-- , 'Prevsubgrouptotal',0.00
																, 'children', (select JSON_ARRAYAGG(json_object ('name', act.accountname,'Currtotal', case 
																													when act.acttotal < 0 then format(-(act.acttotal),0)
																													else format(act.acttotal,0) end
																													-- , 'Prevtotal', 0.00
																						))
																			from acttotal act  
																		 where act.sbgrpid = fsg.finsubgroupid
																		 and fsg.finsubgroupid in (1,7)
																		 and act.acttotal != 0
																		)
																		))
																from adinovis.finsubgroup fsg 
																join subgrptotal sbg on fsg.finsubgroupname = sbg.accountname
																where fg.fingroupid = fsg.fingroupid
																order by fsg.finsubgroupid , fsg.sequenceorder)
				
													)) 
                
													from adinovis.fingroup fg
													join grptotal bg on fg.fingroupname = bg.accountname
													where bsoris = 'Balance Statement'
													order by fg.fingroupid ,fg.sequenceorder 
											),total_l_e)
									as Result;
						end;
					end if;

				end;
			end if;         
            end;
		else
					set errorCode = '99999';
					set errorMessage = 'delete cannot peform on master trail balance record';
		end if;
end if;
END