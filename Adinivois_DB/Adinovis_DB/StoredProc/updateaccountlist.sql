﻿CREATE DEFINER=`admin`@`%` PROCEDURE `updateaccountlist`(IN inengagementsid int(20))
BEGIN

Declare loopstart INT(20);
Declare loopend INT(20);
Declare vfingroupid int(20); 
Declare checkid int(5);


 drop TEMPORARY TABLE if exists countid;
create TEMPORARY TABLE countid(id int(20),fingroupid int(20), isprocessed bit(1));

 drop TEMPORARY TABLE if exists testdata;

 
 create TEMPORARY TABLE testdata as 
select 
a.accountlistdataid,
a.engagementsid,
a.type,
f.fingroupid,
f.fingroupname
 from adinovis.accountlistdata a, adinovis.fingroup f
where a.type like concat('%',left(right(fingroupname,9),6),'%')
 and a.fingroupid is null
 and a.isprocessed is null
 and a.engagementsid  = inengagementsid
order by a.accountlistdataid;

insert into countid(id) select accountlistdataid from adinovis.accountlistdata where  fingroupid is null and 
isprocessed is null and engagementsid  = inengagementsid;

select min(id) into loopstart  from countid;
select max(id) into loopend  from countid;

while loopstart <= loopend
do
begin

select fingroupid into vfingroupid from testdata where accountlistdataid = loopstart;

if vfingroupid is null then

select 
case 
when type in ('Income','Cost of Goods Sold','Other Income') then 5
when type in ('Accounts payable (A/P)','Credit Card') then 2
when type in ('Accounts receivable (A/R)') then 1
when type = 'Bank' and balance > 0 then 2
when type = 'Bank' and balance < 0 then 1
end  into vfingroupid
from 
adinovis.accountlistdata where accountlistdataid = loopstart;

end if;
 
-- select vfingroupid;
update countid set fingroupid = vfingroupid, isprocessed = 1 where id =loopstart;
update adinovis.accountlistdata  set fingroupid = vfingroupid
 , isprocessed = b'1' where accountlistdataid = loopstart;

set vfingroupid = null;
end;
set loopstart = loopstart + 1;

end while;

 -- select * from countid;
 /*update adinovis.accountlistdata  set fingroupid = (select fingroupid from countid where id = accountlistdataid)
 , isprocessed = (select isprocessed from countid where id = accountlistdataid)
 where 1=1;*/
 select 1;
 


END