﻿/********************************************************************************************************************************
** SP Name: updateuserprofile
**
** Purpose: Edit user information.
**
**Parameters: 	Puseraccountid : user account id
**				Pfirstname : first name
**				Plastname:  last name
**				Pcphonenumber : cell phone number
**				Pbphonenumber : business phone number
**				Pmemberlicenseno : license number
**				Pbusinessname : business name
**				Pemailaddress : email address 
**				Paddress : address
**				
**				
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateuserprofile`(IN Puseraccountid INT(20), 
											IN Pfirstname varchar(100),
											IN Plastname varchar(100),
											IN Pcphonenumber varchar(100),
											IN Pbphonenumber varchar(100),
											IN Pmemberlicenseno varchar(100),
											IN Pbusinessname varchar(100),
											IN Pemailaddress varchar(100),
											IN Paddress varchar(100),
                                            IN Pprofilepict varchar(255)
                                            
                                            )
BEGIN
DECLARE vuseraccountid INT(20); 
DECLARE vfirstname varchar(100);
DECLARE vlastname varchar(100);
DECLARE vcphonetype varchar(100);
DECLARE vcphonenumber varchar(100);
DECLARE vbphonetype varchar(100);
DECLARE vbphonenumber varchar(100);
DECLARE vmemberlicenseno varchar(100);
DECLARE vbusinessname varchar(100);
DECLARE vemailaddress varchar(100);
DECLARE vaddress varchar(100);
Declare nodate datetime;

DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET  autocommit=0;	
SET vuseraccountid = Puseraccountid;
SET vfirstname = Pfirstname;
SET vlastname = Plastname;
SET vcphonetype = 'cell phone';
SET vcphonenumber= Pcphonenumber;
SET vbphonetype = 'business phone';
SET vbphonenumber = Pbphonenumber;
SET vmemberlicenseno = Pmemberlicenseno;
SET vbusinessname = Pbusinessname;
SET vemailaddress = Pemailaddress;
SET vaddress = Paddress;
set nodate = current_timestamp;



	INSERT INTO adinovis.useraccount_history(useraccountid,
											firstname,
											middlename,
											lastname,
											business_name,
											emailaddress,
											loginpassword,
											statusid,
											modifiedby,
											modifieddate)
									SELECT useraccountid,
									       firstname,
									       middlename,
									       lastname,
									       businessname,
	                                       emailaddress,
	                                       loginpassword,
	                                       statusid,
										   CAST(useraccountid AS CHAR),
										   nodate
									FROM adinovis.useraccount 
									WHERE useraccountid =  vuseraccountid;
	
	UPDATE adinovis.useraccount SET firstname = vfirstname,
									lastname = vlastname,
									businessname = vbusinessname,
									emailaddress = vemailaddress,
									licenseno = vmemberlicenseno,
									modifiedby = cast(vuseraccountid AS CHAR),
									modifieddate = nodate	
                                    ,profilepicture = Pprofilepict
	WHERE  useraccountid = vuseraccountid;

	
	INSERT INTO adinovis.address_history(addressid,
										useraccountid,
										address,
										city,
										state,
										country,
										postcode,
										modifiedby,
										modifieddate)
								SELECT addressid,
										useraccountid,
										address,
										city,
										state,
										country,
										postcode,
										CAST(useraccountid AS CHAR),
										nodate
								FROM adinovis.address
								WHERE useraccountid =  vuseraccountid;
								
    UPDATE adinovis.address SET address = vaddress, 
								modifiedby = cast(vuseraccountid AS CHAR),
								modifieddate = nodate

	WHERE  useraccountid = vuseraccountid;								
	
	INSERT INTO adinovis.phone_history( phoneid,
										useraccountid,
										phonenumber,
										countrycode,
										phonetype,
										modifiedby,
										modifieddate)
								SELECT phoneid,
	                                   useraccountid,
	                                   phonenumber,
	                                   countrycode,
	                                   phonetype,
									   CAST(useraccountid AS CHAR),
									   nodate
								FROM adinovis.phone
								WHERE useraccountid =  vuseraccountid;
	
	UPDATE adinovis.phone SET phonenumber = vbphonenumber,
							modifiedby = cast(vuseraccountid AS CHAR),
							modifieddate = nodate
	WHERE  useraccountid = vuseraccountid
	AND phonetype = vbphonetype;

	UPDATE adinovis.phone SET phonenumber = vcphonenumber,
							modifiedby = cast(vuseraccountid AS CHAR),
							modifieddate = nodate
	WHERE  useraccountid = vuseraccountid
	AND phonetype = vcphonetype;

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.updateuserprofile',nodate); 
	end if;
END;
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;


END