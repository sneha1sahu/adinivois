﻿/********************************************************************************************************************************
** SP Name: saveengaement
**
** Purpose: Save engagement information into DB for given client.
**
**Parameters: 	Pclientfirmid : client firm id
**				Pengagementname : engagement name 
**				Pclientname : client name 
**				Pengagementid : engagement id
**				Pcompliation : compliation
**				Psubenties : subenties 
**				Pyearend : finanical year end date  
**				Pincorporationdate : Incorporation date
**				Padditionalinfo : additional information
**				Puseraccoutid_roleid_list : auditor and user role list 
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 20-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `saveengaement`(Pclientfirmid int(20),
								Pengagementname varchar(100),
								Pengagementtypeid int(20),
								Pcompliation varchar(100),
								Psubenties varchar(100),
								Pyearend date,
								Padditionalinfo varchar(500),
								Puseraccoutid_roleid_list varchar(4000),
								Ploginid int(20))
BEGIN
Declare vclientfirmid int(20);
Declare vengagementsid int(20);
Declare vuseraccountid int(20);
Declare vcontactperson varchar(100);
Declare vjurisdictionid int(20);
Declare vtypeofentityid int(20);
Declare vengagementname varchar(100);
Declare vclientname varchar(100);
Declare vengagementtypeid int(20);
Declare vcompliation varchar(100);
Declare vsubenties varchar(100);
Declare vyearend date;
Declare vincorporationdate date;
Declare vadditionalinfo varchar(500);
Declare vaccountbook varchar(500);
Declare vaccesskey varchar(500);
Declare vsecretkey varchar(500);
Declare value1 varchar(4000);
Declare nodate date;
Declare checkval int(2);
Declare V_DESIGNATION_acc int(20);
Declare V_DESIGNATION_role int(20);
Declare vuserroleid int(20);
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
	
set vuseraccountid = Ploginid;

SET vclientfirmid = Pclientfirmid;
SET vengagementname = Pengagementname;
SET vengagementtypeid = Pengagementtypeid;
SET vcompliation = Pcompliation;
SET vsubenties=Psubenties;
SET vyearend = Pyearend;
 SET vadditionalinfo = Padditionalinfo;
SET value1 = Puseraccoutid_roleid_list;
SET autocommit = 0;
SET nodate = current_timestamp;
SET checkval = 0;
  
				

	
	INSERT INTO adinovis.engagements(
									  clientfirmid
									, engagementname
                                    , subentitles
                                    , engagementtypeid
                                    , compilationtype
                                    , finanicalyearenddate
                                    , additionalinfo
                                    , statusid
                                    , tbloadstatusid
                                    , isactive
                                    , isdelete
                                    , createdby
                                    , createddate
                                    , modifiedby
                                    , modifieddate    
									)
								Values(
									    vclientfirmid,
										vengagementname,
                                        vsubenties,
										vengagementtypeid,									
										vcompliation,
										vyearend,
                                        vadditionalinfo,
                                        9,
                                        11,
										b'1',
										b'0',
										cast(vuseraccountid as char),
										nodate,
										cast(vuseraccountid as char),
										nodate);
										
				set vengagementsid = (SELECT  LAST_INSERT_ID());
                
         
	
	-- select userroleid from adinovis.auditrole where auditroleid = 2;
BEGIN 
    WHILE (LOCATE(',', value1) > 0) DO -- 406,1,408,2,409,6
    -- select value1;
      SET V_DESIGNATION_acc = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
      SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1); 
    --  select V_DESIGNATION_acc; -- 406,
    
			  if LOCATE(',', value1) = 0 then
			  set V_DESIGNATION_role = value1;
			  else
			  SET V_DESIGNATION_role = SUBSTRING(value1,1, LOCATE(',',value1)-1); 
			  SET value1 = SUBSTRING(value1, LOCATE(',',value1) + 1);
			 -- select V_DESIGNATION_role_seq; -- 1
			  end if;
	  
	  select userroleid into vuserroleid from adinovis.auditrole where auditroleid = V_DESIGNATION_role;
 
	
	INSERT INTO adinovis.clientfirmauditorrole(engagementsid,
												useraccountid,
												userroleid,
                                                auditroleid,
												isactive,
												isdelete,
												createdby,
												createddate,
												modifiedby,
												modifieddate
                                                )
										Values(vengagementsid,
												V_DESIGNATION_acc,
												vuserroleid,
												V_DESIGNATION_role,
												b'1',
												b'0',
												cast(vuseraccountid as char),
												nodate,
												cast(vuseraccountid as char),
												nodate);
												
   END WHILE;
  END;

BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.saveengaement',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END