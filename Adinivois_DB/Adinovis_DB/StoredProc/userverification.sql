﻿CREATE DEFINER=`admin`@`%` PROCEDURE `userverification`(IN emailid varchar(100))
BEGIN
select emailaddress from adinovis.userinformation
			Where emailaddress = emailid
			AND useractive = 1
			AND userdelete = 0
            AND status = 3;
END