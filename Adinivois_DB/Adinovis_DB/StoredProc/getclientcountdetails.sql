﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getclientcountdetails`(Ploginid int(20))
BEGIN


with clientstatus as
(
select distinct
  clientfirmid
, statusid
from adinovis.clientprofile
where  userdelete = 0 and clientfirmdelete = 0 and clientcreatedby = Ploginid
union
SELECT distinct
  cf.clientfirmid
, cf.statusid 
FROM adinovis.clientfirmauditorrole cfr
join adinovis.engagements eg on cfr.engagementsid = eg.engagementsid and eg.isactive = true and eg.isdelete  = false
join adinovis.clientprofile cf on eg.clientfirmid = cf.clientfirmid and cf.userdelete  = false
WHERE  cfr.isactive = true and cfr.isdelete  = false
and cfr.useraccountid  = Ploginid
) 
, engstatus as
(
select distinct
engagementsid
from adinovis.engagements 
where isactive = 1 and isdelete = 0 
and createdby = Ploginid
union
SELECT distinct
eg.engagementsid 
FROM adinovis.clientfirmauditorrole cfr
join adinovis.engagements eg on cfr.engagementsid = eg.engagementsid and eg.isactive = true and eg.isdelete  = false
WHERE  cfr.isactive = true and cfr.isdelete  = false
and cfr.useraccountid  = Ploginid
)
select distinct
  coalesce(count(clientfirmid),0) as totalclient
, (select count(distinct clientfirmid) from clientstatus where statusid = 5) as activeclient
, (select count(distinct clientfirmid) from clientstatus where statusid = 4) as invitependingwithclient
, (select count(distinct engagementsid) from engstatus ) as totalengagement
from clientstatus;

END