﻿/********************************************************************************************************************************
** SP Name: getengagementdetails
**
** Purpose: provide engagement details for given client firm. 
**		
**  
** Parameters:  clientfirmid
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 18-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getengagementdetails`(Pclientfirmid int(20),
																	Ploginid int(20))
BEGIN

select 
ed.engagementsid,
ed.clientfirmid,
cf.useraccountid,
(select c.businessname from adinovis.clientprofile c where c.clientfirmid = cf.clientfirmid) as businessname,
cf.contactperson,
cf.incorporationdate,
ed.engagementname,
ed.subentitles,
ed.engagementtypeid,
ed.engagementtype,
ed.compilationtype,
ed.finanicalyearenddate,
ed.additionalinfo,
ed.engagementstatusid as statusid,
ed.tbloadstatusid ,
(select s.statusname from adinovis.status s where s.statusid = ed.engagementstatusid) as statusname,
		date(ed.createddate) as engagementcreateddate,
		JSON_ARRAYAGG(json_object('clientfirmauditroleid',cfr.clientfirmauditorroleid , 'engagementsid', cfr.engagementsid, 'useraccountid',cfr.useraccountid,'auditroleid',cfr.auditroleid,'rolename',ur.rolename,'fullname',ui.fullname,'rowindex',ar.rowindex)) as auditorrole
, cast(ed.modifieddate as date) as modifieddate
from adinovis.clientprofile cf
join adinovis.engagementdetails ed on ed.clientfirmid = cf.clientfirmid and ed.isactive=1 and ed.isdelete =0
left join adinovis.clientfirmauditorrole cfr on ed.engagementsid = cfr.engagementsid 
join adinovis.auditrole ar on cfr.auditroleid = ar.auditroleid and ar.isactive = 1 and ar.isdelete = 0
join adinovis.userrole ur on ar.userroleid = ur.userroleid and ur.isactive = 1 and ur.isdelete = 0
join adinovis.userinformation ui on cfr.useraccountid = ui.useraccountid 
join adinovis.engagementtype eg on ed.engagementtypeid = eg.engagementtypeid  and eg.isactive = 1 and eg.isdelete = 0
where cf.clientfirmid = Pclientfirmid 
and cf.clientfirmdelete = 0 
and cf.clientcreatedby = Ploginid
union
select 
ed.engagementsid,
ed.clientfirmid,
cf.useraccountid,
(select c.businessname from adinovis.clientprofile c where c.clientfirmid = cf.clientfirmid) as businessname,
cf.contactperson,
cf.incorporationdate,
ed.engagementname,
ed.subentitles,
ed.engagementtypeid,
ed.engagementtype,
ed.compilationtype,
ed.finanicalyearenddate,
ed.additionalinfo,
ed.engagementstatusid as statusid,
ed.tbloadstatusid ,
(select s.statusname from adinovis.status s where s.statusid = ed.engagementstatusid) as statusname,
		date(ed.createddate) as engagementcreateddate,
		JSON_ARRAYAGG(json_object('clientfirmauditroleid',cfr.clientfirmauditorroleid , 'engagementsid', cfr.engagementsid, 'useraccountid',cfr.useraccountid,'auditroleid',cfr.auditroleid,'rolename',ur.rolename,'fullname',ui.fullname,'rowindex',ar.rowindex)) as auditorrole
, cast(ed.modifieddate as date) as modifieddate
from adinovis.clientprofile cf
join adinovis.engagementdetails ed on ed.clientfirmid = cf.clientfirmid and ed.isactive=1 and ed.isdelete =0
left join adinovis.clientfirmauditorrole cfr on ed.engagementsid = cfr.engagementsid 
join adinovis.auditrole ar on cfr.auditroleid = ar.auditroleid and ar.isactive = 1 and ar.isdelete = 0
join adinovis.userrole ur on ar.userroleid = ur.userroleid and ur.isactive = 1 and ur.isdelete = 0
join adinovis.userinformation ui on cfr.useraccountid = ui.useraccountid 
join adinovis.engagementtype eg on ed.engagementtypeid = eg.engagementtypeid  and eg.isactive = 1 and eg.isdelete = 0
where cf.clientfirmid = Pclientfirmid 
and cf.clientfirmdelete = 0 
and cfr.useraccountid = Ploginid
order by modifieddate desc;




	 
END