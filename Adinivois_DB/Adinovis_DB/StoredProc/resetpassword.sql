﻿/********************************************************************************************************************************
** SP Name: resetpassword
**
** Purpose: Reset the user email password after forgot passowrd.
**
**Parameters: 	tokenkey
**				new password
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 20-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `resetpassword`(IN tokenkey varchar(100), IN newpassword varchar(100))
BEGIN

/* Variable Declaration*/
DECLARE vuseraccountid INT(20);
DECLARE vemailid INT(20);
Declare vtokenkey varchar(100);
DECLARE vnewpassword varchar(100);
DECLARE nodate datetime;
DECLARE checkmailid INT(2);
DECLARE tokencheck INT(2);
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
 

/* set variables value*/
SET checkmailid = 0;
SET tokencheck = 0;
set vtokenkey = tokenkey;
set nodate = current_timestamp;
SET  autocommit=0; 
call adinovis.savepassword(newpassword, @pwd);
SET vnewpassword = @pwd;  
    


select 1,useraccountid into tokencheck,vuseraccountid from adinovis.useraccountauth  where tokenvalue = vtokenkey  and  isactive = 1 and  isdelete= 0 and tokentypeid = 2;

 select 1 into checkmailid from adinovis.useraccount where useraccountid = vuseraccountid and  isactive = 1 and  isdelete= 0 and statusid = 2;


/* update the new password when user exist*/
 IF checkmailid = 1 AND tokencheck = 1  THEN
		UPDATE adinovis.useraccount SET loginpassword = vnewpassword,
										statusid = 3,
										modifiedby = cast(vuseraccountid AS CHAR),
										modifieddate = nodate
		WHERE useraccountid = vuseraccountid;

		UPDATE adinovis.useraccountauth SET tokentypeid = 3,
										modifiedby = cast(vuseraccountid AS CHAR),
										modifieddate = nodate
		WHERE useraccountid = vuseraccountid;
	else
		set errorCode = '99999';
        set errorMessage = 'token invalid';


END IF;


COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
    
END