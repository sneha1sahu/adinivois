﻿CREATE DEFINER=`admin`@`%` PROCEDURE `loadjsonintotrailbalance`()
BEGIN

		Declare loopstart INT(20);
		Declare loopend INT(20);
		Declare loopclientapidataid bigint(20);


		drop TEMPORARY TABLE if exists procesjson;

		CREATE TEMPORARY TABLE procesjson
		( tlid bigint NOT NULL AUTO_INCREMENT
		,  utclientapidataid bigint null
		, isprocessed bit(1)
		, PRIMARY KEY (`tlid`)
		)AUTO_INCREMENT=0 ;
        
        
			insert into procesjson
			(utclientapidataid ,isprocessed)
			select 
			  cad.clientapidataid
			, cad.isprocessed
			from adinovis.clientapidata cad
			where cad.recieveddata not in ('null')
			and cad.isprocessed = 0;

			select min(tlid) into loopstart  from procesjson;
			select max(tlid) into loopend  from procesjson;
            
		if ( loopstart > 0 ) then
			while loopstart <= loopend
				do
					begin
						select utclientapidataid into loopclientapidataid from procesjson where tlid = loopstart;
                        
                        call adinovis.extractjsontotable(loopclientapidataid);
						call adinovis.extractjsontoaccountlist(loopclientapidataid);
                       
						update adinovis.clientapidata
							set isprocessed = b'1'
							, modifieddate = current_timestamp
                            where clientapidataid = loopclientapidataid;                
                        
                        set loopclientapidataid = null;
					end;
				set loopstart = loopstart +1;
			end while;
		end if;

	call adinovis.updateaccountlist();

END