﻿
/********************************************************************************************************************************
** SP Name: deleteengagement
**
** Purpose: Validate that client is exist and update all engagement data isactive to 0 and 
**          isdelete to 1.
**
** Parameters: 
**             clientfirmid			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 17-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `deleteengagement`(Pengagementsid int(20)
, ploginid int(20) )
BEGIN

Declare vengagementsid int(20);
Declare vclientfirmid int(20);
Declare vuseraccountid int(20);
Declare nodate datetime;
Declare checkmailid INT(2);
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;
set 	vuseraccountid = 0;
SET checkmailid = 0;
SET vengagementsid = Pengagementsid;
set nodate = current_timestamp;
SET  autocommit=0;

select 1, clientfirmid into checkmailid, vclientfirmid from engagements where engagementsid = vengagementsid and  isactive = 1 and  isdelete= 0; 

select useraccountid into vuseraccountid  from adinovis.clientfirm where clientfirmid = vclientfirmid
and  isactive = 1 and  isdelete= 0;



 if checkmailid = 1  then
			update adinovis.engagements set isactive =0 , 
								isdelete = 1,
                                modifiedby = cast(ploginid AS CHAR),
								modifieddate = nodate
                                where engagementsid = vengagementsid;     
else 
set errorCode = '99999';
set errorMessage = 'user invalid';
end if;

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.deleteengagement',nodate); 
	end if;
END;

COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END