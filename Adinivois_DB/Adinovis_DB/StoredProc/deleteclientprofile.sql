﻿/********************************************************************************************************************************
** SP Name: deleteclientprofile
**
** Purpose: Validate that client is exist and update isactive to 0 and isdelete to 1.
**
** Parameters: 
**             clientfirmid			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 12-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteclientprofile`(Pclientfirmid int(20),
																	Ploginid int(20))
BEGIN
Declare vclientfirmid int(20);
Declare vuseraccountid int(20);
Declare nodate datetime;
Declare checkmailid INT(2);
Declare tokencheck int(2);
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET checkmailid = 0;
SET tokencheck = 0;
SET vclientfirmid = Pclientfirmid;
set nodate = current_timestamp;
SET  autocommit=0;

select 1 into checkmailid from adinovis.clientfirm where clientfirmid = vclientfirmid
and  isactive = 1 and  isdelete= 0;

select useraccountid into vuseraccountid from adinovis.clientfirm where clientfirmid = vclientfirmid
and  isactive = 1 and  isdelete= 0;


/*Select 1 into tokencheck from adinovis.useraccountauth  where useraccountid = vuseraccountid  
and  isactive = 1 and  isdelete= 0;*/

 -- if checkmailid =1 and tokencheck = 1 then

 if checkmailid = 1  then

update adinovis.useraccount set isactive = 0, 
								isdelete = 1,
                                modifiedby = cast(Ploginid AS CHAR),
								modifieddate = nodate
                                where useraccountid = vuseraccountid;

update adinovis.clientfirm set isactive =0 , 
								isdelete = 1,
                                modifiedby = cast(Ploginid AS CHAR),
								modifieddate = nodate
                                where clientfirmid = vclientfirmid;
                                
                            -- select vuseraccountid;
else                            
set errorCode = '99999';
set errorMessage = 'user invalid';
end if;

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.deleteclientprofile',nodate); 
	end if;
END;

COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END