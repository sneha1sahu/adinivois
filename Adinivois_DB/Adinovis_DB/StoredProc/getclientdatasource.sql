﻿/********************************************************************************************************************************
** SP Name: getclientdatasource
**
** Purpose: provide Quick Books or Xero data for given clinetid. 
**		
** Parameters: clinetid
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 20-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `getclientdatasource`(IN Pclientid varchar(255))
BEGIN
SELECT 
cd.clientdatasourceid,
cd.clientfirmid, 
c.useraccountid,
cd.clientid, 
cd.secretkey,
date_add(DATE_SUB(c.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
c.finanicalyearenddate  as enddate,
year(c.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken
from adinovis.clientfirm c, adinovis.clientdatasource cd
where c.clientfirmid = cd.clientfirmid
and cd.clientid = Pclientid
and( cd.sourcelink like '%quick%' )
union
SELECT 
cd.clientdatasourceid,
cd.clientfirmid,
c.useraccountid, 
cd.clientid, 
cd.secretkey,
date_add(DATE_SUB(c.finanicalyearenddate,INTERVAL 1 year), INTERVAL 1 day) as startdate,
c.finanicalyearenddate  as enddate,
year(c.finanicalyearenddate) as accountyear
,cd.sourcelink,
cd.realmid,
cd.refreshtoken
from adinovis.clientfirm c, adinovis.clientdatasource cd
where c.clientfirmid = cd.clientfirmid
and cd.clientid = Pclientid
and( cd.sourcelink like '%xero%' )
and cd.clientfirmid >1;

END