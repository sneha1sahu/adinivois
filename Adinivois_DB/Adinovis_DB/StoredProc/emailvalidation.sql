﻿/********************************************************************************************************************************
** SP Name: emailvalidation
**
** Purpose: Validate that user is exist 
**
** Parameters: 
**             emailid			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `emailvalidation`(IN emailid varchar(100))
BEGIN

/* Variable Declaration*/
Declare checkemail int(2);

/* set variables value*/
set checkemail =0;
select 1 into checkemail  from adinovis.useraccount where emailaddress = emailid;

/* if user exsit then send the emailid */
if checkemail = 1 then
select emailid;
END IF;
END