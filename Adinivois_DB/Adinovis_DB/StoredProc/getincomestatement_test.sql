﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getincomestatement_test`(inengagementid int(20), inyear int(20))
BEGIN

DECLARE tbmapcnt int(20);
DECLARE statementtype int;
DECLARE exppy1sum int(20);
DECLARE expgrp varchar(4000);
DECLARE revgrp varchar(4000);
DECLARE total_rev varchar(4000);
DECLARE total_ex varchar(4000);
DECLARE rev_ex varchar(4000);
DECLARE other_ex_data varchar(4000);
DECLARE before_in_tax varchar(4000);
DECLARE taxes varchar(4000);
DECLARE total_income_data varchar(4000);
DECLARE staring_deficit_data varchar(4000);
DECLARE deficit_end_data varchar(4000);
DECLARE total_rev_prev varchar(4000);
DECLARE total_ex_prev varchar(4000);


drop TEMPORARY TABLE if exists tbloadeng;
drop TEMPORARY TABLE if exists texpenses;
drop TEMPORARY TABLE if exists texpgrp;
drop TEMPORARY TABLE if exists trevenue;
drop TEMPORARY TABLE if exists trevgrp;

CREATE TEMPORARY TABLE tbloadeng
( tlid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, finsubgroupchildId int
, finsubgroupchildname varchar(255)
, statementtypeid int
, statementtypename varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PY2 varchar(255)
, acctyear varchar(255)
,  engagementsid	int
, accountname varchar(255)
, PRIMARY KEY (`tlid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE texpenses
( texpensesid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, finsubgroupchildId int
, finsubgroupchildname varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`texpensesid`)
)AUTO_INCREMENT=0;


 CREATE TEMPORARY TABLE texpgrp
( texpgrpid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`texpgrpid`)
)AUTO_INCREMENT=0;

CREATE TEMPORARY TABLE trevenue
( trevenueid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finsubgrpid int
, finsubgroupname varchar(255)
, finsubgroupchildId int
, finsubgroupchildname varchar(255)
, originalbalance varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, chldid varchar(255)
,  sequenceorder	int
, PRIMARY KEY (`trevenueid`)
)AUTO_INCREMENT=0;

 CREATE TEMPORARY TABLE trevgrp
( tprevgrpid bigint NOT NULL AUTO_INCREMENT
, fingroupid int
, fingroupname varchar(255)
, finalamount varchar(255)
, PY1 varchar(255)
, PRIMARY KEY (`tprevgrpid`)
)AUTO_INCREMENT=0;




-- balance sheet
set statementtype = 2;


-- coding start

-- validating if we have trail balance mapping for balance sheet
select count(tbm.trailbalanceid) into tbmapcnt from adinovis.trailbalancemaping tbm
join adinovis.trailbalance tb on tbm.trailbalanceid = tb.trailbalanceid and tb.isactive  = 1 and tb.isdelete = 0
and tb.engagementsid=inengagementid and tb.acctyear = inyear
where tbm.isactive  = 1 and tbm.isdelete = 0;


if ( tbmapcnt > 0 ) then
	begin

		
				insert into tbloadeng
                (fingroupid, fingroupname, finsubgrpid, finsubgroupname, finsubgroupchildId, finsubgroupchildname,statementtypeid 
					,statementtypename,originalbalance, finalamount,PY1,PY2,acctyear,engagementsid,accountname)
				select 
					vt.fingroupid
                , 	vt.fingroupname
                ,	vt.finsubgrpid
                ,	vt.finsubgroupname
                , 	vt.finsubgroupchildId
                ,	vt.finsubgroupchildname
                , 	vt.statementtypeid
                ,	vt.statementtypename
                ,	coalesce(vt.originalbalance,0) as originalbalance
                , 	coalesce(vt.finalamount,0) as finalamount
                , 	coalesce(vt.PY1,0) as PY1
				,	vt.PY2
                ,	vt.acctyear
                ,	vt.engagementsid  
                ,	vt.accountname
				from adinovis.vtbmapping vt
				where vt.engagementsid = inengagementid
				and vt.acctyear = inyear;
				
			--  Expenses process start
				
				
				insert into texpenses
						(  fingroupid 
						, fingroupname 
						, finsubgrpid 
						, finsubgroupname 
                        , finsubgroupchildId
                        , finsubgroupchildname
						, originalbalance 
						, finalamount 
						, PY1 
                        , chldid
						, sequenceorder	
						)
						select 
						  t.fingroupid
						, t.fingroupname
						, t.finsubgrpid
						, t.finsubgroupname
                        , t.finsubgroupchildId
                        , t.finsubgroupchildname
						, coalesce(sum(t.originalbalance),0) as originalbalance
						, coalesce(sum(t.finalamount),0)  as finalamount
						, coalesce(sum(t.PY1),0)  as PY1
						, concat(t.fingroupid,'.',t.finsubgrpid,'.',fsg.sequenceorder) as chldid
						, fsg.sequenceorder
						from tbloadeng t
						join finsubgroup fsg on t.fingroupid = fsg.fingroupid and t.finsubgrpid = fsg.finsubgroupid
						where t.fingroupid = 4
						 group by t.finsubgroupchildId
						order by fsg.sequenceorder;
					
					
				insert into texpgrp
					(  fingroupid 
					, fingroupname 
					, finalamount 
					, PY1 
					)
					select 
                      fingroupid
                    , fingroupname
					, coalesce(sum(finalamount),0)
					, coalesce(sum(PY1) ,0)
                    from tbloadeng
                    where fingroupid = 4;
					
			
			
			select coalesce(sum(PY1),0) into exppy1sum from tbloadeng where fingroupid is not null;
			
			if ( exppy1sum > 0 ) then
						begin 
							set expgrp = (select JSON_ARRAYAGG(json_object
										('id',	t.fingroupid
                                            , 'name', t.fingroupname
                                            , 'Currgrouptotal', format(t.finalamount,0)
											, 'Prevgrouptotal' ,format(t.PY1,0)
                                            , 'children'     
                                            ,(
                                            select JSON_ARRAYAGG(json_object
										('id',	ct.fingroupid
										, 'chldid', ct.chldid
										, 'name', ct.finsubgroupchildname
										, 'Currbal' ,case when ct.finalamount < 0 then concat('(',format(-(ct.finalamount),0),')')
																			else format(ct.finalamount,0) end
										, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
										)) 
										from texpenses ct
                                        where ct.fingroupid = t.fingroupid
										order by ct.sequenceorder
                                            )
										))
										from texpgrp t);  
						end;
					else
						begin
						set expgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', format(t.finalamount,0)
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupchildname
											, 'Currbal' ,case when ct.finalamount<0 then concat('(',format(-(ct.finalamount),0),')') else format(ct.finalamount ,0) end
											
											)) 
											from texpenses ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from texpgrp t);     
						end;
					end if;
						
			--  Expenses process end
			
			--  Revenue process start
				
				
				insert into trevenue
						(  fingroupid 
						, fingroupname 
						, finsubgrpid 
						, finsubgroupname
                         , finsubgroupchildId
                        , finsubgroupchildname
						, originalbalance 
						, finalamount 
						, PY1 
                        , chldid
						, sequenceorder	
						)
						select 
						  t.fingroupid
						, t.fingroupname
						, t.finsubgrpid
						, t.finsubgroupname
                        , t.finsubgroupchildId
                        , t.finsubgroupchildname
						, coalesce(sum(t.originalbalance),0) as originalbalance
						, coalesce(sum(t.finalamount),0)  as finalamount
						, coalesce(sum(t.PY1),0)  as PY1
						, concat(t.fingroupid,'.',t.finsubgrpid,'.',fsg.sequenceorder) as chldid
						, fsg.sequenceorder
						from tbloadeng t
						join finsubgroup fsg on t.fingroupid = fsg.fingroupid and t.finsubgrpid = fsg.finsubgroupid
						where t.fingroupid = 5
						 group by t.finsubgroupchildId
						order by fsg.sequenceorder;
						
				
				
				insert into trevgrp
					(  fingroupid 
					, fingroupname 
					, finalamount 
					, PY1 
					)
					select 
                      fingroupid
                    , fingroupname
					, coalesce(sum(finalamount),0)
					, coalesce(sum(PY1) ,0)
                    from tbloadeng
                    where fingroupid = 5;
					
				
				
			if ( exppy1sum > 0 ) then
						begin 
							set revgrp = (select JSON_ARRAYAGG(json_object
										('id',	t.fingroupid
                                            , 'name', t.fingroupname
                                            , 'Currgrouptotal', format(t.finalamount,0)
											, 'Prevgrouptotal' ,format(t.PY1,0)
                                            , 'children'     
                                            ,(
                                            select JSON_ARRAYAGG(json_object
										('id',	ct.fingroupid
										, 'chldid', ct.chldid
										, 'name', ct.finsubgroupchildname
										, 'Currbal' ,case when ct.finalamount<0 then concat('(',format(-(ct.finalamount),0),')') else format(ct.finalamount ,0) end
										, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
										)) 
										from trevenue ct
                                        where ct.fingroupid = t.fingroupid
										order by ct.sequenceorder
                                            )
										))
										from trevgrp t);  
						end;
					else
						begin
						set revgrp = (select JSON_ARRAYAGG(json_object
											('id',	t.fingroupid
                                             , 'name', t.fingroupname
                                             , 'Currgrouptotal', format(t.finalamount,0)
                                             , 'children'     
                                             ,(
                                             select JSON_ARRAYAGG(json_object
											('id',	ct.fingroupid
											, 'chldid', ct.chldid
											, 'name', ct.finsubgroupchildname
											, 'Currbal' ,case when ct.finalamount<0 then concat('(',format(-(ct.finalamount),0),')') else format(ct.finalamount ,0) end								
											)) 
											from trevenue ct
                                            where ct.fingroupid = t.fingroupid
											order by ct.sequenceorder
                                             )
											))
											from trevgrp t);     
						end;
					end if;
				
				--  Revenue process end
				
						set total_rev =(select sum(finalamount) from tbloadeng where fingroupid = 5);
						set total_ex = (select sum(finalamount) from tbloadeng where fingroupid = 4);
						
						set total_rev_prev =(select sum(PY1) from tbloadeng where fingroupid = 5);
						set total_ex_prev = (select sum(PY1) from tbloadeng where fingroupid = 4);
				
				if ( exppy1sum > 0 ) then
						
					begin 
						-- Revenue - Expenses
					
						
						with sre as
									 (
										select 
										 6 as id
									   , 'Earnings (Loss) before undernoted' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)as final
									   ,  coalesce(total_rev_prev,0)- coalesce(total_ex_prev,0)  as PY1
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into rev_ex
									from sre ct;
									
						
						-- Other Expenses
						
						with othex as
									 (
										select 
										 7 as id
									   , 'Other Expenses' as nme
									   , sum(finalamount)as final
									   ,  sum(PY1)  as py1
									   from tbloadeng
										where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into other_ex_data
									from othex ct;
						
						-- before income tax
						
						with btax as
									 (
										select 
										 8 as id
									   , 'Earnings (Loss) before Income Tax' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0) as final
									   ,  coalesce(total_rev_prev,0)- coalesce(total_ex_prev,0) - coalesce(sum(PY1),0)  as py1
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into before_in_tax
									from btax ct;
									
						-- taxes				
						
						with tax as
									 (
										select 
										 9 as id
									   , 'Provision for (recovery of) income taxes' as nme
									   , 0 as final
									   , 0 as py1
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into taxes
									from tax ct;
									
									
						-- net earings or loss
						
						with nel as
									 (
										select 
										 10 as id
									   , 'Net Earnings (Loss)' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0)-0 as final
									   ,  coalesce(total_rev_prev,0)- coalesce(total_ex_prev,0) - coalesce(sum(PY1),0)-0  as py1
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into total_income_data
									from nel ct;
									
									
						-- deficit begining of the year
									
						with defstart as
									 (
										select 
										 11 as id
									   , 'Retained earnings (deficit) beginning of year' as nme
									   , 0 as final
									   , 0  as py1
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into staring_deficit_data
									from defstart ct;
									
						-- deficit end of the year
						
						with defend as
									 (
										select 
										 12 as id
									   , 'Deficit, end of the year' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0)-0 as final
									   ,  coalesce(total_rev_prev,0)- coalesce(total_ex_prev,0) - coalesce(sum(PY1),0)-0  as py1
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									, 'Prevbal' ,case when ct.PY1<0 then concat('(',format(-(ct.PY1),0),')') else format(ct.PY1 ,0) end
									 ))
									 into deficit_end_data
									from defend ct;
					end;
				else
					begin 
					
					-- Revenue - Expenses
						
						
						with sre as
									 (
										select 
										 6 as id
									   , 'Earnings (Loss) before undernoted' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)as final
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into rev_ex
									from sre ct;
									
						
						-- Other Expenses
						
						with othex as
									 (
										select 
										 7 as id
									   , 'Other Expenses' as nme
									   , coalesce(sum(finalamount),0) as final
									   from tbloadeng
										where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into other_ex_data
									from othex ct;
						
						-- before income tax
						
						with btax as
									 (
										select 
										 8 as id
									   , 'Earnings (Loss) before Income Tax' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0) as final
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into before_in_tax
									from btax ct;
									
						-- taxes				
						
						with tax as
									 (
										select 
										 9 as id
									   , 'Provision for (recovery of) income taxes' as nme
									   , 0 as final
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into taxes
									from tax ct;
									
									
						-- net earings or loss
						
						with nel as
									 (
										select 
										 10 as id
									   , 'Net Earnings (Loss)' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0)-0 as final
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into total_income_data
									from nel ct;
									
									
						-- deficit begining of the year
									
						with defstart as
									 (
										select 
										 11 as id
									   , 'Retained earnings (deficit) beginning of year' as nme
									   , 0 as final
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into staring_deficit_data
									from defstart ct;
									
						-- deficit end of the year
						
						with defend as
									 (
										select 
										 12 as id
									   , 'Deficit, end of the year' as nme
									   , coalesce(total_rev,0) - coalesce(total_ex,0)-  coalesce(sum(finalamount),0)-0 as final
									   from tbloadeng
									   where finsubgroupchildname = 'Other expenses'
									 )
									select JSON_ARRAYAGG(json_object 
									('id',	ct.id
									, 'name', ct.nme
									, 'Currbal' ,case when ct.final<0 then concat('(',format(-(ct.final),0),')') else format(ct.final ,0) end
									 ))
									 into deficit_end_data
									from defend ct;
					end;
				end if;
				
	begin
     select JSON_MERGE_PRESERVE(revgrp,expgrp,rev_ex,other_ex_data,before_in_tax,taxes,total_income_data,
								staring_deficit_data,deficit_end_data) as result;    
    end;
				
				
					
end;
end if;			
END