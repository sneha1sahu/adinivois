﻿/********************************************************************************************************************************
** SP Name: changeuserpassword
**
** Purpose: Auditor can change his password.
**
** Parameters: 
**             useraccountid,	  	
**             oldpassword,
**			   newpassword.			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 07-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `changeuserpassword`(IN Puseraccountid INT(20),IN oldpassword varchar(100), IN newpassword varchar(100))
BEGIN
/* Variable declaration*/
DECLARE vuseraccountid INT(20);
DECLARE voldpassword varchar(100);
DECLARE vnewpassword varchar(100);
DECLARE nodate datetime;
DECLARE checkmailid INT(2);
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	/* error handeling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variable value*/
SET checkmailid = 0;	
SET vuseraccountid = Puseraccountid;
 SET vnewpassword = newpassword;
set nodate = current_timestamp;
SET  autocommit=0;
call adinovis.savepassword(newpassword, @pwd);
SET vnewpassword = @pwd;


select 1, loginpassword into checkmailid, voldpassword from adinovis.useraccount where useraccountid = vuseraccountid;

/* if user exist and old password match then update new password*/
IF checkmailid = 1 and   (concat('0x0200', (sha1(oldpassword)))  = voldpassword) THEN
	
UPDATE adinovis.useraccount SET loginpassword = vnewpassword,
								statusid = 3,
								modifiedby = cast(vuseraccountid AS CHAR),
								modifieddate = nodate
WHERE  useraccountid = vuseraccountid;
else
set errorCode = '99999';
         set errorMessage = 'user invalid';
END IF;

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.changepassword',nodate); 
	end if;
END;



COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END