﻿CREATE DEFINER=`admin`@`%` PROCEDURE `test0620`(inengagementsid bigint)
BEGIN
begin
	Declare loopstart bigint(20);
	Declare loopend bigint(20);
	Declare vaccountname varchar(1000);
	Declare vmapsheetid varchar(10);
	Declare vparentid varchar(100);
	Declare vmapsheetname varchar(1000);
    Declare vfinsubgroupchildId varchar(100);
    Declare vfinsubgroupchildname varchar(100);
	Declare vmapno varchar(100);
    Declare vtrailbalanceid bigint;
    Declare nodate date;
    Declare vbaltypeid  bigint;
    Declare mapsheetcount bigint;
   Declare vaccounttype varchar(20);
Declare vasset varchar(50);
Declare vexpense varchar(50);
Declare vrevenue varchar(50);
Declare vequity varchar(50);
Declare vliability varchar(50);
Declare voci varchar(50);
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

set vasset = 'Asset';
set vexpense = 'Expenses';
set vrevenue = 'Revenue';
set vequity = 'Equity';
set vliability = 'Liability';
set voci = 'OCI';

   set vmapsheetid = null;
    set vparentid = null;
    set vmapsheetname =null;
    set vmapno = null;

SET nodate = current_timestamp;
 drop TEMPORARY TABLE if exists tableloop;

CREATE TEMPORARY TABLE tableloop
	(  tlid bigint NOT NULL AUTO_INCREMENT
	,  trailbalid bigint null
	, accountname varchar(4000) null
	, accounttype varchar(4000) null
	, baltypeid int null
	, mapshetid bigint null
     , PRIMARY KEY (tlid)
	)AUTO_INCREMENT=0 ;
    
    insert into tableloop
		(trailbalid, accountname, accounttype, baltypeid)
		SELECT trailbalanceid
		, accountname
		, accounttype
		, case when acctcredit > 0 then 1 else 2 end  as balancetype
		FROM adinovis.trailbalance
        where  engagementsid = inengagementsid
        order by accountname;
SELECT   MIN(tlid) INTO loopstart FROM   tableloop;
SELECT   MAX(tlid) INTO loopend FROM     tableloop;

while loopstart <= loopend
		do
			select trailbalid,accountname,accounttype,baltypeid into vtrailbalanceid,vaccountname,vaccounttype,vbaltypeid from tableloop where tlid = loopstart;
	
    if vbaltypeid = 1 then

select map.finsubgroupchildid,fsgc.finsubgroupchildname,map.mapsheetname,map.mapsheetid 
into vfinsubgroupchildId,vfinsubgroupchildname,vmapsheetname,vmapsheetid
from adinovis.mapsheet map,
adinovis.finsubgroupchild fsgc,adinovis.finsubgroup fsg,adinovis.fingroup fg
where 
map.finsubgroupchildid = fsgc.finsubgroupchildid
and fsgc.finsubgroupid = fsg.finsubgroupid
and fsg.fingroupid = fg.fingroupid
and (fg.fingroupname = vrevenue or fg.fingroupname = vequity or fg.fingroupname = vliability)
/*and (fg.fingroupname = vrevenue or fg.fingroupname = vexpense)*/
and match(map.mapsheetname) against(vaccountname in natural language mode) limit 1;
end if;

  if vbaltypeid = 2 then
  
  select map.finsubgroupchildid,fsgc.finsubgroupchildname,map.mapsheetname,map.mapsheetid 
into vfinsubgroupchildId,vfinsubgroupchildname,vmapsheetname,vmapsheetid
from adinovis.mapsheet map,
adinovis.finsubgroupchild fsgc,adinovis.finsubgroup fsg,adinovis.fingroup fg
where 
map.finsubgroupchildid = fsgc.finsubgroupchildid
and fsgc.finsubgroupid = fsg.finsubgroupid
and fsg.fingroupid = fg.fingroupid
and (fg.fingroupname = vasset or fg.fingroupname = vexpense)
and match(map.mapsheetname) against(vaccountname in natural language mode) limit 1;
 
 end if;



update tableloop set mapshetid = vmapsheetid
where trim(accountname) = trim(vaccountname);

 
    
    INSERT INTO adinovis.trailbalancemaping_test
(
trailbalanceid,
mapsheetid,
isactive,
isdelete,
createdby,
createddate,
modifiedby,
modifieddate)
VALUES
(
vtrailbalanceid,
ifnull(vmapsheetid,0),
b'1',
b'0',
1,
nodate,
1,
nodate
);

set vmapsheetid = null;
    set vparentid = null;
    set vmapsheetname =null;
    set vmapno = null;
    
    set loopstart = loopstart + 1;
	end while;
 
select * from tableloop;
      end;      

  BEGIN    
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.test0620',current_timestamp);
	end if;
    END;    
COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
     end;
END