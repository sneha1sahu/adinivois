﻿/********************************************************************************************************************************
** SP Name: forgotpassword
**
** Purpose: when user click forgetpassowrd from UI status will update as forget password. 
**
** Parameters: 
**             emailid			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 15-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `forgotpassword`(IN emailid varchar(100))
BEGIN

/* Variable Declaration*/
DECLARE vemailid varchar(100);
Declare checkmailid INT(2);
Declare nodate datetime;
Declare vuseraccountid int(20);

DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

/* Error handling*/
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

/* set variables value*/	
SET vemailid = emailid;
set nodate = current_timestamp;
SET  autocommit=0;
SET checkmailid =0;

SELECT 1,useraccountid into checkmailid, vuseraccountid from adinovis.useraccount  WHERE  emailaddress = vemailid;

IF checkmailid = 1 THEN
/*if user exist then update status as forgetpassowrd*/
UPDATE adinovis.useraccount SET statusid = 2 ,
								modifiedby = cast(vuseraccountid AS CHAR),
								modifieddate = nodate
WHERE  emailaddress = vemailid;

update adinovis.useraccountauth set tokentypeid = 2,
								modifiedby = cast(vuseraccountid AS CHAR),
								modifieddate = nodate
WHERE useraccountid = vuseraccountid;
	else
		set errorCode = '99999';
		set errorMessage = 'token invalid';


END IF;

/*IF checkmailid = 0 THEN

SELECT 'User Not Exist';

END IF; */

BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.forgotpassword',current_timestamp); 
	end if;
END;

COMMIT;

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END