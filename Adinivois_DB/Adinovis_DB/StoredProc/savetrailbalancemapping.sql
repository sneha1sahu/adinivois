﻿CREATE DEFINER=`admin`@`%` PROCEDURE `savetrailbalancemapping`(IN ptrailbalanceid  INT(20), IN pmapsheetid INT(20), ploginid int(5))
BEGIN

DECLARE chkmappingrecordexists INT(5);
Declare nodate date;
DECLARE errorCode CHAR(5) DEFAULT '00000';
DECLARE errorMessage TEXT DEFAULT '';

DECLARE continue HANDLER FOR SQLEXCEPTION 
BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET  autocommit=0;
SET nodate = current_timestamp;

set chkmappingrecordexists = if( exists (SELECT 1 FROM adinovis.trailbalancemaping where trailbalanceid = ptrailbalanceid),1,0);

begin
if (chkmappingrecordexists = 1)  then 

INSERT INTO adinovis.trailbalancemaping_history(
trailbalanceid
, mapsheetid
, gificode
, isactive
, isdelete
, createdby
, createddate
, modifiedby
, modifieddate
, historydate
)
select 
trailbalanceid
, mapsheetid
, gificode
, isactive
, isdelete
, createdby
, createddate
, modifiedby
, modifieddate
, nodate
from adinovis.trailbalancemaping
where trailbalanceid = ptrailbalanceid;


update adinovis.trailbalancemaping
set mapsheetid = pmapsheetid 
, modifiedby = ploginid
, modifieddate = nodate
where trailbalanceid= ptrailbalanceid;

else 

INSERT INTO adinovis.trailbalancemaping(
trailbalanceid
, mapsheetid
, gificode
, isactive
, isdelete
, createdby
, createddate
, modifiedby
, modifieddate
) 
values 
( ptrailbalanceid
,pmapsheetid
,null
,b'0'
,b'1'
,ploginid
,nodate
,ploginid
,nodate);

end if ;
end;

BEGIN
	IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.savetrailbalancemapping_test',nodate); 
	end if;
    END;    
COMMIT; 

set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;

END