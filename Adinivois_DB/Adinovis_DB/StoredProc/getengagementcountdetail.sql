﻿CREATE DEFINER=`root`@`localhost` PROCEDURE `getengagementcountdetail`(Ploginid int(20))
BEGIN
with engcnt as 
(
select distinct
 eg.engagementsid
,eg.statusid
from adinovis.engagements eg
where isactive = 1 and isdelete = 0 
and createdby = Ploginid
union
SELECT distinct
 eg.engagementsid 
,eg.statusid
FROM adinovis.clientfirmauditorrole cfr
join adinovis.engagements eg on cfr.engagementsid = eg.engagementsid and eg.isactive = true and eg.isdelete  = false
join adinovis.clientfirm cf on eg.clientfirmid = cf.clientfirmid and cf.isactive = true and cf.isdelete  = false
WHERE  cfr.isactive = true and cfr.isdelete  = false
and cfr.useraccountid  = Ploginid and cf.createdby != Ploginid
)
select distinct
    0 as totalclient
, count( engagementsid ) as totalengagement
, (select count(engagementsid) from engcnt where statusid in (7,9) ) as current
, (select count(engagementsid) from engcnt where statusid in (8) ) as completed
from engcnt;
END