﻿/********************************************************************************************************************************
** SP Name: getallAuditor
**
** Purpose: provide list of Auditor. 
**
** Parameters: 
**             			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 17-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getallAuditor`()
BEGIN
select 
  useraccountid
, fullname
, emailaddress
, useraccountroleid
, userroleid
, rolename
from adinovis.userinformation
where useractive = 1
and userdelete = 0
and userroleid = 3;
END