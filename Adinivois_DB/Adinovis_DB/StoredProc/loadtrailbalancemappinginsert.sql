﻿CREATE DEFINER=`admin`@`%` PROCEDURE `loadtrailbalancemappinginsert`( inengagementsid bigint)
BEGIN
	Declare loopstart INT(20);
	Declare loopend INT(20);
	Declare searchindex varchar(4000);
    Declare acttypesearch varchar(4000);
    Declare gfcode varchar(40);
	Declare mpshid bigint;
	Declare bttype INT(20);
	Declare nodate datetime;
    

 drop TEMPORARY TABLE if exists trailmpp;
  drop TEMPORARY TABLE if exists updtrailmpp;

	CREATE TEMPORARY TABLE trailmpp
	( tlid bigint NOT NULL AUTO_INCREMENT
	,  trailbalid bigint null
	, accountname varchar(4000) null
	, accounttype varchar(4000) null
	, baltypeid int null
	, mapshetid bigint null
    , gificode varchar(50) null
	, PRIMARY KEY (`tlid`)
	)AUTO_INCREMENT=0 ;
    

		insert into trailmpp
		(trailbalid, accountname, accounttype, baltypeid)
		SELECT trailbalanceid
		, accountname
		, accounttype
		 -- , case when originalbalance > 0 then 1 else 2 end  as balancetype
		, case when acctcredit > 0 then 1 else 2 end  as balancetype
		FROM adinovis.vtrailbal
        where  engagementsid = inengagementsid
        order by accountname;
 
 set nodate = current_timestamp;
 

 -- mapping trail balance with map sheet
	select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
 
	while loopstart <= loopend
		do
				begin
					select accountname into searchindex from trailmpp where tlid = loopstart;
					select  baltypeid into bttype from trailmpp where tlid = loopstart;			
					select accounttype into acttypesearch from trailmpp where tlid = loopstart;
				  
					set searchindex = rtrim(ltrim(searchindex));                  
                   
					select mt.mapsheetid into mpshid
					from adinovis.mapsheet mt 
					where mt.leadsheetid is not null
					and mt.finsubgroupchildId is not null
					and mt.balancetypeid is not null
					and mt.statementtypeid is not null
					and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
					and mt.balancetypeid = bttype
                    and mt.parentID > 0
					limit 1;

					update trailmpp
					set mapshetid = mpshid
					where tlid = loopstart;
                    
                    set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
				end;

		set loopstart = loopstart + 1;
	end while;
   
   
   -- mapping trail balance w.r.t gifi code which are not matched in above process
    select min(tlid) into loopstart  from trailmpp;
	select max(tlid) into loopend  from trailmpp;
    
                    set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
 
	while loopstart <= loopend
		do
			if (select 1 from trailmpp where tlid = loopstart and mapshetid is  null) then        
				begin
					select accountname into searchindex from trailmpp where tlid = loopstart;
					select  baltypeid into bttype from trailmpp where tlid = loopstart;			
					select accounttype into acttypesearch from trailmpp where tlid = loopstart;
				  
					set searchindex = rtrim(ltrim(searchindex));   
                    
                    if ( acttypesearch is null ) then
						select gificode into gfcode from adinovis.gificode
						where match(gifidescription) against(searchindex IN NATURAL LANGUAGE MODE)
						limit 1;                        
					else
						select gificode into gfcode from adinovis.gificode
						where match(gifidescription) against(searchindex IN NATURAL LANGUAGE MODE)
						and match(fingroup) against(acttypesearch IN NATURAL LANGUAGE MODE)
						limit 1;
					end if;
						
                   
					select mt.mapsheetid into mpshid
					from adinovis.mapsheet mt 
					where mt.leadsheetid is not null
					and mt.finsubgroupchildId is not null
					and mt.balancetypeid is not null
					and mt.statementtypeid is not null
                    and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
					and mt.gificode = gfcode
					and mt.balancetypeid = bttype
                    and mt.parentID > 0
					limit 1;


					-- removing paretnt id > 0
					if ( mpshid is null ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
							and mt.gificode = gfcode
							and mt.balancetypeid = bttype
							limit 1;
						end;
					end if;


					-- removing paretnt id > 0 and balance type (credit or debit)
					if ( mpshid is null ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
							and mt.gificode = gfcode
							limit 1;
						end;
					end if;


				-- removing paretnt id > 0 and balance type (credit or debit) and gifcode
					if ( mpshid is null ) then
						begin
							select mt.mapsheetid into mpshid
							from adinovis.mapsheet mt 
							where mt.leadsheetid is not null
							and mt.finsubgroupchildId is not null
							and mt.balancetypeid is not null
							and mt.statementtypeid is not null
							and match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE)
							limit 1;
						end;
					end if;


					update trailmpp
					set  gificode = gfcode
                    , mapshetid = case when mapshetid is null then mpshid end 
					where tlid = loopstart
                     and mapshetid is null;
                    
                    
                    
                    set searchindex = null;
                    set bttype = null;
                    set acttypesearch = null;
                    set mpshid = null;
                    set gfcode = null;
				end;
			end if;

		set loopstart = loopstart + 1;
	end while;

   -- inserting the mapping details to main table 
insert into adinovis.trailbalancemaping
	( trailbalanceid
    , mapsheetid
    , gificode
    , createdby
    , createddate
    , modifiedby
    , modifieddate
    )
select 
		tm.trailbalid
	,	tm.mapshetid
    ,	tm.gificode
    ,	1
    ,	nodate
    ,	1
	,	nodate
from trailmpp tm
where tm.trailbalid not in ( select trailbalanceid from adinovis.trailbalancemaping where isactive = 1 and isdelete = 0)
and tm.mapshetid is not null;



END