﻿/********************************************************************************************************************************
** SP Name: updateclientprofile
**
** Purpose: Edit client profile information.
**
**Parameters: 	Pclientfirmid : Client firm ID
**				Pbsname : business name
**				Pcontactperson:  Contect person name
**				Pbphonenumber : Business phone number
**				Pcphonenumber : Cell phone number
**				Pemailadd : email address
**				Pjurisdictionid : jurisdiction id
**				Ptypeofentityid : typeofentity id
**				Pincorporationdate :incorporation date
**				Paccountbook : accountbook (QB or Xero)
**				Paccesskey : access key 
**				Psecretkey  : secret key 
**				
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `updateclientprofile`(IN Pclientfirmid INT(20), 
											IN Pbsname varchar(100),
											IN Pcontactperson varchar(100),
											IN Pbphonenumber varchar(100),
											IN Pcphonenumber varchar(100),
											IN Pemailadd varchar(100),
											IN Pjurisdictionid int(20),
											IN Ptypeofentityid int(20),
											IN Pincorporationdate date,
											IN Paccountbook varchar(100),
											IN Paccesskey varchar(255),
											IN Psecretkey varchar(255),
											IN Ploginid int(20))
BEGIN
Declare vclientfirmid INT(20);
Declare vuseraccountid int(20);
Declare vauditorid int(20);
Declare vbsname varchar(100);
Declare vcontactperson varchar(100);
Declare vphonebtype varchar(100);
Declare vbphonenumber varchar(100);
Declare vphonectype varchar(100);
Declare vcphonenumber varchar(100);
Declare vemailadd varchar(100);
Declare vpwd varchar(100);
Declare vjurisdictionid int(20);
Declare vtypeofentityid int(20);
Declare vincorporationdate date;
Declare vaccountbook varchar(100);
Declare vaccesskey varchar(255);
Declare vsecretkey varchar(255);
Declare nodate date;



DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET  autocommit=0;	
SET vclientfirmid = Pclientfirmid;
SET vbsname = Pbsname;
SET vcontactperson = Pcontactperson;
SET vphonebtype = 'business phone';
SET vbphonenumber = Pbphonenumber;
SET vphonectype = 'cell phone';
SET vcphonenumber = Pcphonenumber;
SET vemailadd = Pemailadd;
SET vjurisdictionid = Pjurisdictionid;
SET vtypeofentityid = Ptypeofentityid;
SET vincorporationdate = Pincorporationdate;
SET vaccountbook = Paccountbook;
SET vaccesskey = Paccesskey;
SET vsecretkey = Psecretkey;
set nodate = current_timestamp;
set vauditorid = Ploginid;


	select useraccountid into vuseraccountid from adinovis.clientfirm where clientfirmid = vclientfirmid;

	INSERT INTO adinovis.useraccount_history(useraccountid,
											firstname,
											middlename,
											lastname,
											business_name,
											emailaddress,
											loginpassword,
											statusid,
											modifiedby,
											modifieddate)
									SELECT useraccountid,
									       firstname,
									       middlename,
									       lastname,
									       businessname,
	                                       emailaddress,
	                                       loginpassword,
	                                       statusid,
										   modifiedby,
										   modifieddate
									FROM adinovis.useraccount 
									WHERE useraccountid =  vuseraccountid;
	

    
	UPDATE adinovis.useraccount SET firstname = vcontactperson,
									lastname = vcontactperson,
									businessname = vbsname,
									emailaddress = vemailadd,
									modifiedby = cast(vauditorid AS CHAR),
									 modifieddate= nodate	
	WHERE  useraccountid = vuseraccountid;


	
	INSERT INTO adinovis.phone_history( phoneid,
										useraccountid,
										phonenumber,
										countrycode,
										phonetype,
										modifiedby,
										modifieddate)
								SELECT phoneid,
	                                   useraccountid,
	                                   phonenumber,
	                                   countrycode,
	                                   phonetype,
									   modifiedby,
									   modifieddate
								FROM adinovis.phone
								WHERE useraccountid =  vuseraccountid;
                                

      
	UPDATE adinovis.phone SET phonenumber = vbphonenumber,
							modifiedby = cast(vauditorid AS CHAR),
							modifieddate = nodate
	WHERE  useraccountid = vuseraccountid
	AND phonetype = vphonebtype;

	
       
	UPDATE adinovis.phone SET phonenumber = vcphonenumber,
							modifiedby = cast(vauditorid AS CHAR),
							modifieddate = nodate
	WHERE  useraccountid = vuseraccountid
	AND phonetype = vphonectype;
	

    
	INSERT INTO adinovis.clientfirm_history(clientfirmid,
											useraccountid,
											contactperson,
											incorporationdate,
											engagementname,
											subentitles,
											engagementtype,
											compilationtype,
											finanicalyearenddate,
											additionalinfo,
											typeofentityid,
											jurisdictionid,
											modifiedby,
											modifieddate)
										SELECT clientfirmid,
											useraccountid,
											contactperson,
											incorporationdate,
											engagementname,
											subentitles,
                                            engagementid,
											compilationtype,
											finanicalyearenddate,
											additionalinfo,
											typeofentityid,
											jurisdictionid,
											modifiedby,
											modifieddate
								FROM adinovis.clientfirm
								WHERE useraccountid =  vuseraccountid;
                                

              
        
	UPDATE adinovis.clientfirm SET contactperson =  vcontactperson,
									jurisdictionid = vjurisdictionid,
									typeofentityid = vtypeofentityid,
									incorporationdate = vincorporationdate,
									modifiedby = CAST(vauditorid AS CHAR),
									modifieddate = nodate
									where clientfirmid  =  vclientfirmid;
                                    
				
	
	INSERT INTO adinovis.clientdatasource_history
											  ( clientdatasourceid
											  , engagementsid
                                              , clientfirmid
                                              , sourcelink
                                              , clientid
                                              , secretkey
                                              , realmid
                                              , refreshtoken
                                              , accesstoken
                                              , isactive
                                              , isdelete
                                              , createdby
                                              , createddate
                                              , modifiedby
                                              , modifieddate
                                              , historyloaddate
											)
								SELECT clientdatasourceid
                                      , null
									  , clientfirmid
                                      , sourcelink
                                      , clientid
                                      , secretkey
                                      , realmid
                                      , refreshtoken
                                      , accesstoken
                                      , isactive
                                      , isdelete
                                      , createdby
                                      , createddate
                                      , modifiedby
                                      , modifieddate
                                      , current_timestamp
								from adinovis.clientdatasource
								where clientfirmid  =  vclientfirmid;

    
	UPDATE adinovis.clientdatasource SET 	sourcelink = vaccountbook,
									clientid = vaccesskey,
									secretkey = vsecretkey, 
									modifiedby = CAST(vauditorid AS CHAR),
									modifieddate = nodate
							WHERE clientfirmid  =  vclientfirmid;
                            
                            


BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'updateclientprofile',nodate); 
	end if;
END;

COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;
END