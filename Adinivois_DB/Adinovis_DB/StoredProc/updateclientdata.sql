﻿/********************************************************************************************************************************
** SP Name: updateclientdata
**
** Purpose: Edit Quick books and Xero related information in DB.
**
**Parameters: 	Pclientfirmid
**				Prealmid
**				Prefreshtoken 
**				
**
**
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateclientdata`(IN Pclientfirmid int(20),
									IN Paccesstoken varchar(4000),
									IN Prefreshtoken varchar(4000))
BEGIN
Declare vclientfirmid int(20);
Declare vaccesstoken varchar(4000);
Declare vrefreshtoken varchar(4000);
Declare vuseraccountid int(20);
Declare nodate date;
DECLARE errorCode CHAR(5) DEFAULT '00000';
    DECLARE errorMessage TEXT DEFAULT '';
	DECLARE continue HANDLER FOR SQLEXCEPTION 
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        errorCode = RETURNED_SQLSTATE, errorMessage = MESSAGE_TEXT;
    END;

SET  autocommit=0;	
SET vclientfirmid = Pclientfirmid;
SET vaccesstoken = Paccesstoken;
SET vrefreshtoken = Prefreshtoken;
set nodate = current_timestamp;

select useraccountid into vuseraccountid from adinovis.clientfirm where clientfirmid = vclientfirmid;

INSERT INTO adinovis.clientdatasource_history(clientdatasourceid,
													clientfirmid,
													 sourcelink,
													 clientid,
													 secretkey,
													 modifiedby,
													 modifieddate)
												SELECT clientdatasourceid,
														clientfirmid,
													 sourcelink,
													 clientid,
													 secretkey,
													 CAST(vuseraccountid AS CHAR),
													nodate
													from adinovis.clientdatasource
													where clientfirmid  =  vclientfirmid;

UPDATE adinovis.clientdatasource SET accesstoken = vaccesstoken, 
									refreshtoken = vrefreshtoken, 
									modifiedby = CAST(vuseraccountid AS CHAR),
									modifieddate = nodate
							WHERE clientfirmid  =  vclientfirmid;
							


BEGIN
    IF errorCode != '00000' THEN
    ROLLBACK;
	insert into adinovis.errorlog(errorcode,message,errorprocedure,errortime)
	values(errorCode, errorMessage,'adinovis.updateclientdata',nodate); 
	end if;
END;

COMMIT;
set  autocommit=1;	
begin
	IF errorCode = '00000' THEN
    select 1;
     end if;
end;			
END