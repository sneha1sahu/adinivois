﻿/********************************************************************************************************************************
** SP Name: getauditorrole
**
** Purpose: provide list of Auditor role like 'Preparer', 'Reviewer', 'Admin' . 
**
** Parameters: 
**             			
** 
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 17-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/

CREATE DEFINER=`admin`@`%` PROCEDURE `getauditorrole`()
BEGIN
select 
 ar.auditroleid
, ar.rowindex
, ur.rolename
,  ur.userroleid
from adinovis.userrole ur 
join adinovis.auditrole ar on ur.userroleid = ar.userroleid and ar.isactive = 1 and ar.isdelete = 0
where ur.auditrole =1
and ur.isactive = 1
and ur.isdelete = 0;

END