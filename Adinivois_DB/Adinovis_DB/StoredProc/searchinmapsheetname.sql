﻿CREATE DEFINER=`admin`@`%` PROCEDURE `searchinmapsheetname`(in searchindex varchar(4000))
BEGIN

select *
from adinovis.mapsheet_test mt 
where  match( mt.mapsheetname) against(searchindex IN NATURAL LANGUAGE MODE);

END