﻿CREATE DEFINER=`admin`@`%` PROCEDURE `deleteuseraccount`(in email varchar(400))
BEGIN

with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email
)
delete from adinovis.useraccountauth
where useraccountid in ( select useraccountid from updelete);


with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email
)
delete from adinovis.useraccountrole
where useraccountid in ( select useraccountid from updelete);

with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email
)
delete from adinovis.address
where useraccountid in ( select useraccountid from updelete);

with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email
)
delete from adinovis.phone
where useraccountid in ( select useraccountid from updelete);


with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid  in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.clientfirmauditorrole where engagementsid in ( select engagementsid from updelete);

with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email 
)
delete from adinovis.clientfirmauditorrole where useraccountid in ( select useraccountid from updelete);




with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.engagementsourceload
where engagementsid in ( select engagementsid from updelete);


with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.clientapidata
where engagementsid in ( select engagementsid from updelete);


with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.trailbalancedailyload
where engagementsid in ( select engagementsid from updelete);

with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.accountlistdata
where engagementsid in ( select engagementsid from updelete);


with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.clientquestionanswer
where engagementsid in ( select engagementsid from updelete);


with updelete as 
(
select trailbalanceid from adinovis.trailbalance where engagementsid in (
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email )))
)
delete from adinovis.trailbalancemaping
where trailbalanceid in ( select trailbalanceid from updelete);

with updelete as 
(
select trailbalanceid from adinovis.trailbalance where engagementsid in (
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email )))
)
delete from adinovis.trailadjustment
where trailbalanceid in ( select trailbalanceid from updelete);



with updelete as 
(
select engagementsid from adinovis.engagements where clientfirmid in (
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email ))
)
delete from adinovis.trailbalance
where engagementsid in ( select engagementsid from updelete);

with updelete as 
(
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email )
)
delete from adinovis.engagements
where clientfirmid in ( select clientfirmid from updelete);


with updelete as 
(
select clientfirmid from adinovis.clientfirm where useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email )
)
delete from adinovis.clientdatasource
where clientfirmid in ( select clientfirmid from updelete);


with updelete as 
(
SELECT useraccountid FROM adinovis.useraccount WHERE useraccountid in (SELECT useraccountid FROM adinovis.useraccount WHERE emailaddress  =  email )
)
delete from adinovis.clientfirm
where useraccountid in ( select useraccountid from updelete);


delete from adinovis.useraccount WHERE emailaddress  =  email ;





END