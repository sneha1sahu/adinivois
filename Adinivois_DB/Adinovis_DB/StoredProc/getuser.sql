﻿CREATE DEFINER=`admin`@`%` PROCEDURE `getuser`(Puseraccountid int(20))
BEGIN
select useraccountid,
fullname,
firstname,
lastname,
businessname,
emailaddress,
loginpassword,
status,
userdelete,
addressid,
address,
city,
state,
country,
postcode,
addressdelete,
businessphoneid,
businessphonetype,
businessphonenumber,
businessphonedelete,
cellphoneid,
cellphonetype,
cellphonenumber,
cellphonedelete,
userroleid,
rolename,
userroledelete,
licenseno,
tokenvalue,
profilepicture
from 
adinovis.userinformation
where useraccountid=Puseraccountid;


END