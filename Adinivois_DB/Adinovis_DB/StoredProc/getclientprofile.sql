﻿/********************************************************************************************************************************
** SP Name: getclientprofile
**
** Purpose: provide all clientfirm details. 
**		
**  
** Returns: 1 (success) or 0 (failure)
**
**
** Errors: Use continue HANDLER FOR SQLEXCEPTION in the error handler.
**
***********************************************************************************************
***********************************************************************************************
** Date         Author							Description
** -----------  --------------------------		-------------------------------------------------------------------------------------
** 14-May-2019	Bhavana Thakur				  Intial Creation
**********************************************************************************************************************************/
CREATE DEFINER=`admin`@`%` PROCEDURE `getclientprofile`(Ploginid int(20))
BEGIN

SELECT distinct
useraccountid,
clientfirmid,
businessname,
ifnull(engagementcount,0) as engagement,
case when statusid in (5,10) then 5 else statusid end as statusid ,
case when statusid in (5,10) then 'Accepted' else statusname end as statusname,
case when statusid in (10) then 'Data Source not in List' 
when  upper(sourcelink) like upper('Q%') then 'Quickbooks Online'  
when upper(sourcelink) = upper('X%') then 'Xero Online'
else sourcelink
end as sourcelink,
contactperson,
DATE(clientonboarding) as clientonboarding,
tokenvalue,
cellphonenumber,
businessphonenumber,
emailaddress,
jurisdictionid,
provincesname,
provincescode,
statename,
typeofentityid,
typeofentityname,
typeofentitycode,
incorporationdate,
clientid,
secretkey,
cast(modifieddate as date) as modifieddate,
true as isowned
FROM adinovis.clientprofile cf
where  userdelete = 0
and clientcreatedby = Ploginid
union
SELECT distinct
useraccountid,
clientfirmid,
businessname,
ifnull(engagementcount,0) as engagement,
case when statusid in (5,10) then 5 else statusid end as statusid ,
case when statusid in (5,10) then 'Accepted' else statusname end as statusname,
case when statusid in (10) then 'Data Source not in List' 
when  upper(sourcelink) like upper('Q%') then 'Quickbooks Online'  
when upper(sourcelink) = upper('X%') then 'Xero Online'
else sourcelink
end as sourcelink,
contactperson,
DATE(clientonboarding) as clientonboarding,
tokenvalue,
cellphonenumber,
businessphonenumber,
emailaddress,
jurisdictionid,
provincesname,
provincescode,
statename,
typeofentityid,
typeofentityname,
typeofentitycode,
incorporationdate,
clientid,
secretkey,
cast(modifieddate as date) as modifieddate,
false as isowned
FROM adinovis.clientprofile cf
where  userdelete = 0
and clientfirmid in (SELECT eg.clientfirmid FROM adinovis.clientfirmauditorrole cfr
						join adinovis.engagements eg on cfr.engagementsid = eg.engagementsid and eg.isactive = true and eg.isdelete  = false
                        join adinovis.clientfirm cf on eg.clientfirmid = cf.clientfirmid and cf.isactive = true and cf.isdelete  = false
						WHERE  cfr.isactive = true and cfr.isdelete  = false
						and cfr.useraccountid  = Ploginid  and cf.createdby != Ploginid)
order by modifieddate desc;


END