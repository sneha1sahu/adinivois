﻿insert into adinovis.fingroup(fingroupid,fingroupname,typeofbalance,BSorIS,createdby,modifiedby)values
(1,'Asset','Debit','Balance Statement',1,1),
(2,'Liability','credit','Balance Statement',1,1),
(3,'Equity','credit','Balance Statement',1,1),
(4,'Expenses','Debit','Income Statement',1,1),
(5,'Revenue','credit','Income Statement',1,1),
(6,'OCI','credit','Income Statement',1,1)
commit;