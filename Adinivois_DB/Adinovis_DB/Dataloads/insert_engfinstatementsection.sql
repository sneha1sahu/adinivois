﻿insert into adinovis.engfinstatementsection(name, description, isactive, isdelete, createdby, createddate, modifiedby, modifieddate)
values 
('Cover Page','a page with logo, title, finyear, findate',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Table of Contents','A list of titles of the parts of document',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Compilation Report','A compilation refers to financial statements that were prepared or compiled by an outside accountant of organization ',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Balance Sheet','detailing the balance of income and expenditure',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Income Statement','profit and loss statement',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Statement of Cash Flows','aggregate data regarding all cash inflows and outflows',b'1',b'0','1',current_timestamp ,'1',current_timestamp),
('Notes to Financial Statements','additional information',b'1',b'0','1',current_timestamp ,'1',current_timestamp);
