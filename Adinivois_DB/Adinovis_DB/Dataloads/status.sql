﻿INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (1,'Account Verification','useraccount','1','0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (2,'Forget Password','useraccount','1','0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (3,'Access Token','useraccount','1','0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (4,'Invited','useraccount','1','0','1','2019-05-06 08:15:09','1','2019-05-06 08:15:09');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (5,'Accepted','useraccount','1','0','1','2019-05-10 08:03:24','1','2019-05-10 08:03:24');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (6,'Rejected','useraccount','1','0','1','2019-05-10 08:04:14','1','2019-05-10 08:04:14');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (7,'Is Progress','engagement','1','0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (8,'Completed','engagement','1','0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38');
INSERT INTO adinovis.status (`statusid`,`statusname`,`processed`,`isactive`,`isdelete`,`createdby`,`createddate`,`modifiedby`,`modifieddate`) VALUES (9,'New','engagement','1','0','1','2019-05-21 06:03:38','1','2019-05-21 06:03:38');

insert into adinovis.status(statusid,statusname, processed, isactive, isdelete, createdby, createddate, modifiedby, modifieddate)
values (10,'Accepted; Data Source not in List' ,'client',b'1',b'0','1',current_timestamp,'1', current_timestamp);

insert into adinovis.status(statusname, processed, createdby, modifiedby)
values ('Not yet loaded', 'TBload', 1,1),
('Successfully Load', 'TBload', 1, 1),
('Not loaded, due to error', 'TBload', 1,1),
('No row count', 'TBload', 1, 1);