﻿-- Dev

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('Environment','development','1','1'); 

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('reset password link','http://3.215.103.80:8081/resetpassword?token=','1','1');

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('emailvalidation','http://3.215.103.80:8081/login?token=','1','1');

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('clientlogin','http://3.215.103.80:8081/clientlogin?token=','1','1');

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('baseurl','http://3.215.103.80:8081','1','1');

-- QA
INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('Environment','QA','1','1'); 

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('reset password link','https://qa.adinovis.com/resetpassword?token=','1','1');

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('emailvalidation','https://qa.adinovis.com/login?token=','1','1');

INSERT INTO adinovis.advpropertie(advpropertiename, advpropertievalue, createdby,modifiedby) 
Values('clientlogin','https://qa.adinovis.com/clientlogin?token=','1','1');