﻿INSERT INTO adinovis.leadsheet(fingroupid,leadsheetname,leadsheetcode,desciption,createdby,modifiedby)
VALUES
(1,'Cash','A','Cash',1,1)
,(1,'Marketable Securites','B','Marketable Securites',1,1)
,(1,'Accounts Receivable','C','Accounts Receivable',1,1)
,(1,'Inventory','D','Inventory',1,1)
,(1,'Prepaid Expenses','E','Prepaid Expenses',1,1)
,(1,'Investments','G','Investments',1,1)
,(1,'Property, Plant and Equipment','H','Property, Plant and Equipment',1,1)
,(1,'Other Current Assets','I','Other Current Assets',1,1)
,(1,'Other long term Assets ','J','Other Long term Assets',1,1)
,(1,'Intangible Assets','X','Intangible Assets',1,1)
,(2,'Bank Indebtedness and Notes Payable','AA','Bank Indebtedness and Notes Payable',1,1)
,(2,'Accounts Payable and accrued liabilities ','BB','Accounts Payable and accrued liabilities ',1,1)
,(2,'Current & Future Taxes','CC','Current & Future Taxes',1,1)
,(2,'Other current liabilities ','DD','Other current liabilities ',1,1)
,(2,'Related Party Balances & Transactions','EE','Related Party Balances & Transactions',1,1)
,(2,'Deferred Income','FF','Deferred Income',1,1)
,(2,'Debt','NN','Debt',1,1)
,(2,'Other Long term liabilities ','MM','Other long term liabilities',1,1)
,(3,'Share Capital, Retained Earnings & Equity','TT','Share Capital, Retained Earnings & Equity',1,1)
,(5,'Revenue','20','Revenue',1,1)
,(5,'Income\Loss','21','Other Income Section #1',1,1)
,(4,'Cost of Sales','30','Cost of Sales',1,1)
,(4,'Expenses','40','Expenses',1,1)
,(4,'Farm Revenue','50','Farm Revenue',1,1)
,(4,'Farm Production Expenses','51','Farm Production Expenses',1,1)
,(4,'Other Farm Expenses','52','Other Farm Expenses',1,1)
,(4,'Schedule of Administrative Expenses','60','Schedule of Administrative Expenses',1,1)
,(4,'Schedule of Financial Expenses','61','Schedule of Financial Expenses',1,1)
,(4,'Schedule of Occupancy Costs','62','Schedule of Occupancy Costs',1,1)
,(4,'Schedule of Selling Expenses','63','Schedule of Selling Expenses',1,1)
,(4,'Unallocated Departmental Expenses','69','Unallocated Departmental Expenses',1,1)
,(4,'Other Items','70','Other Items',1,1)
,(4,'Amortization','71','Amortization',1,1)
,(4,'Interest on Long-Term Debt','72','Interest on Long-Term Debt',1,1)
,(4,'Income Taxes','80','Income Taxes',1,1)
,(4,'Discontinued Operations','83','Discontinued Operations',1,1)
,(4,'Goodwill Impairment Loss','82','Goodwill Impairment Loss',1,1)
,(4,'Extraordinary Items','81','Extraordinary Items',1,1)
,(6,'Other Comprehensive Income','OCI','Other Comprehensive Income',1,1)

