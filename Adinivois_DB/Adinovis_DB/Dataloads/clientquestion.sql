﻿insert into adinovis.clientquestion(clientquestiontypeid,questiondescription, answertype, 
createdby,modifiedby) values
(1,'Are there any reasons that affect the decision to continue or accept this engagement?','Yes/No','1','1'),
(1,'Have you identified any independence threats? If yes, please document below.','Yes/No','1','1'),
(2,'Please outline the user of the financial  statements','Text','1','1'),
(2,'Describe the nature of activities or product and services offered by the entity','Text','1','1'),
(2,'List all the relevant third party service providers that is relevant to the business 
a.	Banking 
b.	Legal 
','Text','1','1'),
(2,'List of all related entities and type of relationship and percentage owned.','Text','1','1'),
(2,'List of principal shareholders with over 10% ownership and percentage owned','Text','1','1'),
(2,'List of principal shareholders with over 10% ownership and percentage owned','Text','1','1'),
(2,'Please attached the latest organization chart or entity/group of entities structure or explain the diff entity and their ownership below.','Text','1','1'),
(3,'Year end date when the last engagement letter was signed and received','Date','1','1'),
(3,'Minimum threshold amount which does not require any further investigation or reconciliation by the firm ','Text','1','1'),
(3,'Document any discussions between client throughout the year regarding their business, major operations or any other information that may be relevant to this engagement ','Text','1','1'),
(4,'Ensure compiled financial statements are arithmetically accurate.','Yes/No','1','1'),
(4,'Ensure compiled financial statements are not false or misleading.','Yes/No','1','1'),
(4,'If there are notes to financial statements ensure the notes outlined in the compiled financial statements are arithmetically accurate','Yes/No','1','1'),
(4,'If there are notes to financial statements ensure the notes outlined in the compiled financial statements are not false or misleading','Yes/No','1','1'),
(4,'If there are notes to financial statements ensure the notes are Not disclosing any accounting policy notes','Yes/No','1','1'),
(4,'Obtain and prepare bank reconciliation and agree it to the bank Statements as at year end 
','Yes/No','1','1'),
(4,'Obtain trial balance and agree to GL ','Yes/No','1','1'),
(4,'Prepare tax adjusting entries','Yes/No','1','1');