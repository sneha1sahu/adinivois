﻿select efss.engfinstatementsectionid,
efss.name, 
efss.description, 
efss.isactive, 
efss.isdelete,
efs.engfinstatementid,
efs.engagementsid, 
efs.Part1, 
efs.Part2, 
efs.Part3, 
efs.Part4, 
efs.Part5, 
efs.isactive as engfinstatactive, 
efs.isdelete as engfinstatdelete
from adinovis.engfinstatementsection efss
Left join adinovis.engfinstatement efs on
efss.engfinstatementsectionid= efs.engfinstatementsectionid;