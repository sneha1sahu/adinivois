﻿insert into adinovis.mappingstatus
( mappingstatusname, mappingstatusdescription,  createdby, modifiedby)
values
 ('Unmapped', 'Trail balance is not mapped to mapping sheet',1,1)
,('Auto Mapped', 'Trail balance is mapped through system',1,1)
,('New row', 'New row from trail balance daily load',1,1)
,('Added row', 'trail balance new row added by auditor',1,1)
,('Change row', 'trail balance change value w.r.t to daily load',1,1)
,('All','Select all rows',1,1);