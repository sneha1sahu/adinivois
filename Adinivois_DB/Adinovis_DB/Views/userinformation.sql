﻿CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `admin`@`%` 
    SQL SECURITY DEFINER
VIEW `adinovis`.`userinformation` AS
    SELECT 
        `ua`.`useraccountid` AS `useraccountid`,
        CONCAT(COALESCE(CONCAT(UPPER(SUBSTR(`ua`.`firstname`, 1, 1)),
                                LOWER(SUBSTR(`ua`.`firstname`, 2))),
                        ' '),
                COALESCE(CONCAT(UPPER(SUBSTR(`ua`.`middlename`, 1, 1)),
                                LOWER(SUBSTR(`ua`.`middlename`, 2))),
                        ' '),
                COALESCE(CONCAT(UPPER(SUBSTR(`ua`.`lastname`, 1, 1)),
                                LOWER(SUBSTR(`ua`.`lastname`, 2))),
                        ' ')) AS `fullname`,
        CONCAT(UPPER(SUBSTR(`ua`.`firstname`, 1, 1)),
                LOWER(SUBSTR(`ua`.`firstname`, 2))) AS `firstname`,
        CONCAT(UPPER(SUBSTR(`ua`.`lastname`, 1, 1)),
                LOWER(SUBSTR(`ua`.`lastname`, 2))) AS `lastname`,
        CONCAT(UPPER(SUBSTR(`ua`.`businessname`, 1, 1)),
                LOWER(SUBSTR(`ua`.`businessname`, 2))) AS `businessname`,
        `ua`.`emailaddress` AS `emailaddress`,
        `ua`.`loginpassword` AS `loginpassword`,
        `ua`.`statusid` AS `status`,
        `ua`.`isactive` AS `useractive`,
        `ua`.`isdelete` AS `userdelete`,
        `a`.`addressid` AS `addressid`,
        `a`.`address` AS `address`,
        `a`.`city` AS `city`,
        `a`.`state` AS `state`,
        `a`.`country` AS `country`,
        `a`.`postcode` AS `postcode`,
        `a`.`isactive` AS `addressactive`,
        `a`.`isdelete` AS `addressdelete`,
        (SELECT 
                `p1`.`phoneid`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'business phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphoneid`,
        (SELECT 
                `p1`.`phonetype`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'business phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonetype`,
        (SELECT 
                `p1`.`phonenumber`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'business phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonenumber`,
        (SELECT 
                `p1`.`isactive`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'business phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphoneactive`,
        (SELECT 
                `p1`.`isdelete`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'business phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonedelete`,
        (SELECT 
                `p1`.`phoneid`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'cell phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphoneid`,
        (SELECT 
                `p1`.`phonetype`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'cell phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphonetype`,
        (SELECT 
                `p1`.`phonenumber`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'cell phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphonenumber`,
        (SELECT 
                `p1`.`isactive`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'cell phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphoneactive`,
        (SELECT 
                `p1`.`isdelete`
            FROM
                `adinovis`.`phone` `p1`
            WHERE
                ((`p1`.`phonetype` = 'cell phone')
                    AND (`p1`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphonedelete`,
        `uar`.`useraccountroleID` AS `useraccountroleID`,
        `uar`.`userroleid` AS `userroleid`,
        `ur`.`rolename` AS `rolename`,
        `ur`.`auditrole` AS `auditrole`,
        `uar`.`isactive` AS `userroleactive`,
        `uar`.`isdelete` AS `userroledelete`,
        `ua`.`licenseno` AS `licenseno`,
        (SELECT 
                `t`.`tokenvalue`
            FROM
                `adinovis`.`useraccountauth` `t`
            WHERE
                (`t`.`useraccountid` = `ua`.`useraccountid`)) AS `tokenvalue`,
        `ua`.`profilepicture` AS `profilepicture`
    FROM
        (((`adinovis`.`useraccount` `ua`
        JOIN `adinovis`.`address` `a`)
        JOIN `adinovis`.`useraccountrole` `uar`)
        JOIN `adinovis`.`userrole` `ur`)
    WHERE
        ((`ua`.`useraccountid` = `a`.`useraccountid`)
            AND (`ua`.`useraccountid` = `uar`.`useraccountid`)
            AND (`uar`.`userroleid` = `ur`.`userroleid`)
            AND (`ua`.`isactive` = 1)
            AND (`ua`.`isdelete` = 0)
            AND (`a`.`isactive` = 1)
            AND (`a`.`isdelete` = 0)
            AND (`uar`.`isactive` = 1)
            AND (`uar`.`isdelete` = 0))