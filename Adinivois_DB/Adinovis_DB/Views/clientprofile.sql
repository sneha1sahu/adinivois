﻿CREATE algorithm=undefined definer=`admin`@`%` sql security definer VIEW `adinovis`.`clientprofile` AS WITH `ctephone` AS
( 
       SELECT `p`.`phoneid`       AS `phoneid`, 
              `p`.`useraccountid` AS `useraccountid`, 
              `p`.`phonenumber`   AS `phonenumber`, 
              `p`.`phonetype`     AS `phonetype`, 
              `p`.`isactive`      AS `isactive`, 
              `p`.`isdelete`      AS `isdelete` 
       FROM   `adinovis`.`phone` `p` 
       WHERE  (( 
                            `p`.`isactive` = 1) 
              AND    ( 
                            `p`.`isdelete` = 0))), `engagecount` AS 
( 
         SELECT   `eg`.`clientfirmid`     AS `clientfirmid`, 
                  count(`eg`.`createdby`) AS `engagementcount` 
         FROM     `adinovis`.`engagements` `eg` 
         WHERE    (( 
                                    `eg`.`isactive` = 1) 
                  AND      ( 
                                    `eg`.`isdelete` = 0)) 
         GROUP BY `eg`.`clientfirmid`)SELECT DISTINCT `ua`.`useraccountid`                                                                                AS `useraccountid`,
                                concat(upper(substr(`ua`.`businessname`,1,1)),lower(substr(`ua`.`businessname`,2))) AS `businessname`,
                                concat(upper(substr(`ua`.`firstname`,1,1)),lower(substr(`ua`.`firstname`,2)))       AS `clientname`,
                `ua`.`emailaddress`                                                                                 AS `emailaddress`,
                `ua`.`isdelete`                                                                                     AS `userdelete`,( 
                CASE 
                                WHEN ( 
                                                                `ua`.`statusid` IN (2,3)) THEN 5
                                ELSE `ua`.`statusid` 
                END) AS `statusid`,( 
                CASE 
                                WHEN ( 
                                                                `sts`.`statusname` IN ('Forget Password',
                                                                                       'Access Token')) THEN 'Accepted'
                                ELSE `sts`.`statusname` 
                END)               AS `statusname`, 
                `ua`.`createddate` AS `clientonboarding`, 
                `ur`.`userroleid`  AS `userroleid`, 
                `usr`.`rolename`   AS `rolename`, 
                ( 
                       SELECT `cp`.`phoneid` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphoneid`,
                ( 
                       SELECT `cp`.`phonetype` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonetype`,
                ( 
                       SELECT `cp`.`phonenumber` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonenumber`,
                ( 
                       SELECT `cp`.`isactive` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphoneactive`,
                ( 
                       SELECT `cp`.`isdelete` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `businessphonedelete`,
                ( 
                       SELECT `cp`.`phoneid` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'cell phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphoneid`,
                ( 
                       SELECT `cp`.`phonetype` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'cell phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphonetype`,
                ( 
                       SELECT `cp`.`phonenumber` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'cell phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphonenumber`,
                ( 
                       SELECT `cp`.`isactive` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`))) AS `cellphoneactive`,
                ( 
                       SELECT `cp`.`isdelete` 
                       FROM   `ctephone` `cp` 
                       WHERE  (( 
                                            `cp`.`phonetype` = 'business phone') 
                              AND    ( 
                                            `cp`.`useraccountid` = `ua`.`useraccountid`)))                            AS `cellphonedelete`,
                `cf`.`clientfirmid`                                                                                   AS `clientfirmid`,
                                concat(upper(substr(`cf`.`contactperson`,1,1)),lower(substr(`cf`.`contactperson`,2))) AS `contactperson`,
                `cf`.`jurisdictionid`                                                                                 AS `jurisdictionid`,
                `ju`.`provincesname`                                                                                  AS `provincesname`,
                `ju`.`provincescode`                                                                                  AS `provincescode`,
                `ju`.`statename`                                                                                      AS `statename`,
                `cf`.`typeofentityid`                                                                                 AS `typeofentityid`,
                `te`.`typeofentityname`                                                                               AS `typeofentityname`,
                `te`.`typeofentitycode`                                                                               AS `typeofentitycode`,
                `cf`.`incorporationdate`                                                                              AS `incorporationdate`,
                `cf`.`createdby`                                                                                      AS `clientcreatedby`,
                `cf`.`modifiedby`                                                                                     AS `clientmodifiedby`,
                ifnull(`ec`.`engagementcount`,0)                                                                      AS `engagementcount`,
                `cd`.`clientdatasourceid`                                                                             AS `clientdatasourceid`,
                `cf`.`isdelete`                                                                                       AS `clientfirmdelete`,
                `cd`.`sourcelink`                                                                                     AS `sourcelink`,
                `cd`.`clientid`                                                                                       AS `clientid`,
                `cd`.`secretkey`                                                                                      AS `secretkey`,
                `cd`.`isdelete`                                                                                       AS `clientdatasourcedelete`,
                `uaa`.`tokenvalue`                                                                                    AS `tokenvalue`,
                `ua`.`createdby`                                                                                      AS `usercreatedby`,
                `cf`.`modifieddate`                                                                                   AS `modifieddate`
FROM            (((((((((`adinovis`.`useraccount` `ua` 
JOIN            `adinovis`.`useraccountrole` `ur` 
ON             ((( 
                                                                `ua`.`useraccountid` = `ur`.`useraccountid`)
                                AND             ( 
                                                                `ur`.`isactive` = 1) 
                                AND             ( 
                                                                `ur`.`isdelete` = 0) 
                                AND             ( 
                                                                `ur`.`userroleid` = 4)))) 
JOIN            `adinovis`.`clientfirm` `cf` 
ON             ((( 
                                                                `ua`.`useraccountid` = `cf`.`useraccountid`)
                                AND             ( 
                                                                `cf`.`isactive` = 1) 
                                AND             ( 
                                                                `cf`.`isdelete` = 0)))) 
JOIN            `adinovis`.`clientdatasource` `cd` 
ON             ((( 
                                                                `cf`.`clientfirmid` = `cd`.`clientfirmid`)
                                AND             ( 
                                                                `cd`.`isactive` = 1) 
                                AND             ( 
                                                                `cd`.`isdelete` = 0)))) 
JOIN            `adinovis`.`status` `sts` 
ON             ((( 
                                                                `ua`.`statusid` = `sts`.`statusid`)
                                AND             ( 
                                                                `sts`.`isactive` = 1) 
                                AND             ( 
                                                                `sts`.`isdelete` = 0)))) 
JOIN            `adinovis`.`userrole` `usr` 
ON             ((( 
                                                                `ur`.`userroleid` = `usr`.`userroleid`)
                                AND             ( 
                                                                `usr`.`isactive` = 1) 
                                AND             ( 
                                                                `usr`.`isdelete` = 0)))) 
JOIN            `adinovis`.`jurisdiction` `ju` 
ON             ((( 
                                                                `cf`.`jurisdictionid` = `ju`.`jurisdictionid`)
                                AND             ( 
                                                                `ju`.`isactive` = 1) 
                                AND             ( 
                                                                `ju`.`isdelete` = 0)))) 
JOIN            `adinovis`.`typeofentity` `te` 
ON             ((( 
                                                                `cf`.`typeofentityid` = `te`.`typeofentityid`)
                                AND             ( 
                                                                `te`.`isactive` = 1) 
                                AND             ( 
                                                                `te`.`isdelete` = 0)))) 
JOIN            `adinovis`.`useraccountauth` `uaa` 
ON             ((( 
                                                                `ua`.`useraccountid` = `uaa`.`useraccountid`)
                                AND             ( 
                                                                `uaa`.`isactive` = 1) 
                                AND             ( 
                                                                `uaa`.`isdelete` = 0)))) 
LEFT JOIN       `engagecount` `ec` 
ON             (( 
                                                `cf`.`clientfirmid` = `ec`.`clientfirmid`)))