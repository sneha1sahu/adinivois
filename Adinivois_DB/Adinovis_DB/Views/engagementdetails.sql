CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `admin`@`%` 
    SQL SECURITY DEFINER
VIEW `adinovis`.`engagementdetails` AS
    SELECT 
        `eg`.`engagementsid` AS `engagementsid`,
        `eg`.`clientfirmid` AS `clientfirmid`,
        `eg`.`engagementname` AS `engagementname`,
        `eg`.`subentitles` AS `subentitles`,
        `eg`.`engagementtypeid` AS `engagementtypeid`,
        (SELECT 
                `e`.`engagementtype`
            FROM
                `adinovis`.`engagementtype` `e`
            WHERE
                (`e`.`engagementtypeid` = `eg`.`engagementtypeid`)) AS `engagementtype`,
        `eg`.`compilationtype` AS `compilationtype`,
        `eg`.`finanicalyearenddate` AS `finanicalyearenddate`,
        `eg`.`additionalinfo` AS `additionalinfo`,
        `eg`.`statusid` AS `engagementstatusid`,
        `eg`.`createddate` AS `createddate`,
        `eg`.`createdby` AS `createdby`,
        `eg`.`isactive` AS `isactive`,
        `eg`.`isdelete` AS `isdelete`,
        `eg`.`tbloadstatusid` AS `tbloadstatusid`,
		 eg.modifieddate 
    FROM
        `adinovis`.`engagements` `eg`
    WHERE
        ((`eg`.`isdelete` = 0)
            AND (`eg`.`isactive` = 1))