﻿CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `admin`@`%` 
    SQL SECURITY DEFINER
VIEW `vtbmapping` AS
    SELECT 
        `tb`.`trailbalanceid` AS `trailbalanceid`,
        `tb`.`engagementsid` AS `engagementsid`,
        `tb`.`clientdatasourceid` AS `clientdatasourceid`,
        `tb`.`clientapidataid` AS `clientapidataid`,
        `tb`.`recieveddate` AS `recieveddate`,
        `tb`.`accountcode` AS `accountcode`,
        `tb`.`accountname` AS `accountname`,
        `tb`.`accounttype` AS `accounttype`,
        `tb`.`originalbalance` AS `originalbalance`,
        `tb`.`trailadjustmentid` AS `trailadjustmentid`,
        `tb`.`adjustmenttypeid` AS `adjustmenttypeid`,
        `tb`.`adjustmenttypename` AS `adjustmenttypename`,
        `tb`.`adjustmentamount` AS `adjustmentamount`,
        `tbm`.`trailbalancemapingid` AS `trailbalancemapingid`,
        `tbm`.`mapsheetid` AS `mapsheetid`,
        `ms`.`mapno` AS `mapno`,
        `ms`.`mapsheetname` AS `mapsheetname`,
        `ms`.`fingroupid` AS `fingroupid`,
        `ms`.`fingroupname` AS `fingroupname`,
        `ms`.`finsubgrpid` AS `finsubgrpid`,
        `ms`.`finsubgroupname` AS `finsubgroupname`,
        `ms`.`finsubgroupchildId` AS `finsubgroupchildId`,
        `ms`.`finsubgroupchildname` AS `finsubgroupchildname`,
        `ms`.`leadsheetid` AS `leadsheetid`,
        `ms`.`leadsheetname` AS `leadsheetname`,
        `ms`.`leadsheetcode` AS `leadsheetcode`,
        `ms`.`balancetypeid` AS `balancetypeid`,
        `ms`.`balancetypename` AS `balancetypename`,
        `ms`.`statementtypeid` AS `statementtypeid`,
        `ms`.`statementtypename` AS `statementtypename`,
        `ms`.`gificode` AS `gificode`,
        0.00 AS `fxtranslation`,
        (`tb`.`originalbalance` + `tb`.`adjustmentamount`) AS `finalamount`,
        `tb`.`isclientdatasource` AS `isclientdatasource`,
        `tb`.`PY1` AS `PY1`,
        (`tb`.`originalbalance` - `tb`.`PY1`) AS `changes`,
        `tb`.`PY2` AS `PY2`,
        `ms`.`Notes` AS `notes`,
        `msts`.`mappingstatusname` AS `mappingstatus`,
        `msts`.`mappingstatusid` AS `mappingstatusid`,
        `tb`.`acctyear` AS `acctyear`
    FROM
        ((((`vtrailbal` `tb`
        LEFT JOIN `trailbalancemaping` `tbm` ON (((`tbm`.`trailbalanceid` = `tb`.`trailbalanceid`)
            AND (`tbm`.`isactive` = 1)
            AND (`tbm`.`isdelete` = 0))))
        LEFT JOIN `vmapsheet` `ms` ON (((`tbm`.`mapsheetid` = `ms`.`mapsheetid`)
            AND (`ms`.`isdelete` = 0))))
        LEFT JOIN `trailbalancedailyload` `tdl` ON (((`tb`.`trailbalanceid` = `tdl`.`trailbalanceid`)
            AND (`tdl`.`isactive` = 1)
            AND (`tdl`.`isdelete` = 0)
            AND (`tdl`.`TBshowonUI` = 1)
            AND ((`tdl`.`isdifferwithexisting` = 1)
            OR (`tdl`.`isnewrecord` = 1)))))
        LEFT JOIN `mappingstatus` `msts` ON (((CASE
            WHEN
                (ISNULL(`tbm`.`mapsheetid`)
                    OR (`tbm`.`mapsheetid` = 0))
            THEN
                1
            WHEN (`tdl`.`isdifferwithexisting` = 1) THEN 5
            WHEN (`tdl`.`isnewrecord` = 1) THEN 3
            WHEN (`tb`.`isclientdatasource` = 0) THEN 4
            ELSE 2
        END) = `msts`.`mappingstatusid`)))