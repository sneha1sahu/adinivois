﻿CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `admin`@`%` 
    SQL SECURITY DEFINER
VIEW `adinovis`.`vtrailbal` AS
    SELECT DISTINCT
        `tb`.`trailbalanceid` AS `trailbalanceid`,
        `tb`.`engagementsid` AS `engagementsid`,
        `tb`.`clientdatasourceid` AS `clientdatasourceid`,
        `tb`.`clientapidataid` AS `clientapidataid`,
        `tb`.`recieveddate` AS `recieveddate`,
        `tb`.`accountcode` AS `accountcode`,
        `tb`.`accountname` AS `accountname`,
        `tb`.`accounttype` AS `accounttype`,
        `tb`.`acctcredit` AS `acctcredit`,
        `tb`.`acctdebit` AS `acctdebit`,
        (CASE
            WHEN (`tb`.`acctcredit` > 0.00) THEN -(`tb`.`acctcredit`)
            WHEN (`tb`.`acctdebit` > 0.00) THEN `tb`.`acctdebit`
            ELSE 0.00
        END) AS `originalbalance`,
        `tb`.`PY1` AS `PY1`,
        `tb`.`PY2` AS `PY2`,
        `tba`.`trailadjustmentid` AS `trailadjustmentid`,
        `tba`.`adjustmenttypeid` AS `adjustmenttypeid`,
        `adt`.`adjustmenttypename` AS `adjustmenttypename`,
        `tba`.`acctcredit` AS `adjustcredit`,
        `tba`.`acctdebit` AS `adjustdebit`,
        (CASE
            WHEN (`tba`.`acctcredit` > 0.00) THEN `tba`.`acctcredit`
            WHEN (`tba`.`acctdebit` > 0.00) THEN -(`tba`.`acctdebit`)
            ELSE 0.00
        END) AS `adjustmentamount`,
        `tb`.`isclientdatasource` AS `isclientdatasource`,
        `tb`.`acctyear` AS `acctyear`
    FROM
        ((`adinovis`.`trailbalance` `tb`
        LEFT JOIN `adinovis`.`trailadjustment` `tba` ON (((`tb`.`trailbalanceid` = `tba`.`trailbalanceid`)
            AND (`tba`.`isactive` = 1)
            AND (`tba`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`adjustmenttype` `adt` ON (((`tba`.`adjustmenttypeid` = `adt`.`adjustmenttypeid`)
            AND (`adt`.`isactive` = 1)
            AND (`adt`.`isdelete` = 0))))
    WHERE
        ((`tb`.`isactive` = 1)
            AND (`tb`.`isdelete` = 0))