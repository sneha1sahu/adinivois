﻿create view adinovis.tn_fg_fsg_fscg as 
select fingroupname,fsg.finsubgroupname,fscg.finsubgroupchildname
from adinovis.Finsubgroup fsg
join adinovis.Fingroup fg on fsg.fingroupid = fg.fingroupid
join adinovis.finsubgroupchild fscg on fscg.finsubgroupid = fsg.finsubgrpid;
