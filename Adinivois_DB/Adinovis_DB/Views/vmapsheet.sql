﻿CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `admin`@`%` 
    SQL SECURITY DEFINER
VIEW `adinovis`.`vmapsheet` AS
    SELECT DISTINCT
        `ms`.`mapsheetid` AS `mapsheetid`,
        `ms`.`mapno` AS `mapno`,
        `ms`.`mapsheetname` AS `mapsheetname`,
        `fg`.`fingroupid` AS `fingroupid`,
        `fg`.`fingroupname` AS `fingroupname`,
        `fsg`.`finsubgroupid` AS `finsubgrpid`,
        `fsg`.`finsubgroupname` AS `finsubgroupname`,
        `ms`.`finsubgroupchildId` AS `finsubgroupchildId`,
        `fsgc`.`finsubgroupchildname` AS `finsubgroupchildname`,
        `ms`.`leadsheetid` AS `leadsheetid`,
        `ls`.`leadsheetname` AS `leadsheetname`,
        `ls`.`leadsheetcode` AS `leadsheetcode`,
        `ms`.`balancetypeid` AS `balancetypeid`,
        `bt`.`balancetypename` AS `balancetypename`,
        `ms`.`statementtypeid` AS `statementtypeid`,
        `st`.`statementtypename` AS `statementtypename`,
        `ms`.`gificode` AS `gificode`,
        `ms`.`isdelete` AS `isdelete`,
        `ms`.`Notes` AS `Notes`
    FROM
        ((((((`adinovis`.`mapsheet` `ms`
        LEFT JOIN `adinovis`.`finsubgroupchild` `fsgc` ON (((`ms`.`finsubgroupchildId` = `fsgc`.`finsubgroupchildId`)
            AND (`fsgc`.`isactive` = 1)
            AND (`fsgc`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`finsubgroup` `fsg` ON (((`fsgc`.`finsubgroupid` = `fsg`.`finsubgroupid`)
            AND (`fsg`.`isactive` = 1)
            AND (`fsg`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`fingroup` `fg` ON (((`fsg`.`fingroupid` = `fg`.`fingroupid`)
            AND (`fg`.`isactive` = 1)
            AND (`fg`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`leadsheet` `ls` ON (((`ms`.`leadsheetid` = `ls`.`leadsheetid`)
            AND (`ls`.`isactive` = 1)
            AND (`ls`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`balancetype` `bt` ON (((`ms`.`balancetypeid` = `bt`.`balancetypeid`)
            AND (`bt`.`isactive` = 1)
            AND (`bt`.`isdelete` = 0))))
        LEFT JOIN `adinovis`.`statementtype` `st` ON (((`ms`.`statementtypeid` = `st`.`statementtypeid`)
            AND (`st`.`isactive` = 1)
            AND (`st`.`isdelete` = 0))))