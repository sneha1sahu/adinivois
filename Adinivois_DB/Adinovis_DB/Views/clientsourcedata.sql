﻿CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `adinovis`.`clientsourcedata` AS
    SELECT 
        `c`.`clientfirmid` AS `clientfirmid`,
        `c`.`useraccountid` AS `useraccountid`,
        `c`.`contactperson` AS `contactperson`,
        `c`.`jurisdictionid` AS `jurisdictionid`,
        `j`.`provincesname` AS `provincesname`,
        `j`.`provincescode` AS `provincescode`,
        `j`.`statename` AS `statename`,
        `c`.`typeofentityid` AS `typeofentityid`,
        `t`.`typeofentityname` AS `typeofentityname`,
        `t`.`typeofentitycode` AS `typeofentitycode`,
        `c`.`incorporationdate` AS `incorporationdate`,
        `c`.`engagementname` AS `engagementname`,
        `c`.`subentitles` AS `subentitles`,
        `c`.`engagementid` AS `engagementid`,
        (SELECT 
                `e`.`engagementtype`
            FROM
                `adinovis`.`engagement` `e`
            WHERE
                (`e`.`engagementid` = `c`.`engagementid`)) AS `engagementtype`,
        `c`.`compilationtype` AS `compilationtype`,
        `c`.`finanicalyearenddate` AS `finanicalyearenddate`,
        `c`.`additionalinfo` AS `additionalinfo`,
        `c`.`isactive` AS `clientfirmactive`,
        `c`.`isdelete` AS `clientfirmdeleted`,
        `cfar`.`clientfirmauditorroleid` AS `clientfirmauditorroleid`,
        `cfar`.`userroleid` AS `userroleid`,
        `ur`.`rolename` AS `rolename`,
        `cfar`.`perhourcal` AS `perhourcal`,
        `cfar`.`isactive` AS `clientfirmauditoractive`,
        `cfar`.`isdelete` AS `clientfirmauditordeleted`,
        `cds`.`clientdatasourceid` AS `clientdatasourceid`,
        `cds`.`sourcelink` AS `sourcelink`,
        `cds`.`clientid` AS `clientid`,
        `cds`.`secretkey` AS `secretkey`,
        `cds`.`isactive` AS `clientdatasourceactive`,
        `cds`.`isdelete` AS `clientdatasourcedeleted`
    FROM
        (((((`adinovis`.`clientfirm` `c`
        JOIN `adinovis`.`clientfirmauditorrole` `cfar`)
        JOIN `adinovis`.`clientdatasource` `cds`)
        JOIN `adinovis`.`userrole` `ur`)
        JOIN `adinovis`.`jurisdiction` `j`)
        JOIN `adinovis`.`typeofentity` `t`)
    WHERE
        ((`c`.`clientfirmid` = `cfar`.`clientfirmid`)
            AND (`c`.`clientfirmid` = `cds`.`clientfirmid`)
            AND (`cfar`.`userroleid` = `ur`.`userroleid`)
            AND (`c`.`typeofentityid` = `t`.`typeofentityid`)
            AND (`c`.`jurisdictionid` = `j`.`jurisdictionid`))