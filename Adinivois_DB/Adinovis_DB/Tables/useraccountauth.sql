﻿CREATE TABLE `adinovis`.`useraccountauth` (
  `useraccountauthid` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountid` bigint(20) NOT NULL,
  `tokentypeid` bigint(20) NOT NULL,
  `tokenvalue` varchar(255) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Modifiedby` varchar(100) NOT NULL,
  `Modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`useraccountauthid`),
  KEY `fk_useraccroleid` (`useraccountid`),
  KEY `fk_ttype` (`tokentypeid`),
  CONSTRAINT `fk_ttype` FOREIGN KEY (`tokentypeid`) REFERENCES `tokentype` (`tokentypeid`),
  CONSTRAINT `fk_useraccroleid` FOREIGN KEY (`useraccountid`) REFERENCES `useraccount` (`useraccountid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4