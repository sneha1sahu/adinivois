﻿CREATE TABLE`adinovis`.`clientdoc`(
    `clientdocsid` bigint(20) NOT NULL AUTO_INCREMENT,
    `clientfirmid` bigint(20) NOT NULL,
    `clientdoctypeid` bigint(20) NOT NULL,
    `clientdocname` text,
    `clientdocpath` varchar(100) DEFAULT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`clientdocsid`),
    KEY`fk_cdoc`(`clientfirmid`),
    KEY`fk_cdoc1`(`clientdoctypeid`),
    CONSTRAINT`clientdoc_ibfk_1` FOREIGN KEY(`clientfirmid`) REFERENCES`clientfirm`(`clientfirmid`),
    CONSTRAINT`clientdoc_ibfk_2` FOREIGN KEY(`clientdoctypeid`) REFERENCES`clientdoctype`(`clientdoctypeid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci