﻿CREATE TABLE `fingroup` (
  `fingroupid` bigint(20) NOT NULL AUTO_INCREMENT,
  `fingroupname` varchar(100) NOT NULL,
  `typeofbalance` varchar(10) NOT NULL,
  `BSorIS` varchar(1000) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `sequenceorder` int NOT NULl,
	 `statementtypeid` bigint(20) NOT NULL,
  PRIMARY KEY (`fingroupid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci