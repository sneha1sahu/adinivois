﻿CREATE TABLE`adinovis`.`leadsheet`(
    `leadsheetid` bigint(20) NOT NULL AUTO_INCREMENT,
    `fingroupid` bigint(20) NOT NULL,
    `leadsheetname` varchar(100) NOT NULL,
    `leadsheetcode` varchar(10) NOT NULL,
    `desciption` varchar(100) NOT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`leadsheetid`),
    KEY`fk_fingrpls`(`fingroupid`),
    CONSTRAINT`leadsheet_ibfk_1` FOREIGN KEY(`fingroupid`) REFERENCES`fingroup`(`fingroupid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci