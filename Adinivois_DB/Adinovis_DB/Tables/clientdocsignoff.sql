﻿CREATE TABLE`adinovis`.`clientdocsignoff`(
    `clientdocsignoffid` bigint(20) NOT NULL AUTO_INCREMENT,
    `clientdocsid` bigint(20) NOT NULL,
    `clientfirmauditorroleid` bigint(20) NOT NULL,
    `signby` text,
    `signdate` varchar(100) DEFAULT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`clientdocsignoffid`),
    KEY`fk_csing`(`clientdocsid`),
    KEY`fk_csing1`(`clientfirmauditorroleid`),
    CONSTRAINT`clientdocsignoff_ibfk_1` FOREIGN KEY(`clientdocsid`) REFERENCES`clientdoc`(`clientdocsid`),
    CONSTRAINT`clientdocsignoff_ibfk_2` FOREIGN KEY(`clientfirmauditorroleid`) REFERENCES`clientfirmauditorrole`(`clientfirmauditorroleid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci