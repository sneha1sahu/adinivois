﻿CREATE TABLE `adinovis`.`tokentype` (
  `tokentypeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `tokenname` varchar(100) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tokentypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4