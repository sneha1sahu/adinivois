﻿CREATE TABLE `clientapidata` (
  `clientapidataid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientdatasourceid` bigint(20) NOT NULL,
  `engagementsid` bigint(20) DEFAULT NULL,
  `recieveddata` text,
  `accountlistdata` text,
  `responsestatus` text,
  `isprocessed` bit(1) NOT NULL DEFAULT b'0',
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientapidataid`),
  KEY `clientapidata_engagementsid_idx` (`engagementsid`),
  CONSTRAINT `fk_clientapidata_engagementsid` FOREIGN KEY (`engagementsid`) REFERENCES `engagements` (`engagementsid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci