﻿CREATE TABLE `clientquestionanswer` (
  `clientquestionanswerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `engagementsid` bigint(20) NOT NULL,
  `clientquestionid` bigint(20) NOT NULL,
  `clientanswer` text,
  `clientnote` text,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientquestionanswerid`),
  KEY `fk_cquesans1` (`clientquestionid`),
  KEY `engagementsid_ibf_idx` (`engagementsid`),
  CONSTRAINT `clientquestionanswer_ibfk_2` FOREIGN KEY (`clientquestionid`) REFERENCES `clientquestion` (`clientquestionid`),
  CONSTRAINT `engagementsid_ibf` FOREIGN KEY (`engagementsid`) REFERENCES `engagements` (`engagementsid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci