﻿CREATE TABLE`adinovis`.`comprocedures`(
    `comproceduresid` bigint(20) NOT NULL AUTO_INCREMENT,
    `trailbalanceid` bigint(20) NOT NULL,
    `clientdoctypeid` bigint(20) NOT NULL,
    `tickmarkid` bigint(20) NOT NULL,
    `ticknotes` text,
    `clientdocname` text,
    `clientdocpath` varchar(100) DEFAULT NULL,
    `signby` text,
    `signdate` date DEFAULT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`comproceduresid`),
    KEY`fk_cproc`(`trailbalanceid`),
    KEY`fk_cproc1`(`clientdoctypeid`),
    KEY`fk_cproc2`(`tickmarkid`),
    CONSTRAINT`comprocedures_ibfk_1` FOREIGN KEY(`trailbalanceid`) REFERENCES`trailbalance`(`trailbalanceid`),
    CONSTRAINT`comprocedures_ibfk_2` FOREIGN KEY(`clientdoctypeid`) REFERENCES`clientdoctype`(`clientdoctypeid`),
    CONSTRAINT`comprocedures_ibfk_3` FOREIGN KEY(`tickmarkid`) REFERENCES`tickmark`(`tickmarkid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci