﻿CREATE TABLE `clientfirm_history` (
  `clientfirmhistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientfirmid` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `contactperson` varchar(100) DEFAULT NULL,
  `incorporationdate` date DEFAULT NULL,
  `engagementname` varchar(100) DEFAULT NULL,
  `subentitles` varchar(100) DEFAULT NULL,
  `engagementtype` varchar(100) DEFAULT NULL,
  `compilationtype` varchar(100) DEFAULT NULL,
  `finanicalyearenddate` date DEFAULT NULL,
  `additionalinfo` varchar(100) DEFAULT NULL,
  `typeofentityid` bigint(20) NOT NULL,
  `jurisdictionid` bigint(20) NOT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientfirmhistoryid`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci