﻿CREATE TABLE `clientdatasource` (
  `clientdatasourceid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientfirmid` bigint(20) DEFAULT NULL,
  `sourcelink` varchar(255) DEFAULT NULL,
  `clientid` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  `realmid` varchar(4000) DEFAULT NULL,
  `refreshtoken` varchar(4000) DEFAULT NULL,
   `accesstoken` varchar(4000) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientdatasourceid`),
  KEY `fk_cfa` (`clientfirmid`),
  CONSTRAINT `clientdatasource_ibfk_1` FOREIGN KEY (`clientfirmid`) REFERENCES `clientfirm` (`clientfirmid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci