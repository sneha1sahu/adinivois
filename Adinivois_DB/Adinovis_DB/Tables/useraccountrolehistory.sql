﻿CREATE TABLE `adinovis`.`useraccountrole_history` (
  `useraccountrolehistoryID` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountroleID` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `userroleid` bigint(20) DEFAULT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`useraccountrolehistoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4