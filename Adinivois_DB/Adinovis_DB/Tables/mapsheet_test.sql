﻿CREATE TABLE `mapsheet_test` (
  `mapsheetid` bigint(20) NOT NULL AUTO_INCREMENT,
  `finsubgroupid` int(20) DEFAULT NULL,
  `balancetypeid` int(20) DEFAULT NULL,
  `statementtypeid` int(20) DEFAULT NULL,
  `leadsheetid` int(20) DEFAULT NULL,
  `mapsheetname` varchar(255) DEFAULT NULL,
  `map_No` varchar(100) DEFAULT NULL,
  `Tax_GIFI_codes` varchar(100) DEFAULT NULL,
  `searchindex` varchar(100) DEFAULT NULL,
  `createdby` varchar(100) DEFAULT NULL,
  `modifiedby` varchar(100) DEFAULT NULL,
  `parentmapsheetid` varchar(100) DEFAULT NULL,
  `parentgroupid` int(20) DEFAULT NULL,
  PRIMARY KEY (`mapsheetid`),
  FULLTEXT KEY `flmshetname` (`mapsheetname`),
  FULLTEXT KEY `mapsheetname` (`mapsheetname`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci