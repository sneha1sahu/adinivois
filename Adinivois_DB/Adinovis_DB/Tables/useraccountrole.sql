﻿CREATE TABLE `adinovis`.`useraccountrole` (
  `useraccountroleID` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountid` bigint(20) NOT NULL,
  `userroleid` bigint(20) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`useraccountroleID`),
  KEY `fk_useraccountroleid` (`useraccountid`),
  KEY `fk_userroleid` (`userroleid`),
  CONSTRAINT `fk_useraccountroleid` FOREIGN KEY (`useraccountid`) REFERENCES `useraccount` (`useraccountid`),
  CONSTRAINT `fk_userroleid` FOREIGN KEY (`userroleid`) REFERENCES `userrole` (`userroleid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci