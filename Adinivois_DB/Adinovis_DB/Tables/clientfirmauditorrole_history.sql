﻿CREATE TABLE `clientfirmauditorrole_history` (
  `clientfirmauditorhistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientfirmauditorroleid` bigint(20) DEFAULT NULL,
  `engagementsid` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `userroleid` bigint(20) DEFAULT NULL,
  `rolesequence` int(20) DEFAULT NULL,
  `perhourcal` varchar(100) DEFAULT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientfirmauditorhistoryid`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci