﻿CREATE TABLE `engagements` (
  `engagementsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `clientfirmid` bigint(20) NOT NULL,
  `engagementname` varchar(100) DEFAULT NULL,
  `subentitles` varchar(100) DEFAULT NULL,
  `engagementtypeid` bigint(20) DEFAULT NULL,
  `compilationtype` varchar(100) DEFAULT NULL,
  `finanicalyearenddate` date DEFAULT NULL,
  `additionalinfo` varchar(100) DEFAULT NULL,
  `statusid` bigint(20) DEFAULT NULL,
  `tbloadstatusid` bigint(20) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`engagementsid`),
  KEY `fk_ef` (`clientfirmid`),
  KEY `fk_ef3_idx` (`engagementtypeid`),
  KEY `fk_ef4_idx` (`statusid`),
  KEY `fk_ef5_idx` (`tbloadstatusid`),
  CONSTRAINT `fk_ef` FOREIGN KEY (`clientfirmid`) REFERENCES `clientfirm` (`clientfirmid`),
  CONSTRAINT `fk_ef3` FOREIGN KEY (`engagementtypeid`) REFERENCES `engagementtype` (`engagementtypeid`),
  CONSTRAINT `fk_ef4` FOREIGN KEY (`statusid`) REFERENCES `status` (`statusid`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci