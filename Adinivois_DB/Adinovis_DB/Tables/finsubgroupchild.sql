﻿CREATE TABLE `finsubgroupchild` (
  `finsubgroupchildId` bigint(20) NOT NULL AUTO_INCREMENT,
  `finsubgroupid` bigint(20) NOT NULL,
  `finsubgroupchildname` varchar(1000) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`finsubgroupchildId`),
  KEY `fk_finsubgroupid_1` (`finsubgroupid`),
  CONSTRAINT `finsubgroupchild_ibfk_1` FOREIGN KEY (`finsubgroupid`) REFERENCES `finsubgroup` (`finsubgrpid`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci