﻿CREATE TABLE `adinovis`.`useraccount_history` (
  `useraccounthistory_id` bigint(20) NOT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `emailaddress` varchar(100) DEFAULT NULL,
  `loginpassword` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`useraccounthistory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4