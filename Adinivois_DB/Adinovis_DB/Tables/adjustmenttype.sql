﻿CREATE TABLE`adinovis`.`adjustmenttype`(
    `adjustmenttypeid` bigint(20) NOT NULL AUTO_INCREMENT,
    `adjustmenttypename` varchar(100) NOT NULL,
    `desciption` varchar(100) DEFAULT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`adjustmenttypeid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci