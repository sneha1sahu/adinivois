﻿CREATE TABLE`adinovis`.`usertrailbalance`(
	`usertrailbalanceid` bigint(20) NOT NULL AUTO_INCREMENT,
    `trailbalanceid` bigint(20) NOT NULL,
    `accountcode` varchar(100) DEFAULT NULL,
    `accountname` varchar(255) DEFAULT NULL,
    `accounttype` varchar(100) DEFAULT NULL,
    `original` bigint(20) DEFAULT NULL,
    `acctyear` varchar(50) DEFAULT NULL,
	`isclientdatasource` bit(1) NOT NULL DEFAULT b'0',
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`usertrailbalanceid`),
    KEY`fk_tb ub`(`trailbalanceid`),
    CONSTRAINT`trailbalance_ibfk_1` FOREIGN KEY(`trailbalanceid`) REFERENCES`trailbalance`(`trailbalanceid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci