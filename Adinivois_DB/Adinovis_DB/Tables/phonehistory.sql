﻿CREATE TABLE `adinovis`.`phone_history` (
  `phonehistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `phoneid` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `phonenumber` varchar(255) DEFAULT NULL,
  `countrycode` varchar(255) DEFAULT NULL,
  `phonetype` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`phonehistoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4