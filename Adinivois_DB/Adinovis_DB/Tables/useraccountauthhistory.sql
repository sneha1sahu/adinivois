﻿CREATE TABLE `adinovis`.`useraccountauth_history` (
  `useraccountauthhistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountauthid` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `tokentypeid` bigint(20) DEFAULT NULL,
  `tokenvalue` varchar(255) DEFAULT NULL,
  `Modifiedby` varchar(100) NOT NULL,
  `Modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`useraccountauthhistoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4