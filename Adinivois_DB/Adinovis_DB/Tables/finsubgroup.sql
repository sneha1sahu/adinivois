﻿CREATE TABLE `finsubgroup` (
  `finsubgrpid` bigint(20) NOT NULL AUTO_INCREMENT,
  `finsubgroupname` varchar(1000) NOT NULL,
  `fingroupid` bigint(20) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `sequenceorder` int NOT NULl,
  PRIMARY KEY (`finsubgrpid`),
  KEY `fk_fingroupid` (`fingroupid`),
  CONSTRAINT `finsubgroup_ibfk_1` FOREIGN KEY (`fingroupid`) REFERENCES `fingroup` (`fingroupid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci