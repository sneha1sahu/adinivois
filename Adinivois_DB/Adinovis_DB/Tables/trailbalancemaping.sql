﻿CREATE TABLE `trailbalancemaping` (
  `trailbalancemapingid` bigint(20) NOT NULL AUTO_INCREMENT,
  `trailbalanceid` bigint(20) NOT NULL,
  `mapsheetid` bigint(20) NOT NULL,
   `gificode` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`trailbalancemapingid`),
  KEY `trailbalancemaping_ibfk_1_idx` (`trailbalanceid`),
  KEY `trailbalancemaping_ibfk_2_idx` (`mapsheetid`),
  CONSTRAINT `trailbalancemaping_ibfk_1` FOREIGN KEY (`trailbalanceid`) REFERENCES `trailbalance` (`trailbalanceid`),
  CONSTRAINT `trailbalancemaping_ibfk_2` FOREIGN KEY (`mapsheetid`) REFERENCES `mapsheet` (`mapsheetid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci