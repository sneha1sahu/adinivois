﻿CREATE TABLE `adinovis`.`gificode` (
  `gificodeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `gificode` varchar(100) NOT NULL,
  `gifidescription` varchar(4000) CHARACTER SET utf8 DEFAULT NULL,
  `fingroup` varchar(100) DEFAULT NULL,
  `fingroupid` bigint(20) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gificodeid`) ,
  FULLTEXT KEY `gifnamefulltext` (`gifidescription`),
  FULLTEXT KEY `gfingpfulltext` (`fingroup`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4