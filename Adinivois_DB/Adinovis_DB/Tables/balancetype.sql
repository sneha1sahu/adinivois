﻿CREATE TABLE`adinovis`.`balancetype`(
    `balancetypeid` bigint(20) NOT NULL AUTO_INCREMENT,
    `balancetypename` varchar(100) NOT NULL,
    `balancetypecode` varchar(10) NOT NULL,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`balancetypeid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci