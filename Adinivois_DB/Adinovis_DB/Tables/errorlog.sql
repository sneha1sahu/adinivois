﻿CREATE TABLE `adinovis`.`errorlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `errorcode` varchar(30) NOT NULL,
  `message` text NOT NULL,
  `errorprocedure` varchar(100) DEFAULT NULL,
  `errortime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4