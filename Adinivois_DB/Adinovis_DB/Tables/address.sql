﻿CREATE TABLE `adinovis`.`address` (
  `addressid` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountid` bigint(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` int(20) DEFAULT NULL,
  `isactive` bit(2) NOT NULL DEFAULT b'1',
  `isdelete` bit(2) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`addressid`),
  KEY `fk_address` (`useraccountid`),
  CONSTRAINT `fk_address` FOREIGN KEY (`useraccountid`) REFERENCES `useraccount` (`useraccountid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci