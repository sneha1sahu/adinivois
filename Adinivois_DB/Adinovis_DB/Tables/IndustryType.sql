﻿create table adinovis.IndustryType
(
IndustryId  bigint(20) NOT NULL AUTO_INCREMENT primary key,
IndustryName varchar(1000) NOT NULL,
isactive bit(2) NOT NULL DEFAULT b'1',
isdelete bit(2) NOT NULL DEFAULT b'0',
createdby varchar(100) NOT NULL,
createddate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
modifiedby varchar(100) NOT NULL,
modifieddate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP);