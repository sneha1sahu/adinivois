﻿CREATE TABLE`adinovis`.`clientquestion`(
    `clientquestionid` bigint(20) NOT NULL AUTO_INCREMENT,
    `clientquestiontypeid` bigint(20) NOT NULL,
    `questiondescription` text,
    `answertype` text,
    `isactive` bit(1) NOT NULL DEFAULT b'1',
    `isdelete` bit(1) NOT NULL DEFAULT b'0',
    `createdby` varchar(100) NOT NULL,
    `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modifiedby` varchar(100) NOT NULL,
    `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`clientquestionid`),
    KEY`fk_cques`(`clientquestiontypeid`),
    CONSTRAINT`clientquestion_ibfk_1` FOREIGN KEY(`clientquestiontypeid`) REFERENCES`clientquestiontype`(`clientquestiontypeid`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci