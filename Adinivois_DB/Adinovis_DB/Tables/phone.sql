﻿CREATE TABLE `adinovis`.`phone` (
  `phoneid` bigint(20) NOT NULL AUTO_INCREMENT,
  `useraccountid` bigint(20) DEFAULT NULL,
  `phonenumber` varchar(255) NOT NULL,
  `countrycode` varchar(255) DEFAULT NULL,
  `phonetype` varchar(255) NOT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`phoneid`),
  KEY `fk_phone` (`useraccountid`),
  CONSTRAINT `fk_phone` FOREIGN KEY (`useraccountid`) REFERENCES `useraccount` (`useraccountid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci