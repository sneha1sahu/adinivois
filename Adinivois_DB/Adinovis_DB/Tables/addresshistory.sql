﻿CREATE TABLE `adinovis`.`address_history` (
  `addresshistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `addressid` bigint(20) DEFAULT NULL,
  `useraccountid` bigint(20) DEFAULT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` int(20) DEFAULT NULL,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`addresshistoryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4