CREATE TABLE `engagements_history` (
  `engagementshistoryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `engagementsid` bigint(20) NOT NULL,
  `clientfirmid` bigint(20) NOT NULL,
  `engagementname` varchar(100) DEFAULT NULL,
  `subentitles` varchar(100) DEFAULT NULL,
  `engagementtypeid` bigint(20) DEFAULT NULL,
  `compilationtype` varchar(100) DEFAULT NULL,
  `finanicalyearenddate` date DEFAULT NULL,
  `additionalinfo` varchar(100) DEFAULT NULL,
  `statusid` bigint(20) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `isdelete` bit(1) NOT NULL DEFAULT b'0',
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`engagementshistoryid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci