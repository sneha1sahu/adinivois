﻿CREATE TABLE `mappingstatus` (
  `mappingstatusid` int(11) NOT NULL AUTO_INCREMENT,
  `mappingstatusname` varchar(100) DEFAULT NULL,
  `mappingstatusdescription` varchar(255) DEFAULT NULL,
  `isactive` bit(1) DEFAULT b'1',
  `isdelete` bit(1) DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mappingstatusid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci