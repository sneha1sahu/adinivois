﻿CREATE TABLE `fsnamechange` (
  `fsnamechangeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `engagementsid` bigint(20) NOT NULL,
  `acctyear` varchar(50) DEFAULT NULL,
  `fstype` varchar(255) DEFAULT NULL,
  `fsid` varchar(255) DEFAULT NULL,
  `currentname` varchar(255) DEFAULT NULL,
  `newname` varchar(255) DEFAULT NULL,
  `isactive` bit(2) NOT NULL DEFAULT b'1',
  `isdelete` bit(2) NOT NULL DEFAULT b'0',
  `createdby` varchar(100) NOT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` varchar(100) NOT NULL,
  `modifieddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fsnamechangeid`),
  KEY `fsnamechange_FK_engid_idx` (`engagementsid`),
  CONSTRAINT `fsnamechange_FK_engid` FOREIGN KEY (`engagementsid`) REFERENCES `engagements` (`engagementsid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci